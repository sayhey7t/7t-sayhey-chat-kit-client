module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    'jest/globals': true,
    node: true
  },
  extends: [
    'airbnb-typescript/base',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier',
    'prettier/@typescript-eslint',
    'plugin:security/recommended'
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['simple-import-sort', 'prettier', 'jest', '@typescript-eslint', 'import', 'security'],
  root: true,
  rules: {
    'security/detect-bidi-characters': 'error',
    'security/detect-buffer-noassert': 'error',
    'security/detect-child-process': 'error',
    'security/detect-disable-mustache-escape': 'error',
    'security/detect-eval-with-expression': 'error',
    'security/detect-new-buffer': 'error',
    'security/detect-no-csrf-before-method-override': 'error',
    'security/detect-non-literal-fs-filename': 'error',
    'security/detect-non-literal-regexp': 'error',
    'security/detect-non-literal-require': 'error',
    'security/detect-object-injection': 'error',
    'security/detect-possible-timing-attacks': 'error',
    'security/detect-pseudoRandomBytes': 'error',
    'security/detect-unsafe-regex': 'error',
    '@typescript-eslint/camelcase': 'error',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/no-use-before-define': 'off',
    '@typescript-eslint/no-unused-expressions': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    'arrow-body-style': 'warn',
    camelcase: 'error',
    'class-methods-use-this': 'off',
    curly: ['error', 'all'],
    'func-names': 'off',
    'global-require': 'off',
    'import/first': 'error',
    'import/no-extraneous-dependencies': 'off',
    'import/no-unresolved': 'warn',
    'import/order': 'off',
    'import/prefer-default-export': 'off',
    'import/unambiguous': 'off',
    'lines-around-comment': [
      'error',
      {
        afterBlockComment: false,
        afterLineComment: false,
        allowArrayEnd: true,
        allowArrayStart: true,
        allowBlockEnd: true,
        allowBlockStart: true,
        allowObjectEnd: true,
        allowObjectStart: true,
        beforeBlockComment: true,
        beforeLineComment: false
      }
    ],
    'max-classes-per-file': 'off',
    'no-await-in-loop': 'off',
    'no-confusing-arrow': 'error',
    'no-console': 'warn',
    'no-param-reassign': [
      'warn',
      {
        props: false
      }
    ],
    'no-plusplus': 'off',
    'no-prototype-builtins': 'off',
    'no-restricted-globals': 'warn',
    'no-restricted-syntax': ['warn', 'LabeledStatement', 'WithStatement'],
    'no-shadow': 'warn',
    'no-underscore-dangle': ['warn', { allowAfterThis: true }],
    'no-unused-expressions': 'off',
    'no-unused-vars': [
      'error',
      {
        args: 'none',
        ignoreRestSiblings: false,
        vars: 'all'
      }
    ],
    'no-use-before-define': 'off',
    'one-var': 'warn',
    'prefer-arrow-callback': ['off'],
    'prefer-destructuring': 'warn',
    'prettier/prettier': 'off', // Will be autofixed
    'simple-import-sort/sort': [
      'error',
      {
        groups: [
          // Node.js builtins. You could also generate this regex if you use a `.js` config.
          // For example: `^(${require("module").builtinModules.join("|")})(/|$)`
          // [
          //   '^(assert|buffer|child_process|cluster|console|constants|crypto|dgram|dns|domain|events|fs|http|https|module|net|os|path|punycode|querystring|readline|repl|stream|string_decoder|sys|timers|tls|tty|url|util|vm|zlib|freelist|v8|process|async_hooks|http2|perf_hooks)(/.*|$)',
          // ],
          // Packages. `react` related packages come first.
          ['^react', '^@?\\w'],
          // Internal packages.
          // Side effect imports.
          ['^\\u0000'],

          ['^(@|@tests|@src|@db|@services|@middlewares|@controllers|@tools|@utils)(/.*|$)'],
          // Parent imports. Put `..` last.
          // ['^\\.\\.(?!/?$)', '^\\.\\./?$'],
          ['^\\.\\.(?!/?$)'],
          // Other relative imports. Put same-folder imports and `.` last.
          // ['^\\./(?=.*/)(?!/?$)', '^\\.(?!/?$)', '^\\./?$'],
          ['^\\.(?!/?$)', '^\\./?$']
        ]
      }
    ],
    'sort-imports': 'off',
    'spaced-comment': 'off',
    '@typescript-eslint/no-namespace': 'off'
  }
}
