import { ChatAdminKit } from '../../src/chat-admin-kit'

/**
 * Dummy test
 */
describe('Dummy test', () => {
  it('works if true is truthy', () => {
    expect(true).toBeTruthy()
  })

  it('DummyClass is instantiable', () => {
    expect(new ChatAdminKit()).toBeInstanceOf(ChatAdminKit)
  })
})
