import axios from 'axios';
import decode from 'jwt-decode';
import { ok, err } from 'neverthrow';
import { length } from 'stringz';
import PublisherKitClient from '@7t/publisher-kit-client';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
}

/**
 * Http Types
 */
var _a, _b, _c, _d, _e;
var HttpMethod;
(function (HttpMethod) {
    HttpMethod["Post"] = "post";
    HttpMethod["Get"] = "get";
    HttpMethod["Delete"] = "delete";
    HttpMethod["Patch"] = "patch";
    HttpMethod["Put"] = "put";
})(HttpMethod || (HttpMethod = {}));
/**
 * Reaction Types
 */
var ReactionType;
(function (ReactionType) {
    ReactionType["Thumb"] = "Thumb";
    ReactionType["Heart"] = "Heart";
    ReactionType["Clap"] = "Clap";
    ReactionType["HaHa"] = "HaHa";
    ReactionType["Exclamation"] = "Exclamation";
})(ReactionType || (ReactionType = {}));
var ReactionsObject = /** @class */ (function () {
    function ReactionsObject() {
        this[_a] = 0;
        this[_b] = 0;
        this[_c] = 0;
        this[_d] = 0;
        this[_e] = 0;
    }
    return ReactionsObject;
}());
_a = ReactionType.Thumb, _b = ReactionType.Heart, _c = ReactionType.Clap, _d = ReactionType.HaHa, _e = ReactionType.Exclamation;
/**
 * Message Types
 */
var ImageSizes;
(function (ImageSizes) {
    ImageSizes["Tiny"] = "tiny";
    ImageSizes["Small"] = "small";
    ImageSizes["Medium"] = "medium";
    ImageSizes["Large"] = "large";
    ImageSizes["XLarge"] = "xlarge";
})(ImageSizes || (ImageSizes = {}));
var FileType;
(function (FileType) {
    FileType["Video"] = "video";
    FileType["Image"] = "image";
    FileType["Audio"] = "audio";
    FileType["Other"] = "other";
})(FileType || (FileType = {}));
var MessageType;
(function (MessageType) {
    MessageType["System"] = "system";
    MessageType["Text"] = "text";
    MessageType["Image"] = "image";
    MessageType["Video"] = "video";
    MessageType["Document"] = "document";
    MessageType["Gif"] = "gif";
    MessageType["Emoji"] = "emoji";
    MessageType["Audio"] = "audio";
})(MessageType || (MessageType = {}));
var HighLightType;
(function (HighLightType) {
    HighLightType["Text"] = "text";
    HighLightType["Email"] = "email";
    HighLightType["Phone"] = "phone";
    HighLightType["Url"] = "url";
    HighLightType["UserId"] = "userId";
})(HighLightType || (HighLightType = {}));
var SystemMessageType;
(function (SystemMessageType) {
    SystemMessageType["MemberRemoved"] = "MemberRemoved";
    SystemMessageType["MemberAdded"] = "MemberAdded";
    SystemMessageType["OwnerRemoved"] = "OwnerRemoved";
    SystemMessageType["OwnerAdded"] = "OwnerAdded";
    SystemMessageType["GroupPictureChanged"] = "GroupPictureChanged";
    SystemMessageType["GroupNameChanged"] = "GroupNameChanged";
    SystemMessageType["GroupDisabled"] = "GroupDisabled";
    SystemMessageType["GroupCreated"] = "GroupCreated";
    SystemMessageType["IndividualChatCreated"] = "IndividualChatCreated";
    SystemMessageType["ExternalUserConversationInvitationAccepted"] = "ExternalUserConversationInvitationAccepted";
    SystemMessageType["ExternalUserNudged"] = "ExternalUserNudged";
})(SystemMessageType || (SystemMessageType = {}));
/**
 * Conversation Types
 */
var ConversationType;
(function (ConversationType) {
    ConversationType["Group"] = "group";
    ConversationType["Individual"] = "individual";
})(ConversationType || (ConversationType = {}));
var ConversationGroupType;
(function (ConversationGroupType) {
    ConversationGroupType["Regular"] = "regular";
    ConversationGroupType["Quick"] = "quick";
    ConversationGroupType["Space"] = "space";
    ConversationGroupType["Sms"] = "sms";
})(ConversationGroupType || (ConversationGroupType = {}));
/**
 * User Types
 */
var UserScope;
(function (UserScope) {
    UserScope["Internal"] = "internal";
    UserScope["External"] = "external";
})(UserScope || (UserScope = {}));
var UserTypes;
(function (UserTypes) {
    UserTypes["User"] = "user";
    UserTypes["Admin"] = "admin";
    UserTypes["HrAdmin"] = "hr_admin";
    UserTypes["SuperHrAdmin"] = "super_hr_admin";
})(UserTypes || (UserTypes = {}));
/* User Admin Type */
var UserAdminSortFields;
(function (UserAdminSortFields) {
    UserAdminSortFields["UserAdminName"] = "userAdminName";
    UserAdminSortFields["UserAdminEmail"] = "userAdminEmail";
    UserAdminSortFields["UserAdminRole"] = "userAdminRole";
})(UserAdminSortFields || (UserAdminSortFields = {}));
var UserAdminStatusType;
(function (UserAdminStatusType) {
    UserAdminStatusType["Enabled"] = "enabled";
    UserAdminStatusType["Disabled"] = "disabled";
    UserAdminStatusType["All"] = "all";
})(UserAdminStatusType || (UserAdminStatusType = {}));
var APISignature;
(function (APISignature) {
    APISignature["ME"] = "me";
})(APISignature || (APISignature = {}));
/* User Management Type */
var UserManagementType;
(function (UserManagementType) {
    UserManagementType["Perm"] = "permanent";
    UserManagementType["Temp"] = "temporary";
})(UserManagementType || (UserManagementType = {}));
var UserManagementSortFields;
(function (UserManagementSortFields) {
    UserManagementSortFields["UserName"] = "userName";
    UserManagementSortFields["UserEmail"] = "userEmail";
    UserManagementSortFields["UserEmpCode"] = "userEmployeeCode";
    UserManagementSortFields["UserDepartment"] = "userDepartment";
    UserManagementSortFields["UserTitle"] = "userTitle";
    UserManagementSortFields["UserHrAdmin"] = "userHrAdmin";
})(UserManagementSortFields || (UserManagementSortFields = {}));
var UserStatusType;
(function (UserStatusType) {
    UserStatusType["Enabled"] = "enabled";
    UserStatusType["Disabled"] = "disabled";
    UserStatusType["All"] = "all";
})(UserStatusType || (UserStatusType = {}));
var UserMovementSelected = /** @class */ (function () {
    function UserMovementSelected() {
    }
    return UserMovementSelected;
}());
var UnmanagedUsersSortFields;
(function (UnmanagedUsersSortFields) {
    UnmanagedUsersSortFields["UserName"] = "userName";
    UnmanagedUsersSortFields["UserEmail"] = "userEmail";
    UnmanagedUsersSortFields["UserEmpCode"] = "userEmployeeCode";
    UnmanagedUsersSortFields["UserDepartment"] = "userDepartment";
    UnmanagedUsersSortFields["UserTitle"] = "userTitle";
})(UnmanagedUsersSortFields || (UnmanagedUsersSortFields = {}));
var ConversationUserQueryType;
(function (ConversationUserQueryType) {
    ConversationUserQueryType["All"] = "all";
    ConversationUserQueryType["Current"] = "current";
    ConversationUserQueryType["Removed"] = "removed";
})(ConversationUserQueryType || (ConversationUserQueryType = {}));
var Publisher;
(function (Publisher) {
    var EventType;
    (function (EventType) {
        EventType["NewMessage"] = "NEW_MESSAGE";
        EventType["ConversationCreated"] = "CONVERSATION_CREATED";
        EventType["ConversationMuted"] = "CONVERSATION_MUTED";
        EventType["ConversationUnmuted"] = "CONVERSATION_UNMUTED";
        EventType["ConversationPinned"] = "CONVERSATION_PINNED";
        EventType["ConversationUnpinned"] = "CONVERSATION_UNPINNED";
        EventType["ConversationHideAndClear"] = "CONVERSATION_HIDE_AND_CLEAR";
        EventType["ConversationHideAndClearAll"] = "CONVERSATION_HIDE_AND_CLEAR_ALL";
        EventType["ConversationGroupInfoChanged"] = "CONVERSATION_GROUP_INFO_CHANGED";
        EventType["ConversationQuickGroupChangedToGroup"] = "CONVERSATION_QUICK_GROUP_CHANGED_TO_GROUP";
        EventType["ExternalUserConversationInvitationAccepted"] = "EXTERNAL_USER_CONVERSATION_INVITATION_ACCEPTED";
        EventType["UserInfoChanged"] = "USER_INFO_CHANGED";
        EventType["ConversationGroupOwnerAdded"] = "CONVERSATION_GROUP_OWNER_ADDED";
        EventType["ConversationGroupOwnerRemoved"] = "CONVERSATION_GROUP_OWNER_REMOVED";
        EventType["ConversationGroupMemberAdded"] = "CONVERSATION_GROUP_MEMBER_ADDED";
        EventType["ConversationGroupMemberRemoved"] = "CONVERSATION_GROUP_MEMBER_REMOVED";
        EventType["UserReactedToMessage"] = "USER_REACTED_TO_MESSAGE";
        EventType["UserUnReactedToMessage"] = "USER_UNREACTED_TO_MESSAGE";
        EventType["MessageReactionsUpdate"] = "MESSAGE_REACTIONS_UPDATE";
        EventType["UserDisabled"] = "USER_DISABLED";
        EventType["UserEnabled"] = "USER_ENABLED";
        EventType["MessageDeleted"] = "MESSAGE_DELETED";
        EventType["UserDeleted"] = "USER_DELETED";
        EventType["MessageRead"] = "MESSAGE_READ";
        EventType["ExternalUserNudged"] = "EXTERNAL_USER_NUDGED";
    })(EventType = Publisher.EventType || (Publisher.EventType = {}));
})(Publisher || (Publisher = {}));
var SayHey;
(function (SayHey) {
    var ChatKitError;
    (function (ChatKitError) {
        var ErrorType;
        (function (ErrorType) {
            var InternalErrorType;
            (function (InternalErrorType) {
                InternalErrorType["Unknown"] = "InternalUnknown";
                InternalErrorType["Uninitialized"] = "InternalUninitialized";
                InternalErrorType["AuthMissing"] = "InternalAuthMissing";
                InternalErrorType["BadData"] = "InternalBadData";
                InternalErrorType["MissingParameter"] = "InternalMissingParameter";
            })(InternalErrorType = ErrorType.InternalErrorType || (ErrorType.InternalErrorType = {}));
            var AuthErrorType;
            (function (AuthErrorType) {
                AuthErrorType["InvalidKeyErrorType"] = "AuthErrorTypeInvalidKey";
                AuthErrorType["InvalidUserType"] = "AuthErrorTypeInvalidUserType";
                AuthErrorType["NotFoundErrorType"] = "AuthErrorTypeNotFound";
                AuthErrorType["MissingParamsErrorType"] = "AuthErrorTypeMissingParams";
                AuthErrorType["InvalidPassword"] = "AuthErrorTypeInvalidPassword";
                AuthErrorType["TokenExpiredType"] = "AuthErrorTypeTokenExpired";
                AuthErrorType["AppHasNoNotificationSetup"] = "AuthErrorTypeAppHasNoNotificationSetup";
                AuthErrorType["NotPartOfApp"] = "AuthErrorTypeNotPartOfApp";
                AuthErrorType["InvalidWebhookKey"] = "AuthErrorTypeInvalidWebhookKey";
                AuthErrorType["NoAccessToUser"] = "AuthErrorTypeNoAccessToUser";
                AuthErrorType["ServerNoAccessToUser"] = "AuthErrorTypeServerNoAccessToUser";
                AuthErrorType["IncorrectPassword"] = "AuthErrorTypeIncorrectPassword";
                AuthErrorType["NoSamePassword"] = "AuthErrorTypeNoSamePassword";
                AuthErrorType["NoPasswordSet"] = "AuthErrorTypeNoPasswordSet";
                AuthErrorType["PasswordAlreadySet"] = "AuthErrorTypePasswordAlreadySet";
                AuthErrorType["AccountNotPreVerified"] = "AuthErrorTypeAccountNotPreVerified";
                AuthErrorType["AccountAlreadyPreVerified"] = "AuthErrorTypeAccountAlreadyPreVerified";
                AuthErrorType["NoAppAccessToUser"] = "AuthErrorTypeNoAppAccessToUser";
                AuthErrorType["TokenNotStarted"] = "AuthErrorTypeTokenNotStarted";
                AuthErrorType["AccountAlreadyExists"] = "AuthErrorTypeAccountAlreadyExists";
                AuthErrorType["AdminAlreadyExists"] = "AuthErrorTypeAdminAlreadyExists";
                AuthErrorType["UserNotFound"] = "AuthErrorTypeUserNotFound";
                AuthErrorType["InvalidToken"] = "AuthErrorTypeInvalidToken";
                AuthErrorType["MissingAuthHeaderField"] = "AuthErrorTypeMissingAuthHeaderField";
                AuthErrorType["InvalidCredentials"] = "AuthErrorTypeInvalidCredentials";
                AuthErrorType["HashGeneration"] = "AuthErrorTypeHashGeneration";
                AuthErrorType["HashComparison"] = "AuthErrorTypeHashComparison";
                AuthErrorType["RefreshTokenAlreadyExchanged"] = "AuthErrorTypeRefreshTokenAlreadyExchanged";
                AuthErrorType["RefreshTokenNotFound"] = "AuthErrorTypeRefreshTokenNotFound";
                AuthErrorType["UserDisabled"] = "AuthErrorTypeUserDisabled";
                AuthErrorType["InvalidOTP"] = "AuthErrorTypeInvalidOTP";
                AuthErrorType["OTPExpired"] = "AuthErrorTypeOTPExpired";
                AuthErrorType["AdminNotAssociatedWithApp"] = "AuthErrorTypeAdminNotAssociatedWithApp";
                AuthErrorType["InvitationCodeNotFound"] = "AuthErrorTypeInvitationCodeNotFound";
                AuthErrorType["InvitationCodeExpired"] = "AuthErrorTypeInvitationCodeExpired";
                AuthErrorType["InvitationCodeRedeemed"] = "AuthErrorTypeInvitationCodeRedeemed";
                AuthErrorType["InvitationNotForUser"] = "AuthErrorTypeInvitationNotForUser";
                AuthErrorType["UserAlreadySignedUp"] = "AuthErrorTypeUserAlreadySignedUp";
                AuthErrorType["SignUpIncomplete"] = "AuthErrorTypeSignUpIncomplete";
                AuthErrorType["UserInactive"] = "AuthErrorTypeUserInactive";
                AuthErrorType["SignInIntegrated"] = "AuthErrorTypeSignInIntegrated";
                AuthErrorType["SelfSignUpNotAllowed"] = "AuthErrorTypeSelfSignUpNotAllowed";
                AuthErrorType["EmailDoesNotExist"] = "AuthErrorTypeEmailDoesNotExist";
                AuthErrorType["MissingIntegratedLoginUrl"] = "AuthErrorTypeMissingIntegratedLoginUrl";
                AuthErrorType["InvalidIntegratedLoginUrl"] = "AuthErrorTypeInvalidIntegratedLoginUrl";
                AuthErrorType["YouMustAcceptTermsAndPrivacy"] = "AuthErrorTypeYouMustAcceptTermsAndPrivacy";
                AuthErrorType["TooManyFailedAttempts"] = "AuthErrorTypeTooManyFailedAttempts";
                AuthErrorType["InCorrectUserNameOrPassword"] = "AuthErrorTypeInCorrectUserNameOrPassword";
                AuthErrorType["InCorrectPhoneNumberOrOtp"] = "AuthErrorTypeInCorrectPhoneNumberOrOtp";
                AuthErrorType["InCorrectUserOrOtp"] = "AuthErrorTypeInCorrectUserOrOtp";
            })(AuthErrorType = ErrorType.AuthErrorType || (ErrorType.AuthErrorType = {}));
            var FileErrorType;
            (function (FileErrorType) {
                FileErrorType["SizeLimitExceeded"] = "FileErrorTypeSizeLimitExceeded";
                FileErrorType["NoFileSentType"] = "FileErrorTypeNoFileSentType";
                FileErrorType["InvalidFieldNameForFile"] = "FileErrorTypeInvalidFieldNameForFile";
                FileErrorType["InvalidMetadata"] = "FileErrorTypeInvalidMetadata";
                FileErrorType["FileSizeLimitExceeded"] = "FileErrorTypeFileSizeLimitExceeded";
                FileErrorType["InvalidFileName"] = "FileErrorTypeInvalidFileName";
                FileErrorType["NotSent"] = "FileErrorTypeNotSent";
                FileErrorType["NotFound"] = "FileErrorTypeNotFound";
                FileErrorType["NoThumbnailFound"] = "FileErrorTypeNoThumbnailFound";
                FileErrorType["ImageNotForConversation"] = "FileErrorTypeImageNotForConversation";
                FileErrorType["ImageNotForUser"] = "FileErrorTypeImageNotForUser";
                FileErrorType["FileNotForMessage"] = "FileErrorTypeFileNotForMessage";
                FileErrorType["SizeNotFound"] = "FileErrorTypeSizeNotFound";
                FileErrorType["NotForApp"] = "FileErrorTypeNotForApp";
                FileErrorType["IncorrectFileType"] = "FileErrorTypeIncorrectFileType";
                FileErrorType["NoHeadersFound"] = "FileErrorTypeNoHeadersFound";
                FileErrorType["InvalidHeaders"] = "FileErrorTypeInvalidHeaders";
            })(FileErrorType = ErrorType.FileErrorType || (ErrorType.FileErrorType = {}));
            var ServerErrorType;
            (function (ServerErrorType) {
                ServerErrorType["UnknownError"] = "UnknownError";
                ServerErrorType["InternalServerError"] = "InternalServerError";
            })(ServerErrorType = ErrorType.ServerErrorType || (ErrorType.ServerErrorType = {}));
            var GeneralErrorType;
            (function (GeneralErrorType) {
                GeneralErrorType["MissingQueryParams"] = "GeneralErrorTypeMissingQueryParams";
                GeneralErrorType["RouteNotFound"] = "GeneralErrorTypeRouteNotFound";
                GeneralErrorType["LimitNotValid"] = "GeneralErrorTypeLimitNotValid";
                GeneralErrorType["OffsetNotValid"] = "GeneralErrorTypeOffsetNotValid";
                GeneralErrorType["MissingInfoToUpdate"] = "GeneralErrorTypeMissingInfoToUpdate";
            })(GeneralErrorType = ErrorType.GeneralErrorType || (ErrorType.GeneralErrorType = {}));
            var MessageErrorType;
            (function (MessageErrorType) {
                MessageErrorType["InvalidDate"] = "MessageErrorTypeInvalidDate";
                MessageErrorType["NotFound"] = "MessageErrorTypeNotFound";
                MessageErrorType["NotFoundForUser"] = "MessageErrorTypeNotFoundForUser";
                MessageErrorType["MessageAlreadyRead"] = "MessageErrorTypeMessageAlreadyRead";
                MessageErrorType["CannotDeleteSystemMessage"] = "MessageErrorTypeCannotDeleteSystemMessage";
                MessageErrorType["MissingMessageId"] = "MessageErrorTypeMissingMessageId";
                MessageErrorType["IsoDateStringRequired"] = "MessageErrorTypeIsoDateStringRequired";
            })(MessageErrorType = ErrorType.MessageErrorType || (ErrorType.MessageErrorType = {}));
            var UserErrorType;
            (function (UserErrorType) {
                UserErrorType["AlreadyMuted"] = "UserErrorTypeAlreadyMuted";
                UserErrorType["NotOnMute"] = "UserErrorTypeNotOnMute";
                UserErrorType["AlreadyEnabled"] = "UserErrorTypeAlreadyEnabled";
                UserErrorType["AlreadyDisabled"] = "UserErrorTypeAlreadyDisabled";
                UserErrorType["NotFound"] = "UserErrorTypeNotFound";
            })(UserErrorType = ErrorType.UserErrorType || (ErrorType.UserErrorType = {}));
            var UserRegistrationErrorType;
            (function (UserRegistrationErrorType) {
                UserRegistrationErrorType["ContactCompanyAdmin"] = "UserRegistrationErrorTypeContactCompanyAdmin";
            })(UserRegistrationErrorType = ErrorType.UserRegistrationErrorType || (ErrorType.UserRegistrationErrorType = {}));
            var ConversationErrorType;
            (function (ConversationErrorType) {
                ConversationErrorType["NotFound"] = "ConversationErrorTypeNotFound";
                ConversationErrorType["CannotConverseWithUser"] = "ConversationErrorTypeCannotConverseWithUser";
                ConversationErrorType["NotAnOwner"] = "ConversationErrorTypeNotAnOwner";
                ConversationErrorType["AlreadyAnOwner"] = "ConversationErrorTypeAlreadyAnOwner";
                ConversationErrorType["NotFoundForApp"] = "ConversationErrorTypeNotFoundForApp";
                ConversationErrorType["InvalidType"] = "ConversationErrorTypeInvalidType";
                ConversationErrorType["InvalidConversationGroupType"] = "ConversationErrorTypeInvalidConversationGroupType";
                ConversationErrorType["InvalidConversationUserType"] = "ConversationErrorTypeInvalidConversationUserType";
                ConversationErrorType["MustBeOwner"] = "ConversationErrorTypeMustBeOwner";
                ConversationErrorType["MustBeCreator"] = "ConversationErrorTypeMustBeCreator";
                ConversationErrorType["NotPartOfApp"] = "ConversationErrorTypeNotPartOfApp";
                ConversationErrorType["NotFoundForUser"] = "ConversationErrorTypeNotFoundForUser";
                ConversationErrorType["AlreadyExists"] = "ConversationErrorTypeAlreadyExists";
                ConversationErrorType["CannotChatWithSelf"] = "ConversationErrorTypeCannotChatWithSelf";
                ConversationErrorType["AlreadyMuted"] = "ConversationErrorTypeAlreadyMuted";
                ConversationErrorType["NotOnMute"] = "ConversationErrorTypeNotOnMute";
                ConversationErrorType["AlreadyPinned"] = "ConversationErrorTypeAlreadyPinned";
                ConversationErrorType["NotPinned"] = "ConversationErrorTypeNotPinned";
                ConversationErrorType["UsersNotInApp"] = "ConversationErrorTypeUsersNotInApp";
                ConversationErrorType["NoValidUsersToAdd"] = "ConversationErrorTypeNoValidUsersToAdd";
                ConversationErrorType["NoValidUsersToRemove"] = "ConversationErrorTypeNoValidUsersToRemove";
                ConversationErrorType["CannotChangeAccessToSelf"] = "ConversationErrorTypeCannotChangeAccessToSelf";
                ConversationErrorType["CannotAddExistingUsers"] = "ConversationErrorTypeCannotAddExistingUsers";
                ConversationErrorType["AlreadyRemovedUsers"] = "ConversationErrorTypeAlreadyRemovedUsers";
                ConversationErrorType["UsersNotInConversation"] = "ConversationErrorTypeUsersNotInConversation";
                ConversationErrorType["CannotRemoveOwners"] = "ConversationErrorTypeCannotRemoveOwners";
                ConversationErrorType["DivisionsNotInApp"] = "ConversationErrorTypeDivisionsNotInApp";
                ConversationErrorType["CannotAddExistingDivisions"] = "ConversationErrorTypeCannotAddExistingDivisions";
                ConversationErrorType["CannotRemoveNonExistingDivisions"] = "ConversationErrorTypeCannotRemoveNonExistingDivisions";
                ConversationErrorType["DivisionIdOrMemberIdRequiredForSpaceGroup"] = "ConversationErrorTypeDivisionIdOrMemberIdRequiredForSpaceGroup";
                ConversationErrorType["NudgeLessThanThreshold"] = "ConversationErrorTypeNudgeLessThanThreshold";
            })(ConversationErrorType = ErrorType.ConversationErrorType || (ErrorType.ConversationErrorType = {}));
            var UserManagementErrorType;
            (function (UserManagementErrorType) {
                UserManagementErrorType["NoSameAdminAssignment"] = "UserManagementErrorTypeNoSameAdminAssignment";
                UserManagementErrorType["MustBeHrAdmin"] = "UserManagementErrorTypeMustBeHrAdmin";
                UserManagementErrorType["NoDuplicateUsers"] = "UserManagementErrorTypeNoDuplicateUsers";
                UserManagementErrorType["NotHrAdmin"] = "UserManagementErrorTypeNotHrAdmin";
                UserManagementErrorType["UserAlreadyAssigned"] = "UserManagementErrorTypeUserAlreadyAssigned";
            })(UserManagementErrorType = ErrorType.UserManagementErrorType || (ErrorType.UserManagementErrorType = {}));
            var DateErrorType;
            (function (DateErrorType) {
                DateErrorType["InvalidFromDateError"] = "DateErrorTypeInvalidFromDateError";
                DateErrorType["InvalidToDateError"] = "DateErrorTypeInvalidToDateError";
            })(DateErrorType = ErrorType.DateErrorType || (ErrorType.DateErrorType = {}));
            var AppRegistrationErrorType;
            (function (AppRegistrationErrorType) {
                AppRegistrationErrorType["MissingEmailConfigParams"] = "AppRegistrationErrorTypeMissingEmailConfigParams";
            })(AppRegistrationErrorType = ErrorType.AppRegistrationErrorType || (ErrorType.AppRegistrationErrorType = {}));
            var OTPErrorType;
            (function (OTPErrorType) {
                OTPErrorType["MissingEmailService"] = "OTPErrorTypeMissingEmailService";
            })(OTPErrorType = ErrorType.OTPErrorType || (ErrorType.OTPErrorType = {}));
            var EmailErrorType;
            (function (EmailErrorType) {
                EmailErrorType["MissingEmailService"] = "EmailErrorTypeMissingEmailService";
                EmailErrorType["EmailDomainNotAllowed"] = "EmailErrorTypeEmailDomainNotAllowed";
            })(EmailErrorType = ErrorType.EmailErrorType || (ErrorType.EmailErrorType = {}));
            var SmsErrorType;
            (function (SmsErrorType) {
                SmsErrorType["NotConfigured"] = "SmsErrorTypeNotConfigured";
                SmsErrorType["NotConfiguredForClient"] = "SmsErrorTypeNotConfiguredForClient";
                SmsErrorType["RequiredCountryCode"] = "SmsErrorTypeRequiredCountryCode";
                SmsErrorType["CountryCodeNotSupported"] = "SmsErrorTypeCountryCodeNotSupported";
            })(SmsErrorType = ErrorType.SmsErrorType || (ErrorType.SmsErrorType = {}));
            var DepartmentErrorTpe;
            (function (DepartmentErrorTpe) {
                DepartmentErrorTpe["NotFound"] = "DepartmentErrorTypeNotFound";
                DepartmentErrorTpe["AlreadyExists"] = "DepartmentErrorTypeAlreadyExists";
                DepartmentErrorTpe["MissingInfoToUpdate"] = "DepartmentErrorTypeMissingInfoToUpdate";
                DepartmentErrorTpe["MissingDefaultDeptOrDiv"] = "DepartmentErrorTypeMissingDefaultDeptOrDiv";
                DepartmentErrorTpe["UpdateDefaultNotAllowed"] = "DepartmentErrorTypeUpdateDefaultNotAllowed";
            })(DepartmentErrorTpe = ErrorType.DepartmentErrorTpe || (ErrorType.DepartmentErrorTpe = {}));
            var DivisionErrorType;
            (function (DivisionErrorType) {
                DivisionErrorType["NotPartOfDepartment"] = "DivisionErrorTypeNotPartOfDepartment";
                DivisionErrorType["AlreadyExists"] = "DivisionErrorTypeAlreadyExists";
                DivisionErrorType["DepartmentRequired"] = "DivisionErrorTypeDepartmentRequired";
                DivisionErrorType["UpdateDefaultNotAllowed"] = "DivisionErrorTypeUpdateDefaultNotAllowed";
            })(DivisionErrorType = ErrorType.DivisionErrorType || (ErrorType.DivisionErrorType = {}));
            var AppFeatureErrorTpe;
            (function (AppFeatureErrorTpe) {
                AppFeatureErrorTpe["NotFound"] = "AppFeatureErrorTpeNotFound";
                AppFeatureErrorTpe["AlreadyExists"] = "AppFeatureErrorTypeAlreadyExists";
                AppFeatureErrorTpe["InvalidParams"] = "AppFeatureErrorTypeInvalidParams";
                AppFeatureErrorTpe["AdminPortalLink"] = "AppFeatureErrorTypeAdminPortalLink";
            })(AppFeatureErrorTpe = ErrorType.AppFeatureErrorTpe || (ErrorType.AppFeatureErrorTpe = {}));
            var UserAdminErrorType;
            (function (UserAdminErrorType) {
                UserAdminErrorType["NotFound"] = "UserAdminErrorTypeNotFound";
                UserAdminErrorType["AlreadyExists"] = "UserAdminErrorTypeAlreadyExists";
                UserAdminErrorType["NoAccessToUserAdmin"] = "UserAdminErrorTypeNoAccessToUserAdmin";
                UserAdminErrorType["NoEmailUpdateAccessOfSignedUpUsers"] = "UserAdminErrorTypeNoEmailUpdateAccessOfSignedUpUsers";
                UserAdminErrorType["CannotUpdateSelf"] = "UserAdminErrorTypeCannotUpdateSelf";
                UserAdminErrorType["UserAdminNoAccessToUser"] = "UserAdminNoAccessToUser";
                UserAdminErrorType["NoChangeInLeaveStatus"] = "UserAdminErrorTypeNoChangeInLeaveStatus";
                UserAdminErrorType["SignUpIncomplete"] = "UserAdminErrorTypeSignUpIncomplete";
                UserAdminErrorType["Disabled"] = "UserAdminErrorTypeDisabled";
            })(UserAdminErrorType = ErrorType.UserAdminErrorType || (ErrorType.UserAdminErrorType = {}));
            var AppConfigErrorType;
            (function (AppConfigErrorType) {
                AppConfigErrorType["NotFound"] = "AppConfigErrorTypeNotFound";
                AppConfigErrorType["AlreadyExists"] = "AppConfigErrorTypeAlreadyExists";
            })(AppConfigErrorType = ErrorType.AppConfigErrorType || (ErrorType.AppConfigErrorType = {}));
            var KeywordErrorType;
            (function (KeywordErrorType) {
                KeywordErrorType["NoDuplicates"] = "KeywordErrorTypeNoDuplicates";
                KeywordErrorType["AlreadyExists"] = "KeywordErrorTypeAlreadyExists";
                KeywordErrorType["NotFound"] = "KeywordErrorTypeNotFound";
                KeywordErrorType["NoChangeInPriority"] = "KeywordErrorTypeNoChangeInPriority";
            })(KeywordErrorType = ErrorType.KeywordErrorType || (ErrorType.KeywordErrorType = {}));
            var UserAdminUpdateRoleType;
            (function (UserAdminUpdateRoleType) {
                UserAdminUpdateRoleType["CannotUpdateRoleOfSelf"] = "UserAdminUpdateRoleTypeCannotUpdateRoleOfSelf";
                UserAdminUpdateRoleType["ManagedUserExist"] = "UserAdminUpdateRoleTypeManagedUserExist";
                UserAdminUpdateRoleType["AlreadyHasRequestedRole"] = "UserAdminErrorTypeAlreadyHasRequestedRole";
            })(UserAdminUpdateRoleType = ErrorType.UserAdminUpdateRoleType || (ErrorType.UserAdminUpdateRoleType = {}));
            var FlaggedMessageErrorType;
            (function (FlaggedMessageErrorType) {
                FlaggedMessageErrorType["AlreadyReported"] = "FlaggedMessageErrorTypeAlreadyReported";
                FlaggedMessageErrorType["NotFound"] = "FlaggedMessageErrorTypeNotFound";
                FlaggedMessageErrorType["AlreadyResolved"] = "FlaggedMessageErrorTypeAlreadyResolved";
            })(FlaggedMessageErrorType = ErrorType.FlaggedMessageErrorType || (ErrorType.FlaggedMessageErrorType = {}));
            var UserOnboardingJobErrorType;
            (function (UserOnboardingJobErrorType) {
                UserOnboardingJobErrorType["NotFound"] = "UserOnboardingJobErrorTypeNotFound";
                UserOnboardingJobErrorType["NotCompleted"] = "UserOnboardingJobErrorTypeNotCompleted";
            })(UserOnboardingJobErrorType = ErrorType.UserOnboardingJobErrorType || (ErrorType.UserOnboardingJobErrorType = {}));
            var ExternalUserErrorType;
            (function (ExternalUserErrorType) {
                ExternalUserErrorType["ContactAlreadyExists"] = "ExternalUserErrorTypeContactAlreadyExists";
                ExternalUserErrorType["NotFound"] = "ExternalUserErrorTypeNotFound";
                ExternalUserErrorType["UserDisabled"] = "ExternalUserErrorTypeUserDisabled";
                ExternalUserErrorType["InvalidPhoneNumber"] = "ExternalUserErrorTypeInvalidPhoneNumber";
            })(ExternalUserErrorType = ErrorType.ExternalUserErrorType || (ErrorType.ExternalUserErrorType = {}));
            var ContactErrorType;
            (function (ContactErrorType) {
                ContactErrorType["ContactAlreadyExists"] = "ContactErrorTypeContactAlreadyExists";
            })(ContactErrorType = ErrorType.ContactErrorType || (ErrorType.ContactErrorType = {}));
            var SmsConversationErrorType;
            (function (SmsConversationErrorType) {
                SmsConversationErrorType["NotAnExternalUser"] = "SmsConversationErrorTypeNotAnExternalUser";
                SmsConversationErrorType["ExternalUserNotFound"] = "SmsConversationErrorTypeNotFoundExternalUserNotFound";
            })(SmsConversationErrorType = ErrorType.SmsConversationErrorType || (ErrorType.SmsConversationErrorType = {}));
            var SmsConversationInvitationErrorType;
            (function (SmsConversationInvitationErrorType) {
                SmsConversationInvitationErrorType["NotFound"] = "SmsConversationInvitationErrorTypeNotFound";
                SmsConversationInvitationErrorType["InvitationNotAccepted"] = "SmsConversationInvitationErrorTypeInvitationNotAccepted";
                SmsConversationInvitationErrorType["InvitationAlreadyAccepted"] = "SmsConversationInvitationErrorTypeInvitationAlreadyAccepted";
                SmsConversationInvitationErrorType["InvitationExpired"] = "SmsConversationInvitationErrorTypeInvitationExpired";
            })(SmsConversationInvitationErrorType = ErrorType.SmsConversationInvitationErrorType || (ErrorType.SmsConversationInvitationErrorType = {}));
        })(ErrorType = ChatKitError.ErrorType || (ChatKitError.ErrorType = {}));
        var PushNotificationError;
        (function (PushNotificationError) {
            PushNotificationError["InvalidPushParams"] = "DeviceTokenErrorTypeInvalidPushParams";
            PushNotificationError["AlreadyRegisteredForUser"] = "DeviceTokenErrorTypeAlreadyRegisteredForUser";
            PushNotificationError["NotFound"] = "DeviceTokenErrorTypeNotFound";
            PushNotificationError["NoTokensToPush"] = "DeviceTokenErrorTypeNoTokensToPush";
            PushNotificationError["OnlyStringData"] = "DeviceTokenErrorTypeOnlyStringData";
        })(PushNotificationError = ChatKitError.PushNotificationError || (ChatKitError.PushNotificationError = {}));
        var AppError = /** @class */ (function () {
            function AppError(err, type) {
                var _this = this;
                this.type = ErrorType.InternalErrorType.Unknown;
                this.setStatusCode = function (statusCode) {
                    _this.statusCode = statusCode;
                };
                this.setType = function (type) {
                    _this.type = type;
                };
                this.setMessage = function (msg) {
                    _this.message = msg;
                };
                this.setErrorActions = function (params) {
                    var authFailed = params.authFailed, exchange = params.exchange;
                    _this.exchange = exchange;
                    _this.authFailed = authFailed;
                };
                if (type) {
                    this.type = type;
                }
                if (err instanceof SayHey.Error) {
                    this.error = err;
                    this.message = err.message;
                }
                else {
                    this.message = err;
                }
            }
            return AppError;
        }());
        ChatKitError.AppError = AppError;
    })(ChatKitError = SayHey.ChatKitError || (SayHey.ChatKitError = {}));
    SayHey.EventType = Publisher.EventType;
    // SAFE - Used for export
    // eslint-disable-next-line no-unused-vars
    SayHey.ErrorType = ChatKitError.ErrorType;
    SayHey.Error = ChatKitError.AppError;
})(SayHey || (SayHey = {}));
/* Departments and Divisions */
/* Params */
var tempDefaultColumn;
(function (tempDefaultColumn) {
    tempDefaultColumn[tempDefaultColumn["isDefault"] = 1] = "isDefault";
})(tempDefaultColumn || (tempDefaultColumn = {}));
var SortOrderType;
(function (SortOrderType) {
    SortOrderType[SortOrderType["ASC"] = 1] = "ASC";
    SortOrderType[SortOrderType["DESC"] = -1] = "DESC";
})(SortOrderType || (SortOrderType = {}));
var DepartmentSortFields;
(function (DepartmentSortFields) {
    DepartmentSortFields["DepartmentName"] = "departmentName";
})(DepartmentSortFields || (DepartmentSortFields = {}));
var DivisionSortFields;
(function (DivisionSortFields) {
    DivisionSortFields["DivisionName"] = "divisionName";
})(DivisionSortFields || (DivisionSortFields = {}));
var TokenType;
(function (TokenType) {
    TokenType["AccessToken"] = "TokenTypeAccessToken";
    TokenType["RefreshToken"] = "TokenTypeRefreshToken";
    TokenType["PublisherToken"] = "TokenTypePublisherToken";
    TokenType["ResetPasswordToken"] = "TokenTypeResetPasswordToken";
    TokenType["NewUserSetPasswordToken"] = "TokenTypeNewUserSetPasswordToken";
    TokenType["VerifyAccountToken"] = "TokenTypeVerifyAccountToken";
})(TokenType || (TokenType = {}));
/* Dashboard Types */
var TimeFrameType;
(function (TimeFrameType) {
    TimeFrameType["AllTime"] = "allTime";
    TimeFrameType["Month"] = "month";
    TimeFrameType["Year"] = "year";
})(TimeFrameType || (TimeFrameType = {}));
var UnresolvedMsgScopeType;
(function (UnresolvedMsgScopeType) {
    UnresolvedMsgScopeType["Global"] = "global";
    UnresolvedMsgScopeType["Mine"] = "mine";
})(UnresolvedMsgScopeType || (UnresolvedMsgScopeType = {}));
/* Keyword Types */
var KeyWordPriorityTypes;
(function (KeyWordPriorityTypes) {
    KeyWordPriorityTypes["High"] = "high";
    KeyWordPriorityTypes["Low"] = "low";
})(KeyWordPriorityTypes || (KeyWordPriorityTypes = {}));
/* Message Flagging Types */
var FlaggedMessageType;
(function (FlaggedMessageType) {
    FlaggedMessageType["HIGH"] = "high";
    FlaggedMessageType["LOW"] = "low";
    FlaggedMessageType["USER_REPORTED"] = "user_reported";
})(FlaggedMessageType || (FlaggedMessageType = {}));
/* Audit Types */
var QueryCondition;
(function (QueryCondition) {
    QueryCondition["AND"] = "and";
    QueryCondition["OR"] = "or";
})(QueryCondition || (QueryCondition = {}));
var FlaggedMessagePriorityType;
(function (FlaggedMessagePriorityType) {
    FlaggedMessagePriorityType["HIGH"] = "high";
    FlaggedMessagePriorityType["LOW"] = "low";
})(FlaggedMessagePriorityType || (FlaggedMessagePriorityType = {}));
var ResolutionStatusType;
(function (ResolutionStatusType) {
    ResolutionStatusType["NO_ACTION"] = "no_action";
    ResolutionStatusType["ACTION_TAKEN"] = "action_taken";
})(ResolutionStatusType || (ResolutionStatusType = {}));
var UserOnboardingJobStatus;
(function (UserOnboardingJobStatus) {
    UserOnboardingJobStatus["Completed"] = "completed";
    UserOnboardingJobStatus["Pending"] = "pending";
    UserOnboardingJobStatus["InProgress"] = "in_progress";
})(UserOnboardingJobStatus || (UserOnboardingJobStatus = {}));
var UserOnboardingJobRecordStatus;
(function (UserOnboardingJobRecordStatus) {
    UserOnboardingJobRecordStatus["Completed"] = "completed";
    UserOnboardingJobRecordStatus["Pending"] = "pending";
    UserOnboardingJobRecordStatus["Errored"] = "errored";
})(UserOnboardingJobRecordStatus || (UserOnboardingJobRecordStatus = {}));

var API_VERSION = 'v1';
var ApiKeyName = 'x-sayhey-client-api-key';
var MESSAGE_READ_UPDATE_INTERVAL = 5000;
var ChatClientRoutes = {
    SignIn: {
        Endpoint: '/users/sign-in',
        Method: HttpMethod.Post
    },
    SignUp: {
        Endpoint: '/users/sign-up',
        Method: HttpMethod.Post
    },
    ChangePassword: {
        Endpoint: '/users/change-password',
        Method: HttpMethod.Post
    },
    ForgotPassword: {
        Endpoint: '/users/forgot-password',
        Method: HttpMethod.Post
    },
    ResetPassword: {
        Endpoint: '/users/reset-password',
        Method: HttpMethod.Post
    },
    GetExistingConversationWithUser: {
        Endpoint: function (userId) { return "/users/" + userId + "/conversations/individual"; },
        Method: HttpMethod.Get
    },
    GetExistingIndvidualOrQuickConversation: {
        Endpoint: '/conversations/individual-quick',
        Method: HttpMethod.Get
    },
    GetConversations: {
        Endpoint: '/conversations',
        Method: HttpMethod.Get
    },
    CreateIndividualConversation: {
        Endpoint: '/conversations/individual',
        Method: HttpMethod.Post
    },
    CreateGroupConversation: {
        Endpoint: '/conversations/group',
        Method: HttpMethod.Post
    },
    CreateQuickConversation: {
        Endpoint: '/conversations/quick-group',
        Method: HttpMethod.Post
    },
    GetMessages: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/messages"; },
        Method: HttpMethod.Get
    },
    SendMessage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/messages"; },
        Method: HttpMethod.Post
    },
    MuteConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/mute"; },
        Method: HttpMethod.Put
    },
    UnmuteConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/mute"; },
        Method: HttpMethod.Delete
    },
    PinConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/pin"; },
        Method: HttpMethod.Put
    },
    UnpinConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/pin"; },
        Method: HttpMethod.Delete
    },
    DeleteConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/hide-clear"; },
        Method: HttpMethod.Put
    },
    DeleteAllConversations: {
        Endpoint: "/conversations/hide-clear",
        Method: HttpMethod.Put
    },
    UpdateUserProfileImage: {
        Endpoint: '/users/profile-image',
        Method: HttpMethod.Put
    },
    UpdateUserProfileInfo: {
        Endpoint: '/users/profile-info',
        Method: HttpMethod.Put
    },
    GetConversationReachableUsers: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/conversable-users"; },
        Method: HttpMethod.Get
    },
    GetUserConversationReactions: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/reactions"; },
        Method: HttpMethod.Get
    },
    GetUserConversationMessagesReactions: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/get-messages-reactions"; },
        Method: HttpMethod.Post
    },
    SetMessageReaction: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/reactions/add"; },
        Method: HttpMethod.Put
    },
    RemoveMessageReaction: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/reactions/remove"; },
        Method: HttpMethod.Put
    },
    MuteAllNotifications: {
        Endpoint: '/users/mute-all',
        Method: HttpMethod.Put
    },
    UnmuteAllNotifications: {
        Endpoint: '/users/mute-all',
        Method: HttpMethod.Delete
    },
    GetUserProfile: {
        Endpoint: '/users/profile-info',
        Method: HttpMethod.Get
    },
    MarkMessageAsRead: {
        Endpoint: function (messageId) { return "/read-messages/" + messageId; },
        Method: HttpMethod.Put
    },
    GetConversationBasicUsers: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/basic-users"; },
        Method: HttpMethod.Get
    },
    RegisterPushNotification: {
        Endpoint: '/device-tokens',
        Method: HttpMethod.Post
    },
    UnregisterPushNotification: {
        Endpoint: function (instanceId) { return "/device-tokens/" + instanceId; },
        Method: HttpMethod.Delete
    },
    SetPassword: {
        Endpoint: '/users/set-password',
        Method: HttpMethod.Post
    },
    CheckEmail: {
        Endpoint: '/users/email-check',
        Method: HttpMethod.Post
    },
    RequestSignUpOTP: {
        Endpoint: '/otp/sign-up',
        Method: HttpMethod.Post
    },
    RequestSetPasswordOTP: {
        Endpoint: '/otp/set-password',
        Method: HttpMethod.Post
    },
    RequestResetPasswordOTP: {
        Endpoint: '/otp/reset-password',
        Method: HttpMethod.Post
    },
    RequestPreverificationOTP: {
        Endpoint: '/otp/pre-verification',
        Method: HttpMethod.Post
    },
    ValidateSignUpOTP: {
        Endpoint: '/otp/validate-sign-up',
        Method: HttpMethod.Post
    },
    ValidateSetPasswordOTP: {
        Endpoint: '/otp/validate-set-password',
        Method: HttpMethod.Post
    },
    ValidateResetPasswordOTP: {
        Endpoint: '/otp/validate-reset-password',
        Method: HttpMethod.Post
    },
    ValidatePreverificationOTP: {
        Endpoint: '/otp/validate-pre-verification',
        Method: HttpMethod.Post
    },
    GetAppConfig: {
        Endpoint: '/app-config',
        Method: HttpMethod.Get
    },
    ClientSignInWithToken: {
        Endpoint: '/users/client-signin-with-token',
        Method: HttpMethod.Post
    },
    CreateFlaggedMessage: {
        Endpoint: '/flagged-messages',
        Method: HttpMethod.Post
    },
    /* SMS */
    AddToContact: {
        Endpoint: 'contacts',
        Method: HttpMethod.Post
    },
    GetInternalUserContactsBatch: {
        Endpoint: 'contacts',
        Method: HttpMethod.Get
    },
    FindExternalUserByPhone: {
        Endpoint: 'external-users',
        Method: HttpMethod.Get
    },
    RequestSignInExternalOTP: {
        Endpoint: 'otp/sign-in-external',
        Method: HttpMethod.Post
    },
    SignInExternal: {
        Endpoint: 'users/sign-in-external',
        Method: HttpMethod.Post
    },
    SignInExternalByUserId: {
        Endpoint: function (id) { return "users/" + id + "/sign-in-external'"; },
        Method: HttpMethod.Post
    },
    CreateNewContact: {
        Endpoint: 'users',
        Method: HttpMethod.Post
    },
    AcceptTermsAndPrivacy: {
        Endpoint: 'external-users/accepted-terms-privacy',
        Method: HttpMethod.Post
    },
    GetTermsAndPrivacy: {
        Endpoint: 'external-users/accepted-terms-privacy',
        Method: HttpMethod.Get
    },
    GetExistingSmsConversation: {
        Endpoint: 'conversations/existing-sms-conversations',
        Method: HttpMethod.Get
    },
    CheckSmsConversationInvitation: {
        Endpoint: function (conversationId) {
            return "conversations/" + conversationId + "/smsConversationInvitations/status";
        },
        Method: HttpMethod.Get
    },
    AcceptSmsConversationInvitation: {
        Endpoint: function (conversationId) {
            return "conversations/" + conversationId + "/smsConversationInvitations/status";
        },
        Method: HttpMethod.Patch
    },
    ResendSmsConversationInvitation: {
        Endpoint: function (conversationId) {
            return "conversations/" + conversationId + "/smsConversationInvitations";
        },
        Method: HttpMethod.Put
    },
    CreateSmsConversation: {
        Endpoint: 'conversations/sms-group',
        Method: HttpMethod.Post
    },
    OptToReceiveSms: {
        Endpoint: 'external-users/opted-receive-sms',
        Method: HttpMethod.Put
    },
    OptNotToReceiveSms: {
        Endpoint: 'external-users/opted-receive-sms',
        Method: HttpMethod.Delete
    },
    GetOptToReceiveSmsStatus: {
        Endpoint: 'external-users/opted-receive-sms',
        Method: HttpMethod.Get
    },
    NudgeExternalUserToConversation: {
        Endpoint: function (conversationId) {
            return "conversations/" + conversationId + "/nudge-unread-sms-conversation";
        },
        Method: HttpMethod.Put
    }
};
var ChatAdminRoutes = {
    /* User admins */
    Invite: {
        Endpoint: '/user-admins/invitation',
        Method: HttpMethod.Post
    },
    SignUp: {
        Endpoint: '/user-admins/sign-up',
        Method: HttpMethod.Post
    },
    SignIn: {
        Endpoint: '/user-admins/sign-in',
        Method: HttpMethod.Post
    },
    ReInvite: {
        Endpoint: function (userAdminId) { return "/user-admins/" + userAdminId + "/reinvitation"; },
        Method: HttpMethod.Post
    },
    DisableAdmin: {
        Endpoint: function (userAdminId) { return "/user-admins/" + userAdminId + "/disable"; },
        Method: HttpMethod.Put
    },
    EnableAdmin: {
        Endpoint: function (userAdminId) { return "/user-admins/" + userAdminId + "/enable"; },
        Method: HttpMethod.Put
    },
    ForgotPassword: {
        Endpoint: '/user-admins/forgot-password',
        Method: HttpMethod.Post
    },
    ResetPassword: {
        Endpoint: '/user-admins/reset-password',
        Method: HttpMethod.Post
    },
    GetUserAdmins: {
        Endpoint: '/user-admins',
        Method: HttpMethod.Get
    },
    UpdateUserAdminInfo: {
        Endpoint: function (userAdminId) { return "/user-admins/" + userAdminId; },
        Method: HttpMethod.Patch
    },
    UpdateUserAdminRole: {
        Endpoint: function (userAdminId) { return "/user-admins/" + userAdminId + "/role"; },
        Method: HttpMethod.Patch
    },
    UpdateUserAdminOnLeave: {
        Endpoint: '/user-admins/onLeave',
        Method: HttpMethod.Patch
    },
    GetUserAdminSelfInfo: {
        Endpoint: "/user-admins/" + APISignature.ME,
        Method: HttpMethod.Get
    },
    UpdateUserAdminSelfInfo: {
        Endpoint: "/user-admins/" + APISignature.ME,
        Method: HttpMethod.Patch
    },
    /* User Management */
    AssignManagedUsers: {
        Endpoint: '/user-managements',
        Method: HttpMethod.Post
    },
    MoveAllManagedUsers: {
        Endpoint: '/user-managements',
        Method: HttpMethod.Put
    },
    MoveBackAllManagedUsers: {
        Endpoint: function (userAdminId) { return "/user-managements/" + userAdminId + "/movement"; },
        Method: HttpMethod.Put
    },
    GetManagedUsers: {
        Endpoint: '/user-managements',
        Method: HttpMethod.Get
    },
    GetManagedUsersCount: {
        Endpoint: '/user-managements/count',
        Method: HttpMethod.Get
    },
    MoveSelectedUsers: {
        Endpoint: '/user-managements/users/move-selected',
        Method: HttpMethod.Put
    },
    /* Users */
    RegisterUser: {
        Endpoint: '/users/registration',
        Method: HttpMethod.Post
    },
    DisableUser: {
        Endpoint: function (userId) { return "/users/" + userId + "/disabled"; },
        Method: HttpMethod.Put
    },
    EnableUser: {
        Endpoint: function (userId) { return "/users/" + userId + "/disabled"; },
        Method: HttpMethod.Delete
    },
    UpdateUser: {
        Endpoint: function (userId) { return "/users/" + userId; },
        Method: HttpMethod.Put
    },
    GetUnmanagedUsers: {
        Endpoint: '/users/unmanaged',
        Method: HttpMethod.Get
    },
    GetUnmanagedUsersCount: {
        Endpoint: '/users/unmanaged-count',
        Method: HttpMethod.Get
    },
    /* Departments */
    CreateDepartment: {
        Endpoint: '/departments',
        Method: HttpMethod.Post
    },
    GetDepartments: {
        Endpoint: '/departments',
        Method: HttpMethod.Get
    },
    UpdateDepartments: {
        Endpoint: function (departmentId) { return "/departments/" + departmentId; },
        Method: HttpMethod.Put
    },
    /* Divisions */
    CreateDivisions: {
        Endpoint: function (departmentId) { return "/departments/" + departmentId + "/divisions"; },
        Method: HttpMethod.Post
    },
    GetDivisions: {
        Endpoint: function (departmentId) { return "/departments/" + departmentId + "/divisions"; },
        Method: HttpMethod.Get
    },
    UpdateDivisions: {
        Endpoint: function (_a) {
            var departmentId = _a.departmentId, divisionId = _a.divisionId;
            return "/departments/" + departmentId + "/divisions/" + divisionId;
        },
        Method: HttpMethod.Put
    },
    /* Keywords */
    CreateKeyword: {
        Endpoint: '/keywords',
        Method: HttpMethod.Post
    },
    CreateKeywordBulk: {
        Endpoint: '/keywords/bulk',
        Method: HttpMethod.Post
    },
    DeleteKeyword: {
        Endpoint: function (id) { return "/keywords/" + id; },
        Method: HttpMethod.Delete
    },
    UpdateKeyword: {
        Endpoint: function (id) { return "/keywords/" + id; },
        Method: HttpMethod.Patch
    },
    GetKeywordsBatch: {
        Endpoint: '/keywords',
        Method: HttpMethod.Get
    },
    GetMessagesBeforeSequence: {
        Endpoint: function (conversationId) { return "/conversation/" + conversationId + "/before-sequence"; },
        Method: HttpMethod.Get
    },
    GetMessagesAfterSequence: {
        Endpoint: function (conversationId) { return "/conversation/" + conversationId + "/after-sequence"; },
        Method: HttpMethod.Get
    },
    GetMessageContext: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/context"; },
        Method: HttpMethod.Get
    },
    GetAuditMessagesBatch: {
        Endpoint: '/messages/audit',
        Method: HttpMethod.Get
    },
    DownloadAuditMessages: {
        Endpoint: '/messages/audit-download',
        Method: HttpMethod.Get
    },
    CreateUpdateUserAdminSearch: {
        Endpoint: '/user-admin-searches',
        Method: HttpMethod.Put
    },
    GetUserAdminSearch: {
        Endpoint: '/user-admin-searches',
        Method: HttpMethod.Get
    },
    GetUserBasicInfoBatch: {
        Endpoint: '/users',
        Method: HttpMethod.Get
    },
    // Monitoring or Flagged Messages
    GetFlaggedMessagesBatch: {
        Endpoint: '/flagged-messages',
        Method: HttpMethod.Get
    },
    GetFlaggedMessagesCount: {
        Endpoint: '/flagged-messages/count',
        Method: HttpMethod.Get
    },
    ResolveFlaggedMessages: {
        Endpoint: function (id) { return "/flagged-messages/" + id + "/resolve"; },
        Method: HttpMethod.Post
    },
    GetFlaggedMessageDetails: {
        Endpoint: function (id) { return "/flagged-messages/" + id; },
        Method: HttpMethod.Get
    },
    GetFlaggedMessageUserReports: {
        Endpoint: function (id) { return "/flagged-messages/" + id + "/user-reports"; },
        Method: HttpMethod.Get
    },
    GetFlaggedMessageKeywordScans: {
        Endpoint: function (id) { return "/flagged-messages/" + id + "/keyword-scans"; },
        Method: HttpMethod.Get
    },
    GetFlaggedMessageResolutions: {
        Endpoint: function (id) { return "/flagged-messages/" + id + "/resolutions"; },
        Method: HttpMethod.Get
    },
    GetFlaggedMessageResolutionUserReports: {
        Endpoint: function (id) { return "/flagged-resolutions/" + id + "/user-reports"; },
        Method: HttpMethod.Get
    },
    GetFlaggedMessageResolutionKeywordScans: {
        Endpoint: function (id) { return "/flagged-resolutions/" + id + "/keyword-scans"; },
        Method: HttpMethod.Get
    },
    /* Groups */
    CreateGroupConversation: {
        Endpoint: '/conversations/user-admin-managed',
        Method: HttpMethod.Post
    },
    UpdateGroupConversationOwner: {
        Endpoint: function (conversationId, userAdminId) {
            return "conversations/" + conversationId + "/owner/" + userAdminId;
        },
        Method: HttpMethod.Patch
    },
    GetUserAdminGroups: {
        Endpoint: 'conversations/user-admin-groups',
        Method: HttpMethod.Get
    },
    /* Conversation Space Group Divisions */
    GetSpaceGroupDivisions: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId + "/space-group-divisions"; },
        Method: HttpMethod.Get
    },
    CreateConversationSpaceGroupDivision: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId + "/space-group-divisions"; },
        Method: HttpMethod.Post
    },
    DeleteConversationSpaceGroupDivision: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId + "/space-group-divisions"; },
        Method: HttpMethod.Delete
    },
    /* Dashboard */
    GetTotalUsersCount: {
        Endpoint: 'users/total-count',
        Method: HttpMethod.Get
    },
    GetMyUsersCount: {
        Endpoint: 'user-managements/my-users-count',
        Method: HttpMethod.Get
    },
    GetResolvedFlaggedMessagesCount: {
        Endpoint: 'flagged-messages/resolved/total-count',
        Method: HttpMethod.Get
    },
    GetSentMessagesCountDaily: {
        Endpoint: 'sent-message-counts/daily',
        Method: HttpMethod.Get
    },
    GetSentMessagesCountTotal: {
        Endpoint: 'sent-message-counts/total-count',
        Method: HttpMethod.Get
    },
    GetUnresolvedFlaggedMessagesCount: {
        Endpoint: 'flagged-messages/unresolved/total-count',
        Method: HttpMethod.Get
    },
    GetUnresolvedFlaggedMessageCountsDaily: {
        Endpoint: 'flagged-messages/unresolved/daily',
        Method: HttpMethod.Get
    },
    GetEligibleRolesToInviteAdmin: {
        Endpoint: "/user-admins/invitable-roles",
        Method: HttpMethod.Get
    },
    /* User Onboarding Job */
    ProcessUserOnboardingJobUpload: {
        Endpoint: 'user-onboarding-jobs/files',
        Method: HttpMethod.Post
    },
    GetUserOnboardingJobBatch: {
        Endpoint: 'user-onboarding-jobs',
        Method: HttpMethod.Get
    },
    DownloadSampleFile: {
        Endpoint: 'user-onboarding-jobs/download',
        Method: HttpMethod.Get
    },
    GetAllErroredUserOnboardingJobRecords: {
        Endpoint: function (id) { return "user-onboarding-jobs/" + id + "/user-onboarding-job-records/errored"; },
        Method: HttpMethod.Get
    }
};
var ChatCommonRoutes = {
    UploadFile: {
        Endpoint: '/files',
        Method: HttpMethod.Post
    },
    GetReactedUsers: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/reacted-users"; },
        Method: HttpMethod.Get
    },
    GetReachableUsers: {
        Endpoint: '/users/conversable',
        Method: HttpMethod.Get
    },
    GetConversationUsers: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/users"; },
        Method: HttpMethod.Get
    },
    AddUsersToConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/users"; },
        Method: HttpMethod.Post
    },
    RemoveUsersFromConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/delete-users"; },
        Method: HttpMethod.Post
    },
    UpdateGroupImage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-image"; },
        Method: HttpMethod.Put
    },
    RemoveGroupImage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-image"; },
        Method: HttpMethod.Delete
    },
    GetGroupDefaultImageSet: {
        Endpoint: '/default-group-images',
        Method: HttpMethod.Get
    },
    UpdateGroupInfo: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-info"; },
        Method: HttpMethod.Put
    },
    MakeConversationOwner: {
        Endpoint: function (conversationId, userId) {
            return "/conversations/" + conversationId + "/owners/" + userId;
        },
        Method: HttpMethod.Put
    },
    RemoveConversationOwner: {
        Endpoint: function (conversationId, userId) {
            return "/conversations/" + conversationId + "/owners/" + userId;
        },
        Method: HttpMethod.Delete
    },
    GetConversationDetails: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId; },
        Method: HttpMethod.Get
    },
    DeleteGroupConversation: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId; },
        Method: HttpMethod.Delete
    }
};

function checkApiKey(config) {
    // SAFE TO REMOVE - Not user input
    // eslint-disable-next-line security/detect-object-injection
    if (!config.headers[ApiKeyName]) {
        throw new SayHey.Error('Missing api key in request header', SayHey.ErrorType.InternalErrorType.AuthMissing);
    }
    return config;
}
function checkaccessToken(config) {
    if (!config.headers.Authorization) {
        throw new SayHey.Error('Missing auth token in request header', SayHey.ErrorType.InternalErrorType.AuthMissing);
    }
    return config;
}
function requestFailed(error) {
    var response = error.response;
    var err = new SayHey.Error(response === null || response === void 0 ? void 0 : response.data.message, response === null || response === void 0 ? void 0 : response.data.type);
    err.setStatusCode(response === null || response === void 0 ? void 0 : response.status);
    err.setErrorActions((response === null || response === void 0 ? void 0 : response.data.actions) || {});
    throw err;
}
var httpClient = {
    createWithoutAuth: function (config) {
        var _a;
        var baseURL = config.baseURL, apiKey = config.apiKey;
        var instance = axios.create({
            baseURL: baseURL,
            headers: (_a = {},
                _a[ApiKeyName] = apiKey,
                _a)
        });
        instance.interceptors.request.use(checkApiKey);
        instance.interceptors.response.use(undefined, requestFailed);
        return instance;
    },
    createWithAuth: function (config) {
        var _a;
        var baseURL = config.baseURL, accessToken = config.accessToken, apiKey = config.apiKey;
        var instance = axios.create({
            baseURL: baseURL,
            headers: (_a = {},
                _a[ApiKeyName] = apiKey,
                _a.Authorization = "Bearer " + accessToken,
                _a)
        });
        instance.interceptors.request.use(checkApiKey);
        instance.interceptors.request.use(checkaccessToken);
        instance.interceptors.response.use(undefined, requestFailed);
        return instance;
    }
};

var emojiRegex = /\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]/;
var isSingleEmoji = function (text) { return length(text) === 1 && text.match(emojiRegex); };
// SAFE TO REMOVE - not user input
// eslint-disable-next-line security/detect-unsafe-regex, no-useless-escape
var textHighlightRegex = /(\{\{([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})\}\})|((([hH][tT]{2}[pP][sS]?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b)([-a-zA-Z0-9@:%_\+.~#?&\/=]*[-a-zA-Z0-9@:%_\+~#?&\/=])*)|(((^|\s{1}))((\+?1\s?)?((\([2-9]{1}\d{2}\))|([2-9]{1}\d{2}))[\s\-]?\d{3}[\s\-]?\d{4})(\s|$))|(\S+@\S+\.\S+)/gi;
// SAFE TO REMOVE - not user input
// eslint-disable-next-line security/detect-unsafe-regex, no-useless-escape
var urlRegex = /(([hH][tT]{2}[pP][sS]?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b)([-a-zA-Z0-9@:%_\+.~#?&\/=]*[-a-zA-Z0-9@:%_\+~#?&\/=])*/;
// New phone regex (?:^|\s{1})((\+?1\s?)?((?:\([2-9]{1}\d{2}\))|(?:[2-9]{1}\d{2}))[\s\-]?(\d{3})[\s\-]?(\d{4}))(?:\s|$)
// SAFE TO REMOVE - not user input
// eslint-disable-next-line security/detect-unsafe-regex, no-useless-escape
var phoneRegex = /((^|\s{1}))((\+?1\s?)?((\([2-9]{1}\d{2}\))|([2-9]{1}\d{2}))[\s\-]?\d{3}[\s\-]?\d{4})(\s|$)/;
// SAFE TO REMOVE - not user input
// eslint-disable-next-line security/detect-unsafe-regex, no-useless-escape
var userIdRegex = /\{\{([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})\}\}/i;
var isUrl = function (url) { return !!urlRegex.test(url); };
var isPhone = function (phone) { return !!phoneRegex.test(phone); };
var isUser = function (userId) { return !!userIdRegex.test(userId); };
var parseText = function (text) {
    if (!text) {
        return null;
    }
    var result = [];
    var trimedText = text.trim();
    var url = null;
    var splitTrimedTextArray = trimedText.split('\n');
    splitTrimedTextArray.forEach(function (line, index) {
        result.push([]);
        if (line.trim()) {
            var lastIndex = 0;
            var match = void 0;
            // SAFE TO REMOVE - not user input
            // eslint-disable-next-line no-cond-assign
            while ((match = textHighlightRegex.exec(line))) {
                if (isUrl(match[0])) {
                    if (match.index > lastIndex) {
                        var currentText = line.substring(lastIndex, match.index);
                        if (currentText.length > 1) {
                            result[+index].push({ type: HighLightType.Text, value: currentText });
                        }
                    }
                    if (!url) {
                        url = match[0].replace(match[4], match[4].toLowerCase());
                    }
                    result[+index].push({ type: HighLightType.Url, value: match[0] });
                    lastIndex = textHighlightRegex.lastIndex;
                }
                else if (isUser(match[0])) {
                    if (match.index > lastIndex) {
                        var currentText = line.substring(lastIndex, match.index);
                        if (currentText.length > 1) {
                            result[+index].push({ type: HighLightType.Text, value: currentText });
                        }
                    }
                    result[+index].push({ type: HighLightType.UserId, value: match[2] });
                    lastIndex = textHighlightRegex.lastIndex;
                }
                else if (isPhone(match[0])) {
                    var startIndex = match[9] ? match.index + 1 : match.index;
                    if (startIndex > lastIndex) {
                        var currentText = line.substring(lastIndex, startIndex);
                        if (currentText.length >= 1) {
                            result[+index].push({ type: HighLightType.Text, value: currentText });
                        }
                    }
                    result[+index].push({ type: HighLightType.Phone, value: match[10] });
                    lastIndex = match[15] ? textHighlightRegex.lastIndex - 1 : textHighlightRegex.lastIndex;
                }
                else {
                    if (match.index > lastIndex) {
                        var currentText = line.substring(lastIndex, match.index);
                        if (currentText.length > 1) {
                            result[+index].push({ type: HighLightType.Text, value: currentText });
                        }
                    }
                    result[+index].push({ type: HighLightType.Email, value: match[0] });
                    lastIndex = textHighlightRegex.lastIndex;
                }
            }
            if (lastIndex <= line.length - 1) {
                result[+index].push({ type: HighLightType.Text, value: line.substring(lastIndex) });
            }
        }
        if (index !== splitTrimedTextArray.length - 1 || !line.trim()) {
            result[+index].push({ type: HighLightType.Text, value: '\n' });
        }
    });
    return {
        parsedText: result,
        previewUrl: url
    };
};
var mimeTypes = {
    '3gp': 'video/3gpp',
    a: 'application/octet-stream',
    ai: 'application/postscript',
    aif: 'audio/x-aiff',
    aiff: 'audio/x-aiff',
    asc: 'application/pgp-signature',
    asf: 'video/x-ms-asf',
    asm: 'text/x-asm',
    asx: 'video/x-ms-asf',
    atom: 'application/atom+xml',
    au: 'audio/basic',
    avi: 'video/x-msvideo',
    bat: 'application/x-msdownload',
    bin: 'application/octet-stream',
    bmp: 'image/bmp',
    bz2: 'application/x-bzip2',
    c: 'text/x-csrc',
    cab: 'application/vnd.ms-cab-compressed',
    can: 'application/candor',
    cc: 'text/x-c++src',
    chm: 'application/vnd.ms-htmlhelp',
    class: 'application/octet-stream',
    com: 'application/x-msdownload',
    conf: 'text/plain',
    cpp: 'text/x-c',
    crt: 'application/x-x509-ca-cert',
    css: 'text/css',
    csv: 'text/csv',
    cxx: 'text/x-c',
    deb: 'application/x-debian-package',
    der: 'application/x-x509-ca-cert',
    diff: 'text/x-diff',
    djv: 'image/vnd.djvu',
    djvu: 'image/vnd.djvu',
    dll: 'application/x-msdownload',
    dmg: 'application/octet-stream',
    doc: 'application/msword',
    dot: 'application/msword',
    dtd: 'application/xml-dtd',
    dvi: 'application/x-dvi',
    ear: 'application/java-archive',
    eml: 'message/rfc822',
    eps: 'application/postscript',
    exe: 'application/x-msdownload',
    f: 'text/x-fortran',
    f77: 'text/x-fortran',
    f90: 'text/x-fortran',
    flv: 'video/x-flv',
    for: 'text/x-fortran',
    gem: 'application/octet-stream',
    gemspec: 'text/x-script.ruby',
    gif: 'image/gif',
    gyp: 'text/x-script.python',
    gypi: 'text/x-script.python',
    gz: 'application/x-gzip',
    h: 'text/x-chdr',
    hh: 'text/x-c++hdr',
    heic: 'image/heic',
    heif: 'image/heif',
    hevc: 'video/mp4',
    htm: 'text/html',
    html: 'text/html',
    ico: 'image/vnd.microsoft.icon',
    ics: 'text/calendar',
    ifb: 'text/calendar',
    iso: 'application/octet-stream',
    jpeg: 'image/jpeg',
    jpg: 'image/jpeg',
    js: 'application/javascript',
    json: 'application/json',
    less: 'text/css',
    log: 'text/plain',
    lua: 'text/x-script.lua',
    luac: 'application/x-bytecode.lua',
    m3u: 'audio/x-mpegurl',
    m4v: 'video/mp4',
    man: 'text/troff',
    manifest: 'text/cache-manifest',
    markdown: 'text/x-markdown',
    mathml: 'application/mathml+xml',
    mbox: 'application/mbox',
    mdoc: 'text/troff',
    md: 'text/x-markdown',
    me: 'text/troff',
    mid: 'audio/midi',
    midi: 'audio/midi',
    mime: 'message/rfc822',
    mml: 'application/mathml+xml',
    mng: 'video/x-mng',
    mov: 'video/quicktime',
    mp3: 'audio/mpeg',
    mp4: 'video/mp4',
    mp4v: 'video/mp4',
    mpeg: 'video/mpeg',
    mpg: 'video/mpeg',
    ms: 'text/troff',
    msi: 'application/x-msdownload',
    odp: 'application/vnd.oasis.opendocument.presentation',
    ods: 'application/vnd.oasis.opendocument.spreadsheet',
    odt: 'application/vnd.oasis.opendocument.text',
    ogg: 'application/ogg',
    p: 'text/x-pascal',
    pas: 'text/x-pascal',
    pbm: 'image/x-portable-bitmap',
    pdf: 'application/pdf',
    pem: 'application/x-x509-ca-cert',
    pgm: 'image/x-portable-graymap',
    pgp: 'application/pgp-encrypted',
    pkg: 'application/octet-stream',
    png: 'image/png',
    pnm: 'image/x-portable-anymap',
    ppm: 'image/x-portable-pixmap',
    pps: 'application/vnd.ms-powerpoint',
    ppt: 'application/vnd.ms-powerpoint',
    ps: 'application/postscript',
    psd: 'image/vnd.adobe.photoshop',
    qt: 'video/quicktime',
    ra: 'audio/x-pn-realaudio',
    ram: 'audio/x-pn-realaudio',
    rar: 'application/x-rar-compressed',
    rdf: 'application/rdf+xml',
    roff: 'text/troff',
    rss: 'application/rss+xml',
    rtf: 'application/rtf',
    s: 'text/x-asm',
    sgm: 'text/sgml',
    sgml: 'text/sgml',
    sh: 'application/x-sh',
    sig: 'application/pgp-signature',
    snd: 'audio/basic',
    so: 'application/octet-stream',
    svg: 'image/svg+xml',
    svgz: 'image/svg+xml',
    swf: 'application/x-shockwave-flash',
    t: 'text/troff',
    tar: 'application/x-tar',
    tbz: 'application/x-bzip-compressed-tar',
    tci: 'application/x-topcloud',
    tcl: 'application/x-tcl',
    tex: 'application/x-tex',
    texi: 'application/x-texinfo',
    texinfo: 'application/x-texinfo',
    text: 'text/plain',
    tif: 'image/tiff',
    tiff: 'image/tiff',
    torrent: 'application/x-bittorrent',
    tr: 'text/troff',
    ttf: 'application/x-font-ttf',
    txt: 'text/plain',
    vcf: 'text/x-vcard',
    vcs: 'text/x-vcalendar',
    war: 'application/java-archive',
    wav: 'audio/x-wav',
    webm: 'video/webm',
    wma: 'audio/x-ms-wma',
    wmv: 'video/x-ms-wmv',
    wmx: 'video/x-ms-wmx',
    wrl: 'model/vrml',
    wsdl: 'application/wsdl+xml',
    xbm: 'image/x-xbitmap',
    xhtml: 'application/xhtml+xml',
    xls: 'application/vnd.ms-excel',
    xml: 'application/xml',
    zip: 'application/zip'
};

var ConversationMapper = /** @class */ (function () {
    function ConversationMapper() {
    }
    ConversationMapper.toDomainEntity = function (conversation) {
        var id = conversation.id, lastReadMessageId = conversation.lastReadMessageId, lastMessage = conversation.lastMessage, muteUntil = conversation.muteUntil, pinnedAt = conversation.pinnedAt, badge = conversation.badge, type = conversation.type, createdAt = conversation.createdAt, updatedAt = conversation.updatedAt, isOwner = conversation.isOwner, startAfter = conversation.startAfter, visible = conversation.visible, endBefore = conversation.endBefore, deletedFromConversationAt = conversation.deletedFromConversationAt, serverCreated = conversation.serverCreated, serverManaged = conversation.serverManaged, isQuickGroup = conversation.isQuickGroup, isSpace = conversation.isSpace, lastUnreadNudgedAt = conversation.lastUnreadNudgedAt, creatorId = conversation.creatorId;
        var common = {
            id: id,
            lastReadMessageId: lastReadMessageId,
            lastMessage: lastMessage ? MessageMapper.toDomainEntity(lastMessage) : null,
            muteUntil: muteUntil ? new Date(muteUntil) : null,
            pinnedAt: pinnedAt ? new Date(pinnedAt) : null,
            createdAt: new Date(createdAt),
            updatedAt: new Date(updatedAt),
            startAfter: startAfter ? new Date(startAfter) : null,
            endBefore: endBefore ? new Date(endBefore) : null,
            deletedFromConversationAt: deletedFromConversationAt
                ? new Date(deletedFromConversationAt)
                : null,
            badge: badge || 0,
            type: type,
            isOwner: isOwner,
            visible: visible,
            serverCreated: serverCreated,
            serverManaged: serverManaged,
            isQuickGroup: isQuickGroup,
            isSpace: isSpace,
            lastUnreadNudgedAt: lastUnreadNudgedAt,
            creatorId: creatorId
        };
        if (conversation.type === ConversationType.Group) {
            var group = conversation.group;
            return __assign(__assign({}, common), { type: ConversationType.Group, group: __assign(__assign({}, group), { picture: group.picture ? MediaMapper.toDomainEntity(group.picture) : null }) });
        }
        var user = conversation.user;
        return __assign(__assign({}, common), { type: ConversationType.Individual, user: __assign(__assign({}, user), { picture: user.picture ? MediaMapper.toDomainEntity(user.picture) : null }) });
    };
    ConversationMapper.toDomainEntities = function (conversations) {
        return conversations.map(function (conversation) { return ConversationMapper.toDomainEntity(conversation); });
    };
    return ConversationMapper;
}());
var SentMessageMapper = /** @class */ (function () {
    function SentMessageMapper() {
    }
    SentMessageMapper.toDomainEntity = function (message) {
        var id = message.id, conversationId = message.conversationId, sender = message.sender, type = message.type, text = message.text, sequence = message.sequence, file = message.file, createdAt = message.createdAt, reactions = message.reactions, refUrl = message.refUrl, deletedAt = message.deletedAt;
        var parsedResult = parseText(text);
        return {
            id: id,
            conversationId: conversationId,
            sender: sender,
            type: type,
            text: text,
            deletedAt: deletedAt ? new Date(deletedAt) : null,
            sequence: sequence,
            createdAt: new Date(createdAt),
            reactions: reactions,
            sent: true,
            parsedText: parsedResult ? parsedResult.parsedText : null,
            file: file ? MediaMapper.toDomainEntity(file) : null,
            refUrl: refUrl,
            systemMessage: null,
            previewUrl: parsedResult === null || parsedResult === void 0 ? void 0 : parsedResult.previewUrl
        };
    };
    SentMessageMapper.toDomainEntities = function (messages) {
        return messages.map(function (value) { return SentMessageMapper.toDomainEntity(value); });
    };
    return SentMessageMapper;
}());
var MessageMapper = /** @class */ (function () {
    function MessageMapper() {
    }
    MessageMapper.toDomainEntity = function (message) {
        return __assign(__assign({}, SentMessageMapper.toDomainEntity(message)), { systemMessage: message.systemMessage
                ? SystemMessageMapper.toDomainEntity(message.systemMessage)
                : null });
    };
    MessageMapper.toDomainEntities = function (messages) {
        return messages.map(function (value) { return MessageMapper.toDomainEntity(value); });
    };
    MessageMapper.toPeekDomainEntities = function (messages) {
        return messages.map(function (value) {
            var flaggedInfo = value.flaggedInfo;
            return __assign(__assign({}, MessageMapper.toDomainEntity(value)), { flaggedInfo: flaggedInfo });
        });
    };
    return MessageMapper;
}());
var UserMapper = /** @class */ (function () {
    function UserMapper() {
    }
    UserMapper.toDomainEntity = function (user) {
        var firstName = user.firstName, lastName = user.lastName, picture = user.picture, deletedAt = user.deletedAt;
        if (user.userScope === UserScope.Internal) {
            return __assign(__assign({}, user), { deletedAt: deletedAt ? new Date(deletedAt) : null, picture: picture ? MediaMapper.toDomainEntity(picture) : null, fullName: firstName + " " + lastName, email: user.email, username: user.email });
        }
        return __assign(__assign({}, user), { deletedAt: deletedAt ? new Date(deletedAt) : null, picture: picture ? MediaMapper.toDomainEntity(picture) : null, fullName: firstName + " " + lastName, phone: user.phone, username: user.phone.phoneNumber });
    };
    UserMapper.toUserForUserAdminDomainEntity = function (user) {
        var firstName = user.firstName, lastName = user.lastName, picture = user.picture, deletedAt = user.deletedAt;
        if (user.userScope === UserScope.Internal) {
            return __assign(__assign({}, user), { title: user.title, employeeId: user.employeeId, departmentId: user.departmentId, divisionId: user.divisionId, division: user.division, department: user.department, email: user.email, deletedAt: deletedAt ? new Date(deletedAt) : null, picture: picture ? MediaMapper.toDomainEntity(picture) : null, fullName: firstName + " " + lastName, username: user.email });
        }
        return __assign(__assign({}, user), { userScope: user.userScope, fullName: firstName + " " + lastName, username: user.phone.phoneNumber, phone: user.phone, deletedAt: deletedAt ? new Date(deletedAt) : null, picture: picture ? MediaMapper.toDomainEntity(picture) : null });
    };
    UserMapper.toConversableUserDomainEntity = function (user) {
        var firstName = user.firstName, lastName = user.lastName, picture = user.picture, deletedAt = user.deletedAt;
        if (user.userScope === UserScope.Internal) {
            return __assign(__assign({}, user), { username: user.email, fullName: firstName + " " + lastName, picture: picture ? MediaMapper.toDomainEntity(picture) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
        }
        return __assign(__assign({}, user), { username: user.phone.phoneNumber, fullName: firstName + " " + lastName, phone: user.phone, picture: picture ? MediaMapper.toDomainEntity(picture) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
    };
    UserMapper.toConversableUserDomainForUserAdminEntity = function (user) {
        var firstName = user.firstName, lastName = user.lastName, picture = user.picture, deletedAt = user.deletedAt;
        if (user.userScope === UserScope.Internal) {
            return __assign(__assign({}, user), { username: user.email, fullName: firstName + " " + lastName, picture: picture ? MediaMapper.toDomainEntity(picture) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
        }
        return __assign(__assign({}, user), { username: user.phone.phoneNumber, fullName: firstName + " " + lastName, picture: picture ? MediaMapper.toDomainEntity(picture) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
    };
    UserMapper.toDomainEntities = function (users) {
        return users.map(UserMapper.toDomainEntity);
    };
    UserMapper.toUserForUserDomainEntities = function (users) {
        return users.map(UserMapper.toUserForUserAdminDomainEntity);
    };
    UserMapper.toConversableUserDomainEntities = function (users) {
        return users.map(UserMapper.toConversableUserDomainEntity);
    };
    UserMapper.toConversableUserDomainForUserAdminEntities = function (users) {
        return users.map(UserMapper.toConversableUserDomainForUserAdminEntity);
    };
    UserMapper.toSelfDomainEntity = function (user) {
        var firstName = user.firstName, lastName = user.lastName, picture = user.picture, muteAllUntil = user.muteAllUntil, deletedAt = user.deletedAt;
        if (user.userScope === UserScope.Internal) {
            return __assign(__assign({}, user), { username: user.email, fullName: firstName + " " + lastName, picture: picture ? MediaMapper.toDomainEntity(picture) : null, muteAllUntil: muteAllUntil ? new Date(muteAllUntil) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
        }
        return __assign(__assign({}, user), { username: user.phone.phoneNumber, fullName: firstName + " " + lastName, picture: picture ? MediaMapper.toDomainEntity(picture) : null, muteAllUntil: muteAllUntil ? new Date(muteAllUntil) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
    };
    return UserMapper;
}());
var MediaMapper = /** @class */ (function () {
    function MediaMapper() {
    }
    MediaMapper.toDomainEntity = function (media) {
        var urlPath = media.urlPath, fileSize = media.fileSize, fileType = media.fileType, extension = media.extension, filename = media.filename, mimeType = media.mimeType, encoding = media.encoding, imageInfo = media.imageInfo, videoInfo = media.videoInfo;
        return {
            urlPath: urlPath,
            fileSize: fileSize,
            fileType: fileType,
            extension: extension,
            filename: filename,
            mimeType: mimeType,
            encoding: encoding,
            imageInfo: imageInfo,
            videoInfo: !videoInfo
                ? null
                : {
                    duration: videoInfo.duration,
                    thumbnail: videoInfo.thumbnail ? MediaMapper.toDomainEntity(videoInfo.thumbnail) : null
                }
        };
    };
    return MediaMapper;
}());
var SystemMessageMapper = /** @class */ (function () {
    function SystemMessageMapper() {
    }
    SystemMessageMapper.toDomainEntity = function (systemMessage) {
        var systemMessageType = systemMessage.systemMessageType, id = systemMessage.id, refValue = systemMessage.refValue, actorCount = systemMessage.actorCount, initiator = systemMessage.initiator, actors = systemMessage.actors, text = systemMessage.text;
        return {
            systemMessageType: systemMessageType,
            id: id,
            refValue: refValue,
            actorCount: actorCount,
            initiator: initiator,
            actors: actors,
            text: text
        };
    };
    return SystemMessageMapper;
}());
var ReactionMapper = /** @class */ (function () {
    function ReactionMapper() {
    }
    ReactionMapper.toDomianEntity = function (reactions) {
        var conversationReactions = new Map();
        reactions.forEach(function (item) {
            var messageId = item.messageId;
            var reactionTypesArray = Array.isArray(item.type) ? item.type : [item.type];
            var conversationReactionsForMsg = conversationReactions.get(messageId);
            if (!conversationReactionsForMsg) {
                conversationReactions.set(messageId, []);
            }
            var currentReactionTypes = conversationReactionsForMsg || [];
            var reactionSet = new Set(__spreadArrays(currentReactionTypes, reactionTypesArray));
            conversationReactions.set(messageId, Array.from(reactionSet));
        });
        var conversationReactionObj = {};
        var keys = Array.from(conversationReactions.keys());
        for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
            var key = keys_1[_i];
            // SAFE To remove - not user input
            // eslint-disable-next-line security/detect-object-injection
            conversationReactionObj[key] = conversationReactions.get(key) || undefined;
        }
        return conversationReactionObj;
    };
    return ReactionMapper;
}());
var AuditMessageMapper = /** @class */ (function () {
    function AuditMessageMapper() {
    }
    AuditMessageMapper.toAuditMessageDomainEntityBatchDTO = function (auditMessageBatchDTO) {
        var auditMessageDomainEntityDTO = auditMessageBatchDTO.results.map(function (auditMsg) {
            var messageDomainEntity = SentMessageMapper.toDomainEntity(auditMsg);
            return __assign(__assign({}, messageDomainEntity), { flagged: auditMsg.flagged });
        });
        return __assign(__assign({}, auditMessageBatchDTO), { results: auditMessageDomainEntityDTO });
    };
    AuditMessageMapper.toAuditMessageDomainEntityDTO = function (auditMessageDTO) {
        var messageDomainEntity = SentMessageMapper.toDomainEntity(auditMessageDTO);
        return __assign(__assign({}, messageDomainEntity), { flagged: auditMessageDTO.flagged, resolved: auditMessageDTO.resolved ? auditMessageDTO.resolved : undefined });
    };
    return AuditMessageMapper;
}());

var ChatAdminKit = /** @class */ (function () {
    function ChatAdminKit() {
        var _this = this;
        this.accessToken = '';
        this.refreshToken = '';
        this._apiVersion = API_VERSION;
        this.appId = '';
        this.apiKey = '';
        this.serverUrl = '';
        this._initialized = false;
        this._subscribed = false;
        this._isUserAuthenticated = false;
        this.exchangeTokenPromise = null;
        this.loginId = 0;
        this.isUserAuthenticated = function () { return _this._isUserAuthenticated; };
        /**
         * Listeners
         */
        this.addListeners = function (listeners) {
            if (_this._initialized) {
                var onAuthDetailChange = listeners.onAuthDetailChange, onSessionExpired = listeners.onSessionExpired;
                _this._subscribed = true;
                _this.authDetailChangeListener = onAuthDetailChange;
                _this.sessionExpiredListener = onSessionExpired;
                return ok(true);
            }
            return err(new SayHey.Error('ChatKit has not been initialized!', SayHey.ErrorType.InternalErrorType.Uninitialized));
        };
        /**
         *
         * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
         */
        this.guard = function (cb) { return __awaiter(_this, void 0, void 0, function () {
            var res, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._initialized) {
                            return [2 /*return*/, err(new SayHey.Error('ChatKit has not been initialized!', SayHey.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        if (!this._subscribed) {
                            return [2 /*return*/, err(new SayHey.Error('ChatKit events have not been subscribed to!', SayHey.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, cb()];
                    case 2:
                        res = _a.sent();
                        if (res instanceof SayHey.Error) {
                            return [2 /*return*/, err(res)];
                        }
                        return [2 /*return*/, ok(res)];
                    case 3:
                        e_1 = _a.sent();
                        if (e_1 instanceof SayHey.Error) {
                            return [2 /*return*/, err(e_1)];
                        }
                        return [2 /*return*/, err(new SayHey.Error('Unknown error', SayHey.ErrorType.InternalErrorType.Unknown))];
                    case 4: return [2 /*return*/];
                }
            });
        }); };
        this.exchangeTokenOnce = function () { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.exchangeTokenPromise) {
                            return [2 /*return*/, this.exchangeTokenPromise];
                        }
                        this.exchangeTokenPromise = this.exchangeToken();
                        return [4 /*yield*/, this.exchangeTokenPromise];
                    case 1:
                        response = _a.sent();
                        this.exchangeTokenPromise = null;
                        return [2 /*return*/, response];
                }
            });
        }); };
        /**
         *
         * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
         */
        this.guardWithAuth = function (cb) { return __awaiter(_this, void 0, void 0, function () {
            var currentLoginId, res, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        currentLoginId = this.loginId;
                        return [4 /*yield*/, this.guard(cb)];
                    case 1:
                        res = _a.sent();
                        if (!res.isErr()) return [3 /*break*/, 5];
                        if (!(!this.disableAutoRefreshToken &&
                            res.error.type === SayHey.ErrorType.AuthErrorType.TokenExpiredType)) return [3 /*break*/, 5];
                        if (!(currentLoginId === this.loginId)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.exchangeTokenOnce()];
                    case 2:
                        response = _a.sent();
                        if (response.isOk() && response.value === true) {
                            // --> NOTE: Modified to check response is also true => isUserAuthenticated, else logout
                            return [2 /*return*/, this.guard(cb)];
                        }
                        return [4 /*yield*/, this.signOut()];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4: 
                    // If token has been exchanged, use the new credentials to make the request again
                    return [2 /*return*/, this.guard(cb)];
                    case 5: return [2 /*return*/, res];
                }
            });
        }); };
        /**
         *
         * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
         * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
         */
        this.updateAuthDetails = function (accessToken, refreshToken) { return __awaiter(_this, void 0, void 0, function () {
            var id;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.accessToken = accessToken;
                        this.refreshToken = refreshToken;
                        id = accessToken ? decode(accessToken).id : '';
                        if (accessToken && refreshToken) {
                            this.loginId += 1;
                            this.httpClient = httpClient.createWithAuth({
                                apiKey: this.apiKey,
                                baseURL: this.serverUrl + "/" + this._apiVersion + "/apps/" + this.appId,
                                accessToken: this.accessToken
                            });
                        }
                        return [4 /*yield*/, ((_a = this.authDetailChangeListener) === null || _a === void 0 ? void 0 : _a.call(this, {
                                userId: id,
                                accessToken: accessToken,
                                refreshToken: refreshToken
                            }))];
                    case 1:
                        _b.sent();
                        // NOTE: Should be done at the end?
                        this._isUserAuthenticated = !!(accessToken && refreshToken);
                        return [2 /*return*/];
                }
            });
        }); };
        /**
         *
         * @param urlPath - url path returned from SayHey
         */
        this.getMediaSourceDetails = function (urlPath) {
            var _a;
            return ({
                uri: "" + _this.serverUrl + urlPath,
                method: 'GET',
                headers: (_a = {},
                    _a[ApiKeyName] = _this.apiKey,
                    _a.Authorization = "Bearer " + _this.accessToken,
                    _a.Accept = 'image/*, video/*, audio/*',
                    _a)
            });
        };
        this.getMediaDirectUrl = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var getMediaDirectUrl;
            var _this = this;
            return __generator(this, function (_a) {
                getMediaDirectUrl = function () { return __awaiter(_this, void 0, void 0, function () {
                    var queryString, data;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                queryString = {
                                    noRedirect: true
                                };
                                if (params.size) {
                                    queryString.size = params.size;
                                }
                                return [4 /*yield*/, this.httpClient({
                                        url: "" + this.serverUrl + params.urlPath,
                                        method: 'GET',
                                        params: queryString
                                    })];
                            case 1:
                                data = (_a.sent()).data;
                                return [2 /*return*/, data.url];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getMediaDirectUrl)];
            });
        }); };
        this.uploadFile = function (fileUri, fileName, progress, thumbnailId) { return __awaiter(_this, void 0, void 0, function () {
            var uploadFile;
            var _this = this;
            return __generator(this, function (_a) {
                uploadFile = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, formData, extension, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatCommonRoutes.UploadFile, Endpoint = _a.Endpoint, Method = _a.Method;
                                formData = new FormData();
                                if (fileUri instanceof Blob) {
                                    formData.append('file', fileUri, fileName);
                                    if (thumbnailId) {
                                        formData.append('thumbnailId', thumbnailId);
                                    }
                                }
                                else {
                                    extension = fileName.split('.').pop();
                                    if (extension) {
                                        formData.append('file', {
                                            uri: fileUri,
                                            name: fileName,
                                            type: mimeTypes[extension.toLowerCase()]
                                        });
                                        if (thumbnailId) {
                                            formData.append('thumbnailId', thumbnailId);
                                        }
                                    }
                                    else {
                                        return [2 /*return*/, new SayHey.Error('Invalid file')];
                                    }
                                }
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: formData,
                                        onUploadProgress: progress
                                            ? function (event) {
                                                var loaded = event.loaded, total = event.total;
                                                progress((loaded / total) * 0.95);
                                            }
                                            : undefined
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                progress === null || progress === void 0 ? void 0 : progress(1);
                                return [2 /*return*/, data.id];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(uploadFile)];
            });
        }); };
        this.uploadImage = function (fileUri, fileName) { return __awaiter(_this, void 0, void 0, function () {
            var extension, isSupportedImage;
            var _a;
            return __generator(this, function (_b) {
                extension = (_a = fileName
                    .split('.')
                    .pop()) === null || _a === void 0 ? void 0 : _a.toLowerCase();
                isSupportedImage = mimeTypes[extension || ''].split('/')[0] === 'image';
                if (isSupportedImage) {
                    return [2 /*return*/, this.uploadFile(fileUri, fileName)];
                }
                return [2 /*return*/, err(new SayHey.Error('Please select a valid image!', SayHey.ErrorType.InternalErrorType.BadData))];
            });
        }); };
        /**
         * Token APIs
         */
        /**
         *
         * @param email - user email for SayHey
         * @param password - user password for SayHey
         *
         */
        this.exchangeToken = function () { return __awaiter(_this, void 0, void 0, function () {
            var exchangeToken;
            var _this = this;
            return __generator(this, function (_a) {
                exchangeToken = function () { return __awaiter(_this, void 0, void 0, function () {
                    var data, refreshToken, accessToken, e_2;
                    var _a, _b;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                _c.trys.push([0, 6, , 8]);
                                return [4 /*yield*/, axios({
                                        method: HttpMethod.Post,
                                        url: this.serverUrl + "/" + this._apiVersion + "/tokens/exchange",
                                        data: {
                                            refreshToken: this.refreshToken
                                        }
                                    })];
                            case 1:
                                data = (_c.sent()).data;
                                refreshToken = data.refreshToken, accessToken = data.accessToken;
                                if (!(refreshToken && accessToken)) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, refreshToken)
                                    // this._isUserAuthenticated = true
                                ];
                            case 2:
                                _c.sent();
                                return [3 /*break*/, 5];
                            case 3: return [4 /*yield*/, this.updateAuthDetails('', '')];
                            case 4:
                                _c.sent();
                                (_a = this.sessionExpiredListener) === null || _a === void 0 ? void 0 : _a.call(this);
                                _c.label = 5;
                            case 5: return [3 /*break*/, 8];
                            case 6:
                                e_2 = _c.sent();
                                return [4 /*yield*/, this.updateAuthDetails('', '')];
                            case 7:
                                _c.sent();
                                (_b = this.sessionExpiredListener) === null || _b === void 0 ? void 0 : _b.call(this);
                                return [3 /*break*/, 8];
                            case 8: return [2 /*return*/, this._isUserAuthenticated];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(exchangeToken)]; // --> NOTE: Looks like it will always be ok(true| false), never err()??
            });
        }); };
        /*  User Admin APIs */
        this.signOut = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this._isUserAuthenticated = false; // NOTE: Is manually set in the beginning as well?
                        this.loginId = 0;
                        return [4 /*yield*/, this.updateAuthDetails('', '')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        this.signInWithToken = function (accessToken, refreshToken) { return __awaiter(_this, void 0, void 0, function () {
            var signInWithTokenCallback;
            var _this = this;
            return __generator(this, function (_a) {
                signInWithTokenCallback = function () { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.updateAuthDetails(accessToken, refreshToken)
                                // this._isUserAuthenticated = true
                            ];
                            case 1:
                                _a.sent();
                                // this._isUserAuthenticated = true
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(signInWithTokenCallback)];
            });
        }); };
        this.invite = function (_a) {
            var firstName = _a.firstName, lastName = _a.lastName, email = _a.email, role = _a.role;
            return __awaiter(_this, void 0, void 0, function () {
                var invite;
                var _this = this;
                return __generator(this, function (_b) {
                    invite = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.Invite, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                firstName: firstName,
                                                lastName: lastName,
                                                email: email,
                                                role: role
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(invite)];
                });
            });
        };
        this.signUp = function (_a) {
            var password = _a.password, userAdminId = _a.userAdminId, invitationCode = _a.invitationCode;
            return __awaiter(_this, void 0, void 0, function () {
                var signUp;
                var _this = this;
                return __generator(this, function (_b) {
                    signUp = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, accessToken, refreshToken;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.SignUp, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                password: password,
                                                userAdminId: userAdminId,
                                                invitationCode: invitationCode
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    accessToken = data.accessToken, refreshToken = data.refreshToken;
                                    return [4 /*yield*/, this.updateAuthDetails(accessToken, refreshToken)];
                                case 2:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(signUp)];
                });
            });
        };
        this.signIn = function (email, password) { return __awaiter(_this, void 0, void 0, function () {
            var signIn;
            var _this = this;
            return __generator(this, function (_a) {
                signIn = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data, accessToken, refreshToken;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.SignIn, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email,
                                            password: password
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                accessToken = data.accessToken, refreshToken = data.refreshToken;
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, refreshToken)];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(signIn)];
            });
        }); };
        this.reInvite = function (_a) {
            var userAdminId = _a.userAdminId;
            return __awaiter(_this, void 0, void 0, function () {
                var reInvite;
                var _this = this;
                return __generator(this, function (_b) {
                    reInvite = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.ReInvite, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userAdminId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(reInvite)];
                });
            });
        };
        this.disableAdmin = function (_a) {
            var userAdminId = _a.userAdminId;
            return __awaiter(_this, void 0, void 0, function () {
                var disableAdmin;
                var _this = this;
                return __generator(this, function (_b) {
                    disableAdmin = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.DisableAdmin, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userAdminId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(disableAdmin)];
                });
            });
        };
        this.enableAdmin = function (_a) {
            var userAdminId = _a.userAdminId;
            return __awaiter(_this, void 0, void 0, function () {
                var enableAdmin;
                var _this = this;
                return __generator(this, function (_b) {
                    enableAdmin = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.EnableAdmin, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userAdminId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(enableAdmin)];
                });
            });
        };
        this.updateUserAdminInfo = function (_a) {
            var firstName = _a.firstName, lastName = _a.lastName, email = _a.email, userAdminId = _a.userAdminId;
            return __awaiter(_this, void 0, void 0, function () {
                var updateUserAdminInfo;
                var _this = this;
                return __generator(this, function (_b) {
                    updateUserAdminInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.UpdateUserAdminInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userAdminId),
                                            method: Method,
                                            data: { firstName: firstName, lastName: lastName, email: email }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(updateUserAdminInfo)];
                });
            });
        };
        this.updateUserAdminRole = function (_a) {
            var role = _a.role, userAdminId = _a.userAdminId;
            return __awaiter(_this, void 0, void 0, function () {
                var updateUserAdminRole;
                var _this = this;
                return __generator(this, function (_b) {
                    updateUserAdminRole = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.UpdateUserAdminRole, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userAdminId),
                                            method: Method,
                                            data: { role: role }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(updateUserAdminRole)];
                });
            });
        };
        this.updateUserAdminOnLeave = function (_a) {
            var onLeave = _a.onLeave;
            return __awaiter(_this, void 0, void 0, function () {
                var updateUserAdminOnLeave;
                var _this = this;
                return __generator(this, function (_b) {
                    updateUserAdminOnLeave = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.UpdateUserAdminOnLeave, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: { onLeave: onLeave }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(updateUserAdminOnLeave)];
                });
            });
        };
        this.forgotPassword = function (_a) {
            var email = _a.email;
            return __awaiter(_this, void 0, void 0, function () {
                var forgotPassword;
                var _this = this;
                return __generator(this, function (_b) {
                    forgotPassword = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.ForgotPassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                email: email
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(forgotPassword)];
                });
            });
        };
        this.resetPassword = function (_a) {
            var password = _a.password, resetToken = _a.resetToken;
            return __awaiter(_this, void 0, void 0, function () {
                var resetPassword;
                var _this = this;
                return __generator(this, function (_b) {
                    resetPassword = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.ResetPassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                password: password,
                                                resetToken: resetToken
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(resetPassword)];
                });
            });
        };
        this.getUserAdmins = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, userAdminStatus = _a.userAdminStatus, sort = _a.sort, sortOrder = _a.sortOrder, role = _a.role;
            return __awaiter(_this, void 0, void 0, function () {
                var getUserAdminsBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getUserAdminsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetUserAdmins, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, userAdminStatus: userAdminStatus, sort: sort, sortOrder: sortOrder, role: role }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign({}, data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getUserAdminsBatch)];
                });
            });
        };
        this.getUserAdminsSelfInfo = function () { return __awaiter(_this, void 0, void 0, function () {
            var getUserAdminsSelfInfo;
            var _this = this;
            return __generator(this, function (_a) {
                getUserAdminsSelfInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetUserAdminSelfInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUserAdminsSelfInfo)];
            });
        }); };
        this.updateUserAdminsSelfInfo = function (_a) {
            var firstName = _a.firstName, lastName = _a.lastName;
            return __awaiter(_this, void 0, void 0, function () {
                var updateUserAdminSelfInfo;
                var _this = this;
                return __generator(this, function (_b) {
                    updateUserAdminSelfInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.UpdateUserAdminSelfInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            data: {
                                                firstName: firstName,
                                                lastName: lastName
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(updateUserAdminSelfInfo)];
                });
            });
        };
        /* User Management APIs */
        this.assignManagedUsers = function (list) { return __awaiter(_this, void 0, void 0, function () {
            var assignManagedUsers;
            var _this = this;
            return __generator(this, function (_a) {
                assignManagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.AssignManagedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            list: list
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(assignManagedUsers)];
            });
        }); };
        this.moveAllManagedUsers = function (_a) {
            var fromUserAdminId = _a.fromUserAdminId, toUserAdminId = _a.toUserAdminId, assignmentType = _a.assignmentType;
            return __awaiter(_this, void 0, void 0, function () {
                var moveAllManagedUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    moveAllManagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.MoveAllManagedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                fromUserAdminId: fromUserAdminId,
                                                toUserAdminId: toUserAdminId,
                                                assignmentType: assignmentType
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(moveAllManagedUsers)];
                });
            });
        };
        this.moveBackAllManagedUsers = function (userAdminId) { return __awaiter(_this, void 0, void 0, function () {
            var moveBackAllManagedUsers;
            var _this = this;
            return __generator(this, function (_a) {
                moveBackAllManagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.MoveBackAllManagedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(userAdminId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(moveBackAllManagedUsers)];
            });
        }); };
        this.getAllManagedUsers = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, userStatus = _a.userStatus, userManagementType = _a.userManagementType, sort = _a.sort, sortOrder = _a.sortOrder;
            return __awaiter(_this, void 0, void 0, function () {
                var getManagedUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    getManagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetManagedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, userStatus: userStatus, userManagementType: userManagementType, sort: sort, sortOrder: sortOrder }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign({}, data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getManagedUsers)];
                });
            });
        };
        this.getAllManagedUsersCount = function (queryParams) { return __awaiter(_this, void 0, void 0, function () {
            var getManagedUsers;
            var _this = this;
            return __generator(this, function (_a) {
                getManagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetManagedUsersCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method,
                                        params: queryParams
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getManagedUsers)];
            });
        }); };
        this.moveSelectedUsers = function (userMovementSelected) { return __awaiter(_this, void 0, void 0, function () {
            var moveSelectedUsers;
            var _this = this;
            return __generator(this, function (_a) {
                moveSelectedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.MoveSelectedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: __assign({}, userMovementSelected)
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(moveSelectedUsers)];
            });
        }); };
        this.getUnmanagedUsers = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, userStatus = _a.userStatus, sort = _a.sort, sortOrder = _a.sortOrder;
            return __awaiter(_this, void 0, void 0, function () {
                var getUnmanagedUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    getUnmanagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetUnmanagedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, userStatus: userStatus, sort: sort, sortOrder: sortOrder }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign({}, data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getUnmanagedUsers)];
                });
            });
        };
        this.getUnmanagedUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
            var getUnmanagedUsersCount;
            var _this = this;
            return __generator(this, function (_a) {
                getUnmanagedUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetUnmanagedUsersCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUnmanagedUsersCount)];
            });
        }); };
        /* User Onboarding APIs */
        this.registerUser = function (_a) {
            var firstName = _a.firstName, lastName = _a.lastName, email = _a.email, hrAdminId = _a.hrAdminId, departmentId = _a.departmentId, divisionId = _a.divisionId, employeeId = _a.employeeId, title = _a.title;
            return __awaiter(_this, void 0, void 0, function () {
                var userRegistration;
                var _this = this;
                return __generator(this, function (_b) {
                    userRegistration = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.RegisterUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                firstName: firstName,
                                                lastName: lastName,
                                                email: email,
                                                hrAdminId: hrAdminId,
                                                departmentId: departmentId,
                                                divisionId: divisionId,
                                                employeeId: employeeId,
                                                title: title
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(userRegistration)];
                });
            });
        };
        this.updateUser = function (_a) {
            var firstName = _a.firstName, lastName = _a.lastName, email = _a.email, title = _a.title, departmentId = _a.departmentId, divisionId = _a.divisionId, employeeId = _a.employeeId, userId = _a.userId;
            return __awaiter(_this, void 0, void 0, function () {
                var updateUser;
                var _this = this;
                return __generator(this, function (_b) {
                    updateUser = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.UpdateUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userId),
                                            method: Method,
                                            data: {
                                                firstName: firstName,
                                                lastName: lastName,
                                                email: email,
                                                title: title,
                                                departmentId: departmentId,
                                                divisionId: divisionId,
                                                employeeId: employeeId
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(updateUser)];
                });
            });
        };
        /* Department APIs */
        this.getDepartments = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, sort = _a.sort, sortOrder = _a.sortOrder, _b = _a.includeDivisions, includeDivisions = _b === void 0 ? false : _b;
            return __awaiter(_this, void 0, void 0, function () {
                var getDepartments;
                var _this = this;
                return __generator(this, function (_c) {
                    getDepartments = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetDepartments, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, sort: sort, sortOrder: sortOrder, includeDivisions: includeDivisions }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getDepartments)];
                });
            });
        };
        this.createDepartment = function (name) { return __awaiter(_this, void 0, void 0, function () {
            var createDepartment;
            var _this = this;
            return __generator(this, function (_a) {
                createDepartment = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.CreateDepartment, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            name: name
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createDepartment)];
            });
        }); };
        this.updateDepartment = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var updateDepartment;
            var _this = this;
            return __generator(this, function (_a) {
                updateDepartment = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.UpdateDepartments, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(params.departmentId),
                                        method: Method,
                                        data: {
                                            name: params.name
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateDepartment)];
            });
        }); };
        /* Division APIs */
        this.getDivisions = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, sort = _a.sort, sortOrder = _a.sortOrder, departmentId = _a.departmentId;
            return __awaiter(_this, void 0, void 0, function () {
                var getDivisions;
                var _this = this;
                return __generator(this, function (_b) {
                    getDivisions = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetDivisions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(departmentId),
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, sort: sort, sortOrder: sortOrder }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getDivisions)];
                });
            });
        };
        this.createDivision = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createDivision;
            var _this = this;
            return __generator(this, function (_a) {
                createDivision = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, name, departmentId, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.CreateDivisions, Endpoint = _a.Endpoint, Method = _a.Method;
                                name = params.name, departmentId = params.departmentId;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(departmentId),
                                        method: Method,
                                        data: {
                                            name: name
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createDivision)];
            });
        }); };
        this.updateDivision = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var updateDivision;
            var _this = this;
            return __generator(this, function (_a) {
                updateDivision = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, name, departmentId, divisionId;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.UpdateDivisions, Endpoint = _a.Endpoint, Method = _a.Method;
                                name = params.name, departmentId = params.departmentId, divisionId = params.divisionId;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint({ departmentId: departmentId, divisionId: divisionId }),
                                        method: Method,
                                        data: {
                                            name: name
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateDivision)];
            });
        }); };
        /* Keyword APIs */
        this.createKeyword = function (_a) {
            var term = _a.term, priority = _a.priority;
            return __awaiter(_this, void 0, void 0, function () {
                var createKeyword;
                var _this = this;
                return __generator(this, function (_b) {
                    createKeyword = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.CreateKeyword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                term: term,
                                                priority: priority
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(createKeyword)];
                });
            });
        };
        this.createKeywordBulk = function (list) { return __awaiter(_this, void 0, void 0, function () {
            var createKeywordBulk;
            var _this = this;
            return __generator(this, function (_a) {
                createKeywordBulk = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.CreateKeywordBulk, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            list: list
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createKeywordBulk)];
            });
        }); };
        this.deleteKeyword = function (id) { return __awaiter(_this, void 0, void 0, function () {
            var deleteKeyword;
            var _this = this;
            return __generator(this, function (_a) {
                deleteKeyword = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.DeleteKeyword, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(id),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(deleteKeyword)];
            });
        }); };
        this.updateKeyword = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var updateKeyword;
            var _this = this;
            return __generator(this, function (_a) {
                updateKeyword = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.UpdateKeyword, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(params.id),
                                        method: Method,
                                        data: {
                                            priority: params.priority
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateKeyword)];
            });
        }); };
        this.getKeywordsBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, priority = _a.priority;
            return __awaiter(_this, void 0, void 0, function () {
                var getKeywordsBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getKeywordsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetKeywordsBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, priority: priority }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign({}, data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getKeywordsBatch)];
                });
            });
        };
        this.disableUser = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var disableUser;
            var _this = this;
            return __generator(this, function (_a) {
                disableUser = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.DisableUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(params.userId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(disableUser)];
            });
        }); };
        this.enableUser = function (params) {
            var enableUser = function () { return __awaiter(_this, void 0, void 0, function () {
                var _a, Endpoint, Method;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            _a = ChatAdminRoutes.EnableUser, Endpoint = _a.Endpoint, Method = _a.Method;
                            return [4 /*yield*/, this.httpClient({
                                    url: Endpoint(params.userId),
                                    method: Method
                                })];
                        case 1:
                            _b.sent();
                            return [2 /*return*/, true];
                    }
                });
            }); };
            return _this.guardWithAuth(enableUser);
        };
        this.getMessagesBeforeSequence = function (_a) {
            var beforeSequence = _a.beforeSequence, _b = _a.limit, limit = _b === void 0 ? 20 : _b, conversationId = _a.conversationId;
            return __awaiter(_this, void 0, void 0, function () {
                var getMessagesBeforeSequence;
                var _this = this;
                return __generator(this, function (_c) {
                    getMessagesBeforeSequence = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetMessagesBeforeSequence, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                beforeSequence: beforeSequence,
                                                limit: limit
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, MessageMapper.toPeekDomainEntities(data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getMessagesBeforeSequence)];
                });
            });
        };
        this.getMessagesAfterSequence = function (_a) {
            var afterSequence = _a.afterSequence, _b = _a.limit, limit = _b === void 0 ? 20 : _b, conversationId = _a.conversationId;
            return __awaiter(_this, void 0, void 0, function () {
                var getMessagesAfterSequence;
                var _this = this;
                return __generator(this, function (_c) {
                    getMessagesAfterSequence = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetMessagesAfterSequence, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                afterSequence: afterSequence,
                                                limit: limit
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, MessageMapper.toPeekDomainEntities(data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getMessagesAfterSequence)];
                });
            });
        };
        this.getMessageContext = function (_a) {
            var peekBeforeLimit = _a.peekBeforeLimit, peekAfterLimit = _a.peekAfterLimit, messageId = _a.messageId;
            return __awaiter(_this, void 0, void 0, function () {
                var getMessageContext;
                var _this = this;
                return __generator(this, function (_b) {
                    getMessageContext = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetMessageContext, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            params: {
                                                peekBeforeLimit: peekBeforeLimit,
                                                peekAfterLimit: peekAfterLimit
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, MessageMapper.toPeekDomainEntities(data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getMessageContext)];
                });
            });
        };
        /* Messages Audit */
        this.getAuditMessagesBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, startDate = _a.startDate, endDate = _a.endDate, text = _a.text, textSearchCondition = _a.textSearchCondition, userId = _a.userId, hasAttachment = _a.hasAttachment, hasFlagged = _a.hasFlagged;
            return __awaiter(_this, void 0, void 0, function () {
                var getAuditMessagesBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getAuditMessagesBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetAuditMessagesBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            params: {
                                                offset: offset,
                                                limit: limit,
                                                startDate: startDate,
                                                endDate: endDate,
                                                text: text,
                                                textSearchCondition: textSearchCondition,
                                                userId: userId,
                                                hasAttachment: hasAttachment,
                                                hasFlagged: hasFlagged
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, AuditMessageMapper.toAuditMessageDomainEntityBatchDTO(data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getAuditMessagesBatch)];
                });
            });
        };
        this.downloadAuditMessages = function (_a) {
            var startDate = _a.startDate, endDate = _a.endDate, text = _a.text, textSearchCondition = _a.textSearchCondition, userId = _a.userId, hasAttachment = _a.hasAttachment, hasFlagged = _a.hasFlagged;
            return __awaiter(_this, void 0, void 0, function () {
                var downloadAuditMessages;
                var _this = this;
                return __generator(this, function (_b) {
                    downloadAuditMessages = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.DownloadAuditMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            params: {
                                                startDate: startDate,
                                                endDate: endDate,
                                                text: text,
                                                textSearchCondition: textSearchCondition,
                                                userId: userId,
                                                hasAttachment: hasAttachment,
                                                hasFlagged: hasFlagged
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data.map(AuditMessageMapper.toAuditMessageDomainEntityDTO)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(downloadAuditMessages)];
                });
            });
        };
        this.getUserBasicInfoBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search;
            return __awaiter(_this, void 0, void 0, function () {
                var getAuditMessagesBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getAuditMessagesBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetUserBasicInfoBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            params: {
                                                offset: offset,
                                                limit: limit,
                                                search: search
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getAuditMessagesBatch)];
                });
            });
        };
        /*User Admin Search */
        this.createUpdateUserAdminSearch = function (userAdminSearchCriteria) { return __awaiter(_this, void 0, void 0, function () {
            var createUpdateUserAdminSearch;
            var _this = this;
            return __generator(this, function (_a) {
                createUpdateUserAdminSearch = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.CreateUpdateUserAdminSearch, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: userAdminSearchCriteria
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createUpdateUserAdminSearch)];
            });
        }); };
        this.getUserAdminSearch = function () { return __awaiter(_this, void 0, void 0, function () {
            var getUserAdminSearch;
            var _this = this;
            return __generator(this, function (_a) {
                getUserAdminSearch = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetUserAdminSearch, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUserAdminSearch)];
            });
        }); };
        /* Message Flagging */
        this.getFlaggedMessagesBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, _b = _a.flagType, flagType = _b === void 0 ? 'all' : _b, _c = _a.status, status = _c === void 0 ? 'all' : _c;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesBatch;
                var _this = this;
                return __generator(this, function (_d) {
                    getFlaggedMessagesBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data, flaggedMsgsDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetFlaggedMessagesBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, flagType: flagType, status: status }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesBatch)];
                });
            });
        };
        this.getFlaggedMessagesCount = function () { return __awaiter(_this, void 0, void 0, function () {
            var getFlaggedMessagesCount;
            var _this = this;
            return __generator(this, function (_a) {
                getFlaggedMessagesCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetFlaggedMessagesCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesCount)];
            });
        }); };
        this.resolveFlaggedMessage = function (_a) {
            var resolutionStatus = _a.resolutionStatus, resolutionNotes = _a.resolutionNotes, messageDeletion = _a.messageDeletion, flaggedMessageId = _a.flaggedMessageId, notifySuperAdmin = _a.notifySuperAdmin;
            return __awaiter(_this, void 0, void 0, function () {
                var resolveFlaggedMessage;
                var _this = this;
                return __generator(this, function (_b) {
                    resolveFlaggedMessage = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.ResolveFlaggedMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(flaggedMessageId),
                                            method: Method,
                                            data: { resolutionStatus: resolutionStatus, resolutionNotes: resolutionNotes, messageDeletion: messageDeletion, notifySuperAdmin: notifySuperAdmin }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(resolveFlaggedMessage)];
                });
            });
        };
        this.getFlaggedMessageDetails = function (_a) {
            var flaggedMessageId = _a.flaggedMessageId;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessageDetails;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessageDetails = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetFlaggedMessageDetails, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(flaggedMessageId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign({}, data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessageDetails)];
                });
            });
        };
        this.getFlaggedMessagesUserReportsBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, flaggedMessageId = _a.flaggedMessageId, status = _a.status;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesUserReportsBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessagesUserReportsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, flaggedMsgsUserReportsDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetFlaggedMessageUserReports, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(flaggedMessageId),
                                            method: Method,
                                            params: { offset: offset, limit: limit, status: status }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsUserReportsDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsUserReportsDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesUserReportsBatch)];
                });
            });
        };
        this.getFlaggedMessagesKeywordScansBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, flaggedMessageId = _a.flaggedMessageId, status = _a.status;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesKeywordScansBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessagesKeywordScansBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, flaggedMsgsKeywordScansDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetFlaggedMessageKeywordScans, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(flaggedMessageId),
                                            method: Method,
                                            params: { offset: offset, limit: limit, status: status }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsKeywordScansDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsKeywordScansDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesKeywordScansBatch)];
                });
            });
        };
        this.getFlaggedMessagesResolutionsBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, flaggedMessageId = _a.flaggedMessageId;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesResolutionsBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessagesResolutionsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, flaggedMsgsResolutionsDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetFlaggedMessageResolutions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(flaggedMessageId),
                                            method: Method,
                                            params: { offset: offset, limit: limit }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsResolutionsDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsResolutionsDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesResolutionsBatch)];
                });
            });
        };
        this.getFlaggedMessagesResolutionUserReportsBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, resolutionId = _a.resolutionId;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesResolutionUserReportsBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessagesResolutionUserReportsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, flaggedMsgsResolutionUserReportsDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetFlaggedMessageResolutionUserReports, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(resolutionId),
                                            method: Method,
                                            params: { offset: offset, limit: limit }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsResolutionUserReportsDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsResolutionUserReportsDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesResolutionUserReportsBatch)];
                });
            });
        };
        this.getFlaggedMessagesResolutionKeywordScansBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, resolutionId = _a.resolutionId;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesResolutionKeywordScansBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessagesResolutionKeywordScansBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, flaggedMsgsResolutionKeywordScansDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetFlaggedMessageResolutionKeywordScans, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(resolutionId),
                                            method: Method,
                                            params: { offset: offset, limit: limit }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsResolutionKeywordScansDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsResolutionKeywordScansDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesResolutionKeywordScansBatch)];
                });
            });
        };
        this.getReactedUsers = function (_a) {
            var messageId = _a.messageId, type = _a.type, limit = _a.limit, afterSequence = _a.afterSequence;
            return __awaiter(_this, void 0, void 0, function () {
                var getReactedUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    getReactedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.GetReactedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            params: {
                                                limit: limit,
                                                afterSequence: afterSequence,
                                                type: type || ''
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getReactedUsers)];
                });
            });
        };
        /* Conversation Space Group Divsions */
        this.getSpaceGroupDivisions = function (_a) {
            var conversationId = _a.conversationId;
            return __awaiter(_this, void 0, void 0, function () {
                var getSpaceGroupDivisions;
                var _this = this;
                return __generator(this, function (_b) {
                    getSpaceGroupDivisions = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetSpaceGroupDivisions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getSpaceGroupDivisions)];
                });
            });
        };
        this.createConversationSpaceGroupDivision = function (_a) {
            var conversationId = _a.conversationId, divisionIds = _a.divisionIds, memberIds = _a.memberIds;
            return __awaiter(_this, void 0, void 0, function () {
                var createConversationSpaceGroupDivision;
                var _this = this;
                return __generator(this, function (_b) {
                    createConversationSpaceGroupDivision = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.CreateConversationSpaceGroupDivision, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                divisionIds: divisionIds,
                                                memberIds: memberIds
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(createConversationSpaceGroupDivision)];
                });
            });
        };
        this.deleteConversationSpaceGroupDivision = function (_a) {
            var conversationId = _a.conversationId, divisionIds = _a.divisionIds;
            return __awaiter(_this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_b) {
                    return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = ChatAdminRoutes.DeleteConversationSpaceGroupDivision, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, this.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                data: {
                                                    divisionIds: divisionIds
                                                }
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        }); })
                        /* Groups */
                    ];
                });
            });
        };
        /* Groups */
        this.createGroupConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createGroupConversation;
            var _this = this;
            return __generator(this, function (_a) {
                createGroupConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, file, groupName, userIds, divisionIds, fileIdResponse, data_1, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.CreateGroupConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                file = params.file, groupName = params.groupName, userIds = params.userIds, divisionIds = params.divisionIds;
                                if (!file) return [3 /*break*/, 4];
                                return [4 /*yield*/, this.uploadFile(file.path, file.fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            groupName: groupName,
                                            groupImageId: fileIdResponse.value,
                                            memberIds: userIds,
                                            divisionIds: divisionIds
                                        }
                                    })];
                            case 2:
                                data_1 = (_b.sent()).data;
                                return [2 /*return*/, data_1];
                            case 3: return [2 /*return*/, fileIdResponse.error];
                            case 4: return [4 /*yield*/, this.httpClient({
                                    url: Endpoint,
                                    method: Method,
                                    data: {
                                        groupName: groupName,
                                        groupImageId: null,
                                        memberIds: userIds,
                                        divisionIds: divisionIds
                                    }
                                })];
                            case 5:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createGroupConversation)];
            });
        }); };
        this.updateGroupOwner = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var updateGroupOwner;
            var _this = this;
            return __generator(this, function (_a) {
                updateGroupOwner = function () { return __awaiter(_this, void 0, void 0, function () {
                    var conversationId, userAdminId, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                conversationId = params.conversationId, userAdminId = params.userAdminId;
                                _a = ChatAdminRoutes.UpdateGroupConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId, userAdminId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateGroupOwner)];
            });
        }); };
        this.getGroupConversationBatch = function (_a) {
            var search = _a.search, offset = _a.offset, limit = _a.limit;
            return __awaiter(_this, void 0, void 0, function () {
                var getConversationGroupForUserAdminBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getConversationGroupForUserAdminBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, conversationGroupBatchDTOForUserAdminWithUserCount;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetUserAdminGroups, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            params: { search: search, offset: offset, limit: limit }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    conversationGroupBatchDTOForUserAdminWithUserCount = __assign({}, data);
                                    return [2 /*return*/, conversationGroupBatchDTOForUserAdminWithUserCount];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getConversationGroupForUserAdminBatch)];
                });
            });
        };
        this.deleteGroupConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var deleteGroupConversation;
            var _this = this;
            return __generator(this, function (_a) {
                deleteGroupConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var conversationId, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                conversationId = params.conversationId;
                                _a = ChatCommonRoutes.DeleteGroupConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(deleteGroupConversation)];
            });
        }); };
        this.getReachableUsers = function (_a) {
            var _b = _a === void 0 ? {} : _a, searchString = _b.searchString, limit = _b.limit, nextCursor = _b.nextCursor, includeRefIdInSearch = _b.includeRefIdInSearch;
            return __awaiter(_this, void 0, void 0, function () {
                var getReachableUsers;
                var _this = this;
                return __generator(this, function (_c) {
                    getReachableUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, params, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.GetReachableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    params = {
                                        search: searchString || '',
                                        includeRefIdInSearch: includeRefIdInSearch || false
                                    };
                                    if (limit) {
                                        params.limit = limit;
                                    }
                                    if (nextCursor) {
                                        params.offset = nextCursor;
                                    }
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: params
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign(__assign({}, data), { results: UserMapper.toUserForUserDomainEntities(data.results) })];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getReachableUsers)];
                });
            });
        };
        this.getConversationUsers = function (_a) {
            var conversationId = _a.conversationId, searchString = _a.searchString, limit = _a.limit, nextCursor = _a.nextCursor, type = _a.type, includeRefIdInSearch = _a.includeRefIdInSearch;
            var getConversationUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                var _a, Endpoint, Method, data;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            _a = ChatCommonRoutes.GetConversationUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                            return [4 /*yield*/, this.httpClient({
                                    url: Endpoint(conversationId),
                                    method: Method,
                                    params: {
                                        type: type || ConversationUserQueryType.Current,
                                        offset: nextCursor,
                                        limit: limit,
                                        search: searchString,
                                        includeRefIdInSearch: includeRefIdInSearch || false
                                    }
                                })];
                        case 1:
                            data = (_b.sent()).data;
                            return [2 /*return*/, __assign(__assign({}, data), { results: UserMapper.toConversableUserDomainForUserAdminEntities(data.results) })];
                    }
                });
            }); };
            return _this.guardWithAuth(getConversationUsers);
        };
        this.addUsersToConversation = function (conversationId, userIds, force) { return __awaiter(_this, void 0, void 0, function () {
            var addUsersToConversation;
            var _this = this;
            return __generator(this, function (_a) {
                addUsersToConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatCommonRoutes.AddUsersToConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            memberIds: userIds
                                        },
                                        params: {
                                            force: force
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(addUsersToConversation)];
            });
        }); };
        this.removeUsersFromConversation = function (conversationId, userIds, force) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.RemoveUsersFromConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                memberIds: userIds
                                            },
                                            params: {
                                                force: force
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.updateGroupImage = function (conversationId, file) { return __awaiter(_this, void 0, void 0, function () {
            var updateGroupImage;
            var _this = this;
            return __generator(this, function (_a) {
                updateGroupImage = function () { return __awaiter(_this, void 0, void 0, function () {
                    var fileIdResponse, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, this.uploadFile(file.path, file.fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                _a = ChatCommonRoutes.UpdateGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            groupImageId: fileIdResponse.value
                                        }
                                    })];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                            case 3: return [2 /*return*/, false];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateGroupImage)];
            });
        }); };
        this.removeGroupImage = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var removeGroupImage;
            var _this = this;
            return __generator(this, function (_a) {
                removeGroupImage = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatCommonRoutes.RemoveGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(removeGroupImage)];
            });
        }); };
        this.getGroupDefaultImageSet = function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.GetGroupDefaultImageSet, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); })];
            });
        }); };
        this.updateGroupInfo = function (conversationId, groupName) { return __awaiter(_this, void 0, void 0, function () {
            var updateGroupInfo;
            var _this = this;
            return __generator(this, function (_a) {
                updateGroupInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatCommonRoutes.UpdateGroupInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            groupName: groupName
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateGroupInfo)];
            });
        }); };
        this.makeConversationOwner = function (conversationId, userId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.MakeConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId, userId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.removeConversationOwner = function (conversationId, userId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.RemoveConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId, userId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.getConversationDetails = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var getConversationDetails;
            var _this = this;
            return __generator(this, function (_a) {
                getConversationDetails = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatCommonRoutes.GetConversationDetails, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getConversationDetails)];
            });
        }); };
        this.getTotalUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
            var getTotalUsersCount;
            var _this = this;
            return __generator(this, function (_a) {
                getTotalUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetTotalUsersCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getTotalUsersCount)];
            });
        }); };
        this.getMyUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
            var getMyUsersCount;
            var _this = this;
            return __generator(this, function (_a) {
                getMyUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetMyUsersCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getMyUsersCount)];
            });
        }); };
        this.getResolvedFlaggedMessagesCount = function (timeFrame) { return __awaiter(_this, void 0, void 0, function () {
            var getResolvedFlaggedMessagesCount;
            var _this = this;
            return __generator(this, function (_a) {
                getResolvedFlaggedMessagesCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetResolvedFlaggedMessagesCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method,
                                        params: { timeFrame: timeFrame }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getResolvedFlaggedMessagesCount)];
            });
        }); };
        this.getSentMessagesCountDaily = function () { return __awaiter(_this, void 0, void 0, function () {
            var getSentMessagesCountDaily;
            var _this = this;
            return __generator(this, function (_a) {
                getSentMessagesCountDaily = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetSentMessagesCountDaily, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getSentMessagesCountDaily)];
            });
        }); };
        this.getSentMessagesCountTotal = function (timeFrame) { return __awaiter(_this, void 0, void 0, function () {
            var getSentMessagesCountTotal;
            var _this = this;
            return __generator(this, function (_a) {
                getSentMessagesCountTotal = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetSentMessagesCountTotal, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method,
                                        params: { timeFrame: timeFrame }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getSentMessagesCountTotal)];
            });
        }); };
        this.getUnresolvedFlaggedMessagesCount = function (unresolvedMsgScope) { return __awaiter(_this, void 0, void 0, function () {
            var getUnresolvedFlaggedMessagesCount;
            var _this = this;
            return __generator(this, function (_a) {
                getUnresolvedFlaggedMessagesCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetUnresolvedFlaggedMessagesCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method,
                                        params: { unresolvedMsgScope: unresolvedMsgScope }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUnresolvedFlaggedMessagesCount)];
            });
        }); };
        this.getUnresolvedFlaggedMessagesCountDaily = function () { return __awaiter(_this, void 0, void 0, function () {
            var getUnresolvedFlaggedMessagesCountDaily;
            var _this = this;
            return __generator(this, function (_a) {
                getUnresolvedFlaggedMessagesCountDaily = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetUnresolvedFlaggedMessageCountsDaily, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUnresolvedFlaggedMessagesCountDaily)];
            });
        }); };
        this.getEligibleRolesToInviteUserAdmin = function () { return __awaiter(_this, void 0, void 0, function () {
            var getEligibleRolesToInviteUserAdmin;
            var _this = this;
            return __generator(this, function (_a) {
                getEligibleRolesToInviteUserAdmin = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.GetEligibleRolesToInviteAdmin, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getEligibleRolesToInviteUserAdmin)];
            });
        }); };
        /* User Onboarding Job  */
        this.processUserOnboardingJobUpload = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var processUserOnboardingJobUpload;
            var _this = this;
            return __generator(this, function (_a) {
                processUserOnboardingJobUpload = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, formData, fileUri, fileName, departmentId, divisionId, extension, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.ProcessUserOnboardingJobUpload, Endpoint = _a.Endpoint, Method = _a.Method;
                                formData = new FormData();
                                fileUri = params.fileUri, fileName = params.fileName, departmentId = params.departmentId, divisionId = params.divisionId;
                                formData.append('file', fileUri);
                                if (fileUri instanceof Blob) {
                                    formData.append('file', fileUri, fileName);
                                }
                                else {
                                    extension = fileName.split('.').pop();
                                    if (extension) {
                                        formData.append('file', {
                                            uri: fileUri,
                                            name: fileName,
                                            type: mimeTypes[extension.toLowerCase()]
                                        });
                                    }
                                    else {
                                        return [2 /*return*/, new SayHey.Error('Invalid file')];
                                    }
                                }
                                formData.append('departmentId', departmentId);
                                formData.append('divisionId', divisionId);
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: formData
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(processUserOnboardingJobUpload)];
            });
        }); };
        this.getUserOnboardingJobBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, status = _a.status;
            return __awaiter(_this, void 0, void 0, function () {
                var getUserOnboardingJobBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getUserOnboardingJobBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetUserOnboardingJobBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, status: status }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getUserOnboardingJobBatch)];
                });
            });
        };
        this.downloadSampleFile = function () { return __awaiter(_this, void 0, void 0, function () {
            var downloadSampleFile;
            var _this = this;
            return __generator(this, function (_a) {
                downloadSampleFile = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatAdminRoutes.DownloadSampleFile, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(downloadSampleFile)];
            });
        }); };
        this.getAllErroredUserOnboardingJobRecords = function (_a) {
            var userOnboardingJobId = _a.userOnboardingJobId;
            return __awaiter(_this, void 0, void 0, function () {
                var getAllErroredUserOnboardingJobRecords;
                var _this = this;
                return __generator(this, function (_b) {
                    getAllErroredUserOnboardingJobRecords = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatAdminRoutes.GetAllErroredUserOnboardingJobRecords, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userOnboardingJobId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getAllErroredUserOnboardingJobRecords)];
                });
            });
        };
    }
    /**
     * Setup methods
     */
    ChatAdminKit.prototype.initialize = function (params) {
        var sayheyAppId = params.sayheyAppId, sayheyApiKey = params.sayheyApiKey, sayheyUrl = params.sayheyUrl, disableAutoRefreshToken = params.disableAutoRefreshToken, apiVersion = params.apiVersion;
        this.serverUrl = sayheyUrl;
        this._apiVersion = apiVersion || API_VERSION;
        var baseURL = this.serverUrl + "/" + this._apiVersion + "/apps/" + sayheyAppId;
        this.apiKey = sayheyApiKey;
        this.appId = sayheyAppId;
        this.disableAutoRefreshToken = disableAutoRefreshToken;
        this.httpClient = httpClient.createWithoutAuth({
            apiKey: this.apiKey,
            baseURL: baseURL
        });
        this._initialized = true;
    };
    return ChatAdminKit;
}());

var ChatClientKit = /** @class */ (function () {
    function ChatClientKit() {
        var _this = this;
        this.accessToken = '';
        this.publisherToken = '';
        this.refreshToken = '';
        this.instanceIdPushNotification = '';
        this._apiVersion = API_VERSION;
        this.appId = '';
        this.apiKey = '';
        this.serverUrl = '';
        this._initialized = false;
        this._subscribed = false;
        this._isUserAuthenticated = false;
        this.readMessageMap = {};
        // readMessageSequenceMap: { [conversationId: string]: number } = {}
        this.readMessageSequenceMap = new Map();
        this.isSendingReadReceipt = false;
        this.timerHandle = null;
        this.exchangeTokenPromise = null;
        this.loginId = 0;
        this.isUserAuthenticated = function () { return _this._isUserAuthenticated; };
        this.addListeners = function (listeners) {
            if (_this._initialized) {
                var onSocketConnect = listeners.onSocketConnect, onSocketDisconnect = listeners.onSocketDisconnect, onSocketError = listeners.onSocketError, onSocketReconnect = listeners.onSocketReconnect, onAuthDetailChange = listeners.onAuthDetailChange, onEvent = listeners.onEvent, onSessionExpired = listeners.onSessionExpired;
                _this._subscribed = true;
                _this.authDetailChangeListener = onAuthDetailChange;
                _this.eventListener = onEvent;
                _this.sessionExpiredListener = onSessionExpired;
                _this.publisherKitClient.onMessage(_this.onMessage);
                onSocketConnect && _this.publisherKitClient.onConnect(onSocketConnect);
                onSocketDisconnect && _this.publisherKitClient.onDisconnect(onSocketDisconnect);
                onSocketError && _this.publisherKitClient.onError(onSocketError);
                onSocketReconnect && _this.publisherKitClient.onReconnect(onSocketReconnect);
                return ok(true);
            }
            return err(new SayHey.Error('ChatKit has not been initialized!', SayHey.ErrorType.InternalErrorType.Uninitialized));
        };
        /**
         * Helper methods
         */
        /**
         *
         * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
         */
        this.guard = function (cb) { return __awaiter(_this, void 0, void 0, function () {
            var res, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._initialized) {
                            return [2 /*return*/, err(new SayHey.Error('ChatKit has not been initialized!', SayHey.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        if (!this._subscribed) {
                            return [2 /*return*/, err(new SayHey.Error('ChatKit events have not been subscribed to!', SayHey.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, cb()];
                    case 2:
                        res = _a.sent();
                        if (res instanceof SayHey.Error) {
                            return [2 /*return*/, err(res)];
                        }
                        return [2 /*return*/, ok(res)];
                    case 3:
                        e_1 = _a.sent();
                        if (e_1 instanceof SayHey.Error) {
                            return [2 /*return*/, err(e_1)];
                        }
                        return [2 /*return*/, err(new SayHey.Error('Unknown error', SayHey.ErrorType.InternalErrorType.Unknown))];
                    case 4: return [2 /*return*/];
                }
            });
        }); };
        this.exchangeTokenOnce = function () { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.exchangeTokenPromise) {
                            return [2 /*return*/, this.exchangeTokenPromise];
                        }
                        this.exchangeTokenPromise = this.exchangeToken();
                        return [4 /*yield*/, this.exchangeTokenPromise];
                    case 1:
                        response = _a.sent();
                        this.exchangeTokenPromise = null;
                        return [2 /*return*/, response];
                }
            });
        }); };
        this.isSocketConnected = function () { return _this.publisherKitClient.isConnected(); };
        /**
         *
         * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
         */
        this.guardWithAuth = function (cb) { return __awaiter(_this, void 0, void 0, function () {
            var currentLoginId, res, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        currentLoginId = this.loginId;
                        return [4 /*yield*/, this.guard(cb)];
                    case 1:
                        res = _a.sent();
                        if (!res.isErr()) return [3 /*break*/, 5];
                        if (!(!this.disableAutoRefreshToken &&
                            res.error.type === SayHey.ErrorType.AuthErrorType.TokenExpiredType)) return [3 /*break*/, 5];
                        if (!(currentLoginId === this.loginId)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.exchangeTokenOnce()];
                    case 2:
                        response = _a.sent();
                        if (response.isOk() && response.value === true) {
                            // --> NOTE: Modified to check response is also true => isUserAuthenticated, else logout
                            return [2 /*return*/, this.guard(cb)];
                        }
                        return [4 /*yield*/, this.signOut()];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4: 
                    // If token has been exchanged, use the new credentials to make the request again
                    return [2 /*return*/, this.guard(cb)];
                    case 5: return [2 /*return*/, res];
                }
            });
        }); };
        /**
         *
         * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
         * @param publisherToken - publisher token returned from SayHey that will be used to verify Publisher access
         * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
         */
        this.updateAuthDetails = function (accessToken, publisherToken, refreshToken) { return __awaiter(_this, void 0, void 0, function () {
            var id;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.accessToken = accessToken;
                        this.publisherToken = publisherToken;
                        this.refreshToken = refreshToken;
                        id = accessToken ? decode(accessToken).id : '';
                        if (publisherToken && accessToken && refreshToken) {
                            this.loginId += 1;
                            this.httpClient = httpClient.createWithAuth({
                                apiKey: this.apiKey,
                                baseURL: this.serverUrl + "/" + this._apiVersion + "/apps/" + this.appId,
                                accessToken: this.accessToken
                            });
                            this.publisherKitClient.updateAccessParams({
                                token: publisherToken,
                                userId: id
                            });
                            if (!this.publisherKitClient.isConnected()) {
                                this.publisherKitClient.connect();
                            }
                        }
                        return [4 /*yield*/, ((_a = this.authDetailChangeListener) === null || _a === void 0 ? void 0 : _a.call(this, {
                                userId: id,
                                accessToken: accessToken,
                                publisherToken: publisherToken,
                                refreshToken: refreshToken
                            }))];
                    case 1:
                        _b.sent();
                        // NOTE: Should be done at the end?
                        this._isUserAuthenticated = !!(publisherToken && accessToken && refreshToken);
                        return [2 /*return*/];
                }
            });
        }); };
        // NOTE: Expose these methods only when there is a strong need to do so
        // public addGuard = async <T>(
        //   cb: (...args: any[]) => Promise<T | SayHey.Error>
        // ): Promise<Result<T, SayHey.Error>> => {
        //   return this.guard(cb)
        // }
        // public addGuardWithAuth = async <T>(
        //   cb: (...args: any[]) => Promise<T | SayHey.Error>
        // ): Promise<Result<T, SayHey.Error>> => {
        //   return this.guardWithAuth(cb)
        // }
        /**
         *
         * @param urlPath - url path returned from SayHey
         */
        this.getMediaSourceDetails = function (urlPath) {
            var _a;
            return ({
                uri: "" + _this.serverUrl + urlPath,
                method: 'GET',
                headers: (_a = {},
                    _a[ApiKeyName] = _this.apiKey,
                    _a.Authorization = "Bearer " + _this.accessToken,
                    _a.Accept = 'image/*, video/*, audio/*',
                    _a)
            });
        };
        this.getMediaDirectUrl = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var getMediaDirectUrl;
            var _this = this;
            return __generator(this, function (_a) {
                getMediaDirectUrl = function () { return __awaiter(_this, void 0, void 0, function () {
                    var queryString, data;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                queryString = {
                                    noRedirect: true
                                };
                                if (params.size) {
                                    queryString.size = params.size;
                                }
                                return [4 /*yield*/, this.httpClient({
                                        url: "" + this.serverUrl + params.urlPath,
                                        method: 'GET',
                                        params: queryString
                                    })];
                            case 1:
                                data = (_a.sent()).data;
                                return [2 /*return*/, data.url];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getMediaDirectUrl)];
            });
        }); };
        this.uploadFile = function (fileUri, fileName, progress, thumbnailId) { return __awaiter(_this, void 0, void 0, function () {
            var uploadFile;
            var _this = this;
            return __generator(this, function (_a) {
                uploadFile = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, formData, extension, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatCommonRoutes.UploadFile, Endpoint = _a.Endpoint, Method = _a.Method;
                                formData = new FormData();
                                if (fileUri instanceof Blob) {
                                    formData.append('file', fileUri, fileName);
                                    if (thumbnailId) {
                                        formData.append('thumbnailId', thumbnailId);
                                    }
                                }
                                else {
                                    extension = fileName.split('.').pop();
                                    if (extension) {
                                        formData.append('file', {
                                            uri: fileUri,
                                            name: fileName,
                                            type: mimeTypes[extension.toLowerCase()]
                                        });
                                        if (thumbnailId) {
                                            formData.append('thumbnailId', thumbnailId);
                                        }
                                    }
                                    else {
                                        return [2 /*return*/, new SayHey.Error('Invalid file')];
                                    }
                                }
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: formData,
                                        onUploadProgress: progress
                                            ? function (event) {
                                                var loaded = event.loaded, total = event.total;
                                                progress((loaded / total) * 0.95);
                                            }
                                            : undefined
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                progress === null || progress === void 0 ? void 0 : progress(1);
                                return [2 /*return*/, data.id];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(uploadFile)];
            });
        }); };
        this.uploadImage = function (fileUri, fileName) { return __awaiter(_this, void 0, void 0, function () {
            var extension, isSupportedImage;
            var _a;
            return __generator(this, function (_b) {
                extension = (_a = fileName
                    .split('.')
                    .pop()) === null || _a === void 0 ? void 0 : _a.toLowerCase();
                isSupportedImage = mimeTypes[extension || ''].split('/')[0] === 'image';
                if (isSupportedImage) {
                    return [2 /*return*/, this.uploadFile(fileUri, fileName)];
                }
                return [2 /*return*/, err(new SayHey.Error('Please select a valid image!', SayHey.ErrorType.InternalErrorType.BadData))];
            });
        }); };
        this.disconnectPublisher = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.publisherKitClient.disconnect();
                return [2 /*return*/];
            });
        }); };
        this.reconnectPublisher = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.publisherKitClient.connect();
                return [2 /*return*/];
            });
        }); };
        /**
         * Token APIs
         */
        /**
         *
         * @param email - user email for SayHey
         * @param password - user password for SayHey
         *
         */
        this.signIn = function (email, password) { return __awaiter(_this, void 0, void 0, function () {
            var signInCallback;
            var _this = this;
            return __generator(this, function (_a) {
                signInCallback = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.SignIn, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email,
                                            password: password
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)
                                    // this._isUserAuthenticated = true
                                ];
                            case 2:
                                _b.sent();
                                // this._isUserAuthenticated = true
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(signInCallback)];
            });
        }); };
        this.signInWithToken = function (accessToken, publisherToken, refreshToken) { return __awaiter(_this, void 0, void 0, function () {
            var signInWithTokenCallback;
            var _this = this;
            return __generator(this, function (_a) {
                signInWithTokenCallback = function () { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)
                                // this._isUserAuthenticated = true
                            ];
                            case 1:
                                _a.sent();
                                // this._isUserAuthenticated = true
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(signInWithTokenCallback)];
            });
        }); };
        this.signUp = function (email, password, firstName, lastName) { return __awaiter(_this, void 0, void 0, function () {
            var signUp;
            var _this = this;
            return __generator(this, function (_a) {
                signUp = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.SignUp, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email,
                                            password: password,
                                            firstName: firstName,
                                            lastName: lastName
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)
                                    // this._isUserAuthenticated = true
                                ];
                            case 2:
                                _b.sent();
                                // this._isUserAuthenticated = true
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(signUp)];
            });
        }); };
        this.signOut = function () { return __awaiter(_this, void 0, void 0, function () {
            var unregisteringPushNotification;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.publisherKitClient.disconnect();
                        this._isUserAuthenticated = false; // NOTE: Is manually set in the beginning as well?
                        this.loginId = 0;
                        unregisteringPushNotification = function () { return __awaiter(_this, void 0, void 0, function () {
                            var result;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, this.unregisterForPushNotification()];
                                    case 1:
                                        result = _a.sent();
                                        if (result.isOk() ||
                                            (result.isErr() && result.error.statusCode && result.error.statusCode < 500)) {
                                            return [2 /*return*/];
                                        }
                                        setTimeout(unregisteringPushNotification, 3000);
                                        return [2 /*return*/];
                                }
                            });
                        }); };
                        return [4 /*yield*/, unregisteringPushNotification()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.updateAuthDetails('', '', '')];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        this.exchangeToken = function () { return __awaiter(_this, void 0, void 0, function () {
            var exchangeToken;
            var _this = this;
            return __generator(this, function (_a) {
                exchangeToken = function () { return __awaiter(_this, void 0, void 0, function () {
                    var data, refreshToken, accessToken, publisherToken, e_2;
                    var _a, _b;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                _c.trys.push([0, 6, , 8]);
                                return [4 /*yield*/, axios({
                                        method: HttpMethod.Post,
                                        url: this.serverUrl + "/" + this._apiVersion + "/tokens/exchange",
                                        data: {
                                            refreshToken: this.refreshToken
                                        }
                                    })];
                            case 1:
                                data = (_c.sent()).data;
                                refreshToken = data.refreshToken, accessToken = data.accessToken, publisherToken = data.publisherToken;
                                if (!(refreshToken && accessToken)) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)
                                    // this._isUserAuthenticated = true
                                ];
                            case 2:
                                _c.sent();
                                return [3 /*break*/, 5];
                            case 3: return [4 /*yield*/, this.updateAuthDetails('', '', '')];
                            case 4:
                                _c.sent();
                                (_a = this.sessionExpiredListener) === null || _a === void 0 ? void 0 : _a.call(this);
                                _c.label = 5;
                            case 5: return [3 /*break*/, 8];
                            case 6:
                                e_2 = _c.sent();
                                return [4 /*yield*/, this.updateAuthDetails('', '', '')];
                            case 7:
                                _c.sent();
                                (_b = this.sessionExpiredListener) === null || _b === void 0 ? void 0 : _b.call(this);
                                return [3 /*break*/, 8];
                            case 8: return [2 /*return*/, this._isUserAuthenticated];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(exchangeToken)]; // --> NOTE: Looks like it will always be ok(true| false), never err()??
            });
        }); };
        this.changePassword = function (currentPassword, newPassword) { return __awaiter(_this, void 0, void 0, function () {
            var changePassword;
            var _this = this;
            return __generator(this, function (_a) {
                changePassword = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.ChangePassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            currentPassword: currentPassword,
                                            newPassword: newPassword
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(changePassword)];
            });
        }); };
        this.forgotPassword = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var forgotPassword;
            var _this = this;
            return __generator(this, function (_a) {
                forgotPassword = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.ForgotPassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(forgotPassword)];
            });
        }); };
        this.resetPassword = function (_a) {
            var password = _a.password, resetPasswordToken = _a.resetPasswordToken;
            return __awaiter(_this, void 0, void 0, function () {
                var resetPassword;
                var _this = this;
                return __generator(this, function (_b) {
                    resetPassword = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.ResetPassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                password: password,
                                                resetPasswordToken: resetPasswordToken
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(resetPassword)];
                });
            });
        };
        /**
         * User APIs
         */
        this.getReachableUsers = function (_a) {
            var _b = _a === void 0 ? {} : _a, searchString = _b.searchString, limit = _b.limit, nextCursor = _b.nextCursor, includeRefIdInSearch = _b.includeRefIdInSearch;
            return __awaiter(_this, void 0, void 0, function () {
                var getReachableUsers;
                var _this = this;
                return __generator(this, function (_c) {
                    getReachableUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, params, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.GetReachableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    params = {
                                        search: searchString || '',
                                        includeRefIdInSearch: includeRefIdInSearch || false
                                    };
                                    if (limit) {
                                        params.limit = limit;
                                    }
                                    if (nextCursor) {
                                        params.offset = nextCursor;
                                    }
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: params
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign(__assign({}, data), { results: UserMapper.toDomainEntities(data.results) })];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getReachableUsers)];
                });
            });
        };
        this.updateUserProfileImage = function (fileUri, fileName) { return __awaiter(_this, void 0, void 0, function () {
            var updateUserProfileImage;
            var _this = this;
            return __generator(this, function (_a) {
                updateUserProfileImage = function () { return __awaiter(_this, void 0, void 0, function () {
                    var fileIdResponse, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, this.uploadFile(fileUri, fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                _a = ChatClientRoutes.UpdateUserProfileImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            profileImageId: fileIdResponse.value
                                        }
                                    })];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                            case 3: return [2 /*return*/, false];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateUserProfileImage)];
            });
        }); };
        this.updateUserProfileInfo = function (options) { return __awaiter(_this, void 0, void 0, function () {
            var updateUserProfileInfo;
            var _this = this;
            return __generator(this, function (_a) {
                updateUserProfileInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.UpdateUserProfileInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: options
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateUserProfileInfo)];
            });
        }); };
        this.getUserDetails = function () { return __awaiter(_this, void 0, void 0, function () {
            var getUserDetails;
            var _this = this;
            return __generator(this, function (_a) {
                getUserDetails = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.GetUserProfile, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, UserMapper.toSelfDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUserDetails)];
            });
        }); };
        this.muteAllNotifications = function () { return __awaiter(_this, void 0, void 0, function () {
            var muteAllNotifications;
            var _this = this;
            return __generator(this, function (_a) {
                muteAllNotifications = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.MuteAllNotifications, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(muteAllNotifications)];
            });
        }); };
        this.unmuteAllNotifications = function () { return __awaiter(_this, void 0, void 0, function () {
            var unmuteAllNotifications;
            var _this = this;
            return __generator(this, function (_a) {
                unmuteAllNotifications = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.UnmuteAllNotifications, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(unmuteAllNotifications)];
            });
        }); };
        /**
         * Conversation APIs
         */
        this.getExistingIndvidualOrQuickConversation = function (memberIds) { return __awaiter(_this, void 0, void 0, function () {
            var getExistingIndvidualOrQuickConversation;
            var _this = this;
            return __generator(this, function (_a) {
                getExistingIndvidualOrQuickConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.GetExistingIndvidualOrQuickConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        params: {
                                            memberIds: memberIds
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                if (!data) {
                                    return [2 /*return*/, null];
                                }
                                return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getExistingIndvidualOrQuickConversation)];
            });
        }); };
        this.getConversations = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationType, conversationGroupType, getConversations;
            var _this = this;
            return __generator(this, function (_a) {
                conversationType = params.conversationType, conversationGroupType = params.conversationGroupType;
                getConversations = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.GetConversations, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        params: {
                                            conversationType: conversationType,
                                            conversationGroupType: conversationGroupType
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, ConversationMapper.toDomainEntities(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getConversations)];
            });
        }); };
        this.getConversationDetails = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var getConversationDetails;
            var _this = this;
            return __generator(this, function (_a) {
                getConversationDetails = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatCommonRoutes.GetConversationDetails, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getConversationDetails)];
            });
        }); };
        this.checkForExistingConversation = function (withUserId) { return __awaiter(_this, void 0, void 0, function () {
            var checkForExistingConversation;
            var _this = this;
            return __generator(this, function (_a) {
                checkForExistingConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.GetExistingConversationWithUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(withUserId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(checkForExistingConversation)];
            });
        }); };
        this.createIndividualConversation = function (withUserId) { return __awaiter(_this, void 0, void 0, function () {
            var createIndividualConversation;
            var _this = this;
            return __generator(this, function (_a) {
                createIndividualConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.CreateIndividualConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            memberId: withUserId
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createIndividualConversation)];
            });
        }); };
        this.createGroupConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createGroupConversation;
            var _this = this;
            return __generator(this, function (_a) {
                createGroupConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, file, groupName, userIds, fileIdResponse, data_1, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.CreateGroupConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                file = params.file, groupName = params.groupName, userIds = params.userIds;
                                if (!file) return [3 /*break*/, 4];
                                return [4 /*yield*/, this.uploadFile(file.path, file.fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            groupName: groupName,
                                            groupImageId: fileIdResponse.value,
                                            memberIds: userIds
                                        }
                                    })];
                            case 2:
                                data_1 = (_b.sent()).data;
                                return [2 /*return*/, ConversationMapper.toDomainEntity(data_1)];
                            case 3: return [2 /*return*/, fileIdResponse.error];
                            case 4: return [4 /*yield*/, this.httpClient({
                                    url: Endpoint,
                                    method: Method,
                                    data: {
                                        groupName: groupName,
                                        groupImageId: null,
                                        memberIds: userIds
                                    }
                                })];
                            case 5:
                                data = (_b.sent()).data;
                                return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createGroupConversation)];
            });
        }); };
        this.createQuickConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createQuickConversation;
            var _this = this;
            return __generator(this, function (_a) {
                createQuickConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, groupName, memberIds, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.CreateQuickConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                groupName = params.groupName, memberIds = params.memberIds;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            groupName: groupName,
                                            memberIds: memberIds
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createQuickConversation)];
            });
        }); };
        this.muteConversation = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var muteConversation;
            var _this = this;
            return __generator(this, function (_a) {
                muteConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.MuteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data.muteUntil];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(muteConversation)];
            });
        }); };
        this.unmuteConversation = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var unmuteConversation;
            var _this = this;
            return __generator(this, function (_a) {
                unmuteConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.UnmuteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(unmuteConversation)];
            });
        }); };
        this.pinConversation = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var pinConversation;
            var _this = this;
            return __generator(this, function (_a) {
                pinConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.PinConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data.pinnedAt];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(pinConversation)];
            });
        }); };
        this.unpinConversation = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var unpinConversation;
            var _this = this;
            return __generator(this, function (_a) {
                unpinConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.UnpinConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(unpinConversation)];
            });
        }); };
        this.deleteConversation = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var deleteConversation;
            var _this = this;
            return __generator(this, function (_a) {
                deleteConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.DeleteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(deleteConversation)];
            });
        }); };
        this.deleteAllConversations = function () { return __awaiter(_this, void 0, void 0, function () {
            var deleteAllConversations;
            var _this = this;
            return __generator(this, function (_a) {
                deleteAllConversations = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.DeleteAllConversations, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(deleteAllConversations)];
            });
        }); };
        this.updateGroupImage = function (conversationId, file) { return __awaiter(_this, void 0, void 0, function () {
            var updateGroupImage;
            var _this = this;
            return __generator(this, function (_a) {
                updateGroupImage = function () { return __awaiter(_this, void 0, void 0, function () {
                    var fileIdResponse, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, this.uploadFile(file.path, file.fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                _a = ChatCommonRoutes.UpdateGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            groupImageId: fileIdResponse.value
                                        }
                                    })];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                            case 3: return [2 /*return*/, false];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateGroupImage)];
            });
        }); };
        this.updateGroupInfo = function (conversationId, groupName) { return __awaiter(_this, void 0, void 0, function () {
            var updateGroupInfo;
            var _this = this;
            return __generator(this, function (_a) {
                updateGroupInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatCommonRoutes.UpdateGroupInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            groupName: groupName
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateGroupInfo)];
            });
        }); };
        this.getConversationUsers = function (_a) {
            var conversationId = _a.conversationId, searchString = _a.searchString, limit = _a.limit, nextCursor = _a.nextCursor, type = _a.type, includeRefIdInSearch = _a.includeRefIdInSearch;
            var getConversationUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                var _a, Endpoint, Method, data;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            _a = ChatCommonRoutes.GetConversationUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                            return [4 /*yield*/, this.httpClient({
                                    url: Endpoint(conversationId),
                                    method: Method,
                                    params: {
                                        type: type || ConversationUserQueryType.Current,
                                        offset: nextCursor,
                                        limit: limit,
                                        search: searchString,
                                        includeRefIdInSearch: includeRefIdInSearch || false
                                    }
                                })];
                        case 1:
                            data = (_b.sent()).data;
                            return [2 /*return*/, __assign(__assign({}, data), { results: UserMapper.toConversableUserDomainEntities(data.results) })];
                    }
                });
            }); };
            return _this.guardWithAuth(getConversationUsers);
        };
        this.getConversationReachableUsers = function (_a) {
            var conversationId = _a.conversationId, searchString = _a.searchString, limit = _a.limit, nextCursor = _a.nextCursor, includeRefIdInSearch = _a.includeRefIdInSearch;
            return __awaiter(_this, void 0, void 0, function () {
                var getConversationReachableUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    getConversationReachableUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.GetConversationReachableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint(conversationId);
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: {
                                                limit: limit,
                                                search: searchString,
                                                offset: nextCursor,
                                                includeRefIdInSearch: includeRefIdInSearch || false
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign(__assign({}, data), { results: UserMapper.toDomainEntities(data.results) })];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getConversationReachableUsers)];
                });
            });
        };
        this.getConversationBasicUsers = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationId, _a, type;
            var _this = this;
            return __generator(this, function (_b) {
                conversationId = params.conversationId, _a = params.type, type = _a === void 0 ? 'current' : _a;
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.GetConversationBasicUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                type: type
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); })];
            });
        }); };
        this.addUsersToConversation = function (conversationId, userIds, force) { return __awaiter(_this, void 0, void 0, function () {
            var addUsersToConversation;
            var _this = this;
            return __generator(this, function (_a) {
                addUsersToConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatCommonRoutes.AddUsersToConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            memberIds: userIds
                                        },
                                        params: {
                                            force: force
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(addUsersToConversation)];
            });
        }); };
        this.removeUsersFromConversation = function (conversationId, userIds, force) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.RemoveUsersFromConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                memberIds: userIds
                                            },
                                            params: {
                                                force: force
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.makeConversationOwner = function (conversationId, userId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.MakeConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId, userId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.removeConversationOwner = function (conversationId, userId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.RemoveConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId, userId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.removeGroupImage = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var removeGroupImage;
            var _this = this;
            return __generator(this, function (_a) {
                removeGroupImage = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatCommonRoutes.RemoveGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(removeGroupImage)];
            });
        }); };
        this.deleteGroupConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var deleteGroupConversation;
            var _this = this;
            return __generator(this, function (_a) {
                deleteGroupConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var conversationId, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                conversationId = params.conversationId;
                                _a = ChatCommonRoutes.DeleteGroupConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(deleteGroupConversation)];
            });
        }); };
        /**
         * @deprecated The method should not be used
         */
        this.getUserConversationReactions = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.GetUserConversationReactions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, ReactionMapper.toDomianEntity(data)];
                            }
                        });
                    }); })];
            });
        }); };
        this.getUserConversationMessagesReactions = function (conversationId, messageIds) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.GetUserConversationMessagesReactions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                messageIds: messageIds
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, ReactionMapper.toDomianEntity(data)];
                            }
                        });
                    }); })];
            });
        }); };
        this.getGroupDefaultImageSet = function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.GetGroupDefaultImageSet, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); })
                    /**
                     * Message APIs
                     */
                ];
            });
        }); };
        /**
         * Message APIs
         */
        this.getMessages = function (conversationId, pivotSequence, after, limit) {
            if (limit === void 0) { limit = 20; }
            return __awaiter(_this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data, lastMessageSequenceInBatch, messageListIds, messageList, data, incomingMessageList, _i, incomingMessageList_1, m;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = ChatClientRoutes.GetMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                        if (!!after) return [3 /*break*/, 2];
                                        return [4 /*yield*/, this.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: {
                                                    beforeSequence: pivotSequence,
                                                    limit: limit
                                                }
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, MessageMapper.toDomainEntities(data)];
                                    case 2:
                                        if (!pivotSequence) {
                                            return [2 /*return*/, []];
                                        }
                                        lastMessageSequenceInBatch = Number.MAX_SAFE_INTEGER;
                                        messageListIds = new Set();
                                        messageList = [];
                                        _b.label = 3;
                                    case 3:
                                        if (!(lastMessageSequenceInBatch > pivotSequence + 1)) return [3 /*break*/, 5];
                                        return [4 /*yield*/, this.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: {
                                                    limit: limit,
                                                    beforeSequence: lastMessageSequenceInBatch
                                                }
                                            })];
                                    case 4:
                                        data = (_b.sent()).data;
                                        incomingMessageList = data;
                                        if (incomingMessageList.length === 0) {
                                            return [3 /*break*/, 5];
                                        }
                                        for (_i = 0, incomingMessageList_1 = incomingMessageList; _i < incomingMessageList_1.length; _i++) {
                                            m = incomingMessageList_1[_i];
                                            lastMessageSequenceInBatch = m.sequence;
                                            if (lastMessageSequenceInBatch <= pivotSequence) {
                                                break;
                                            }
                                            if (!messageListIds.has(m.id)) {
                                                messageListIds.add(m.id);
                                                messageList.push(m);
                                            }
                                        }
                                        return [3 /*break*/, 3];
                                    case 5: return [2 /*return*/, MessageMapper.toDomainEntities(messageList)];
                                }
                            });
                        }); })];
                });
            });
        };
        this.onMessage = function (data) {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
            switch (data.event) {
                case Publisher.EventType.NewMessage: {
                    (_a = _this.eventListener) === null || _a === void 0 ? void 0 : _a.call(_this, {
                        event: data.event,
                        payload: {
                            message: MessageMapper.toDomainEntity(data.payload.message),
                            clientRefId: data.payload.clientRefId
                        }
                    });
                    break;
                }
                case Publisher.EventType.ConversationMuted: {
                    var _o = data.payload, conversationId = _o.conversationId, muteUntil = _o.conversationMuteDto.muteUntil;
                    (_b = _this.eventListener) === null || _b === void 0 ? void 0 : _b.call(_this, {
                        event: data.event,
                        payload: {
                            conversationId: conversationId,
                            muteUntil: new Date(muteUntil)
                        }
                    });
                    break;
                }
                case Publisher.EventType.ConversationPinned: {
                    var _p = data.payload, conversationId = _p.conversationId, pinnedAt = _p.conversationPinDto.pinnedAt;
                    (_c = _this.eventListener) === null || _c === void 0 ? void 0 : _c.call(_this, {
                        event: data.event,
                        payload: {
                            conversationId: conversationId,
                            pinnedAt: new Date(pinnedAt)
                        }
                    });
                    break;
                }
                case Publisher.EventType.ConversationHideAndClear: {
                    var _q = data.payload, conversationId = _q.conversationId, _r = _q.conversationUserHideClearDto, startAfter = _r.startAfter, visible = _r.visible;
                    (_d = _this.eventListener) === null || _d === void 0 ? void 0 : _d.call(_this, {
                        event: data.event,
                        payload: {
                            conversationId: conversationId,
                            visible: visible,
                            startAfter: new Date(startAfter)
                        }
                    });
                    break;
                }
                case Publisher.EventType.ConversationHideAndClearAll: {
                    var _s = data.payload.conversationUserHideClearDto, startAfter = _s.startAfter, visible = _s.visible;
                    (_e = _this.eventListener) === null || _e === void 0 ? void 0 : _e.call(_this, {
                        event: data.event,
                        payload: {
                            visible: visible,
                            startAfter: new Date(startAfter)
                        }
                    });
                    break;
                }
                case Publisher.EventType.ConversationGroupInfoChanged: {
                    (_f = _this.eventListener) === null || _f === void 0 ? void 0 : _f.call(_this, {
                        event: data.event,
                        payload: __assign(__assign({}, data.payload), { groupPicture: data.payload.groupPicture
                                ? MediaMapper.toDomainEntity(data.payload.groupPicture)
                                : null })
                    });
                    break;
                }
                case Publisher.EventType.ConversationQuickGroupChangedToGroup: {
                    (_g = _this.eventListener) === null || _g === void 0 ? void 0 : _g.call(_this, {
                        event: data.event,
                        payload: __assign({}, data.payload)
                    });
                    break;
                }
                case Publisher.EventType.ExternalUserConversationInvitationAccepted: {
                    (_h = _this.eventListener) === null || _h === void 0 ? void 0 : _h.call(_this, {
                        event: data.event,
                        payload: __assign({}, data.payload)
                    });
                    break;
                }
                case Publisher.EventType.ExternalUserNudged: {
                    (_j = _this.eventListener) === null || _j === void 0 ? void 0 : _j.call(_this, {
                        event: data.event,
                        payload: __assign({}, data.payload)
                    });
                    break;
                }
                case Publisher.EventType.ConversationCreated:
                case Publisher.EventType.ConversationUnpinned:
                case Publisher.EventType.ConversationUnmuted:
                case Publisher.EventType.UserInfoChanged:
                case Publisher.EventType.ConversationGroupOwnerAdded:
                case Publisher.EventType.ConversationGroupOwnerRemoved:
                case Publisher.EventType.ConversationGroupMemberAdded:
                case Publisher.EventType.ConversationGroupMemberRemoved:
                case Publisher.EventType.UserReactedToMessage:
                case Publisher.EventType.UserUnReactedToMessage:
                case Publisher.EventType.MessageReactionsUpdate:
                case Publisher.EventType.MessageDeleted:
                case Publisher.EventType.UserEnabled: {
                    (_k = _this.eventListener) === null || _k === void 0 ? void 0 : _k.call(_this, data);
                    break;
                }
                case Publisher.EventType.UserDisabled: {
                    (_l = _this.eventListener) === null || _l === void 0 ? void 0 : _l.call(_this, data);
                    if (!_this.disableUserDisabledHandler) {
                        _this.signOut();
                    }
                    break;
                }
                case Publisher.EventType.UserDeleted: {
                    (_m = _this.eventListener) === null || _m === void 0 ? void 0 : _m.call(_this, data);
                    if (!_this.disableUserDeletedHandler) {
                        _this.signOut();
                    }
                    break;
                }
            }
        };
        this.sendMessage = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var conversationId, clientRefId, _a, Endpoint, Method, payload, _b, file, progress_1, subProgress, fileIdResponse, file, progress_2, subProgress, uri, fileName, thumbnail, thumbnailId, thumbnailIdResponse, videoUploadProgress, fileIdResponse, data;
                        var _c, _d, _e;
                        return __generator(this, function (_f) {
                            switch (_f.label) {
                                case 0:
                                    conversationId = params.conversationId, clientRefId = params.clientRefId;
                                    _a = ChatClientRoutes.SendMessage, Endpoint = _a.Endpoint, Method = _a.Method;
                                    _b = params.type;
                                    switch (_b) {
                                        case MessageType.Text: return [3 /*break*/, 1];
                                        case MessageType.Emoji: return [3 /*break*/, 1];
                                        case MessageType.Gif: return [3 /*break*/, 2];
                                        case MessageType.Audio: return [3 /*break*/, 3];
                                        case MessageType.Image: return [3 /*break*/, 3];
                                        case MessageType.Document: return [3 /*break*/, 3];
                                        case MessageType.Video: return [3 /*break*/, 5];
                                    }
                                    return [3 /*break*/, 9];
                                case 1:
                                    {
                                        payload = {
                                            type: params.type,
                                            text: params.text.trim()
                                        };
                                        return [3 /*break*/, 10];
                                    }
                                case 2:
                                    {
                                        if (!isUrl(params.url)) {
                                            return [2 /*return*/, new SayHey.Error('Invalid URL!', SayHey.ErrorType.InternalErrorType.MissingParameter)];
                                        }
                                        payload = {
                                            type: MessageType.Gif,
                                            refUrl: params.url,
                                            text: (_c = params.text) === null || _c === void 0 ? void 0 : _c.trim()
                                        };
                                        return [3 /*break*/, 10];
                                    }
                                case 3:
                                    file = params.file, progress_1 = params.progress;
                                    subProgress = progress_1
                                        ? function (percentage) {
                                            progress_1(percentage * 0.9);
                                        }
                                        : undefined;
                                    return [4 /*yield*/, this.uploadFile(file.uri, file.fileName, subProgress)];
                                case 4:
                                    fileIdResponse = _f.sent();
                                    if (fileIdResponse.isErr()) {
                                        return [2 /*return*/, fileIdResponse.error];
                                    }
                                    payload = {
                                        type: file.type,
                                        text: (_d = file.text) === null || _d === void 0 ? void 0 : _d.trim(),
                                        fileId: fileIdResponse.value
                                    };
                                    return [3 /*break*/, 10];
                                case 5:
                                    file = params.file, progress_2 = params.progress;
                                    subProgress = progress_2
                                        ? function (percentage) {
                                            progress_2(percentage * 0.4);
                                        }
                                        : undefined;
                                    uri = file.uri, fileName = file.fileName, thumbnail = file.thumbnail;
                                    thumbnailId = '';
                                    if (!thumbnail) return [3 /*break*/, 7];
                                    return [4 /*yield*/, this.uploadFile(thumbnail.uri, thumbnail.name, subProgress)];
                                case 6:
                                    thumbnailIdResponse = _f.sent();
                                    if (thumbnailIdResponse.isErr()) {
                                        return [2 /*return*/, thumbnailIdResponse.error];
                                    }
                                    thumbnailId = thumbnailIdResponse.value;
                                    _f.label = 7;
                                case 7:
                                    videoUploadProgress = progress_2
                                        ? function (percentage) {
                                            progress_2(percentage * 0.6 + 0.4);
                                        }
                                        : undefined;
                                    return [4 /*yield*/, this.uploadFile(uri, fileName, videoUploadProgress, thumbnailId)];
                                case 8:
                                    fileIdResponse = _f.sent();
                                    if (fileIdResponse.isErr()) {
                                        return [2 /*return*/, fileIdResponse.error];
                                    }
                                    payload = {
                                        type: file.type,
                                        text: (_e = file.text) === null || _e === void 0 ? void 0 : _e.trim(),
                                        fileId: fileIdResponse.value
                                    };
                                    return [3 /*break*/, 10];
                                case 9: return [3 /*break*/, 10];
                                case 10: return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: payload,
                                        params: {
                                            clientRefId: clientRefId
                                        }
                                    })];
                                case 11:
                                    data = (_f.sent()).data;
                                    return [2 /*return*/, MessageMapper.toDomainEntity(data)];
                            }
                        });
                    }); })];
            });
        }); };
        this.getGalleryMessages = function (conversationId, beforeSequence, limit) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, messages, mediaList;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.GetMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                galleryOnly: true,
                                                limit: limit,
                                                beforeSequence: beforeSequence
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    messages = MessageMapper.toDomainEntities(data);
                                    mediaList = messages.map(function (message) { return message.file; });
                                    return [2 /*return*/, {
                                            nextCursor: mediaList.length ? messages[messages.length - 1].sequence : -1,
                                            messageList: messages
                                        }];
                            }
                        });
                    }); })];
            });
        }); };
        this.setMessageReaction = function (messageId, type) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.SetMessageReaction, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            data: {
                                                type: type
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.removeMessageReaction = function (messageId, type) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.RemoveMessageReaction, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            data: {
                                                type: type
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.readMessage = function (params) {
            var conversationId = params.conversationId, messageId = params.messageId, sequence = params.sequence, immediateUpdate = params.immediateUpdate;
            var mark = function () {
                var readMessageSequenceMapForConvId = _this.readMessageSequenceMap.get(conversationId);
                if (readMessageSequenceMapForConvId &&
                    Number.isInteger(readMessageSequenceMapForConvId) &&
                    readMessageSequenceMapForConvId >= sequence) {
                    return;
                }
                if (immediateUpdate) {
                    _this.markMessageRead(messageId).then(function (res) {
                        if (res.isErr()) {
                            _this.readMessageSequenceMap.set(conversationId, messageId);
                        }
                    });
                }
                else {
                    _this.readMessageSequenceMap.set(conversationId, messageId);
                }
                _this.readMessageSequenceMap.set(conversationId, sequence);
            };
            if (!_this.isSendingReadReceipt) {
                mark();
            }
            else {
                setTimeout(mark, 500);
            }
        };
        this.markMessageRead = function (messageId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.MarkMessageAsRead, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })
                    /** Push Notification APIs */
                ];
            });
        }); };
        /** Push Notification APIs */
        this.registerForPushNotification = function (instanceId, token) { return __awaiter(_this, void 0, void 0, function () {
            var registerForPushNotification;
            var _this = this;
            return __generator(this, function (_a) {
                if (!instanceId || !token) {
                    return [2 /*return*/, err(new SayHey.Error('Invalid instance id or token string!'))];
                }
                registerForPushNotification = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.RegisterPushNotification, Endpoint = _a.Endpoint, Method = _a.Method;
                                this.instanceIdPushNotification = instanceId;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            instanceId: instanceId,
                                            token: token
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(registerForPushNotification)];
            });
        }); };
        this.unregisterForPushNotification = function () { return __awaiter(_this, void 0, void 0, function () {
            var unregisterForPushNotification;
            var _this = this;
            return __generator(this, function (_a) {
                if (!this.instanceIdPushNotification) {
                    return [2 /*return*/, ok(true)];
                }
                unregisterForPushNotification = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.UnregisterPushNotification, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(this.instanceIdPushNotification),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                this.instanceIdPushNotification = '';
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(unregisterForPushNotification)];
            });
        }); };
        this.getReactedUsers = function (_a) {
            var messageId = _a.messageId, type = _a.type, limit = _a.limit, afterSequence = _a.afterSequence;
            return __awaiter(_this, void 0, void 0, function () {
                var getReactedUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    getReactedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatCommonRoutes.GetReactedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            params: {
                                                limit: limit,
                                                afterSequence: afterSequence,
                                                type: type || ''
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getReactedUsers)];
                });
            });
        };
        this.getAppConfig = function () { return __awaiter(_this, void 0, void 0, function () {
            var getAppConfig;
            var _this = this;
            return __generator(this, function (_a) {
                getAppConfig = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.GetAppConfig, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getAppConfig)];
            });
        }); };
        /* User Onboarding APIs */
        this.checkEmail = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var checkEmail;
            var _this = this;
            return __generator(this, function (_a) {
                checkEmail = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.CheckEmail, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(checkEmail)];
            });
        }); };
        this.setPassword = function (_a) {
            var setPasswordToken = _a.setPasswordToken, password = _a.password;
            return __awaiter(_this, void 0, void 0, function () {
                var setPassword;
                var _this = this;
                return __generator(this, function (_b) {
                    setPassword = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.SetPassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                setPasswordToken: setPasswordToken,
                                                password: password
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(setPassword)];
                });
            });
        };
        /* Request OTP */
        this.requestSignUpOTP = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var requestSignUpOTP;
            var _this = this;
            return __generator(this, function (_a) {
                requestSignUpOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.RequestSignUpOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(requestSignUpOTP)];
            });
        }); };
        this.requestSetPasswordOTP = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var requestSetPasswordOTP;
            var _this = this;
            return __generator(this, function (_a) {
                requestSetPasswordOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.RequestSetPasswordOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(requestSetPasswordOTP)];
            });
        }); };
        this.requestResetPasswordOTP = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var requestResetPasswordOTP;
            var _this = this;
            return __generator(this, function (_a) {
                requestResetPasswordOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.RequestResetPasswordOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(requestResetPasswordOTP)];
            });
        }); };
        this.requestPreverificationOTP = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var requestPreverificationOTP;
            var _this = this;
            return __generator(this, function (_a) {
                requestPreverificationOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.RequestPreverificationOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(requestPreverificationOTP)];
            });
        }); };
        /* Validate OTP */
        this.validateSignUpOTP = function (_a) {
            var otp = _a.otp, email = _a.email;
            return __awaiter(_this, void 0, void 0, function () {
                var validateSignUpOTP;
                var _this = this;
                return __generator(this, function (_b) {
                    validateSignUpOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.ValidateSignUpOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                otp: otp,
                                                email: email
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(validateSignUpOTP)];
                });
            });
        };
        this.validateSetPasswordOTP = function (_a) {
            var otp = _a.otp, email = _a.email;
            return __awaiter(_this, void 0, void 0, function () {
                var validateSetPasswordOTP;
                var _this = this;
                return __generator(this, function (_b) {
                    validateSetPasswordOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.ValidateSetPasswordOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                otp: otp,
                                                email: email
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(validateSetPasswordOTP)];
                });
            });
        };
        this.validateResetPasswordOTP = function (_a) {
            var otp = _a.otp, email = _a.email;
            return __awaiter(_this, void 0, void 0, function () {
                var validateResetPasswordOTP;
                var _this = this;
                return __generator(this, function (_b) {
                    validateResetPasswordOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.ValidateResetPasswordOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                otp: otp,
                                                email: email
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(validateResetPasswordOTP)];
                });
            });
        };
        this.validatePreverificationOTP = function (_a) {
            var otp = _a.otp, email = _a.email;
            return __awaiter(_this, void 0, void 0, function () {
                var validatePreverificationOTP;
                var _this = this;
                return __generator(this, function (_b) {
                    validatePreverificationOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.ValidatePreverificationOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                otp: otp,
                                                email: email
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(validatePreverificationOTP)];
                });
            });
        };
        this.clientSignInWithToken = function (sayheyLoginToken) { return __awaiter(_this, void 0, void 0, function () {
            var clientSignInWithToken;
            var _this = this;
            return __generator(this, function (_a) {
                clientSignInWithToken = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.ClientSignInWithToken, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            sayheyLoginToken: sayheyLoginToken
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(clientSignInWithToken)];
            });
        }); };
        /* Message Flagging */
        this.reportFlaggedMessage = function (_a) {
            var messageId = _a.messageId, reason = _a.reason;
            return __awaiter(_this, void 0, void 0, function () {
                var reportFlaggedMessage;
                var _this = this;
                return __generator(this, function (_b) {
                    reportFlaggedMessage = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.CreateFlaggedMessage, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                messageId: messageId,
                                                reason: reason
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(reportFlaggedMessage)];
                });
            });
        };
        /* SMS */
        this.addToContact = function (_a) {
            var externalUserId = _a.externalUserId;
            return __awaiter(_this, void 0, void 0, function () {
                var addToContact;
                var _this = this;
                return __generator(this, function (_b) {
                    addToContact = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.AddToContact, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                externalUserId: externalUserId
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(addToContact)];
                });
            });
        };
        this.getInternalUserContactsBatch = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var getInternalUserContactsBatch;
            var _this = this;
            return __generator(this, function (_a) {
                getInternalUserContactsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.GetInternalUserContactsBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        params: params
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getInternalUserContactsBatch)];
            });
        }); };
        this.findExternalUserByPhone = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var findExternalUserByPhone;
            var _this = this;
            return __generator(this, function (_a) {
                findExternalUserByPhone = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.FindExternalUserByPhone, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        params: params
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(findExternalUserByPhone)];
            });
        }); };
        this.requestSignInExternalOTP = function (_a) {
            var phone = _a.phone;
            return __awaiter(_this, void 0, void 0, function () {
                var requestSignInExternalOTP;
                var _this = this;
                return __generator(this, function (_b) {
                    requestSignInExternalOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.RequestSignInExternalOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                phone: phone
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(requestSignInExternalOTP)];
                });
            });
        };
        this.signInExternal = function (_a) {
            var otp = _a.otp, phone = _a.phone;
            return __awaiter(_this, void 0, void 0, function () {
                var signInExternal;
                var _this = this;
                return __generator(this, function (_b) {
                    signInExternal = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.SignInExternal, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                otp: otp,
                                                phone: phone
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                    return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                                case 2:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(signInExternal)];
                });
            });
        };
        this.signInExternalByUserId = function (_a) {
            var id = _a.id, otp = _a.otp;
            return __awaiter(_this, void 0, void 0, function () {
                var signInExternalByUserId;
                var _this = this;
                return __generator(this, function (_b) {
                    signInExternalByUserId = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = ChatClientRoutes.SignInExternalByUserId, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(id),
                                            method: Method,
                                            data: {
                                                otp: otp
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                    return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                                case 2:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(signInExternalByUserId)];
                });
            });
        };
        this.createNewContact = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createNewContact;
            var _this = this;
            return __generator(this, function (_a) {
                createNewContact = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.CreateNewContact, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: params
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createNewContact)];
            });
        }); };
        this.acceptTermsAndPrivacy = function () { return __awaiter(_this, void 0, void 0, function () {
            var acceptTermsAndPrivacy;
            var _this = this;
            return __generator(this, function (_a) {
                acceptTermsAndPrivacy = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.AcceptTermsAndPrivacy, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(acceptTermsAndPrivacy)];
            });
        }); };
        this.getTermsAndPrivacy = function () { return __awaiter(_this, void 0, void 0, function () {
            var getTermsAndPrivacy;
            var _this = this;
            return __generator(this, function (_a) {
                getTermsAndPrivacy = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.GetTermsAndPrivacy, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getTermsAndPrivacy)];
            });
        }); };
        this.getExistingSmsConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var externalUserId, getExistingSmsConversation;
            var _this = this;
            return __generator(this, function (_a) {
                externalUserId = params.externalUserId;
                getExistingSmsConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.GetExistingSmsConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        params: {
                                            externalUserId: externalUserId
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                if (!data) {
                                    return [2 /*return*/, null];
                                }
                                return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getExistingSmsConversation)];
            });
        }); };
        this.checkSmsConversationInvitation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationId, checkSmsConversationInvitation;
            var _this = this;
            return __generator(this, function (_a) {
                conversationId = params.conversationId;
                checkSmsConversationInvitation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.CheckSmsConversationInvitation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(checkSmsConversationInvitation)];
            });
        }); };
        this.acceptSmsConversationInvitation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationId, acceptSmsConversationInvitation;
            var _this = this;
            return __generator(this, function (_a) {
                conversationId = params.conversationId;
                acceptSmsConversationInvitation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.AcceptSmsConversationInvitation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(acceptSmsConversationInvitation)];
            });
        }); };
        this.resendSmsConversationInvitation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationId, resendSmsConversationInvitation;
            var _this = this;
            return __generator(this, function (_a) {
                conversationId = params.conversationId;
                resendSmsConversationInvitation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.ResendSmsConversationInvitation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(resendSmsConversationInvitation)];
            });
        }); };
        this.createSmsConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createSmsConversation;
            var _this = this;
            return __generator(this, function (_a) {
                createSmsConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, memberId, groupImageId, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.CreateSmsConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                memberId = params.memberId, groupImageId = params.groupImageId;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            memberId: memberId,
                                            groupImageId: groupImageId
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createSmsConversation)];
            });
        }); };
        this.optToReceiveSms = function () { return __awaiter(_this, void 0, void 0, function () {
            var optToReceiveSms;
            var _this = this;
            return __generator(this, function (_a) {
                optToReceiveSms = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.OptToReceiveSms, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(optToReceiveSms)];
            });
        }); };
        this.optNotToReceiveSms = function () { return __awaiter(_this, void 0, void 0, function () {
            var optNotToReceiveSms;
            var _this = this;
            return __generator(this, function (_a) {
                optNotToReceiveSms = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.OptNotToReceiveSms, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(optNotToReceiveSms)];
            });
        }); };
        this.getOptToReceiveSmsStatus = function () { return __awaiter(_this, void 0, void 0, function () {
            var getOptToReceiveSmsStatus;
            var _this = this;
            return __generator(this, function (_a) {
                getOptToReceiveSmsStatus = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.GetOptToReceiveSmsStatus, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getOptToReceiveSmsStatus)];
            });
        }); };
        this.nudgeExternalUserToConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationId, nudgeExternalUserToConversation;
            var _this = this;
            return __generator(this, function (_a) {
                conversationId = params.conversationId;
                nudgeExternalUserToConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = ChatClientRoutes.NudgeExternalUserToConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(nudgeExternalUserToConversation)];
            });
        }); };
        this.publisherKitClient = new PublisherKitClient();
    }
    /**
     * Setup methods
     */
    ChatClientKit.prototype.initialize = function (params) {
        var _this = this;
        var sayheyAppId = params.sayheyAppId, sayheyApiKey = params.sayheyApiKey, publisherApiKey = params.publisherApiKey, publisherAppId = params.publisherAppId, sayheyUrl = params.sayheyUrl, publisherUrl = params.publisherUrl, messageUpdateInterval = params.messageUpdateInterval, disableAutoRefreshToken = params.disableAutoRefreshToken, disableUserDeletedHandler = params.disableUserDeletedHandler, disableUserDisabledHandler = params.disableUserDisabledHandler, apiVersion = params.apiVersion;
        this.serverUrl = sayheyUrl;
        this._apiVersion = apiVersion || API_VERSION;
        var baseURL = this.serverUrl + "/" + this._apiVersion + "/apps/" + sayheyAppId;
        this.apiKey = sayheyApiKey;
        this.appId = sayheyAppId;
        this.disableAutoRefreshToken = disableAutoRefreshToken;
        this.disableUserDisabledHandler = disableUserDisabledHandler;
        this.disableUserDeletedHandler = disableUserDeletedHandler;
        this.httpClient = httpClient.createWithoutAuth({
            apiKey: this.apiKey,
            baseURL: baseURL
        });
        this.publisherKitClient.initialize({
            appId: publisherAppId,
            clientApiKey: publisherApiKey,
            publisherUrl: publisherUrl
        });
        if (this.timerHandle !== null) {
            clearInterval(this.timerHandle);
        }
        this.timerHandle = setInterval(function () {
            _this.isSendingReadReceipt = true;
            var copy = new Map(Object.entries(_this.readMessageMap));
            _this.readMessageMap = {};
            _this.isSendingReadReceipt = false;
            var conversationIds = Object.keys(copy);
            for (var i = 0; i < conversationIds.length; i++) {
                var conversationId = conversationIds[+i];
                var messageId = copy.get(conversationId);
                messageId && _this.markMessageRead(messageId);
            }
        }, messageUpdateInterval || MESSAGE_READ_UPDATE_INTERVAL);
        this._initialized = true;
    };
    return ChatClientKit;
}());

export { APISignature, ChatAdminKit, ChatClientKit, ConversationGroupType, ConversationType, ConversationUserQueryType, DepartmentSortFields, DivisionSortFields, FileType, FlaggedMessagePriorityType, FlaggedMessageType, HighLightType, HttpMethod, ImageSizes, KeyWordPriorityTypes, MessageType, Publisher, QueryCondition, ReactionType, ReactionsObject, ResolutionStatusType, SayHey, SortOrderType, SystemMessageType, TimeFrameType, TokenType, UnmanagedUsersSortFields, UnresolvedMsgScopeType, UserAdminSortFields, UserAdminStatusType, UserManagementSortFields, UserManagementType, UserMovementSelected, UserOnboardingJobRecordStatus, UserOnboardingJobStatus, UserScope, UserStatusType, UserTypes, isSingleEmoji, parseText, tempDefaultColumn };
//# sourceMappingURL=sayhey-chat-kit-client.es5.js.map
