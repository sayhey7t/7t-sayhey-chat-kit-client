import { HttpMethod } from './types';
export declare const API_VERSION = "v1";
export declare const ApiKeyName = "x-sayhey-client-api-key";
export declare const MESSAGE_READ_UPDATE_INTERVAL = 5000;
export declare const ChatClientRoutes: {
    SignIn: {
        Endpoint: string;
        Method: HttpMethod;
    };
    SignUp: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ChangePassword: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ForgotPassword: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ResetPassword: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetExistingConversationWithUser: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    GetExistingIndvidualOrQuickConversation: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetConversations: {
        Endpoint: string;
        Method: HttpMethod;
    };
    CreateIndividualConversation: {
        Endpoint: string;
        Method: HttpMethod;
    };
    CreateGroupConversation: {
        Endpoint: string;
        Method: HttpMethod;
    };
    CreateQuickConversation: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetMessages: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    SendMessage: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    MuteConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    UnmuteConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    PinConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    UnpinConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    DeleteConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    DeleteAllConversations: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UpdateUserProfileImage: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UpdateUserProfileInfo: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetConversationReachableUsers: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetUserConversationReactions: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetUserConversationMessagesReactions: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    SetMessageReaction: {
        Endpoint: (messageId: string) => string;
        Method: HttpMethod;
    };
    RemoveMessageReaction: {
        Endpoint: (messageId: string) => string;
        Method: HttpMethod;
    };
    MuteAllNotifications: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UnmuteAllNotifications: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUserProfile: {
        Endpoint: string;
        Method: HttpMethod;
    };
    MarkMessageAsRead: {
        Endpoint: (messageId: string) => string;
        Method: HttpMethod;
    };
    GetConversationBasicUsers: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    RegisterPushNotification: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UnregisterPushNotification: {
        Endpoint: (instanceId: string) => string;
        Method: HttpMethod;
    };
    SetPassword: {
        Endpoint: string;
        Method: HttpMethod;
    };
    CheckEmail: {
        Endpoint: string;
        Method: HttpMethod;
    };
    RequestSignUpOTP: {
        Endpoint: string;
        Method: HttpMethod;
    };
    RequestSetPasswordOTP: {
        Endpoint: string;
        Method: HttpMethod;
    };
    RequestResetPasswordOTP: {
        Endpoint: string;
        Method: HttpMethod;
    };
    RequestPreverificationOTP: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ValidateSignUpOTP: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ValidateSetPasswordOTP: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ValidateResetPasswordOTP: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ValidatePreverificationOTP: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetAppConfig: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ClientSignInWithToken: {
        Endpoint: string;
        Method: HttpMethod;
    };
    CreateFlaggedMessage: {
        Endpoint: string;
        Method: HttpMethod;
    };
    AddToContact: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetInternalUserContactsBatch: {
        Endpoint: string;
        Method: HttpMethod;
    };
    FindExternalUserByPhone: {
        Endpoint: string;
        Method: HttpMethod;
    };
    RequestSignInExternalOTP: {
        Endpoint: string;
        Method: HttpMethod;
    };
    SignInExternal: {
        Endpoint: string;
        Method: HttpMethod;
    };
    SignInExternalByUserId: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
    CreateNewContact: {
        Endpoint: string;
        Method: HttpMethod;
    };
    AcceptTermsAndPrivacy: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetTermsAndPrivacy: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetExistingSmsConversation: {
        Endpoint: string;
        Method: HttpMethod;
    };
    CheckSmsConversationInvitation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    AcceptSmsConversationInvitation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    ResendSmsConversationInvitation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    CreateSmsConversation: {
        Endpoint: string;
        Method: HttpMethod;
    };
    OptToReceiveSms: {
        Endpoint: string;
        Method: HttpMethod;
    };
    OptNotToReceiveSms: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetOptToReceiveSmsStatus: {
        Endpoint: string;
        Method: HttpMethod;
    };
    NudgeExternalUserToConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
};
export declare const ChatAdminRoutes: {
    Invite: {
        Endpoint: string;
        Method: HttpMethod;
    };
    SignUp: {
        Endpoint: string;
        Method: HttpMethod;
    };
    SignIn: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ReInvite: {
        Endpoint: (userAdminId: string) => string;
        Method: HttpMethod;
    };
    DisableAdmin: {
        Endpoint: (userAdminId: string) => string;
        Method: HttpMethod;
    };
    EnableAdmin: {
        Endpoint: (userAdminId: string) => string;
        Method: HttpMethod;
    };
    ForgotPassword: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ResetPassword: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUserAdmins: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UpdateUserAdminInfo: {
        Endpoint: (userAdminId: string) => string;
        Method: HttpMethod;
    };
    UpdateUserAdminRole: {
        Endpoint: (userAdminId: string) => string;
        Method: HttpMethod;
    };
    UpdateUserAdminOnLeave: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUserAdminSelfInfo: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UpdateUserAdminSelfInfo: {
        Endpoint: string;
        Method: HttpMethod;
    };
    AssignManagedUsers: {
        Endpoint: string;
        Method: HttpMethod;
    };
    MoveAllManagedUsers: {
        Endpoint: string;
        Method: HttpMethod;
    };
    MoveBackAllManagedUsers: {
        Endpoint: (userAdminId: string) => string;
        Method: HttpMethod;
    };
    GetManagedUsers: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetManagedUsersCount: {
        Endpoint: string;
        Method: HttpMethod;
    };
    MoveSelectedUsers: {
        Endpoint: string;
        Method: HttpMethod;
    };
    RegisterUser: {
        Endpoint: string;
        Method: HttpMethod;
    };
    DisableUser: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    EnableUser: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    UpdateUser: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    GetUnmanagedUsers: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUnmanagedUsersCount: {
        Endpoint: string;
        Method: HttpMethod;
    };
    CreateDepartment: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetDepartments: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UpdateDepartments: {
        Endpoint: (departmentId: string) => string;
        Method: HttpMethod;
    };
    CreateDivisions: {
        Endpoint: (departmentId: string) => string;
        Method: HttpMethod;
    };
    GetDivisions: {
        Endpoint: (departmentId: string) => string;
        Method: HttpMethod;
    };
    UpdateDivisions: {
        Endpoint: ({ departmentId, divisionId }: {
            departmentId: string;
            divisionId: string;
        }) => string;
        Method: HttpMethod;
    };
    CreateKeyword: {
        Endpoint: string;
        Method: HttpMethod;
    };
    CreateKeywordBulk: {
        Endpoint: string;
        Method: HttpMethod;
    };
    DeleteKeyword: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
    UpdateKeyword: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
    GetKeywordsBatch: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetMessagesBeforeSequence: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetMessagesAfterSequence: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetMessageContext: {
        Endpoint: (messageId: string) => string;
        Method: HttpMethod;
    };
    GetAuditMessagesBatch: {
        Endpoint: string;
        Method: HttpMethod;
    };
    DownloadAuditMessages: {
        Endpoint: string;
        Method: HttpMethod;
    };
    CreateUpdateUserAdminSearch: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUserAdminSearch: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUserBasicInfoBatch: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetFlaggedMessagesBatch: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetFlaggedMessagesCount: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ResolveFlaggedMessages: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
    GetFlaggedMessageDetails: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
    GetFlaggedMessageUserReports: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
    GetFlaggedMessageKeywordScans: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
    GetFlaggedMessageResolutions: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
    GetFlaggedMessageResolutionUserReports: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
    GetFlaggedMessageResolutionKeywordScans: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
    CreateGroupConversation: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UpdateGroupConversationOwner: {
        Endpoint: (conversationId: string, userAdminId: string) => string;
        Method: HttpMethod;
    };
    GetUserAdminGroups: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetSpaceGroupDivisions: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    CreateConversationSpaceGroupDivision: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    DeleteConversationSpaceGroupDivision: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetTotalUsersCount: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetMyUsersCount: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetResolvedFlaggedMessagesCount: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetSentMessagesCountDaily: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetSentMessagesCountTotal: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUnresolvedFlaggedMessagesCount: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUnresolvedFlaggedMessageCountsDaily: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetEligibleRolesToInviteAdmin: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ProcessUserOnboardingJobUpload: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUserOnboardingJobBatch: {
        Endpoint: string;
        Method: HttpMethod;
    };
    DownloadSampleFile: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetAllErroredUserOnboardingJobRecords: {
        Endpoint: (id: string) => string;
        Method: HttpMethod;
    };
};
export declare const ChatCommonRoutes: {
    UploadFile: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetReactedUsers: {
        Endpoint: (messageId: string) => string;
        Method: HttpMethod;
    };
    GetReachableUsers: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetConversationUsers: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    AddUsersToConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    RemoveUsersFromConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    UpdateGroupImage: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    RemoveGroupImage: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetGroupDefaultImageSet: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UpdateGroupInfo: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    MakeConversationOwner: {
        Endpoint: (conversationId: string, userId: string) => string;
        Method: HttpMethod;
    };
    RemoveConversationOwner: {
        Endpoint: (conversationId: string, userId: string) => string;
        Method: HttpMethod;
    };
    GetConversationDetails: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    DeleteGroupConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
};
