import { AuditMessageBatchDTO, AuditMessageDomainEntityBatchDTO, AuditMessageDomainEntityDTO, AuditMessageDTO, ConversationDomainEntity, ConversationDTO, ConversationReactionsDomainEntity, ConversationReactionsDTO, ConversationUserDomainEntity, ConversationUserDomainForUserAdminEntity, MediaDomainEntity, MediaDTO, MessageDomainEntity, MessageDTO, PeekMessageDomainEntity, PeekMessageDTO, SentMessageDomainEntity, SentMessageDTO, SystemMessageDomainEntity, SystemMessageDTO, UserDomainEntity, UserDTO, UserForUserAdminDomainEntity, UserForUserAdminDTO, UserOfConversationDTO, UserOfConversationForUserAdminDTO, UserSelfDomainEntity, UserSelfDTO } from './types';
export declare class ConversationMapper {
    static toDomainEntity(conversation: ConversationDTO): ConversationDomainEntity;
    static toDomainEntities(conversations: ConversationDTO[]): ConversationDomainEntity[];
}
export declare class SentMessageMapper {
    static toDomainEntity(message: SentMessageDTO): Pick<MessageDomainEntity, "id" | "type" | "createdAt" | "sender" | "file" | "deletedAt" | "conversationId" | "text" | "refUrl" | "sequence" | "reactions" | "sent" | "parsedText" | "previewUrl">;
    static toDomainEntities(messages: SentMessageDTO[]): SentMessageDomainEntity[];
}
export declare class MessageMapper {
    static toDomainEntity(message: MessageDTO): MessageDomainEntity;
    static toDomainEntities(messages: MessageDTO[]): MessageDomainEntity[];
    static toPeekDomainEntities(messages: PeekMessageDTO[]): PeekMessageDomainEntity[];
}
export declare class UserMapper {
    static toDomainEntity(user: UserDTO): UserDomainEntity;
    static toUserForUserAdminDomainEntity(user: UserForUserAdminDTO): UserForUserAdminDomainEntity;
    static toConversableUserDomainEntity(user: UserOfConversationDTO): ConversationUserDomainEntity;
    static toConversableUserDomainForUserAdminEntity(user: UserOfConversationForUserAdminDTO): ConversationUserDomainForUserAdminEntity;
    static toDomainEntities(users: UserDTO[]): UserDomainEntity[];
    static toUserForUserDomainEntities(users: UserForUserAdminDTO[]): UserForUserAdminDomainEntity[];
    static toConversableUserDomainEntities(users: UserOfConversationDTO[]): ConversationUserDomainEntity[];
    static toConversableUserDomainForUserAdminEntities(users: UserOfConversationForUserAdminDTO[]): ConversationUserDomainForUserAdminEntity[];
    static toSelfDomainEntity(user: UserSelfDTO): UserSelfDomainEntity;
}
export declare class MediaMapper {
    static toDomainEntity(media: MediaDTO): MediaDomainEntity;
}
export declare class SystemMessageMapper {
    static toDomainEntity(systemMessage: SystemMessageDTO): SystemMessageDomainEntity;
}
export declare class ReactionMapper {
    static toDomianEntity(reactions: ConversationReactionsDTO[]): ConversationReactionsDomainEntity;
}
export declare class AuditMessageMapper {
    static toAuditMessageDomainEntityBatchDTO(auditMessageBatchDTO: AuditMessageBatchDTO): AuditMessageDomainEntityBatchDTO;
    static toAuditMessageDomainEntityDTO(auditMessageDTO: AuditMessageDTO): AuditMessageDomainEntityDTO;
}
