import PublisherKitClient from '@7t/publisher-kit-client';
import { AxiosInstance } from 'axios';
import { Result } from 'neverthrow';
import { ApiKeyName } from './constants';
import { AppConfigDTO, AuthResponse, ConversableUsersBatchResult, ConversationDomainEntity, ConversationGroupType, ConversationReachableUsersQuery, ConversationReactionsDomainEntity, ConversationType, ConversationUserQuery, ConversationUsersBasicDTO, ConversationUsersBatch, CreateNewContact, DefaultGroupImageUrlSet, ExternalUserContactDTO, ExternalUserOptToReceiveSmsDTO, ExternalUserTermsAndPrivacyVersionDTO, FileUpload, FindByPhoneQueryParams, FindSmsConversation, GalleryPayload, ImageSizes, InternalUserContactsBatchDTO, MessageDomainEntity, PhoneWithCode, ReachableUsersQuery, ReactedUserDTO, ReactionType, SayHey, SearchPaginateParams, SendMessageParams, SmsConversationInvitationDTO, SmsConversationInvitationUserDTO, UserSelfDomainEntity, ValidateResetPasswordOTPDTO, ValidateSetPasswordOTPDTO } from './types';
export declare class ChatClientKit {
    publisherKitClient: PublisherKitClient;
    accessToken: string;
    publisherToken: string;
    refreshToken: string;
    instanceIdPushNotification: string;
    private _apiVersion;
    httpClient: AxiosInstance;
    appId: string;
    apiKey: string;
    serverUrl: string;
    authDetailChangeListener?: (res: AuthResponse) => Promise<void> | void;
    eventListener?: (event: SayHey.Event) => void;
    /** NOTE: Will be called when refresh token exchange session API call has failed */
    sessionExpiredListener?: () => void;
    private _initialized;
    private _subscribed;
    private _isUserAuthenticated;
    private readMessageMap;
    readMessageSequenceMap: Map<string, number | string>;
    private isSendingReadReceipt;
    private timerHandle;
    private exchangeTokenPromise;
    private disableAutoRefreshToken?;
    private disableUserDisabledHandler?;
    private disableUserDeletedHandler?;
    private loginId;
    constructor();
    isUserAuthenticated: () => boolean;
    /**
     * Setup methods
     */
    initialize(params: {
        sayheyAppId: string;
        sayheyApiKey: string;
        publisherAppId: string;
        publisherApiKey: string;
        sayheyUrl: string;
        publisherUrl: string;
        apiVersion?: 'v1' | 'v2' | 'v3' | 'v4';
        messageUpdateInterval?: number;
        disableAutoRefreshToken?: boolean;
        disableUserDisabledHandler?: boolean;
        disableUserDeletedHandler?: boolean;
    }): void;
    addListeners: (listeners: {
        onAuthDetailChange: (res: AuthResponse) => Promise<void> | void;
        onEvent: (event: SayHey.Event) => void;
        onSocketConnect?: (() => void) | undefined;
        onSocketDisconnect?: ((reason: string) => void) | undefined;
        onSocketError?: ((error: Error) => void) | undefined;
        onSocketReconnect?: ((attemptNumber: number) => void) | undefined;
        onSessionExpired?: (() => void) | undefined;
    }) => Result<boolean, SayHey.Error>;
    /**
     * Helper methods
     */
    /**
     *
     * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
     */
    private guard;
    exchangeTokenOnce: () => Promise<Result<boolean, SayHey.Error>>;
    isSocketConnected: () => boolean;
    /**
     *
     * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
     */
    private guardWithAuth;
    /**
     *
     * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
     * @param publisherToken - publisher token returned from SayHey that will be used to verify Publisher access
     * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
     */
    private updateAuthDetails;
    /**
     *
     * @param urlPath - url path returned from SayHey
     */
    getMediaSourceDetails: (urlPath: string) => {
        uri: string;
        method: string;
        headers: {
            "x-sayhey-client-api-key": string;
            Authorization: string;
            Accept: string;
        };
    };
    getMediaDirectUrl: (params: {
        urlPath: string;
        size?: ImageSizes;
    }) => Promise<Result<string, SayHey.Error>>;
    private uploadFile;
    uploadImage: (fileUri: string | Blob, fileName: string) => Promise<Result<string, SayHey.Error>>;
    disconnectPublisher: () => Promise<void>;
    reconnectPublisher: () => Promise<void>;
    /**
     * Token APIs
     */
    /**
     *
     * @param email - user email for SayHey
     * @param password - user password for SayHey
     *
     */
    signIn: (email: string, password: string) => Promise<Result<boolean, SayHey.Error>>;
    signInWithToken: (accessToken: string, publisherToken: string, refreshToken: string) => Promise<Result<boolean, SayHey.Error>>;
    signUp: (email: string, password: string, firstName: string, lastName: string) => Promise<Result<boolean, SayHey.Error>>;
    signOut: () => Promise<void>;
    exchangeToken: () => Promise<Result<boolean, SayHey.Error>>;
    changePassword: (currentPassword: string, newPassword: string) => Promise<Result<boolean, SayHey.Error>>;
    forgotPassword: (email: string) => Promise<Result<boolean, SayHey.Error>>;
    resetPassword: ({ password, resetPasswordToken }: {
        password: string;
        resetPasswordToken: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    /**
     * User APIs
     */
    getReachableUsers: ({ searchString, limit, nextCursor, includeRefIdInSearch }?: ReachableUsersQuery) => Promise<Result<ConversableUsersBatchResult, SayHey.Error>>;
    updateUserProfileImage: (fileUri: string, fileName: string) => Promise<Result<boolean, SayHey.Error>>;
    updateUserProfileInfo: (options: {
        firstName?: string;
        lastName?: string;
        email?: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    getUserDetails: () => Promise<Result<UserSelfDomainEntity, SayHey.Error>>;
    muteAllNotifications: () => Promise<Result<boolean, SayHey.Error>>;
    unmuteAllNotifications: () => Promise<Result<boolean, SayHey.Error>>;
    /**
     * Conversation APIs
     */
    getExistingIndvidualOrQuickConversation: (memberIds: string[]) => Promise<Result<ConversationDomainEntity | null, SayHey.Error>>;
    getConversations: (params: {
        conversationType?: ConversationType | undefined;
        conversationGroupType?: ConversationGroupType | undefined;
    }) => Promise<Result<ConversationDomainEntity[], SayHey.Error>>;
    getConversationDetails: (conversationId: string) => Promise<Result<ConversationDomainEntity, SayHey.Error>>;
    checkForExistingConversation: (withUserId: string) => Promise<Result<ConversationDomainEntity, SayHey.Error>>;
    createIndividualConversation: (withUserId: string) => Promise<Result<ConversationDomainEntity, SayHey.Error>>;
    createGroupConversation: (params: {
        groupName: string;
        file?: FileUpload;
        userIds: string[];
    }) => Promise<Result<ConversationDomainEntity, SayHey.Error>>;
    createQuickConversation: (params: {
        groupName: string;
        memberIds: string[];
    }) => Promise<Result<ConversationDomainEntity, SayHey.Error>>;
    muteConversation: (conversationId: string) => Promise<Result<string, SayHey.Error>>;
    unmuteConversation: (conversationId: string) => Promise<Result<boolean, SayHey.Error>>;
    pinConversation: (conversationId: string) => Promise<Result<string, SayHey.Error>>;
    unpinConversation: (conversationId: string) => Promise<Result<boolean, SayHey.Error>>;
    deleteConversation: (conversationId: string) => Promise<Result<boolean, SayHey.Error>>;
    deleteAllConversations: () => Promise<Result<boolean, SayHey.Error>>;
    updateGroupImage: (conversationId: string, file: FileUpload) => Promise<Result<boolean, SayHey.Error>>;
    updateGroupInfo: (conversationId: string, groupName: string) => Promise<Result<boolean, SayHey.Error>>;
    getConversationUsers: ({ conversationId, searchString, limit, nextCursor, type, includeRefIdInSearch }: ConversationUserQuery) => Promise<Result<ConversationUsersBatch, SayHey.Error>>;
    getConversationReachableUsers: ({ conversationId, searchString, limit, nextCursor, includeRefIdInSearch }: ConversationReachableUsersQuery) => Promise<Result<ConversableUsersBatchResult, SayHey.Error>>;
    getConversationBasicUsers: (params: {
        conversationId: string;
        type?: "all" | "current" | "default" | undefined;
    }) => Promise<Result<ConversationUsersBasicDTO[], SayHey.Error>>;
    addUsersToConversation: (conversationId: string, userIds: string[], force?: boolean | undefined) => Promise<Result<boolean, SayHey.Error>>;
    removeUsersFromConversation: (conversationId: string, userIds: string[], force?: boolean | undefined) => Promise<Result<boolean, SayHey.Error>>;
    makeConversationOwner: (conversationId: string, userId: string) => Promise<Result<boolean, SayHey.Error>>;
    removeConversationOwner: (conversationId: string, userId: string) => Promise<Result<boolean, SayHey.Error>>;
    removeGroupImage: (conversationId: string) => Promise<Result<boolean, SayHey.Error>>;
    deleteGroupConversation: (params: {
        conversationId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    /**
     * @deprecated The method should not be used
     */
    getUserConversationReactions: (conversationId: string) => Promise<Result<ConversationReactionsDomainEntity, SayHey.Error>>;
    getUserConversationMessagesReactions: (conversationId: string, messageIds: string[]) => Promise<Result<ConversationReactionsDomainEntity, SayHey.Error>>;
    getGroupDefaultImageSet: () => Promise<Result<DefaultGroupImageUrlSet[], SayHey.Error>>;
    /**
     * Message APIs
     */
    getMessages: (conversationId: string, pivotSequence?: number | undefined, after?: boolean | undefined, limit?: number) => Promise<Result<MessageDomainEntity[], SayHey.Error>>;
    private onMessage;
    sendMessage: (params: SendMessageParams) => Promise<Result<MessageDomainEntity, SayHey.Error>>;
    getGalleryMessages: (conversationId: string, beforeSequence?: number | undefined, limit?: number | undefined) => Promise<Result<GalleryPayload, SayHey.Error>>;
    setMessageReaction: (messageId: string, type: ReactionType) => Promise<Result<boolean, SayHey.Error>>;
    removeMessageReaction: (messageId: string, type: ReactionType) => Promise<Result<boolean, SayHey.Error>>;
    readMessage: (params: {
        conversationId: string;
        messageId: string;
        sequence: number;
        immediateUpdate?: boolean | undefined;
    }) => void;
    private markMessageRead;
    /** Push Notification APIs */
    registerForPushNotification: (instanceId: string, token: string) => Promise<Result<boolean, SayHey.Error>>;
    unregisterForPushNotification: () => Promise<Result<boolean, SayHey.Error>>;
    getReactedUsers: ({ messageId, type, limit, afterSequence }: {
        messageId: string;
        type?: ReactionType | undefined;
        limit: number;
        afterSequence: number | null;
    }) => Promise<Result<ReactedUserDTO[], SayHey.Error>>;
    getAppConfig: () => Promise<Result<AppConfigDTO, SayHey.Error>>;
    checkEmail: (email: string) => Promise<Result<boolean, SayHey.Error>>;
    setPassword: ({ setPasswordToken, password }: {
        setPasswordToken: string;
        password: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    requestSignUpOTP: (email: string) => Promise<Result<boolean, SayHey.Error>>;
    requestSetPasswordOTP: (email: string) => Promise<Result<boolean, SayHey.Error>>;
    requestResetPasswordOTP: (email: string) => Promise<Result<boolean, SayHey.Error>>;
    requestPreverificationOTP: (email: string) => Promise<Result<boolean, SayHey.Error>>;
    validateSignUpOTP: ({ otp, email }: {
        otp: string;
        email: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    validateSetPasswordOTP: ({ otp, email }: {
        otp: string;
        email: string;
    }) => Promise<Result<ValidateSetPasswordOTPDTO, SayHey.Error>>;
    validateResetPasswordOTP: ({ otp, email }: {
        otp: string;
        email: string;
    }) => Promise<Result<ValidateResetPasswordOTPDTO, SayHey.Error>>;
    validatePreverificationOTP: ({ otp, email }: {
        otp: string;
        email: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    clientSignInWithToken: (sayheyLoginToken: string) => Promise<Result<boolean, SayHey.Error>>;
    reportFlaggedMessage: ({ messageId, reason }: {
        messageId: string;
        reason: string;
    }) => Promise<Result<{
        id: string;
    }, SayHey.Error>>;
    addToContact: ({ externalUserId }: {
        externalUserId: string;
    }) => Promise<Result<{
        internalUserId: string;
        externalUserId: string;
    }, SayHey.Error>>;
    getInternalUserContactsBatch: (params: SearchPaginateParams) => Promise<Result<InternalUserContactsBatchDTO, SayHey.Error>>;
    findExternalUserByPhone: (params: FindByPhoneQueryParams) => Promise<Result<ExternalUserContactDTO, SayHey.Error>>;
    requestSignInExternalOTP: ({ phone }: {
        phone: PhoneWithCode;
    }) => Promise<Result<boolean, SayHey.Error>>;
    signInExternal: ({ otp, phone }: {
        otp: string;
        phone: PhoneWithCode;
    }) => Promise<Result<boolean, SayHey.Error>>;
    signInExternalByUserId: ({ id, otp }: {
        id: string;
        otp: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    createNewContact: (params: CreateNewContact) => Promise<Result<{
        id: string;
    }, SayHey.Error>>;
    acceptTermsAndPrivacy: () => Promise<Result<boolean, SayHey.Error>>;
    getTermsAndPrivacy: () => Promise<Result<ExternalUserTermsAndPrivacyVersionDTO, SayHey.Error>>;
    getExistingSmsConversation: (params: FindSmsConversation) => Promise<Result<ConversationDomainEntity | null, SayHey.Error>>;
    checkSmsConversationInvitation: (params: {
        conversationId: string;
    }) => Promise<Result<SmsConversationInvitationUserDTO, SayHey.Error>>;
    acceptSmsConversationInvitation: (params: {
        conversationId: string;
        receiverId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    resendSmsConversationInvitation: (params: {
        conversationId: string;
    }) => Promise<Result<SmsConversationInvitationDTO, SayHey.Error>>;
    createSmsConversation: (params: {
        memberId: string;
        groupImageId?: string | null;
    }) => Promise<Result<ConversationDomainEntity, SayHey.Error>>;
    optToReceiveSms: () => Promise<Result<boolean, SayHey.Error>>;
    optNotToReceiveSms: () => Promise<Result<boolean, SayHey.Error>>;
    getOptToReceiveSmsStatus: () => Promise<Result<ExternalUserOptToReceiveSmsDTO, SayHey.Error>>;
    nudgeExternalUserToConversation: (params: {
        conversationId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
}
