import { AxiosInstance } from 'axios';
import { Result } from 'neverthrow';
import { ApiKeyName } from './constants';
import { AdminAuthResponse, AllFlaggedMessageCountDailyDTO, AuditMessageDomainEntityBatchDTO, AuditMessageDomainEntityDTO, ConversableUsersForUserAdminBatchResult, ConversationDTOForUserAdmin, ConversationGroupBatchDTOForUserAdminWithUserCount, ConversationGroupDTOForUserAdminWithUserCount, ConversationUserQuery, ConversationUsersForUserAdminBatch, DefaultGroupImageUrlSet, DepartmentBatchDTO, DepartmentDivisionDTO, DepartmentSortFields, DivisionBatchDTO, DivisionSortFields, FileUpload, FlaggedMessageBatchDTO, FlaggedMessageCountDTO, FlaggedMessageDetailsDTO, FlaggedMessageResolutionBatchDTO, FlaggedMessageResolutionScanBatchDTO, FlaggedMessageResolutionUserReportBatchDTO, FlaggedMessageScanBatchDTO, FlaggedMessageUserReportBatchDTO, ImageSizes, KeywordBatchDTO, KeyWordPriorityTypes, PeekMessageDomainEntity, QueryCondition, ReachableUsersQuery, ReactedUserDTO, ReactionType, ResolutionStatusType, SayHey, SentMessageCountDailyDTO, SortOrderType, TimeFrameType, TotalUsersCountDTO, UnmanagedUsersBatchDTO, UnmanagedUsersCountDTO, UnmanagedUsersSortFields, UnresolvedFlaggedMessagesCountDTO, UnresolvedMsgScopeType, UserAdminBatchDTO, UserAdminDTO, UserAdminSearchDTO, UserAdminSortFields, UserAdminStatusType, UserBasicInfoBatchDTO, UserManagementBatchDTO, UserManagementCountQueryParams, UserManagementCountResultParams, UserManagementSortFields, UserManagementType, UserMovementSelected, UserOnboardingJobBatchDTO, UserOnboardingJobStatus, UserRole, UserSearchCriteria, UserStatusType, UserTypes } from './types';
export * from './types';
export declare class ChatAdminKit {
    accessToken: string;
    refreshToken: string;
    private _apiVersion;
    httpClient: AxiosInstance;
    appId: string;
    apiKey: string;
    serverUrl: string;
    authDetailChangeListener?: (res: AdminAuthResponse) => Promise<void> | void;
    /** NOTE: Will be called when refresh token exchange session API call has failed */
    sessionExpiredListener?: () => void;
    private _initialized;
    private _subscribed;
    private _isUserAuthenticated;
    private exchangeTokenPromise;
    private disableAutoRefreshToken?;
    private loginId;
    isUserAuthenticated: () => boolean;
    /**
     * Setup methods
     */
    initialize(params: {
        sayheyAppId: string;
        sayheyApiKey: string;
        sayheyUrl: string;
        apiVersion?: 'v1' | 'v2' | 'v3' | 'v4';
        disableAutoRefreshToken?: boolean;
    }): void;
    /**
     * Listeners
     */
    addListeners: (listeners: {
        onAuthDetailChange: (res: AdminAuthResponse) => Promise<void> | void;
        onSessionExpired?: (() => void) | undefined;
    }) => Result<boolean, SayHey.Error>;
    /**
     *
     * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
     */
    private guard;
    exchangeTokenOnce: () => Promise<Result<boolean, SayHey.Error>>;
    /**
     *
     * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
     */
    private guardWithAuth;
    /**
     *
     * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
     * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
     */
    private updateAuthDetails;
    /**
     *
     * @param urlPath - url path returned from SayHey
     */
    getMediaSourceDetails: (urlPath: string) => {
        uri: string;
        method: string;
        headers: {
            "x-sayhey-client-api-key": string;
            Authorization: string;
            Accept: string;
        };
    };
    getMediaDirectUrl: (params: {
        urlPath: string;
        size?: ImageSizes;
    }) => Promise<Result<string, SayHey.Error>>;
    private uploadFile;
    uploadImage: (fileUri: string | Blob, fileName: string) => Promise<Result<string, SayHey.Error>>;
    /**
     * Token APIs
     */
    /**
     *
     * @param email - user email for SayHey
     * @param password - user password for SayHey
     *
     */
    exchangeToken: () => Promise<Result<boolean, SayHey.Error>>;
    signOut: () => Promise<void>;
    signInWithToken: (accessToken: string, refreshToken: string) => Promise<Result<boolean, SayHey.Error>>;
    invite: ({ firstName, lastName, email, role }: {
        firstName: string;
        lastName: string;
        email: string;
        role: UserTypes.HrAdmin | UserTypes.SuperHrAdmin;
    }) => Promise<Result<boolean, SayHey.Error>>;
    signUp: ({ password, userAdminId, invitationCode }: {
        password: string;
        userAdminId: string;
        invitationCode: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    signIn: (email: string, password: string) => Promise<Result<boolean, SayHey.Error>>;
    reInvite: ({ userAdminId }: {
        userAdminId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    disableAdmin: ({ userAdminId }: {
        userAdminId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    enableAdmin: ({ userAdminId }: {
        userAdminId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    updateUserAdminInfo: ({ firstName, lastName, email, userAdminId }: {
        userAdminId: string;
        firstName?: string | undefined;
        lastName?: string | undefined;
        email?: string | undefined;
    }) => Promise<Result<boolean, SayHey.Error>>;
    updateUserAdminRole: ({ role, userAdminId }: {
        userAdminId: string;
        role: UserTypes.HrAdmin | UserTypes.SuperHrAdmin;
    }) => Promise<Result<boolean, SayHey.Error>>;
    updateUserAdminOnLeave: ({ onLeave }: {
        onLeave: boolean;
    }) => Promise<Result<boolean, SayHey.Error>>;
    forgotPassword: ({ email }: {
        email: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    resetPassword: ({ password, resetToken }: {
        password: string;
        resetToken: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    getUserAdmins: ({ offset, limit, search, userAdminStatus, sort, sortOrder, role }: {
        offset?: number | undefined;
        limit?: number | undefined;
        search?: string | null | undefined;
        userAdminStatus?: UserAdminStatusType | undefined;
        sort?: UserAdminSortFields | undefined;
        sortOrder?: SortOrderType | undefined;
        role?: UserTypes.HrAdmin | UserTypes.SuperHrAdmin | undefined;
    }) => Promise<Result<UserAdminBatchDTO, SayHey.Error>>;
    getUserAdminsSelfInfo: () => Promise<Result<UserAdminDTO, SayHey.Error>>;
    updateUserAdminsSelfInfo: ({ firstName, lastName }: {
        firstName?: string | undefined;
        lastName?: string | undefined;
    }) => Promise<Result<boolean, SayHey.Error>>;
    assignManagedUsers: (list: {
        userId: string;
        userAdminId: string;
    }[]) => Promise<Result<{
        ids: string[];
    }, SayHey.Error>>;
    moveAllManagedUsers: ({ fromUserAdminId, toUserAdminId, assignmentType }: {
        fromUserAdminId: string;
        toUserAdminId: string;
        assignmentType: UserManagementType;
    }) => Promise<Result<boolean, SayHey.Error>>;
    moveBackAllManagedUsers: (userAdminId: string) => Promise<Result<boolean, SayHey.Error>>;
    getAllManagedUsers: ({ offset, limit, search, userStatus, userManagementType, sort, sortOrder }: {
        offset?: number | undefined;
        limit?: number | undefined;
        search?: string | null | undefined;
        userStatus?: UserStatusType | undefined;
        userManagementType?: UserManagementType | undefined;
        sort?: UserManagementSortFields | undefined;
        sortOrder?: SortOrderType | undefined;
    }) => Promise<Result<UserManagementBatchDTO, SayHey.Error>>;
    getAllManagedUsersCount: (queryParams: UserManagementCountQueryParams) => Promise<Result<UserManagementCountResultParams, SayHey.Error>>;
    moveSelectedUsers: (userMovementSelected: UserMovementSelected) => Promise<Result<boolean, SayHey.Error>>;
    getUnmanagedUsers: ({ offset, limit, search, userStatus, sort, sortOrder }: {
        offset?: number | undefined;
        limit?: number | undefined;
        search?: string | null | undefined;
        userStatus?: UserStatusType | undefined;
        sort?: UnmanagedUsersSortFields | undefined;
        sortOrder?: 1 | -1 | undefined;
    }) => Promise<Result<UnmanagedUsersBatchDTO, SayHey.Error>>;
    getUnmanagedUsersCount: () => Promise<Result<UnmanagedUsersCountDTO, SayHey.Error>>;
    registerUser: ({ firstName, lastName, email, hrAdminId, departmentId, divisionId, employeeId, title }: {
        firstName: string;
        lastName: string;
        email: string;
        hrAdminId: string;
        departmentId: string;
        divisionId: string;
        employeeId?: string | undefined;
        title?: string | undefined;
    }) => Promise<Result<{
        id: string;
    }, SayHey.Error>>;
    updateUser: ({ firstName, lastName, email, title, departmentId, divisionId, employeeId, userId }: {
        firstName?: string | undefined;
        lastName?: string | undefined;
        email?: string | undefined;
        title?: string | undefined;
        departmentId?: string | undefined;
        divisionId?: string | undefined;
        employeeId?: string | undefined;
        userId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    getDepartments: ({ offset, limit, search, sort, sortOrder, includeDivisions }: {
        offset?: number | undefined;
        limit?: number | undefined;
        search?: string | null | undefined;
        sort?: DepartmentSortFields | undefined;
        sortOrder?: SortOrderType | undefined;
        includeDivisions?: boolean | undefined;
    }) => Promise<Result<DepartmentBatchDTO, SayHey.Error>>;
    createDepartment: (name: string) => Promise<Result<{
        id: string;
    }, SayHey.Error>>;
    updateDepartment: (params: {
        name: string;
        departmentId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    getDivisions: ({ offset, limit, search, sort, sortOrder, departmentId }: {
        departmentId: string;
        offset?: number | undefined;
        limit?: number | undefined;
        search?: string | null | undefined;
        sort?: DivisionSortFields | undefined;
        sortOrder?: SortOrderType | undefined;
    }) => Promise<Result<DivisionBatchDTO, SayHey.Error>>;
    createDivision: (params: {
        name: string;
        departmentId: string;
    }) => Promise<Result<{
        id: string;
    }, SayHey.Error>>;
    updateDivision: (params: {
        name: string;
        departmentId: string;
        divisionId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    createKeyword: ({ term, priority }: {
        term: string;
        priority: KeyWordPriorityTypes;
    }) => Promise<Result<{
        id: string;
    }, SayHey.Error>>;
    createKeywordBulk: (list: {
        term: string;
        priority: KeyWordPriorityTypes;
    }[]) => Promise<Result<{
        id: string;
    }[], SayHey.Error>>;
    deleteKeyword: (id: string) => Promise<Result<boolean, SayHey.Error>>;
    updateKeyword: (params: {
        id: string;
        priority: KeyWordPriorityTypes;
    }) => Promise<Result<boolean, SayHey.Error>>;
    getKeywordsBatch: ({ offset, limit, search, priority }: {
        offset?: number | undefined;
        limit?: number | undefined;
        search?: string | null | undefined;
        priority?: KeyWordPriorityTypes | undefined;
    }) => Promise<Result<KeywordBatchDTO, SayHey.Error>>;
    disableUser: (params: {
        userId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    enableUser: (params: {
        userId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    getMessagesBeforeSequence: ({ beforeSequence, limit, conversationId }: {
        beforeSequence?: number | undefined;
        limit?: number | undefined;
        conversationId: string;
    }) => Promise<Result<PeekMessageDomainEntity[], SayHey.Error>>;
    getMessagesAfterSequence: ({ afterSequence, limit, conversationId }: {
        afterSequence?: number | undefined;
        limit?: number | undefined;
        conversationId: string;
    }) => Promise<Result<PeekMessageDomainEntity[], SayHey.Error>>;
    getMessageContext: ({ peekBeforeLimit, peekAfterLimit, messageId }: {
        peekBeforeLimit?: number | undefined;
        peekAfterLimit?: number | undefined;
        messageId: string;
    }) => Promise<Result<PeekMessageDomainEntity[], SayHey.Error>>;
    getAuditMessagesBatch: ({ offset, limit, startDate, endDate, text, textSearchCondition, userId, hasAttachment, hasFlagged }: {
        offset?: number | undefined;
        limit?: number | undefined;
        startDate?: string | undefined;
        endDate?: string | undefined;
        text?: string[] | undefined;
        textSearchCondition?: QueryCondition | undefined;
        userId?: string | undefined;
        hasAttachment?: boolean | undefined;
        hasFlagged?: boolean | undefined;
    }) => Promise<Result<AuditMessageDomainEntityBatchDTO, SayHey.Error>>;
    downloadAuditMessages: ({ startDate, endDate, text, textSearchCondition, userId, hasAttachment, hasFlagged }: {
        startDate?: string | undefined;
        endDate?: string | undefined;
        text?: string[] | undefined;
        textSearchCondition?: QueryCondition | undefined;
        userId?: string | undefined;
        hasAttachment?: boolean | undefined;
        hasFlagged?: boolean | undefined;
    }) => Promise<Result<AuditMessageDomainEntityDTO[], SayHey.Error>>;
    getUserBasicInfoBatch: ({ offset, limit, search }: {
        offset?: number | undefined;
        limit?: number | undefined;
        search?: string | null | undefined;
    }) => Promise<Result<UserBasicInfoBatchDTO, SayHey.Error>>;
    createUpdateUserAdminSearch: (userAdminSearchCriteria: UserSearchCriteria) => Promise<Result<boolean, SayHey.Error>>;
    getUserAdminSearch: () => Promise<Result<UserAdminSearchDTO, SayHey.Error>>;
    getFlaggedMessagesBatch: ({ offset, limit, search, flagType, status }: {
        offset?: number | undefined;
        limit?: number | undefined;
        search?: string | null | undefined;
        flagType?: "high" | "low" | "user_reported" | "all" | undefined;
        status?: "all" | "pending" | "resolved" | undefined;
    }) => Promise<Result<FlaggedMessageBatchDTO, SayHey.Error>>;
    getFlaggedMessagesCount: () => Promise<Result<FlaggedMessageCountDTO, SayHey.Error>>;
    resolveFlaggedMessage: ({ resolutionStatus, resolutionNotes, messageDeletion, flaggedMessageId, notifySuperAdmin }: {
        resolutionStatus: ResolutionStatusType.NO_ACTION | ResolutionStatusType.ACTION_TAKEN;
        resolutionNotes?: string | undefined;
        messageDeletion?: boolean | undefined;
        notifySuperAdmin?: boolean | undefined;
        flaggedMessageId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    getFlaggedMessageDetails: ({ flaggedMessageId }: {
        flaggedMessageId: string;
    }) => Promise<Result<FlaggedMessageDetailsDTO, SayHey.Error>>;
    getFlaggedMessagesUserReportsBatch: ({ offset, limit, flaggedMessageId, status }: {
        offset?: number | undefined;
        limit?: number | undefined;
        flaggedMessageId: string;
        status?: "pending" | "resolved" | undefined;
    }) => Promise<Result<FlaggedMessageUserReportBatchDTO, SayHey.Error>>;
    getFlaggedMessagesKeywordScansBatch: ({ offset, limit, flaggedMessageId, status }: {
        offset?: number | undefined;
        limit?: number | undefined;
        flaggedMessageId: string;
        status?: "pending" | "resolved" | undefined;
    }) => Promise<Result<FlaggedMessageScanBatchDTO, SayHey.Error>>;
    getFlaggedMessagesResolutionsBatch: ({ offset, limit, flaggedMessageId }: {
        offset?: number | undefined;
        limit?: number | undefined;
        flaggedMessageId: string;
    }) => Promise<Result<FlaggedMessageResolutionBatchDTO, SayHey.Error>>;
    getFlaggedMessagesResolutionUserReportsBatch: ({ offset, limit, resolutionId }: {
        offset?: number | undefined;
        limit?: number | undefined;
        resolutionId: string;
    }) => Promise<Result<FlaggedMessageResolutionUserReportBatchDTO, SayHey.Error>>;
    getFlaggedMessagesResolutionKeywordScansBatch: ({ offset, limit, resolutionId }: {
        offset?: number | undefined;
        limit?: number | undefined;
        resolutionId: string;
    }) => Promise<Result<FlaggedMessageResolutionScanBatchDTO, SayHey.Error>>;
    getReactedUsers: ({ messageId, type, limit, afterSequence }: {
        messageId: string;
        type?: ReactionType | undefined;
        limit: number;
        afterSequence: number | null;
    }) => Promise<Result<ReactedUserDTO[], SayHey.Error>>;
    getSpaceGroupDivisions: ({ conversationId }: {
        conversationId: string;
        offset?: number | undefined;
        limit?: number | undefined;
    }) => Promise<Result<DepartmentDivisionDTO[], SayHey.Error>>;
    createConversationSpaceGroupDivision: ({ conversationId, divisionIds, memberIds }: {
        divisionIds?: string[] | undefined;
        conversationId: string;
        memberIds?: string[] | undefined;
    }) => Promise<Result<boolean, SayHey.Error>>;
    deleteConversationSpaceGroupDivision: ({ conversationId, divisionIds }: {
        divisionIds: string[];
        conversationId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    createGroupConversation: (params: {
        groupName: string;
        file?: FileUpload;
        userIds?: string[];
        divisionIds?: string[];
    }) => Promise<Result<ConversationGroupDTOForUserAdminWithUserCount, SayHey.Error>>;
    updateGroupOwner: (params: {
        conversationId: string;
        userAdminId: KeyWordPriorityTypes;
    }) => Promise<Result<boolean, SayHey.Error>>;
    getGroupConversationBatch: ({ search, offset, limit }: {
        search?: string | undefined;
        offset?: number | undefined;
        limit?: number | undefined;
    }) => Promise<Result<ConversationGroupBatchDTOForUserAdminWithUserCount, SayHey.Error>>;
    deleteGroupConversation: (params: {
        conversationId: string;
    }) => Promise<Result<boolean, SayHey.Error>>;
    getReachableUsers: ({ searchString, limit, nextCursor, includeRefIdInSearch }?: ReachableUsersQuery) => Promise<Result<ConversableUsersForUserAdminBatchResult, SayHey.Error>>;
    getConversationUsers: ({ conversationId, searchString, limit, nextCursor, type, includeRefIdInSearch }: ConversationUserQuery) => Promise<Result<ConversationUsersForUserAdminBatch, SayHey.Error>>;
    addUsersToConversation: (conversationId: string, userIds: string[], force?: boolean | undefined) => Promise<Result<boolean, SayHey.Error>>;
    removeUsersFromConversation: (conversationId: string, userIds: string[], force?: boolean | undefined) => Promise<Result<boolean, SayHey.Error>>;
    updateGroupImage: (conversationId: string, file: FileUpload) => Promise<Result<boolean, SayHey.Error>>;
    removeGroupImage: (conversationId: string) => Promise<Result<boolean, SayHey.Error>>;
    getGroupDefaultImageSet: () => Promise<Result<DefaultGroupImageUrlSet[], SayHey.Error>>;
    updateGroupInfo: (conversationId: string, groupName: string) => Promise<Result<boolean, SayHey.Error>>;
    makeConversationOwner: (conversationId: string, userId: string) => Promise<Result<boolean, SayHey.Error>>;
    removeConversationOwner: (conversationId: string, userId: string) => Promise<Result<boolean, SayHey.Error>>;
    getConversationDetails: (conversationId: string) => Promise<Result<ConversationDTOForUserAdmin, SayHey.Error>>;
    getTotalUsersCount: () => Promise<Result<TotalUsersCountDTO, SayHey.Error>>;
    getMyUsersCount: () => Promise<Result<{
        myUsersCount: number;
    }, SayHey.Error>>;
    getResolvedFlaggedMessagesCount: (timeFrame?: TimeFrameType | undefined) => Promise<Result<{
        resolvedMessagesCount: number;
    }, SayHey.Error>>;
    getSentMessagesCountDaily: () => Promise<Result<SentMessageCountDailyDTO[], SayHey.Error>>;
    getSentMessagesCountTotal: (timeFrame?: TimeFrameType | undefined) => Promise<Result<{
        sentMessagesCount: number;
    }, SayHey.Error>>;
    getUnresolvedFlaggedMessagesCount: (unresolvedMsgScope?: UnresolvedMsgScopeType | undefined) => Promise<Result<UnresolvedFlaggedMessagesCountDTO, SayHey.Error>>;
    getUnresolvedFlaggedMessagesCountDaily: () => Promise<Result<AllFlaggedMessageCountDailyDTO, SayHey.Error>>;
    getEligibleRolesToInviteUserAdmin: () => Promise<Result<UserRole[], SayHey.Error>>;
    processUserOnboardingJobUpload: (params: {
        fileUri: string | Blob;
        fileName: string;
        departmentId: string;
        divisionId: string;
    }) => Promise<Result<{
        message: string;
    }, SayHey.Error>>;
    getUserOnboardingJobBatch: ({ offset, limit, search, status }: {
        offset?: number | undefined;
        limit?: number | undefined;
        search?: string | null | undefined;
        status?: UserOnboardingJobStatus[] | undefined;
    }) => Promise<Result<UserOnboardingJobBatchDTO, SayHey.Error>>;
    downloadSampleFile: () => Promise<Result<Blob, SayHey.Error>>;
    getAllErroredUserOnboardingJobRecords: ({ userOnboardingJobId }: {
        userOnboardingJobId: string;
    }) => Promise<Result<Blob, SayHey.Error>>;
}
