declare const _default: {
    createWithoutAuth: (config: {
        baseURL: string;
        apiKey: string;
    }) => import("axios").AxiosInstance;
    createWithAuth: (config: {
        baseURL: string;
        apiKey: string;
        accessToken: string;
    }) => import("axios").AxiosInstance;
};
export default _default;
