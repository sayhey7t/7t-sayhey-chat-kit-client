/**
 * Http Types
 */
import { EventMessage } from '@7t/publisher-kit-client';
export declare enum HttpMethod {
    Post = "post",
    Get = "get",
    Delete = "delete",
    Patch = "patch",
    Put = "put"
}
export interface SearchPaginateResponse {
    search: string | null;
    totalCount: number;
    offset: number;
    nextOffset: number;
    limit: number;
    completed: boolean;
}
/**
 * Reaction Types
 */
export declare enum ReactionType {
    Thumb = "Thumb",
    Heart = "Heart",
    Clap = "Clap",
    HaHa = "HaHa",
    Exclamation = "Exclamation"
}
export interface Reaction {
    type: ReactionType;
    count: number;
    reacted: boolean;
}
export declare class ReactionsObject {
    [ReactionType.Thumb]: number;
    [ReactionType.Heart]: number;
    [ReactionType.Clap]: number;
    [ReactionType.HaHa]: number;
    [ReactionType.Exclamation]: number;
}
/**
 * Message Types
 */
export declare enum ImageSizes {
    Tiny = "tiny",
    Small = "small",
    Medium = "medium",
    Large = "large",
    XLarge = "xlarge"
}
export interface SubImageMinimal {
    imageSize: ImageSizes;
    width: number;
    height: number;
    postfix: string;
}
export interface ImageInfoMedia {
    width: number;
    height: number;
    subImagesInfo: SubImageMinimal[];
}
export declare enum FileType {
    Video = "video",
    Image = "image",
    Audio = "audio",
    Other = "other"
}
interface BaseMedia {
    fileType: FileType;
    urlPath: string;
    mimeType: string;
    extension: string;
    encoding: string;
    filename: string;
    fileSize: number;
    imageInfo: ImageInfoMedia | null;
}
export interface MediaDTO extends BaseMedia {
    videoInfo: {
        duration: number;
        thumbnail: MediaDTO | null;
    } | null;
}
export interface MediaDomainEntity extends BaseMedia {
    videoInfo: {
        duration: number;
        thumbnail: MediaDomainEntity | null;
    } | null;
}
export declare enum MessageType {
    System = "system",
    Text = "text",
    Image = "image",
    Video = "video",
    Document = "document",
    Gif = "gif",
    Emoji = "emoji",
    Audio = "audio"
}
export declare enum HighLightType {
    Text = "text",
    Email = "email",
    Phone = "phone",
    Url = "url",
    UserId = "userId"
}
export declare enum SystemMessageType {
    MemberRemoved = "MemberRemoved",
    MemberAdded = "MemberAdded",
    OwnerRemoved = "OwnerRemoved",
    OwnerAdded = "OwnerAdded",
    GroupPictureChanged = "GroupPictureChanged",
    GroupNameChanged = "GroupNameChanged",
    GroupDisabled = "GroupDisabled",
    GroupCreated = "GroupCreated",
    IndividualChatCreated = "IndividualChatCreated",
    ExternalUserConversationInvitationAccepted = "ExternalUserConversationInvitationAccepted",
    ExternalUserNudged = "ExternalUserNudged"
}
export interface SystemMessageBase {
    systemMessageType: SystemMessageType;
    id: string;
    refValue: string | null;
    actorCount: number;
    initiator: UserSimpleInfo | null;
    actors: UserSimpleInfo[];
    text?: string | null;
}
export declare type SystemMessageDTO = SystemMessageBase;
export declare type SystemMessageDomainEntity = SystemMessageBase;
export interface UserSimpleInfo {
    firstName: string;
    lastName: string;
    id: string;
    refId: string | null;
}
interface BaseMessage {
    id: string;
    conversationId: string;
    type: MessageType;
    text: string | null;
    refUrl: string | null;
    sequence: number;
    reactions: ReactionsObject;
}
export interface MessageDTO extends BaseMessage {
    createdAt: string;
    sender: UserDTO | null;
    file: MediaDTO | null;
    systemMessage: SystemMessageDTO | null;
    deletedAt: string | null;
}
export declare type ParsedLine = {
    type: HighLightType;
    value: string;
}[];
export interface FlaggedInfoEntity {
    createdAt: Date;
    updatedAt: Date;
    id: string;
    messageId: string;
    messageDeleted: boolean;
    resolved: boolean;
    resolvedAt: Date | null;
}
export interface PeekMessageDomainEntity extends MessageDomainEntity {
    flaggedInfo: FlaggedInfoEntity | null;
}
export interface PeekMessageDTO extends MessageDTO {
    flaggedInfo: FlaggedInfoEntity | null;
}
export interface MessageDomainEntity extends BaseMessage {
    sender: UserDomainEntity | null;
    file: MediaDomainEntity | null;
    createdAt: Date;
    deletedAt: Date | null;
    sent: boolean;
    parsedText: ParsedLine[] | null;
    previewUrl?: string | null;
    systemMessage: SystemMessageDomainEntity | null;
}
export declare type SentMessageDomainEntity = Omit<MessageDomainEntity, 'systemMessage'>;
/**
 * Conversation Types
 */
export declare enum ConversationType {
    Group = "group",
    Individual = "individual"
}
export declare enum ConversationGroupType {
    Regular = "regular",
    Quick = "quick",
    Space = "space",
    Sms = "sms"
}
interface ConversationGroupInfoDTO {
    picture: MediaDTO | null;
    name: string;
    type: ConversationGroupType;
}
export declare type ConversationInfoDTO = (ConversationInfoInternalDTO | ConversationInfoExternalDTO) & {
    userScope: UserScope;
    picture: MediaDTO | null;
    name: string;
    type: ConversationGroupType;
};
export interface ConversationInfoDomainEntityInternal {
    userScope: UserScope.Internal;
    email: string;
}
export interface ConversationInfoDomainEntityExternal {
    userScope: UserScope.External;
    phone: PhoneWithCode;
}
export declare type ConversationInfoInternalDTO = ConversationInfoDomainEntityInternal;
export declare type ConversationInfoExternalDTO = ConversationInfoDomainEntityExternal;
interface ConversationGroupInfoDomainEntity {
    picture: MediaDomainEntity | null;
    name: string;
    type: ConversationGroupType;
}
export declare type ConversationInfoDomainEntity = (ConversationInfoDomainEntityInternal | ConversationInfoDomainEntityExternal) & {
    userScope: UserScope;
    picture: MediaDomainEntity | null;
    name: string;
    type: ConversationGroupType;
};
declare type UserConversationInfoDTO = ConversationInfoDTO & {
    id: string;
    refId: string | null;
};
export declare type UserConversationInfoDomainEntity = ConversationInfoDomainEntity & {
    id: string;
    refId: string | null;
};
interface BaseConversation {
    id: string;
    lastReadMessageId: string | null;
    type: ConversationType.Group | ConversationType.Individual;
    isOwner: boolean;
    visible: boolean;
    serverCreated: boolean;
    serverManaged: boolean;
    isSpace: boolean;
    isQuickGroup: boolean;
    lastUnreadNudgedAt: Date | null;
    creatorId: string | null;
}
export interface BaseConversationDTO extends BaseConversation {
    muteUntil: string | null;
    pinnedAt: string | null;
    badge: number | null;
    startAfter: string | null;
    endBefore: string | null;
    deletedFromConversationAt: string | null;
    createdAt: string;
    updatedAt: string;
    lastMessage: MessageDTO | null;
}
export interface IndividualConversationDTO extends BaseConversationDTO {
    type: ConversationType.Individual;
    user: UserConversationInfoDTO;
}
export interface GroupConversationDTO extends BaseConversationDTO {
    type: ConversationType.Group;
    group: ConversationGroupInfoDTO;
}
export declare type ConversationDTO = IndividualConversationDTO | GroupConversationDTO;
export interface ConversationDTOForUserAdmin {
    id: string;
    appId: string;
    type: ConversationType;
    group?: GroupDisplayDetail;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date | null;
    serverCreated: boolean;
    serverManaged: boolean;
    userAdminOwnerId: string;
}
export interface BaseConversationDomainEntity extends Omit<BaseConversation, 'user' | 'group'> {
    muteUntil: Date | null;
    pinnedAt: Date | null;
    badge: number;
    startAfter: Date | null;
    endBefore: Date | null;
    deletedFromConversationAt: Date | null;
    createdAt: Date;
    updatedAt: Date;
    lastMessage: MessageDomainEntity | null;
}
export interface GroupConversationDomainEntity extends BaseConversationDomainEntity {
    type: ConversationType.Group;
    group: ConversationGroupInfoDomainEntity;
}
export interface IndividualConversationDomainEntity extends BaseConversationDomainEntity {
    type: ConversationType.Individual;
    user: UserConversationInfoDomainEntity;
}
export declare type ConversationDomainEntity = GroupConversationDomainEntity | IndividualConversationDomainEntity;
/**
 * Auth Types
 */
export interface AuthDTO {
    accessToken: string;
    publisherToken: string;
    refreshToken: string;
}
/**
 * User Types
 */
export declare enum UserScope {
    Internal = "internal",
    External = "external"
}
export interface UserCommonDTO {
    id: string;
    firstName: string;
    lastName: string;
    refId: string | null;
    picture: MediaDTO | null;
    disabled: boolean;
    deletedAt: Date | null;
    userScope: UserScope;
}
export interface UserInternalDTO extends UserCommonDTO {
    email: string;
    userScope: UserScope.Internal;
}
export interface UserExternalDTO extends UserCommonDTO {
    phone: PhoneWithCode;
    userScope: UserScope.External;
}
export declare type UserDTO = UserInternalDTO | UserExternalDTO;
export interface UserInternalForUserAdminDTO extends UserInternalDTO {
    title: string | null;
    employeeId: string | null;
    departmentId: string;
    department: DepartmentDTO;
    divisionId: string;
    division: DivisionDTO;
}
export declare type UserExternalForUserAdminDTO = UserExternalDTO;
export declare type UserForUserAdminDTO = UserInternalForUserAdminDTO | UserExternalForUserAdminDTO;
interface UserDomainEntityBase {
    fullName: string;
    username: string | null;
    picture: MediaDomainEntity | null;
    deletedAt: Date | null;
}
interface UserDomainEntityInternal extends UserInternalDTO, UserDomainEntityBase {
}
interface UserDomainEntityExternal extends UserExternalDTO, UserDomainEntityBase {
}
export declare type UserDomainEntity = UserDomainEntityInternal | UserDomainEntityExternal;
declare type UserForUserAdminDomainEntityInternal = UserDomainEntityInternal & {
    title: string | null;
    employeeId: string | null;
    departmentId: string;
    department: DepartmentDTO;
    divisionId: string;
    division: DivisionDTO;
};
declare type UserForUserAdminDomainEntityExternal = UserDomainEntityExternal;
export declare type UserForUserAdminDomainEntity = UserForUserAdminDomainEntityInternal | UserForUserAdminDomainEntityExternal;
export interface AuthResponse {
    userId: string;
    accessToken: string;
    publisherToken: string;
    refreshToken: string;
}
export interface AdminAuthResponse {
    userId: string;
    accessToken: string;
    refreshToken: string;
}
export declare type FileUpload = {
    path: string | Blob;
    fileName: string;
};
export declare type FileWithText = {
    uri: string | Blob;
    thumbnail?: {
        name: string;
        uri: string | Blob;
    };
    type: MessageType;
    text?: string;
    fileName: string;
};
export interface GalleryPayload {
    nextCursor: number;
    messageList: MessageDomainEntity[];
}
export declare enum UserTypes {
    User = "user",
    Admin = "admin",
    HrAdmin = "hr_admin",
    SuperHrAdmin = "super_hr_admin"
}
export declare enum UserAdminSortFields {
    UserAdminName = "userAdminName",
    UserAdminEmail = "userAdminEmail",
    UserAdminRole = "userAdminRole"
}
export declare enum UserAdminStatusType {
    Enabled = "enabled",
    Disabled = "disabled",
    All = "all"
}
export declare enum APISignature {
    ME = "me"
}
export declare enum UserManagementType {
    Perm = "permanent",
    Temp = "temporary"
}
export interface SearchPaginateParams {
    search: string;
    offset: number;
    limit: number;
}
export declare type UserManagementSearchPaginateResponse = SearchPaginateResponse;
export declare type UnmanagedUsersSearchPaginateResponse = SearchPaginateResponse;
export declare type UserAdminSearchPaginateResponse = SearchPaginateResponse;
export declare enum UserManagementSortFields {
    UserName = "userName",
    UserEmail = "userEmail",
    UserEmpCode = "userEmployeeCode",
    UserDepartment = "userDepartment",
    UserTitle = "userTitle",
    UserHrAdmin = "userHrAdmin"
}
export declare enum UserStatusType {
    Enabled = "enabled",
    Disabled = "disabled",
    All = "all"
}
export declare class UserMovementSelected {
    userIds: string[];
    toUserAdminId: string;
    assignmentType: UserManagementType;
}
export declare enum UnmanagedUsersSortFields {
    UserName = "userName",
    UserEmail = "userEmail",
    UserEmpCode = "userEmployeeCode",
    UserDepartment = "userDepartment",
    UserTitle = "userTitle"
}
/**
 * DTOs
 */
interface ConversableUsersBatchMeta extends SearchPaginateResponse {
    userId: string;
}
export interface ConversableUsersBatchDTO extends ConversableUsersBatchMeta {
    results: UserDTO[];
}
export interface ConversableUsersForUserAdminBatchDTO extends UserSearchPaginateResponse {
    results: UserForUserAdminDTO[];
}
export interface ConversableUsersBatchResult extends ConversableUsersBatchMeta {
    results: UserDomainEntity[];
}
export interface ConversableUsersForUserAdminBatchResult extends SearchPaginateResponse {
    results: UserForUserAdminDomainEntity[];
}
export interface ReachableUsersQuery {
    searchString?: string;
    limit?: number;
    nextCursor?: number;
    includeRefIdInSearch?: boolean;
}
export declare enum ConversationUserQueryType {
    All = "all",
    Current = "current",
    Removed = "removed"
}
export interface ConversationReachableUsersQuery {
    conversationId: string;
    searchString?: string;
    limit?: number;
    nextCursor?: number;
    includeRefIdInSearch?: boolean;
}
export interface ConversationUserQuery extends ConversationReachableUsersQuery {
    type?: ConversationUserQueryType;
}
interface AdditionalUserOfConversation {
    deletedFromConversationAt: Date | null;
    isOwner: boolean;
}
export interface ConversationConversableUsersBatchDTO extends ConversationConversableUsersBatchMeta {
    results: UserDTO[];
}
export interface ConversationConversableUsersBatch extends ConversationConversableUsersBatchMeta {
    results: UserDomainEntity[];
}
interface ConversationConversableUsersBatchMeta extends SearchPaginateResponse {
    conversationId: string;
}
interface ConversationUsersBatchMeta extends ConversationConversableUsersBatchMeta {
    type: ConversationUserQueryType;
}
export interface UserTitle {
    title: string | null;
}
interface UserDepartmentAndDivision {
    departmentId: string;
    divisionId: string;
    departmentName: string;
    divisionName: string;
}
export declare type UserOfConversationDTO = UserDTO & AdditionalUserOfConversation & UserTitle;
export declare type UserOfConversationForUserAdminDTO = UserDTO & AdditionalUserOfConversation & UserTitle & UserDepartmentAndDivision;
export interface ConversationUsersBatchDTO extends ConversationUsersBatchMeta {
    results: UserOfConversationDTO[];
}
export interface ConversationUsersBatchForUserAdminDTO extends ConversationUsersBatchMeta {
    results: UserOfConversationForUserAdminDTO[];
}
export declare type ConversationUserDomainEntity = UserDomainEntity & UserTitle & {
    deletedFromConversationAt: Date | null;
    isOwner: boolean;
};
export declare type ConversationUserDomainForUserAdminEntity = UserDomainEntity & UserTitle & UserDepartmentAndDivision & {
    deletedFromConversationAt: Date | null;
    isOwner: boolean;
};
export interface ConversationUsersBatch extends ConversationConversableUsersBatchMeta {
    results: ConversationUserDomainEntity[];
}
export interface ConversationUsersForUserAdminBatch extends ConversationConversableUsersBatchMeta {
    results: ConversationUserDomainForUserAdminEntity[];
}
export interface ConversationUsersBasicDTO {
    isOwner: boolean;
    id: string;
    deletedFromConversationAt: Date | null;
    userDisabled: boolean;
    userDeletedAt: Date | null;
}
export declare namespace Publisher {
    export enum EventType {
        NewMessage = "NEW_MESSAGE",
        ConversationCreated = "CONVERSATION_CREATED",
        ConversationMuted = "CONVERSATION_MUTED",
        ConversationUnmuted = "CONVERSATION_UNMUTED",
        ConversationPinned = "CONVERSATION_PINNED",
        ConversationUnpinned = "CONVERSATION_UNPINNED",
        ConversationHideAndClear = "CONVERSATION_HIDE_AND_CLEAR",
        ConversationHideAndClearAll = "CONVERSATION_HIDE_AND_CLEAR_ALL",
        ConversationGroupInfoChanged = "CONVERSATION_GROUP_INFO_CHANGED",
        ConversationQuickGroupChangedToGroup = "CONVERSATION_QUICK_GROUP_CHANGED_TO_GROUP",
        ExternalUserConversationInvitationAccepted = "EXTERNAL_USER_CONVERSATION_INVITATION_ACCEPTED",
        UserInfoChanged = "USER_INFO_CHANGED",
        ConversationGroupOwnerAdded = "CONVERSATION_GROUP_OWNER_ADDED",
        ConversationGroupOwnerRemoved = "CONVERSATION_GROUP_OWNER_REMOVED",
        ConversationGroupMemberAdded = "CONVERSATION_GROUP_MEMBER_ADDED",
        ConversationGroupMemberRemoved = "CONVERSATION_GROUP_MEMBER_REMOVED",
        UserReactedToMessage = "USER_REACTED_TO_MESSAGE",
        UserUnReactedToMessage = "USER_UNREACTED_TO_MESSAGE",
        MessageReactionsUpdate = "MESSAGE_REACTIONS_UPDATE",
        UserDisabled = "USER_DISABLED",
        UserEnabled = "USER_ENABLED",
        MessageDeleted = "MESSAGE_DELETED",
        UserDeleted = "USER_DELETED",
        MessageRead = "MESSAGE_READ",
        ExternalUserNudged = "EXTERNAL_USER_NUDGED"
    }
    namespace Payload {
        interface NewMessage {
            message: MessageDTO;
            clientRefId: string | null;
        }
        interface ConversationCreated {
            conversationId: string;
        }
        interface ConversationUnmuted {
            conversationId: string;
        }
        interface ConversationUnpinned {
            conversationId: string;
        }
        interface ConversationMuted {
            conversationId: string;
            conversationMuteDto: {
                muteUntil: string;
            };
        }
        interface ConversationPinned {
            conversationId: string;
            conversationPinDto: {
                pinnedAt: string;
            };
        }
        interface ConversationHideAndClear {
            conversationId: string;
            conversationUserHideClearDto: {
                startAfter: string;
                visible: boolean;
            };
        }
        interface ConversationHideAndClearAll {
            conversationUserHideClearDto: {
                startAfter: string;
                visible: boolean;
            };
        }
        interface ConversationGroupInfoChanged {
            conversationId: string;
            groupName: string;
            groupPicture: MediaDTO | null;
        }
        interface ConversationQuickGroupChangedToGroup {
            conversationId: string;
            groupName: string;
            isQuickGroup: boolean;
        }
        interface ExternalUserConversationInvitationAccepted {
            conversationId: string;
            acceptedAt: Date;
            userId: string;
            userFirstName: string;
            userLastName: string;
            messageDomain: MessageDomainEntity;
            messageDto: MessageDTO;
        }
        interface UserInfoChanged {
            userId: string;
            userEmail: string;
            userPicture: MediaDTO | null;
            userFirstName: string;
            userLastName: string;
            userRefId: string | null;
            userMuteAllUntil: Date | null;
        }
        interface ConversationGroupOwnerRemoved {
            conversationId: string;
            userId: string;
            isOwner: boolean;
        }
        interface ConversationGroupOwnerAdded {
            conversationId: string;
            userId: string;
            isOwner: boolean;
        }
        interface ConversationGroupMemberAdded {
            conversationId: string;
            userIds: string[];
            deletedAt: null;
            endBefore: null;
        }
        interface ConversationGroupMemberRemoved {
            conversationId: string;
            userIds: string[];
            deletedAt: Date;
            endBefore: Date;
        }
        interface UserReactedToMessage {
            conversationId: string;
            messageId: string;
            type: ReactionType;
            userId: string;
        }
        interface UserUnReactedToMessage {
            conversationId: string;
            messageId: string;
            userId: string;
            type?: ReactionType;
        }
        interface MessageReactionsUpdate {
            conversationId: string;
            messageId: string;
            messageReactionsObject: ReactionsObject;
        }
        interface UserDisabled {
            userId: string;
            disabled: true;
        }
        interface UserEnabled {
            userId: string;
            disabled: false;
        }
        interface MessageDeleted {
            conversationId: string;
            messageId: string;
            deletedAt: Date;
        }
        interface UserDeleted {
            userId: string;
            deletedAt: Date;
        }
        interface MessageRead {
            userId: string;
            conversationId: string;
            sequence: number;
        }
        interface ExternalUserNudged {
            conversationId: string;
            messageDomain: MessageDomainEntity;
            messageDto: MessageDTO;
            updateData: {
                lastUnreadNudgedAt: Date;
            };
        }
    }
    export namespace Event {
        interface NewMessage extends EventMessage {
            event: EventType.NewMessage;
            payload: Payload.NewMessage;
        }
        interface ConversationCreated extends EventMessage {
            event: EventType.ConversationCreated;
            payload: Payload.ConversationCreated;
        }
        interface ConversationUnmuted extends EventMessage {
            event: EventType.ConversationUnmuted;
            payload: Payload.ConversationUnmuted;
        }
        interface ConversationUnpinned extends EventMessage {
            event: EventType.ConversationUnpinned;
            payload: Payload.ConversationUnpinned;
        }
        interface ConversationMuted extends EventMessage {
            event: EventType.ConversationMuted;
            payload: Payload.ConversationMuted;
        }
        interface ConversationPinned extends EventMessage {
            event: EventType.ConversationPinned;
            payload: Payload.ConversationPinned;
        }
        interface ConversationHideAndClear extends EventMessage {
            event: EventType.ConversationHideAndClear;
            payload: Payload.ConversationHideAndClear;
        }
        interface ConversationHideAndClearAll extends EventMessage {
            event: EventType.ConversationHideAndClearAll;
            payload: Payload.ConversationHideAndClearAll;
        }
        interface ConversationGroupInfoChanged extends EventMessage {
            event: EventType.ConversationGroupInfoChanged;
            payload: Payload.ConversationGroupInfoChanged;
        }
        interface ConversationQuickGroupChangedToGroup extends EventMessage {
            event: EventType.ConversationQuickGroupChangedToGroup;
            payload: Payload.ConversationQuickGroupChangedToGroup;
        }
        interface ExternalUserConversationInvitationAccepted extends EventMessage {
            event: EventType.ExternalUserConversationInvitationAccepted;
            payload: Payload.ExternalUserConversationInvitationAccepted;
        }
        interface UserInfoChanged extends EventMessage {
            event: EventType.UserInfoChanged;
            payload: Payload.UserInfoChanged;
        }
        interface ConversationGroupOwnerAdded extends EventMessage {
            event: EventType.ConversationGroupOwnerAdded;
            payload: Payload.ConversationGroupOwnerAdded;
        }
        interface ConversationGroupOwnerRemoved extends EventMessage {
            event: EventType.ConversationGroupOwnerRemoved;
            payload: Payload.ConversationGroupOwnerRemoved;
        }
        interface ConversationGroupMemberRemoved extends EventMessage {
            event: EventType.ConversationGroupMemberRemoved;
            payload: Payload.ConversationGroupMemberRemoved;
        }
        interface ConversationGroupMemberAdded extends EventMessage {
            event: EventType.ConversationGroupMemberAdded;
            payload: Payload.ConversationGroupMemberAdded;
        }
        interface UserReactedToMessage extends EventMessage {
            event: EventType.UserReactedToMessage;
            payload: Payload.UserReactedToMessage;
        }
        interface UserUnReactedToMessage extends EventMessage {
            event: EventType.UserUnReactedToMessage;
            payload: Payload.UserUnReactedToMessage;
        }
        interface MessageReactionsUpdate extends EventMessage {
            event: EventType.MessageReactionsUpdate;
            payload: Payload.MessageReactionsUpdate;
        }
        interface UserDisabled extends EventMessage {
            event: EventType.UserDisabled;
            payload: Payload.UserDisabled;
        }
        interface UserEnabled extends EventMessage {
            event: EventType.UserEnabled;
            payload: Payload.UserEnabled;
        }
        interface MessageDeleted extends EventMessage {
            event: EventType.MessageDeleted;
            payload: Payload.MessageDeleted;
        }
        interface UserDeleted extends EventMessage {
            event: EventType.UserDeleted;
            payload: Payload.UserDeleted;
        }
        interface MessageRead extends EventMessage {
            event: EventType.MessageRead;
            payload: Payload.MessageRead;
        }
        interface ExternalUserNudged extends EventMessage {
            event: EventType.ExternalUserNudged;
            payload: Payload.ExternalUserNudged;
        }
        export type AllEvents = NewMessage | ConversationCreated | ConversationMuted | ConversationPinned | ConversationUnmuted | ConversationUnpinned | ConversationHideAndClear | ConversationHideAndClearAll | ConversationGroupInfoChanged | ConversationQuickGroupChangedToGroup | ExternalUserConversationInvitationAccepted | UserInfoChanged | ConversationGroupOwnerAdded | ConversationGroupOwnerRemoved | ConversationGroupMemberAdded | ConversationGroupMemberRemoved | UserReactedToMessage | UserUnReactedToMessage | MessageReactionsUpdate | UserDisabled | UserEnabled | MessageDeleted | UserDeleted | MessageRead | ExternalUserNudged;
        export {};
    }
    export import Event = Event.AllEvents;
    export {};
}
export declare namespace SayHey {
    namespace Payload {
        interface ConversationHideAndClearAll {
            startAfter: Date;
            visible: boolean;
        }
        interface ConversationCreated {
            conversationId: string;
        }
        interface NewMessage {
            message: MessageDomainEntity;
            clientRefId: string | null;
        }
        interface ConversationMuted {
            conversationId: string;
            muteUntil: Date;
        }
        interface ConversationPinned {
            conversationId: string;
            pinnedAt: Date;
        }
        interface ConversationUnmuted {
            conversationId: string;
        }
        interface ConversationUnpinned {
            conversationId: string;
        }
        interface ConversationHideAndClear {
            conversationId: string;
            startAfter: Date;
            visible: boolean;
        }
        interface ConversationGroupInfoChanged {
            conversationId: string;
            groupName: string;
            groupPicture: MediaDomainEntity | null;
        }
        interface ConversationQuickGroupChangedToGroup {
            conversationId: string;
            groupName: string;
            isQuickGroup: boolean;
        }
        interface ExternalUserConversationInvitationAccepted {
            conversationId: string;
            acceptedAt: Date;
            userId: string;
            userFirstName: string;
            userLastName: string;
            messageDomain: MessageDomainEntity;
            messageDto: MessageDTO;
        }
        interface UserInfoChanged {
            userId: string;
            userEmail: string;
            userPicture: MediaDomainEntity | null;
            userFirstName: string;
            userLastName: string;
            userRefId: string | null;
            userMuteAllUntil: Date | null;
        }
        interface ConversationGroupOwnerRemoved {
            conversationId: string;
            userId: string;
            isOwner: boolean;
        }
        interface ConversationGroupOwnerAdded {
            conversationId: string;
            userId: string;
            isOwner: boolean;
        }
        interface ConversationGroupMemberRemoved {
            conversationId: string;
            userIds: string[];
            deletedAt: Date;
            endBefore: Date;
        }
        interface ConversationGroupMemberAdded {
            conversationId: string;
            userIds: string[];
            deletedAt: null;
            endBefore: null;
        }
        interface UserReactedToMessage {
            conversationId: string;
            messageId: string;
            type: ReactionType;
            userId: string;
        }
        interface MessageReactionsUpdate {
            conversationId: string;
            messageId: string;
            messageReactionsObject: ReactionsObject;
        }
        interface UserUnReactedToMessage {
            conversationId: string;
            messageId: string;
            userId: string;
            type?: ReactionType;
        }
        interface UserDisabled {
            userId: string;
            disabled: boolean;
        }
        interface UserEnabled {
            userId: string;
            disabled: boolean;
        }
        interface MessageDeleted {
            conversationId: string;
            messageId: string;
            deletedAt: Date;
        }
        interface UserDeleted {
            userId: string;
            deletedAt: Date;
        }
        interface MessageRead {
            userId: string;
            conversationId: string;
            sequence: number;
        }
        interface ExternalUserNudged {
            conversationId: string;
            messageDomain: MessageDomainEntity;
            messageDto: MessageDTO;
            updateData: {
                lastUnreadNudgedAt: Date;
            };
        }
    }
    namespace Event {
        interface AllConversationsDeleted {
            event: SayHey.EventType.ConversationHideAndClearAll;
            payload: Payload.ConversationHideAndClearAll;
        }
        interface ConversationCreated {
            event: SayHey.EventType.ConversationCreated;
            payload: Payload.ConversationCreated;
        }
        interface NewMessage {
            event: SayHey.EventType.NewMessage;
            payload: Payload.NewMessage;
        }
        interface ConversationMuted {
            event: SayHey.EventType.ConversationMuted;
            payload: Payload.ConversationMuted;
        }
        interface ConversationPinned {
            event: SayHey.EventType.ConversationPinned;
            payload: Payload.ConversationPinned;
        }
        interface ConversationUnmuted {
            event: SayHey.EventType.ConversationUnmuted;
            payload: Payload.ConversationUnmuted;
        }
        interface ConversationUnpinned {
            event: SayHey.EventType.ConversationUnpinned;
            payload: Payload.ConversationUnpinned;
        }
        interface ConversationHideAndClear {
            event: SayHey.EventType.ConversationHideAndClear;
            payload: Payload.ConversationHideAndClear;
        }
        interface ConversationGroupInfoChanged {
            event: SayHey.EventType.ConversationGroupInfoChanged;
            payload: Payload.ConversationGroupInfoChanged;
        }
        interface ConversationQuickGroupChangedToGroup {
            event: SayHey.EventType.ConversationQuickGroupChangedToGroup;
            payload: Payload.ConversationQuickGroupChangedToGroup;
        }
        interface ExternalUserConversationInvitationAccepted {
            event: SayHey.EventType.ExternalUserConversationInvitationAccepted;
            payload: Payload.ExternalUserConversationInvitationAccepted;
        }
        interface UserInfoChanged {
            event: SayHey.EventType.UserInfoChanged;
            payload: Payload.UserInfoChanged;
        }
        interface ConversationGroupOwnerRemoved {
            event: SayHey.EventType.ConversationGroupOwnerRemoved;
            payload: Payload.ConversationGroupOwnerRemoved;
        }
        interface ConversationGroupOwnerAdded {
            event: SayHey.EventType.ConversationGroupOwnerAdded;
            payload: Payload.ConversationGroupOwnerAdded;
        }
        interface ConversationGroupMemberRemoved extends EventMessage {
            event: SayHey.EventType.ConversationGroupMemberRemoved;
            payload: Payload.ConversationGroupMemberRemoved;
        }
        interface ConversationGroupMemberAdded extends EventMessage {
            event: SayHey.EventType.ConversationGroupMemberAdded;
            payload: Payload.ConversationGroupMemberAdded;
        }
        interface UserReactedToMessage extends EventMessage {
            event: SayHey.EventType.UserReactedToMessage;
            payload: Payload.UserReactedToMessage;
        }
        interface UserUnReactedToMessage extends EventMessage {
            event: SayHey.EventType.UserUnReactedToMessage;
            payload: Payload.UserUnReactedToMessage;
        }
        interface MessageReactionsUpdate extends EventMessage {
            event: SayHey.EventType.MessageReactionsUpdate;
            payload: Payload.MessageReactionsUpdate;
        }
        interface UserDisabled extends EventMessage {
            event: SayHey.EventType.UserDisabled;
            payload: Payload.UserDisabled;
        }
        interface UserEnabled extends EventMessage {
            event: SayHey.EventType.UserEnabled;
            payload: Payload.UserEnabled;
        }
        interface MessageDeleted extends EventMessage {
            event: EventType.MessageDeleted;
            payload: Payload.MessageDeleted;
        }
        interface UserDeleted extends EventMessage {
            event: EventType.UserDeleted;
            payload: Payload.UserDeleted;
        }
        interface MessageRead extends EventMessage {
            event: EventType.MessageRead;
            payload: Payload.MessageRead;
        }
        interface ExternalUserNudged extends EventMessage {
            event: SayHey.EventType.ExternalUserNudged;
            payload: Payload.ExternalUserNudged;
        }
        export type AllEvents = AllConversationsDeleted | ConversationCreated | NewMessage | ConversationMuted | ConversationUnmuted | ConversationUnpinned | ConversationPinned | ConversationHideAndClear | ConversationGroupInfoChanged | ConversationQuickGroupChangedToGroup | ExternalUserConversationInvitationAccepted | UserInfoChanged | ConversationGroupOwnerAdded | ConversationGroupOwnerRemoved | ConversationGroupMemberRemoved | ConversationGroupMemberAdded | UserReactedToMessage | UserUnReactedToMessage | MessageReactionsUpdate | UserDisabled | UserEnabled | MessageDeleted | UserDeleted | MessageRead | ExternalUserNudged;
        export {};
    }
    namespace ChatKitError {
        namespace ErrorType {
            type Type = InternalErrorType | AuthErrorType | FileErrorType | ServerErrorType | GeneralErrorType | MessageErrorType | UserErrorType | UserRegistrationErrorType | ConversationErrorType | UserManagementErrorType | DateErrorType | AppRegistrationErrorType | OTPErrorType | EmailErrorType | DepartmentErrorTpe | DivisionErrorType | AppFeatureErrorTpe | UserAdminErrorType | AppConfigErrorType | KeywordErrorType | UserAdminUpdateRoleType | FlaggedMessageErrorType | UserOnboardingJobErrorType | ExternalUserErrorType | ContactErrorType | SmsConversationErrorType | PushNotificationError | SmsErrorType | SmsConversationInvitationErrorType;
            enum InternalErrorType {
                Unknown = "InternalUnknown",
                Uninitialized = "InternalUninitialized",
                AuthMissing = "InternalAuthMissing",
                BadData = "InternalBadData",
                MissingParameter = "InternalMissingParameter"
            }
            enum AuthErrorType {
                InvalidKeyErrorType = "AuthErrorTypeInvalidKey",
                InvalidUserType = "AuthErrorTypeInvalidUserType",
                NotFoundErrorType = "AuthErrorTypeNotFound",
                MissingParamsErrorType = "AuthErrorTypeMissingParams",
                InvalidPassword = "AuthErrorTypeInvalidPassword",
                TokenExpiredType = "AuthErrorTypeTokenExpired",
                AppHasNoNotificationSetup = "AuthErrorTypeAppHasNoNotificationSetup",
                NotPartOfApp = "AuthErrorTypeNotPartOfApp",
                InvalidWebhookKey = "AuthErrorTypeInvalidWebhookKey",
                NoAccessToUser = "AuthErrorTypeNoAccessToUser",
                ServerNoAccessToUser = "AuthErrorTypeServerNoAccessToUser",
                IncorrectPassword = "AuthErrorTypeIncorrectPassword",
                NoSamePassword = "AuthErrorTypeNoSamePassword",
                NoPasswordSet = "AuthErrorTypeNoPasswordSet",
                PasswordAlreadySet = "AuthErrorTypePasswordAlreadySet",
                AccountNotPreVerified = "AuthErrorTypeAccountNotPreVerified",
                AccountAlreadyPreVerified = "AuthErrorTypeAccountAlreadyPreVerified",
                NoAppAccessToUser = "AuthErrorTypeNoAppAccessToUser",
                TokenNotStarted = "AuthErrorTypeTokenNotStarted",
                AccountAlreadyExists = "AuthErrorTypeAccountAlreadyExists",
                AdminAlreadyExists = "AuthErrorTypeAdminAlreadyExists",
                UserNotFound = "AuthErrorTypeUserNotFound",
                InvalidToken = "AuthErrorTypeInvalidToken",
                MissingAuthHeaderField = "AuthErrorTypeMissingAuthHeaderField",
                InvalidCredentials = "AuthErrorTypeInvalidCredentials",
                HashGeneration = "AuthErrorTypeHashGeneration",
                HashComparison = "AuthErrorTypeHashComparison",
                RefreshTokenAlreadyExchanged = "AuthErrorTypeRefreshTokenAlreadyExchanged",
                RefreshTokenNotFound = "AuthErrorTypeRefreshTokenNotFound",
                UserDisabled = "AuthErrorTypeUserDisabled",
                InvalidOTP = "AuthErrorTypeInvalidOTP",
                OTPExpired = "AuthErrorTypeOTPExpired",
                AdminNotAssociatedWithApp = "AuthErrorTypeAdminNotAssociatedWithApp",
                InvitationCodeNotFound = "AuthErrorTypeInvitationCodeNotFound",
                InvitationCodeExpired = "AuthErrorTypeInvitationCodeExpired",
                InvitationCodeRedeemed = "AuthErrorTypeInvitationCodeRedeemed",
                InvitationNotForUser = "AuthErrorTypeInvitationNotForUser",
                UserAlreadySignedUp = "AuthErrorTypeUserAlreadySignedUp",
                SignUpIncomplete = "AuthErrorTypeSignUpIncomplete",
                UserInactive = "AuthErrorTypeUserInactive",
                SignInIntegrated = "AuthErrorTypeSignInIntegrated",
                SelfSignUpNotAllowed = "AuthErrorTypeSelfSignUpNotAllowed",
                EmailDoesNotExist = "AuthErrorTypeEmailDoesNotExist",
                MissingIntegratedLoginUrl = "AuthErrorTypeMissingIntegratedLoginUrl",
                InvalidIntegratedLoginUrl = "AuthErrorTypeInvalidIntegratedLoginUrl",
                YouMustAcceptTermsAndPrivacy = "AuthErrorTypeYouMustAcceptTermsAndPrivacy",
                TooManyFailedAttempts = "AuthErrorTypeTooManyFailedAttempts",
                InCorrectUserNameOrPassword = "AuthErrorTypeInCorrectUserNameOrPassword",
                InCorrectPhoneNumberOrOtp = "AuthErrorTypeInCorrectPhoneNumberOrOtp",
                InCorrectUserOrOtp = "AuthErrorTypeInCorrectUserOrOtp"
            }
            enum FileErrorType {
                SizeLimitExceeded = "FileErrorTypeSizeLimitExceeded",
                NoFileSentType = "FileErrorTypeNoFileSentType",
                InvalidFieldNameForFile = "FileErrorTypeInvalidFieldNameForFile",
                InvalidMetadata = "FileErrorTypeInvalidMetadata",
                FileSizeLimitExceeded = "FileErrorTypeFileSizeLimitExceeded",
                InvalidFileName = "FileErrorTypeInvalidFileName",
                NotSent = "FileErrorTypeNotSent",
                NotFound = "FileErrorTypeNotFound",
                NoThumbnailFound = "FileErrorTypeNoThumbnailFound",
                ImageNotForConversation = "FileErrorTypeImageNotForConversation",
                ImageNotForUser = "FileErrorTypeImageNotForUser",
                FileNotForMessage = "FileErrorTypeFileNotForMessage",
                SizeNotFound = "FileErrorTypeSizeNotFound",
                NotForApp = "FileErrorTypeNotForApp",
                IncorrectFileType = "FileErrorTypeIncorrectFileType",
                NoHeadersFound = "FileErrorTypeNoHeadersFound",
                InvalidHeaders = "FileErrorTypeInvalidHeaders"
            }
            enum ServerErrorType {
                UnknownError = "UnknownError",
                InternalServerError = "InternalServerError"
            }
            enum GeneralErrorType {
                MissingQueryParams = "GeneralErrorTypeMissingQueryParams",
                RouteNotFound = "GeneralErrorTypeRouteNotFound",
                LimitNotValid = "GeneralErrorTypeLimitNotValid",
                OffsetNotValid = "GeneralErrorTypeOffsetNotValid",
                MissingInfoToUpdate = "GeneralErrorTypeMissingInfoToUpdate"
            }
            enum MessageErrorType {
                InvalidDate = "MessageErrorTypeInvalidDate",
                NotFound = "MessageErrorTypeNotFound",
                NotFoundForUser = "MessageErrorTypeNotFoundForUser",
                MessageAlreadyRead = "MessageErrorTypeMessageAlreadyRead",
                CannotDeleteSystemMessage = "MessageErrorTypeCannotDeleteSystemMessage",
                MissingMessageId = "MessageErrorTypeMissingMessageId",
                IsoDateStringRequired = "MessageErrorTypeIsoDateStringRequired"
            }
            enum UserErrorType {
                AlreadyMuted = "UserErrorTypeAlreadyMuted",
                NotOnMute = "UserErrorTypeNotOnMute",
                AlreadyEnabled = "UserErrorTypeAlreadyEnabled",
                AlreadyDisabled = "UserErrorTypeAlreadyDisabled",
                NotFound = "UserErrorTypeNotFound"
            }
            enum UserRegistrationErrorType {
                ContactCompanyAdmin = "UserRegistrationErrorTypeContactCompanyAdmin"
            }
            enum ConversationErrorType {
                NotFound = "ConversationErrorTypeNotFound",
                CannotConverseWithUser = "ConversationErrorTypeCannotConverseWithUser",
                NotAnOwner = "ConversationErrorTypeNotAnOwner",
                AlreadyAnOwner = "ConversationErrorTypeAlreadyAnOwner",
                NotFoundForApp = "ConversationErrorTypeNotFoundForApp",
                InvalidType = "ConversationErrorTypeInvalidType",
                InvalidConversationGroupType = "ConversationErrorTypeInvalidConversationGroupType",
                InvalidConversationUserType = "ConversationErrorTypeInvalidConversationUserType",
                MustBeOwner = "ConversationErrorTypeMustBeOwner",
                MustBeCreator = "ConversationErrorTypeMustBeCreator",
                NotPartOfApp = "ConversationErrorTypeNotPartOfApp",
                NotFoundForUser = "ConversationErrorTypeNotFoundForUser",
                AlreadyExists = "ConversationErrorTypeAlreadyExists",
                CannotChatWithSelf = "ConversationErrorTypeCannotChatWithSelf",
                AlreadyMuted = "ConversationErrorTypeAlreadyMuted",
                NotOnMute = "ConversationErrorTypeNotOnMute",
                AlreadyPinned = "ConversationErrorTypeAlreadyPinned",
                NotPinned = "ConversationErrorTypeNotPinned",
                UsersNotInApp = "ConversationErrorTypeUsersNotInApp",
                NoValidUsersToAdd = "ConversationErrorTypeNoValidUsersToAdd",
                NoValidUsersToRemove = "ConversationErrorTypeNoValidUsersToRemove",
                CannotChangeAccessToSelf = "ConversationErrorTypeCannotChangeAccessToSelf",
                CannotAddExistingUsers = "ConversationErrorTypeCannotAddExistingUsers",
                AlreadyRemovedUsers = "ConversationErrorTypeAlreadyRemovedUsers",
                UsersNotInConversation = "ConversationErrorTypeUsersNotInConversation",
                CannotRemoveOwners = "ConversationErrorTypeCannotRemoveOwners",
                DivisionsNotInApp = "ConversationErrorTypeDivisionsNotInApp",
                CannotAddExistingDivisions = "ConversationErrorTypeCannotAddExistingDivisions",
                CannotRemoveNonExistingDivisions = "ConversationErrorTypeCannotRemoveNonExistingDivisions",
                DivisionIdOrMemberIdRequiredForSpaceGroup = "ConversationErrorTypeDivisionIdOrMemberIdRequiredForSpaceGroup",
                NudgeLessThanThreshold = "ConversationErrorTypeNudgeLessThanThreshold"
            }
            enum UserManagementErrorType {
                NoSameAdminAssignment = "UserManagementErrorTypeNoSameAdminAssignment",
                MustBeHrAdmin = "UserManagementErrorTypeMustBeHrAdmin",
                NoDuplicateUsers = "UserManagementErrorTypeNoDuplicateUsers",
                NotHrAdmin = "UserManagementErrorTypeNotHrAdmin",
                UserAlreadyAssigned = "UserManagementErrorTypeUserAlreadyAssigned"
            }
            enum DateErrorType {
                InvalidFromDateError = "DateErrorTypeInvalidFromDateError",
                InvalidToDateError = "DateErrorTypeInvalidToDateError"
            }
            enum AppRegistrationErrorType {
                MissingEmailConfigParams = "AppRegistrationErrorTypeMissingEmailConfigParams"
            }
            enum OTPErrorType {
                MissingEmailService = "OTPErrorTypeMissingEmailService"
            }
            enum EmailErrorType {
                MissingEmailService = "EmailErrorTypeMissingEmailService",
                EmailDomainNotAllowed = "EmailErrorTypeEmailDomainNotAllowed"
            }
            enum SmsErrorType {
                NotConfigured = "SmsErrorTypeNotConfigured",
                NotConfiguredForClient = "SmsErrorTypeNotConfiguredForClient",
                RequiredCountryCode = "SmsErrorTypeRequiredCountryCode",
                CountryCodeNotSupported = "SmsErrorTypeCountryCodeNotSupported"
            }
            enum DepartmentErrorTpe {
                NotFound = "DepartmentErrorTypeNotFound",
                AlreadyExists = "DepartmentErrorTypeAlreadyExists",
                MissingInfoToUpdate = "DepartmentErrorTypeMissingInfoToUpdate",
                MissingDefaultDeptOrDiv = "DepartmentErrorTypeMissingDefaultDeptOrDiv",
                UpdateDefaultNotAllowed = "DepartmentErrorTypeUpdateDefaultNotAllowed"
            }
            enum DivisionErrorType {
                NotPartOfDepartment = "DivisionErrorTypeNotPartOfDepartment",
                AlreadyExists = "DivisionErrorTypeAlreadyExists",
                DepartmentRequired = "DivisionErrorTypeDepartmentRequired",
                UpdateDefaultNotAllowed = "DivisionErrorTypeUpdateDefaultNotAllowed"
            }
            enum AppFeatureErrorTpe {
                NotFound = "AppFeatureErrorTpeNotFound",
                AlreadyExists = "AppFeatureErrorTypeAlreadyExists",
                InvalidParams = "AppFeatureErrorTypeInvalidParams",
                AdminPortalLink = "AppFeatureErrorTypeAdminPortalLink"
            }
            enum UserAdminErrorType {
                NotFound = "UserAdminErrorTypeNotFound",
                AlreadyExists = "UserAdminErrorTypeAlreadyExists",
                NoAccessToUserAdmin = "UserAdminErrorTypeNoAccessToUserAdmin",
                NoEmailUpdateAccessOfSignedUpUsers = "UserAdminErrorTypeNoEmailUpdateAccessOfSignedUpUsers",
                CannotUpdateSelf = "UserAdminErrorTypeCannotUpdateSelf",
                UserAdminNoAccessToUser = "UserAdminNoAccessToUser",
                NoChangeInLeaveStatus = "UserAdminErrorTypeNoChangeInLeaveStatus",
                SignUpIncomplete = "UserAdminErrorTypeSignUpIncomplete",
                Disabled = "UserAdminErrorTypeDisabled"
            }
            enum AppConfigErrorType {
                NotFound = "AppConfigErrorTypeNotFound",
                AlreadyExists = "AppConfigErrorTypeAlreadyExists"
            }
            enum KeywordErrorType {
                NoDuplicates = "KeywordErrorTypeNoDuplicates",
                AlreadyExists = "KeywordErrorTypeAlreadyExists",
                NotFound = "KeywordErrorTypeNotFound",
                NoChangeInPriority = "KeywordErrorTypeNoChangeInPriority"
            }
            enum UserAdminUpdateRoleType {
                CannotUpdateRoleOfSelf = "UserAdminUpdateRoleTypeCannotUpdateRoleOfSelf",
                ManagedUserExist = "UserAdminUpdateRoleTypeManagedUserExist",
                AlreadyHasRequestedRole = "UserAdminErrorTypeAlreadyHasRequestedRole"
            }
            enum FlaggedMessageErrorType {
                AlreadyReported = "FlaggedMessageErrorTypeAlreadyReported",
                NotFound = "FlaggedMessageErrorTypeNotFound",
                AlreadyResolved = "FlaggedMessageErrorTypeAlreadyResolved"
            }
            enum UserOnboardingJobErrorType {
                NotFound = "UserOnboardingJobErrorTypeNotFound",
                NotCompleted = "UserOnboardingJobErrorTypeNotCompleted"
            }
            enum ExternalUserErrorType {
                ContactAlreadyExists = "ExternalUserErrorTypeContactAlreadyExists",
                NotFound = "ExternalUserErrorTypeNotFound",
                UserDisabled = "ExternalUserErrorTypeUserDisabled",
                InvalidPhoneNumber = "ExternalUserErrorTypeInvalidPhoneNumber"
            }
            enum ContactErrorType {
                ContactAlreadyExists = "ContactErrorTypeContactAlreadyExists"
            }
            enum SmsConversationErrorType {
                NotAnExternalUser = "SmsConversationErrorTypeNotAnExternalUser",
                ExternalUserNotFound = "SmsConversationErrorTypeNotFoundExternalUserNotFound"
            }
            enum SmsConversationInvitationErrorType {
                NotFound = "SmsConversationInvitationErrorTypeNotFound",
                InvitationNotAccepted = "SmsConversationInvitationErrorTypeInvitationNotAccepted",
                InvitationAlreadyAccepted = "SmsConversationInvitationErrorTypeInvitationAlreadyAccepted",
                InvitationExpired = "SmsConversationInvitationErrorTypeInvitationExpired"
            }
        }
        enum PushNotificationError {
            InvalidPushParams = "DeviceTokenErrorTypeInvalidPushParams",
            AlreadyRegisteredForUser = "DeviceTokenErrorTypeAlreadyRegisteredForUser",
            NotFound = "DeviceTokenErrorTypeNotFound",
            NoTokensToPush = "DeviceTokenErrorTypeNoTokensToPush",
            OnlyStringData = "DeviceTokenErrorTypeOnlyStringData"
        }
        type Type = ErrorType.Type | PushNotificationError;
        class AppError {
            type: Type;
            statusCode?: number;
            error?: Error;
            message?: string;
            authFailed?: boolean;
            exchange?: boolean;
            constructor(err?: string | Error, type?: Type);
            setStatusCode: (statusCode?: number | undefined) => void;
            setType: (type: Type) => void;
            setMessage: (msg: string) => void;
            setErrorActions: (params: {
                authFailed?: boolean | undefined;
                exchange?: boolean | undefined;
            }) => void;
        }
    }
    export import EventType = Publisher.EventType;
    export import ErrorType = ChatKitError.ErrorType;
    export import Event = Event.AllEvents;
    export import Error = ChatKitError.AppError;
}
interface SendMessageParamsBase {
    conversationId: string;
    clientRefId: string;
}
interface SendTextMessageParams extends SendMessageParamsBase {
    type: MessageType.Text;
    text: string;
}
interface SendEmojiMessageParams extends SendMessageParamsBase {
    type: MessageType.Emoji;
    text: string;
}
interface SendGifMessageParams extends SendMessageParamsBase {
    type: MessageType.Gif;
    text?: string;
    url: string;
}
interface SendFileMessageParams extends SendMessageParamsBase {
    type: MessageType.Image | MessageType.Audio | MessageType.Video | MessageType.Document;
    file: FileWithText;
    progress?: (percentage: number) => void;
}
export declare type SendMessageParams = SendTextMessageParams | SendEmojiMessageParams | SendGifMessageParams | SendFileMessageParams;
export interface UserManagementCountQueryParams {
    active?: boolean;
    inactive?: boolean;
    temp?: boolean;
    perm?: boolean;
    activeAndPerm?: boolean;
    activeAndTemp?: boolean;
    inactiveAndTemp?: boolean;
    inactiveAndPerm?: boolean;
}
export interface UserManagementCountResultParams {
    active?: number;
    inactive?: number;
    temp?: number;
    perm?: number;
    activeAndPerm?: number;
    activeAndTemp?: number;
    inactiveAndTemp?: number;
    inactiveAndPerm?: number;
}
export interface ConversationReactionsDTO {
    messageId: string;
    type: ReactionType | ReactionType[];
}
export declare type ConversationReactionsDomainEntity = Partial<{
    [messageId: string]: ReactionType[];
}>;
export declare type UserSelfDTO = UserDTO & {
    muteAllUntil: Date | null;
};
export declare type UserSelfDomainEntity = UserDomainEntity & {
    muteAllUntil: Date | null;
};
export interface DefaultGroupImageUrlSet {
    filename: string;
    original: string;
    thumbnail: string;
}
export interface UserSearchPaginateResponse extends SearchPaginateResponse {
    includeRefIdInSearch: boolean;
}
export interface ReactedUserDTO {
    type: ReactionType;
    sequence: number;
    createdAt: Date;
    messageId: string;
    userId: string;
    user: UserDTO;
}
export declare enum tempDefaultColumn {
    isDefault = 1
}
export declare type DepartmentSearchPaginateParams = SearchPaginateParams;
export declare type DepartmentSearchPaginateResponse = SearchPaginateResponse;
export declare type DivisionSearchPaginateParams = SearchPaginateParams;
export declare type DivisionSearchPaginateResponse = SearchPaginateResponse;
export declare enum SortOrderType {
    ASC = 1,
    DESC = -1
}
export declare enum DepartmentSortFields {
    DepartmentName = "departmentName"
}
export declare enum DivisionSortFields {
    DivisionName = "divisionName"
}
export interface DepartmentDTO {
    id: string;
    name: string;
    appId: string;
    deletedAt: Date | null;
    isDefault: tempDefaultColumn | null;
    divisions?: DivisionDTO[];
}
export interface DepartmentWithDivisionCountDTO extends DepartmentDTO {
    divisionCount: number;
}
export interface DepartmentBatchDTO extends DepartmentSearchPaginateResponse {
    results: DepartmentWithDivisionCountDTO[];
}
export interface DivisionDTO {
    id: string;
    name: string;
    departmentId: string;
    deletedAt: Date | null;
    isDefault: boolean;
}
export interface DivisionBatchDTO extends DivisionSearchPaginateResponse {
    results: DivisionDTO[];
}
export declare type UserFullInfoDTO = UserDTO & {
    title: string | null;
    employeeId: string | null;
    departmentId: string | null;
    department: DepartmentDTO | null;
    divisionId: string | null;
    division: DivisionDTO | null;
    isAutoAssigned: boolean;
};
export interface InternalUserFullInfoDTO {
    id: string;
    email: string | null;
    firstName: string;
    lastName: string;
    refId: string | null;
    picture: MediaDTO | null;
    disabled: boolean;
    deletedAt: Date | null;
    userScope: UserScope;
    title: string | null;
    employeeId: string | null;
    departmentId: string;
    department: DepartmentDTO;
    divisionId: string;
    division: DivisionDTO;
    isAutoAssigned: boolean;
}
export interface UserBasicInfoCommon {
    id: string;
    firstName: string;
    lastName: string;
    userScope: UserScope;
}
export interface UserBasicInfoDTOInternal extends UserBasicInfoCommon {
    userScope: UserScope.Internal;
    email: string;
}
export interface UserBasicInfoDTOExternal extends UserBasicInfoCommon {
    userScope: UserScope.External;
    phone: PhoneWithCode;
}
export declare type UserBasicInfoDTO = UserBasicInfoDTOInternal | UserBasicInfoDTOExternal;
export declare type UserBasicInfoSearchPaginateResponse = SearchPaginateResponse;
export interface UserBasicInfoBatchDTO extends UserBasicInfoSearchPaginateResponse {
    results: UserBasicInfoDTO[];
}
export interface UserManagementDTO {
    id: string;
    userId: string;
    currentUserAdminId: string;
    permanentUserAdminId: string;
    user: InternalUserFullInfoDTO;
    currentUserAdmin: UserAdminDTO;
    permanentUserAdmin: UserAdminDTO;
}
export interface UserManagementBatchDTO extends UserManagementSearchPaginateResponse {
    results: UserManagementDTO[];
}
export interface UnmanagedUsersBatchDTO extends UnmanagedUsersSearchPaginateResponse {
    results: InternalUserFullInfoDTO[];
}
export interface UnmanagedUsersCountDTO {
    count: number;
}
export interface UserAdminDTO {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    role: UserTypes.HrAdmin | UserTypes.SuperHrAdmin;
    appId: string;
    disabled: boolean;
    verified: boolean;
    isActive: boolean;
    deletedAt: Date | null;
    signUpComplete: boolean;
    onLeave: boolean;
    roleTitle: string;
}
export interface UserAdminBatchDTO extends UserAdminSearchPaginateResponse {
    results: UserAdminDTO[];
}
export declare enum TokenType {
    AccessToken = "TokenTypeAccessToken",
    RefreshToken = "TokenTypeRefreshToken",
    PublisherToken = "TokenTypePublisherToken",
    ResetPasswordToken = "TokenTypeResetPasswordToken",
    NewUserSetPasswordToken = "TokenTypeNewUserSetPasswordToken",
    VerifyAccountToken = "TokenTypeVerifyAccountToken"
}
export interface ValidateSetPasswordOTPDTO {
    setPasswordToken: string;
}
export interface ValidateResetPasswordOTPDTO {
    resetPasswordToken: string;
}
export interface AppConfigDTO {
    appId: string;
    privacyPolicyUrl: string | null;
    termsAndConditionsUrl: string | null;
    quickGroupUserSelectionLimit: number;
    nudgeDelayHours: number;
}
export declare enum TimeFrameType {
    AllTime = "allTime",
    Month = "month",
    Year = "year"
}
export interface SentMessageCountDailyDTO {
    appId: string;
    messageCount: number;
    createdAt: Date;
    updatedAt: Date;
    dateStringWithHourOnly: string;
}
export declare enum UnresolvedMsgScopeType {
    Global = "global",
    Mine = "mine"
}
export interface UnresolvedFlaggedMessagesCountDTO {
    highUnresolvedCount: number;
    lowUnresolvedCount: number;
    userReportedUnresolvedCount: number;
}
export interface FlaggedMessageCountDailyDTO {
    flaggedMessageCount: number;
    createdAt: Date;
    updatedAt: Date;
    dateStringWithHourOnly: string;
}
export interface AllFlaggedMessageCountDailyDTO {
    highFlaggedMessages: FlaggedMessageCountDailyDTO[];
    lowFlaggedMessages: FlaggedMessageCountDailyDTO[];
    userReportedFlaggedMessages: FlaggedMessageCountDailyDTO[];
}
export declare enum KeyWordPriorityTypes {
    High = "high",
    Low = "low"
}
export interface KeywordDTO {
    id: string;
    term: string;
    priority: KeyWordPriorityTypes;
    appId: string;
    deletedAt: Date | null;
}
export declare type KeywordSearchPaginateResponse = SearchPaginateResponse;
export interface KeywordBatchDTO extends KeywordSearchPaginateResponse {
    results: KeywordDTO[];
}
export declare enum FlaggedMessageType {
    'HIGH' = "high",
    'LOW' = "low",
    'USER_REPORTED' = "user_reported"
}
export declare enum QueryCondition {
    AND = "and",
    OR = "or"
}
export declare type AuditMessagSearchPaginateResponse = Omit<SearchPaginateResponse, 'search'>;
export interface AuditMessageBatchDTO extends AuditMessagSearchPaginateResponse {
    results: AuditMessageDTO[];
}
export interface AuditMessageDomainEntityBatchDTO extends AuditMessagSearchPaginateResponse {
    results: AuditMessageDomainEntityDTO[];
}
export declare type SentMessageDTO = Omit<MessageDTO, 'systemMessage'>;
export interface UserSearchCriteria {
    userId?: string;
    text?: string[];
    hasAttachment?: boolean;
    startDate?: string;
    endDate?: string;
    textSearchCondition?: QueryCondition;
    hasFlagged?: boolean;
}
export interface UserAdminSearchDTO {
    id: string;
    userAdminId: string;
    criteria: UserSearchCriteria | null;
}
export interface FlaggedMessageFlagTypeCount {
    unresolvedCount: number;
    resolvedCount: number;
}
export interface FlaggedMessageCount {
    high: FlaggedMessageFlagTypeCount;
    low: FlaggedMessageFlagTypeCount;
    userReport: FlaggedMessageFlagTypeCount;
}
export interface FlaggedMessageMinimalDomain {
    id: string;
    resolved: boolean;
    messageDeleted: boolean;
    resolvedAt: Date | null;
    messageId: string;
    createdAt: Date;
    updatedAt: Date;
}
export interface UserMessageDTO {
    id: string;
    conversationId: string;
    createdAt: Date;
    sender: UserDTO | null;
    type: MessageType;
    text: string | null;
    refUrl: string | null;
    sequence: number;
    file: MediaDTO | null;
    reactions: ReactionsObject;
    deletedAt: Date | null;
}
export interface FlaggedMessageDTO extends FlaggedMessageMinimalDomain {
    message: UserMessageDTO;
    keywords: {
        high: {
            keywordId: string;
            keyword: string;
        }[];
        low: {
            keywordId: string;
            keyword: string;
        }[];
    };
    unresolvedUserReports: number;
}
export interface FlaggedMessageDetailsDTO extends FlaggedMessageMinimalDomain {
    message: UserMessageDTO;
    keywords: {
        high: {
            keywordId: string;
            keyword: string;
        }[];
        low: {
            keywordId: string;
            keyword: string;
        }[];
    };
    count: FlaggedMessageCount;
}
export interface FlaggedMessageBatchDTO extends FlaggedMessageSearchPaginateResponse {
    results: FlaggedMessageDTO[];
}
export declare type FlaggedMessageSearchPaginateResponse = SearchPaginateResponse;
export interface FlaggedMessageCountDTO {
    highUnresolved: 5;
    lowUnresolved: 5;
    userReportedUnresolved: 5;
    resolved: 3;
}
export interface FlaggedMessageUserReportDTO {
    flaggedMessageId: string;
    resolved: boolean;
    updatedAt: Date;
    reporter: UserMinimal;
    reason: string;
    flaggedResolutionId: string | null;
    resolvedAt: Date;
}
export interface FlaggedMessageUserReportBatchDTO extends FlaggedMessageUserReportPaginateResponse {
    results: FlaggedMessageUserReportDTO[];
}
export interface FlaggedMessageUserReportPaginateResponse extends PaginateResponse {
    resolved: boolean;
}
export interface PaginateResponse {
    totalCount: number;
    offset: number;
    nextOffset: number;
    limit: number;
    completed: boolean;
}
export interface UserMinimalCommon {
    id: string;
    firstName: string;
    lastName: string;
    refId: string | null;
    profileImageId: string | null;
    profileImage: MediaDomain | null;
    profileImageLocalId: string | null;
    disabled: boolean;
    deletedAt: Date | null;
    isPreVerified: boolean;
    userScope: UserScope;
}
export interface UserMinimalInternal extends UserMinimalCommon {
    userScope: UserScope.Internal;
    email: string;
    passwordHash: string | null;
    isEmailVerified: boolean;
    isAutoAssigned: boolean;
}
export interface UserMinimalExternal extends UserMinimalCommon {
    userScope: UserScope.External;
    phone: PhoneWithCode;
    isPhoneVerified: boolean;
}
export declare type UserMinimal = UserMinimalInternal | UserMinimalExternal;
export interface AuditMessageDomainEntityDTO extends SentMessageDomainEntity {
    flagged: boolean;
    resolved?: boolean;
}
export interface MediaDomain {
    id: string;
    appId: string;
    creatorId: string | null;
    mimeType: string;
    extension: string;
    encoding: string;
    filename: string;
    fileSize: number;
    fileType: FileType;
    s3Key: string;
    s3Url: string;
    imageMetadataDomain: ImageMetadataDomain | null;
    videoMetadataDomain: {
        duration: number;
        thumbnail: MediaDomain | null;
    } | null;
}
export interface ImageMetadataDomain {
    fileId: string;
    width: number;
    height: number;
    subImagesMinimal: SubImageMinimal[];
}
export interface SubImageInfoMedia {
    width: number;
    height: number;
    fileSize: number;
    max: number;
    imageSize: ImageSizes;
}
export interface AuditMessageDTO extends SentMessageDTO {
    flagged: boolean;
    resolved?: boolean;
}
export declare enum FlaggedMessagePriorityType {
    'HIGH' = "high",
    'LOW' = "low"
}
export interface FlaggedMessageScanMinimalDomain {
    flaggedMessageId: string;
    resolved: boolean;
    updatedAt: Date;
    flaggedResolutionId: string | null;
    createdAt: Date;
    scannedAt: Date | null;
    id: string;
    priorityType: FlaggedMessagePriorityType.HIGH | FlaggedMessagePriorityType.LOW;
    totalMetrics: number;
}
export interface FlaggedMessageScanDTO extends FlaggedMessageScanMinimalDomain {
    keywords: {
        high: {
            keywordId: string;
            keyword: string;
        }[];
        low: {
            keywordId: string;
            keyword: string;
        }[];
    };
}
export interface FlaggedMessageScanBatchDTO extends FlaggedMessageScanPaginateResponse {
    results: FlaggedMessageScanDTO[];
}
export interface FlaggedMessageScanPaginateResponse extends PaginateResponse {
    resolved: boolean;
}
export declare type FlaggedMessageResolutionPaginateResponse = PaginateResponse;
export interface FlaggedMessageResolutionDTO {
    createdAt: Date;
    updatedAt: Date;
    id: string;
    flaggedMessageId: string;
    resolved: boolean;
    resolvedAt: Date | null;
    messageDeleted: boolean;
    resolutionStatus: ResolutionStatusType;
    resolvedBy: UserAdminDTO;
    resolutionNotes: string | null;
}
export interface FlaggedMessageResolutionBatchDTO extends FlaggedMessageResolutionPaginateResponse {
    results: FlaggedMessageResolutionDTO[];
}
export declare enum ResolutionStatusType {
    'NO_ACTION' = "no_action",
    'ACTION_TAKEN' = "action_taken"
}
export interface FlaggedMessageResolutionUserReportBatchDTO extends FlaggedMessageResolutionPaginateResponse {
    results: FlaggedMessageUserReportDTO[];
}
export interface FlaggedMessageResolutionScanBatchDTO extends FlaggedMessageResolutionPaginateResponse {
    results: FlaggedMessageScanDTO[];
}
export interface GroupDisplayDetail {
    picture: MediaDTO | null;
    name: string;
    type: ConversationGroupType;
}
export declare type ConversationSearchPaginateResponse = SearchPaginateResponse;
export interface ConversationGroupDTOForUserAdmin extends GroupConversationDTO {
    group: GroupDisplayDetail;
    userAdminOwnerId: string;
}
export interface ConversationGroupDTOForUserAdminWithUserCount extends ConversationGroupDTOForUserAdmin {
    conversationUsersCount: number;
}
export interface ConversationGroupBatchDTOForUserAdminWithUserCount extends ConversationSearchPaginateResponse {
    results: ConversationGroupDTOForUserAdminWithUserCount[];
}
export interface DepartmentDivisionDTO {
    id: string;
    name: string;
    appId: string;
    deletedAt: Date | null;
    isDefault: tempDefaultColumn | null;
    divisions: DivisionDTO[];
}
export interface TotalUsersCountDTO {
    activeUsersCount: number;
    inActiveUsersCount: number;
    totalUsersCount: number;
}
export interface UserRole {
    type: UserTypes;
    title: string;
}
export declare enum UserOnboardingJobStatus {
    Completed = "completed",
    Pending = "pending",
    InProgress = "in_progress"
}
export interface UserOnboardingJobMinDTO {
    id: string;
    appId: string;
    fileName: string;
    status: UserOnboardingJobStatus;
    uploadedById: string;
    departmentId: string;
    divisionId: string;
    createdAt: Date;
}
export interface UserOnboardingJobDTO extends UserOnboardingJobMinDTO {
    department: DepartmentDTO;
    division: DivisionDTO;
}
export declare type UserOnboardingJobSearchPaginateResponse = SearchPaginateResponse;
export interface UserOnboardingJobBatchDTO extends UserOnboardingJobSearchPaginateResponse {
    results: UserOnboardingJobDTO[];
}
export declare enum UserOnboardingJobRecordStatus {
    Completed = "completed",
    Pending = "pending",
    Errored = "errored"
}
export interface UserOnboardingJobRecordMinDTO {
    id: string;
    userOnboardingJobId: string;
    firstName: string | null;
    lastName: string | null;
    email: string | null;
    title: string | null;
    employeeId: string | null;
    status: UserOnboardingJobRecordStatus;
    error: string | null;
}
export interface CreateExternalUserParams {
    userId: string;
}
export interface FindByPhoneQueryParams {
    phone: string;
}
export interface CreateNewContact {
    firstName: string;
    lastName: string;
    externalUserInfo: {
        phone: PhoneWithCode;
    };
}
export declare type InternalUserContactsSearchPaginateResponse = SearchPaginateResponse;
export interface InternalUserContactDTO {
    internalUserId: string;
    externalUserId: string;
    externalUser: UserBasicInfoDTOExternal;
}
export interface InternalUserContactsBatchDTO extends InternalUserContactsSearchPaginateResponse {
    results: InternalUserContactDTO[];
}
export interface ExternalUserContactDTO {
    isOwnContact: boolean;
    externalUserId: string;
    externalUser: UserBasicInfoDTOExternal;
}
export interface ExternalUserTermsAndPrivacyVersionDTO {
    appId: string;
    userId: string;
    termsAndPrivacyVersion: number;
    acceptedTermsAndPrivacyAt: Date | null;
}
export interface ExternalUserOptToReceiveSmsDTO {
    appId: string;
    userId: string;
    hasOptedToReceiveSms: boolean;
}
export interface PhoneWithCode {
    phoneNumber: string;
    countryCode: string;
}
export interface FindSmsConversation {
    externalUserId: string;
}
export interface SmsConversationInvitationDTO {
    id: string;
    conversationGroupInfoId: string;
    senderId: string;
    receiverId: string;
    expiresAt: Date;
    acceptedAt: Date | null;
    resentAt: Date | null;
    createdAt: Date | null;
}
export interface SmsConversationInvitationUserDTO {
    id: string;
    conversationGroupInfoId: string;
    senderId: string;
    receiverId: string;
    expiresAt: Date;
    acceptedAt: Date | null;
    resentAt: Date | null;
    createdAt: Date;
    sender: UserBasicInfoDTOInternal;
    receiver: UserBasicInfoDTOExternal;
}
export {};
