"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatCommonRoutes = exports.ChatAdminRoutes = exports.ChatClientRoutes = exports.MESSAGE_READ_UPDATE_INTERVAL = exports.ApiKeyName = exports.API_VERSION = void 0;
var types_1 = require("./types");
exports.API_VERSION = 'v1';
exports.ApiKeyName = 'x-sayhey-client-api-key';
exports.MESSAGE_READ_UPDATE_INTERVAL = 5000;
exports.ChatClientRoutes = {
    SignIn: {
        Endpoint: '/users/sign-in',
        Method: types_1.HttpMethod.Post
    },
    SignUp: {
        Endpoint: '/users/sign-up',
        Method: types_1.HttpMethod.Post
    },
    ChangePassword: {
        Endpoint: '/users/change-password',
        Method: types_1.HttpMethod.Post
    },
    ForgotPassword: {
        Endpoint: '/users/forgot-password',
        Method: types_1.HttpMethod.Post
    },
    ResetPassword: {
        Endpoint: '/users/reset-password',
        Method: types_1.HttpMethod.Post
    },
    GetExistingConversationWithUser: {
        Endpoint: function (userId) { return "/users/" + userId + "/conversations/individual"; },
        Method: types_1.HttpMethod.Get
    },
    GetExistingIndvidualOrQuickConversation: {
        Endpoint: '/conversations/individual-quick',
        Method: types_1.HttpMethod.Get
    },
    GetConversations: {
        Endpoint: '/conversations',
        Method: types_1.HttpMethod.Get
    },
    CreateIndividualConversation: {
        Endpoint: '/conversations/individual',
        Method: types_1.HttpMethod.Post
    },
    CreateGroupConversation: {
        Endpoint: '/conversations/group',
        Method: types_1.HttpMethod.Post
    },
    CreateQuickConversation: {
        Endpoint: '/conversations/quick-group',
        Method: types_1.HttpMethod.Post
    },
    GetMessages: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/messages"; },
        Method: types_1.HttpMethod.Get
    },
    SendMessage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/messages"; },
        Method: types_1.HttpMethod.Post
    },
    MuteConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/mute"; },
        Method: types_1.HttpMethod.Put
    },
    UnmuteConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/mute"; },
        Method: types_1.HttpMethod.Delete
    },
    PinConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/pin"; },
        Method: types_1.HttpMethod.Put
    },
    UnpinConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/pin"; },
        Method: types_1.HttpMethod.Delete
    },
    DeleteConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/hide-clear"; },
        Method: types_1.HttpMethod.Put
    },
    DeleteAllConversations: {
        Endpoint: "/conversations/hide-clear",
        Method: types_1.HttpMethod.Put
    },
    UpdateUserProfileImage: {
        Endpoint: '/users/profile-image',
        Method: types_1.HttpMethod.Put
    },
    UpdateUserProfileInfo: {
        Endpoint: '/users/profile-info',
        Method: types_1.HttpMethod.Put
    },
    GetConversationReachableUsers: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/conversable-users"; },
        Method: types_1.HttpMethod.Get
    },
    GetUserConversationReactions: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/reactions"; },
        Method: types_1.HttpMethod.Get
    },
    GetUserConversationMessagesReactions: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/get-messages-reactions"; },
        Method: types_1.HttpMethod.Post
    },
    SetMessageReaction: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/reactions/add"; },
        Method: types_1.HttpMethod.Put
    },
    RemoveMessageReaction: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/reactions/remove"; },
        Method: types_1.HttpMethod.Put
    },
    MuteAllNotifications: {
        Endpoint: '/users/mute-all',
        Method: types_1.HttpMethod.Put
    },
    UnmuteAllNotifications: {
        Endpoint: '/users/mute-all',
        Method: types_1.HttpMethod.Delete
    },
    GetUserProfile: {
        Endpoint: '/users/profile-info',
        Method: types_1.HttpMethod.Get
    },
    MarkMessageAsRead: {
        Endpoint: function (messageId) { return "/read-messages/" + messageId; },
        Method: types_1.HttpMethod.Put
    },
    GetConversationBasicUsers: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/basic-users"; },
        Method: types_1.HttpMethod.Get
    },
    RegisterPushNotification: {
        Endpoint: '/device-tokens',
        Method: types_1.HttpMethod.Post
    },
    UnregisterPushNotification: {
        Endpoint: function (instanceId) { return "/device-tokens/" + instanceId; },
        Method: types_1.HttpMethod.Delete
    },
    SetPassword: {
        Endpoint: '/users/set-password',
        Method: types_1.HttpMethod.Post
    },
    CheckEmail: {
        Endpoint: '/users/email-check',
        Method: types_1.HttpMethod.Post
    },
    RequestSignUpOTP: {
        Endpoint: '/otp/sign-up',
        Method: types_1.HttpMethod.Post
    },
    RequestSetPasswordOTP: {
        Endpoint: '/otp/set-password',
        Method: types_1.HttpMethod.Post
    },
    RequestResetPasswordOTP: {
        Endpoint: '/otp/reset-password',
        Method: types_1.HttpMethod.Post
    },
    RequestPreverificationOTP: {
        Endpoint: '/otp/pre-verification',
        Method: types_1.HttpMethod.Post
    },
    ValidateSignUpOTP: {
        Endpoint: '/otp/validate-sign-up',
        Method: types_1.HttpMethod.Post
    },
    ValidateSetPasswordOTP: {
        Endpoint: '/otp/validate-set-password',
        Method: types_1.HttpMethod.Post
    },
    ValidateResetPasswordOTP: {
        Endpoint: '/otp/validate-reset-password',
        Method: types_1.HttpMethod.Post
    },
    ValidatePreverificationOTP: {
        Endpoint: '/otp/validate-pre-verification',
        Method: types_1.HttpMethod.Post
    },
    GetAppConfig: {
        Endpoint: '/app-config',
        Method: types_1.HttpMethod.Get
    },
    ClientSignInWithToken: {
        Endpoint: '/users/client-signin-with-token',
        Method: types_1.HttpMethod.Post
    },
    CreateFlaggedMessage: {
        Endpoint: '/flagged-messages',
        Method: types_1.HttpMethod.Post
    },
    /* SMS */
    AddToContact: {
        Endpoint: 'contacts',
        Method: types_1.HttpMethod.Post
    },
    GetInternalUserContactsBatch: {
        Endpoint: 'contacts',
        Method: types_1.HttpMethod.Get
    },
    FindExternalUserByPhone: {
        Endpoint: 'external-users',
        Method: types_1.HttpMethod.Get
    },
    RequestSignInExternalOTP: {
        Endpoint: 'otp/sign-in-external',
        Method: types_1.HttpMethod.Post
    },
    SignInExternal: {
        Endpoint: 'users/sign-in-external',
        Method: types_1.HttpMethod.Post
    },
    SignInExternalByUserId: {
        Endpoint: function (id) { return "users/" + id + "/sign-in-external'"; },
        Method: types_1.HttpMethod.Post
    },
    CreateNewContact: {
        Endpoint: 'users',
        Method: types_1.HttpMethod.Post
    },
    AcceptTermsAndPrivacy: {
        Endpoint: 'external-users/accepted-terms-privacy',
        Method: types_1.HttpMethod.Post
    },
    GetTermsAndPrivacy: {
        Endpoint: 'external-users/accepted-terms-privacy',
        Method: types_1.HttpMethod.Get
    },
    GetExistingSmsConversation: {
        Endpoint: 'conversations/existing-sms-conversations',
        Method: types_1.HttpMethod.Get
    },
    CheckSmsConversationInvitation: {
        Endpoint: function (conversationId) {
            return "conversations/" + conversationId + "/smsConversationInvitations/status";
        },
        Method: types_1.HttpMethod.Get
    },
    AcceptSmsConversationInvitation: {
        Endpoint: function (conversationId) {
            return "conversations/" + conversationId + "/smsConversationInvitations/status";
        },
        Method: types_1.HttpMethod.Patch
    },
    ResendSmsConversationInvitation: {
        Endpoint: function (conversationId) {
            return "conversations/" + conversationId + "/smsConversationInvitations";
        },
        Method: types_1.HttpMethod.Put
    },
    CreateSmsConversation: {
        Endpoint: 'conversations/sms-group',
        Method: types_1.HttpMethod.Post
    },
    OptToReceiveSms: {
        Endpoint: 'external-users/opted-receive-sms',
        Method: types_1.HttpMethod.Put
    },
    OptNotToReceiveSms: {
        Endpoint: 'external-users/opted-receive-sms',
        Method: types_1.HttpMethod.Delete
    },
    GetOptToReceiveSmsStatus: {
        Endpoint: 'external-users/opted-receive-sms',
        Method: types_1.HttpMethod.Get
    },
    NudgeExternalUserToConversation: {
        Endpoint: function (conversationId) {
            return "conversations/" + conversationId + "/nudge-unread-sms-conversation";
        },
        Method: types_1.HttpMethod.Put
    }
};
exports.ChatAdminRoutes = {
    /* User admins */
    Invite: {
        Endpoint: '/user-admins/invitation',
        Method: types_1.HttpMethod.Post
    },
    SignUp: {
        Endpoint: '/user-admins/sign-up',
        Method: types_1.HttpMethod.Post
    },
    SignIn: {
        Endpoint: '/user-admins/sign-in',
        Method: types_1.HttpMethod.Post
    },
    ReInvite: {
        Endpoint: function (userAdminId) { return "/user-admins/" + userAdminId + "/reinvitation"; },
        Method: types_1.HttpMethod.Post
    },
    DisableAdmin: {
        Endpoint: function (userAdminId) { return "/user-admins/" + userAdminId + "/disable"; },
        Method: types_1.HttpMethod.Put
    },
    EnableAdmin: {
        Endpoint: function (userAdminId) { return "/user-admins/" + userAdminId + "/enable"; },
        Method: types_1.HttpMethod.Put
    },
    ForgotPassword: {
        Endpoint: '/user-admins/forgot-password',
        Method: types_1.HttpMethod.Post
    },
    ResetPassword: {
        Endpoint: '/user-admins/reset-password',
        Method: types_1.HttpMethod.Post
    },
    GetUserAdmins: {
        Endpoint: '/user-admins',
        Method: types_1.HttpMethod.Get
    },
    UpdateUserAdminInfo: {
        Endpoint: function (userAdminId) { return "/user-admins/" + userAdminId; },
        Method: types_1.HttpMethod.Patch
    },
    UpdateUserAdminRole: {
        Endpoint: function (userAdminId) { return "/user-admins/" + userAdminId + "/role"; },
        Method: types_1.HttpMethod.Patch
    },
    UpdateUserAdminOnLeave: {
        Endpoint: '/user-admins/onLeave',
        Method: types_1.HttpMethod.Patch
    },
    GetUserAdminSelfInfo: {
        Endpoint: "/user-admins/" + types_1.APISignature.ME,
        Method: types_1.HttpMethod.Get
    },
    UpdateUserAdminSelfInfo: {
        Endpoint: "/user-admins/" + types_1.APISignature.ME,
        Method: types_1.HttpMethod.Patch
    },
    /* User Management */
    AssignManagedUsers: {
        Endpoint: '/user-managements',
        Method: types_1.HttpMethod.Post
    },
    MoveAllManagedUsers: {
        Endpoint: '/user-managements',
        Method: types_1.HttpMethod.Put
    },
    MoveBackAllManagedUsers: {
        Endpoint: function (userAdminId) { return "/user-managements/" + userAdminId + "/movement"; },
        Method: types_1.HttpMethod.Put
    },
    GetManagedUsers: {
        Endpoint: '/user-managements',
        Method: types_1.HttpMethod.Get
    },
    GetManagedUsersCount: {
        Endpoint: '/user-managements/count',
        Method: types_1.HttpMethod.Get
    },
    MoveSelectedUsers: {
        Endpoint: '/user-managements/users/move-selected',
        Method: types_1.HttpMethod.Put
    },
    /* Users */
    RegisterUser: {
        Endpoint: '/users/registration',
        Method: types_1.HttpMethod.Post
    },
    DisableUser: {
        Endpoint: function (userId) { return "/users/" + userId + "/disabled"; },
        Method: types_1.HttpMethod.Put
    },
    EnableUser: {
        Endpoint: function (userId) { return "/users/" + userId + "/disabled"; },
        Method: types_1.HttpMethod.Delete
    },
    UpdateUser: {
        Endpoint: function (userId) { return "/users/" + userId; },
        Method: types_1.HttpMethod.Put
    },
    GetUnmanagedUsers: {
        Endpoint: '/users/unmanaged',
        Method: types_1.HttpMethod.Get
    },
    GetUnmanagedUsersCount: {
        Endpoint: '/users/unmanaged-count',
        Method: types_1.HttpMethod.Get
    },
    /* Departments */
    CreateDepartment: {
        Endpoint: '/departments',
        Method: types_1.HttpMethod.Post
    },
    GetDepartments: {
        Endpoint: '/departments',
        Method: types_1.HttpMethod.Get
    },
    UpdateDepartments: {
        Endpoint: function (departmentId) { return "/departments/" + departmentId; },
        Method: types_1.HttpMethod.Put
    },
    /* Divisions */
    CreateDivisions: {
        Endpoint: function (departmentId) { return "/departments/" + departmentId + "/divisions"; },
        Method: types_1.HttpMethod.Post
    },
    GetDivisions: {
        Endpoint: function (departmentId) { return "/departments/" + departmentId + "/divisions"; },
        Method: types_1.HttpMethod.Get
    },
    UpdateDivisions: {
        Endpoint: function (_a) {
            var departmentId = _a.departmentId, divisionId = _a.divisionId;
            return "/departments/" + departmentId + "/divisions/" + divisionId;
        },
        Method: types_1.HttpMethod.Put
    },
    /* Keywords */
    CreateKeyword: {
        Endpoint: '/keywords',
        Method: types_1.HttpMethod.Post
    },
    CreateKeywordBulk: {
        Endpoint: '/keywords/bulk',
        Method: types_1.HttpMethod.Post
    },
    DeleteKeyword: {
        Endpoint: function (id) { return "/keywords/" + id; },
        Method: types_1.HttpMethod.Delete
    },
    UpdateKeyword: {
        Endpoint: function (id) { return "/keywords/" + id; },
        Method: types_1.HttpMethod.Patch
    },
    GetKeywordsBatch: {
        Endpoint: '/keywords',
        Method: types_1.HttpMethod.Get
    },
    GetMessagesBeforeSequence: {
        Endpoint: function (conversationId) { return "/conversation/" + conversationId + "/before-sequence"; },
        Method: types_1.HttpMethod.Get
    },
    GetMessagesAfterSequence: {
        Endpoint: function (conversationId) { return "/conversation/" + conversationId + "/after-sequence"; },
        Method: types_1.HttpMethod.Get
    },
    GetMessageContext: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/context"; },
        Method: types_1.HttpMethod.Get
    },
    GetAuditMessagesBatch: {
        Endpoint: '/messages/audit',
        Method: types_1.HttpMethod.Get
    },
    DownloadAuditMessages: {
        Endpoint: '/messages/audit-download',
        Method: types_1.HttpMethod.Get
    },
    CreateUpdateUserAdminSearch: {
        Endpoint: '/user-admin-searches',
        Method: types_1.HttpMethod.Put
    },
    GetUserAdminSearch: {
        Endpoint: '/user-admin-searches',
        Method: types_1.HttpMethod.Get
    },
    GetUserBasicInfoBatch: {
        Endpoint: '/users',
        Method: types_1.HttpMethod.Get
    },
    // Monitoring or Flagged Messages
    GetFlaggedMessagesBatch: {
        Endpoint: '/flagged-messages',
        Method: types_1.HttpMethod.Get
    },
    GetFlaggedMessagesCount: {
        Endpoint: '/flagged-messages/count',
        Method: types_1.HttpMethod.Get
    },
    ResolveFlaggedMessages: {
        Endpoint: function (id) { return "/flagged-messages/" + id + "/resolve"; },
        Method: types_1.HttpMethod.Post
    },
    GetFlaggedMessageDetails: {
        Endpoint: function (id) { return "/flagged-messages/" + id; },
        Method: types_1.HttpMethod.Get
    },
    GetFlaggedMessageUserReports: {
        Endpoint: function (id) { return "/flagged-messages/" + id + "/user-reports"; },
        Method: types_1.HttpMethod.Get
    },
    GetFlaggedMessageKeywordScans: {
        Endpoint: function (id) { return "/flagged-messages/" + id + "/keyword-scans"; },
        Method: types_1.HttpMethod.Get
    },
    GetFlaggedMessageResolutions: {
        Endpoint: function (id) { return "/flagged-messages/" + id + "/resolutions"; },
        Method: types_1.HttpMethod.Get
    },
    GetFlaggedMessageResolutionUserReports: {
        Endpoint: function (id) { return "/flagged-resolutions/" + id + "/user-reports"; },
        Method: types_1.HttpMethod.Get
    },
    GetFlaggedMessageResolutionKeywordScans: {
        Endpoint: function (id) { return "/flagged-resolutions/" + id + "/keyword-scans"; },
        Method: types_1.HttpMethod.Get
    },
    /* Groups */
    CreateGroupConversation: {
        Endpoint: '/conversations/user-admin-managed',
        Method: types_1.HttpMethod.Post
    },
    UpdateGroupConversationOwner: {
        Endpoint: function (conversationId, userAdminId) {
            return "conversations/" + conversationId + "/owner/" + userAdminId;
        },
        Method: types_1.HttpMethod.Patch
    },
    GetUserAdminGroups: {
        Endpoint: 'conversations/user-admin-groups',
        Method: types_1.HttpMethod.Get
    },
    /* Conversation Space Group Divisions */
    GetSpaceGroupDivisions: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId + "/space-group-divisions"; },
        Method: types_1.HttpMethod.Get
    },
    CreateConversationSpaceGroupDivision: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId + "/space-group-divisions"; },
        Method: types_1.HttpMethod.Post
    },
    DeleteConversationSpaceGroupDivision: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId + "/space-group-divisions"; },
        Method: types_1.HttpMethod.Delete
    },
    /* Dashboard */
    GetTotalUsersCount: {
        Endpoint: 'users/total-count',
        Method: types_1.HttpMethod.Get
    },
    GetMyUsersCount: {
        Endpoint: 'user-managements/my-users-count',
        Method: types_1.HttpMethod.Get
    },
    GetResolvedFlaggedMessagesCount: {
        Endpoint: 'flagged-messages/resolved/total-count',
        Method: types_1.HttpMethod.Get
    },
    GetSentMessagesCountDaily: {
        Endpoint: 'sent-message-counts/daily',
        Method: types_1.HttpMethod.Get
    },
    GetSentMessagesCountTotal: {
        Endpoint: 'sent-message-counts/total-count',
        Method: types_1.HttpMethod.Get
    },
    GetUnresolvedFlaggedMessagesCount: {
        Endpoint: 'flagged-messages/unresolved/total-count',
        Method: types_1.HttpMethod.Get
    },
    GetUnresolvedFlaggedMessageCountsDaily: {
        Endpoint: 'flagged-messages/unresolved/daily',
        Method: types_1.HttpMethod.Get
    },
    GetEligibleRolesToInviteAdmin: {
        Endpoint: "/user-admins/invitable-roles",
        Method: types_1.HttpMethod.Get
    },
    /* User Onboarding Job */
    ProcessUserOnboardingJobUpload: {
        Endpoint: 'user-onboarding-jobs/files',
        Method: types_1.HttpMethod.Post
    },
    GetUserOnboardingJobBatch: {
        Endpoint: 'user-onboarding-jobs',
        Method: types_1.HttpMethod.Get
    },
    DownloadSampleFile: {
        Endpoint: 'user-onboarding-jobs/download',
        Method: types_1.HttpMethod.Get
    },
    GetAllErroredUserOnboardingJobRecords: {
        Endpoint: function (id) { return "user-onboarding-jobs/" + id + "/user-onboarding-job-records/errored"; },
        Method: types_1.HttpMethod.Get
    }
};
exports.ChatCommonRoutes = {
    UploadFile: {
        Endpoint: '/files',
        Method: types_1.HttpMethod.Post
    },
    GetReactedUsers: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/reacted-users"; },
        Method: types_1.HttpMethod.Get
    },
    GetReachableUsers: {
        Endpoint: '/users/conversable',
        Method: types_1.HttpMethod.Get
    },
    GetConversationUsers: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/users"; },
        Method: types_1.HttpMethod.Get
    },
    AddUsersToConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/users"; },
        Method: types_1.HttpMethod.Post
    },
    RemoveUsersFromConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/delete-users"; },
        Method: types_1.HttpMethod.Post
    },
    UpdateGroupImage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-image"; },
        Method: types_1.HttpMethod.Put
    },
    RemoveGroupImage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-image"; },
        Method: types_1.HttpMethod.Delete
    },
    GetGroupDefaultImageSet: {
        Endpoint: '/default-group-images',
        Method: types_1.HttpMethod.Get
    },
    UpdateGroupInfo: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-info"; },
        Method: types_1.HttpMethod.Put
    },
    MakeConversationOwner: {
        Endpoint: function (conversationId, userId) {
            return "/conversations/" + conversationId + "/owners/" + userId;
        },
        Method: types_1.HttpMethod.Put
    },
    RemoveConversationOwner: {
        Endpoint: function (conversationId, userId) {
            return "/conversations/" + conversationId + "/owners/" + userId;
        },
        Method: types_1.HttpMethod.Delete
    },
    GetConversationDetails: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId; },
        Method: types_1.HttpMethod.Get
    },
    DeleteGroupConversation: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId; },
        Method: types_1.HttpMethod.Delete
    }
};
//# sourceMappingURL=constants.js.map