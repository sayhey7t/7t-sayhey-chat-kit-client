"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var constants_1 = require("./constants");
var types_1 = require("./types");
function checkApiKey(config) {
    // SAFE TO REMOVE - Not user input
    // eslint-disable-next-line security/detect-object-injection
    if (!config.headers[constants_1.ApiKeyName]) {
        throw new types_1.SayHey.Error('Missing api key in request header', types_1.SayHey.ErrorType.InternalErrorType.AuthMissing);
    }
    return config;
}
function checkaccessToken(config) {
    if (!config.headers.Authorization) {
        throw new types_1.SayHey.Error('Missing auth token in request header', types_1.SayHey.ErrorType.InternalErrorType.AuthMissing);
    }
    return config;
}
function requestFailed(error) {
    var response = error.response;
    var err = new types_1.SayHey.Error(response === null || response === void 0 ? void 0 : response.data.message, response === null || response === void 0 ? void 0 : response.data.type);
    err.setStatusCode(response === null || response === void 0 ? void 0 : response.status);
    err.setErrorActions((response === null || response === void 0 ? void 0 : response.data.actions) || {});
    throw err;
}
exports.default = {
    createWithoutAuth: function (config) {
        var _a;
        var baseURL = config.baseURL, apiKey = config.apiKey;
        var instance = axios_1.default.create({
            baseURL: baseURL,
            headers: (_a = {},
                _a[constants_1.ApiKeyName] = apiKey,
                _a)
        });
        instance.interceptors.request.use(checkApiKey);
        instance.interceptors.response.use(undefined, requestFailed);
        return instance;
    },
    createWithAuth: function (config) {
        var _a;
        var baseURL = config.baseURL, accessToken = config.accessToken, apiKey = config.apiKey;
        var instance = axios_1.default.create({
            baseURL: baseURL,
            headers: (_a = {},
                _a[constants_1.ApiKeyName] = apiKey,
                _a.Authorization = "Bearer " + accessToken,
                _a)
        });
        instance.interceptors.request.use(checkApiKey);
        instance.interceptors.request.use(checkaccessToken);
        instance.interceptors.response.use(undefined, requestFailed);
        return instance;
    }
};
//# sourceMappingURL=axios.js.map