"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatAdminKit = void 0;
var axios_1 = __importDefault(require("axios"));
var jwt_decode_1 = __importDefault(require("jwt-decode"));
var neverthrow_1 = require("neverthrow");
var axios_2 = __importDefault(require("./axios"));
var constants_1 = require("./constants");
var helper_1 = require("./helper");
var mapper_1 = require("./mapper");
var types_1 = require("./types");
__exportStar(require("./types"), exports);
var ChatAdminKit = /** @class */ (function () {
    function ChatAdminKit() {
        var _this = this;
        this.accessToken = '';
        this.refreshToken = '';
        this._apiVersion = constants_1.API_VERSION;
        this.appId = '';
        this.apiKey = '';
        this.serverUrl = '';
        this._initialized = false;
        this._subscribed = false;
        this._isUserAuthenticated = false;
        this.exchangeTokenPromise = null;
        this.loginId = 0;
        this.isUserAuthenticated = function () { return _this._isUserAuthenticated; };
        /**
         * Listeners
         */
        this.addListeners = function (listeners) {
            if (_this._initialized) {
                var onAuthDetailChange = listeners.onAuthDetailChange, onSessionExpired = listeners.onSessionExpired;
                _this._subscribed = true;
                _this.authDetailChangeListener = onAuthDetailChange;
                _this.sessionExpiredListener = onSessionExpired;
                return neverthrow_1.ok(true);
            }
            return neverthrow_1.err(new types_1.SayHey.Error('ChatKit has not been initialized!', types_1.SayHey.ErrorType.InternalErrorType.Uninitialized));
        };
        /**
         *
         * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
         */
        this.guard = function (cb) { return __awaiter(_this, void 0, void 0, function () {
            var res, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._initialized) {
                            return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('ChatKit has not been initialized!', types_1.SayHey.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        if (!this._subscribed) {
                            return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('ChatKit events have not been subscribed to!', types_1.SayHey.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, cb()];
                    case 2:
                        res = _a.sent();
                        if (res instanceof types_1.SayHey.Error) {
                            return [2 /*return*/, neverthrow_1.err(res)];
                        }
                        return [2 /*return*/, neverthrow_1.ok(res)];
                    case 3:
                        e_1 = _a.sent();
                        if (e_1 instanceof types_1.SayHey.Error) {
                            return [2 /*return*/, neverthrow_1.err(e_1)];
                        }
                        return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('Unknown error', types_1.SayHey.ErrorType.InternalErrorType.Unknown))];
                    case 4: return [2 /*return*/];
                }
            });
        }); };
        this.exchangeTokenOnce = function () { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.exchangeTokenPromise) {
                            return [2 /*return*/, this.exchangeTokenPromise];
                        }
                        this.exchangeTokenPromise = this.exchangeToken();
                        return [4 /*yield*/, this.exchangeTokenPromise];
                    case 1:
                        response = _a.sent();
                        this.exchangeTokenPromise = null;
                        return [2 /*return*/, response];
                }
            });
        }); };
        /**
         *
         * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
         */
        this.guardWithAuth = function (cb) { return __awaiter(_this, void 0, void 0, function () {
            var currentLoginId, res, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        currentLoginId = this.loginId;
                        return [4 /*yield*/, this.guard(cb)];
                    case 1:
                        res = _a.sent();
                        if (!res.isErr()) return [3 /*break*/, 5];
                        if (!(!this.disableAutoRefreshToken &&
                            res.error.type === types_1.SayHey.ErrorType.AuthErrorType.TokenExpiredType)) return [3 /*break*/, 5];
                        if (!(currentLoginId === this.loginId)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.exchangeTokenOnce()];
                    case 2:
                        response = _a.sent();
                        if (response.isOk() && response.value === true) {
                            // --> NOTE: Modified to check response is also true => isUserAuthenticated, else logout
                            return [2 /*return*/, this.guard(cb)];
                        }
                        return [4 /*yield*/, this.signOut()];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4: 
                    // If token has been exchanged, use the new credentials to make the request again
                    return [2 /*return*/, this.guard(cb)];
                    case 5: return [2 /*return*/, res];
                }
            });
        }); };
        /**
         *
         * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
         * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
         */
        this.updateAuthDetails = function (accessToken, refreshToken) { return __awaiter(_this, void 0, void 0, function () {
            var id;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.accessToken = accessToken;
                        this.refreshToken = refreshToken;
                        id = accessToken ? jwt_decode_1.default(accessToken).id : '';
                        if (accessToken && refreshToken) {
                            this.loginId += 1;
                            this.httpClient = axios_2.default.createWithAuth({
                                apiKey: this.apiKey,
                                baseURL: this.serverUrl + "/" + this._apiVersion + "/apps/" + this.appId,
                                accessToken: this.accessToken
                            });
                        }
                        return [4 /*yield*/, ((_a = this.authDetailChangeListener) === null || _a === void 0 ? void 0 : _a.call(this, {
                                userId: id,
                                accessToken: accessToken,
                                refreshToken: refreshToken
                            }))];
                    case 1:
                        _b.sent();
                        // NOTE: Should be done at the end?
                        this._isUserAuthenticated = !!(accessToken && refreshToken);
                        return [2 /*return*/];
                }
            });
        }); };
        /**
         *
         * @param urlPath - url path returned from SayHey
         */
        this.getMediaSourceDetails = function (urlPath) {
            var _a;
            return ({
                uri: "" + _this.serverUrl + urlPath,
                method: 'GET',
                headers: (_a = {},
                    _a[constants_1.ApiKeyName] = _this.apiKey,
                    _a.Authorization = "Bearer " + _this.accessToken,
                    _a.Accept = 'image/*, video/*, audio/*',
                    _a)
            });
        };
        this.getMediaDirectUrl = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var getMediaDirectUrl;
            var _this = this;
            return __generator(this, function (_a) {
                getMediaDirectUrl = function () { return __awaiter(_this, void 0, void 0, function () {
                    var queryString, data;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                queryString = {
                                    noRedirect: true
                                };
                                if (params.size) {
                                    queryString.size = params.size;
                                }
                                return [4 /*yield*/, this.httpClient({
                                        url: "" + this.serverUrl + params.urlPath,
                                        method: 'GET',
                                        params: queryString
                                    })];
                            case 1:
                                data = (_a.sent()).data;
                                return [2 /*return*/, data.url];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getMediaDirectUrl)];
            });
        }); };
        this.uploadFile = function (fileUri, fileName, progress, thumbnailId) { return __awaiter(_this, void 0, void 0, function () {
            var uploadFile;
            var _this = this;
            return __generator(this, function (_a) {
                uploadFile = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, formData, extension, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatCommonRoutes.UploadFile, Endpoint = _a.Endpoint, Method = _a.Method;
                                formData = new FormData();
                                if (fileUri instanceof Blob) {
                                    formData.append('file', fileUri, fileName);
                                    if (thumbnailId) {
                                        formData.append('thumbnailId', thumbnailId);
                                    }
                                }
                                else {
                                    extension = fileName.split('.').pop();
                                    if (extension) {
                                        formData.append('file', {
                                            uri: fileUri,
                                            name: fileName,
                                            type: helper_1.mimeTypes[extension.toLowerCase()]
                                        });
                                        if (thumbnailId) {
                                            formData.append('thumbnailId', thumbnailId);
                                        }
                                    }
                                    else {
                                        return [2 /*return*/, new types_1.SayHey.Error('Invalid file')];
                                    }
                                }
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: formData,
                                        onUploadProgress: progress
                                            ? function (event) {
                                                var loaded = event.loaded, total = event.total;
                                                progress((loaded / total) * 0.95);
                                            }
                                            : undefined
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                progress === null || progress === void 0 ? void 0 : progress(1);
                                return [2 /*return*/, data.id];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(uploadFile)];
            });
        }); };
        this.uploadImage = function (fileUri, fileName) { return __awaiter(_this, void 0, void 0, function () {
            var extension, isSupportedImage;
            var _a;
            return __generator(this, function (_b) {
                extension = (_a = fileName
                    .split('.')
                    .pop()) === null || _a === void 0 ? void 0 : _a.toLowerCase();
                isSupportedImage = helper_1.mimeTypes[extension || ''].split('/')[0] === 'image';
                if (isSupportedImage) {
                    return [2 /*return*/, this.uploadFile(fileUri, fileName)];
                }
                return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('Please select a valid image!', types_1.SayHey.ErrorType.InternalErrorType.BadData))];
            });
        }); };
        /**
         * Token APIs
         */
        /**
         *
         * @param email - user email for SayHey
         * @param password - user password for SayHey
         *
         */
        this.exchangeToken = function () { return __awaiter(_this, void 0, void 0, function () {
            var exchangeToken;
            var _this = this;
            return __generator(this, function (_a) {
                exchangeToken = function () { return __awaiter(_this, void 0, void 0, function () {
                    var data, refreshToken, accessToken, e_2;
                    var _a, _b;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                _c.trys.push([0, 6, , 8]);
                                return [4 /*yield*/, axios_1.default({
                                        method: types_1.HttpMethod.Post,
                                        url: this.serverUrl + "/" + this._apiVersion + "/tokens/exchange",
                                        data: {
                                            refreshToken: this.refreshToken
                                        }
                                    })];
                            case 1:
                                data = (_c.sent()).data;
                                refreshToken = data.refreshToken, accessToken = data.accessToken;
                                if (!(refreshToken && accessToken)) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, refreshToken)
                                    // this._isUserAuthenticated = true
                                ];
                            case 2:
                                _c.sent();
                                return [3 /*break*/, 5];
                            case 3: return [4 /*yield*/, this.updateAuthDetails('', '')];
                            case 4:
                                _c.sent();
                                (_a = this.sessionExpiredListener) === null || _a === void 0 ? void 0 : _a.call(this);
                                _c.label = 5;
                            case 5: return [3 /*break*/, 8];
                            case 6:
                                e_2 = _c.sent();
                                return [4 /*yield*/, this.updateAuthDetails('', '')];
                            case 7:
                                _c.sent();
                                (_b = this.sessionExpiredListener) === null || _b === void 0 ? void 0 : _b.call(this);
                                return [3 /*break*/, 8];
                            case 8: return [2 /*return*/, this._isUserAuthenticated];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(exchangeToken)]; // --> NOTE: Looks like it will always be ok(true| false), never err()??
            });
        }); };
        /*  User Admin APIs */
        this.signOut = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this._isUserAuthenticated = false; // NOTE: Is manually set in the beginning as well?
                        this.loginId = 0;
                        return [4 /*yield*/, this.updateAuthDetails('', '')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        this.signInWithToken = function (accessToken, refreshToken) { return __awaiter(_this, void 0, void 0, function () {
            var signInWithTokenCallback;
            var _this = this;
            return __generator(this, function (_a) {
                signInWithTokenCallback = function () { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.updateAuthDetails(accessToken, refreshToken)
                                // this._isUserAuthenticated = true
                            ];
                            case 1:
                                _a.sent();
                                // this._isUserAuthenticated = true
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(signInWithTokenCallback)];
            });
        }); };
        this.invite = function (_a) {
            var firstName = _a.firstName, lastName = _a.lastName, email = _a.email, role = _a.role;
            return __awaiter(_this, void 0, void 0, function () {
                var invite;
                var _this = this;
                return __generator(this, function (_b) {
                    invite = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.Invite, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                firstName: firstName,
                                                lastName: lastName,
                                                email: email,
                                                role: role
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(invite)];
                });
            });
        };
        this.signUp = function (_a) {
            var password = _a.password, userAdminId = _a.userAdminId, invitationCode = _a.invitationCode;
            return __awaiter(_this, void 0, void 0, function () {
                var signUp;
                var _this = this;
                return __generator(this, function (_b) {
                    signUp = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, accessToken, refreshToken;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.SignUp, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                password: password,
                                                userAdminId: userAdminId,
                                                invitationCode: invitationCode
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    accessToken = data.accessToken, refreshToken = data.refreshToken;
                                    return [4 /*yield*/, this.updateAuthDetails(accessToken, refreshToken)];
                                case 2:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(signUp)];
                });
            });
        };
        this.signIn = function (email, password) { return __awaiter(_this, void 0, void 0, function () {
            var signIn;
            var _this = this;
            return __generator(this, function (_a) {
                signIn = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data, accessToken, refreshToken;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.SignIn, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email,
                                            password: password
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                accessToken = data.accessToken, refreshToken = data.refreshToken;
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, refreshToken)];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(signIn)];
            });
        }); };
        this.reInvite = function (_a) {
            var userAdminId = _a.userAdminId;
            return __awaiter(_this, void 0, void 0, function () {
                var reInvite;
                var _this = this;
                return __generator(this, function (_b) {
                    reInvite = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.ReInvite, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userAdminId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(reInvite)];
                });
            });
        };
        this.disableAdmin = function (_a) {
            var userAdminId = _a.userAdminId;
            return __awaiter(_this, void 0, void 0, function () {
                var disableAdmin;
                var _this = this;
                return __generator(this, function (_b) {
                    disableAdmin = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.DisableAdmin, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userAdminId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(disableAdmin)];
                });
            });
        };
        this.enableAdmin = function (_a) {
            var userAdminId = _a.userAdminId;
            return __awaiter(_this, void 0, void 0, function () {
                var enableAdmin;
                var _this = this;
                return __generator(this, function (_b) {
                    enableAdmin = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.EnableAdmin, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userAdminId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(enableAdmin)];
                });
            });
        };
        this.updateUserAdminInfo = function (_a) {
            var firstName = _a.firstName, lastName = _a.lastName, email = _a.email, userAdminId = _a.userAdminId;
            return __awaiter(_this, void 0, void 0, function () {
                var updateUserAdminInfo;
                var _this = this;
                return __generator(this, function (_b) {
                    updateUserAdminInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.UpdateUserAdminInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userAdminId),
                                            method: Method,
                                            data: { firstName: firstName, lastName: lastName, email: email }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(updateUserAdminInfo)];
                });
            });
        };
        this.updateUserAdminRole = function (_a) {
            var role = _a.role, userAdminId = _a.userAdminId;
            return __awaiter(_this, void 0, void 0, function () {
                var updateUserAdminRole;
                var _this = this;
                return __generator(this, function (_b) {
                    updateUserAdminRole = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.UpdateUserAdminRole, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userAdminId),
                                            method: Method,
                                            data: { role: role }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(updateUserAdminRole)];
                });
            });
        };
        this.updateUserAdminOnLeave = function (_a) {
            var onLeave = _a.onLeave;
            return __awaiter(_this, void 0, void 0, function () {
                var updateUserAdminOnLeave;
                var _this = this;
                return __generator(this, function (_b) {
                    updateUserAdminOnLeave = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.UpdateUserAdminOnLeave, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: { onLeave: onLeave }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(updateUserAdminOnLeave)];
                });
            });
        };
        this.forgotPassword = function (_a) {
            var email = _a.email;
            return __awaiter(_this, void 0, void 0, function () {
                var forgotPassword;
                var _this = this;
                return __generator(this, function (_b) {
                    forgotPassword = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.ForgotPassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                email: email
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(forgotPassword)];
                });
            });
        };
        this.resetPassword = function (_a) {
            var password = _a.password, resetToken = _a.resetToken;
            return __awaiter(_this, void 0, void 0, function () {
                var resetPassword;
                var _this = this;
                return __generator(this, function (_b) {
                    resetPassword = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.ResetPassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                password: password,
                                                resetToken: resetToken
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(resetPassword)];
                });
            });
        };
        this.getUserAdmins = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, userAdminStatus = _a.userAdminStatus, sort = _a.sort, sortOrder = _a.sortOrder, role = _a.role;
            return __awaiter(_this, void 0, void 0, function () {
                var getUserAdminsBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getUserAdminsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetUserAdmins, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, userAdminStatus: userAdminStatus, sort: sort, sortOrder: sortOrder, role: role }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign({}, data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getUserAdminsBatch)];
                });
            });
        };
        this.getUserAdminsSelfInfo = function () { return __awaiter(_this, void 0, void 0, function () {
            var getUserAdminsSelfInfo;
            var _this = this;
            return __generator(this, function (_a) {
                getUserAdminsSelfInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetUserAdminSelfInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUserAdminsSelfInfo)];
            });
        }); };
        this.updateUserAdminsSelfInfo = function (_a) {
            var firstName = _a.firstName, lastName = _a.lastName;
            return __awaiter(_this, void 0, void 0, function () {
                var updateUserAdminSelfInfo;
                var _this = this;
                return __generator(this, function (_b) {
                    updateUserAdminSelfInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.UpdateUserAdminSelfInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            data: {
                                                firstName: firstName,
                                                lastName: lastName
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(updateUserAdminSelfInfo)];
                });
            });
        };
        /* User Management APIs */
        this.assignManagedUsers = function (list) { return __awaiter(_this, void 0, void 0, function () {
            var assignManagedUsers;
            var _this = this;
            return __generator(this, function (_a) {
                assignManagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.AssignManagedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            list: list
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(assignManagedUsers)];
            });
        }); };
        this.moveAllManagedUsers = function (_a) {
            var fromUserAdminId = _a.fromUserAdminId, toUserAdminId = _a.toUserAdminId, assignmentType = _a.assignmentType;
            return __awaiter(_this, void 0, void 0, function () {
                var moveAllManagedUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    moveAllManagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.MoveAllManagedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                fromUserAdminId: fromUserAdminId,
                                                toUserAdminId: toUserAdminId,
                                                assignmentType: assignmentType
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(moveAllManagedUsers)];
                });
            });
        };
        this.moveBackAllManagedUsers = function (userAdminId) { return __awaiter(_this, void 0, void 0, function () {
            var moveBackAllManagedUsers;
            var _this = this;
            return __generator(this, function (_a) {
                moveBackAllManagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.MoveBackAllManagedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(userAdminId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(moveBackAllManagedUsers)];
            });
        }); };
        this.getAllManagedUsers = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, userStatus = _a.userStatus, userManagementType = _a.userManagementType, sort = _a.sort, sortOrder = _a.sortOrder;
            return __awaiter(_this, void 0, void 0, function () {
                var getManagedUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    getManagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetManagedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, userStatus: userStatus, userManagementType: userManagementType, sort: sort, sortOrder: sortOrder }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign({}, data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getManagedUsers)];
                });
            });
        };
        this.getAllManagedUsersCount = function (queryParams) { return __awaiter(_this, void 0, void 0, function () {
            var getManagedUsers;
            var _this = this;
            return __generator(this, function (_a) {
                getManagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetManagedUsersCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method,
                                        params: queryParams
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getManagedUsers)];
            });
        }); };
        this.moveSelectedUsers = function (userMovementSelected) { return __awaiter(_this, void 0, void 0, function () {
            var moveSelectedUsers;
            var _this = this;
            return __generator(this, function (_a) {
                moveSelectedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.MoveSelectedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: __assign({}, userMovementSelected)
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(moveSelectedUsers)];
            });
        }); };
        this.getUnmanagedUsers = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, userStatus = _a.userStatus, sort = _a.sort, sortOrder = _a.sortOrder;
            return __awaiter(_this, void 0, void 0, function () {
                var getUnmanagedUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    getUnmanagedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetUnmanagedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, userStatus: userStatus, sort: sort, sortOrder: sortOrder }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign({}, data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getUnmanagedUsers)];
                });
            });
        };
        this.getUnmanagedUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
            var getUnmanagedUsersCount;
            var _this = this;
            return __generator(this, function (_a) {
                getUnmanagedUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetUnmanagedUsersCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUnmanagedUsersCount)];
            });
        }); };
        /* User Onboarding APIs */
        this.registerUser = function (_a) {
            var firstName = _a.firstName, lastName = _a.lastName, email = _a.email, hrAdminId = _a.hrAdminId, departmentId = _a.departmentId, divisionId = _a.divisionId, employeeId = _a.employeeId, title = _a.title;
            return __awaiter(_this, void 0, void 0, function () {
                var userRegistration;
                var _this = this;
                return __generator(this, function (_b) {
                    userRegistration = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.RegisterUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                firstName: firstName,
                                                lastName: lastName,
                                                email: email,
                                                hrAdminId: hrAdminId,
                                                departmentId: departmentId,
                                                divisionId: divisionId,
                                                employeeId: employeeId,
                                                title: title
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(userRegistration)];
                });
            });
        };
        this.updateUser = function (_a) {
            var firstName = _a.firstName, lastName = _a.lastName, email = _a.email, title = _a.title, departmentId = _a.departmentId, divisionId = _a.divisionId, employeeId = _a.employeeId, userId = _a.userId;
            return __awaiter(_this, void 0, void 0, function () {
                var updateUser;
                var _this = this;
                return __generator(this, function (_b) {
                    updateUser = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.UpdateUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userId),
                                            method: Method,
                                            data: {
                                                firstName: firstName,
                                                lastName: lastName,
                                                email: email,
                                                title: title,
                                                departmentId: departmentId,
                                                divisionId: divisionId,
                                                employeeId: employeeId
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(updateUser)];
                });
            });
        };
        /* Department APIs */
        this.getDepartments = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, sort = _a.sort, sortOrder = _a.sortOrder, _b = _a.includeDivisions, includeDivisions = _b === void 0 ? false : _b;
            return __awaiter(_this, void 0, void 0, function () {
                var getDepartments;
                var _this = this;
                return __generator(this, function (_c) {
                    getDepartments = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetDepartments, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, sort: sort, sortOrder: sortOrder, includeDivisions: includeDivisions }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getDepartments)];
                });
            });
        };
        this.createDepartment = function (name) { return __awaiter(_this, void 0, void 0, function () {
            var createDepartment;
            var _this = this;
            return __generator(this, function (_a) {
                createDepartment = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.CreateDepartment, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            name: name
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createDepartment)];
            });
        }); };
        this.updateDepartment = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var updateDepartment;
            var _this = this;
            return __generator(this, function (_a) {
                updateDepartment = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.UpdateDepartments, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(params.departmentId),
                                        method: Method,
                                        data: {
                                            name: params.name
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateDepartment)];
            });
        }); };
        /* Division APIs */
        this.getDivisions = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, sort = _a.sort, sortOrder = _a.sortOrder, departmentId = _a.departmentId;
            return __awaiter(_this, void 0, void 0, function () {
                var getDivisions;
                var _this = this;
                return __generator(this, function (_b) {
                    getDivisions = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetDivisions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(departmentId),
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, sort: sort, sortOrder: sortOrder }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getDivisions)];
                });
            });
        };
        this.createDivision = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createDivision;
            var _this = this;
            return __generator(this, function (_a) {
                createDivision = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, name, departmentId, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.CreateDivisions, Endpoint = _a.Endpoint, Method = _a.Method;
                                name = params.name, departmentId = params.departmentId;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(departmentId),
                                        method: Method,
                                        data: {
                                            name: name
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createDivision)];
            });
        }); };
        this.updateDivision = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var updateDivision;
            var _this = this;
            return __generator(this, function (_a) {
                updateDivision = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, name, departmentId, divisionId;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.UpdateDivisions, Endpoint = _a.Endpoint, Method = _a.Method;
                                name = params.name, departmentId = params.departmentId, divisionId = params.divisionId;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint({ departmentId: departmentId, divisionId: divisionId }),
                                        method: Method,
                                        data: {
                                            name: name
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateDivision)];
            });
        }); };
        /* Keyword APIs */
        this.createKeyword = function (_a) {
            var term = _a.term, priority = _a.priority;
            return __awaiter(_this, void 0, void 0, function () {
                var createKeyword;
                var _this = this;
                return __generator(this, function (_b) {
                    createKeyword = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.CreateKeyword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                term: term,
                                                priority: priority
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(createKeyword)];
                });
            });
        };
        this.createKeywordBulk = function (list) { return __awaiter(_this, void 0, void 0, function () {
            var createKeywordBulk;
            var _this = this;
            return __generator(this, function (_a) {
                createKeywordBulk = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.CreateKeywordBulk, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            list: list
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createKeywordBulk)];
            });
        }); };
        this.deleteKeyword = function (id) { return __awaiter(_this, void 0, void 0, function () {
            var deleteKeyword;
            var _this = this;
            return __generator(this, function (_a) {
                deleteKeyword = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.DeleteKeyword, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(id),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(deleteKeyword)];
            });
        }); };
        this.updateKeyword = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var updateKeyword;
            var _this = this;
            return __generator(this, function (_a) {
                updateKeyword = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.UpdateKeyword, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(params.id),
                                        method: Method,
                                        data: {
                                            priority: params.priority
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateKeyword)];
            });
        }); };
        this.getKeywordsBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, priority = _a.priority;
            return __awaiter(_this, void 0, void 0, function () {
                var getKeywordsBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getKeywordsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetKeywordsBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, priority: priority }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign({}, data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getKeywordsBatch)];
                });
            });
        };
        this.disableUser = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var disableUser;
            var _this = this;
            return __generator(this, function (_a) {
                disableUser = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.DisableUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(params.userId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(disableUser)];
            });
        }); };
        this.enableUser = function (params) {
            var enableUser = function () { return __awaiter(_this, void 0, void 0, function () {
                var _a, Endpoint, Method;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            _a = constants_1.ChatAdminRoutes.EnableUser, Endpoint = _a.Endpoint, Method = _a.Method;
                            return [4 /*yield*/, this.httpClient({
                                    url: Endpoint(params.userId),
                                    method: Method
                                })];
                        case 1:
                            _b.sent();
                            return [2 /*return*/, true];
                    }
                });
            }); };
            return _this.guardWithAuth(enableUser);
        };
        this.getMessagesBeforeSequence = function (_a) {
            var beforeSequence = _a.beforeSequence, _b = _a.limit, limit = _b === void 0 ? 20 : _b, conversationId = _a.conversationId;
            return __awaiter(_this, void 0, void 0, function () {
                var getMessagesBeforeSequence;
                var _this = this;
                return __generator(this, function (_c) {
                    getMessagesBeforeSequence = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetMessagesBeforeSequence, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                beforeSequence: beforeSequence,
                                                limit: limit
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, mapper_1.MessageMapper.toPeekDomainEntities(data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getMessagesBeforeSequence)];
                });
            });
        };
        this.getMessagesAfterSequence = function (_a) {
            var afterSequence = _a.afterSequence, _b = _a.limit, limit = _b === void 0 ? 20 : _b, conversationId = _a.conversationId;
            return __awaiter(_this, void 0, void 0, function () {
                var getMessagesAfterSequence;
                var _this = this;
                return __generator(this, function (_c) {
                    getMessagesAfterSequence = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetMessagesAfterSequence, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                afterSequence: afterSequence,
                                                limit: limit
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, mapper_1.MessageMapper.toPeekDomainEntities(data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getMessagesAfterSequence)];
                });
            });
        };
        this.getMessageContext = function (_a) {
            var peekBeforeLimit = _a.peekBeforeLimit, peekAfterLimit = _a.peekAfterLimit, messageId = _a.messageId;
            return __awaiter(_this, void 0, void 0, function () {
                var getMessageContext;
                var _this = this;
                return __generator(this, function (_b) {
                    getMessageContext = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetMessageContext, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            params: {
                                                peekBeforeLimit: peekBeforeLimit,
                                                peekAfterLimit: peekAfterLimit
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, mapper_1.MessageMapper.toPeekDomainEntities(data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getMessageContext)];
                });
            });
        };
        /* Messages Audit */
        this.getAuditMessagesBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, startDate = _a.startDate, endDate = _a.endDate, text = _a.text, textSearchCondition = _a.textSearchCondition, userId = _a.userId, hasAttachment = _a.hasAttachment, hasFlagged = _a.hasFlagged;
            return __awaiter(_this, void 0, void 0, function () {
                var getAuditMessagesBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getAuditMessagesBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetAuditMessagesBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            params: {
                                                offset: offset,
                                                limit: limit,
                                                startDate: startDate,
                                                endDate: endDate,
                                                text: text,
                                                textSearchCondition: textSearchCondition,
                                                userId: userId,
                                                hasAttachment: hasAttachment,
                                                hasFlagged: hasFlagged
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, mapper_1.AuditMessageMapper.toAuditMessageDomainEntityBatchDTO(data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getAuditMessagesBatch)];
                });
            });
        };
        this.downloadAuditMessages = function (_a) {
            var startDate = _a.startDate, endDate = _a.endDate, text = _a.text, textSearchCondition = _a.textSearchCondition, userId = _a.userId, hasAttachment = _a.hasAttachment, hasFlagged = _a.hasFlagged;
            return __awaiter(_this, void 0, void 0, function () {
                var downloadAuditMessages;
                var _this = this;
                return __generator(this, function (_b) {
                    downloadAuditMessages = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.DownloadAuditMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            params: {
                                                startDate: startDate,
                                                endDate: endDate,
                                                text: text,
                                                textSearchCondition: textSearchCondition,
                                                userId: userId,
                                                hasAttachment: hasAttachment,
                                                hasFlagged: hasFlagged
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data.map(mapper_1.AuditMessageMapper.toAuditMessageDomainEntityDTO)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(downloadAuditMessages)];
                });
            });
        };
        this.getUserBasicInfoBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search;
            return __awaiter(_this, void 0, void 0, function () {
                var getAuditMessagesBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getAuditMessagesBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetUserBasicInfoBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            params: {
                                                offset: offset,
                                                limit: limit,
                                                search: search
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getAuditMessagesBatch)];
                });
            });
        };
        /*User Admin Search */
        this.createUpdateUserAdminSearch = function (userAdminSearchCriteria) { return __awaiter(_this, void 0, void 0, function () {
            var createUpdateUserAdminSearch;
            var _this = this;
            return __generator(this, function (_a) {
                createUpdateUserAdminSearch = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.CreateUpdateUserAdminSearch, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: userAdminSearchCriteria
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createUpdateUserAdminSearch)];
            });
        }); };
        this.getUserAdminSearch = function () { return __awaiter(_this, void 0, void 0, function () {
            var getUserAdminSearch;
            var _this = this;
            return __generator(this, function (_a) {
                getUserAdminSearch = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetUserAdminSearch, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUserAdminSearch)];
            });
        }); };
        /* Message Flagging */
        this.getFlaggedMessagesBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, _b = _a.flagType, flagType = _b === void 0 ? 'all' : _b, _c = _a.status, status = _c === void 0 ? 'all' : _c;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesBatch;
                var _this = this;
                return __generator(this, function (_d) {
                    getFlaggedMessagesBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data, flaggedMsgsDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetFlaggedMessagesBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, flagType: flagType, status: status }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesBatch)];
                });
            });
        };
        this.getFlaggedMessagesCount = function () { return __awaiter(_this, void 0, void 0, function () {
            var getFlaggedMessagesCount;
            var _this = this;
            return __generator(this, function (_a) {
                getFlaggedMessagesCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetFlaggedMessagesCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesCount)];
            });
        }); };
        this.resolveFlaggedMessage = function (_a) {
            var resolutionStatus = _a.resolutionStatus, resolutionNotes = _a.resolutionNotes, messageDeletion = _a.messageDeletion, flaggedMessageId = _a.flaggedMessageId, notifySuperAdmin = _a.notifySuperAdmin;
            return __awaiter(_this, void 0, void 0, function () {
                var resolveFlaggedMessage;
                var _this = this;
                return __generator(this, function (_b) {
                    resolveFlaggedMessage = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.ResolveFlaggedMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(flaggedMessageId),
                                            method: Method,
                                            data: { resolutionStatus: resolutionStatus, resolutionNotes: resolutionNotes, messageDeletion: messageDeletion, notifySuperAdmin: notifySuperAdmin }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(resolveFlaggedMessage)];
                });
            });
        };
        this.getFlaggedMessageDetails = function (_a) {
            var flaggedMessageId = _a.flaggedMessageId;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessageDetails;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessageDetails = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetFlaggedMessageDetails, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(flaggedMessageId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign({}, data)];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessageDetails)];
                });
            });
        };
        this.getFlaggedMessagesUserReportsBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, flaggedMessageId = _a.flaggedMessageId, status = _a.status;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesUserReportsBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessagesUserReportsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, flaggedMsgsUserReportsDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetFlaggedMessageUserReports, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(flaggedMessageId),
                                            method: Method,
                                            params: { offset: offset, limit: limit, status: status }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsUserReportsDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsUserReportsDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesUserReportsBatch)];
                });
            });
        };
        this.getFlaggedMessagesKeywordScansBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, flaggedMessageId = _a.flaggedMessageId, status = _a.status;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesKeywordScansBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessagesKeywordScansBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, flaggedMsgsKeywordScansDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetFlaggedMessageKeywordScans, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(flaggedMessageId),
                                            method: Method,
                                            params: { offset: offset, limit: limit, status: status }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsKeywordScansDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsKeywordScansDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesKeywordScansBatch)];
                });
            });
        };
        this.getFlaggedMessagesResolutionsBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, flaggedMessageId = _a.flaggedMessageId;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesResolutionsBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessagesResolutionsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, flaggedMsgsResolutionsDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetFlaggedMessageResolutions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(flaggedMessageId),
                                            method: Method,
                                            params: { offset: offset, limit: limit }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsResolutionsDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsResolutionsDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesResolutionsBatch)];
                });
            });
        };
        this.getFlaggedMessagesResolutionUserReportsBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, resolutionId = _a.resolutionId;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesResolutionUserReportsBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessagesResolutionUserReportsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, flaggedMsgsResolutionUserReportsDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetFlaggedMessageResolutionUserReports, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(resolutionId),
                                            method: Method,
                                            params: { offset: offset, limit: limit }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsResolutionUserReportsDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsResolutionUserReportsDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesResolutionUserReportsBatch)];
                });
            });
        };
        this.getFlaggedMessagesResolutionKeywordScansBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, resolutionId = _a.resolutionId;
            return __awaiter(_this, void 0, void 0, function () {
                var getFlaggedMessagesResolutionKeywordScansBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getFlaggedMessagesResolutionKeywordScansBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, flaggedMsgsResolutionKeywordScansDTO;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetFlaggedMessageResolutionKeywordScans, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(resolutionId),
                                            method: Method,
                                            params: { offset: offset, limit: limit }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    flaggedMsgsResolutionKeywordScansDTO = __assign({}, data);
                                    return [2 /*return*/, flaggedMsgsResolutionKeywordScansDTO];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getFlaggedMessagesResolutionKeywordScansBatch)];
                });
            });
        };
        this.getReactedUsers = function (_a) {
            var messageId = _a.messageId, type = _a.type, limit = _a.limit, afterSequence = _a.afterSequence;
            return __awaiter(_this, void 0, void 0, function () {
                var getReactedUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    getReactedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.GetReactedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            params: {
                                                limit: limit,
                                                afterSequence: afterSequence,
                                                type: type || ''
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getReactedUsers)];
                });
            });
        };
        /* Conversation Space Group Divsions */
        this.getSpaceGroupDivisions = function (_a) {
            var conversationId = _a.conversationId;
            return __awaiter(_this, void 0, void 0, function () {
                var getSpaceGroupDivisions;
                var _this = this;
                return __generator(this, function (_b) {
                    getSpaceGroupDivisions = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetSpaceGroupDivisions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getSpaceGroupDivisions)];
                });
            });
        };
        this.createConversationSpaceGroupDivision = function (_a) {
            var conversationId = _a.conversationId, divisionIds = _a.divisionIds, memberIds = _a.memberIds;
            return __awaiter(_this, void 0, void 0, function () {
                var createConversationSpaceGroupDivision;
                var _this = this;
                return __generator(this, function (_b) {
                    createConversationSpaceGroupDivision = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.CreateConversationSpaceGroupDivision, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                divisionIds: divisionIds,
                                                memberIds: memberIds
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(createConversationSpaceGroupDivision)];
                });
            });
        };
        this.deleteConversationSpaceGroupDivision = function (_a) {
            var conversationId = _a.conversationId, divisionIds = _a.divisionIds;
            return __awaiter(_this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_b) {
                    return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.ChatAdminRoutes.DeleteConversationSpaceGroupDivision, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, this.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                data: {
                                                    divisionIds: divisionIds
                                                }
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        }); })
                        /* Groups */
                    ];
                });
            });
        };
        /* Groups */
        this.createGroupConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createGroupConversation;
            var _this = this;
            return __generator(this, function (_a) {
                createGroupConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, file, groupName, userIds, divisionIds, fileIdResponse, data_1, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.CreateGroupConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                file = params.file, groupName = params.groupName, userIds = params.userIds, divisionIds = params.divisionIds;
                                if (!file) return [3 /*break*/, 4];
                                return [4 /*yield*/, this.uploadFile(file.path, file.fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            groupName: groupName,
                                            groupImageId: fileIdResponse.value,
                                            memberIds: userIds,
                                            divisionIds: divisionIds
                                        }
                                    })];
                            case 2:
                                data_1 = (_b.sent()).data;
                                return [2 /*return*/, data_1];
                            case 3: return [2 /*return*/, fileIdResponse.error];
                            case 4: return [4 /*yield*/, this.httpClient({
                                    url: Endpoint,
                                    method: Method,
                                    data: {
                                        groupName: groupName,
                                        groupImageId: null,
                                        memberIds: userIds,
                                        divisionIds: divisionIds
                                    }
                                })];
                            case 5:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createGroupConversation)];
            });
        }); };
        this.updateGroupOwner = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var updateGroupOwner;
            var _this = this;
            return __generator(this, function (_a) {
                updateGroupOwner = function () { return __awaiter(_this, void 0, void 0, function () {
                    var conversationId, userAdminId, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                conversationId = params.conversationId, userAdminId = params.userAdminId;
                                _a = constants_1.ChatAdminRoutes.UpdateGroupConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId, userAdminId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateGroupOwner)];
            });
        }); };
        this.getGroupConversationBatch = function (_a) {
            var search = _a.search, offset = _a.offset, limit = _a.limit;
            return __awaiter(_this, void 0, void 0, function () {
                var getConversationGroupForUserAdminBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getConversationGroupForUserAdminBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, conversationGroupBatchDTOForUserAdminWithUserCount;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetUserAdminGroups, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            params: { search: search, offset: offset, limit: limit }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    conversationGroupBatchDTOForUserAdminWithUserCount = __assign({}, data);
                                    return [2 /*return*/, conversationGroupBatchDTOForUserAdminWithUserCount];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getConversationGroupForUserAdminBatch)];
                });
            });
        };
        this.deleteGroupConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var deleteGroupConversation;
            var _this = this;
            return __generator(this, function (_a) {
                deleteGroupConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var conversationId, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                conversationId = params.conversationId;
                                _a = constants_1.ChatCommonRoutes.DeleteGroupConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(deleteGroupConversation)];
            });
        }); };
        this.getReachableUsers = function (_a) {
            var _b = _a === void 0 ? {} : _a, searchString = _b.searchString, limit = _b.limit, nextCursor = _b.nextCursor, includeRefIdInSearch = _b.includeRefIdInSearch;
            return __awaiter(_this, void 0, void 0, function () {
                var getReachableUsers;
                var _this = this;
                return __generator(this, function (_c) {
                    getReachableUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, params, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.GetReachableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    params = {
                                        search: searchString || '',
                                        includeRefIdInSearch: includeRefIdInSearch || false
                                    };
                                    if (limit) {
                                        params.limit = limit;
                                    }
                                    if (nextCursor) {
                                        params.offset = nextCursor;
                                    }
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: params
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign(__assign({}, data), { results: mapper_1.UserMapper.toUserForUserDomainEntities(data.results) })];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getReachableUsers)];
                });
            });
        };
        this.getConversationUsers = function (_a) {
            var conversationId = _a.conversationId, searchString = _a.searchString, limit = _a.limit, nextCursor = _a.nextCursor, type = _a.type, includeRefIdInSearch = _a.includeRefIdInSearch;
            var getConversationUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                var _a, Endpoint, Method, data;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            _a = constants_1.ChatCommonRoutes.GetConversationUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                            return [4 /*yield*/, this.httpClient({
                                    url: Endpoint(conversationId),
                                    method: Method,
                                    params: {
                                        type: type || types_1.ConversationUserQueryType.Current,
                                        offset: nextCursor,
                                        limit: limit,
                                        search: searchString,
                                        includeRefIdInSearch: includeRefIdInSearch || false
                                    }
                                })];
                        case 1:
                            data = (_b.sent()).data;
                            return [2 /*return*/, __assign(__assign({}, data), { results: mapper_1.UserMapper.toConversableUserDomainForUserAdminEntities(data.results) })];
                    }
                });
            }); };
            return _this.guardWithAuth(getConversationUsers);
        };
        this.addUsersToConversation = function (conversationId, userIds, force) { return __awaiter(_this, void 0, void 0, function () {
            var addUsersToConversation;
            var _this = this;
            return __generator(this, function (_a) {
                addUsersToConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatCommonRoutes.AddUsersToConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            memberIds: userIds
                                        },
                                        params: {
                                            force: force
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(addUsersToConversation)];
            });
        }); };
        this.removeUsersFromConversation = function (conversationId, userIds, force) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.RemoveUsersFromConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                memberIds: userIds
                                            },
                                            params: {
                                                force: force
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.updateGroupImage = function (conversationId, file) { return __awaiter(_this, void 0, void 0, function () {
            var updateGroupImage;
            var _this = this;
            return __generator(this, function (_a) {
                updateGroupImage = function () { return __awaiter(_this, void 0, void 0, function () {
                    var fileIdResponse, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, this.uploadFile(file.path, file.fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                _a = constants_1.ChatCommonRoutes.UpdateGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            groupImageId: fileIdResponse.value
                                        }
                                    })];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                            case 3: return [2 /*return*/, false];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateGroupImage)];
            });
        }); };
        this.removeGroupImage = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var removeGroupImage;
            var _this = this;
            return __generator(this, function (_a) {
                removeGroupImage = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatCommonRoutes.RemoveGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(removeGroupImage)];
            });
        }); };
        this.getGroupDefaultImageSet = function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.GetGroupDefaultImageSet, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); })];
            });
        }); };
        this.updateGroupInfo = function (conversationId, groupName) { return __awaiter(_this, void 0, void 0, function () {
            var updateGroupInfo;
            var _this = this;
            return __generator(this, function (_a) {
                updateGroupInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatCommonRoutes.UpdateGroupInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            groupName: groupName
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateGroupInfo)];
            });
        }); };
        this.makeConversationOwner = function (conversationId, userId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.MakeConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId, userId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.removeConversationOwner = function (conversationId, userId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.RemoveConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId, userId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.getConversationDetails = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var getConversationDetails;
            var _this = this;
            return __generator(this, function (_a) {
                getConversationDetails = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatCommonRoutes.GetConversationDetails, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getConversationDetails)];
            });
        }); };
        this.getTotalUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
            var getTotalUsersCount;
            var _this = this;
            return __generator(this, function (_a) {
                getTotalUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetTotalUsersCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getTotalUsersCount)];
            });
        }); };
        this.getMyUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
            var getMyUsersCount;
            var _this = this;
            return __generator(this, function (_a) {
                getMyUsersCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetMyUsersCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getMyUsersCount)];
            });
        }); };
        this.getResolvedFlaggedMessagesCount = function (timeFrame) { return __awaiter(_this, void 0, void 0, function () {
            var getResolvedFlaggedMessagesCount;
            var _this = this;
            return __generator(this, function (_a) {
                getResolvedFlaggedMessagesCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetResolvedFlaggedMessagesCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method,
                                        params: { timeFrame: timeFrame }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getResolvedFlaggedMessagesCount)];
            });
        }); };
        this.getSentMessagesCountDaily = function () { return __awaiter(_this, void 0, void 0, function () {
            var getSentMessagesCountDaily;
            var _this = this;
            return __generator(this, function (_a) {
                getSentMessagesCountDaily = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetSentMessagesCountDaily, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getSentMessagesCountDaily)];
            });
        }); };
        this.getSentMessagesCountTotal = function (timeFrame) { return __awaiter(_this, void 0, void 0, function () {
            var getSentMessagesCountTotal;
            var _this = this;
            return __generator(this, function (_a) {
                getSentMessagesCountTotal = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetSentMessagesCountTotal, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method,
                                        params: { timeFrame: timeFrame }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getSentMessagesCountTotal)];
            });
        }); };
        this.getUnresolvedFlaggedMessagesCount = function (unresolvedMsgScope) { return __awaiter(_this, void 0, void 0, function () {
            var getUnresolvedFlaggedMessagesCount;
            var _this = this;
            return __generator(this, function (_a) {
                getUnresolvedFlaggedMessagesCount = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetUnresolvedFlaggedMessagesCount, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method,
                                        params: { unresolvedMsgScope: unresolvedMsgScope }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUnresolvedFlaggedMessagesCount)];
            });
        }); };
        this.getUnresolvedFlaggedMessagesCountDaily = function () { return __awaiter(_this, void 0, void 0, function () {
            var getUnresolvedFlaggedMessagesCountDaily;
            var _this = this;
            return __generator(this, function (_a) {
                getUnresolvedFlaggedMessagesCountDaily = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetUnresolvedFlaggedMessageCountsDaily, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign({}, data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUnresolvedFlaggedMessagesCountDaily)];
            });
        }); };
        this.getEligibleRolesToInviteUserAdmin = function () { return __awaiter(_this, void 0, void 0, function () {
            var getEligibleRolesToInviteUserAdmin;
            var _this = this;
            return __generator(this, function (_a) {
                getEligibleRolesToInviteUserAdmin = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.GetEligibleRolesToInviteAdmin, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                return [4 /*yield*/, this.httpClient({
                                        url: url,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getEligibleRolesToInviteUserAdmin)];
            });
        }); };
        /* User Onboarding Job  */
        this.processUserOnboardingJobUpload = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var processUserOnboardingJobUpload;
            var _this = this;
            return __generator(this, function (_a) {
                processUserOnboardingJobUpload = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, formData, fileUri, fileName, departmentId, divisionId, extension, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.ProcessUserOnboardingJobUpload, Endpoint = _a.Endpoint, Method = _a.Method;
                                formData = new FormData();
                                fileUri = params.fileUri, fileName = params.fileName, departmentId = params.departmentId, divisionId = params.divisionId;
                                formData.append('file', fileUri);
                                if (fileUri instanceof Blob) {
                                    formData.append('file', fileUri, fileName);
                                }
                                else {
                                    extension = fileName.split('.').pop();
                                    if (extension) {
                                        formData.append('file', {
                                            uri: fileUri,
                                            name: fileName,
                                            type: helper_1.mimeTypes[extension.toLowerCase()]
                                        });
                                    }
                                    else {
                                        return [2 /*return*/, new types_1.SayHey.Error('Invalid file')];
                                    }
                                }
                                formData.append('departmentId', departmentId);
                                formData.append('divisionId', divisionId);
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: formData
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(processUserOnboardingJobUpload)];
            });
        }); };
        this.getUserOnboardingJobBatch = function (_a) {
            var offset = _a.offset, limit = _a.limit, search = _a.search, status = _a.status;
            return __awaiter(_this, void 0, void 0, function () {
                var getUserOnboardingJobBatch;
                var _this = this;
                return __generator(this, function (_b) {
                    getUserOnboardingJobBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetUserOnboardingJobBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: { offset: offset, limit: limit, search: search, status: status }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getUserOnboardingJobBatch)];
                });
            });
        };
        this.downloadSampleFile = function () { return __awaiter(_this, void 0, void 0, function () {
            var downloadSampleFile;
            var _this = this;
            return __generator(this, function (_a) {
                downloadSampleFile = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatAdminRoutes.DownloadSampleFile, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(downloadSampleFile)];
            });
        }); };
        this.getAllErroredUserOnboardingJobRecords = function (_a) {
            var userOnboardingJobId = _a.userOnboardingJobId;
            return __awaiter(_this, void 0, void 0, function () {
                var getAllErroredUserOnboardingJobRecords;
                var _this = this;
                return __generator(this, function (_b) {
                    getAllErroredUserOnboardingJobRecords = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatAdminRoutes.GetAllErroredUserOnboardingJobRecords, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(userOnboardingJobId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getAllErroredUserOnboardingJobRecords)];
                });
            });
        };
    }
    /**
     * Setup methods
     */
    ChatAdminKit.prototype.initialize = function (params) {
        var sayheyAppId = params.sayheyAppId, sayheyApiKey = params.sayheyApiKey, sayheyUrl = params.sayheyUrl, disableAutoRefreshToken = params.disableAutoRefreshToken, apiVersion = params.apiVersion;
        this.serverUrl = sayheyUrl;
        this._apiVersion = apiVersion || constants_1.API_VERSION;
        var baseURL = this.serverUrl + "/" + this._apiVersion + "/apps/" + sayheyAppId;
        this.apiKey = sayheyApiKey;
        this.appId = sayheyAppId;
        this.disableAutoRefreshToken = disableAutoRefreshToken;
        this.httpClient = axios_2.default.createWithoutAuth({
            apiKey: this.apiKey,
            baseURL: baseURL
        });
        this._initialized = true;
    };
    return ChatAdminKit;
}());
exports.ChatAdminKit = ChatAdminKit;
//# sourceMappingURL=chat-admin-kit.js.map