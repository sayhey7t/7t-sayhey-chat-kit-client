"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatClientKit = void 0;
var publisher_kit_client_1 = __importDefault(require("@7t/publisher-kit-client"));
var axios_1 = __importDefault(require("axios"));
var jwt_decode_1 = __importDefault(require("jwt-decode"));
var neverthrow_1 = require("neverthrow");
var axios_2 = __importDefault(require("./axios"));
var constants_1 = require("./constants");
var helper_1 = require("./helper");
var mapper_1 = require("./mapper");
var types_1 = require("./types");
var ChatClientKit = /** @class */ (function () {
    function ChatClientKit() {
        var _this = this;
        this.accessToken = '';
        this.publisherToken = '';
        this.refreshToken = '';
        this.instanceIdPushNotification = '';
        this._apiVersion = constants_1.API_VERSION;
        this.appId = '';
        this.apiKey = '';
        this.serverUrl = '';
        this._initialized = false;
        this._subscribed = false;
        this._isUserAuthenticated = false;
        this.readMessageMap = {};
        // readMessageSequenceMap: { [conversationId: string]: number } = {}
        this.readMessageSequenceMap = new Map();
        this.isSendingReadReceipt = false;
        this.timerHandle = null;
        this.exchangeTokenPromise = null;
        this.loginId = 0;
        this.isUserAuthenticated = function () { return _this._isUserAuthenticated; };
        this.addListeners = function (listeners) {
            if (_this._initialized) {
                var onSocketConnect = listeners.onSocketConnect, onSocketDisconnect = listeners.onSocketDisconnect, onSocketError = listeners.onSocketError, onSocketReconnect = listeners.onSocketReconnect, onAuthDetailChange = listeners.onAuthDetailChange, onEvent = listeners.onEvent, onSessionExpired = listeners.onSessionExpired;
                _this._subscribed = true;
                _this.authDetailChangeListener = onAuthDetailChange;
                _this.eventListener = onEvent;
                _this.sessionExpiredListener = onSessionExpired;
                _this.publisherKitClient.onMessage(_this.onMessage);
                onSocketConnect && _this.publisherKitClient.onConnect(onSocketConnect);
                onSocketDisconnect && _this.publisherKitClient.onDisconnect(onSocketDisconnect);
                onSocketError && _this.publisherKitClient.onError(onSocketError);
                onSocketReconnect && _this.publisherKitClient.onReconnect(onSocketReconnect);
                return neverthrow_1.ok(true);
            }
            return neverthrow_1.err(new types_1.SayHey.Error('ChatKit has not been initialized!', types_1.SayHey.ErrorType.InternalErrorType.Uninitialized));
        };
        /**
         * Helper methods
         */
        /**
         *
         * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
         */
        this.guard = function (cb) { return __awaiter(_this, void 0, void 0, function () {
            var res, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._initialized) {
                            return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('ChatKit has not been initialized!', types_1.SayHey.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        if (!this._subscribed) {
                            return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('ChatKit events have not been subscribed to!', types_1.SayHey.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, cb()];
                    case 2:
                        res = _a.sent();
                        if (res instanceof types_1.SayHey.Error) {
                            return [2 /*return*/, neverthrow_1.err(res)];
                        }
                        return [2 /*return*/, neverthrow_1.ok(res)];
                    case 3:
                        e_1 = _a.sent();
                        if (e_1 instanceof types_1.SayHey.Error) {
                            return [2 /*return*/, neverthrow_1.err(e_1)];
                        }
                        return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('Unknown error', types_1.SayHey.ErrorType.InternalErrorType.Unknown))];
                    case 4: return [2 /*return*/];
                }
            });
        }); };
        this.exchangeTokenOnce = function () { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.exchangeTokenPromise) {
                            return [2 /*return*/, this.exchangeTokenPromise];
                        }
                        this.exchangeTokenPromise = this.exchangeToken();
                        return [4 /*yield*/, this.exchangeTokenPromise];
                    case 1:
                        response = _a.sent();
                        this.exchangeTokenPromise = null;
                        return [2 /*return*/, response];
                }
            });
        }); };
        this.isSocketConnected = function () { return _this.publisherKitClient.isConnected(); };
        /**
         *
         * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
         */
        this.guardWithAuth = function (cb) { return __awaiter(_this, void 0, void 0, function () {
            var currentLoginId, res, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        currentLoginId = this.loginId;
                        return [4 /*yield*/, this.guard(cb)];
                    case 1:
                        res = _a.sent();
                        if (!res.isErr()) return [3 /*break*/, 5];
                        if (!(!this.disableAutoRefreshToken &&
                            res.error.type === types_1.SayHey.ErrorType.AuthErrorType.TokenExpiredType)) return [3 /*break*/, 5];
                        if (!(currentLoginId === this.loginId)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.exchangeTokenOnce()];
                    case 2:
                        response = _a.sent();
                        if (response.isOk() && response.value === true) {
                            // --> NOTE: Modified to check response is also true => isUserAuthenticated, else logout
                            return [2 /*return*/, this.guard(cb)];
                        }
                        return [4 /*yield*/, this.signOut()];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4: 
                    // If token has been exchanged, use the new credentials to make the request again
                    return [2 /*return*/, this.guard(cb)];
                    case 5: return [2 /*return*/, res];
                }
            });
        }); };
        /**
         *
         * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
         * @param publisherToken - publisher token returned from SayHey that will be used to verify Publisher access
         * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
         */
        this.updateAuthDetails = function (accessToken, publisherToken, refreshToken) { return __awaiter(_this, void 0, void 0, function () {
            var id;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.accessToken = accessToken;
                        this.publisherToken = publisherToken;
                        this.refreshToken = refreshToken;
                        id = accessToken ? jwt_decode_1.default(accessToken).id : '';
                        if (publisherToken && accessToken && refreshToken) {
                            this.loginId += 1;
                            this.httpClient = axios_2.default.createWithAuth({
                                apiKey: this.apiKey,
                                baseURL: this.serverUrl + "/" + this._apiVersion + "/apps/" + this.appId,
                                accessToken: this.accessToken
                            });
                            this.publisherKitClient.updateAccessParams({
                                token: publisherToken,
                                userId: id
                            });
                            if (!this.publisherKitClient.isConnected()) {
                                this.publisherKitClient.connect();
                            }
                        }
                        return [4 /*yield*/, ((_a = this.authDetailChangeListener) === null || _a === void 0 ? void 0 : _a.call(this, {
                                userId: id,
                                accessToken: accessToken,
                                publisherToken: publisherToken,
                                refreshToken: refreshToken
                            }))];
                    case 1:
                        _b.sent();
                        // NOTE: Should be done at the end?
                        this._isUserAuthenticated = !!(publisherToken && accessToken && refreshToken);
                        return [2 /*return*/];
                }
            });
        }); };
        // NOTE: Expose these methods only when there is a strong need to do so
        // public addGuard = async <T>(
        //   cb: (...args: any[]) => Promise<T | SayHey.Error>
        // ): Promise<Result<T, SayHey.Error>> => {
        //   return this.guard(cb)
        // }
        // public addGuardWithAuth = async <T>(
        //   cb: (...args: any[]) => Promise<T | SayHey.Error>
        // ): Promise<Result<T, SayHey.Error>> => {
        //   return this.guardWithAuth(cb)
        // }
        /**
         *
         * @param urlPath - url path returned from SayHey
         */
        this.getMediaSourceDetails = function (urlPath) {
            var _a;
            return ({
                uri: "" + _this.serverUrl + urlPath,
                method: 'GET',
                headers: (_a = {},
                    _a[constants_1.ApiKeyName] = _this.apiKey,
                    _a.Authorization = "Bearer " + _this.accessToken,
                    _a.Accept = 'image/*, video/*, audio/*',
                    _a)
            });
        };
        this.getMediaDirectUrl = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var getMediaDirectUrl;
            var _this = this;
            return __generator(this, function (_a) {
                getMediaDirectUrl = function () { return __awaiter(_this, void 0, void 0, function () {
                    var queryString, data;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                queryString = {
                                    noRedirect: true
                                };
                                if (params.size) {
                                    queryString.size = params.size;
                                }
                                return [4 /*yield*/, this.httpClient({
                                        url: "" + this.serverUrl + params.urlPath,
                                        method: 'GET',
                                        params: queryString
                                    })];
                            case 1:
                                data = (_a.sent()).data;
                                return [2 /*return*/, data.url];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getMediaDirectUrl)];
            });
        }); };
        this.uploadFile = function (fileUri, fileName, progress, thumbnailId) { return __awaiter(_this, void 0, void 0, function () {
            var uploadFile;
            var _this = this;
            return __generator(this, function (_a) {
                uploadFile = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, formData, extension, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatCommonRoutes.UploadFile, Endpoint = _a.Endpoint, Method = _a.Method;
                                formData = new FormData();
                                if (fileUri instanceof Blob) {
                                    formData.append('file', fileUri, fileName);
                                    if (thumbnailId) {
                                        formData.append('thumbnailId', thumbnailId);
                                    }
                                }
                                else {
                                    extension = fileName.split('.').pop();
                                    if (extension) {
                                        formData.append('file', {
                                            uri: fileUri,
                                            name: fileName,
                                            type: helper_1.mimeTypes[extension.toLowerCase()]
                                        });
                                        if (thumbnailId) {
                                            formData.append('thumbnailId', thumbnailId);
                                        }
                                    }
                                    else {
                                        return [2 /*return*/, new types_1.SayHey.Error('Invalid file')];
                                    }
                                }
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: formData,
                                        onUploadProgress: progress
                                            ? function (event) {
                                                var loaded = event.loaded, total = event.total;
                                                progress((loaded / total) * 0.95);
                                            }
                                            : undefined
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                progress === null || progress === void 0 ? void 0 : progress(1);
                                return [2 /*return*/, data.id];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(uploadFile)];
            });
        }); };
        this.uploadImage = function (fileUri, fileName) { return __awaiter(_this, void 0, void 0, function () {
            var extension, isSupportedImage;
            var _a;
            return __generator(this, function (_b) {
                extension = (_a = fileName
                    .split('.')
                    .pop()) === null || _a === void 0 ? void 0 : _a.toLowerCase();
                isSupportedImage = helper_1.mimeTypes[extension || ''].split('/')[0] === 'image';
                if (isSupportedImage) {
                    return [2 /*return*/, this.uploadFile(fileUri, fileName)];
                }
                return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('Please select a valid image!', types_1.SayHey.ErrorType.InternalErrorType.BadData))];
            });
        }); };
        this.disconnectPublisher = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.publisherKitClient.disconnect();
                return [2 /*return*/];
            });
        }); };
        this.reconnectPublisher = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.publisherKitClient.connect();
                return [2 /*return*/];
            });
        }); };
        /**
         * Token APIs
         */
        /**
         *
         * @param email - user email for SayHey
         * @param password - user password for SayHey
         *
         */
        this.signIn = function (email, password) { return __awaiter(_this, void 0, void 0, function () {
            var signInCallback;
            var _this = this;
            return __generator(this, function (_a) {
                signInCallback = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.SignIn, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email,
                                            password: password
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)
                                    // this._isUserAuthenticated = true
                                ];
                            case 2:
                                _b.sent();
                                // this._isUserAuthenticated = true
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(signInCallback)];
            });
        }); };
        this.signInWithToken = function (accessToken, publisherToken, refreshToken) { return __awaiter(_this, void 0, void 0, function () {
            var signInWithTokenCallback;
            var _this = this;
            return __generator(this, function (_a) {
                signInWithTokenCallback = function () { return __awaiter(_this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)
                                // this._isUserAuthenticated = true
                            ];
                            case 1:
                                _a.sent();
                                // this._isUserAuthenticated = true
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(signInWithTokenCallback)];
            });
        }); };
        this.signUp = function (email, password, firstName, lastName) { return __awaiter(_this, void 0, void 0, function () {
            var signUp;
            var _this = this;
            return __generator(this, function (_a) {
                signUp = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.SignUp, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email,
                                            password: password,
                                            firstName: firstName,
                                            lastName: lastName
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)
                                    // this._isUserAuthenticated = true
                                ];
                            case 2:
                                _b.sent();
                                // this._isUserAuthenticated = true
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(signUp)];
            });
        }); };
        this.signOut = function () { return __awaiter(_this, void 0, void 0, function () {
            var unregisteringPushNotification;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.publisherKitClient.disconnect();
                        this._isUserAuthenticated = false; // NOTE: Is manually set in the beginning as well?
                        this.loginId = 0;
                        unregisteringPushNotification = function () { return __awaiter(_this, void 0, void 0, function () {
                            var result;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, this.unregisterForPushNotification()];
                                    case 1:
                                        result = _a.sent();
                                        if (result.isOk() ||
                                            (result.isErr() && result.error.statusCode && result.error.statusCode < 500)) {
                                            return [2 /*return*/];
                                        }
                                        setTimeout(unregisteringPushNotification, 3000);
                                        return [2 /*return*/];
                                }
                            });
                        }); };
                        return [4 /*yield*/, unregisteringPushNotification()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.updateAuthDetails('', '', '')];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        this.exchangeToken = function () { return __awaiter(_this, void 0, void 0, function () {
            var exchangeToken;
            var _this = this;
            return __generator(this, function (_a) {
                exchangeToken = function () { return __awaiter(_this, void 0, void 0, function () {
                    var data, refreshToken, accessToken, publisherToken, e_2;
                    var _a, _b;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                _c.trys.push([0, 6, , 8]);
                                return [4 /*yield*/, axios_1.default({
                                        method: types_1.HttpMethod.Post,
                                        url: this.serverUrl + "/" + this._apiVersion + "/tokens/exchange",
                                        data: {
                                            refreshToken: this.refreshToken
                                        }
                                    })];
                            case 1:
                                data = (_c.sent()).data;
                                refreshToken = data.refreshToken, accessToken = data.accessToken, publisherToken = data.publisherToken;
                                if (!(refreshToken && accessToken)) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)
                                    // this._isUserAuthenticated = true
                                ];
                            case 2:
                                _c.sent();
                                return [3 /*break*/, 5];
                            case 3: return [4 /*yield*/, this.updateAuthDetails('', '', '')];
                            case 4:
                                _c.sent();
                                (_a = this.sessionExpiredListener) === null || _a === void 0 ? void 0 : _a.call(this);
                                _c.label = 5;
                            case 5: return [3 /*break*/, 8];
                            case 6:
                                e_2 = _c.sent();
                                return [4 /*yield*/, this.updateAuthDetails('', '', '')];
                            case 7:
                                _c.sent();
                                (_b = this.sessionExpiredListener) === null || _b === void 0 ? void 0 : _b.call(this);
                                return [3 /*break*/, 8];
                            case 8: return [2 /*return*/, this._isUserAuthenticated];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(exchangeToken)]; // --> NOTE: Looks like it will always be ok(true| false), never err()??
            });
        }); };
        this.changePassword = function (currentPassword, newPassword) { return __awaiter(_this, void 0, void 0, function () {
            var changePassword;
            var _this = this;
            return __generator(this, function (_a) {
                changePassword = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.ChangePassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            currentPassword: currentPassword,
                                            newPassword: newPassword
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(changePassword)];
            });
        }); };
        this.forgotPassword = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var forgotPassword;
            var _this = this;
            return __generator(this, function (_a) {
                forgotPassword = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.ForgotPassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(forgotPassword)];
            });
        }); };
        this.resetPassword = function (_a) {
            var password = _a.password, resetPasswordToken = _a.resetPasswordToken;
            return __awaiter(_this, void 0, void 0, function () {
                var resetPassword;
                var _this = this;
                return __generator(this, function (_b) {
                    resetPassword = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.ResetPassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                password: password,
                                                resetPasswordToken: resetPasswordToken
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(resetPassword)];
                });
            });
        };
        /**
         * User APIs
         */
        this.getReachableUsers = function (_a) {
            var _b = _a === void 0 ? {} : _a, searchString = _b.searchString, limit = _b.limit, nextCursor = _b.nextCursor, includeRefIdInSearch = _b.includeRefIdInSearch;
            return __awaiter(_this, void 0, void 0, function () {
                var getReachableUsers;
                var _this = this;
                return __generator(this, function (_c) {
                    getReachableUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, params, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.GetReachableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    params = {
                                        search: searchString || '',
                                        includeRefIdInSearch: includeRefIdInSearch || false
                                    };
                                    if (limit) {
                                        params.limit = limit;
                                    }
                                    if (nextCursor) {
                                        params.offset = nextCursor;
                                    }
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: params
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign(__assign({}, data), { results: mapper_1.UserMapper.toDomainEntities(data.results) })];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getReachableUsers)];
                });
            });
        };
        this.updateUserProfileImage = function (fileUri, fileName) { return __awaiter(_this, void 0, void 0, function () {
            var updateUserProfileImage;
            var _this = this;
            return __generator(this, function (_a) {
                updateUserProfileImage = function () { return __awaiter(_this, void 0, void 0, function () {
                    var fileIdResponse, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, this.uploadFile(fileUri, fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                _a = constants_1.ChatClientRoutes.UpdateUserProfileImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            profileImageId: fileIdResponse.value
                                        }
                                    })];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                            case 3: return [2 /*return*/, false];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateUserProfileImage)];
            });
        }); };
        this.updateUserProfileInfo = function (options) { return __awaiter(_this, void 0, void 0, function () {
            var updateUserProfileInfo;
            var _this = this;
            return __generator(this, function (_a) {
                updateUserProfileInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.UpdateUserProfileInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: options
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateUserProfileInfo)];
            });
        }); };
        this.getUserDetails = function () { return __awaiter(_this, void 0, void 0, function () {
            var getUserDetails;
            var _this = this;
            return __generator(this, function (_a) {
                getUserDetails = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.GetUserProfile, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.UserMapper.toSelfDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getUserDetails)];
            });
        }); };
        this.muteAllNotifications = function () { return __awaiter(_this, void 0, void 0, function () {
            var muteAllNotifications;
            var _this = this;
            return __generator(this, function (_a) {
                muteAllNotifications = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.MuteAllNotifications, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(muteAllNotifications)];
            });
        }); };
        this.unmuteAllNotifications = function () { return __awaiter(_this, void 0, void 0, function () {
            var unmuteAllNotifications;
            var _this = this;
            return __generator(this, function (_a) {
                unmuteAllNotifications = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.UnmuteAllNotifications, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(unmuteAllNotifications)];
            });
        }); };
        /**
         * Conversation APIs
         */
        this.getExistingIndvidualOrQuickConversation = function (memberIds) { return __awaiter(_this, void 0, void 0, function () {
            var getExistingIndvidualOrQuickConversation;
            var _this = this;
            return __generator(this, function (_a) {
                getExistingIndvidualOrQuickConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.GetExistingIndvidualOrQuickConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        params: {
                                            memberIds: memberIds
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                if (!data) {
                                    return [2 /*return*/, null];
                                }
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getExistingIndvidualOrQuickConversation)];
            });
        }); };
        this.getConversations = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationType, conversationGroupType, getConversations;
            var _this = this;
            return __generator(this, function (_a) {
                conversationType = params.conversationType, conversationGroupType = params.conversationGroupType;
                getConversations = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.GetConversations, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        params: {
                                            conversationType: conversationType,
                                            conversationGroupType: conversationGroupType
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntities(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getConversations)];
            });
        }); };
        this.getConversationDetails = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var getConversationDetails;
            var _this = this;
            return __generator(this, function (_a) {
                getConversationDetails = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatCommonRoutes.GetConversationDetails, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getConversationDetails)];
            });
        }); };
        this.checkForExistingConversation = function (withUserId) { return __awaiter(_this, void 0, void 0, function () {
            var checkForExistingConversation;
            var _this = this;
            return __generator(this, function (_a) {
                checkForExistingConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.GetExistingConversationWithUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(withUserId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(checkForExistingConversation)];
            });
        }); };
        this.createIndividualConversation = function (withUserId) { return __awaiter(_this, void 0, void 0, function () {
            var createIndividualConversation;
            var _this = this;
            return __generator(this, function (_a) {
                createIndividualConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.CreateIndividualConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            memberId: withUserId
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createIndividualConversation)];
            });
        }); };
        this.createGroupConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createGroupConversation;
            var _this = this;
            return __generator(this, function (_a) {
                createGroupConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, file, groupName, userIds, fileIdResponse, data_1, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.CreateGroupConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                file = params.file, groupName = params.groupName, userIds = params.userIds;
                                if (!file) return [3 /*break*/, 4];
                                return [4 /*yield*/, this.uploadFile(file.path, file.fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            groupName: groupName,
                                            groupImageId: fileIdResponse.value,
                                            memberIds: userIds
                                        }
                                    })];
                            case 2:
                                data_1 = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data_1)];
                            case 3: return [2 /*return*/, fileIdResponse.error];
                            case 4: return [4 /*yield*/, this.httpClient({
                                    url: Endpoint,
                                    method: Method,
                                    data: {
                                        groupName: groupName,
                                        groupImageId: null,
                                        memberIds: userIds
                                    }
                                })];
                            case 5:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createGroupConversation)];
            });
        }); };
        this.createQuickConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createQuickConversation;
            var _this = this;
            return __generator(this, function (_a) {
                createQuickConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, groupName, memberIds, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.CreateQuickConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                groupName = params.groupName, memberIds = params.memberIds;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            groupName: groupName,
                                            memberIds: memberIds
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createQuickConversation)];
            });
        }); };
        this.muteConversation = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var muteConversation;
            var _this = this;
            return __generator(this, function (_a) {
                muteConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.MuteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data.muteUntil];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(muteConversation)];
            });
        }); };
        this.unmuteConversation = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var unmuteConversation;
            var _this = this;
            return __generator(this, function (_a) {
                unmuteConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.UnmuteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(unmuteConversation)];
            });
        }); };
        this.pinConversation = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var pinConversation;
            var _this = this;
            return __generator(this, function (_a) {
                pinConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.PinConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data.pinnedAt];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(pinConversation)];
            });
        }); };
        this.unpinConversation = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var unpinConversation;
            var _this = this;
            return __generator(this, function (_a) {
                unpinConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.UnpinConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(unpinConversation)];
            });
        }); };
        this.deleteConversation = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var deleteConversation;
            var _this = this;
            return __generator(this, function (_a) {
                deleteConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.DeleteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(deleteConversation)];
            });
        }); };
        this.deleteAllConversations = function () { return __awaiter(_this, void 0, void 0, function () {
            var deleteAllConversations;
            var _this = this;
            return __generator(this, function (_a) {
                deleteAllConversations = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.DeleteAllConversations, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(deleteAllConversations)];
            });
        }); };
        this.updateGroupImage = function (conversationId, file) { return __awaiter(_this, void 0, void 0, function () {
            var updateGroupImage;
            var _this = this;
            return __generator(this, function (_a) {
                updateGroupImage = function () { return __awaiter(_this, void 0, void 0, function () {
                    var fileIdResponse, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, this.uploadFile(file.path, file.fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                _a = constants_1.ChatCommonRoutes.UpdateGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            groupImageId: fileIdResponse.value
                                        }
                                    })];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                            case 3: return [2 /*return*/, false];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateGroupImage)];
            });
        }); };
        this.updateGroupInfo = function (conversationId, groupName) { return __awaiter(_this, void 0, void 0, function () {
            var updateGroupInfo;
            var _this = this;
            return __generator(this, function (_a) {
                updateGroupInfo = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatCommonRoutes.UpdateGroupInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            groupName: groupName
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(updateGroupInfo)];
            });
        }); };
        this.getConversationUsers = function (_a) {
            var conversationId = _a.conversationId, searchString = _a.searchString, limit = _a.limit, nextCursor = _a.nextCursor, type = _a.type, includeRefIdInSearch = _a.includeRefIdInSearch;
            var getConversationUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                var _a, Endpoint, Method, data;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            _a = constants_1.ChatCommonRoutes.GetConversationUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                            return [4 /*yield*/, this.httpClient({
                                    url: Endpoint(conversationId),
                                    method: Method,
                                    params: {
                                        type: type || types_1.ConversationUserQueryType.Current,
                                        offset: nextCursor,
                                        limit: limit,
                                        search: searchString,
                                        includeRefIdInSearch: includeRefIdInSearch || false
                                    }
                                })];
                        case 1:
                            data = (_b.sent()).data;
                            return [2 /*return*/, __assign(__assign({}, data), { results: mapper_1.UserMapper.toConversableUserDomainEntities(data.results) })];
                    }
                });
            }); };
            return _this.guardWithAuth(getConversationUsers);
        };
        this.getConversationReachableUsers = function (_a) {
            var conversationId = _a.conversationId, searchString = _a.searchString, limit = _a.limit, nextCursor = _a.nextCursor, includeRefIdInSearch = _a.includeRefIdInSearch;
            return __awaiter(_this, void 0, void 0, function () {
                var getConversationReachableUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    getConversationReachableUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.GetConversationReachableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint(conversationId);
                                    return [4 /*yield*/, this.httpClient({
                                            url: url,
                                            method: Method,
                                            params: {
                                                limit: limit,
                                                search: searchString,
                                                offset: nextCursor,
                                                includeRefIdInSearch: includeRefIdInSearch || false
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign(__assign({}, data), { results: mapper_1.UserMapper.toDomainEntities(data.results) })];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getConversationReachableUsers)];
                });
            });
        };
        this.getConversationBasicUsers = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationId, _a, type;
            var _this = this;
            return __generator(this, function (_b) {
                conversationId = params.conversationId, _a = params.type, type = _a === void 0 ? 'current' : _a;
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.GetConversationBasicUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                type: type
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); })];
            });
        }); };
        this.addUsersToConversation = function (conversationId, userIds, force) { return __awaiter(_this, void 0, void 0, function () {
            var addUsersToConversation;
            var _this = this;
            return __generator(this, function (_a) {
                addUsersToConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatCommonRoutes.AddUsersToConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            memberIds: userIds
                                        },
                                        params: {
                                            force: force
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(addUsersToConversation)];
            });
        }); };
        this.removeUsersFromConversation = function (conversationId, userIds, force) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.RemoveUsersFromConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                memberIds: userIds
                                            },
                                            params: {
                                                force: force
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.makeConversationOwner = function (conversationId, userId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.MakeConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId, userId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.removeConversationOwner = function (conversationId, userId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.RemoveConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId, userId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.removeGroupImage = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var removeGroupImage;
            var _this = this;
            return __generator(this, function (_a) {
                removeGroupImage = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatCommonRoutes.RemoveGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(removeGroupImage)];
            });
        }); };
        this.deleteGroupConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var deleteGroupConversation;
            var _this = this;
            return __generator(this, function (_a) {
                deleteGroupConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var conversationId, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                conversationId = params.conversationId;
                                _a = constants_1.ChatCommonRoutes.DeleteGroupConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(deleteGroupConversation)];
            });
        }); };
        /**
         * @deprecated The method should not be used
         */
        this.getUserConversationReactions = function (conversationId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.GetUserConversationReactions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, mapper_1.ReactionMapper.toDomianEntity(data)];
                            }
                        });
                    }); })];
            });
        }); };
        this.getUserConversationMessagesReactions = function (conversationId, messageIds) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.GetUserConversationMessagesReactions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                messageIds: messageIds
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, mapper_1.ReactionMapper.toDomianEntity(data)];
                            }
                        });
                    }); })];
            });
        }); };
        this.getGroupDefaultImageSet = function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.GetGroupDefaultImageSet, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); })
                    /**
                     * Message APIs
                     */
                ];
            });
        }); };
        /**
         * Message APIs
         */
        this.getMessages = function (conversationId, pivotSequence, after, limit) {
            if (limit === void 0) { limit = 20; }
            return __awaiter(_this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data, lastMessageSequenceInBatch, messageListIds, messageList, data, incomingMessageList, _i, incomingMessageList_1, m;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.ChatClientRoutes.GetMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                        if (!!after) return [3 /*break*/, 2];
                                        return [4 /*yield*/, this.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: {
                                                    beforeSequence: pivotSequence,
                                                    limit: limit
                                                }
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, mapper_1.MessageMapper.toDomainEntities(data)];
                                    case 2:
                                        if (!pivotSequence) {
                                            return [2 /*return*/, []];
                                        }
                                        lastMessageSequenceInBatch = Number.MAX_SAFE_INTEGER;
                                        messageListIds = new Set();
                                        messageList = [];
                                        _b.label = 3;
                                    case 3:
                                        if (!(lastMessageSequenceInBatch > pivotSequence + 1)) return [3 /*break*/, 5];
                                        return [4 /*yield*/, this.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: {
                                                    limit: limit,
                                                    beforeSequence: lastMessageSequenceInBatch
                                                }
                                            })];
                                    case 4:
                                        data = (_b.sent()).data;
                                        incomingMessageList = data;
                                        if (incomingMessageList.length === 0) {
                                            return [3 /*break*/, 5];
                                        }
                                        for (_i = 0, incomingMessageList_1 = incomingMessageList; _i < incomingMessageList_1.length; _i++) {
                                            m = incomingMessageList_1[_i];
                                            lastMessageSequenceInBatch = m.sequence;
                                            if (lastMessageSequenceInBatch <= pivotSequence) {
                                                break;
                                            }
                                            if (!messageListIds.has(m.id)) {
                                                messageListIds.add(m.id);
                                                messageList.push(m);
                                            }
                                        }
                                        return [3 /*break*/, 3];
                                    case 5: return [2 /*return*/, mapper_1.MessageMapper.toDomainEntities(messageList)];
                                }
                            });
                        }); })];
                });
            });
        };
        this.onMessage = function (data) {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
            switch (data.event) {
                case types_1.Publisher.EventType.NewMessage: {
                    (_a = _this.eventListener) === null || _a === void 0 ? void 0 : _a.call(_this, {
                        event: data.event,
                        payload: {
                            message: mapper_1.MessageMapper.toDomainEntity(data.payload.message),
                            clientRefId: data.payload.clientRefId
                        }
                    });
                    break;
                }
                case types_1.Publisher.EventType.ConversationMuted: {
                    var _o = data.payload, conversationId = _o.conversationId, muteUntil = _o.conversationMuteDto.muteUntil;
                    (_b = _this.eventListener) === null || _b === void 0 ? void 0 : _b.call(_this, {
                        event: data.event,
                        payload: {
                            conversationId: conversationId,
                            muteUntil: new Date(muteUntil)
                        }
                    });
                    break;
                }
                case types_1.Publisher.EventType.ConversationPinned: {
                    var _p = data.payload, conversationId = _p.conversationId, pinnedAt = _p.conversationPinDto.pinnedAt;
                    (_c = _this.eventListener) === null || _c === void 0 ? void 0 : _c.call(_this, {
                        event: data.event,
                        payload: {
                            conversationId: conversationId,
                            pinnedAt: new Date(pinnedAt)
                        }
                    });
                    break;
                }
                case types_1.Publisher.EventType.ConversationHideAndClear: {
                    var _q = data.payload, conversationId = _q.conversationId, _r = _q.conversationUserHideClearDto, startAfter = _r.startAfter, visible = _r.visible;
                    (_d = _this.eventListener) === null || _d === void 0 ? void 0 : _d.call(_this, {
                        event: data.event,
                        payload: {
                            conversationId: conversationId,
                            visible: visible,
                            startAfter: new Date(startAfter)
                        }
                    });
                    break;
                }
                case types_1.Publisher.EventType.ConversationHideAndClearAll: {
                    var _s = data.payload.conversationUserHideClearDto, startAfter = _s.startAfter, visible = _s.visible;
                    (_e = _this.eventListener) === null || _e === void 0 ? void 0 : _e.call(_this, {
                        event: data.event,
                        payload: {
                            visible: visible,
                            startAfter: new Date(startAfter)
                        }
                    });
                    break;
                }
                case types_1.Publisher.EventType.ConversationGroupInfoChanged: {
                    (_f = _this.eventListener) === null || _f === void 0 ? void 0 : _f.call(_this, {
                        event: data.event,
                        payload: __assign(__assign({}, data.payload), { groupPicture: data.payload.groupPicture
                                ? mapper_1.MediaMapper.toDomainEntity(data.payload.groupPicture)
                                : null })
                    });
                    break;
                }
                case types_1.Publisher.EventType.ConversationQuickGroupChangedToGroup: {
                    (_g = _this.eventListener) === null || _g === void 0 ? void 0 : _g.call(_this, {
                        event: data.event,
                        payload: __assign({}, data.payload)
                    });
                    break;
                }
                case types_1.Publisher.EventType.ExternalUserConversationInvitationAccepted: {
                    (_h = _this.eventListener) === null || _h === void 0 ? void 0 : _h.call(_this, {
                        event: data.event,
                        payload: __assign({}, data.payload)
                    });
                    break;
                }
                case types_1.Publisher.EventType.ExternalUserNudged: {
                    (_j = _this.eventListener) === null || _j === void 0 ? void 0 : _j.call(_this, {
                        event: data.event,
                        payload: __assign({}, data.payload)
                    });
                    break;
                }
                case types_1.Publisher.EventType.ConversationCreated:
                case types_1.Publisher.EventType.ConversationUnpinned:
                case types_1.Publisher.EventType.ConversationUnmuted:
                case types_1.Publisher.EventType.UserInfoChanged:
                case types_1.Publisher.EventType.ConversationGroupOwnerAdded:
                case types_1.Publisher.EventType.ConversationGroupOwnerRemoved:
                case types_1.Publisher.EventType.ConversationGroupMemberAdded:
                case types_1.Publisher.EventType.ConversationGroupMemberRemoved:
                case types_1.Publisher.EventType.UserReactedToMessage:
                case types_1.Publisher.EventType.UserUnReactedToMessage:
                case types_1.Publisher.EventType.MessageReactionsUpdate:
                case types_1.Publisher.EventType.MessageDeleted:
                case types_1.Publisher.EventType.UserEnabled: {
                    (_k = _this.eventListener) === null || _k === void 0 ? void 0 : _k.call(_this, data);
                    break;
                }
                case types_1.Publisher.EventType.UserDisabled: {
                    (_l = _this.eventListener) === null || _l === void 0 ? void 0 : _l.call(_this, data);
                    if (!_this.disableUserDisabledHandler) {
                        _this.signOut();
                    }
                    break;
                }
                case types_1.Publisher.EventType.UserDeleted: {
                    (_m = _this.eventListener) === null || _m === void 0 ? void 0 : _m.call(_this, data);
                    if (!_this.disableUserDeletedHandler) {
                        _this.signOut();
                    }
                    break;
                }
                default:
                    break;
            }
        };
        this.sendMessage = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var conversationId, clientRefId, _a, Endpoint, Method, payload, _b, file, progress_1, subProgress, fileIdResponse, file, progress_2, subProgress, uri, fileName, thumbnail, thumbnailId, thumbnailIdResponse, videoUploadProgress, fileIdResponse, data;
                        var _c, _d, _e;
                        return __generator(this, function (_f) {
                            switch (_f.label) {
                                case 0:
                                    conversationId = params.conversationId, clientRefId = params.clientRefId;
                                    _a = constants_1.ChatClientRoutes.SendMessage, Endpoint = _a.Endpoint, Method = _a.Method;
                                    _b = params.type;
                                    switch (_b) {
                                        case types_1.MessageType.Text: return [3 /*break*/, 1];
                                        case types_1.MessageType.Emoji: return [3 /*break*/, 1];
                                        case types_1.MessageType.Gif: return [3 /*break*/, 2];
                                        case types_1.MessageType.Audio: return [3 /*break*/, 3];
                                        case types_1.MessageType.Image: return [3 /*break*/, 3];
                                        case types_1.MessageType.Document: return [3 /*break*/, 3];
                                        case types_1.MessageType.Video: return [3 /*break*/, 5];
                                    }
                                    return [3 /*break*/, 9];
                                case 1:
                                    {
                                        payload = {
                                            type: params.type,
                                            text: params.text.trim()
                                        };
                                        return [3 /*break*/, 10];
                                    }
                                    _f.label = 2;
                                case 2:
                                    {
                                        if (!helper_1.isUrl(params.url)) {
                                            return [2 /*return*/, new types_1.SayHey.Error('Invalid URL!', types_1.SayHey.ErrorType.InternalErrorType.MissingParameter)];
                                        }
                                        payload = {
                                            type: types_1.MessageType.Gif,
                                            refUrl: params.url,
                                            text: (_c = params.text) === null || _c === void 0 ? void 0 : _c.trim()
                                        };
                                        return [3 /*break*/, 10];
                                    }
                                    _f.label = 3;
                                case 3:
                                    file = params.file, progress_1 = params.progress;
                                    subProgress = progress_1
                                        ? function (percentage) {
                                            progress_1(percentage * 0.9);
                                        }
                                        : undefined;
                                    return [4 /*yield*/, this.uploadFile(file.uri, file.fileName, subProgress)];
                                case 4:
                                    fileIdResponse = _f.sent();
                                    if (fileIdResponse.isErr()) {
                                        return [2 /*return*/, fileIdResponse.error];
                                    }
                                    payload = {
                                        type: file.type,
                                        text: (_d = file.text) === null || _d === void 0 ? void 0 : _d.trim(),
                                        fileId: fileIdResponse.value
                                    };
                                    return [3 /*break*/, 10];
                                case 5:
                                    file = params.file, progress_2 = params.progress;
                                    subProgress = progress_2
                                        ? function (percentage) {
                                            progress_2(percentage * 0.4);
                                        }
                                        : undefined;
                                    uri = file.uri, fileName = file.fileName, thumbnail = file.thumbnail;
                                    thumbnailId = '';
                                    if (!thumbnail) return [3 /*break*/, 7];
                                    return [4 /*yield*/, this.uploadFile(thumbnail.uri, thumbnail.name, subProgress)];
                                case 6:
                                    thumbnailIdResponse = _f.sent();
                                    if (thumbnailIdResponse.isErr()) {
                                        return [2 /*return*/, thumbnailIdResponse.error];
                                    }
                                    thumbnailId = thumbnailIdResponse.value;
                                    _f.label = 7;
                                case 7:
                                    videoUploadProgress = progress_2
                                        ? function (percentage) {
                                            progress_2(percentage * 0.6 + 0.4);
                                        }
                                        : undefined;
                                    return [4 /*yield*/, this.uploadFile(uri, fileName, videoUploadProgress, thumbnailId)];
                                case 8:
                                    fileIdResponse = _f.sent();
                                    if (fileIdResponse.isErr()) {
                                        return [2 /*return*/, fileIdResponse.error];
                                    }
                                    payload = {
                                        type: file.type,
                                        text: (_e = file.text) === null || _e === void 0 ? void 0 : _e.trim(),
                                        fileId: fileIdResponse.value
                                    };
                                    return [3 /*break*/, 10];
                                case 9: return [3 /*break*/, 10];
                                case 10: return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: payload,
                                        params: {
                                            clientRefId: clientRefId
                                        }
                                    })];
                                case 11:
                                    data = (_f.sent()).data;
                                    return [2 /*return*/, mapper_1.MessageMapper.toDomainEntity(data)];
                            }
                        });
                    }); })];
            });
        }); };
        this.getGalleryMessages = function (conversationId, beforeSequence, limit) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, messages, mediaList;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.GetMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                galleryOnly: true,
                                                limit: limit,
                                                beforeSequence: beforeSequence
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    messages = mapper_1.MessageMapper.toDomainEntities(data);
                                    mediaList = messages.map(function (message) { return message.file; });
                                    return [2 /*return*/, {
                                            nextCursor: mediaList.length ? messages[messages.length - 1].sequence : -1,
                                            messageList: messages
                                        }];
                            }
                        });
                    }); })];
            });
        }); };
        this.setMessageReaction = function (messageId, type) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.SetMessageReaction, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            data: {
                                                type: type
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.removeMessageReaction = function (messageId, type) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.RemoveMessageReaction, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            data: {
                                                type: type
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        }); };
        this.readMessage = function (params) {
            var conversationId = params.conversationId, messageId = params.messageId, sequence = params.sequence, immediateUpdate = params.immediateUpdate;
            var mark = function () {
                var readMessageSequenceMapForConvId = _this.readMessageSequenceMap.get(conversationId);
                if (readMessageSequenceMapForConvId &&
                    Number.isInteger(readMessageSequenceMapForConvId) &&
                    readMessageSequenceMapForConvId >= sequence) {
                    return;
                }
                if (immediateUpdate) {
                    _this.markMessageRead(messageId).then(function (res) {
                        if (res.isErr()) {
                            _this.readMessageSequenceMap.set(conversationId, messageId);
                        }
                    });
                }
                else {
                    _this.readMessageSequenceMap.set(conversationId, messageId);
                }
                _this.readMessageSequenceMap.set(conversationId, sequence);
            };
            if (!_this.isSendingReadReceipt) {
                mark();
            }
            else {
                setTimeout(mark, 500);
            }
        };
        this.markMessageRead = function (messageId) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.MarkMessageAsRead, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })
                    /** Push Notification APIs */
                ];
            });
        }); };
        /** Push Notification APIs */
        this.registerForPushNotification = function (instanceId, token) { return __awaiter(_this, void 0, void 0, function () {
            var registerForPushNotification;
            var _this = this;
            return __generator(this, function (_a) {
                if (!instanceId || !token) {
                    return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('Invalid instance id or token string!'))];
                }
                registerForPushNotification = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.RegisterPushNotification, Endpoint = _a.Endpoint, Method = _a.Method;
                                this.instanceIdPushNotification = instanceId;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            instanceId: instanceId,
                                            token: token
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(registerForPushNotification)];
            });
        }); };
        this.unregisterForPushNotification = function () { return __awaiter(_this, void 0, void 0, function () {
            var unregisterForPushNotification;
            var _this = this;
            return __generator(this, function (_a) {
                if (!this.instanceIdPushNotification) {
                    return [2 /*return*/, neverthrow_1.ok(true)];
                }
                unregisterForPushNotification = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.UnregisterPushNotification, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(this.instanceIdPushNotification),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                this.instanceIdPushNotification = '';
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(unregisterForPushNotification)];
            });
        }); };
        this.getReactedUsers = function (_a) {
            var messageId = _a.messageId, type = _a.type, limit = _a.limit, afterSequence = _a.afterSequence;
            return __awaiter(_this, void 0, void 0, function () {
                var getReactedUsers;
                var _this = this;
                return __generator(this, function (_b) {
                    getReactedUsers = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatCommonRoutes.GetReactedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            params: {
                                                limit: limit,
                                                afterSequence: afterSequence,
                                                type: type || ''
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(getReactedUsers)];
                });
            });
        };
        this.getAppConfig = function () { return __awaiter(_this, void 0, void 0, function () {
            var getAppConfig;
            var _this = this;
            return __generator(this, function (_a) {
                getAppConfig = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.GetAppConfig, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getAppConfig)];
            });
        }); };
        /* User Onboarding APIs */
        this.checkEmail = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var checkEmail;
            var _this = this;
            return __generator(this, function (_a) {
                checkEmail = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.CheckEmail, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(checkEmail)];
            });
        }); };
        this.setPassword = function (_a) {
            var setPasswordToken = _a.setPasswordToken, password = _a.password;
            return __awaiter(_this, void 0, void 0, function () {
                var setPassword;
                var _this = this;
                return __generator(this, function (_b) {
                    setPassword = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.SetPassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                setPasswordToken: setPasswordToken,
                                                password: password
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(setPassword)];
                });
            });
        };
        /* Request OTP */
        this.requestSignUpOTP = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var requestSignUpOTP;
            var _this = this;
            return __generator(this, function (_a) {
                requestSignUpOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.RequestSignUpOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(requestSignUpOTP)];
            });
        }); };
        this.requestSetPasswordOTP = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var requestSetPasswordOTP;
            var _this = this;
            return __generator(this, function (_a) {
                requestSetPasswordOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.RequestSetPasswordOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(requestSetPasswordOTP)];
            });
        }); };
        this.requestResetPasswordOTP = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var requestResetPasswordOTP;
            var _this = this;
            return __generator(this, function (_a) {
                requestResetPasswordOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.RequestResetPasswordOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(requestResetPasswordOTP)];
            });
        }); };
        this.requestPreverificationOTP = function (email) { return __awaiter(_this, void 0, void 0, function () {
            var requestPreverificationOTP;
            var _this = this;
            return __generator(this, function (_a) {
                requestPreverificationOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.RequestPreverificationOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(requestPreverificationOTP)];
            });
        }); };
        /* Validate OTP */
        this.validateSignUpOTP = function (_a) {
            var otp = _a.otp, email = _a.email;
            return __awaiter(_this, void 0, void 0, function () {
                var validateSignUpOTP;
                var _this = this;
                return __generator(this, function (_b) {
                    validateSignUpOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.ValidateSignUpOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                otp: otp,
                                                email: email
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(validateSignUpOTP)];
                });
            });
        };
        this.validateSetPasswordOTP = function (_a) {
            var otp = _a.otp, email = _a.email;
            return __awaiter(_this, void 0, void 0, function () {
                var validateSetPasswordOTP;
                var _this = this;
                return __generator(this, function (_b) {
                    validateSetPasswordOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.ValidateSetPasswordOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                otp: otp,
                                                email: email
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(validateSetPasswordOTP)];
                });
            });
        };
        this.validateResetPasswordOTP = function (_a) {
            var otp = _a.otp, email = _a.email;
            return __awaiter(_this, void 0, void 0, function () {
                var validateResetPasswordOTP;
                var _this = this;
                return __generator(this, function (_b) {
                    validateResetPasswordOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.ValidateResetPasswordOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                otp: otp,
                                                email: email
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(validateResetPasswordOTP)];
                });
            });
        };
        this.validatePreverificationOTP = function (_a) {
            var otp = _a.otp, email = _a.email;
            return __awaiter(_this, void 0, void 0, function () {
                var validatePreverificationOTP;
                var _this = this;
                return __generator(this, function (_b) {
                    validatePreverificationOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.ValidatePreverificationOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                otp: otp,
                                                email: email
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guard(validatePreverificationOTP)];
                });
            });
        };
        this.clientSignInWithToken = function (sayheyLoginToken) { return __awaiter(_this, void 0, void 0, function () {
            var clientSignInWithToken;
            var _this = this;
            return __generator(this, function (_a) {
                clientSignInWithToken = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.ClientSignInWithToken, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            sayheyLoginToken: sayheyLoginToken
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guard(clientSignInWithToken)];
            });
        }); };
        /* Message Flagging */
        this.reportFlaggedMessage = function (_a) {
            var messageId = _a.messageId, reason = _a.reason;
            return __awaiter(_this, void 0, void 0, function () {
                var reportFlaggedMessage;
                var _this = this;
                return __generator(this, function (_b) {
                    reportFlaggedMessage = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.CreateFlaggedMessage, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                messageId: messageId,
                                                reason: reason
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(reportFlaggedMessage)];
                });
            });
        };
        /* SMS */
        this.addToContact = function (_a) {
            var externalUserId = _a.externalUserId;
            return __awaiter(_this, void 0, void 0, function () {
                var addToContact;
                var _this = this;
                return __generator(this, function (_b) {
                    addToContact = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.AddToContact, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                externalUserId: externalUserId
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(addToContact)];
                });
            });
        };
        this.getInternalUserContactsBatch = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var getInternalUserContactsBatch;
            var _this = this;
            return __generator(this, function (_a) {
                getInternalUserContactsBatch = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.GetInternalUserContactsBatch, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        params: params
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getInternalUserContactsBatch)];
            });
        }); };
        this.findExternalUserByPhone = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var findExternalUserByPhone;
            var _this = this;
            return __generator(this, function (_a) {
                findExternalUserByPhone = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.FindExternalUserByPhone, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        params: params
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(findExternalUserByPhone)];
            });
        }); };
        this.requestSignInExternalOTP = function (_a) {
            var phone = _a.phone;
            return __awaiter(_this, void 0, void 0, function () {
                var requestSignInExternalOTP;
                var _this = this;
                return __generator(this, function (_b) {
                    requestSignInExternalOTP = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.RequestSignInExternalOTP, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                phone: phone
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(requestSignInExternalOTP)];
                });
            });
        };
        this.signInExternal = function (_a) {
            var otp = _a.otp, phone = _a.phone;
            return __awaiter(_this, void 0, void 0, function () {
                var signInExternal;
                var _this = this;
                return __generator(this, function (_b) {
                    signInExternal = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.SignInExternal, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                otp: otp,
                                                phone: phone
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                    return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                                case 2:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(signInExternal)];
                });
            });
        };
        this.signInExternalByUserId = function (_a) {
            var id = _a.id, otp = _a.otp;
            return __awaiter(_this, void 0, void 0, function () {
                var signInExternalByUserId;
                var _this = this;
                return __generator(this, function (_b) {
                    signInExternalByUserId = function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.ChatClientRoutes.SignInExternalByUserId, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(id),
                                            method: Method,
                                            data: {
                                                otp: otp
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                    return [4 /*yield*/, this.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                                case 2:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); };
                    return [2 /*return*/, this.guardWithAuth(signInExternalByUserId)];
                });
            });
        };
        this.createNewContact = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createNewContact;
            var _this = this;
            return __generator(this, function (_a) {
                createNewContact = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.CreateNewContact, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: params
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createNewContact)];
            });
        }); };
        this.acceptTermsAndPrivacy = function () { return __awaiter(_this, void 0, void 0, function () {
            var acceptTermsAndPrivacy;
            var _this = this;
            return __generator(this, function (_a) {
                acceptTermsAndPrivacy = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.AcceptTermsAndPrivacy, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(acceptTermsAndPrivacy)];
            });
        }); };
        this.getTermsAndPrivacy = function () { return __awaiter(_this, void 0, void 0, function () {
            var getTermsAndPrivacy;
            var _this = this;
            return __generator(this, function (_a) {
                getTermsAndPrivacy = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.GetTermsAndPrivacy, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getTermsAndPrivacy)];
            });
        }); };
        this.getExistingSmsConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var externalUserId, getExistingSmsConversation;
            var _this = this;
            return __generator(this, function (_a) {
                externalUserId = params.externalUserId;
                getExistingSmsConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.GetExistingSmsConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        params: {
                                            externalUserId: externalUserId
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                if (!data) {
                                    return [2 /*return*/, null];
                                }
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getExistingSmsConversation)];
            });
        }); };
        this.checkSmsConversationInvitation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationId, checkSmsConversationInvitation;
            var _this = this;
            return __generator(this, function (_a) {
                conversationId = params.conversationId;
                checkSmsConversationInvitation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.CheckSmsConversationInvitation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(checkSmsConversationInvitation)];
            });
        }); };
        this.acceptSmsConversationInvitation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationId, acceptSmsConversationInvitation;
            var _this = this;
            return __generator(this, function (_a) {
                conversationId = params.conversationId;
                acceptSmsConversationInvitation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.AcceptSmsConversationInvitation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(acceptSmsConversationInvitation)];
            });
        }); };
        this.resendSmsConversationInvitation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationId, resendSmsConversationInvitation;
            var _this = this;
            return __generator(this, function (_a) {
                conversationId = params.conversationId;
                resendSmsConversationInvitation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.ResendSmsConversationInvitation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(resendSmsConversationInvitation)];
            });
        }); };
        this.createSmsConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var createSmsConversation;
            var _this = this;
            return __generator(this, function (_a) {
                createSmsConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, memberId, groupImageId, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.CreateSmsConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                memberId = params.memberId, groupImageId = params.groupImageId;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            memberId: memberId,
                                            groupImageId: groupImageId
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(createSmsConversation)];
            });
        }); };
        this.optToReceiveSms = function () { return __awaiter(_this, void 0, void 0, function () {
            var optToReceiveSms;
            var _this = this;
            return __generator(this, function (_a) {
                optToReceiveSms = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.OptToReceiveSms, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(optToReceiveSms)];
            });
        }); };
        this.optNotToReceiveSms = function () { return __awaiter(_this, void 0, void 0, function () {
            var optNotToReceiveSms;
            var _this = this;
            return __generator(this, function (_a) {
                optNotToReceiveSms = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.OptNotToReceiveSms, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(optNotToReceiveSms)];
            });
        }); };
        this.getOptToReceiveSmsStatus = function () { return __awaiter(_this, void 0, void 0, function () {
            var getOptToReceiveSmsStatus;
            var _this = this;
            return __generator(this, function (_a) {
                getOptToReceiveSmsStatus = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.GetOptToReceiveSmsStatus, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(getOptToReceiveSmsStatus)];
            });
        }); };
        this.nudgeExternalUserToConversation = function (params) { return __awaiter(_this, void 0, void 0, function () {
            var conversationId, nudgeExternalUserToConversation;
            var _this = this;
            return __generator(this, function (_a) {
                conversationId = params.conversationId;
                nudgeExternalUserToConversation = function () { return __awaiter(_this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.ChatClientRoutes.NudgeExternalUserToConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, this.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                }); };
                return [2 /*return*/, this.guardWithAuth(nudgeExternalUserToConversation)];
            });
        }); };
        this.publisherKitClient = new publisher_kit_client_1.default();
    }
    /**
     * Setup methods
     */
    ChatClientKit.prototype.initialize = function (params) {
        var _this = this;
        var sayheyAppId = params.sayheyAppId, sayheyApiKey = params.sayheyApiKey, publisherApiKey = params.publisherApiKey, publisherAppId = params.publisherAppId, sayheyUrl = params.sayheyUrl, publisherUrl = params.publisherUrl, messageUpdateInterval = params.messageUpdateInterval, disableAutoRefreshToken = params.disableAutoRefreshToken, disableUserDeletedHandler = params.disableUserDeletedHandler, disableUserDisabledHandler = params.disableUserDisabledHandler, apiVersion = params.apiVersion;
        this.serverUrl = sayheyUrl;
        this._apiVersion = apiVersion || constants_1.API_VERSION;
        var baseURL = this.serverUrl + "/" + this._apiVersion + "/apps/" + sayheyAppId;
        this.apiKey = sayheyApiKey;
        this.appId = sayheyAppId;
        this.disableAutoRefreshToken = disableAutoRefreshToken;
        this.disableUserDisabledHandler = disableUserDisabledHandler;
        this.disableUserDeletedHandler = disableUserDeletedHandler;
        this.httpClient = axios_2.default.createWithoutAuth({
            apiKey: this.apiKey,
            baseURL: baseURL
        });
        this.publisherKitClient.initialize({
            appId: publisherAppId,
            clientApiKey: publisherApiKey,
            publisherUrl: publisherUrl
        });
        if (this.timerHandle !== null) {
            clearInterval(this.timerHandle);
        }
        this.timerHandle = setInterval(function () {
            _this.isSendingReadReceipt = true;
            var copy = new Map(Object.entries(_this.readMessageMap));
            _this.readMessageMap = {};
            _this.isSendingReadReceipt = false;
            var conversationIds = Object.keys(copy);
            for (var i = 0; i < conversationIds.length; i++) {
                var conversationId = conversationIds[+i];
                var messageId = copy.get(conversationId);
                messageId && _this.markMessageRead(messageId);
            }
        }, messageUpdateInterval || constants_1.MESSAGE_READ_UPDATE_INTERVAL);
        this._initialized = true;
    };
    return ChatClientKit;
}());
exports.ChatClientKit = ChatClientKit;
//# sourceMappingURL=chat-client-kit.js.map