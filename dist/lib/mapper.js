"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuditMessageMapper = exports.ReactionMapper = exports.SystemMessageMapper = exports.MediaMapper = exports.UserMapper = exports.MessageMapper = exports.SentMessageMapper = exports.ConversationMapper = void 0;
var helper_1 = require("./helper");
var types_1 = require("./types");
var ConversationMapper = /** @class */ (function () {
    function ConversationMapper() {
    }
    ConversationMapper.toDomainEntity = function (conversation) {
        var id = conversation.id, lastReadMessageId = conversation.lastReadMessageId, lastMessage = conversation.lastMessage, muteUntil = conversation.muteUntil, pinnedAt = conversation.pinnedAt, badge = conversation.badge, type = conversation.type, createdAt = conversation.createdAt, updatedAt = conversation.updatedAt, isOwner = conversation.isOwner, startAfter = conversation.startAfter, visible = conversation.visible, endBefore = conversation.endBefore, deletedFromConversationAt = conversation.deletedFromConversationAt, serverCreated = conversation.serverCreated, serverManaged = conversation.serverManaged, isQuickGroup = conversation.isQuickGroup, isSpace = conversation.isSpace, lastUnreadNudgedAt = conversation.lastUnreadNudgedAt, creatorId = conversation.creatorId;
        var common = {
            id: id,
            lastReadMessageId: lastReadMessageId,
            lastMessage: lastMessage ? MessageMapper.toDomainEntity(lastMessage) : null,
            muteUntil: muteUntil ? new Date(muteUntil) : null,
            pinnedAt: pinnedAt ? new Date(pinnedAt) : null,
            createdAt: new Date(createdAt),
            updatedAt: new Date(updatedAt),
            startAfter: startAfter ? new Date(startAfter) : null,
            endBefore: endBefore ? new Date(endBefore) : null,
            deletedFromConversationAt: deletedFromConversationAt
                ? new Date(deletedFromConversationAt)
                : null,
            badge: badge || 0,
            type: type,
            isOwner: isOwner,
            visible: visible,
            serverCreated: serverCreated,
            serverManaged: serverManaged,
            isQuickGroup: isQuickGroup,
            isSpace: isSpace,
            lastUnreadNudgedAt: lastUnreadNudgedAt,
            creatorId: creatorId
        };
        if (conversation.type === types_1.ConversationType.Group) {
            var group = conversation.group;
            return __assign(__assign({}, common), { type: types_1.ConversationType.Group, group: __assign(__assign({}, group), { picture: group.picture ? MediaMapper.toDomainEntity(group.picture) : null }) });
        }
        var user = conversation.user;
        return __assign(__assign({}, common), { type: types_1.ConversationType.Individual, user: __assign(__assign({}, user), { picture: user.picture ? MediaMapper.toDomainEntity(user.picture) : null }) });
    };
    ConversationMapper.toDomainEntities = function (conversations) {
        return conversations.map(function (conversation) { return ConversationMapper.toDomainEntity(conversation); });
    };
    return ConversationMapper;
}());
exports.ConversationMapper = ConversationMapper;
var SentMessageMapper = /** @class */ (function () {
    function SentMessageMapper() {
    }
    SentMessageMapper.toDomainEntity = function (message) {
        var id = message.id, conversationId = message.conversationId, sender = message.sender, type = message.type, text = message.text, sequence = message.sequence, file = message.file, createdAt = message.createdAt, reactions = message.reactions, refUrl = message.refUrl, deletedAt = message.deletedAt;
        var parsedResult = helper_1.parseText(text);
        return {
            id: id,
            conversationId: conversationId,
            sender: sender,
            type: type,
            text: text,
            deletedAt: deletedAt ? new Date(deletedAt) : null,
            sequence: sequence,
            createdAt: new Date(createdAt),
            reactions: reactions,
            sent: true,
            parsedText: parsedResult ? parsedResult.parsedText : null,
            file: file ? MediaMapper.toDomainEntity(file) : null,
            refUrl: refUrl,
            systemMessage: null,
            previewUrl: parsedResult === null || parsedResult === void 0 ? void 0 : parsedResult.previewUrl
        };
    };
    SentMessageMapper.toDomainEntities = function (messages) {
        return messages.map(function (value) { return SentMessageMapper.toDomainEntity(value); });
    };
    return SentMessageMapper;
}());
exports.SentMessageMapper = SentMessageMapper;
var MessageMapper = /** @class */ (function () {
    function MessageMapper() {
    }
    MessageMapper.toDomainEntity = function (message) {
        return __assign(__assign({}, SentMessageMapper.toDomainEntity(message)), { systemMessage: message.systemMessage
                ? SystemMessageMapper.toDomainEntity(message.systemMessage)
                : null });
    };
    MessageMapper.toDomainEntities = function (messages) {
        return messages.map(function (value) { return MessageMapper.toDomainEntity(value); });
    };
    MessageMapper.toPeekDomainEntities = function (messages) {
        return messages.map(function (value) {
            var flaggedInfo = value.flaggedInfo;
            return __assign(__assign({}, MessageMapper.toDomainEntity(value)), { flaggedInfo: flaggedInfo });
        });
    };
    return MessageMapper;
}());
exports.MessageMapper = MessageMapper;
var UserMapper = /** @class */ (function () {
    function UserMapper() {
    }
    UserMapper.toDomainEntity = function (user) {
        var firstName = user.firstName, lastName = user.lastName, picture = user.picture, deletedAt = user.deletedAt;
        if (user.userScope === types_1.UserScope.Internal) {
            return __assign(__assign({}, user), { deletedAt: deletedAt ? new Date(deletedAt) : null, picture: picture ? MediaMapper.toDomainEntity(picture) : null, fullName: firstName + " " + lastName, email: user.email, username: user.email });
        }
        return __assign(__assign({}, user), { deletedAt: deletedAt ? new Date(deletedAt) : null, picture: picture ? MediaMapper.toDomainEntity(picture) : null, fullName: firstName + " " + lastName, phone: user.phone, username: user.phone.phoneNumber });
    };
    UserMapper.toUserForUserAdminDomainEntity = function (user) {
        var firstName = user.firstName, lastName = user.lastName, picture = user.picture, deletedAt = user.deletedAt;
        if (user.userScope === types_1.UserScope.Internal) {
            return __assign(__assign({}, user), { title: user.title, employeeId: user.employeeId, departmentId: user.departmentId, divisionId: user.divisionId, division: user.division, department: user.department, email: user.email, deletedAt: deletedAt ? new Date(deletedAt) : null, picture: picture ? MediaMapper.toDomainEntity(picture) : null, fullName: firstName + " " + lastName, username: user.email });
        }
        return __assign(__assign({}, user), { userScope: user.userScope, fullName: firstName + " " + lastName, username: user.phone.phoneNumber, phone: user.phone, deletedAt: deletedAt ? new Date(deletedAt) : null, picture: picture ? MediaMapper.toDomainEntity(picture) : null });
    };
    UserMapper.toConversableUserDomainEntity = function (user) {
        var firstName = user.firstName, lastName = user.lastName, picture = user.picture, deletedAt = user.deletedAt;
        if (user.userScope === types_1.UserScope.Internal) {
            return __assign(__assign({}, user), { username: user.email, fullName: firstName + " " + lastName, picture: picture ? MediaMapper.toDomainEntity(picture) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
        }
        return __assign(__assign({}, user), { username: user.phone.phoneNumber, fullName: firstName + " " + lastName, phone: user.phone, picture: picture ? MediaMapper.toDomainEntity(picture) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
    };
    UserMapper.toConversableUserDomainForUserAdminEntity = function (user) {
        var firstName = user.firstName, lastName = user.lastName, picture = user.picture, deletedAt = user.deletedAt;
        if (user.userScope === types_1.UserScope.Internal) {
            return __assign(__assign({}, user), { username: user.email, fullName: firstName + " " + lastName, picture: picture ? MediaMapper.toDomainEntity(picture) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
        }
        return __assign(__assign({}, user), { username: user.phone.phoneNumber, fullName: firstName + " " + lastName, picture: picture ? MediaMapper.toDomainEntity(picture) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
    };
    UserMapper.toDomainEntities = function (users) {
        return users.map(UserMapper.toDomainEntity);
    };
    UserMapper.toUserForUserDomainEntities = function (users) {
        return users.map(UserMapper.toUserForUserAdminDomainEntity);
    };
    UserMapper.toConversableUserDomainEntities = function (users) {
        return users.map(UserMapper.toConversableUserDomainEntity);
    };
    UserMapper.toConversableUserDomainForUserAdminEntities = function (users) {
        return users.map(UserMapper.toConversableUserDomainForUserAdminEntity);
    };
    UserMapper.toSelfDomainEntity = function (user) {
        var firstName = user.firstName, lastName = user.lastName, picture = user.picture, muteAllUntil = user.muteAllUntil, deletedAt = user.deletedAt;
        if (user.userScope === types_1.UserScope.Internal) {
            return __assign(__assign({}, user), { username: user.email, fullName: firstName + " " + lastName, picture: picture ? MediaMapper.toDomainEntity(picture) : null, muteAllUntil: muteAllUntil ? new Date(muteAllUntil) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
        }
        return __assign(__assign({}, user), { username: user.phone.phoneNumber, fullName: firstName + " " + lastName, picture: picture ? MediaMapper.toDomainEntity(picture) : null, muteAllUntil: muteAllUntil ? new Date(muteAllUntil) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
    };
    return UserMapper;
}());
exports.UserMapper = UserMapper;
var MediaMapper = /** @class */ (function () {
    function MediaMapper() {
    }
    MediaMapper.toDomainEntity = function (media) {
        var urlPath = media.urlPath, fileSize = media.fileSize, fileType = media.fileType, extension = media.extension, filename = media.filename, mimeType = media.mimeType, encoding = media.encoding, imageInfo = media.imageInfo, videoInfo = media.videoInfo;
        return {
            urlPath: urlPath,
            fileSize: fileSize,
            fileType: fileType,
            extension: extension,
            filename: filename,
            mimeType: mimeType,
            encoding: encoding,
            imageInfo: imageInfo,
            videoInfo: !videoInfo
                ? null
                : {
                    duration: videoInfo.duration,
                    thumbnail: videoInfo.thumbnail ? MediaMapper.toDomainEntity(videoInfo.thumbnail) : null
                }
        };
    };
    return MediaMapper;
}());
exports.MediaMapper = MediaMapper;
var SystemMessageMapper = /** @class */ (function () {
    function SystemMessageMapper() {
    }
    SystemMessageMapper.toDomainEntity = function (systemMessage) {
        var systemMessageType = systemMessage.systemMessageType, id = systemMessage.id, refValue = systemMessage.refValue, actorCount = systemMessage.actorCount, initiator = systemMessage.initiator, actors = systemMessage.actors, text = systemMessage.text;
        return {
            systemMessageType: systemMessageType,
            id: id,
            refValue: refValue,
            actorCount: actorCount,
            initiator: initiator,
            actors: actors,
            text: text
        };
    };
    return SystemMessageMapper;
}());
exports.SystemMessageMapper = SystemMessageMapper;
var ReactionMapper = /** @class */ (function () {
    function ReactionMapper() {
    }
    ReactionMapper.toDomianEntity = function (reactions) {
        var conversationReactions = new Map();
        reactions.forEach(function (item) {
            var messageId = item.messageId;
            var reactionTypesArray = Array.isArray(item.type) ? item.type : [item.type];
            var conversationReactionsForMsg = conversationReactions.get(messageId);
            if (!conversationReactionsForMsg) {
                conversationReactions.set(messageId, []);
            }
            var currentReactionTypes = conversationReactionsForMsg || [];
            var reactionSet = new Set(__spreadArrays(currentReactionTypes, reactionTypesArray));
            conversationReactions.set(messageId, Array.from(reactionSet));
        });
        var conversationReactionObj = {};
        var keys = Array.from(conversationReactions.keys());
        for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
            var key = keys_1[_i];
            // SAFE To remove - not user input
            // eslint-disable-next-line security/detect-object-injection
            conversationReactionObj[key] = conversationReactions.get(key) || undefined;
        }
        return conversationReactionObj;
    };
    return ReactionMapper;
}());
exports.ReactionMapper = ReactionMapper;
var AuditMessageMapper = /** @class */ (function () {
    function AuditMessageMapper() {
    }
    AuditMessageMapper.toAuditMessageDomainEntityBatchDTO = function (auditMessageBatchDTO) {
        var auditMessageDomainEntityDTO = auditMessageBatchDTO.results.map(function (auditMsg) {
            var messageDomainEntity = SentMessageMapper.toDomainEntity(auditMsg);
            return __assign(__assign({}, messageDomainEntity), { flagged: auditMsg.flagged });
        });
        return __assign(__assign({}, auditMessageBatchDTO), { results: auditMessageDomainEntityDTO });
    };
    AuditMessageMapper.toAuditMessageDomainEntityDTO = function (auditMessageDTO) {
        var messageDomainEntity = SentMessageMapper.toDomainEntity(auditMessageDTO);
        return __assign(__assign({}, messageDomainEntity), { flagged: auditMessageDTO.flagged, resolved: auditMessageDTO.resolved ? auditMessageDTO.resolved : undefined });
    };
    return AuditMessageMapper;
}());
exports.AuditMessageMapper = AuditMessageMapper;
//# sourceMappingURL=mapper.js.map