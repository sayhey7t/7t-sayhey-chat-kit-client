"use strict";
/**
 * Http Types
 */
var _a, _b, _c, _d, _e;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserOnboardingJobRecordStatus = exports.UserOnboardingJobStatus = exports.ResolutionStatusType = exports.FlaggedMessagePriorityType = exports.QueryCondition = exports.FlaggedMessageType = exports.KeyWordPriorityTypes = exports.UnresolvedMsgScopeType = exports.TimeFrameType = exports.TokenType = exports.DivisionSortFields = exports.DepartmentSortFields = exports.SortOrderType = exports.tempDefaultColumn = exports.SayHey = exports.Publisher = exports.ConversationUserQueryType = exports.UnmanagedUsersSortFields = exports.UserMovementSelected = exports.UserStatusType = exports.UserManagementSortFields = exports.UserManagementType = exports.APISignature = exports.UserAdminStatusType = exports.UserAdminSortFields = exports.UserTypes = exports.UserScope = exports.ConversationGroupType = exports.ConversationType = exports.SystemMessageType = exports.HighLightType = exports.MessageType = exports.FileType = exports.ImageSizes = exports.ReactionsObject = exports.ReactionType = exports.HttpMethod = void 0;
var HttpMethod;
(function (HttpMethod) {
    HttpMethod["Post"] = "post";
    HttpMethod["Get"] = "get";
    HttpMethod["Delete"] = "delete";
    HttpMethod["Patch"] = "patch";
    HttpMethod["Put"] = "put";
})(HttpMethod = exports.HttpMethod || (exports.HttpMethod = {}));
/**
 * Reaction Types
 */
var ReactionType;
(function (ReactionType) {
    ReactionType["Thumb"] = "Thumb";
    ReactionType["Heart"] = "Heart";
    ReactionType["Clap"] = "Clap";
    ReactionType["HaHa"] = "HaHa";
    ReactionType["Exclamation"] = "Exclamation";
})(ReactionType = exports.ReactionType || (exports.ReactionType = {}));
var ReactionsObject = /** @class */ (function () {
    function ReactionsObject() {
        this[_a] = 0;
        this[_b] = 0;
        this[_c] = 0;
        this[_d] = 0;
        this[_e] = 0;
    }
    return ReactionsObject;
}());
exports.ReactionsObject = ReactionsObject;
_a = ReactionType.Thumb, _b = ReactionType.Heart, _c = ReactionType.Clap, _d = ReactionType.HaHa, _e = ReactionType.Exclamation;
/**
 * Message Types
 */
var ImageSizes;
(function (ImageSizes) {
    ImageSizes["Tiny"] = "tiny";
    ImageSizes["Small"] = "small";
    ImageSizes["Medium"] = "medium";
    ImageSizes["Large"] = "large";
    ImageSizes["XLarge"] = "xlarge";
})(ImageSizes = exports.ImageSizes || (exports.ImageSizes = {}));
var FileType;
(function (FileType) {
    FileType["Video"] = "video";
    FileType["Image"] = "image";
    FileType["Audio"] = "audio";
    FileType["Other"] = "other";
})(FileType = exports.FileType || (exports.FileType = {}));
var MessageType;
(function (MessageType) {
    MessageType["System"] = "system";
    MessageType["Text"] = "text";
    MessageType["Image"] = "image";
    MessageType["Video"] = "video";
    MessageType["Document"] = "document";
    MessageType["Gif"] = "gif";
    MessageType["Emoji"] = "emoji";
    MessageType["Audio"] = "audio";
})(MessageType = exports.MessageType || (exports.MessageType = {}));
var HighLightType;
(function (HighLightType) {
    HighLightType["Text"] = "text";
    HighLightType["Email"] = "email";
    HighLightType["Phone"] = "phone";
    HighLightType["Url"] = "url";
    HighLightType["UserId"] = "userId";
})(HighLightType = exports.HighLightType || (exports.HighLightType = {}));
var SystemMessageType;
(function (SystemMessageType) {
    SystemMessageType["MemberRemoved"] = "MemberRemoved";
    SystemMessageType["MemberAdded"] = "MemberAdded";
    SystemMessageType["OwnerRemoved"] = "OwnerRemoved";
    SystemMessageType["OwnerAdded"] = "OwnerAdded";
    SystemMessageType["GroupPictureChanged"] = "GroupPictureChanged";
    SystemMessageType["GroupNameChanged"] = "GroupNameChanged";
    SystemMessageType["GroupDisabled"] = "GroupDisabled";
    SystemMessageType["GroupCreated"] = "GroupCreated";
    SystemMessageType["IndividualChatCreated"] = "IndividualChatCreated";
    SystemMessageType["ExternalUserConversationInvitationAccepted"] = "ExternalUserConversationInvitationAccepted";
    SystemMessageType["ExternalUserNudged"] = "ExternalUserNudged";
})(SystemMessageType = exports.SystemMessageType || (exports.SystemMessageType = {}));
/**
 * Conversation Types
 */
var ConversationType;
(function (ConversationType) {
    ConversationType["Group"] = "group";
    ConversationType["Individual"] = "individual";
})(ConversationType = exports.ConversationType || (exports.ConversationType = {}));
var ConversationGroupType;
(function (ConversationGroupType) {
    ConversationGroupType["Regular"] = "regular";
    ConversationGroupType["Quick"] = "quick";
    ConversationGroupType["Space"] = "space";
    ConversationGroupType["Sms"] = "sms";
})(ConversationGroupType = exports.ConversationGroupType || (exports.ConversationGroupType = {}));
/**
 * User Types
 */
var UserScope;
(function (UserScope) {
    UserScope["Internal"] = "internal";
    UserScope["External"] = "external";
})(UserScope = exports.UserScope || (exports.UserScope = {}));
var UserTypes;
(function (UserTypes) {
    UserTypes["User"] = "user";
    UserTypes["Admin"] = "admin";
    UserTypes["HrAdmin"] = "hr_admin";
    UserTypes["SuperHrAdmin"] = "super_hr_admin";
})(UserTypes = exports.UserTypes || (exports.UserTypes = {}));
/* User Admin Type */
var UserAdminSortFields;
(function (UserAdminSortFields) {
    UserAdminSortFields["UserAdminName"] = "userAdminName";
    UserAdminSortFields["UserAdminEmail"] = "userAdminEmail";
    UserAdminSortFields["UserAdminRole"] = "userAdminRole";
})(UserAdminSortFields = exports.UserAdminSortFields || (exports.UserAdminSortFields = {}));
var UserAdminStatusType;
(function (UserAdminStatusType) {
    UserAdminStatusType["Enabled"] = "enabled";
    UserAdminStatusType["Disabled"] = "disabled";
    UserAdminStatusType["All"] = "all";
})(UserAdminStatusType = exports.UserAdminStatusType || (exports.UserAdminStatusType = {}));
var APISignature;
(function (APISignature) {
    APISignature["ME"] = "me";
})(APISignature = exports.APISignature || (exports.APISignature = {}));
/* User Management Type */
var UserManagementType;
(function (UserManagementType) {
    UserManagementType["Perm"] = "permanent";
    UserManagementType["Temp"] = "temporary";
})(UserManagementType = exports.UserManagementType || (exports.UserManagementType = {}));
var UserManagementSortFields;
(function (UserManagementSortFields) {
    UserManagementSortFields["UserName"] = "userName";
    UserManagementSortFields["UserEmail"] = "userEmail";
    UserManagementSortFields["UserEmpCode"] = "userEmployeeCode";
    UserManagementSortFields["UserDepartment"] = "userDepartment";
    UserManagementSortFields["UserTitle"] = "userTitle";
    UserManagementSortFields["UserHrAdmin"] = "userHrAdmin";
})(UserManagementSortFields = exports.UserManagementSortFields || (exports.UserManagementSortFields = {}));
var UserStatusType;
(function (UserStatusType) {
    UserStatusType["Enabled"] = "enabled";
    UserStatusType["Disabled"] = "disabled";
    UserStatusType["All"] = "all";
})(UserStatusType = exports.UserStatusType || (exports.UserStatusType = {}));
var UserMovementSelected = /** @class */ (function () {
    function UserMovementSelected() {
    }
    return UserMovementSelected;
}());
exports.UserMovementSelected = UserMovementSelected;
var UnmanagedUsersSortFields;
(function (UnmanagedUsersSortFields) {
    UnmanagedUsersSortFields["UserName"] = "userName";
    UnmanagedUsersSortFields["UserEmail"] = "userEmail";
    UnmanagedUsersSortFields["UserEmpCode"] = "userEmployeeCode";
    UnmanagedUsersSortFields["UserDepartment"] = "userDepartment";
    UnmanagedUsersSortFields["UserTitle"] = "userTitle";
})(UnmanagedUsersSortFields = exports.UnmanagedUsersSortFields || (exports.UnmanagedUsersSortFields = {}));
var ConversationUserQueryType;
(function (ConversationUserQueryType) {
    ConversationUserQueryType["All"] = "all";
    ConversationUserQueryType["Current"] = "current";
    ConversationUserQueryType["Removed"] = "removed";
})(ConversationUserQueryType = exports.ConversationUserQueryType || (exports.ConversationUserQueryType = {}));
var Publisher;
(function (Publisher) {
    var EventType;
    (function (EventType) {
        EventType["NewMessage"] = "NEW_MESSAGE";
        EventType["ConversationCreated"] = "CONVERSATION_CREATED";
        EventType["ConversationMuted"] = "CONVERSATION_MUTED";
        EventType["ConversationUnmuted"] = "CONVERSATION_UNMUTED";
        EventType["ConversationPinned"] = "CONVERSATION_PINNED";
        EventType["ConversationUnpinned"] = "CONVERSATION_UNPINNED";
        EventType["ConversationHideAndClear"] = "CONVERSATION_HIDE_AND_CLEAR";
        EventType["ConversationHideAndClearAll"] = "CONVERSATION_HIDE_AND_CLEAR_ALL";
        EventType["ConversationGroupInfoChanged"] = "CONVERSATION_GROUP_INFO_CHANGED";
        EventType["ConversationQuickGroupChangedToGroup"] = "CONVERSATION_QUICK_GROUP_CHANGED_TO_GROUP";
        EventType["ExternalUserConversationInvitationAccepted"] = "EXTERNAL_USER_CONVERSATION_INVITATION_ACCEPTED";
        EventType["UserInfoChanged"] = "USER_INFO_CHANGED";
        EventType["ConversationGroupOwnerAdded"] = "CONVERSATION_GROUP_OWNER_ADDED";
        EventType["ConversationGroupOwnerRemoved"] = "CONVERSATION_GROUP_OWNER_REMOVED";
        EventType["ConversationGroupMemberAdded"] = "CONVERSATION_GROUP_MEMBER_ADDED";
        EventType["ConversationGroupMemberRemoved"] = "CONVERSATION_GROUP_MEMBER_REMOVED";
        EventType["UserReactedToMessage"] = "USER_REACTED_TO_MESSAGE";
        EventType["UserUnReactedToMessage"] = "USER_UNREACTED_TO_MESSAGE";
        EventType["MessageReactionsUpdate"] = "MESSAGE_REACTIONS_UPDATE";
        EventType["UserDisabled"] = "USER_DISABLED";
        EventType["UserEnabled"] = "USER_ENABLED";
        EventType["MessageDeleted"] = "MESSAGE_DELETED";
        EventType["UserDeleted"] = "USER_DELETED";
        EventType["MessageRead"] = "MESSAGE_READ";
        EventType["ExternalUserNudged"] = "EXTERNAL_USER_NUDGED";
    })(EventType = Publisher.EventType || (Publisher.EventType = {}));
})(Publisher = exports.Publisher || (exports.Publisher = {}));
var SayHey;
(function (SayHey) {
    var ChatKitError;
    (function (ChatKitError) {
        var ErrorType;
        (function (ErrorType) {
            var InternalErrorType;
            (function (InternalErrorType) {
                InternalErrorType["Unknown"] = "InternalUnknown";
                InternalErrorType["Uninitialized"] = "InternalUninitialized";
                InternalErrorType["AuthMissing"] = "InternalAuthMissing";
                InternalErrorType["BadData"] = "InternalBadData";
                InternalErrorType["MissingParameter"] = "InternalMissingParameter";
            })(InternalErrorType = ErrorType.InternalErrorType || (ErrorType.InternalErrorType = {}));
            var AuthErrorType;
            (function (AuthErrorType) {
                AuthErrorType["InvalidKeyErrorType"] = "AuthErrorTypeInvalidKey";
                AuthErrorType["InvalidUserType"] = "AuthErrorTypeInvalidUserType";
                AuthErrorType["NotFoundErrorType"] = "AuthErrorTypeNotFound";
                AuthErrorType["MissingParamsErrorType"] = "AuthErrorTypeMissingParams";
                AuthErrorType["InvalidPassword"] = "AuthErrorTypeInvalidPassword";
                AuthErrorType["TokenExpiredType"] = "AuthErrorTypeTokenExpired";
                AuthErrorType["AppHasNoNotificationSetup"] = "AuthErrorTypeAppHasNoNotificationSetup";
                AuthErrorType["NotPartOfApp"] = "AuthErrorTypeNotPartOfApp";
                AuthErrorType["InvalidWebhookKey"] = "AuthErrorTypeInvalidWebhookKey";
                AuthErrorType["NoAccessToUser"] = "AuthErrorTypeNoAccessToUser";
                AuthErrorType["ServerNoAccessToUser"] = "AuthErrorTypeServerNoAccessToUser";
                AuthErrorType["IncorrectPassword"] = "AuthErrorTypeIncorrectPassword";
                AuthErrorType["NoSamePassword"] = "AuthErrorTypeNoSamePassword";
                AuthErrorType["NoPasswordSet"] = "AuthErrorTypeNoPasswordSet";
                AuthErrorType["PasswordAlreadySet"] = "AuthErrorTypePasswordAlreadySet";
                AuthErrorType["AccountNotPreVerified"] = "AuthErrorTypeAccountNotPreVerified";
                AuthErrorType["AccountAlreadyPreVerified"] = "AuthErrorTypeAccountAlreadyPreVerified";
                AuthErrorType["NoAppAccessToUser"] = "AuthErrorTypeNoAppAccessToUser";
                AuthErrorType["TokenNotStarted"] = "AuthErrorTypeTokenNotStarted";
                AuthErrorType["AccountAlreadyExists"] = "AuthErrorTypeAccountAlreadyExists";
                AuthErrorType["AdminAlreadyExists"] = "AuthErrorTypeAdminAlreadyExists";
                AuthErrorType["UserNotFound"] = "AuthErrorTypeUserNotFound";
                AuthErrorType["InvalidToken"] = "AuthErrorTypeInvalidToken";
                AuthErrorType["MissingAuthHeaderField"] = "AuthErrorTypeMissingAuthHeaderField";
                AuthErrorType["InvalidCredentials"] = "AuthErrorTypeInvalidCredentials";
                AuthErrorType["HashGeneration"] = "AuthErrorTypeHashGeneration";
                AuthErrorType["HashComparison"] = "AuthErrorTypeHashComparison";
                AuthErrorType["RefreshTokenAlreadyExchanged"] = "AuthErrorTypeRefreshTokenAlreadyExchanged";
                AuthErrorType["RefreshTokenNotFound"] = "AuthErrorTypeRefreshTokenNotFound";
                AuthErrorType["UserDisabled"] = "AuthErrorTypeUserDisabled";
                AuthErrorType["InvalidOTP"] = "AuthErrorTypeInvalidOTP";
                AuthErrorType["OTPExpired"] = "AuthErrorTypeOTPExpired";
                AuthErrorType["AdminNotAssociatedWithApp"] = "AuthErrorTypeAdminNotAssociatedWithApp";
                AuthErrorType["InvitationCodeNotFound"] = "AuthErrorTypeInvitationCodeNotFound";
                AuthErrorType["InvitationCodeExpired"] = "AuthErrorTypeInvitationCodeExpired";
                AuthErrorType["InvitationCodeRedeemed"] = "AuthErrorTypeInvitationCodeRedeemed";
                AuthErrorType["InvitationNotForUser"] = "AuthErrorTypeInvitationNotForUser";
                AuthErrorType["UserAlreadySignedUp"] = "AuthErrorTypeUserAlreadySignedUp";
                AuthErrorType["SignUpIncomplete"] = "AuthErrorTypeSignUpIncomplete";
                AuthErrorType["UserInactive"] = "AuthErrorTypeUserInactive";
                AuthErrorType["SignInIntegrated"] = "AuthErrorTypeSignInIntegrated";
                AuthErrorType["SelfSignUpNotAllowed"] = "AuthErrorTypeSelfSignUpNotAllowed";
                AuthErrorType["EmailDoesNotExist"] = "AuthErrorTypeEmailDoesNotExist";
                AuthErrorType["MissingIntegratedLoginUrl"] = "AuthErrorTypeMissingIntegratedLoginUrl";
                AuthErrorType["InvalidIntegratedLoginUrl"] = "AuthErrorTypeInvalidIntegratedLoginUrl";
                AuthErrorType["YouMustAcceptTermsAndPrivacy"] = "AuthErrorTypeYouMustAcceptTermsAndPrivacy";
                AuthErrorType["TooManyFailedAttempts"] = "AuthErrorTypeTooManyFailedAttempts";
                AuthErrorType["InCorrectUserNameOrPassword"] = "AuthErrorTypeInCorrectUserNameOrPassword";
                AuthErrorType["InCorrectPhoneNumberOrOtp"] = "AuthErrorTypeInCorrectPhoneNumberOrOtp";
                AuthErrorType["InCorrectUserOrOtp"] = "AuthErrorTypeInCorrectUserOrOtp";
            })(AuthErrorType = ErrorType.AuthErrorType || (ErrorType.AuthErrorType = {}));
            var FileErrorType;
            (function (FileErrorType) {
                FileErrorType["SizeLimitExceeded"] = "FileErrorTypeSizeLimitExceeded";
                FileErrorType["NoFileSentType"] = "FileErrorTypeNoFileSentType";
                FileErrorType["InvalidFieldNameForFile"] = "FileErrorTypeInvalidFieldNameForFile";
                FileErrorType["InvalidMetadata"] = "FileErrorTypeInvalidMetadata";
                FileErrorType["FileSizeLimitExceeded"] = "FileErrorTypeFileSizeLimitExceeded";
                FileErrorType["InvalidFileName"] = "FileErrorTypeInvalidFileName";
                FileErrorType["NotSent"] = "FileErrorTypeNotSent";
                FileErrorType["NotFound"] = "FileErrorTypeNotFound";
                FileErrorType["NoThumbnailFound"] = "FileErrorTypeNoThumbnailFound";
                FileErrorType["ImageNotForConversation"] = "FileErrorTypeImageNotForConversation";
                FileErrorType["ImageNotForUser"] = "FileErrorTypeImageNotForUser";
                FileErrorType["FileNotForMessage"] = "FileErrorTypeFileNotForMessage";
                FileErrorType["SizeNotFound"] = "FileErrorTypeSizeNotFound";
                FileErrorType["NotForApp"] = "FileErrorTypeNotForApp";
                FileErrorType["IncorrectFileType"] = "FileErrorTypeIncorrectFileType";
                FileErrorType["NoHeadersFound"] = "FileErrorTypeNoHeadersFound";
                FileErrorType["InvalidHeaders"] = "FileErrorTypeInvalidHeaders";
            })(FileErrorType = ErrorType.FileErrorType || (ErrorType.FileErrorType = {}));
            var ServerErrorType;
            (function (ServerErrorType) {
                ServerErrorType["UnknownError"] = "UnknownError";
                ServerErrorType["InternalServerError"] = "InternalServerError";
            })(ServerErrorType = ErrorType.ServerErrorType || (ErrorType.ServerErrorType = {}));
            var GeneralErrorType;
            (function (GeneralErrorType) {
                GeneralErrorType["MissingQueryParams"] = "GeneralErrorTypeMissingQueryParams";
                GeneralErrorType["RouteNotFound"] = "GeneralErrorTypeRouteNotFound";
                GeneralErrorType["LimitNotValid"] = "GeneralErrorTypeLimitNotValid";
                GeneralErrorType["OffsetNotValid"] = "GeneralErrorTypeOffsetNotValid";
                GeneralErrorType["MissingInfoToUpdate"] = "GeneralErrorTypeMissingInfoToUpdate";
            })(GeneralErrorType = ErrorType.GeneralErrorType || (ErrorType.GeneralErrorType = {}));
            var MessageErrorType;
            (function (MessageErrorType) {
                MessageErrorType["InvalidDate"] = "MessageErrorTypeInvalidDate";
                MessageErrorType["NotFound"] = "MessageErrorTypeNotFound";
                MessageErrorType["NotFoundForUser"] = "MessageErrorTypeNotFoundForUser";
                MessageErrorType["MessageAlreadyRead"] = "MessageErrorTypeMessageAlreadyRead";
                MessageErrorType["CannotDeleteSystemMessage"] = "MessageErrorTypeCannotDeleteSystemMessage";
                MessageErrorType["MissingMessageId"] = "MessageErrorTypeMissingMessageId";
                MessageErrorType["IsoDateStringRequired"] = "MessageErrorTypeIsoDateStringRequired";
            })(MessageErrorType = ErrorType.MessageErrorType || (ErrorType.MessageErrorType = {}));
            var UserErrorType;
            (function (UserErrorType) {
                UserErrorType["AlreadyMuted"] = "UserErrorTypeAlreadyMuted";
                UserErrorType["NotOnMute"] = "UserErrorTypeNotOnMute";
                UserErrorType["AlreadyEnabled"] = "UserErrorTypeAlreadyEnabled";
                UserErrorType["AlreadyDisabled"] = "UserErrorTypeAlreadyDisabled";
                UserErrorType["NotFound"] = "UserErrorTypeNotFound";
            })(UserErrorType = ErrorType.UserErrorType || (ErrorType.UserErrorType = {}));
            var UserRegistrationErrorType;
            (function (UserRegistrationErrorType) {
                UserRegistrationErrorType["ContactCompanyAdmin"] = "UserRegistrationErrorTypeContactCompanyAdmin";
            })(UserRegistrationErrorType = ErrorType.UserRegistrationErrorType || (ErrorType.UserRegistrationErrorType = {}));
            var ConversationErrorType;
            (function (ConversationErrorType) {
                ConversationErrorType["NotFound"] = "ConversationErrorTypeNotFound";
                ConversationErrorType["CannotConverseWithUser"] = "ConversationErrorTypeCannotConverseWithUser";
                ConversationErrorType["NotAnOwner"] = "ConversationErrorTypeNotAnOwner";
                ConversationErrorType["AlreadyAnOwner"] = "ConversationErrorTypeAlreadyAnOwner";
                ConversationErrorType["NotFoundForApp"] = "ConversationErrorTypeNotFoundForApp";
                ConversationErrorType["InvalidType"] = "ConversationErrorTypeInvalidType";
                ConversationErrorType["InvalidConversationGroupType"] = "ConversationErrorTypeInvalidConversationGroupType";
                ConversationErrorType["InvalidConversationUserType"] = "ConversationErrorTypeInvalidConversationUserType";
                ConversationErrorType["MustBeOwner"] = "ConversationErrorTypeMustBeOwner";
                ConversationErrorType["MustBeCreator"] = "ConversationErrorTypeMustBeCreator";
                ConversationErrorType["NotPartOfApp"] = "ConversationErrorTypeNotPartOfApp";
                ConversationErrorType["NotFoundForUser"] = "ConversationErrorTypeNotFoundForUser";
                ConversationErrorType["AlreadyExists"] = "ConversationErrorTypeAlreadyExists";
                ConversationErrorType["CannotChatWithSelf"] = "ConversationErrorTypeCannotChatWithSelf";
                ConversationErrorType["AlreadyMuted"] = "ConversationErrorTypeAlreadyMuted";
                ConversationErrorType["NotOnMute"] = "ConversationErrorTypeNotOnMute";
                ConversationErrorType["AlreadyPinned"] = "ConversationErrorTypeAlreadyPinned";
                ConversationErrorType["NotPinned"] = "ConversationErrorTypeNotPinned";
                ConversationErrorType["UsersNotInApp"] = "ConversationErrorTypeUsersNotInApp";
                ConversationErrorType["NoValidUsersToAdd"] = "ConversationErrorTypeNoValidUsersToAdd";
                ConversationErrorType["NoValidUsersToRemove"] = "ConversationErrorTypeNoValidUsersToRemove";
                ConversationErrorType["CannotChangeAccessToSelf"] = "ConversationErrorTypeCannotChangeAccessToSelf";
                ConversationErrorType["CannotAddExistingUsers"] = "ConversationErrorTypeCannotAddExistingUsers";
                ConversationErrorType["AlreadyRemovedUsers"] = "ConversationErrorTypeAlreadyRemovedUsers";
                ConversationErrorType["UsersNotInConversation"] = "ConversationErrorTypeUsersNotInConversation";
                ConversationErrorType["CannotRemoveOwners"] = "ConversationErrorTypeCannotRemoveOwners";
                ConversationErrorType["DivisionsNotInApp"] = "ConversationErrorTypeDivisionsNotInApp";
                ConversationErrorType["CannotAddExistingDivisions"] = "ConversationErrorTypeCannotAddExistingDivisions";
                ConversationErrorType["CannotRemoveNonExistingDivisions"] = "ConversationErrorTypeCannotRemoveNonExistingDivisions";
                ConversationErrorType["DivisionIdOrMemberIdRequiredForSpaceGroup"] = "ConversationErrorTypeDivisionIdOrMemberIdRequiredForSpaceGroup";
                ConversationErrorType["NudgeLessThanThreshold"] = "ConversationErrorTypeNudgeLessThanThreshold";
            })(ConversationErrorType = ErrorType.ConversationErrorType || (ErrorType.ConversationErrorType = {}));
            var UserManagementErrorType;
            (function (UserManagementErrorType) {
                UserManagementErrorType["NoSameAdminAssignment"] = "UserManagementErrorTypeNoSameAdminAssignment";
                UserManagementErrorType["MustBeHrAdmin"] = "UserManagementErrorTypeMustBeHrAdmin";
                UserManagementErrorType["NoDuplicateUsers"] = "UserManagementErrorTypeNoDuplicateUsers";
                UserManagementErrorType["NotHrAdmin"] = "UserManagementErrorTypeNotHrAdmin";
                UserManagementErrorType["UserAlreadyAssigned"] = "UserManagementErrorTypeUserAlreadyAssigned";
            })(UserManagementErrorType = ErrorType.UserManagementErrorType || (ErrorType.UserManagementErrorType = {}));
            var DateErrorType;
            (function (DateErrorType) {
                DateErrorType["InvalidFromDateError"] = "DateErrorTypeInvalidFromDateError";
                DateErrorType["InvalidToDateError"] = "DateErrorTypeInvalidToDateError";
            })(DateErrorType = ErrorType.DateErrorType || (ErrorType.DateErrorType = {}));
            var AppRegistrationErrorType;
            (function (AppRegistrationErrorType) {
                AppRegistrationErrorType["MissingEmailConfigParams"] = "AppRegistrationErrorTypeMissingEmailConfigParams";
            })(AppRegistrationErrorType = ErrorType.AppRegistrationErrorType || (ErrorType.AppRegistrationErrorType = {}));
            var OTPErrorType;
            (function (OTPErrorType) {
                OTPErrorType["MissingEmailService"] = "OTPErrorTypeMissingEmailService";
            })(OTPErrorType = ErrorType.OTPErrorType || (ErrorType.OTPErrorType = {}));
            var EmailErrorType;
            (function (EmailErrorType) {
                EmailErrorType["MissingEmailService"] = "EmailErrorTypeMissingEmailService";
                EmailErrorType["EmailDomainNotAllowed"] = "EmailErrorTypeEmailDomainNotAllowed";
            })(EmailErrorType = ErrorType.EmailErrorType || (ErrorType.EmailErrorType = {}));
            var SmsErrorType;
            (function (SmsErrorType) {
                SmsErrorType["NotConfigured"] = "SmsErrorTypeNotConfigured";
                SmsErrorType["NotConfiguredForClient"] = "SmsErrorTypeNotConfiguredForClient";
                SmsErrorType["RequiredCountryCode"] = "SmsErrorTypeRequiredCountryCode";
                SmsErrorType["CountryCodeNotSupported"] = "SmsErrorTypeCountryCodeNotSupported";
            })(SmsErrorType = ErrorType.SmsErrorType || (ErrorType.SmsErrorType = {}));
            var DepartmentErrorTpe;
            (function (DepartmentErrorTpe) {
                DepartmentErrorTpe["NotFound"] = "DepartmentErrorTypeNotFound";
                DepartmentErrorTpe["AlreadyExists"] = "DepartmentErrorTypeAlreadyExists";
                DepartmentErrorTpe["MissingInfoToUpdate"] = "DepartmentErrorTypeMissingInfoToUpdate";
                DepartmentErrorTpe["MissingDefaultDeptOrDiv"] = "DepartmentErrorTypeMissingDefaultDeptOrDiv";
                DepartmentErrorTpe["UpdateDefaultNotAllowed"] = "DepartmentErrorTypeUpdateDefaultNotAllowed";
            })(DepartmentErrorTpe = ErrorType.DepartmentErrorTpe || (ErrorType.DepartmentErrorTpe = {}));
            var DivisionErrorType;
            (function (DivisionErrorType) {
                DivisionErrorType["NotPartOfDepartment"] = "DivisionErrorTypeNotPartOfDepartment";
                DivisionErrorType["AlreadyExists"] = "DivisionErrorTypeAlreadyExists";
                DivisionErrorType["DepartmentRequired"] = "DivisionErrorTypeDepartmentRequired";
                DivisionErrorType["UpdateDefaultNotAllowed"] = "DivisionErrorTypeUpdateDefaultNotAllowed";
            })(DivisionErrorType = ErrorType.DivisionErrorType || (ErrorType.DivisionErrorType = {}));
            var AppFeatureErrorTpe;
            (function (AppFeatureErrorTpe) {
                AppFeatureErrorTpe["NotFound"] = "AppFeatureErrorTpeNotFound";
                AppFeatureErrorTpe["AlreadyExists"] = "AppFeatureErrorTypeAlreadyExists";
                AppFeatureErrorTpe["InvalidParams"] = "AppFeatureErrorTypeInvalidParams";
                AppFeatureErrorTpe["AdminPortalLink"] = "AppFeatureErrorTypeAdminPortalLink";
            })(AppFeatureErrorTpe = ErrorType.AppFeatureErrorTpe || (ErrorType.AppFeatureErrorTpe = {}));
            var UserAdminErrorType;
            (function (UserAdminErrorType) {
                UserAdminErrorType["NotFound"] = "UserAdminErrorTypeNotFound";
                UserAdminErrorType["AlreadyExists"] = "UserAdminErrorTypeAlreadyExists";
                UserAdminErrorType["NoAccessToUserAdmin"] = "UserAdminErrorTypeNoAccessToUserAdmin";
                UserAdminErrorType["NoEmailUpdateAccessOfSignedUpUsers"] = "UserAdminErrorTypeNoEmailUpdateAccessOfSignedUpUsers";
                UserAdminErrorType["CannotUpdateSelf"] = "UserAdminErrorTypeCannotUpdateSelf";
                UserAdminErrorType["UserAdminNoAccessToUser"] = "UserAdminNoAccessToUser";
                UserAdminErrorType["NoChangeInLeaveStatus"] = "UserAdminErrorTypeNoChangeInLeaveStatus";
                UserAdminErrorType["SignUpIncomplete"] = "UserAdminErrorTypeSignUpIncomplete";
                UserAdminErrorType["Disabled"] = "UserAdminErrorTypeDisabled";
            })(UserAdminErrorType = ErrorType.UserAdminErrorType || (ErrorType.UserAdminErrorType = {}));
            var AppConfigErrorType;
            (function (AppConfigErrorType) {
                AppConfigErrorType["NotFound"] = "AppConfigErrorTypeNotFound";
                AppConfigErrorType["AlreadyExists"] = "AppConfigErrorTypeAlreadyExists";
            })(AppConfigErrorType = ErrorType.AppConfigErrorType || (ErrorType.AppConfigErrorType = {}));
            var KeywordErrorType;
            (function (KeywordErrorType) {
                KeywordErrorType["NoDuplicates"] = "KeywordErrorTypeNoDuplicates";
                KeywordErrorType["AlreadyExists"] = "KeywordErrorTypeAlreadyExists";
                KeywordErrorType["NotFound"] = "KeywordErrorTypeNotFound";
                KeywordErrorType["NoChangeInPriority"] = "KeywordErrorTypeNoChangeInPriority";
            })(KeywordErrorType = ErrorType.KeywordErrorType || (ErrorType.KeywordErrorType = {}));
            var UserAdminUpdateRoleType;
            (function (UserAdminUpdateRoleType) {
                UserAdminUpdateRoleType["CannotUpdateRoleOfSelf"] = "UserAdminUpdateRoleTypeCannotUpdateRoleOfSelf";
                UserAdminUpdateRoleType["ManagedUserExist"] = "UserAdminUpdateRoleTypeManagedUserExist";
                UserAdminUpdateRoleType["AlreadyHasRequestedRole"] = "UserAdminErrorTypeAlreadyHasRequestedRole";
            })(UserAdminUpdateRoleType = ErrorType.UserAdminUpdateRoleType || (ErrorType.UserAdminUpdateRoleType = {}));
            var FlaggedMessageErrorType;
            (function (FlaggedMessageErrorType) {
                FlaggedMessageErrorType["AlreadyReported"] = "FlaggedMessageErrorTypeAlreadyReported";
                FlaggedMessageErrorType["NotFound"] = "FlaggedMessageErrorTypeNotFound";
                FlaggedMessageErrorType["AlreadyResolved"] = "FlaggedMessageErrorTypeAlreadyResolved";
            })(FlaggedMessageErrorType = ErrorType.FlaggedMessageErrorType || (ErrorType.FlaggedMessageErrorType = {}));
            var UserOnboardingJobErrorType;
            (function (UserOnboardingJobErrorType) {
                UserOnboardingJobErrorType["NotFound"] = "UserOnboardingJobErrorTypeNotFound";
                UserOnboardingJobErrorType["NotCompleted"] = "UserOnboardingJobErrorTypeNotCompleted";
            })(UserOnboardingJobErrorType = ErrorType.UserOnboardingJobErrorType || (ErrorType.UserOnboardingJobErrorType = {}));
            var ExternalUserErrorType;
            (function (ExternalUserErrorType) {
                ExternalUserErrorType["ContactAlreadyExists"] = "ExternalUserErrorTypeContactAlreadyExists";
                ExternalUserErrorType["NotFound"] = "ExternalUserErrorTypeNotFound";
                ExternalUserErrorType["UserDisabled"] = "ExternalUserErrorTypeUserDisabled";
                ExternalUserErrorType["InvalidPhoneNumber"] = "ExternalUserErrorTypeInvalidPhoneNumber";
            })(ExternalUserErrorType = ErrorType.ExternalUserErrorType || (ErrorType.ExternalUserErrorType = {}));
            var ContactErrorType;
            (function (ContactErrorType) {
                ContactErrorType["ContactAlreadyExists"] = "ContactErrorTypeContactAlreadyExists";
            })(ContactErrorType = ErrorType.ContactErrorType || (ErrorType.ContactErrorType = {}));
            var SmsConversationErrorType;
            (function (SmsConversationErrorType) {
                SmsConversationErrorType["NotAnExternalUser"] = "SmsConversationErrorTypeNotAnExternalUser";
                SmsConversationErrorType["ExternalUserNotFound"] = "SmsConversationErrorTypeNotFoundExternalUserNotFound";
            })(SmsConversationErrorType = ErrorType.SmsConversationErrorType || (ErrorType.SmsConversationErrorType = {}));
            var SmsConversationInvitationErrorType;
            (function (SmsConversationInvitationErrorType) {
                SmsConversationInvitationErrorType["NotFound"] = "SmsConversationInvitationErrorTypeNotFound";
                SmsConversationInvitationErrorType["InvitationNotAccepted"] = "SmsConversationInvitationErrorTypeInvitationNotAccepted";
                SmsConversationInvitationErrorType["InvitationAlreadyAccepted"] = "SmsConversationInvitationErrorTypeInvitationAlreadyAccepted";
                SmsConversationInvitationErrorType["InvitationExpired"] = "SmsConversationInvitationErrorTypeInvitationExpired";
            })(SmsConversationInvitationErrorType = ErrorType.SmsConversationInvitationErrorType || (ErrorType.SmsConversationInvitationErrorType = {}));
        })(ErrorType = ChatKitError.ErrorType || (ChatKitError.ErrorType = {}));
        var PushNotificationError;
        (function (PushNotificationError) {
            PushNotificationError["InvalidPushParams"] = "DeviceTokenErrorTypeInvalidPushParams";
            PushNotificationError["AlreadyRegisteredForUser"] = "DeviceTokenErrorTypeAlreadyRegisteredForUser";
            PushNotificationError["NotFound"] = "DeviceTokenErrorTypeNotFound";
            PushNotificationError["NoTokensToPush"] = "DeviceTokenErrorTypeNoTokensToPush";
            PushNotificationError["OnlyStringData"] = "DeviceTokenErrorTypeOnlyStringData";
        })(PushNotificationError = ChatKitError.PushNotificationError || (ChatKitError.PushNotificationError = {}));
        var AppError = /** @class */ (function () {
            function AppError(err, type) {
                var _this = this;
                this.type = ErrorType.InternalErrorType.Unknown;
                this.setStatusCode = function (statusCode) {
                    _this.statusCode = statusCode;
                };
                this.setType = function (type) {
                    _this.type = type;
                };
                this.setMessage = function (msg) {
                    _this.message = msg;
                };
                this.setErrorActions = function (params) {
                    var authFailed = params.authFailed, exchange = params.exchange;
                    _this.exchange = exchange;
                    _this.authFailed = authFailed;
                };
                if (type) {
                    this.type = type;
                }
                if (err instanceof SayHey.Error) {
                    this.error = err;
                    this.message = err.message;
                }
                else {
                    this.message = err;
                }
            }
            return AppError;
        }());
        ChatKitError.AppError = AppError;
    })(ChatKitError = SayHey.ChatKitError || (SayHey.ChatKitError = {}));
    SayHey.EventType = Publisher.EventType;
    // SAFE - Used for export
    // eslint-disable-next-line no-unused-vars
    SayHey.ErrorType = ChatKitError.ErrorType;
    SayHey.Error = ChatKitError.AppError;
})(SayHey = exports.SayHey || (exports.SayHey = {}));
/* Departments and Divisions */
/* Params */
var tempDefaultColumn;
(function (tempDefaultColumn) {
    tempDefaultColumn[tempDefaultColumn["isDefault"] = 1] = "isDefault";
})(tempDefaultColumn = exports.tempDefaultColumn || (exports.tempDefaultColumn = {}));
var SortOrderType;
(function (SortOrderType) {
    SortOrderType[SortOrderType["ASC"] = 1] = "ASC";
    SortOrderType[SortOrderType["DESC"] = -1] = "DESC";
})(SortOrderType = exports.SortOrderType || (exports.SortOrderType = {}));
var DepartmentSortFields;
(function (DepartmentSortFields) {
    DepartmentSortFields["DepartmentName"] = "departmentName";
})(DepartmentSortFields = exports.DepartmentSortFields || (exports.DepartmentSortFields = {}));
var DivisionSortFields;
(function (DivisionSortFields) {
    DivisionSortFields["DivisionName"] = "divisionName";
})(DivisionSortFields = exports.DivisionSortFields || (exports.DivisionSortFields = {}));
var TokenType;
(function (TokenType) {
    TokenType["AccessToken"] = "TokenTypeAccessToken";
    TokenType["RefreshToken"] = "TokenTypeRefreshToken";
    TokenType["PublisherToken"] = "TokenTypePublisherToken";
    TokenType["ResetPasswordToken"] = "TokenTypeResetPasswordToken";
    TokenType["NewUserSetPasswordToken"] = "TokenTypeNewUserSetPasswordToken";
    TokenType["VerifyAccountToken"] = "TokenTypeVerifyAccountToken";
})(TokenType = exports.TokenType || (exports.TokenType = {}));
/* Dashboard Types */
var TimeFrameType;
(function (TimeFrameType) {
    TimeFrameType["AllTime"] = "allTime";
    TimeFrameType["Month"] = "month";
    TimeFrameType["Year"] = "year";
})(TimeFrameType = exports.TimeFrameType || (exports.TimeFrameType = {}));
var UnresolvedMsgScopeType;
(function (UnresolvedMsgScopeType) {
    UnresolvedMsgScopeType["Global"] = "global";
    UnresolvedMsgScopeType["Mine"] = "mine";
})(UnresolvedMsgScopeType = exports.UnresolvedMsgScopeType || (exports.UnresolvedMsgScopeType = {}));
/* Keyword Types */
var KeyWordPriorityTypes;
(function (KeyWordPriorityTypes) {
    KeyWordPriorityTypes["High"] = "high";
    KeyWordPriorityTypes["Low"] = "low";
})(KeyWordPriorityTypes = exports.KeyWordPriorityTypes || (exports.KeyWordPriorityTypes = {}));
/* Message Flagging Types */
var FlaggedMessageType;
(function (FlaggedMessageType) {
    FlaggedMessageType["HIGH"] = "high";
    FlaggedMessageType["LOW"] = "low";
    FlaggedMessageType["USER_REPORTED"] = "user_reported";
})(FlaggedMessageType = exports.FlaggedMessageType || (exports.FlaggedMessageType = {}));
/* Audit Types */
var QueryCondition;
(function (QueryCondition) {
    QueryCondition["AND"] = "and";
    QueryCondition["OR"] = "or";
})(QueryCondition = exports.QueryCondition || (exports.QueryCondition = {}));
var FlaggedMessagePriorityType;
(function (FlaggedMessagePriorityType) {
    FlaggedMessagePriorityType["HIGH"] = "high";
    FlaggedMessagePriorityType["LOW"] = "low";
})(FlaggedMessagePriorityType = exports.FlaggedMessagePriorityType || (exports.FlaggedMessagePriorityType = {}));
var ResolutionStatusType;
(function (ResolutionStatusType) {
    ResolutionStatusType["NO_ACTION"] = "no_action";
    ResolutionStatusType["ACTION_TAKEN"] = "action_taken";
})(ResolutionStatusType = exports.ResolutionStatusType || (exports.ResolutionStatusType = {}));
var UserOnboardingJobStatus;
(function (UserOnboardingJobStatus) {
    UserOnboardingJobStatus["Completed"] = "completed";
    UserOnboardingJobStatus["Pending"] = "pending";
    UserOnboardingJobStatus["InProgress"] = "in_progress";
})(UserOnboardingJobStatus = exports.UserOnboardingJobStatus || (exports.UserOnboardingJobStatus = {}));
var UserOnboardingJobRecordStatus;
(function (UserOnboardingJobRecordStatus) {
    UserOnboardingJobRecordStatus["Completed"] = "completed";
    UserOnboardingJobRecordStatus["Pending"] = "pending";
    UserOnboardingJobRecordStatus["Errored"] = "errored";
})(UserOnboardingJobRecordStatus = exports.UserOnboardingJobRecordStatus || (exports.UserOnboardingJobRecordStatus = {}));
//# sourceMappingURL=types.js.map