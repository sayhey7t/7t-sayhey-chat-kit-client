[@7t/sayhey-chat-kit-client](README.md) › [Globals](globals.md)

# @7t/sayhey-chat-kit-client

# Full Stack Sayhey Setup

**Sayhey Repositories**

- Main repos

  - [7t-sayhey](https://bitbucket.org/7Tablets/7t-sayhey.git)
  - [7t-sayhey-file-ms](https://bitbucket.org/7Tablets/7t-sayhey-file-ms.git)
  - [7t-publisher](https://bitbucket.org/7Tablets/7t-publisher.git)
  - [7t-notification](https://bitbucket.org/7Tablets/7t-notification.git)
  - [7t-sayhey-mobile-kit](https://bitbucket.org/7Tablets/7t-sayhey-mobile-kit.git)

- Kit repos
  - [7t-sayhey-chat-kit-client](https://bitbucket.org/7Tablets/7t-sayhey-chat-kit-client.git)
  - [7t-sayhey-chat-kit](https://bitbucket.org/7Tablets/7t-sayhey-chat-kit.git)
  - [7t-push-notification-kit](https://bitbucket.org/7Tablets/7t-push-notification-kit.git)
  - [7t-publisher-kit-client](https://bitbucket.org/7Tablets/7t-publisher-kit-client.git)
  - [7t-publisher-kit](https://bitbucket.org/7Tablets/7t-publisher-kit.git)

## Backend Dev Setup

### Install redis locally

```bash

$ mkdir redis && cd redis
$ curl -O http://download.redis.io/redis-stable.tar.gz
$ tar xzvf redis-stable.tar.gz
$ cd redis-stable
$ make sudo make install

```

**7t-sayhey**

- Install mysql(5.7.25) locally [download](https://drive.google.com/file/d/1mzWYXp5ZsvzodwhNMe7HNiRsX6ICmE1R/view?usp=sharing)
- During install make sure to save user name and password . Username : root@localhost Password : <YOUR_PASSWORD>
- Connect to local mysql db

  ```bash
    $ mysql -u root -p

    mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY '7Tablets!';
  ```

- Create user and db for sayhey

  - NOTE: Database name must not be camelCase to avoid typeorm generate issues
  - NOTE: Database name has to be lowercase for generate:migration to work

  ```sql
  #For Development
  CREATE USER 'sayheyloc'@'localhost' IDENTIFIED BY 'sayheyloc';
  CREATE DATABASE sayheyloc;
  GRANT ALL PRIVILEGES ON sayheyloc.* TO 'sayheyloc'@'localhost';

  #For test
  CREATE USER 'sayheytest'@'localhost' IDENTIFIED BY 'sayheytest';
  CREATE DATABASE sayheytest;
  GRANT ALL PRIVILEGES ON sayheytest.* TO 'sayheytest'@'localhost';
  ```

  - Create `.env` file in project root folder and copy contents from [confuence 7t-sayhey](https://7tsayhey.atlassian.net/wiki/spaces/SAYHEY/pages/62390498/Local+Env+files)
  - Install npm packages `npm i`
  - Run migration `npm run migration:run`
  - Run SQL script on local for app initialization from [confuence 7t-sayhey](https://7tsayhey.atlassian.net/wiki/spaces/SAYHEY/pages/62554318/Local+SQL+Script)
  - Start the server : `npm run start`
  - Hit status API from Postman and verify

**7t-notification**

- Install mysql(5.7.25) locally [download](https://drive.google.com/file/d/1mzWYXp5ZsvzodwhNMe7HNiRsX6ICmE1R/view?usp=sharing)
- During install make sure to save user name and password . Username : root@localhost Password : <YOUR_PASSWORD>
- Connect to local mysql db

  ```bash
    $ mysql -u root -p

    mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY '7Tablets!';
  ```

- Create user and db for notification

  - NOTE: Database name must not be camelCase to avoid typeorm generate issues
  - NOTE: Database name has to be lowercase for generate:migration to work

  ```sql
  #For Development
  CREATE USER 'notificationloc'@'localhost' IDENTIFIED BY 'notificationloc';
  CREATE DATABASE notificationloc;
  GRANT ALL PRIVILEGES ON notificationloc.* TO 'notificationloc'@'localhost';

  #For test
  CREATE USER 'notificationtest'@'localhost' IDENTIFIED BY 'notificationtest';
  CREATE DATABASE notificationtest;
  GRANT ALL PRIVILEGES ON notificationtest.* TO 'notificationtest'@'localhost';
  ```

  - Create .env file and copy contents from [confuence 7t-notification](https://7tsayhey.atlassian.net/wiki/spaces/SAYHEY/pages/62390498/Local+Env+files)
  - Install npm packages `npm i`
  - Run migration `npm run migration:run`
  - Run SQL script on local for app initialization from [confuence 7t-notification](https://7tsayhey.atlassian.net/wiki/spaces/SAYHEY/pages/62554318/Local+SQL+Script)
  - Start the server : `npm run start`
  - Hit status API from Postman and verify

**7t-publisher**

- Install mysql(5.7.25) locally [download](https://drive.google.com/file/d/1mzWYXp5ZsvzodwhNMe7HNiRsX6ICmE1R/view?usp=sharing)
- During install make sure to save user name and password . Username : root@localhost Password : your_password
- Connect to local mysql db

  ```bash
    $ mysql -u root -p

    mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY '7Tablets!';
  ```

- Create user and db for publisher

  - NOTE: Database name must not be camelCase to avoid typeorm generate issues
  - NOTE: Database name has to be lowercase for generate:migration to work

  ```sql
  #For Development
  CREATE USER 'publisherloc'@'localhost' IDENTIFIED BY 'publisherloc';
  CREATE DATABASE publisherloc;
  GRANT ALL PRIVILEGES ON publisherloc.* TO 'publisherloc'@'localhost';

  #For test
  CREATE USER 'publishertest'@'localhost' IDENTIFIED BY 'publishertest';
  CREATE DATABASE publishertest;
  GRANT ALL PRIVILEGES ON publishertest.* TO 'publishertest'@'localhost';
  ```

  - Create .env file and copy contents from [confuence 7t-publisher](https://7tsayhey.atlassian.net/wiki/spaces/SAYHEY/pages/62390498/Local+Env+files)
  - Install npm packages `npm i`
  - Run migration `npm run migration:run`
  - Run SQL script on local for app initialization from [confuence 7t-publisher](https://7tsayhey.atlassian.net/wiki/spaces/SAYHEY/pages/62554318/Local+SQL+Script)
  - Start the server : `npm run start`
  - Hit status API from Postman and verify

**7t-sayhey-file-ms**

- Create .env file and copy contents from [confuence 7t-sayhey-file-ms](https://7tsayhey.atlassian.net/wiki/spaces/SAYHEY/pages/62390498/Local+Env+files)
- Install npm packages `npm i`
- Run migration `npm run migration:run`
- Start the server : `npm run start`
- Hit status API from Postman and verify
