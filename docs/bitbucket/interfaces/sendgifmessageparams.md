[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SendGifMessageParams](sendgifmessageparams.md)

# Interface: SendGifMessageParams

## Hierarchy

* [SendMessageParamsBase](sendmessageparamsbase.md)

  ↳ **SendGifMessageParams**

## Index

### Properties

* [clientRefId](sendgifmessageparams.md#markdown-header-clientrefid)
* [conversationId](sendgifmessageparams.md#markdown-header-conversationid)
* [text](sendgifmessageparams.md#markdown-header-optional-text)
* [type](sendgifmessageparams.md#markdown-header-type)
* [url](sendgifmessageparams.md#markdown-header-url)

## Properties

###  clientRefId

• **clientRefId**: *string*

*Inherited from [SendMessageParamsBase](sendmessageparamsbase.md).[clientRefId](sendmessageparamsbase.md#markdown-header-clientrefid)*

Defined in types.ts:1689

___

###  conversationId

• **conversationId**: *string*

*Inherited from [SendMessageParamsBase](sendmessageparamsbase.md).[conversationId](sendmessageparamsbase.md#markdown-header-conversationid)*

Defined in types.ts:1688

___

### `Optional` text

• **text**? : *undefined | string*

Defined in types.ts:1704

___

###  type

• **type**: *[Gif](../enums/messagetype.md#markdown-header-gif)*

Defined in types.ts:1703

___

###  url

• **url**: *string*

Defined in types.ts:1705
