[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationGroupOwnerAdded](sayhey.payload.conversationgroupowneradded.md)

# Interface: ConversationGroupOwnerAdded

## Hierarchy

* **ConversationGroupOwnerAdded**

## Index

### Properties

* [conversationId](sayhey.payload.conversationgroupowneradded.md#markdown-header-conversationid)
* [isOwner](sayhey.payload.conversationgroupowneradded.md#markdown-header-isowner)
* [userId](sayhey.payload.conversationgroupowneradded.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1114

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:1116

___

###  userId

• **userId**: *string*

Defined in types.ts:1115
