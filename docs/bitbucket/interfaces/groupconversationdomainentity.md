[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [GroupConversationDomainEntity](groupconversationdomainentity.md)

# Interface: GroupConversationDomainEntity

## Hierarchy

  ↳ [BaseConversationDomainEntity](baseconversationdomainentity.md)

  ↳ **GroupConversationDomainEntity**

## Index

### Properties

* [badge](groupconversationdomainentity.md#markdown-header-badge)
* [createdAt](groupconversationdomainentity.md#markdown-header-createdat)
* [deletedFromConversationAt](groupconversationdomainentity.md#markdown-header-deletedfromconversationat)
* [endBefore](groupconversationdomainentity.md#markdown-header-endbefore)
* [group](groupconversationdomainentity.md#markdown-header-group)
* [lastMessage](groupconversationdomainentity.md#markdown-header-lastmessage)
* [muteUntil](groupconversationdomainentity.md#markdown-header-muteuntil)
* [pinnedAt](groupconversationdomainentity.md#markdown-header-pinnedat)
* [startAfter](groupconversationdomainentity.md#markdown-header-startafter)
* [type](groupconversationdomainentity.md#markdown-header-type)
* [updatedAt](groupconversationdomainentity.md#markdown-header-updatedat)

## Properties

###  badge

• **badge**: *number*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[badge](baseconversationdomainentity.md#markdown-header-badge)*

Defined in types.ts:338

___

###  createdAt

• **createdAt**: *Date*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[createdAt](baseconversationdomainentity.md#markdown-header-createdat)*

Defined in types.ts:342

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[deletedFromConversationAt](baseconversationdomainentity.md#markdown-header-deletedfromconversationat)*

Defined in types.ts:341

___

###  endBefore

• **endBefore**: *Date | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[endBefore](baseconversationdomainentity.md#markdown-header-endbefore)*

Defined in types.ts:340

___

###  group

• **group**: *[ConversationGroupInfoDomainEntity](conversationgroupinfodomainentity.md)*

Defined in types.ts:349

___

###  lastMessage

• **lastMessage**: *[MessageDomainEntity](messagedomainentity.md) | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[lastMessage](baseconversationdomainentity.md#markdown-header-lastmessage)*

Defined in types.ts:344

___

###  muteUntil

• **muteUntil**: *Date | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[muteUntil](baseconversationdomainentity.md#markdown-header-muteuntil)*

Defined in types.ts:336

___

###  pinnedAt

• **pinnedAt**: *Date | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[pinnedAt](baseconversationdomainentity.md#markdown-header-pinnedat)*

Defined in types.ts:337

___

###  startAfter

• **startAfter**: *Date | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[startAfter](baseconversationdomainentity.md#markdown-header-startafter)*

Defined in types.ts:339

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group)*

Defined in types.ts:348

___

###  updatedAt

• **updatedAt**: *Date*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[updatedAt](baseconversationdomainentity.md#markdown-header-updatedat)*

Defined in types.ts:343
