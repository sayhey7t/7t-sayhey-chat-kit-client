[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationHideAndClear](sayhey.event.conversationhideandclear.md)

# Interface: ConversationHideAndClear

## Hierarchy

* **ConversationHideAndClear**

## Index

### Properties

* [event](sayhey.event.conversationhideandclear.md#markdown-header-event)
* [payload](sayhey.event.conversationhideandclear.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationHideAndClear*

Defined in types.ts:1225

___

###  payload

• **payload**: *[ConversationHideAndClear](sayhey.payload.conversationhideandclear.md)*

Defined in types.ts:1226
