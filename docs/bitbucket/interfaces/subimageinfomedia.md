[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SubImageInfoMedia](subimageinfomedia.md)

# Interface: SubImageInfoMedia

## Hierarchy

* **SubImageInfoMedia**

## Index

### Properties

* [fileSize](subimageinfomedia.md#markdown-header-filesize)
* [height](subimageinfomedia.md#markdown-header-height)
* [imageSize](subimageinfomedia.md#markdown-header-imagesize)
* [max](subimageinfomedia.md#markdown-header-max)
* [width](subimageinfomedia.md#markdown-header-width)

## Properties

###  fileSize

• **fileSize**: *number*

Defined in types.ts:2236

___

###  height

• **height**: *number*

Defined in types.ts:2235

___

###  imageSize

• **imageSize**: *[ImageSizes](../enums/imagesizes.md)*

Defined in types.ts:2238

___

###  max

• **max**: *number*

Defined in types.ts:2237

___

###  width

• **width**: *number*

Defined in types.ts:2234
