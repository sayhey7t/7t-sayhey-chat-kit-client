[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserAdminDTO](useradmindto.md)

# Interface: UserAdminDTO

## Hierarchy

* **UserAdminDTO**

## Index

### Properties

* [appId](useradmindto.md#markdown-header-appid)
* [deletedAt](useradmindto.md#markdown-header-deletedat)
* [disabled](useradmindto.md#markdown-header-disabled)
* [email](useradmindto.md#markdown-header-email)
* [firstName](useradmindto.md#markdown-header-firstname)
* [id](useradmindto.md#markdown-header-id)
* [isActive](useradmindto.md#markdown-header-isactive)
* [lastName](useradmindto.md#markdown-header-lastname)
* [onLeave](useradmindto.md#markdown-header-onleave)
* [role](useradmindto.md#markdown-header-role)
* [roleTitle](useradmindto.md#markdown-header-roletitle)
* [signUpComplete](useradmindto.md#markdown-header-signupcomplete)
* [verified](useradmindto.md#markdown-header-verified)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:1918

___

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:1922

___

###  disabled

• **disabled**: *boolean*

Defined in types.ts:1919

___

###  email

• **email**: *string*

Defined in types.ts:1916

___

###  firstName

• **firstName**: *string*

Defined in types.ts:1914

___

###  id

• **id**: *string*

Defined in types.ts:1913

___

###  isActive

• **isActive**: *boolean*

Defined in types.ts:1921

___

###  lastName

• **lastName**: *string*

Defined in types.ts:1915

___

###  onLeave

• **onLeave**: *boolean*

Defined in types.ts:1924

___

###  role

• **role**: *[HrAdmin](../enums/usertypes.md#markdown-header-hradmin) | [SuperHrAdmin](../enums/usertypes.md#markdown-header-superhradmin)*

Defined in types.ts:1917

___

###  roleTitle

• **roleTitle**: *string*

Defined in types.ts:1925

___

###  signUpComplete

• **signUpComplete**: *boolean*

Defined in types.ts:1923

___

###  verified

• **verified**: *boolean*

Defined in types.ts:1920
