[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationGroupOwnerAdded](publisher.event.conversationgroupowneradded.md)

# Interface: ConversationGroupOwnerAdded

## Hierarchy

* EventMessage

  ↳ **ConversationGroupOwnerAdded**

## Index

### Properties

* [event](publisher.event.conversationgroupowneradded.md#markdown-header-event)
* [payload](publisher.event.conversationgroupowneradded.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationGroupOwnerAdded](../enums/publisher.eventtype.md#markdown-header-conversationgroupowneradded)*

*Overrides void*

Defined in types.ts:940

___

###  payload

• **payload**: *[ConversationGroupOwnerAdded](publisher.payload.conversationgroupowneradded.md)*

*Overrides void*

Defined in types.ts:941
