[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [KeywordDTO](keyworddto.md)

# Interface: KeywordDTO

## Hierarchy

* **KeywordDTO**

## Index

### Properties

* [appId](keyworddto.md#markdown-header-appid)
* [deletedAt](keyworddto.md#markdown-header-deletedat)
* [id](keyworddto.md#markdown-header-id)
* [priority](keyworddto.md#markdown-header-priority)
* [term](keyworddto.md#markdown-header-term)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:2008

___

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:2009

___

###  id

• **id**: *string*

Defined in types.ts:2005

___

###  priority

• **priority**: *[KeyWordPriorityTypes](../enums/keywordprioritytypes.md)*

Defined in types.ts:2007

___

###  term

• **term**: *string*

Defined in types.ts:2006
