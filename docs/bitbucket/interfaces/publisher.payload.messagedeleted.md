[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [MessageDeleted](publisher.payload.messagedeleted.md)

# Interface: MessageDeleted

## Hierarchy

* **MessageDeleted**

## Index

### Properties

* [conversationId](publisher.payload.messagedeleted.md#markdown-header-conversationid)
* [deletedAt](publisher.payload.messagedeleted.md#markdown-header-deletedat)
* [messageId](publisher.payload.messagedeleted.md#markdown-header-messageid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:854

___

###  deletedAt

• **deletedAt**: *Date*

Defined in types.ts:856

___

###  messageId

• **messageId**: *string*

Defined in types.ts:855
