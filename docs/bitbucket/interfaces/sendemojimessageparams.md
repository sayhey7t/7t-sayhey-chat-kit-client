[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SendEmojiMessageParams](sendemojimessageparams.md)

# Interface: SendEmojiMessageParams

## Hierarchy

* [SendMessageParamsBase](sendmessageparamsbase.md)

  ↳ **SendEmojiMessageParams**

## Index

### Properties

* [clientRefId](sendemojimessageparams.md#markdown-header-clientrefid)
* [conversationId](sendemojimessageparams.md#markdown-header-conversationid)
* [text](sendemojimessageparams.md#markdown-header-text)
* [type](sendemojimessageparams.md#markdown-header-type)

## Properties

###  clientRefId

• **clientRefId**: *string*

*Inherited from [SendMessageParamsBase](sendmessageparamsbase.md).[clientRefId](sendmessageparamsbase.md#markdown-header-clientrefid)*

Defined in types.ts:1689

___

###  conversationId

• **conversationId**: *string*

*Inherited from [SendMessageParamsBase](sendmessageparamsbase.md).[conversationId](sendmessageparamsbase.md#markdown-header-conversationid)*

Defined in types.ts:1688

___

###  text

• **text**: *string*

Defined in types.ts:1699

___

###  type

• **type**: *[Emoji](../enums/messagetype.md#markdown-header-emoji)*

Defined in types.ts:1698
