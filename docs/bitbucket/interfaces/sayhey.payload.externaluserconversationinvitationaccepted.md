[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ExternalUserConversationInvitationAccepted](sayhey.payload.externaluserconversationinvitationaccepted.md)

# Interface: ExternalUserConversationInvitationAccepted

## Hierarchy

* **ExternalUserConversationInvitationAccepted**

## Index

### Properties

* [acceptedAt](sayhey.payload.externaluserconversationinvitationaccepted.md#markdown-header-acceptedat)
* [conversationId](sayhey.payload.externaluserconversationinvitationaccepted.md#markdown-header-conversationid)
* [messageDomain](sayhey.payload.externaluserconversationinvitationaccepted.md#markdown-header-messagedomain)
* [messageDto](sayhey.payload.externaluserconversationinvitationaccepted.md#markdown-header-messagedto)
* [userFirstName](sayhey.payload.externaluserconversationinvitationaccepted.md#markdown-header-userfirstname)
* [userId](sayhey.payload.externaluserconversationinvitationaccepted.md#markdown-header-userid)
* [userLastName](sayhey.payload.externaluserconversationinvitationaccepted.md#markdown-header-userlastname)

## Properties

###  acceptedAt

• **acceptedAt**: *Date*

Defined in types.ts:1089

___

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1088

___

###  messageDomain

• **messageDomain**: *[MessageDomainEntity](messagedomainentity.md)*

Defined in types.ts:1093

___

###  messageDto

• **messageDto**: *[MessageDTO](messagedto.md)*

Defined in types.ts:1094

___

###  userFirstName

• **userFirstName**: *string*

Defined in types.ts:1091

___

###  userId

• **userId**: *string*

Defined in types.ts:1090

___

###  userLastName

• **userLastName**: *string*

Defined in types.ts:1092
