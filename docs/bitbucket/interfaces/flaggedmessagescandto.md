[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageScanDTO](flaggedmessagescandto.md)

# Interface: FlaggedMessageScanDTO

## Hierarchy

* [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md)

  ↳ **FlaggedMessageScanDTO**

## Index

### Properties

* [createdAt](flaggedmessagescandto.md#markdown-header-createdat)
* [flaggedMessageId](flaggedmessagescandto.md#markdown-header-flaggedmessageid)
* [flaggedResolutionId](flaggedmessagescandto.md#markdown-header-flaggedresolutionid)
* [id](flaggedmessagescandto.md#markdown-header-id)
* [keywords](flaggedmessagescandto.md#markdown-header-keywords)
* [priorityType](flaggedmessagescandto.md#markdown-header-prioritytype)
* [resolved](flaggedmessagescandto.md#markdown-header-resolved)
* [scannedAt](flaggedmessagescandto.md#markdown-header-scannedat)
* [totalMetrics](flaggedmessagescandto.md#markdown-header-totalmetrics)
* [updatedAt](flaggedmessagescandto.md#markdown-header-updatedat)

## Properties

###  createdAt

• **createdAt**: *Date*

*Inherited from [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md).[createdAt](flaggedmessagescanminimaldomain.md#markdown-header-createdat)*

Defined in types.ts:2256

___

###  flaggedMessageId

• **flaggedMessageId**: *string*

*Inherited from [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md).[flaggedMessageId](flaggedmessagescanminimaldomain.md#markdown-header-flaggedmessageid)*

Defined in types.ts:2252

___

###  flaggedResolutionId

• **flaggedResolutionId**: *string | null*

*Inherited from [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md).[flaggedResolutionId](flaggedmessagescanminimaldomain.md#markdown-header-flaggedresolutionid)*

Defined in types.ts:2255

___

###  id

• **id**: *string*

*Inherited from [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md).[id](flaggedmessagescanminimaldomain.md#markdown-header-id)*

Defined in types.ts:2258

___

###  keywords

• **keywords**: *object*

Defined in types.ts:2264

#### Type declaration:

* **high**: *object[]*

* **low**: *object[]*

___

###  priorityType

• **priorityType**: *[HIGH](../enums/flaggedmessageprioritytype.md#markdown-header-high) | [LOW](../enums/flaggedmessageprioritytype.md#markdown-header-low)*

*Inherited from [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md).[priorityType](flaggedmessagescanminimaldomain.md#markdown-header-prioritytype)*

Defined in types.ts:2259

___

###  resolved

• **resolved**: *boolean*

*Inherited from [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md).[resolved](flaggedmessagescanminimaldomain.md#markdown-header-resolved)*

Defined in types.ts:2253

___

###  scannedAt

• **scannedAt**: *Date | null*

*Inherited from [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md).[scannedAt](flaggedmessagescanminimaldomain.md#markdown-header-scannedat)*

Defined in types.ts:2257

___

###  totalMetrics

• **totalMetrics**: *number*

*Inherited from [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md).[totalMetrics](flaggedmessagescanminimaldomain.md#markdown-header-totalmetrics)*

Defined in types.ts:2260

___

###  updatedAt

• **updatedAt**: *Date*

*Inherited from [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md).[updatedAt](flaggedmessagescanminimaldomain.md#markdown-header-updatedat)*

Defined in types.ts:2254
