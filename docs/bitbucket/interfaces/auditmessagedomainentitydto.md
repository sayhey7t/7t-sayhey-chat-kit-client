[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [AuditMessageDomainEntityDTO](auditmessagedomainentitydto.md)

# Interface: AuditMessageDomainEntityDTO

## Hierarchy

* object

  ↳ **AuditMessageDomainEntityDTO**

## Index

### Properties

* [flagged](auditmessagedomainentitydto.md#markdown-header-flagged)
* [resolved](auditmessagedomainentitydto.md#markdown-header-optional-resolved)

## Properties

###  flagged

• **flagged**: *boolean*

Defined in types.ts:2203

___

### `Optional` resolved

• **resolved**? : *undefined | false | true*

Defined in types.ts:2204
