[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [AuthDTO](authdto.md)

# Interface: AuthDTO

Auth Types

## Hierarchy

* **AuthDTO**

## Index

### Properties

* [accessToken](authdto.md#markdown-header-accesstoken)
* [publisherToken](authdto.md#markdown-header-publishertoken)
* [refreshToken](authdto.md#markdown-header-refreshtoken)

## Properties

###  accessToken

• **accessToken**: *string*

Defined in types.ts:366

___

###  publisherToken

• **publisherToken**: *string*

Defined in types.ts:367

___

###  refreshToken

• **refreshToken**: *string*

Defined in types.ts:368
