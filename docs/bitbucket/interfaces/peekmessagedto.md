[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [PeekMessageDTO](peekmessagedto.md)

# Interface: PeekMessageDTO

## Hierarchy

  ↳ [MessageDTO](messagedto.md)

  ↳ **PeekMessageDTO**

## Index

### Properties

* [conversationId](peekmessagedto.md#markdown-header-conversationid)
* [createdAt](peekmessagedto.md#markdown-header-createdat)
* [deletedAt](peekmessagedto.md#markdown-header-deletedat)
* [file](peekmessagedto.md#markdown-header-file)
* [flaggedInfo](peekmessagedto.md#markdown-header-flaggedinfo)
* [id](peekmessagedto.md#markdown-header-id)
* [reactions](peekmessagedto.md#markdown-header-reactions)
* [refUrl](peekmessagedto.md#markdown-header-refurl)
* [sender](peekmessagedto.md#markdown-header-sender)
* [sequence](peekmessagedto.md#markdown-header-sequence)
* [systemMessage](peekmessagedto.md#markdown-header-systemmessage)
* [text](peekmessagedto.md#markdown-header-text)
* [type](peekmessagedto.md#markdown-header-type)

## Properties

###  conversationId

• **conversationId**: *string*

*Inherited from [BaseMessage](basemessage.md).[conversationId](basemessage.md#markdown-header-conversationid)*

Defined in types.ts:167

___

###  createdAt

• **createdAt**: *string*

*Inherited from [MessageDTO](messagedto.md).[createdAt](messagedto.md#markdown-header-createdat)*

Defined in types.ts:176

___

###  deletedAt

• **deletedAt**: *string | null*

*Inherited from [MessageDTO](messagedto.md).[deletedAt](messagedto.md#markdown-header-deletedat)*

Defined in types.ts:180

___

###  file

• **file**: *[MediaDTO](mediadto.md) | null*

*Inherited from [MessageDTO](messagedto.md).[file](messagedto.md#markdown-header-file)*

Defined in types.ts:178

___

###  flaggedInfo

• **flaggedInfo**: *[FlaggedInfoEntity](flaggedinfoentity.md) | null*

Defined in types.ts:200

___

###  id

• **id**: *string*

*Inherited from [BaseMessage](basemessage.md).[id](basemessage.md#markdown-header-id)*

Defined in types.ts:166

___

###  reactions

• **reactions**: *[ReactionsObject](../classes/reactionsobject.md)*

*Inherited from [BaseMessage](basemessage.md).[reactions](basemessage.md#markdown-header-reactions)*

Defined in types.ts:172

___

###  refUrl

• **refUrl**: *string | null*

*Inherited from [BaseMessage](basemessage.md).[refUrl](basemessage.md#markdown-header-refurl)*

Defined in types.ts:170

___

###  sender

• **sender**: *[UserDTO](../globals.md#markdown-header-userdto) | null*

*Inherited from [MessageDTO](messagedto.md).[sender](messagedto.md#markdown-header-sender)*

Defined in types.ts:177

___

###  sequence

• **sequence**: *number*

*Inherited from [BaseMessage](basemessage.md).[sequence](basemessage.md#markdown-header-sequence)*

Defined in types.ts:171

___

###  systemMessage

• **systemMessage**: *[SystemMessageDTO](../globals.md#markdown-header-systemmessagedto) | null*

*Inherited from [MessageDTO](messagedto.md).[systemMessage](messagedto.md#markdown-header-systemmessage)*

Defined in types.ts:179

___

###  text

• **text**: *string | null*

*Inherited from [BaseMessage](basemessage.md).[text](basemessage.md#markdown-header-text)*

Defined in types.ts:169

___

###  type

• **type**: *[MessageType](../enums/messagetype.md)*

*Inherited from [BaseMessage](basemessage.md).[type](basemessage.md#markdown-header-type)*

Defined in types.ts:168
