[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageResolutionUserReportBatchDTO](flaggedmessageresolutionuserreportbatchdto.md)

# Interface: FlaggedMessageResolutionUserReportBatchDTO

## Hierarchy

* [PaginateResponse](paginateresponse.md)

  ↳ **FlaggedMessageResolutionUserReportBatchDTO**

## Index

### Properties

* [completed](flaggedmessageresolutionuserreportbatchdto.md#markdown-header-completed)
* [limit](flaggedmessageresolutionuserreportbatchdto.md#markdown-header-limit)
* [nextOffset](flaggedmessageresolutionuserreportbatchdto.md#markdown-header-nextoffset)
* [offset](flaggedmessageresolutionuserreportbatchdto.md#markdown-header-offset)
* [results](flaggedmessageresolutionuserreportbatchdto.md#markdown-header-results)
* [totalCount](flaggedmessageresolutionuserreportbatchdto.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[completed](flaggedmessageuserreportbatchdto.md#markdown-header-completed)*

Defined in types.ts:2150

___

###  limit

• **limit**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[limit](flaggedmessageuserreportbatchdto.md#markdown-header-limit)*

Defined in types.ts:2149

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[nextOffset](flaggedmessageuserreportbatchdto.md#markdown-header-nextoffset)*

Defined in types.ts:2148

___

###  offset

• **offset**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[offset](flaggedmessageuserreportbatchdto.md#markdown-header-offset)*

Defined in types.ts:2147

___

###  results

• **results**: *[FlaggedMessageUserReportDTO](flaggedmessageuserreportdto.md)[]*

Defined in types.ts:2304

___

###  totalCount

• **totalCount**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[totalCount](flaggedmessageuserreportbatchdto.md#markdown-header-totalcount)*

Defined in types.ts:2146
