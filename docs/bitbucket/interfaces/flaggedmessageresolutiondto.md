[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageResolutionDTO](flaggedmessageresolutiondto.md)

# Interface: FlaggedMessageResolutionDTO

## Hierarchy

* **FlaggedMessageResolutionDTO**

## Index

### Properties

* [createdAt](flaggedmessageresolutiondto.md#markdown-header-createdat)
* [flaggedMessageId](flaggedmessageresolutiondto.md#markdown-header-flaggedmessageid)
* [id](flaggedmessageresolutiondto.md#markdown-header-id)
* [messageDeleted](flaggedmessageresolutiondto.md#markdown-header-messagedeleted)
* [resolutionNotes](flaggedmessageresolutiondto.md#markdown-header-resolutionnotes)
* [resolutionStatus](flaggedmessageresolutiondto.md#markdown-header-resolutionstatus)
* [resolved](flaggedmessageresolutiondto.md#markdown-header-resolved)
* [resolvedAt](flaggedmessageresolutiondto.md#markdown-header-resolvedat)
* [resolvedBy](flaggedmessageresolutiondto.md#markdown-header-resolvedby)
* [updatedAt](flaggedmessageresolutiondto.md#markdown-header-updatedat)

## Properties

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:2281

___

###  flaggedMessageId

• **flaggedMessageId**: *string*

Defined in types.ts:2284

___

###  id

• **id**: *string*

Defined in types.ts:2283

___

###  messageDeleted

• **messageDeleted**: *boolean*

Defined in types.ts:2287

___

###  resolutionNotes

• **resolutionNotes**: *string | null*

Defined in types.ts:2290

___

###  resolutionStatus

• **resolutionStatus**: *[ResolutionStatusType](../enums/resolutionstatustype.md)*

Defined in types.ts:2288

___

###  resolved

• **resolved**: *boolean*

Defined in types.ts:2285

___

###  resolvedAt

• **resolvedAt**: *Date | null*

Defined in types.ts:2286

___

###  resolvedBy

• **resolvedBy**: *[UserAdminDTO](useradmindto.md)*

Defined in types.ts:2289

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:2282
