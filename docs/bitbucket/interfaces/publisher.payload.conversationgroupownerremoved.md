[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationGroupOwnerRemoved](publisher.payload.conversationgroupownerremoved.md)

# Interface: ConversationGroupOwnerRemoved

## Hierarchy

* **ConversationGroupOwnerRemoved**

## Index

### Properties

* [conversationId](publisher.payload.conversationgroupownerremoved.md#markdown-header-conversationid)
* [isOwner](publisher.payload.conversationgroupownerremoved.md#markdown-header-isowner)
* [userId](publisher.payload.conversationgroupownerremoved.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:798

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:800

___

###  userId

• **userId**: *string*

Defined in types.ts:799
