[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SmsConversationInvitationDTO](smsconversationinvitationdto.md)

# Interface: SmsConversationInvitationDTO

## Hierarchy

* **SmsConversationInvitationDTO**

## Index

### Properties

* [acceptedAt](smsconversationinvitationdto.md#markdown-header-acceptedat)
* [conversationGroupInfoId](smsconversationinvitationdto.md#markdown-header-conversationgroupinfoid)
* [createdAt](smsconversationinvitationdto.md#markdown-header-createdat)
* [expiresAt](smsconversationinvitationdto.md#markdown-header-expiresat)
* [id](smsconversationinvitationdto.md#markdown-header-id)
* [receiverId](smsconversationinvitationdto.md#markdown-header-receiverid)
* [resentAt](smsconversationinvitationdto.md#markdown-header-resentat)
* [senderId](smsconversationinvitationdto.md#markdown-header-senderid)

## Properties

###  acceptedAt

• **acceptedAt**: *Date | null*

Defined in types.ts:2468

___

###  conversationGroupInfoId

• **conversationGroupInfoId**: *string*

Defined in types.ts:2464

___

###  createdAt

• **createdAt**: *Date | null*

Defined in types.ts:2470

___

###  expiresAt

• **expiresAt**: *Date*

Defined in types.ts:2467

___

###  id

• **id**: *string*

Defined in types.ts:2463

___

###  receiverId

• **receiverId**: *string*

Defined in types.ts:2466

___

###  resentAt

• **resentAt**: *Date | null*

Defined in types.ts:2469

___

###  senderId

• **senderId**: *string*

Defined in types.ts:2465
