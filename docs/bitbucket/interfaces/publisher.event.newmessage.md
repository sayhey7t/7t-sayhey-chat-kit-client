[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [NewMessage](publisher.event.newmessage.md)

# Interface: NewMessage

## Hierarchy

* EventMessage

  ↳ **NewMessage**

## Index

### Properties

* [event](publisher.event.newmessage.md#markdown-header-event)
* [payload](publisher.event.newmessage.md#markdown-header-payload)

## Properties

###  event

• **event**: *[NewMessage](../enums/publisher.eventtype.md#markdown-header-newmessage)*

*Overrides void*

Defined in types.ts:880

___

###  payload

• **payload**: *[NewMessage](publisher.payload.newmessage.md)*

*Overrides void*

Defined in types.ts:881
