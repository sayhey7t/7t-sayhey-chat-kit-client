[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [UserDisabled](sayhey.event.userdisabled.md)

# Interface: UserDisabled

## Hierarchy

* EventMessage

  ↳ **UserDisabled**

## Index

### Properties

* [event](sayhey.event.userdisabled.md#markdown-header-event)
* [payload](sayhey.event.userdisabled.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.UserDisabled*

*Overrides void*

Defined in types.ts:1285

___

###  payload

• **payload**: *[UserDisabled](sayhey.payload.userdisabled.md)*

*Overrides void*

Defined in types.ts:1286
