[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [InternalUserContactDTO](internalusercontactdto.md)

# Interface: InternalUserContactDTO

## Hierarchy

* **InternalUserContactDTO**

## Index

### Properties

* [externalUser](internalusercontactdto.md#markdown-header-externaluser)
* [externalUserId](internalusercontactdto.md#markdown-header-externaluserid)
* [internalUserId](internalusercontactdto.md#markdown-header-internaluserid)

## Properties

###  externalUser

• **externalUser**: *[UserBasicInfoDTOExternal](userbasicinfodtoexternal.md)*

Defined in types.ts:2427

___

###  externalUserId

• **externalUserId**: *string*

Defined in types.ts:2426

___

###  internalUserId

• **internalUserId**: *string*

Defined in types.ts:2425
