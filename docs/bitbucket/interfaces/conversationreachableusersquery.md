[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationReachableUsersQuery](conversationreachableusersquery.md)

# Interface: ConversationReachableUsersQuery

## Hierarchy

* **ConversationReachableUsersQuery**

  ↳ [ConversationUserQuery](conversationuserquery.md)

## Index

### Properties

* [conversationId](conversationreachableusersquery.md#markdown-header-conversationid)
* [includeRefIdInSearch](conversationreachableusersquery.md#markdown-header-optional-includerefidinsearch)
* [limit](conversationreachableusersquery.md#markdown-header-optional-limit)
* [nextCursor](conversationreachableusersquery.md#markdown-header-optional-nextcursor)
* [searchString](conversationreachableusersquery.md#markdown-header-optional-searchstring)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:601

___

### `Optional` includeRefIdInSearch

• **includeRefIdInSearch**? : *undefined | false | true*

Defined in types.ts:605

___

### `Optional` limit

• **limit**? : *undefined | number*

Defined in types.ts:603

___

### `Optional` nextCursor

• **nextCursor**? : *undefined | number*

Defined in types.ts:604

___

### `Optional` searchString

• **searchString**? : *undefined | string*

Defined in types.ts:602
