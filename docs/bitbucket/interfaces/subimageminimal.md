[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SubImageMinimal](subimageminimal.md)

# Interface: SubImageMinimal

## Hierarchy

* **SubImageMinimal**

## Index

### Properties

* [height](subimageminimal.md#markdown-header-height)
* [imageSize](subimageminimal.md#markdown-header-imagesize)
* [postfix](subimageminimal.md#markdown-header-postfix)
* [width](subimageminimal.md#markdown-header-width)

## Properties

###  height

• **height**: *number*

Defined in types.ts:69

___

###  imageSize

• **imageSize**: *[ImageSizes](../enums/imagesizes.md)*

Defined in types.ts:67

___

###  postfix

• **postfix**: *string*

Defined in types.ts:70

___

###  width

• **width**: *number*

Defined in types.ts:68
