[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SentMessageCountDailyDTO](sentmessagecountdailydto.md)

# Interface: SentMessageCountDailyDTO

## Hierarchy

* **SentMessageCountDailyDTO**

## Index

### Properties

* [appId](sentmessagecountdailydto.md#markdown-header-appid)
* [createdAt](sentmessagecountdailydto.md#markdown-header-createdat)
* [dateStringWithHourOnly](sentmessagecountdailydto.md#markdown-header-datestringwithhouronly)
* [messageCount](sentmessagecountdailydto.md#markdown-header-messagecount)
* [updatedAt](sentmessagecountdailydto.md#markdown-header-updatedat)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:1966

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:1968

___

###  dateStringWithHourOnly

• **dateStringWithHourOnly**: *string*

Defined in types.ts:1970

___

###  messageCount

• **messageCount**: *number*

Defined in types.ts:1967

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:1969
