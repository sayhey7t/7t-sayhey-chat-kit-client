[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserRole](userrole.md)

# Interface: UserRole

## Hierarchy

* **UserRole**

## Index

### Properties

* [title](userrole.md#markdown-header-title)
* [type](userrole.md#markdown-header-type)

## Properties

###  title

• **title**: *string*

Defined in types.ts:2355

___

###  type

• **type**: *[UserTypes](../enums/usertypes.md)*

Defined in types.ts:2354
