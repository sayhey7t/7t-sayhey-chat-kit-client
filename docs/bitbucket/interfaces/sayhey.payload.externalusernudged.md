[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ExternalUserNudged](sayhey.payload.externalusernudged.md)

# Interface: ExternalUserNudged

## Hierarchy

* **ExternalUserNudged**

## Index

### Properties

* [conversationId](sayhey.payload.externalusernudged.md#markdown-header-conversationid)
* [messageDomain](sayhey.payload.externalusernudged.md#markdown-header-messagedomain)
* [messageDto](sayhey.payload.externalusernudged.md#markdown-header-messagedto)
* [updateData](sayhey.payload.externalusernudged.md#markdown-header-updatedata)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1181

___

###  messageDomain

• **messageDomain**: *[MessageDomainEntity](messagedomainentity.md)*

Defined in types.ts:1182

___

###  messageDto

• **messageDto**: *[MessageDTO](messagedto.md)*

Defined in types.ts:1183

___

###  updateData

• **updateData**: *object*

Defined in types.ts:1184

#### Type declaration:

* **lastUnreadNudgedAt**: *Date*
