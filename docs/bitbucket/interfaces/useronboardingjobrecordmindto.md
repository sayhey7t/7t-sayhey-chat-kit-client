[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserOnboardingJobRecordMinDTO](useronboardingjobrecordmindto.md)

# Interface: UserOnboardingJobRecordMinDTO

## Hierarchy

* **UserOnboardingJobRecordMinDTO**

## Index

### Properties

* [email](useronboardingjobrecordmindto.md#markdown-header-email)
* [employeeId](useronboardingjobrecordmindto.md#markdown-header-employeeid)
* [error](useronboardingjobrecordmindto.md#markdown-header-error)
* [firstName](useronboardingjobrecordmindto.md#markdown-header-firstname)
* [id](useronboardingjobrecordmindto.md#markdown-header-id)
* [lastName](useronboardingjobrecordmindto.md#markdown-header-lastname)
* [status](useronboardingjobrecordmindto.md#markdown-header-status)
* [title](useronboardingjobrecordmindto.md#markdown-header-title)
* [userOnboardingJobId](useronboardingjobrecordmindto.md#markdown-header-useronboardingjobid)

## Properties

###  email

• **email**: *string | null*

Defined in types.ts:2397

___

###  employeeId

• **employeeId**: *string | null*

Defined in types.ts:2399

___

###  error

• **error**: *string | null*

Defined in types.ts:2401

___

###  firstName

• **firstName**: *string | null*

Defined in types.ts:2395

___

###  id

• **id**: *string*

Defined in types.ts:2393

___

###  lastName

• **lastName**: *string | null*

Defined in types.ts:2396

___

###  status

• **status**: *[UserOnboardingJobRecordStatus](../enums/useronboardingjobrecordstatus.md)*

Defined in types.ts:2400

___

###  title

• **title**: *string | null*

Defined in types.ts:2398

___

###  userOnboardingJobId

• **userOnboardingJobId**: *string*

Defined in types.ts:2394
