[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationGroupMemberAdded](sayhey.payload.conversationgroupmemberadded.md)

# Interface: ConversationGroupMemberAdded

## Hierarchy

* **ConversationGroupMemberAdded**

## Index

### Properties

* [conversationId](sayhey.payload.conversationgroupmemberadded.md#markdown-header-conversationid)
* [deletedAt](sayhey.payload.conversationgroupmemberadded.md#markdown-header-deletedat)
* [endBefore](sayhey.payload.conversationgroupmemberadded.md#markdown-header-endbefore)
* [userIds](sayhey.payload.conversationgroupmemberadded.md#markdown-header-userids)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1127

___

###  deletedAt

• **deletedAt**: *null*

Defined in types.ts:1129

___

###  endBefore

• **endBefore**: *null*

Defined in types.ts:1130

___

###  userIds

• **userIds**: *string[]*

Defined in types.ts:1128
