[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [MessageReactionsUpdate](publisher.event.messagereactionsupdate.md)

# Interface: MessageReactionsUpdate

## Hierarchy

* EventMessage

  ↳ **MessageReactionsUpdate**

## Index

### Properties

* [event](publisher.event.messagereactionsupdate.md#markdown-header-event)
* [payload](publisher.event.messagereactionsupdate.md#markdown-header-payload)

## Properties

###  event

• **event**: *[MessageReactionsUpdate](../enums/publisher.eventtype.md#markdown-header-messagereactionsupdate)*

*Overrides void*

Defined in types.ts:970

___

###  payload

• **payload**: *[MessageReactionsUpdate](publisher.payload.messagereactionsupdate.md)*

*Overrides void*

Defined in types.ts:971
