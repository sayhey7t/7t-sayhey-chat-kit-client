[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ReactedUserDTO](reacteduserdto.md)

# Interface: ReactedUserDTO

## Hierarchy

* **ReactedUserDTO**

## Index

### Properties

* [createdAt](reacteduserdto.md#markdown-header-createdat)
* [messageId](reacteduserdto.md#markdown-header-messageid)
* [sequence](reacteduserdto.md#markdown-header-sequence)
* [type](reacteduserdto.md#markdown-header-type)
* [user](reacteduserdto.md#markdown-header-user)
* [userId](reacteduserdto.md#markdown-header-userid)

## Properties

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:1772

___

###  messageId

• **messageId**: *string*

Defined in types.ts:1773

___

###  sequence

• **sequence**: *number*

Defined in types.ts:1771

___

###  type

• **type**: *[ReactionType](../enums/reactiontype.md)*

Defined in types.ts:1770

___

###  user

• **user**: *[UserDTO](../globals.md#markdown-header-userdto)*

Defined in types.ts:1775

___

###  userId

• **userId**: *string*

Defined in types.ts:1774
