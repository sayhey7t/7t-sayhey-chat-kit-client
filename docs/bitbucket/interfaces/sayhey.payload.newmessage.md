[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [NewMessage](sayhey.payload.newmessage.md)

# Interface: NewMessage

## Hierarchy

* **NewMessage**

## Index

### Properties

* [clientRefId](sayhey.payload.newmessage.md#markdown-header-clientrefid)
* [message](sayhey.payload.newmessage.md#markdown-header-message)

## Properties

###  clientRefId

• **clientRefId**: *string | null*

Defined in types.ts:1048

___

###  message

• **message**: *[MessageDomainEntity](messagedomainentity.md)*

Defined in types.ts:1047
