[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [CreateNewContact](createnewcontact.md)

# Interface: CreateNewContact

## Hierarchy

* **CreateNewContact**

## Index

### Properties

* [externalUserInfo](createnewcontact.md#markdown-header-externaluserinfo)
* [firstName](createnewcontact.md#markdown-header-firstname)
* [lastName](createnewcontact.md#markdown-header-lastname)

## Properties

###  externalUserInfo

• **externalUserInfo**: *object*

Defined in types.ts:2417

#### Type declaration:

* **phone**: *[PhoneWithCode](phonewithcode.md)*

___

###  firstName

• **firstName**: *string*

Defined in types.ts:2415

___

###  lastName

• **lastName**: *string*

Defined in types.ts:2416
