[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationMuted](publisher.event.conversationmuted.md)

# Interface: ConversationMuted

## Hierarchy

* EventMessage

  ↳ **ConversationMuted**

## Index

### Properties

* [event](publisher.event.conversationmuted.md#markdown-header-event)
* [payload](publisher.event.conversationmuted.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationMuted](../enums/publisher.eventtype.md#markdown-header-conversationmuted)*

*Overrides void*

Defined in types.ts:900

___

###  payload

• **payload**: *[ConversationMuted](publisher.payload.conversationmuted.md)*

*Overrides void*

Defined in types.ts:901
