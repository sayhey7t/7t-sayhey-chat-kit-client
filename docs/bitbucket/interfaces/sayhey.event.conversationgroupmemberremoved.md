[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationGroupMemberRemoved](sayhey.event.conversationgroupmemberremoved.md)

# Interface: ConversationGroupMemberRemoved

## Hierarchy

* EventMessage

  ↳ **ConversationGroupMemberRemoved**

## Index

### Properties

* [event](sayhey.event.conversationgroupmemberremoved.md#markdown-header-event)
* [payload](sayhey.event.conversationgroupmemberremoved.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationGroupMemberRemoved*

*Overrides void*

Defined in types.ts:1260

___

###  payload

• **payload**: *[ConversationGroupMemberRemoved](sayhey.payload.conversationgroupmemberremoved.md)*

*Overrides void*

Defined in types.ts:1261
