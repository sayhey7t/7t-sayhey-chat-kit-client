[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationHideAndClear](publisher.payload.conversationhideandclear.md)

# Interface: ConversationHideAndClear

## Hierarchy

* **ConversationHideAndClear**

## Index

### Properties

* [conversationId](publisher.payload.conversationhideandclear.md#markdown-header-conversationid)
* [conversationUserHideClearDto](publisher.payload.conversationhideandclear.md#markdown-header-conversationuserhidecleardto)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:751

___

###  conversationUserHideClearDto

• **conversationUserHideClearDto**: *object*

Defined in types.ts:752

#### Type declaration:

* **startAfter**: *string*

* **visible**: *boolean*
