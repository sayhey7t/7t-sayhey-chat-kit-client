[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationHideAndClear](publisher.event.conversationhideandclear.md)

# Interface: ConversationHideAndClear

## Hierarchy

* EventMessage

  ↳ **ConversationHideAndClear**

## Index

### Properties

* [event](publisher.event.conversationhideandclear.md#markdown-header-event)
* [payload](publisher.event.conversationhideandclear.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationHideAndClear](../enums/publisher.eventtype.md#markdown-header-conversationhideandclear)*

*Overrides void*

Defined in types.ts:910

___

###  payload

• **payload**: *[ConversationHideAndClear](publisher.payload.conversationhideandclear.md)*

*Overrides void*

Defined in types.ts:911
