[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserSimpleInfo](usersimpleinfo.md)

# Interface: UserSimpleInfo

## Hierarchy

* **UserSimpleInfo**

## Index

### Properties

* [firstName](usersimpleinfo.md#markdown-header-firstname)
* [id](usersimpleinfo.md#markdown-header-id)
* [lastName](usersimpleinfo.md#markdown-header-lastname)
* [refId](usersimpleinfo.md#markdown-header-refid)

## Properties

###  firstName

• **firstName**: *string*

Defined in types.ts:159

___

###  id

• **id**: *string*

Defined in types.ts:161

___

###  lastName

• **lastName**: *string*

Defined in types.ts:160

___

###  refId

• **refId**: *string | null*

Defined in types.ts:162
