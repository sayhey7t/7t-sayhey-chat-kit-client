[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationGroupMemberRemoved](publisher.payload.conversationgroupmemberremoved.md)

# Interface: ConversationGroupMemberRemoved

## Hierarchy

* **ConversationGroupMemberRemoved**

## Index

### Properties

* [conversationId](publisher.payload.conversationgroupmemberremoved.md#markdown-header-conversationid)
* [deletedAt](publisher.payload.conversationgroupmemberremoved.md#markdown-header-deletedat)
* [endBefore](publisher.payload.conversationgroupmemberremoved.md#markdown-header-endbefore)
* [userIds](publisher.payload.conversationgroupmemberremoved.md#markdown-header-userids)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:817

___

###  deletedAt

• **deletedAt**: *Date*

Defined in types.ts:819

___

###  endBefore

• **endBefore**: *Date*

Defined in types.ts:820

___

###  userIds

• **userIds**: *string[]*

Defined in types.ts:818
