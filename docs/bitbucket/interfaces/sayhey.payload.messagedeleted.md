[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [MessageDeleted](sayhey.payload.messagedeleted.md)

# Interface: MessageDeleted

## Hierarchy

* **MessageDeleted**

## Index

### Properties

* [conversationId](sayhey.payload.messagedeleted.md#markdown-header-conversationid)
* [deletedAt](sayhey.payload.messagedeleted.md#markdown-header-deletedat)
* [messageId](sayhey.payload.messagedeleted.md#markdown-header-messageid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1164

___

###  deletedAt

• **deletedAt**: *Date*

Defined in types.ts:1166

___

###  messageId

• **messageId**: *string*

Defined in types.ts:1165
