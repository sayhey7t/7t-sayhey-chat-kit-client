[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [UserReactedToMessage](publisher.event.userreactedtomessage.md)

# Interface: UserReactedToMessage

## Hierarchy

* EventMessage

  ↳ **UserReactedToMessage**

## Index

### Properties

* [event](publisher.event.userreactedtomessage.md#markdown-header-event)
* [payload](publisher.event.userreactedtomessage.md#markdown-header-payload)

## Properties

###  event

• **event**: *[UserReactedToMessage](../enums/publisher.eventtype.md#markdown-header-userreactedtomessage)*

*Overrides void*

Defined in types.ts:960

___

###  payload

• **payload**: *[UserReactedToMessage](publisher.payload.userreactedtomessage.md)*

*Overrides void*

Defined in types.ts:961
