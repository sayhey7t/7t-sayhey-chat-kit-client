[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [DepartmentWithDivisionCountDTO](departmentwithdivisioncountdto.md)

# Interface: DepartmentWithDivisionCountDTO

## Hierarchy

* [DepartmentDTO](departmentdto.md)

  ↳ **DepartmentWithDivisionCountDTO**

## Index

### Properties

* [appId](departmentwithdivisioncountdto.md#markdown-header-appid)
* [deletedAt](departmentwithdivisioncountdto.md#markdown-header-deletedat)
* [divisionCount](departmentwithdivisioncountdto.md#markdown-header-divisioncount)
* [divisions](departmentwithdivisioncountdto.md#markdown-header-optional-divisions)
* [id](departmentwithdivisioncountdto.md#markdown-header-id)
* [isDefault](departmentwithdivisioncountdto.md#markdown-header-isdefault)
* [name](departmentwithdivisioncountdto.md#markdown-header-name)

## Properties

###  appId

• **appId**: *string*

*Inherited from [DepartmentDTO](departmentdto.md).[appId](departmentdto.md#markdown-header-appid)*

Defined in types.ts:1808

___

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [DepartmentDTO](departmentdto.md).[deletedAt](departmentdto.md#markdown-header-deletedat)*

Defined in types.ts:1809

___

###  divisionCount

• **divisionCount**: *number*

Defined in types.ts:1815

___

### `Optional` divisions

• **divisions**? : *[DivisionDTO](divisiondto.md)[]*

*Inherited from [DepartmentDTO](departmentdto.md).[divisions](departmentdto.md#markdown-header-optional-divisions)*

Defined in types.ts:1811

___

###  id

• **id**: *string*

*Inherited from [DepartmentDTO](departmentdto.md).[id](departmentdto.md#markdown-header-id)*

Defined in types.ts:1806

___

###  isDefault

• **isDefault**: *[tempDefaultColumn](../enums/tempdefaultcolumn.md) | null*

*Inherited from [DepartmentDTO](departmentdto.md).[isDefault](departmentdto.md#markdown-header-isdefault)*

Defined in types.ts:1810

___

###  name

• **name**: *string*

*Inherited from [DepartmentDTO](departmentdto.md).[name](departmentdto.md#markdown-header-name)*

Defined in types.ts:1807
