[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [UserReactedToMessage](publisher.payload.userreactedtomessage.md)

# Interface: UserReactedToMessage

## Hierarchy

* **UserReactedToMessage**

## Index

### Properties

* [conversationId](publisher.payload.userreactedtomessage.md#markdown-header-conversationid)
* [messageId](publisher.payload.userreactedtomessage.md#markdown-header-messageid)
* [type](publisher.payload.userreactedtomessage.md#markdown-header-type)
* [userId](publisher.payload.userreactedtomessage.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:824

___

###  messageId

• **messageId**: *string*

Defined in types.ts:825

___

###  type

• **type**: *[ReactionType](../enums/reactiontype.md)*

Defined in types.ts:826

___

###  userId

• **userId**: *string*

Defined in types.ts:827
