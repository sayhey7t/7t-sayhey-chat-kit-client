[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [NewMessage](publisher.payload.newmessage.md)

# Interface: NewMessage

## Hierarchy

* **NewMessage**

## Index

### Properties

* [clientRefId](publisher.payload.newmessage.md#markdown-header-clientrefid)
* [message](publisher.payload.newmessage.md#markdown-header-message)

## Properties

###  clientRefId

• **clientRefId**: *string | null*

Defined in types.ts:721

___

###  message

• **message**: *[MessageDTO](messagedto.md)*

Defined in types.ts:720
