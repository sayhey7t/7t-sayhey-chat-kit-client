[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserManagementCountResultParams](usermanagementcountresultparams.md)

# Interface: UserManagementCountResultParams

## Hierarchy

* **UserManagementCountResultParams**

## Index

### Properties

* [active](usermanagementcountresultparams.md#markdown-header-optional-active)
* [activeAndPerm](usermanagementcountresultparams.md#markdown-header-optional-activeandperm)
* [activeAndTemp](usermanagementcountresultparams.md#markdown-header-optional-activeandtemp)
* [inactive](usermanagementcountresultparams.md#markdown-header-optional-inactive)
* [inactiveAndPerm](usermanagementcountresultparams.md#markdown-header-optional-inactiveandperm)
* [inactiveAndTemp](usermanagementcountresultparams.md#markdown-header-optional-inactiveandtemp)
* [perm](usermanagementcountresultparams.md#markdown-header-optional-perm)
* [temp](usermanagementcountresultparams.md#markdown-header-optional-temp)

## Properties

### `Optional` active

• **active**? : *undefined | number*

Defined in types.ts:1732

___

### `Optional` activeAndPerm

• **activeAndPerm**? : *undefined | number*

Defined in types.ts:1736

___

### `Optional` activeAndTemp

• **activeAndTemp**? : *undefined | number*

Defined in types.ts:1737

___

### `Optional` inactive

• **inactive**? : *undefined | number*

Defined in types.ts:1733

___

### `Optional` inactiveAndPerm

• **inactiveAndPerm**? : *undefined | number*

Defined in types.ts:1739

___

### `Optional` inactiveAndTemp

• **inactiveAndTemp**? : *undefined | number*

Defined in types.ts:1738

___

### `Optional` perm

• **perm**? : *undefined | number*

Defined in types.ts:1735

___

### `Optional` temp

• **temp**? : *undefined | number*

Defined in types.ts:1734
