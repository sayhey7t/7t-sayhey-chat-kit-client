[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageCountDailyDTO](flaggedmessagecountdailydto.md)

# Interface: FlaggedMessageCountDailyDTO

## Hierarchy

* **FlaggedMessageCountDailyDTO**

## Index

### Properties

* [createdAt](flaggedmessagecountdailydto.md#markdown-header-createdat)
* [dateStringWithHourOnly](flaggedmessagecountdailydto.md#markdown-header-datestringwithhouronly)
* [flaggedMessageCount](flaggedmessagecountdailydto.md#markdown-header-flaggedmessagecount)
* [updatedAt](flaggedmessagecountdailydto.md#markdown-header-updatedat)

## Properties

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:1986

___

###  dateStringWithHourOnly

• **dateStringWithHourOnly**: *string*

Defined in types.ts:1988

___

###  flaggedMessageCount

• **flaggedMessageCount**: *number*

Defined in types.ts:1985

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:1987
