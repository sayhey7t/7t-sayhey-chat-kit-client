[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserCommonDTO](usercommondto.md)

# Interface: UserCommonDTO

## Hierarchy

* **UserCommonDTO**

  ↳ [UserInternalDTO](userinternaldto.md)

  ↳ [UserExternalDTO](userexternaldto.md)

## Index

### Properties

* [deletedAt](usercommondto.md#markdown-header-deletedat)
* [disabled](usercommondto.md#markdown-header-disabled)
* [firstName](usercommondto.md#markdown-header-firstname)
* [id](usercommondto.md#markdown-header-id)
* [lastName](usercommondto.md#markdown-header-lastname)
* [picture](usercommondto.md#markdown-header-picture)
* [refId](usercommondto.md#markdown-header-refid)
* [userScope](usercommondto.md#markdown-header-userscope)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:398

___

###  disabled

• **disabled**: *boolean*

Defined in types.ts:397

___

###  firstName

• **firstName**: *string*

Defined in types.ts:393

___

###  id

• **id**: *string*

Defined in types.ts:392

___

###  lastName

• **lastName**: *string*

Defined in types.ts:394

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

Defined in types.ts:396

___

###  refId

• **refId**: *string | null*

Defined in types.ts:395

___

###  userScope

• **userScope**: *[UserScope](../enums/userscope.md)*

Defined in types.ts:399
