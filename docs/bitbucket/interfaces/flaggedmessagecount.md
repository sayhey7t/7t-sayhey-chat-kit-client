[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageCount](flaggedmessagecount.md)

# Interface: FlaggedMessageCount

## Hierarchy

* **FlaggedMessageCount**

## Index

### Properties

* [high](flaggedmessagecount.md#markdown-header-high)
* [low](flaggedmessagecount.md#markdown-header-low)
* [userReport](flaggedmessagecount.md#markdown-header-userreport)

## Properties

###  high

• **high**: *[FlaggedMessageFlagTypeCount](flaggedmessageflagtypecount.md)*

Defined in types.ts:2067

___

###  low

• **low**: *[FlaggedMessageFlagTypeCount](flaggedmessageflagtypecount.md)*

Defined in types.ts:2068

___

###  userReport

• **userReport**: *[FlaggedMessageFlagTypeCount](flaggedmessageflagtypecount.md)*

Defined in types.ts:2069
