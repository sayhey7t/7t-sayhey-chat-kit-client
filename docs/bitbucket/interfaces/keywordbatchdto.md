[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [KeywordBatchDTO](keywordbatchdto.md)

# Interface: KeywordBatchDTO

## Hierarchy

* [SearchPaginateResponse](searchpaginateresponse.md)

  ↳ **KeywordBatchDTO**

## Index

### Properties

* [completed](keywordbatchdto.md#markdown-header-completed)
* [limit](keywordbatchdto.md#markdown-header-limit)
* [nextOffset](keywordbatchdto.md#markdown-header-nextoffset)
* [offset](keywordbatchdto.md#markdown-header-offset)
* [results](keywordbatchdto.md#markdown-header-results)
* [search](keywordbatchdto.md#markdown-header-search)
* [totalCount](keywordbatchdto.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[KeywordDTO](keyworddto.md)[]*

Defined in types.ts:2015

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
