[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationInfoDomainEntityInternal](conversationinfodomainentityinternal.md)

# Interface: ConversationInfoDomainEntityInternal

## Hierarchy

* **ConversationInfoDomainEntityInternal**

## Index

### Properties

* [email](conversationinfodomainentityinternal.md#markdown-header-email)
* [userScope](conversationinfodomainentityinternal.md#markdown-header-userscope)

## Properties

###  email

• **email**: *string*

Defined in types.ts:247

___

###  userScope

• **userScope**: *[Internal](../enums/userscope.md#markdown-header-internal)*

Defined in types.ts:246
