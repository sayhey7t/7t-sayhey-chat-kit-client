[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageUserReportPaginateResponse](flaggedmessageuserreportpaginateresponse.md)

# Interface: FlaggedMessageUserReportPaginateResponse

## Hierarchy

* [PaginateResponse](paginateresponse.md)

  ↳ **FlaggedMessageUserReportPaginateResponse**

  ↳ [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md)

## Index

### Properties

* [completed](flaggedmessageuserreportpaginateresponse.md#markdown-header-completed)
* [limit](flaggedmessageuserreportpaginateresponse.md#markdown-header-limit)
* [nextOffset](flaggedmessageuserreportpaginateresponse.md#markdown-header-nextoffset)
* [offset](flaggedmessageuserreportpaginateresponse.md#markdown-header-offset)
* [resolved](flaggedmessageuserreportpaginateresponse.md#markdown-header-resolved)
* [totalCount](flaggedmessageuserreportpaginateresponse.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[completed](flaggedmessageuserreportbatchdto.md#markdown-header-completed)*

Defined in types.ts:2150

___

###  limit

• **limit**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[limit](flaggedmessageuserreportbatchdto.md#markdown-header-limit)*

Defined in types.ts:2149

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[nextOffset](flaggedmessageuserreportbatchdto.md#markdown-header-nextoffset)*

Defined in types.ts:2148

___

###  offset

• **offset**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[offset](flaggedmessageuserreportbatchdto.md#markdown-header-offset)*

Defined in types.ts:2147

___

###  resolved

• **resolved**: *boolean*

Defined in types.ts:2142

___

###  totalCount

• **totalCount**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[totalCount](flaggedmessageuserreportbatchdto.md#markdown-header-totalcount)*

Defined in types.ts:2146
