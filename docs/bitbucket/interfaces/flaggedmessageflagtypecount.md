[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageFlagTypeCount](flaggedmessageflagtypecount.md)

# Interface: FlaggedMessageFlagTypeCount

## Hierarchy

* **FlaggedMessageFlagTypeCount**

## Index

### Properties

* [resolvedCount](flaggedmessageflagtypecount.md#markdown-header-resolvedcount)
* [unresolvedCount](flaggedmessageflagtypecount.md#markdown-header-unresolvedcount)

## Properties

###  resolvedCount

• **resolvedCount**: *number*

Defined in types.ts:2064

___

###  unresolvedCount

• **unresolvedCount**: *number*

Defined in types.ts:2063
