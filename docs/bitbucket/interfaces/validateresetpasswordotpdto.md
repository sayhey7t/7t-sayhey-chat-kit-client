[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ValidateResetPasswordOTPDTO](validateresetpasswordotpdto.md)

# Interface: ValidateResetPasswordOTPDTO

## Hierarchy

* **ValidateResetPasswordOTPDTO**

## Index

### Properties

* [resetPasswordToken](validateresetpasswordotpdto.md#markdown-header-resetpasswordtoken)

## Properties

###  resetPasswordToken

• **resetPasswordToken**: *string*

Defined in types.ts:1946
