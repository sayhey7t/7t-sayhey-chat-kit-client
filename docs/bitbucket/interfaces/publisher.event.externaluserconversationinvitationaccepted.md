[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ExternalUserConversationInvitationAccepted](publisher.event.externaluserconversationinvitationaccepted.md)

# Interface: ExternalUserConversationInvitationAccepted

## Hierarchy

* EventMessage

  ↳ **ExternalUserConversationInvitationAccepted**

## Index

### Properties

* [event](publisher.event.externaluserconversationinvitationaccepted.md#markdown-header-event)
* [payload](publisher.event.externaluserconversationinvitationaccepted.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ExternalUserConversationInvitationAccepted](../enums/publisher.eventtype.md#markdown-header-externaluserconversationinvitationaccepted)*

*Overrides void*

Defined in types.ts:930

___

###  payload

• **payload**: *[ExternalUserConversationInvitationAccepted](publisher.payload.externaluserconversationinvitationaccepted.md)*

*Overrides void*

Defined in types.ts:931
