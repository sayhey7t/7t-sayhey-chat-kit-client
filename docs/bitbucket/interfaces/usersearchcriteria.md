[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserSearchCriteria](usersearchcriteria.md)

# Interface: UserSearchCriteria

## Hierarchy

* **UserSearchCriteria**

## Index

### Properties

* [endDate](usersearchcriteria.md#markdown-header-optional-enddate)
* [hasAttachment](usersearchcriteria.md#markdown-header-optional-hasattachment)
* [hasFlagged](usersearchcriteria.md#markdown-header-optional-hasflagged)
* [startDate](usersearchcriteria.md#markdown-header-optional-startdate)
* [text](usersearchcriteria.md#markdown-header-optional-text)
* [textSearchCondition](usersearchcriteria.md#markdown-header-optional-textsearchcondition)
* [userId](usersearchcriteria.md#markdown-header-optional-userid)

## Properties

### `Optional` endDate

• **endDate**? : *undefined | string*

Defined in types.ts:2051

___

### `Optional` hasAttachment

• **hasAttachment**? : *undefined | false | true*

Defined in types.ts:2049

___

### `Optional` hasFlagged

• **hasFlagged**? : *undefined | false | true*

Defined in types.ts:2053

___

### `Optional` startDate

• **startDate**? : *undefined | string*

Defined in types.ts:2050

___

### `Optional` text

• **text**? : *string[]*

Defined in types.ts:2048

___

### `Optional` textSearchCondition

• **textSearchCondition**? : *[QueryCondition](../enums/querycondition.md)*

Defined in types.ts:2052

___

### `Optional` userId

• **userId**? : *undefined | string*

Defined in types.ts:2047
