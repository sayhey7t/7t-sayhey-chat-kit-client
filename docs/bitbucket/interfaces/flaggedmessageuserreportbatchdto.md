[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md)

# Interface: FlaggedMessageUserReportBatchDTO

## Hierarchy

  ↳ [FlaggedMessageUserReportPaginateResponse](flaggedmessageuserreportpaginateresponse.md)

  ↳ **FlaggedMessageUserReportBatchDTO**

## Index

### Properties

* [completed](flaggedmessageuserreportbatchdto.md#markdown-header-completed)
* [limit](flaggedmessageuserreportbatchdto.md#markdown-header-limit)
* [nextOffset](flaggedmessageuserreportbatchdto.md#markdown-header-nextoffset)
* [offset](flaggedmessageuserreportbatchdto.md#markdown-header-offset)
* [resolved](flaggedmessageuserreportbatchdto.md#markdown-header-resolved)
* [results](flaggedmessageuserreportbatchdto.md#markdown-header-results)
* [totalCount](flaggedmessageuserreportbatchdto.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[completed](flaggedmessageuserreportbatchdto.md#markdown-header-completed)*

Defined in types.ts:2150

___

###  limit

• **limit**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[limit](flaggedmessageuserreportbatchdto.md#markdown-header-limit)*

Defined in types.ts:2149

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[nextOffset](flaggedmessageuserreportbatchdto.md#markdown-header-nextoffset)*

Defined in types.ts:2148

___

###  offset

• **offset**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[offset](flaggedmessageuserreportbatchdto.md#markdown-header-offset)*

Defined in types.ts:2147

___

###  resolved

• **resolved**: *boolean*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[resolved](flaggedmessageuserreportbatchdto.md#markdown-header-resolved)*

Defined in types.ts:2142

___

###  results

• **results**: *[FlaggedMessageUserReportDTO](flaggedmessageuserreportdto.md)[]*

Defined in types.ts:2138

___

###  totalCount

• **totalCount**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[totalCount](flaggedmessageuserreportbatchdto.md#markdown-header-totalcount)*

Defined in types.ts:2146
