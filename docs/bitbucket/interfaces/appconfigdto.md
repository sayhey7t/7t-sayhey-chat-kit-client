[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [AppConfigDTO](appconfigdto.md)

# Interface: AppConfigDTO

## Hierarchy

* **AppConfigDTO**

## Index

### Properties

* [appId](appconfigdto.md#markdown-header-appid)
* [nudgeDelayHours](appconfigdto.md#markdown-header-nudgedelayhours)
* [privacyPolicyUrl](appconfigdto.md#markdown-header-privacypolicyurl)
* [quickGroupUserSelectionLimit](appconfigdto.md#markdown-header-quickgroupuserselectionlimit)
* [termsAndConditionsUrl](appconfigdto.md#markdown-header-termsandconditionsurl)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:1950

___

###  nudgeDelayHours

• **nudgeDelayHours**: *number*

Defined in types.ts:1954

___

###  privacyPolicyUrl

• **privacyPolicyUrl**: *string | null*

Defined in types.ts:1951

___

###  quickGroupUserSelectionLimit

• **quickGroupUserSelectionLimit**: *number*

Defined in types.ts:1953

___

###  termsAndConditionsUrl

• **termsAndConditionsUrl**: *string | null*

Defined in types.ts:1952
