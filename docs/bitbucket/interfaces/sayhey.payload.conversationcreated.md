[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationCreated](sayhey.payload.conversationcreated.md)

# Interface: ConversationCreated

## Hierarchy

* **ConversationCreated**

## Index

### Properties

* [conversationId](sayhey.payload.conversationcreated.md#markdown-header-conversationid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1043
