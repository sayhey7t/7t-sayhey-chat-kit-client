[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [PaginateResponse](paginateresponse.md)

# Interface: PaginateResponse

## Hierarchy

* **PaginateResponse**

  ↳ [FlaggedMessageUserReportPaginateResponse](flaggedmessageuserreportpaginateresponse.md)

  ↳ [FlaggedMessageScanPaginateResponse](flaggedmessagescanpaginateresponse.md)

  ↳ [FlaggedMessageResolutionBatchDTO](flaggedmessageresolutionbatchdto.md)

  ↳ [FlaggedMessageResolutionUserReportBatchDTO](flaggedmessageresolutionuserreportbatchdto.md)

  ↳ [FlaggedMessageResolutionScanBatchDTO](flaggedmessageresolutionscanbatchdto.md)

## Index

### Properties

* [completed](paginateresponse.md#markdown-header-completed)
* [limit](paginateresponse.md#markdown-header-limit)
* [nextOffset](paginateresponse.md#markdown-header-nextoffset)
* [offset](paginateresponse.md#markdown-header-offset)
* [totalCount](paginateresponse.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

Defined in types.ts:2150

___

###  limit

• **limit**: *number*

Defined in types.ts:2149

___

###  nextOffset

• **nextOffset**: *number*

Defined in types.ts:2148

___

###  offset

• **offset**: *number*

Defined in types.ts:2147

___

###  totalCount

• **totalCount**: *number*

Defined in types.ts:2146
