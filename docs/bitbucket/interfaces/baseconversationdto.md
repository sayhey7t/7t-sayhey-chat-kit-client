[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [BaseConversationDTO](baseconversationdto.md)

# Interface: BaseConversationDTO

## Hierarchy

* [BaseConversation](baseconversation.md)

  ↳ **BaseConversationDTO**

  ↳ [IndividualConversationDTO](individualconversationdto.md)

  ↳ [GroupConversationDTO](groupconversationdto.md)

## Index

### Properties

* [badge](baseconversationdto.md#markdown-header-badge)
* [createdAt](baseconversationdto.md#markdown-header-createdat)
* [creatorId](baseconversationdto.md#markdown-header-creatorid)
* [deletedFromConversationAt](baseconversationdto.md#markdown-header-deletedfromconversationat)
* [endBefore](baseconversationdto.md#markdown-header-endbefore)
* [id](baseconversationdto.md#markdown-header-id)
* [isOwner](baseconversationdto.md#markdown-header-isowner)
* [isQuickGroup](baseconversationdto.md#markdown-header-isquickgroup)
* [isSpace](baseconversationdto.md#markdown-header-isspace)
* [lastMessage](baseconversationdto.md#markdown-header-lastmessage)
* [lastReadMessageId](baseconversationdto.md#markdown-header-lastreadmessageid)
* [lastUnreadNudgedAt](baseconversationdto.md#markdown-header-lastunreadnudgedat)
* [muteUntil](baseconversationdto.md#markdown-header-muteuntil)
* [pinnedAt](baseconversationdto.md#markdown-header-pinnedat)
* [serverCreated](baseconversationdto.md#markdown-header-servercreated)
* [serverManaged](baseconversationdto.md#markdown-header-servermanaged)
* [startAfter](baseconversationdto.md#markdown-header-startafter)
* [type](baseconversationdto.md#markdown-header-type)
* [updatedAt](baseconversationdto.md#markdown-header-updatedat)
* [visible](baseconversationdto.md#markdown-header-visible)

## Properties

###  badge

• **badge**: *number | null*

Defined in types.ts:301

___

###  createdAt

• **createdAt**: *string*

Defined in types.ts:305

___

###  creatorId

• **creatorId**: *string | null*

*Inherited from [BaseConversation](baseconversation.md).[creatorId](baseconversation.md#markdown-header-creatorid)*

Defined in types.ts:295

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *string | null*

Defined in types.ts:304

___

###  endBefore

• **endBefore**: *string | null*

Defined in types.ts:303

___

###  id

• **id**: *string*

*Inherited from [BaseConversation](baseconversation.md).[id](baseconversation.md#markdown-header-id)*

Defined in types.ts:285

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[isOwner](baseconversation.md#markdown-header-isowner)*

Defined in types.ts:288

___

###  isQuickGroup

• **isQuickGroup**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[isQuickGroup](baseconversation.md#markdown-header-isquickgroup)*

Defined in types.ts:293

___

###  isSpace

• **isSpace**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[isSpace](baseconversation.md#markdown-header-isspace)*

Defined in types.ts:292

___

###  lastMessage

• **lastMessage**: *[MessageDTO](messagedto.md) | null*

Defined in types.ts:307

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

*Inherited from [BaseConversation](baseconversation.md).[lastReadMessageId](baseconversation.md#markdown-header-lastreadmessageid)*

Defined in types.ts:286

___

###  lastUnreadNudgedAt

• **lastUnreadNudgedAt**: *Date | null*

*Inherited from [BaseConversation](baseconversation.md).[lastUnreadNudgedAt](baseconversation.md#markdown-header-lastunreadnudgedat)*

Defined in types.ts:294

___

###  muteUntil

• **muteUntil**: *string | null*

Defined in types.ts:299

___

###  pinnedAt

• **pinnedAt**: *string | null*

Defined in types.ts:300

___

###  serverCreated

• **serverCreated**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[serverCreated](baseconversation.md#markdown-header-servercreated)*

Defined in types.ts:290

___

###  serverManaged

• **serverManaged**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[serverManaged](baseconversation.md#markdown-header-servermanaged)*

Defined in types.ts:291

___

###  startAfter

• **startAfter**: *string | null*

Defined in types.ts:302

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group) | [Individual](../enums/conversationtype.md#markdown-header-individual)*

*Inherited from [BaseConversation](baseconversation.md).[type](baseconversation.md#markdown-header-type)*

Defined in types.ts:287

___

###  updatedAt

• **updatedAt**: *string*

Defined in types.ts:306

___

###  visible

• **visible**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[visible](baseconversation.md#markdown-header-visible)*

Defined in types.ts:289
