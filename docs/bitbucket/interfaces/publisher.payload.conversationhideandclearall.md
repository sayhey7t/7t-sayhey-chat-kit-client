[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationHideAndClearAll](publisher.payload.conversationhideandclearall.md)

# Interface: ConversationHideAndClearAll

## Hierarchy

* **ConversationHideAndClearAll**

## Index

### Properties

* [conversationUserHideClearDto](publisher.payload.conversationhideandclearall.md#markdown-header-conversationuserhidecleardto)

## Properties

###  conversationUserHideClearDto

• **conversationUserHideClearDto**: *object*

Defined in types.ts:759

#### Type declaration:

* **startAfter**: *string*

* **visible**: *boolean*
