[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationInfoDomainEntityExternal](conversationinfodomainentityexternal.md)

# Interface: ConversationInfoDomainEntityExternal

## Hierarchy

* **ConversationInfoDomainEntityExternal**

## Index

### Properties

* [phone](conversationinfodomainentityexternal.md#markdown-header-phone)
* [userScope](conversationinfodomainentityexternal.md#markdown-header-userscope)

## Properties

###  phone

• **phone**: *[PhoneWithCode](phonewithcode.md)*

Defined in types.ts:252

___

###  userScope

• **userScope**: *[External](../enums/userscope.md#markdown-header-external)*

Defined in types.ts:251
