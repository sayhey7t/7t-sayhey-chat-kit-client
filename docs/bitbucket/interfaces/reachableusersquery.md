[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ReachableUsersQuery](reachableusersquery.md)

# Interface: ReachableUsersQuery

## Hierarchy

* **ReachableUsersQuery**

## Index

### Properties

* [includeRefIdInSearch](reachableusersquery.md#markdown-header-optional-includerefidinsearch)
* [limit](reachableusersquery.md#markdown-header-optional-limit)
* [nextCursor](reachableusersquery.md#markdown-header-optional-nextcursor)
* [searchString](reachableusersquery.md#markdown-header-optional-searchstring)

## Properties

### `Optional` includeRefIdInSearch

• **includeRefIdInSearch**? : *undefined | false | true*

Defined in types.ts:591

___

### `Optional` limit

• **limit**? : *undefined | number*

Defined in types.ts:589

___

### `Optional` nextCursor

• **nextCursor**? : *undefined | number*

Defined in types.ts:590

___

### `Optional` searchString

• **searchString**? : *undefined | string*

Defined in types.ts:588
