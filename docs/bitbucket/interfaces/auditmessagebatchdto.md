[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [AuditMessageBatchDTO](auditmessagebatchdto.md)

# Interface: AuditMessageBatchDTO

## Hierarchy

* object

  ↳ **AuditMessageBatchDTO**

## Index

### Properties

* [results](auditmessagebatchdto.md#markdown-header-results)

## Properties

###  results

• **results**: *[AuditMessageDTO](auditmessagedto.md)[]*

Defined in types.ts:2036
