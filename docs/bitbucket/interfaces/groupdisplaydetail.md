[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [GroupDisplayDetail](groupdisplaydetail.md)

# Interface: GroupDisplayDetail

## Hierarchy

* **GroupDisplayDetail**

## Index

### Properties

* [name](groupdisplaydetail.md#markdown-header-name)
* [picture](groupdisplaydetail.md#markdown-header-picture)
* [type](groupdisplaydetail.md#markdown-header-type)

## Properties

###  name

• **name**: *string*

Defined in types.ts:2315

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

Defined in types.ts:2314

___

###  type

• **type**: *[ConversationGroupType](../enums/conversationgrouptype.md)*

Defined in types.ts:2316
