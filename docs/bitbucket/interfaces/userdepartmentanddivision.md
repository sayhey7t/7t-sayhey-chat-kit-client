[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserDepartmentAndDivision](userdepartmentanddivision.md)

# Interface: UserDepartmentAndDivision

## Hierarchy

* **UserDepartmentAndDivision**

## Index

### Properties

* [departmentId](userdepartmentanddivision.md#markdown-header-departmentid)
* [departmentName](userdepartmentanddivision.md#markdown-header-departmentname)
* [divisionId](userdepartmentanddivision.md#markdown-header-divisionid)
* [divisionName](userdepartmentanddivision.md#markdown-header-divisionname)

## Properties

###  departmentId

• **departmentId**: *string*

Defined in types.ts:639

___

###  departmentName

• **departmentName**: *string*

Defined in types.ts:641

___

###  divisionId

• **divisionId**: *string*

Defined in types.ts:640

___

###  divisionName

• **divisionName**: *string*

Defined in types.ts:642
