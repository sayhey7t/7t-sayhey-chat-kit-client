[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationGroupInfoChanged](publisher.event.conversationgroupinfochanged.md)

# Interface: ConversationGroupInfoChanged

## Hierarchy

* EventMessage

  ↳ **ConversationGroupInfoChanged**

## Index

### Properties

* [event](publisher.event.conversationgroupinfochanged.md#markdown-header-event)
* [payload](publisher.event.conversationgroupinfochanged.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationGroupInfoChanged](../enums/publisher.eventtype.md#markdown-header-conversationgroupinfochanged)*

*Overrides void*

Defined in types.ts:920

___

###  payload

• **payload**: *[ConversationGroupInfoChanged](publisher.payload.conversationgroupinfochanged.md)*

*Overrides void*

Defined in types.ts:921
