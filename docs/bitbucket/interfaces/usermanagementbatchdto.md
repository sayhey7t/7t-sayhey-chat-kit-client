[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserManagementBatchDTO](usermanagementbatchdto.md)

# Interface: UserManagementBatchDTO

## Hierarchy

* [SearchPaginateResponse](searchpaginateresponse.md)

  ↳ **UserManagementBatchDTO**

## Index

### Properties

* [completed](usermanagementbatchdto.md#markdown-header-completed)
* [limit](usermanagementbatchdto.md#markdown-header-limit)
* [nextOffset](usermanagementbatchdto.md#markdown-header-nextoffset)
* [offset](usermanagementbatchdto.md#markdown-header-offset)
* [results](usermanagementbatchdto.md#markdown-header-results)
* [search](usermanagementbatchdto.md#markdown-header-search)
* [totalCount](usermanagementbatchdto.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[UserManagementDTO](usermanagementdto.md)[]*

Defined in types.ts:1901

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
