[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageScanMinimalDomain](flaggedmessagescanminimaldomain.md)

# Interface: FlaggedMessageScanMinimalDomain

## Hierarchy

* **FlaggedMessageScanMinimalDomain**

  ↳ [FlaggedMessageScanDTO](flaggedmessagescandto.md)

## Index

### Properties

* [createdAt](flaggedmessagescanminimaldomain.md#markdown-header-createdat)
* [flaggedMessageId](flaggedmessagescanminimaldomain.md#markdown-header-flaggedmessageid)
* [flaggedResolutionId](flaggedmessagescanminimaldomain.md#markdown-header-flaggedresolutionid)
* [id](flaggedmessagescanminimaldomain.md#markdown-header-id)
* [priorityType](flaggedmessagescanminimaldomain.md#markdown-header-prioritytype)
* [resolved](flaggedmessagescanminimaldomain.md#markdown-header-resolved)
* [scannedAt](flaggedmessagescanminimaldomain.md#markdown-header-scannedat)
* [totalMetrics](flaggedmessagescanminimaldomain.md#markdown-header-totalmetrics)
* [updatedAt](flaggedmessagescanminimaldomain.md#markdown-header-updatedat)

## Properties

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:2256

___

###  flaggedMessageId

• **flaggedMessageId**: *string*

Defined in types.ts:2252

___

###  flaggedResolutionId

• **flaggedResolutionId**: *string | null*

Defined in types.ts:2255

___

###  id

• **id**: *string*

Defined in types.ts:2258

___

###  priorityType

• **priorityType**: *[HIGH](../enums/flaggedmessageprioritytype.md#markdown-header-high) | [LOW](../enums/flaggedmessageprioritytype.md#markdown-header-low)*

Defined in types.ts:2259

___

###  resolved

• **resolved**: *boolean*

Defined in types.ts:2253

___

###  scannedAt

• **scannedAt**: *Date | null*

Defined in types.ts:2257

___

###  totalMetrics

• **totalMetrics**: *number*

Defined in types.ts:2260

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:2254
