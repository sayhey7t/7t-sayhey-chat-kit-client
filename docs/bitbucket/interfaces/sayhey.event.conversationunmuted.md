[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationUnmuted](sayhey.event.conversationunmuted.md)

# Interface: ConversationUnmuted

## Hierarchy

* **ConversationUnmuted**

## Index

### Properties

* [event](sayhey.event.conversationunmuted.md#markdown-header-event)
* [payload](sayhey.event.conversationunmuted.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationUnmuted*

Defined in types.ts:1215

___

###  payload

• **payload**: *[ConversationUnmuted](sayhey.payload.conversationunmuted.md)*

Defined in types.ts:1216
