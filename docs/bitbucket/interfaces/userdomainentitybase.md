[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserDomainEntityBase](userdomainentitybase.md)

# Interface: UserDomainEntityBase

## Hierarchy

* **UserDomainEntityBase**

  ↳ [UserDomainEntityInternal](userdomainentityinternal.md)

  ↳ [UserDomainEntityExternal](userdomainentityexternal.md)

## Index

### Properties

* [deletedAt](userdomainentitybase.md#markdown-header-deletedat)
* [fullName](userdomainentitybase.md#markdown-header-fullname)
* [picture](userdomainentitybase.md#markdown-header-picture)
* [username](userdomainentitybase.md#markdown-header-username)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:431

___

###  fullName

• **fullName**: *string*

Defined in types.ts:428

___

###  picture

• **picture**: *[MediaDomainEntity](mediadomainentity.md) | null*

Defined in types.ts:430

___

###  username

• **username**: *string | null*

Defined in types.ts:429
