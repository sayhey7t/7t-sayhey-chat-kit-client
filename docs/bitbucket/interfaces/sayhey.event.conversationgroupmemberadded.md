[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationGroupMemberAdded](sayhey.event.conversationgroupmemberadded.md)

# Interface: ConversationGroupMemberAdded

## Hierarchy

* EventMessage

  ↳ **ConversationGroupMemberAdded**

## Index

### Properties

* [event](sayhey.event.conversationgroupmemberadded.md#markdown-header-event)
* [payload](sayhey.event.conversationgroupmemberadded.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationGroupMemberAdded*

*Overrides void*

Defined in types.ts:1265

___

###  payload

• **payload**: *[ConversationGroupMemberAdded](sayhey.payload.conversationgroupmemberadded.md)*

*Overrides void*

Defined in types.ts:1266
