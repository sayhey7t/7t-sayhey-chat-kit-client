[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageUserReportDTO](flaggedmessageuserreportdto.md)

# Interface: FlaggedMessageUserReportDTO

## Hierarchy

* **FlaggedMessageUserReportDTO**

## Index

### Properties

* [flaggedMessageId](flaggedmessageuserreportdto.md#markdown-header-flaggedmessageid)
* [flaggedResolutionId](flaggedmessageuserreportdto.md#markdown-header-flaggedresolutionid)
* [reason](flaggedmessageuserreportdto.md#markdown-header-reason)
* [reporter](flaggedmessageuserreportdto.md#markdown-header-reporter)
* [resolved](flaggedmessageuserreportdto.md#markdown-header-resolved)
* [resolvedAt](flaggedmessageuserreportdto.md#markdown-header-resolvedat)
* [updatedAt](flaggedmessageuserreportdto.md#markdown-header-updatedat)

## Properties

###  flaggedMessageId

• **flaggedMessageId**: *string*

Defined in types.ts:2128

___

###  flaggedResolutionId

• **flaggedResolutionId**: *string | null*

Defined in types.ts:2133

___

###  reason

• **reason**: *string*

Defined in types.ts:2132

___

###  reporter

• **reporter**: *[UserMinimal](../globals.md#markdown-header-userminimal)*

Defined in types.ts:2131

___

###  resolved

• **resolved**: *boolean*

Defined in types.ts:2129

___

###  resolvedAt

• **resolvedAt**: *Date*

Defined in types.ts:2134

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:2130
