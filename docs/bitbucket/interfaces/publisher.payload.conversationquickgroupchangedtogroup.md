[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationQuickGroupChangedToGroup](publisher.payload.conversationquickgroupchangedtogroup.md)

# Interface: ConversationQuickGroupChangedToGroup

## Hierarchy

* **ConversationQuickGroupChangedToGroup**

## Index

### Properties

* [conversationId](publisher.payload.conversationquickgroupchangedtogroup.md#markdown-header-conversationid)
* [groupName](publisher.payload.conversationquickgroupchangedtogroup.md#markdown-header-groupname)
* [isQuickGroup](publisher.payload.conversationquickgroupchangedtogroup.md#markdown-header-isquickgroup)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:772

___

###  groupName

• **groupName**: *string*

Defined in types.ts:773

___

###  isQuickGroup

• **isQuickGroup**: *boolean*

Defined in types.ts:774
