[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationUsersBatchForUserAdminDTO](conversationusersbatchforuseradmindto.md)

# Interface: ConversationUsersBatchForUserAdminDTO

## Hierarchy

  ↳ [ConversationUsersBatchMeta](conversationusersbatchmeta.md)

  ↳ **ConversationUsersBatchForUserAdminDTO**

## Index

### Properties

* [completed](conversationusersbatchforuseradmindto.md#markdown-header-completed)
* [conversationId](conversationusersbatchforuseradmindto.md#markdown-header-conversationid)
* [limit](conversationusersbatchforuseradmindto.md#markdown-header-limit)
* [nextOffset](conversationusersbatchforuseradmindto.md#markdown-header-nextoffset)
* [offset](conversationusersbatchforuseradmindto.md#markdown-header-offset)
* [results](conversationusersbatchforuseradmindto.md#markdown-header-results)
* [search](conversationusersbatchforuseradmindto.md#markdown-header-search)
* [totalCount](conversationusersbatchforuseradmindto.md#markdown-header-totalcount)
* [type](conversationusersbatchforuseradmindto.md#markdown-header-type)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  conversationId

• **conversationId**: *string*

*Inherited from [ConversationConversableUsersBatchDTO](conversationconversableusersbatchdto.md).[conversationId](conversationconversableusersbatchdto.md#markdown-header-conversationid)*

Defined in types.ts:627

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[UserOfConversationForUserAdminDTO](../globals.md#markdown-header-userofconversationforuseradmindto)[]*

Defined in types.ts:657

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17

___

###  type

• **type**: *[ConversationUserQueryType](../enums/conversationuserquerytype.md)*

*Inherited from [ConversationUsersBatchMeta](conversationusersbatchmeta.md).[type](conversationusersbatchmeta.md#markdown-header-type)*

Defined in types.ts:631
