[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [DepartmentDTO](departmentdto.md)

# Interface: DepartmentDTO

## Hierarchy

* **DepartmentDTO**

  ↳ [DepartmentWithDivisionCountDTO](departmentwithdivisioncountdto.md)

## Index

### Properties

* [appId](departmentdto.md#markdown-header-appid)
* [deletedAt](departmentdto.md#markdown-header-deletedat)
* [divisions](departmentdto.md#markdown-header-optional-divisions)
* [id](departmentdto.md#markdown-header-id)
* [isDefault](departmentdto.md#markdown-header-isdefault)
* [name](departmentdto.md#markdown-header-name)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:1808

___

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:1809

___

### `Optional` divisions

• **divisions**? : *[DivisionDTO](divisiondto.md)[]*

Defined in types.ts:1811

___

###  id

• **id**: *string*

Defined in types.ts:1806

___

###  isDefault

• **isDefault**: *[tempDefaultColumn](../enums/tempdefaultcolumn.md) | null*

Defined in types.ts:1810

___

###  name

• **name**: *string*

Defined in types.ts:1807
