[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserMinimalCommon](userminimalcommon.md)

# Interface: UserMinimalCommon

## Hierarchy

* **UserMinimalCommon**

  ↳ [UserMinimalInternal](userminimalinternal.md)

  ↳ [UserMinimalExternal](userminimalexternal.md)

## Index

### Properties

* [deletedAt](userminimalcommon.md#markdown-header-deletedat)
* [disabled](userminimalcommon.md#markdown-header-disabled)
* [firstName](userminimalcommon.md#markdown-header-firstname)
* [id](userminimalcommon.md#markdown-header-id)
* [isPreVerified](userminimalcommon.md#markdown-header-ispreverified)
* [lastName](userminimalcommon.md#markdown-header-lastname)
* [profileImage](userminimalcommon.md#markdown-header-profileimage)
* [profileImageId](userminimalcommon.md#markdown-header-profileimageid)
* [profileImageLocalId](userminimalcommon.md#markdown-header-profileimagelocalid)
* [refId](userminimalcommon.md#markdown-header-refid)
* [userScope](userminimalcommon.md#markdown-header-userscope)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:2162

___

###  disabled

• **disabled**: *boolean*

Defined in types.ts:2161

___

###  firstName

• **firstName**: *string*

Defined in types.ts:2155

___

###  id

• **id**: *string*

Defined in types.ts:2154

___

###  isPreVerified

• **isPreVerified**: *boolean*

Defined in types.ts:2163

___

###  lastName

• **lastName**: *string*

Defined in types.ts:2156

___

###  profileImage

• **profileImage**: *[MediaDomain](mediadomain.md) | null*

Defined in types.ts:2159

___

###  profileImageId

• **profileImageId**: *string | null*

Defined in types.ts:2158

___

###  profileImageLocalId

• **profileImageLocalId**: *string | null*

Defined in types.ts:2160

___

###  refId

• **refId**: *string | null*

Defined in types.ts:2157

___

###  userScope

• **userScope**: *[UserScope](../enums/userscope.md)*

Defined in types.ts:2164
