[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UnmanagedUsersCountDTO](unmanageduserscountdto.md)

# Interface: UnmanagedUsersCountDTO

## Hierarchy

* **UnmanagedUsersCountDTO**

## Index

### Properties

* [count](unmanageduserscountdto.md#markdown-header-count)

## Properties

###  count

• **count**: *number*

Defined in types.ts:1909
