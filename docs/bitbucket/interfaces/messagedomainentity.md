[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [MessageDomainEntity](messagedomainentity.md)

# Interface: MessageDomainEntity

## Hierarchy

* [BaseMessage](basemessage.md)

  ↳ **MessageDomainEntity**

  ↳ [PeekMessageDomainEntity](peekmessagedomainentity.md)

## Index

### Properties

* [conversationId](messagedomainentity.md#markdown-header-conversationid)
* [createdAt](messagedomainentity.md#markdown-header-createdat)
* [deletedAt](messagedomainentity.md#markdown-header-deletedat)
* [file](messagedomainentity.md#markdown-header-file)
* [id](messagedomainentity.md#markdown-header-id)
* [parsedText](messagedomainentity.md#markdown-header-parsedtext)
* [previewUrl](messagedomainentity.md#markdown-header-optional-previewurl)
* [reactions](messagedomainentity.md#markdown-header-reactions)
* [refUrl](messagedomainentity.md#markdown-header-refurl)
* [sender](messagedomainentity.md#markdown-header-sender)
* [sent](messagedomainentity.md#markdown-header-sent)
* [sequence](messagedomainentity.md#markdown-header-sequence)
* [systemMessage](messagedomainentity.md#markdown-header-systemmessage)
* [text](messagedomainentity.md#markdown-header-text)
* [type](messagedomainentity.md#markdown-header-type)

## Properties

###  conversationId

• **conversationId**: *string*

*Inherited from [BaseMessage](basemessage.md).[conversationId](basemessage.md#markdown-header-conversationid)*

Defined in types.ts:167

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:206

___

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:207

___

###  file

• **file**: *[MediaDomainEntity](mediadomainentity.md) | null*

Defined in types.ts:205

___

###  id

• **id**: *string*

*Inherited from [BaseMessage](basemessage.md).[id](basemessage.md#markdown-header-id)*

Defined in types.ts:166

___

###  parsedText

• **parsedText**: *[ParsedLine](../globals.md#markdown-header-parsedline)[] | null*

Defined in types.ts:209

___

### `Optional` previewUrl

• **previewUrl**? : *string | null*

Defined in types.ts:210

___

###  reactions

• **reactions**: *[ReactionsObject](../classes/reactionsobject.md)*

*Inherited from [BaseMessage](basemessage.md).[reactions](basemessage.md#markdown-header-reactions)*

Defined in types.ts:172

___

###  refUrl

• **refUrl**: *string | null*

*Inherited from [BaseMessage](basemessage.md).[refUrl](basemessage.md#markdown-header-refurl)*

Defined in types.ts:170

___

###  sender

• **sender**: *[UserDomainEntity](../globals.md#markdown-header-userdomainentity) | null*

Defined in types.ts:204

___

###  sent

• **sent**: *boolean*

Defined in types.ts:208

___

###  sequence

• **sequence**: *number*

*Inherited from [BaseMessage](basemessage.md).[sequence](basemessage.md#markdown-header-sequence)*

Defined in types.ts:171

___

###  systemMessage

• **systemMessage**: *[SystemMessageDomainEntity](../globals.md#markdown-header-systemmessagedomainentity) | null*

Defined in types.ts:211

___

###  text

• **text**: *string | null*

*Inherited from [BaseMessage](basemessage.md).[text](basemessage.md#markdown-header-text)*

Defined in types.ts:169

___

###  type

• **type**: *[MessageType](../enums/messagetype.md)*

*Inherited from [BaseMessage](basemessage.md).[type](basemessage.md#markdown-header-type)*

Defined in types.ts:168
