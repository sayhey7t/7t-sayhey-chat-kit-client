[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserAdminBatchDTO](useradminbatchdto.md)

# Interface: UserAdminBatchDTO

## Hierarchy

* [SearchPaginateResponse](searchpaginateresponse.md)

  ↳ **UserAdminBatchDTO**

## Index

### Properties

* [completed](useradminbatchdto.md#markdown-header-completed)
* [limit](useradminbatchdto.md#markdown-header-limit)
* [nextOffset](useradminbatchdto.md#markdown-header-nextoffset)
* [offset](useradminbatchdto.md#markdown-header-offset)
* [results](useradminbatchdto.md#markdown-header-results)
* [search](useradminbatchdto.md#markdown-header-search)
* [totalCount](useradminbatchdto.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[UserAdminDTO](useradmindto.md)[]*

Defined in types.ts:1929

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
