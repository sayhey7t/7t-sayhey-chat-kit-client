[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserInternalDTO](userinternaldto.md)

# Interface: UserInternalDTO

## Hierarchy

* [UserCommonDTO](usercommondto.md)

  ↳ **UserInternalDTO**

  ↳ [UserInternalForUserAdminDTO](userinternalforuseradmindto.md)

  ↳ [UserDomainEntityInternal](userdomainentityinternal.md)

## Index

### Properties

* [deletedAt](userinternaldto.md#markdown-header-deletedat)
* [disabled](userinternaldto.md#markdown-header-disabled)
* [email](userinternaldto.md#markdown-header-email)
* [firstName](userinternaldto.md#markdown-header-firstname)
* [id](userinternaldto.md#markdown-header-id)
* [lastName](userinternaldto.md#markdown-header-lastname)
* [picture](userinternaldto.md#markdown-header-picture)
* [refId](userinternaldto.md#markdown-header-refid)
* [userScope](userinternaldto.md#markdown-header-userscope)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [UserCommonDTO](usercommondto.md).[deletedAt](usercommondto.md#markdown-header-deletedat)*

Defined in types.ts:398

___

###  disabled

• **disabled**: *boolean*

*Inherited from [UserCommonDTO](usercommondto.md).[disabled](usercommondto.md#markdown-header-disabled)*

Defined in types.ts:397

___

###  email

• **email**: *string*

Defined in types.ts:403

___

###  firstName

• **firstName**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[firstName](usercommondto.md#markdown-header-firstname)*

Defined in types.ts:393

___

###  id

• **id**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[id](usercommondto.md#markdown-header-id)*

Defined in types.ts:392

___

###  lastName

• **lastName**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[lastName](usercommondto.md#markdown-header-lastname)*

Defined in types.ts:394

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

*Inherited from [UserCommonDTO](usercommondto.md).[picture](usercommondto.md#markdown-header-picture)*

Defined in types.ts:396

___

###  refId

• **refId**: *string | null*

*Inherited from [UserCommonDTO](usercommondto.md).[refId](usercommondto.md#markdown-header-refid)*

Defined in types.ts:395

___

###  userScope

• **userScope**: *[Internal](../enums/userscope.md#markdown-header-internal)*

*Overrides [UserCommonDTO](usercommondto.md).[userScope](usercommondto.md#markdown-header-userscope)*

Defined in types.ts:404
