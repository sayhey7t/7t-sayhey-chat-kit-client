[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ValidateSetPasswordOTPDTO](validatesetpasswordotpdto.md)

# Interface: ValidateSetPasswordOTPDTO

## Hierarchy

* **ValidateSetPasswordOTPDTO**

## Index

### Properties

* [setPasswordToken](validatesetpasswordotpdto.md#markdown-header-setpasswordtoken)

## Properties

###  setPasswordToken

• **setPasswordToken**: *string*

Defined in types.ts:1942
