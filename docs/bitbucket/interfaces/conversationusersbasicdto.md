[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationUsersBasicDTO](conversationusersbasicdto.md)

# Interface: ConversationUsersBasicDTO

## Hierarchy

* **ConversationUsersBasicDTO**

## Index

### Properties

* [deletedFromConversationAt](conversationusersbasicdto.md#markdown-header-deletedfromconversationat)
* [id](conversationusersbasicdto.md#markdown-header-id)
* [isOwner](conversationusersbasicdto.md#markdown-header-isowner)
* [userDeletedAt](conversationusersbasicdto.md#markdown-header-userdeletedat)
* [userDisabled](conversationusersbasicdto.md#markdown-header-userdisabled)

## Properties

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

Defined in types.ts:684

___

###  id

• **id**: *string*

Defined in types.ts:683

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:682

___

###  userDeletedAt

• **userDeletedAt**: *Date | null*

Defined in types.ts:686

___

###  userDisabled

• **userDisabled**: *boolean*

Defined in types.ts:685
