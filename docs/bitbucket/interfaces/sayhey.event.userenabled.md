[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [UserEnabled](sayhey.event.userenabled.md)

# Interface: UserEnabled

## Hierarchy

* EventMessage

  ↳ **UserEnabled**

## Index

### Properties

* [event](sayhey.event.userenabled.md#markdown-header-event)
* [payload](sayhey.event.userenabled.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.UserEnabled*

*Overrides void*

Defined in types.ts:1290

___

###  payload

• **payload**: *[UserEnabled](sayhey.payload.userenabled.md)*

*Overrides void*

Defined in types.ts:1291
