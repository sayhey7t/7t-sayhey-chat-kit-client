[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FindByPhoneQueryParams](findbyphonequeryparams.md)

# Interface: FindByPhoneQueryParams

## Hierarchy

* **FindByPhoneQueryParams**

## Index

### Properties

* [phone](findbyphonequeryparams.md#markdown-header-phone)

## Properties

###  phone

• **phone**: *string*

Defined in types.ts:2411
