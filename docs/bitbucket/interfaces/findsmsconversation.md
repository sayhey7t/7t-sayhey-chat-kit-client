[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FindSmsConversation](findsmsconversation.md)

# Interface: FindSmsConversation

## Hierarchy

* **FindSmsConversation**

## Index

### Properties

* [externalUserId](findsmsconversation.md#markdown-header-externaluserid)

## Properties

###  externalUserId

• **externalUserId**: *string*

Defined in types.ts:2459
