[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ExternalUserOptToReceiveSmsDTO](externaluseropttoreceivesmsdto.md)

# Interface: ExternalUserOptToReceiveSmsDTO

## Hierarchy

* **ExternalUserOptToReceiveSmsDTO**

## Index

### Properties

* [appId](externaluseropttoreceivesmsdto.md#markdown-header-appid)
* [hasOptedToReceiveSms](externaluseropttoreceivesmsdto.md#markdown-header-hasoptedtoreceivesms)
* [userId](externaluseropttoreceivesmsdto.md#markdown-header-userid)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:2448

___

###  hasOptedToReceiveSms

• **hasOptedToReceiveSms**: *boolean*

Defined in types.ts:2450

___

###  userId

• **userId**: *string*

Defined in types.ts:2449
