[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationUserQuery](conversationuserquery.md)

# Interface: ConversationUserQuery

## Hierarchy

* [ConversationReachableUsersQuery](conversationreachableusersquery.md)

  ↳ **ConversationUserQuery**

## Index

### Properties

* [conversationId](conversationuserquery.md#markdown-header-conversationid)
* [includeRefIdInSearch](conversationuserquery.md#markdown-header-optional-includerefidinsearch)
* [limit](conversationuserquery.md#markdown-header-optional-limit)
* [nextCursor](conversationuserquery.md#markdown-header-optional-nextcursor)
* [searchString](conversationuserquery.md#markdown-header-optional-searchstring)
* [type](conversationuserquery.md#markdown-header-optional-type)

## Properties

###  conversationId

• **conversationId**: *string*

*Inherited from [ConversationReachableUsersQuery](conversationreachableusersquery.md).[conversationId](conversationreachableusersquery.md#markdown-header-conversationid)*

Defined in types.ts:601

___

### `Optional` includeRefIdInSearch

• **includeRefIdInSearch**? : *undefined | false | true*

*Inherited from [ConversationReachableUsersQuery](conversationreachableusersquery.md).[includeRefIdInSearch](conversationreachableusersquery.md#markdown-header-optional-includerefidinsearch)*

Defined in types.ts:605

___

### `Optional` limit

• **limit**? : *undefined | number*

*Inherited from [ConversationReachableUsersQuery](conversationreachableusersquery.md).[limit](conversationreachableusersquery.md#markdown-header-optional-limit)*

Defined in types.ts:603

___

### `Optional` nextCursor

• **nextCursor**? : *undefined | number*

*Inherited from [ConversationReachableUsersQuery](conversationreachableusersquery.md).[nextCursor](conversationreachableusersquery.md#markdown-header-optional-nextcursor)*

Defined in types.ts:604

___

### `Optional` searchString

• **searchString**? : *undefined | string*

*Inherited from [ConversationReachableUsersQuery](conversationreachableusersquery.md).[searchString](conversationreachableusersquery.md#markdown-header-optional-searchstring)*

Defined in types.ts:602

___

### `Optional` type

• **type**? : *[ConversationUserQueryType](../enums/conversationuserquerytype.md)*

Defined in types.ts:609
