[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [InternalUserFullInfoDTO](internaluserfullinfodto.md)

# Interface: InternalUserFullInfoDTO

## Hierarchy

* **InternalUserFullInfoDTO**

## Index

### Properties

* [deletedAt](internaluserfullinfodto.md#markdown-header-deletedat)
* [department](internaluserfullinfodto.md#markdown-header-department)
* [departmentId](internaluserfullinfodto.md#markdown-header-departmentid)
* [disabled](internaluserfullinfodto.md#markdown-header-disabled)
* [division](internaluserfullinfodto.md#markdown-header-division)
* [divisionId](internaluserfullinfodto.md#markdown-header-divisionid)
* [email](internaluserfullinfodto.md#markdown-header-email)
* [employeeId](internaluserfullinfodto.md#markdown-header-employeeid)
* [firstName](internaluserfullinfodto.md#markdown-header-firstname)
* [id](internaluserfullinfodto.md#markdown-header-id)
* [isAutoAssigned](internaluserfullinfodto.md#markdown-header-isautoassigned)
* [lastName](internaluserfullinfodto.md#markdown-header-lastname)
* [picture](internaluserfullinfodto.md#markdown-header-picture)
* [refId](internaluserfullinfodto.md#markdown-header-refid)
* [title](internaluserfullinfodto.md#markdown-header-title)
* [userScope](internaluserfullinfodto.md#markdown-header-userscope)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:1854

___

###  department

• **department**: *[DepartmentDTO](departmentdto.md)*

Defined in types.ts:1859

___

###  departmentId

• **departmentId**: *string*

Defined in types.ts:1858

___

###  disabled

• **disabled**: *boolean*

Defined in types.ts:1853

___

###  division

• **division**: *[DivisionDTO](divisiondto.md)*

Defined in types.ts:1861

___

###  divisionId

• **divisionId**: *string*

Defined in types.ts:1860

___

###  email

• **email**: *string | null*

Defined in types.ts:1848

___

###  employeeId

• **employeeId**: *string | null*

Defined in types.ts:1857

___

###  firstName

• **firstName**: *string*

Defined in types.ts:1849

___

###  id

• **id**: *string*

Defined in types.ts:1847

___

###  isAutoAssigned

• **isAutoAssigned**: *boolean*

Defined in types.ts:1862

___

###  lastName

• **lastName**: *string*

Defined in types.ts:1850

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

Defined in types.ts:1852

___

###  refId

• **refId**: *string | null*

Defined in types.ts:1851

___

###  title

• **title**: *string | null*

Defined in types.ts:1856

___

###  userScope

• **userScope**: *[UserScope](../enums/userscope.md)*

Defined in types.ts:1855
