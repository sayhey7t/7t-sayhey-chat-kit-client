[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Reaction](reaction.md)

# Interface: Reaction

## Hierarchy

* **Reaction**

## Index

### Properties

* [count](reaction.md#markdown-header-count)
* [reacted](reaction.md#markdown-header-reacted)
* [type](reaction.md#markdown-header-type)

## Properties

###  count

• **count**: *number*

Defined in types.ts:38

___

###  reacted

• **reacted**: *boolean*

Defined in types.ts:39

___

###  type

• **type**: *[ReactionType](../enums/reactiontype.md)*

Defined in types.ts:37
