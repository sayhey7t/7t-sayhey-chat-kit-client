[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationUnpinned](sayhey.payload.conversationunpinned.md)

# Interface: ConversationUnpinned

## Hierarchy

* **ConversationUnpinned**

## Index

### Properties

* [conversationId](sayhey.payload.conversationunpinned.md#markdown-header-conversationid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1066
