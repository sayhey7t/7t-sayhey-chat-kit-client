[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationMuted](sayhey.event.conversationmuted.md)

# Interface: ConversationMuted

## Hierarchy

* **ConversationMuted**

## Index

### Properties

* [event](sayhey.event.conversationmuted.md#markdown-header-event)
* [payload](sayhey.event.conversationmuted.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationMuted*

Defined in types.ts:1205

___

###  payload

• **payload**: *[ConversationMuted](sayhey.payload.conversationmuted.md)*

Defined in types.ts:1206
