[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [MessageRead](publisher.payload.messageread.md)

# Interface: MessageRead

## Hierarchy

* **MessageRead**

## Index

### Properties

* [conversationId](publisher.payload.messageread.md#markdown-header-conversationid)
* [sequence](publisher.payload.messageread.md#markdown-header-sequence)
* [userId](publisher.payload.messageread.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:866

___

###  sequence

• **sequence**: *number*

Defined in types.ts:867

___

###  userId

• **userId**: *string*

Defined in types.ts:865
