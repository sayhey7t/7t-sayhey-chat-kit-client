[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [AuditMessageDomainEntityBatchDTO](auditmessagedomainentitybatchdto.md)

# Interface: AuditMessageDomainEntityBatchDTO

## Hierarchy

* object

  ↳ **AuditMessageDomainEntityBatchDTO**

## Index

### Properties

* [results](auditmessagedomainentitybatchdto.md#markdown-header-results)

## Properties

###  results

• **results**: *[AuditMessageDomainEntityDTO](auditmessagedomainentitydto.md)[]*

Defined in types.ts:2040
