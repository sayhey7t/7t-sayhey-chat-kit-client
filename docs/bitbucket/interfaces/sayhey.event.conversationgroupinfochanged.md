[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationGroupInfoChanged](sayhey.event.conversationgroupinfochanged.md)

# Interface: ConversationGroupInfoChanged

## Hierarchy

* **ConversationGroupInfoChanged**

## Index

### Properties

* [event](sayhey.event.conversationgroupinfochanged.md#markdown-header-event)
* [payload](sayhey.event.conversationgroupinfochanged.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationGroupInfoChanged*

Defined in types.ts:1230

___

###  payload

• **payload**: *[ConversationGroupInfoChanged](sayhey.payload.conversationgroupinfochanged.md)*

Defined in types.ts:1231
