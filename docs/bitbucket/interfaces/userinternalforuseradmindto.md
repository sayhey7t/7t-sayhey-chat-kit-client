[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserInternalForUserAdminDTO](userinternalforuseradmindto.md)

# Interface: UserInternalForUserAdminDTO

## Hierarchy

  ↳ [UserInternalDTO](userinternaldto.md)

  ↳ **UserInternalForUserAdminDTO**

## Index

### Properties

* [deletedAt](userinternalforuseradmindto.md#markdown-header-deletedat)
* [department](userinternalforuseradmindto.md#markdown-header-department)
* [departmentId](userinternalforuseradmindto.md#markdown-header-departmentid)
* [disabled](userinternalforuseradmindto.md#markdown-header-disabled)
* [division](userinternalforuseradmindto.md#markdown-header-division)
* [divisionId](userinternalforuseradmindto.md#markdown-header-divisionid)
* [email](userinternalforuseradmindto.md#markdown-header-email)
* [employeeId](userinternalforuseradmindto.md#markdown-header-employeeid)
* [firstName](userinternalforuseradmindto.md#markdown-header-firstname)
* [id](userinternalforuseradmindto.md#markdown-header-id)
* [lastName](userinternalforuseradmindto.md#markdown-header-lastname)
* [picture](userinternalforuseradmindto.md#markdown-header-picture)
* [refId](userinternalforuseradmindto.md#markdown-header-refid)
* [title](userinternalforuseradmindto.md#markdown-header-title)
* [userScope](userinternalforuseradmindto.md#markdown-header-userscope)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [UserCommonDTO](usercommondto.md).[deletedAt](usercommondto.md#markdown-header-deletedat)*

Defined in types.ts:398

___

###  department

• **department**: *[DepartmentDTO](departmentdto.md)*

Defined in types.ts:418

___

###  departmentId

• **departmentId**: *string*

Defined in types.ts:417

___

###  disabled

• **disabled**: *boolean*

*Inherited from [UserCommonDTO](usercommondto.md).[disabled](usercommondto.md#markdown-header-disabled)*

Defined in types.ts:397

___

###  division

• **division**: *[DivisionDTO](divisiondto.md)*

Defined in types.ts:420

___

###  divisionId

• **divisionId**: *string*

Defined in types.ts:419

___

###  email

• **email**: *string*

*Inherited from [UserInternalDTO](userinternaldto.md).[email](userinternaldto.md#markdown-header-email)*

Defined in types.ts:403

___

###  employeeId

• **employeeId**: *string | null*

Defined in types.ts:416

___

###  firstName

• **firstName**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[firstName](usercommondto.md#markdown-header-firstname)*

Defined in types.ts:393

___

###  id

• **id**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[id](usercommondto.md#markdown-header-id)*

Defined in types.ts:392

___

###  lastName

• **lastName**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[lastName](usercommondto.md#markdown-header-lastname)*

Defined in types.ts:394

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

*Inherited from [UserCommonDTO](usercommondto.md).[picture](usercommondto.md#markdown-header-picture)*

Defined in types.ts:396

___

###  refId

• **refId**: *string | null*

*Inherited from [UserCommonDTO](usercommondto.md).[refId](usercommondto.md#markdown-header-refid)*

Defined in types.ts:395

___

###  title

• **title**: *string | null*

Defined in types.ts:415

___

###  userScope

• **userScope**: *[Internal](../enums/userscope.md#markdown-header-internal)*

*Inherited from [UserInternalDTO](userinternaldto.md).[userScope](userinternaldto.md#markdown-header-userscope)*

*Overrides [UserCommonDTO](usercommondto.md).[userScope](usercommondto.md#markdown-header-userscope)*

Defined in types.ts:404
