[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [MessageReactionsUpdate](sayhey.payload.messagereactionsupdate.md)

# Interface: MessageReactionsUpdate

## Hierarchy

* **MessageReactionsUpdate**

## Index

### Properties

* [conversationId](sayhey.payload.messagereactionsupdate.md#markdown-header-conversationid)
* [messageId](sayhey.payload.messagereactionsupdate.md#markdown-header-messageid)
* [messageReactionsObject](sayhey.payload.messagereactionsupdate.md#markdown-header-messagereactionsobject)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1141

___

###  messageId

• **messageId**: *string*

Defined in types.ts:1142

___

###  messageReactionsObject

• **messageReactionsObject**: *[ReactionsObject](../classes/reactionsobject.md)*

Defined in types.ts:1143
