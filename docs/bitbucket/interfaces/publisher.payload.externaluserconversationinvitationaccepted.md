[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ExternalUserConversationInvitationAccepted](publisher.payload.externaluserconversationinvitationaccepted.md)

# Interface: ExternalUserConversationInvitationAccepted

## Hierarchy

* **ExternalUserConversationInvitationAccepted**

## Index

### Properties

* [acceptedAt](publisher.payload.externaluserconversationinvitationaccepted.md#markdown-header-acceptedat)
* [conversationId](publisher.payload.externaluserconversationinvitationaccepted.md#markdown-header-conversationid)
* [messageDomain](publisher.payload.externaluserconversationinvitationaccepted.md#markdown-header-messagedomain)
* [messageDto](publisher.payload.externaluserconversationinvitationaccepted.md#markdown-header-messagedto)
* [userFirstName](publisher.payload.externaluserconversationinvitationaccepted.md#markdown-header-userfirstname)
* [userId](publisher.payload.externaluserconversationinvitationaccepted.md#markdown-header-userid)
* [userLastName](publisher.payload.externaluserconversationinvitationaccepted.md#markdown-header-userlastname)

## Properties

###  acceptedAt

• **acceptedAt**: *Date*

Defined in types.ts:779

___

###  conversationId

• **conversationId**: *string*

Defined in types.ts:778

___

###  messageDomain

• **messageDomain**: *[MessageDomainEntity](messagedomainentity.md)*

Defined in types.ts:783

___

###  messageDto

• **messageDto**: *[MessageDTO](messagedto.md)*

Defined in types.ts:784

___

###  userFirstName

• **userFirstName**: *string*

Defined in types.ts:781

___

###  userId

• **userId**: *string*

Defined in types.ts:780

___

###  userLastName

• **userLastName**: *string*

Defined in types.ts:782
