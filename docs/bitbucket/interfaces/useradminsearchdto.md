[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserAdminSearchDTO](useradminsearchdto.md)

# Interface: UserAdminSearchDTO

## Hierarchy

* **UserAdminSearchDTO**

## Index

### Properties

* [criteria](useradminsearchdto.md#markdown-header-criteria)
* [id](useradminsearchdto.md#markdown-header-id)
* [userAdminId](useradminsearchdto.md#markdown-header-useradminid)

## Properties

###  criteria

• **criteria**: *[UserSearchCriteria](usersearchcriteria.md) | null*

Defined in types.ts:2059

___

###  id

• **id**: *string*

Defined in types.ts:2057

___

###  userAdminId

• **userAdminId**: *string*

Defined in types.ts:2058
