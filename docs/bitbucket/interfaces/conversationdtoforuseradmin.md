[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationDTOForUserAdmin](conversationdtoforuseradmin.md)

# Interface: ConversationDTOForUserAdmin

## Hierarchy

* **ConversationDTOForUserAdmin**

## Index

### Properties

* [appId](conversationdtoforuseradmin.md#markdown-header-appid)
* [createdAt](conversationdtoforuseradmin.md#markdown-header-createdat)
* [deletedAt](conversationdtoforuseradmin.md#markdown-header-deletedat)
* [group](conversationdtoforuseradmin.md#markdown-header-optional-group)
* [id](conversationdtoforuseradmin.md#markdown-header-id)
* [serverCreated](conversationdtoforuseradmin.md#markdown-header-servercreated)
* [serverManaged](conversationdtoforuseradmin.md#markdown-header-servermanaged)
* [type](conversationdtoforuseradmin.md#markdown-header-type)
* [updatedAt](conversationdtoforuseradmin.md#markdown-header-updatedat)
* [userAdminOwnerId](conversationdtoforuseradmin.md#markdown-header-useradminownerid)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:324

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:327

___

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:329

___

### `Optional` group

• **group**? : *[GroupDisplayDetail](groupdisplaydetail.md)*

Defined in types.ts:326

___

###  id

• **id**: *string*

Defined in types.ts:323

___

###  serverCreated

• **serverCreated**: *boolean*

Defined in types.ts:330

___

###  serverManaged

• **serverManaged**: *boolean*

Defined in types.ts:331

___

###  type

• **type**: *[ConversationType](../enums/conversationtype.md)*

Defined in types.ts:325

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:328

___

###  userAdminOwnerId

• **userAdminOwnerId**: *string*

Defined in types.ts:332
