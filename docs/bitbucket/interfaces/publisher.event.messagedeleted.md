[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [MessageDeleted](publisher.event.messagedeleted.md)

# Interface: MessageDeleted

## Hierarchy

* EventMessage

  ↳ **MessageDeleted**

## Index

### Properties

* [event](publisher.event.messagedeleted.md#markdown-header-event)
* [payload](publisher.event.messagedeleted.md#markdown-header-payload)

## Properties

###  event

• **event**: *[MessageDeleted](../enums/publisher.eventtype.md#markdown-header-messagedeleted)*

*Overrides void*

Defined in types.ts:985

___

###  payload

• **payload**: *[MessageDeleted](publisher.payload.messagedeleted.md)*

*Overrides void*

Defined in types.ts:986
