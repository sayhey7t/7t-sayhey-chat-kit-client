[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [AllConversationsDeleted](sayhey.event.allconversationsdeleted.md)

# Interface: AllConversationsDeleted

## Hierarchy

* **AllConversationsDeleted**

## Index

### Properties

* [event](sayhey.event.allconversationsdeleted.md#markdown-header-event)
* [payload](sayhey.event.allconversationsdeleted.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationHideAndClearAll*

Defined in types.ts:1190

___

###  payload

• **payload**: *[ConversationHideAndClearAll](sayhey.payload.conversationhideandclearall.md)*

Defined in types.ts:1191
