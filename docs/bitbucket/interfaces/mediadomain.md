[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [MediaDomain](mediadomain.md)

# Interface: MediaDomain

## Hierarchy

* **MediaDomain**

## Index

### Properties

* [appId](mediadomain.md#markdown-header-appid)
* [creatorId](mediadomain.md#markdown-header-creatorid)
* [encoding](mediadomain.md#markdown-header-encoding)
* [extension](mediadomain.md#markdown-header-extension)
* [fileSize](mediadomain.md#markdown-header-filesize)
* [fileType](mediadomain.md#markdown-header-filetype)
* [filename](mediadomain.md#markdown-header-filename)
* [id](mediadomain.md#markdown-header-id)
* [imageMetadataDomain](mediadomain.md#markdown-header-imagemetadatadomain)
* [mimeType](mediadomain.md#markdown-header-mimetype)
* [s3Key](mediadomain.md#markdown-header-s3key)
* [s3Url](mediadomain.md#markdown-header-s3url)
* [videoMetadataDomain](mediadomain.md#markdown-header-videometadatadomain)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:2209

___

###  creatorId

• **creatorId**: *string | null*

Defined in types.ts:2210

___

###  encoding

• **encoding**: *string*

Defined in types.ts:2213

___

###  extension

• **extension**: *string*

Defined in types.ts:2212

___

###  fileSize

• **fileSize**: *number*

Defined in types.ts:2215

___

###  fileType

• **fileType**: *[FileType](../enums/filetype.md)*

Defined in types.ts:2216

___

###  filename

• **filename**: *string*

Defined in types.ts:2214

___

###  id

• **id**: *string*

Defined in types.ts:2208

___

###  imageMetadataDomain

• **imageMetadataDomain**: *[ImageMetadataDomain](imagemetadatadomain.md) | null*

Defined in types.ts:2219

___

###  mimeType

• **mimeType**: *string*

Defined in types.ts:2211

___

###  s3Key

• **s3Key**: *string*

Defined in types.ts:2217

___

###  s3Url

• **s3Url**: *string*

Defined in types.ts:2218

___

###  videoMetadataDomain

• **videoMetadataDomain**: *object | null*

Defined in types.ts:2220
