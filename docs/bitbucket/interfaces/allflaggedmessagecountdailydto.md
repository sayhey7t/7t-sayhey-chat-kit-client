[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [AllFlaggedMessageCountDailyDTO](allflaggedmessagecountdailydto.md)

# Interface: AllFlaggedMessageCountDailyDTO

## Hierarchy

* **AllFlaggedMessageCountDailyDTO**

## Index

### Properties

* [highFlaggedMessages](allflaggedmessagecountdailydto.md#markdown-header-highflaggedmessages)
* [lowFlaggedMessages](allflaggedmessagecountdailydto.md#markdown-header-lowflaggedmessages)
* [userReportedFlaggedMessages](allflaggedmessagecountdailydto.md#markdown-header-userreportedflaggedmessages)

## Properties

###  highFlaggedMessages

• **highFlaggedMessages**: *[FlaggedMessageCountDailyDTO](flaggedmessagecountdailydto.md)[]*

Defined in types.ts:1992

___

###  lowFlaggedMessages

• **lowFlaggedMessages**: *[FlaggedMessageCountDailyDTO](flaggedmessagecountdailydto.md)[]*

Defined in types.ts:1993

___

###  userReportedFlaggedMessages

• **userReportedFlaggedMessages**: *[FlaggedMessageCountDailyDTO](flaggedmessagecountdailydto.md)[]*

Defined in types.ts:1994
