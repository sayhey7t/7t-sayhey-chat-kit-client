[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedInfoEntity](flaggedinfoentity.md)

# Interface: FlaggedInfoEntity

## Hierarchy

* **FlaggedInfoEntity**

## Index

### Properties

* [createdAt](flaggedinfoentity.md#markdown-header-createdat)
* [id](flaggedinfoentity.md#markdown-header-id)
* [messageDeleted](flaggedinfoentity.md#markdown-header-messagedeleted)
* [messageId](flaggedinfoentity.md#markdown-header-messageid)
* [resolved](flaggedinfoentity.md#markdown-header-resolved)
* [resolvedAt](flaggedinfoentity.md#markdown-header-resolvedat)
* [updatedAt](flaggedinfoentity.md#markdown-header-updatedat)

## Properties

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:186

___

###  id

• **id**: *string*

Defined in types.ts:188

___

###  messageDeleted

• **messageDeleted**: *boolean*

Defined in types.ts:190

___

###  messageId

• **messageId**: *string*

Defined in types.ts:189

___

###  resolved

• **resolved**: *boolean*

Defined in types.ts:191

___

###  resolvedAt

• **resolvedAt**: *Date | null*

Defined in types.ts:192

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:187
