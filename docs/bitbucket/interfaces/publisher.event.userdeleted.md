[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [UserDeleted](publisher.event.userdeleted.md)

# Interface: UserDeleted

## Hierarchy

* EventMessage

  ↳ **UserDeleted**

## Index

### Properties

* [event](publisher.event.userdeleted.md#markdown-header-event)
* [payload](publisher.event.userdeleted.md#markdown-header-payload)

## Properties

###  event

• **event**: *[UserDeleted](../enums/publisher.eventtype.md#markdown-header-userdeleted)*

*Overrides void*

Defined in types.ts:990

___

###  payload

• **payload**: *[UserDeleted](publisher.payload.userdeleted.md)*

*Overrides void*

Defined in types.ts:991
