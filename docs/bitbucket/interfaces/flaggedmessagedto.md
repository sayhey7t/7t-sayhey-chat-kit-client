[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageDTO](flaggedmessagedto.md)

# Interface: FlaggedMessageDTO

## Hierarchy

* [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md)

  ↳ **FlaggedMessageDTO**

## Index

### Properties

* [createdAt](flaggedmessagedto.md#markdown-header-createdat)
* [id](flaggedmessagedto.md#markdown-header-id)
* [keywords](flaggedmessagedto.md#markdown-header-keywords)
* [message](flaggedmessagedto.md#markdown-header-message)
* [messageDeleted](flaggedmessagedto.md#markdown-header-messagedeleted)
* [messageId](flaggedmessagedto.md#markdown-header-messageid)
* [resolved](flaggedmessagedto.md#markdown-header-resolved)
* [resolvedAt](flaggedmessagedto.md#markdown-header-resolvedat)
* [unresolvedUserReports](flaggedmessagedto.md#markdown-header-unresolveduserreports)
* [updatedAt](flaggedmessagedto.md#markdown-header-updatedat)

## Properties

###  createdAt

• **createdAt**: *Date*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[createdAt](flaggedmessageminimaldomain.md#markdown-header-createdat)*

Defined in types.ts:2078

___

###  id

• **id**: *string*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[id](flaggedmessageminimaldomain.md#markdown-header-id)*

Defined in types.ts:2073

___

###  keywords

• **keywords**: *object*

Defined in types.ts:2098

#### Type declaration:

* **high**: *object[]*

* **low**: *object[]*

___

###  message

• **message**: *[UserMessageDTO](usermessagedto.md)*

Defined in types.ts:2097

___

###  messageDeleted

• **messageDeleted**: *boolean*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[messageDeleted](flaggedmessageminimaldomain.md#markdown-header-messagedeleted)*

Defined in types.ts:2075

___

###  messageId

• **messageId**: *string*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[messageId](flaggedmessageminimaldomain.md#markdown-header-messageid)*

Defined in types.ts:2077

___

###  resolved

• **resolved**: *boolean*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[resolved](flaggedmessageminimaldomain.md#markdown-header-resolved)*

Defined in types.ts:2074

___

###  resolvedAt

• **resolvedAt**: *Date | null*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[resolvedAt](flaggedmessageminimaldomain.md#markdown-header-resolvedat)*

Defined in types.ts:2076

___

###  unresolvedUserReports

• **unresolvedUserReports**: *number*

Defined in types.ts:2102

___

###  updatedAt

• **updatedAt**: *Date*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[updatedAt](flaggedmessageminimaldomain.md#markdown-header-updatedat)*

Defined in types.ts:2079
