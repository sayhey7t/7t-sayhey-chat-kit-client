[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SearchPaginateParams](searchpaginateparams.md)

# Interface: SearchPaginateParams

## Hierarchy

* **SearchPaginateParams**

## Index

### Properties

* [limit](searchpaginateparams.md#markdown-header-limit)
* [offset](searchpaginateparams.md#markdown-header-offset)
* [search](searchpaginateparams.md#markdown-header-search)

## Properties

###  limit

• **limit**: *number*

Defined in types.ts:523

___

###  offset

• **offset**: *number*

Defined in types.ts:522

___

###  search

• **search**: *string*

Defined in types.ts:521
