[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [DepartmentDivisionDTO](departmentdivisiondto.md)

# Interface: DepartmentDivisionDTO

## Hierarchy

* **DepartmentDivisionDTO**

## Index

### Properties

* [appId](departmentdivisiondto.md#markdown-header-appid)
* [deletedAt](departmentdivisiondto.md#markdown-header-deletedat)
* [divisions](departmentdivisiondto.md#markdown-header-divisions)
* [id](departmentdivisiondto.md#markdown-header-id)
* [isDefault](departmentdivisiondto.md#markdown-header-isdefault)
* [name](departmentdivisiondto.md#markdown-header-name)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:2339

___

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:2340

___

###  divisions

• **divisions**: *[DivisionDTO](divisiondto.md)[]*

Defined in types.ts:2342

___

###  id

• **id**: *string*

Defined in types.ts:2337

___

###  isDefault

• **isDefault**: *[tempDefaultColumn](../enums/tempdefaultcolumn.md) | null*

Defined in types.ts:2341

___

###  name

• **name**: *string*

Defined in types.ts:2338
