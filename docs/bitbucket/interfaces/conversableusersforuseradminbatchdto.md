[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversableUsersForUserAdminBatchDTO](conversableusersforuseradminbatchdto.md)

# Interface: ConversableUsersForUserAdminBatchDTO

## Hierarchy

  ↳ [UserSearchPaginateResponse](usersearchpaginateresponse.md)

  ↳ **ConversableUsersForUserAdminBatchDTO**

## Index

### Properties

* [completed](conversableusersforuseradminbatchdto.md#markdown-header-completed)
* [includeRefIdInSearch](conversableusersforuseradminbatchdto.md#markdown-header-includerefidinsearch)
* [limit](conversableusersforuseradminbatchdto.md#markdown-header-limit)
* [nextOffset](conversableusersforuseradminbatchdto.md#markdown-header-nextoffset)
* [offset](conversableusersforuseradminbatchdto.md#markdown-header-offset)
* [results](conversableusersforuseradminbatchdto.md#markdown-header-results)
* [search](conversableusersforuseradminbatchdto.md#markdown-header-search)
* [totalCount](conversableusersforuseradminbatchdto.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  includeRefIdInSearch

• **includeRefIdInSearch**: *boolean*

*Inherited from [ConversableUsersForUserAdminBatchDTO](conversableusersforuseradminbatchdto.md).[includeRefIdInSearch](conversableusersforuseradminbatchdto.md#markdown-header-includerefidinsearch)*

Defined in types.ts:1766

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[UserForUserAdminDTO](../globals.md#markdown-header-userforuseradmindto)[]*

Defined in types.ts:576

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
