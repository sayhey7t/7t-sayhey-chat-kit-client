[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserExternalDTO](userexternaldto.md)

# Interface: UserExternalDTO

## Hierarchy

* [UserCommonDTO](usercommondto.md)

  ↳ **UserExternalDTO**

  ↳ [UserDomainEntityExternal](userdomainentityexternal.md)

## Index

### Properties

* [deletedAt](userexternaldto.md#markdown-header-deletedat)
* [disabled](userexternaldto.md#markdown-header-disabled)
* [firstName](userexternaldto.md#markdown-header-firstname)
* [id](userexternaldto.md#markdown-header-id)
* [lastName](userexternaldto.md#markdown-header-lastname)
* [phone](userexternaldto.md#markdown-header-phone)
* [picture](userexternaldto.md#markdown-header-picture)
* [refId](userexternaldto.md#markdown-header-refid)
* [userScope](userexternaldto.md#markdown-header-userscope)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [UserCommonDTO](usercommondto.md).[deletedAt](usercommondto.md#markdown-header-deletedat)*

Defined in types.ts:398

___

###  disabled

• **disabled**: *boolean*

*Inherited from [UserCommonDTO](usercommondto.md).[disabled](usercommondto.md#markdown-header-disabled)*

Defined in types.ts:397

___

###  firstName

• **firstName**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[firstName](usercommondto.md#markdown-header-firstname)*

Defined in types.ts:393

___

###  id

• **id**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[id](usercommondto.md#markdown-header-id)*

Defined in types.ts:392

___

###  lastName

• **lastName**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[lastName](usercommondto.md#markdown-header-lastname)*

Defined in types.ts:394

___

###  phone

• **phone**: *[PhoneWithCode](phonewithcode.md)*

Defined in types.ts:408

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

*Inherited from [UserCommonDTO](usercommondto.md).[picture](usercommondto.md#markdown-header-picture)*

Defined in types.ts:396

___

###  refId

• **refId**: *string | null*

*Inherited from [UserCommonDTO](usercommondto.md).[refId](usercommondto.md#markdown-header-refid)*

Defined in types.ts:395

___

###  userScope

• **userScope**: *[External](../enums/userscope.md#markdown-header-external)*

*Overrides [UserCommonDTO](usercommondto.md).[userScope](usercommondto.md#markdown-header-userscope)*

Defined in types.ts:409
