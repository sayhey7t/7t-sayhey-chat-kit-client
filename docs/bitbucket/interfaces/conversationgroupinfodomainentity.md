[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationGroupInfoDomainEntity](conversationgroupinfodomainentity.md)

# Interface: ConversationGroupInfoDomainEntity

## Hierarchy

* **ConversationGroupInfoDomainEntity**

## Index

### Properties

* [name](conversationgroupinfodomainentity.md#markdown-header-name)
* [picture](conversationgroupinfodomainentity.md#markdown-header-picture)
* [type](conversationgroupinfodomainentity.md#markdown-header-type)

## Properties

###  name

• **name**: *string*

Defined in types.ts:261

___

###  picture

• **picture**: *[MediaDomainEntity](mediadomainentity.md) | null*

Defined in types.ts:260

___

###  type

• **type**: *[ConversationGroupType](../enums/conversationgrouptype.md)*

Defined in types.ts:262
