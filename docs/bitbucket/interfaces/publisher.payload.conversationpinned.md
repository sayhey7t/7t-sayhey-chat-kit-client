[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationPinned](publisher.payload.conversationpinned.md)

# Interface: ConversationPinned

## Hierarchy

* **ConversationPinned**

## Index

### Properties

* [conversationId](publisher.payload.conversationpinned.md#markdown-header-conversationid)
* [conversationPinDto](publisher.payload.conversationpinned.md#markdown-header-conversationpindto)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:744

___

###  conversationPinDto

• **conversationPinDto**: *object*

Defined in types.ts:745

#### Type declaration:

* **pinnedAt**: *string*
