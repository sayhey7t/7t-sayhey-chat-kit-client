[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ExternalUserTermsAndPrivacyVersionDTO](externalusertermsandprivacyversiondto.md)

# Interface: ExternalUserTermsAndPrivacyVersionDTO

## Hierarchy

* **ExternalUserTermsAndPrivacyVersionDTO**

## Index

### Properties

* [acceptedTermsAndPrivacyAt](externalusertermsandprivacyversiondto.md#markdown-header-acceptedtermsandprivacyat)
* [appId](externalusertermsandprivacyversiondto.md#markdown-header-appid)
* [termsAndPrivacyVersion](externalusertermsandprivacyversiondto.md#markdown-header-termsandprivacyversion)
* [userId](externalusertermsandprivacyversiondto.md#markdown-header-userid)

## Properties

###  acceptedTermsAndPrivacyAt

• **acceptedTermsAndPrivacyAt**: *Date | null*

Defined in types.ts:2444

___

###  appId

• **appId**: *string*

Defined in types.ts:2441

___

###  termsAndPrivacyVersion

• **termsAndPrivacyVersion**: *number*

Defined in types.ts:2443

___

###  userId

• **userId**: *string*

Defined in types.ts:2442
