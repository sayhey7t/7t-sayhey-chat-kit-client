[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ExternalUserNudged](sayhey.event.externalusernudged.md)

# Interface: ExternalUserNudged

## Hierarchy

* EventMessage

  ↳ **ExternalUserNudged**

## Index

### Properties

* [event](sayhey.event.externalusernudged.md#markdown-header-event)
* [payload](sayhey.event.externalusernudged.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ExternalUserNudged*

*Overrides void*

Defined in types.ts:1310

___

###  payload

• **payload**: *[ExternalUserNudged](sayhey.payload.externalusernudged.md)*

*Overrides void*

Defined in types.ts:1311
