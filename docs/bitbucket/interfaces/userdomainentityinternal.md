[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserDomainEntityInternal](userdomainentityinternal.md)

# Interface: UserDomainEntityInternal

## Hierarchy

  ↳ [UserInternalDTO](userinternaldto.md)

* [UserDomainEntityBase](userdomainentitybase.md)

  ↳ **UserDomainEntityInternal**

## Index

### Properties

* [deletedAt](userdomainentityinternal.md#markdown-header-deletedat)
* [disabled](userdomainentityinternal.md#markdown-header-disabled)
* [email](userdomainentityinternal.md#markdown-header-email)
* [firstName](userdomainentityinternal.md#markdown-header-firstname)
* [fullName](userdomainentityinternal.md#markdown-header-fullname)
* [id](userdomainentityinternal.md#markdown-header-id)
* [lastName](userdomainentityinternal.md#markdown-header-lastname)
* [picture](userdomainentityinternal.md#markdown-header-picture)
* [refId](userdomainentityinternal.md#markdown-header-refid)
* [userScope](userdomainentityinternal.md#markdown-header-userscope)
* [username](userdomainentityinternal.md#markdown-header-username)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [UserCommonDTO](usercommondto.md).[deletedAt](usercommondto.md#markdown-header-deletedat)*

*Overrides [UserDomainEntityBase](userdomainentitybase.md).[deletedAt](userdomainentitybase.md#markdown-header-deletedat)*

Defined in types.ts:398

___

###  disabled

• **disabled**: *boolean*

*Inherited from [UserCommonDTO](usercommondto.md).[disabled](usercommondto.md#markdown-header-disabled)*

Defined in types.ts:397

___

###  email

• **email**: *string*

*Inherited from [UserInternalDTO](userinternaldto.md).[email](userinternaldto.md#markdown-header-email)*

Defined in types.ts:403

___

###  firstName

• **firstName**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[firstName](usercommondto.md#markdown-header-firstname)*

Defined in types.ts:393

___

###  fullName

• **fullName**: *string*

*Inherited from [UserDomainEntityBase](userdomainentitybase.md).[fullName](userdomainentitybase.md#markdown-header-fullname)*

Defined in types.ts:428

___

###  id

• **id**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[id](usercommondto.md#markdown-header-id)*

Defined in types.ts:392

___

###  lastName

• **lastName**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[lastName](usercommondto.md#markdown-header-lastname)*

Defined in types.ts:394

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

*Inherited from [UserCommonDTO](usercommondto.md).[picture](usercommondto.md#markdown-header-picture)*

*Overrides [UserDomainEntityBase](userdomainentitybase.md).[picture](userdomainentitybase.md#markdown-header-picture)*

Defined in types.ts:396

___

###  refId

• **refId**: *string | null*

*Inherited from [UserCommonDTO](usercommondto.md).[refId](usercommondto.md#markdown-header-refid)*

Defined in types.ts:395

___

###  userScope

• **userScope**: *[Internal](../enums/userscope.md#markdown-header-internal)*

*Inherited from [UserInternalDTO](userinternaldto.md).[userScope](userinternaldto.md#markdown-header-userscope)*

*Overrides [UserCommonDTO](usercommondto.md).[userScope](usercommondto.md#markdown-header-userscope)*

Defined in types.ts:404

___

###  username

• **username**: *string | null*

*Inherited from [UserDomainEntityBase](userdomainentitybase.md).[username](userdomainentitybase.md#markdown-header-username)*

Defined in types.ts:429
