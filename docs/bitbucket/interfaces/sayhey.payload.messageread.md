[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [MessageRead](sayhey.payload.messageread.md)

# Interface: MessageRead

## Hierarchy

* **MessageRead**

## Index

### Properties

* [conversationId](sayhey.payload.messageread.md#markdown-header-conversationid)
* [sequence](sayhey.payload.messageread.md#markdown-header-sequence)
* [userId](sayhey.payload.messageread.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1176

___

###  sequence

• **sequence**: *number*

Defined in types.ts:1177

___

###  userId

• **userId**: *string*

Defined in types.ts:1175
