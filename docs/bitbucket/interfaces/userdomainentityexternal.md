[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserDomainEntityExternal](userdomainentityexternal.md)

# Interface: UserDomainEntityExternal

## Hierarchy

  ↳ [UserExternalDTO](userexternaldto.md)

* [UserDomainEntityBase](userdomainentitybase.md)

  ↳ **UserDomainEntityExternal**

## Index

### Properties

* [deletedAt](userdomainentityexternal.md#markdown-header-deletedat)
* [disabled](userdomainentityexternal.md#markdown-header-disabled)
* [firstName](userdomainentityexternal.md#markdown-header-firstname)
* [fullName](userdomainentityexternal.md#markdown-header-fullname)
* [id](userdomainentityexternal.md#markdown-header-id)
* [lastName](userdomainentityexternal.md#markdown-header-lastname)
* [phone](userdomainentityexternal.md#markdown-header-phone)
* [picture](userdomainentityexternal.md#markdown-header-picture)
* [refId](userdomainentityexternal.md#markdown-header-refid)
* [userScope](userdomainentityexternal.md#markdown-header-userscope)
* [username](userdomainentityexternal.md#markdown-header-username)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [UserCommonDTO](usercommondto.md).[deletedAt](usercommondto.md#markdown-header-deletedat)*

*Overrides [UserDomainEntityBase](userdomainentitybase.md).[deletedAt](userdomainentitybase.md#markdown-header-deletedat)*

Defined in types.ts:398

___

###  disabled

• **disabled**: *boolean*

*Inherited from [UserCommonDTO](usercommondto.md).[disabled](usercommondto.md#markdown-header-disabled)*

Defined in types.ts:397

___

###  firstName

• **firstName**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[firstName](usercommondto.md#markdown-header-firstname)*

Defined in types.ts:393

___

###  fullName

• **fullName**: *string*

*Inherited from [UserDomainEntityBase](userdomainentitybase.md).[fullName](userdomainentitybase.md#markdown-header-fullname)*

Defined in types.ts:428

___

###  id

• **id**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[id](usercommondto.md#markdown-header-id)*

Defined in types.ts:392

___

###  lastName

• **lastName**: *string*

*Inherited from [UserCommonDTO](usercommondto.md).[lastName](usercommondto.md#markdown-header-lastname)*

Defined in types.ts:394

___

###  phone

• **phone**: *[PhoneWithCode](phonewithcode.md)*

*Inherited from [UserExternalDTO](userexternaldto.md).[phone](userexternaldto.md#markdown-header-phone)*

Defined in types.ts:408

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

*Inherited from [UserCommonDTO](usercommondto.md).[picture](usercommondto.md#markdown-header-picture)*

*Overrides [UserDomainEntityBase](userdomainentitybase.md).[picture](userdomainentitybase.md#markdown-header-picture)*

Defined in types.ts:396

___

###  refId

• **refId**: *string | null*

*Inherited from [UserCommonDTO](usercommondto.md).[refId](usercommondto.md#markdown-header-refid)*

Defined in types.ts:395

___

###  userScope

• **userScope**: *[External](../enums/userscope.md#markdown-header-external)*

*Inherited from [UserExternalDTO](userexternaldto.md).[userScope](userexternaldto.md#markdown-header-userscope)*

*Overrides [UserCommonDTO](usercommondto.md).[userScope](usercommondto.md#markdown-header-userscope)*

Defined in types.ts:409

___

###  username

• **username**: *string | null*

*Inherited from [UserDomainEntityBase](userdomainentitybase.md).[username](userdomainentitybase.md#markdown-header-username)*

Defined in types.ts:429
