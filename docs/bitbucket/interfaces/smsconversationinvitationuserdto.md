[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SmsConversationInvitationUserDTO](smsconversationinvitationuserdto.md)

# Interface: SmsConversationInvitationUserDTO

## Hierarchy

* **SmsConversationInvitationUserDTO**

## Index

### Properties

* [acceptedAt](smsconversationinvitationuserdto.md#markdown-header-acceptedat)
* [conversationGroupInfoId](smsconversationinvitationuserdto.md#markdown-header-conversationgroupinfoid)
* [createdAt](smsconversationinvitationuserdto.md#markdown-header-createdat)
* [expiresAt](smsconversationinvitationuserdto.md#markdown-header-expiresat)
* [id](smsconversationinvitationuserdto.md#markdown-header-id)
* [receiver](smsconversationinvitationuserdto.md#markdown-header-receiver)
* [receiverId](smsconversationinvitationuserdto.md#markdown-header-receiverid)
* [resentAt](smsconversationinvitationuserdto.md#markdown-header-resentat)
* [sender](smsconversationinvitationuserdto.md#markdown-header-sender)
* [senderId](smsconversationinvitationuserdto.md#markdown-header-senderid)

## Properties

###  acceptedAt

• **acceptedAt**: *Date | null*

Defined in types.ts:2479

___

###  conversationGroupInfoId

• **conversationGroupInfoId**: *string*

Defined in types.ts:2475

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:2481

___

###  expiresAt

• **expiresAt**: *Date*

Defined in types.ts:2478

___

###  id

• **id**: *string*

Defined in types.ts:2474

___

###  receiver

• **receiver**: *[UserBasicInfoDTOExternal](userbasicinfodtoexternal.md)*

Defined in types.ts:2483

___

###  receiverId

• **receiverId**: *string*

Defined in types.ts:2477

___

###  resentAt

• **resentAt**: *Date | null*

Defined in types.ts:2480

___

###  sender

• **sender**: *[UserBasicInfoDTOInternal](userbasicinfodtointernal.md)*

Defined in types.ts:2482

___

###  senderId

• **senderId**: *string*

Defined in types.ts:2476
