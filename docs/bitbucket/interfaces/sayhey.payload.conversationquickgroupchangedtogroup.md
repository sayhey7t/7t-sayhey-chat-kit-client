[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationQuickGroupChangedToGroup](sayhey.payload.conversationquickgroupchangedtogroup.md)

# Interface: ConversationQuickGroupChangedToGroup

## Hierarchy

* **ConversationQuickGroupChangedToGroup**

## Index

### Properties

* [conversationId](sayhey.payload.conversationquickgroupchangedtogroup.md#markdown-header-conversationid)
* [groupName](sayhey.payload.conversationquickgroupchangedtogroup.md#markdown-header-groupname)
* [isQuickGroup](sayhey.payload.conversationquickgroupchangedtogroup.md#markdown-header-isquickgroup)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1082

___

###  groupName

• **groupName**: *string*

Defined in types.ts:1083

___

###  isQuickGroup

• **isQuickGroup**: *boolean*

Defined in types.ts:1084
