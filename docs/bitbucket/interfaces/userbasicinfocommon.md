[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserBasicInfoCommon](userbasicinfocommon.md)

# Interface: UserBasicInfoCommon

## Hierarchy

* **UserBasicInfoCommon**

  ↳ [UserBasicInfoDTOInternal](userbasicinfodtointernal.md)

  ↳ [UserBasicInfoDTOExternal](userbasicinfodtoexternal.md)

## Index

### Properties

* [firstName](userbasicinfocommon.md#markdown-header-firstname)
* [id](userbasicinfocommon.md#markdown-header-id)
* [lastName](userbasicinfocommon.md#markdown-header-lastname)
* [userScope](userbasicinfocommon.md#markdown-header-userscope)

## Properties

###  firstName

• **firstName**: *string*

Defined in types.ts:1867

___

###  id

• **id**: *string*

Defined in types.ts:1866

___

###  lastName

• **lastName**: *string*

Defined in types.ts:1868

___

###  userScope

• **userScope**: *[UserScope](../enums/userscope.md)*

Defined in types.ts:1869
