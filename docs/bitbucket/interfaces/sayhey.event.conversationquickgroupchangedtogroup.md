[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationQuickGroupChangedToGroup](sayhey.event.conversationquickgroupchangedtogroup.md)

# Interface: ConversationQuickGroupChangedToGroup

## Hierarchy

* **ConversationQuickGroupChangedToGroup**

## Index

### Properties

* [event](sayhey.event.conversationquickgroupchangedtogroup.md#markdown-header-event)
* [payload](sayhey.event.conversationquickgroupchangedtogroup.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationQuickGroupChangedToGroup*

Defined in types.ts:1235

___

###  payload

• **payload**: *[ConversationQuickGroupChangedToGroup](sayhey.payload.conversationquickgroupchangedtogroup.md)*

Defined in types.ts:1236
