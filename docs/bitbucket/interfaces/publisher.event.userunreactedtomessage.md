[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [UserUnReactedToMessage](publisher.event.userunreactedtomessage.md)

# Interface: UserUnReactedToMessage

## Hierarchy

* EventMessage

  ↳ **UserUnReactedToMessage**

## Index

### Properties

* [event](publisher.event.userunreactedtomessage.md#markdown-header-event)
* [payload](publisher.event.userunreactedtomessage.md#markdown-header-payload)

## Properties

###  event

• **event**: *[UserUnReactedToMessage](../enums/publisher.eventtype.md#markdown-header-userunreactedtomessage)*

*Overrides void*

Defined in types.ts:965

___

###  payload

• **payload**: *[UserUnReactedToMessage](publisher.payload.userunreactedtomessage.md)*

*Overrides void*

Defined in types.ts:966
