[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ImageMetadataDomain](imagemetadatadomain.md)

# Interface: ImageMetadataDomain

## Hierarchy

* **ImageMetadataDomain**

## Index

### Properties

* [fileId](imagemetadatadomain.md#markdown-header-fileid)
* [height](imagemetadatadomain.md#markdown-header-height)
* [subImagesMinimal](imagemetadatadomain.md#markdown-header-subimagesminimal)
* [width](imagemetadatadomain.md#markdown-header-width)

## Properties

###  fileId

• **fileId**: *string*

Defined in types.ts:2226

___

###  height

• **height**: *number*

Defined in types.ts:2228

___

###  subImagesMinimal

• **subImagesMinimal**: *[SubImageMinimal](subimageminimal.md)[]*

Defined in types.ts:2229

___

###  width

• **width**: *number*

Defined in types.ts:2227
