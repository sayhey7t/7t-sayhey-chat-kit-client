[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageDetailsDTO](flaggedmessagedetailsdto.md)

# Interface: FlaggedMessageDetailsDTO

## Hierarchy

* [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md)

  ↳ **FlaggedMessageDetailsDTO**

## Index

### Properties

* [count](flaggedmessagedetailsdto.md#markdown-header-count)
* [createdAt](flaggedmessagedetailsdto.md#markdown-header-createdat)
* [id](flaggedmessagedetailsdto.md#markdown-header-id)
* [keywords](flaggedmessagedetailsdto.md#markdown-header-keywords)
* [message](flaggedmessagedetailsdto.md#markdown-header-message)
* [messageDeleted](flaggedmessagedetailsdto.md#markdown-header-messagedeleted)
* [messageId](flaggedmessagedetailsdto.md#markdown-header-messageid)
* [resolved](flaggedmessagedetailsdto.md#markdown-header-resolved)
* [resolvedAt](flaggedmessagedetailsdto.md#markdown-header-resolvedat)
* [updatedAt](flaggedmessagedetailsdto.md#markdown-header-updatedat)

## Properties

###  count

• **count**: *[FlaggedMessageCount](flaggedmessagecount.md)*

Defined in types.ts:2111

___

###  createdAt

• **createdAt**: *Date*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[createdAt](flaggedmessageminimaldomain.md#markdown-header-createdat)*

Defined in types.ts:2078

___

###  id

• **id**: *string*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[id](flaggedmessageminimaldomain.md#markdown-header-id)*

Defined in types.ts:2073

___

###  keywords

• **keywords**: *object*

Defined in types.ts:2107

#### Type declaration:

* **high**: *object[]*

* **low**: *object[]*

___

###  message

• **message**: *[UserMessageDTO](usermessagedto.md)*

Defined in types.ts:2106

___

###  messageDeleted

• **messageDeleted**: *boolean*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[messageDeleted](flaggedmessageminimaldomain.md#markdown-header-messagedeleted)*

Defined in types.ts:2075

___

###  messageId

• **messageId**: *string*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[messageId](flaggedmessageminimaldomain.md#markdown-header-messageid)*

Defined in types.ts:2077

___

###  resolved

• **resolved**: *boolean*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[resolved](flaggedmessageminimaldomain.md#markdown-header-resolved)*

Defined in types.ts:2074

___

###  resolvedAt

• **resolvedAt**: *Date | null*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[resolvedAt](flaggedmessageminimaldomain.md#markdown-header-resolvedat)*

Defined in types.ts:2076

___

###  updatedAt

• **updatedAt**: *Date*

*Inherited from [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md).[updatedAt](flaggedmessageminimaldomain.md#markdown-header-updatedat)*

Defined in types.ts:2079
