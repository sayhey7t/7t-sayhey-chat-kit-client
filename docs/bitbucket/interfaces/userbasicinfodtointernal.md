[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserBasicInfoDTOInternal](userbasicinfodtointernal.md)

# Interface: UserBasicInfoDTOInternal

## Hierarchy

* [UserBasicInfoCommon](userbasicinfocommon.md)

  ↳ **UserBasicInfoDTOInternal**

## Index

### Properties

* [email](userbasicinfodtointernal.md#markdown-header-email)
* [firstName](userbasicinfodtointernal.md#markdown-header-firstname)
* [id](userbasicinfodtointernal.md#markdown-header-id)
* [lastName](userbasicinfodtointernal.md#markdown-header-lastname)
* [userScope](userbasicinfodtointernal.md#markdown-header-userscope)

## Properties

###  email

• **email**: *string*

Defined in types.ts:1874

___

###  firstName

• **firstName**: *string*

*Inherited from [UserBasicInfoCommon](userbasicinfocommon.md).[firstName](userbasicinfocommon.md#markdown-header-firstname)*

Defined in types.ts:1867

___

###  id

• **id**: *string*

*Inherited from [UserBasicInfoCommon](userbasicinfocommon.md).[id](userbasicinfocommon.md#markdown-header-id)*

Defined in types.ts:1866

___

###  lastName

• **lastName**: *string*

*Inherited from [UserBasicInfoCommon](userbasicinfocommon.md).[lastName](userbasicinfocommon.md#markdown-header-lastname)*

Defined in types.ts:1868

___

###  userScope

• **userScope**: *[Internal](../enums/userscope.md#markdown-header-internal)*

*Overrides [UserBasicInfoCommon](userbasicinfocommon.md).[userScope](userbasicinfocommon.md#markdown-header-userscope)*

Defined in types.ts:1873
