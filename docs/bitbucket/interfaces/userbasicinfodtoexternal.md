[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserBasicInfoDTOExternal](userbasicinfodtoexternal.md)

# Interface: UserBasicInfoDTOExternal

## Hierarchy

* [UserBasicInfoCommon](userbasicinfocommon.md)

  ↳ **UserBasicInfoDTOExternal**

## Index

### Properties

* [firstName](userbasicinfodtoexternal.md#markdown-header-firstname)
* [id](userbasicinfodtoexternal.md#markdown-header-id)
* [lastName](userbasicinfodtoexternal.md#markdown-header-lastname)
* [phone](userbasicinfodtoexternal.md#markdown-header-phone)
* [userScope](userbasicinfodtoexternal.md#markdown-header-userscope)

## Properties

###  firstName

• **firstName**: *string*

*Inherited from [UserBasicInfoCommon](userbasicinfocommon.md).[firstName](userbasicinfocommon.md#markdown-header-firstname)*

Defined in types.ts:1867

___

###  id

• **id**: *string*

*Inherited from [UserBasicInfoCommon](userbasicinfocommon.md).[id](userbasicinfocommon.md#markdown-header-id)*

Defined in types.ts:1866

___

###  lastName

• **lastName**: *string*

*Inherited from [UserBasicInfoCommon](userbasicinfocommon.md).[lastName](userbasicinfocommon.md#markdown-header-lastname)*

Defined in types.ts:1868

___

###  phone

• **phone**: *[PhoneWithCode](phonewithcode.md)*

Defined in types.ts:1879

___

###  userScope

• **userScope**: *[External](../enums/userscope.md#markdown-header-external)*

*Overrides [UserBasicInfoCommon](userbasicinfocommon.md).[userScope](userbasicinfocommon.md#markdown-header-userscope)*

Defined in types.ts:1878
