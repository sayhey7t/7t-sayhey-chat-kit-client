[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationGroupDTOForUserAdmin](conversationgroupdtoforuseradmin.md)

# Interface: ConversationGroupDTOForUserAdmin

## Hierarchy

  ↳ [GroupConversationDTO](groupconversationdto.md)

  ↳ **ConversationGroupDTOForUserAdmin**

  ↳ [ConversationGroupDTOForUserAdminWithUserCount](conversationgroupdtoforuseradminwithusercount.md)

## Index

### Properties

* [badge](conversationgroupdtoforuseradmin.md#markdown-header-badge)
* [createdAt](conversationgroupdtoforuseradmin.md#markdown-header-createdat)
* [creatorId](conversationgroupdtoforuseradmin.md#markdown-header-creatorid)
* [deletedFromConversationAt](conversationgroupdtoforuseradmin.md#markdown-header-deletedfromconversationat)
* [endBefore](conversationgroupdtoforuseradmin.md#markdown-header-endbefore)
* [group](conversationgroupdtoforuseradmin.md#markdown-header-group)
* [id](conversationgroupdtoforuseradmin.md#markdown-header-id)
* [isOwner](conversationgroupdtoforuseradmin.md#markdown-header-isowner)
* [isQuickGroup](conversationgroupdtoforuseradmin.md#markdown-header-isquickgroup)
* [isSpace](conversationgroupdtoforuseradmin.md#markdown-header-isspace)
* [lastMessage](conversationgroupdtoforuseradmin.md#markdown-header-lastmessage)
* [lastReadMessageId](conversationgroupdtoforuseradmin.md#markdown-header-lastreadmessageid)
* [lastUnreadNudgedAt](conversationgroupdtoforuseradmin.md#markdown-header-lastunreadnudgedat)
* [muteUntil](conversationgroupdtoforuseradmin.md#markdown-header-muteuntil)
* [pinnedAt](conversationgroupdtoforuseradmin.md#markdown-header-pinnedat)
* [serverCreated](conversationgroupdtoforuseradmin.md#markdown-header-servercreated)
* [serverManaged](conversationgroupdtoforuseradmin.md#markdown-header-servermanaged)
* [startAfter](conversationgroupdtoforuseradmin.md#markdown-header-startafter)
* [type](conversationgroupdtoforuseradmin.md#markdown-header-type)
* [updatedAt](conversationgroupdtoforuseradmin.md#markdown-header-updatedat)
* [userAdminOwnerId](conversationgroupdtoforuseradmin.md#markdown-header-useradminownerid)
* [visible](conversationgroupdtoforuseradmin.md#markdown-header-visible)

## Properties

###  badge

• **badge**: *number | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[badge](baseconversationdto.md#markdown-header-badge)*

Defined in types.ts:301

___

###  createdAt

• **createdAt**: *string*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[createdAt](baseconversationdto.md#markdown-header-createdat)*

Defined in types.ts:305

___

###  creatorId

• **creatorId**: *string | null*

*Inherited from [BaseConversation](baseconversation.md).[creatorId](baseconversation.md#markdown-header-creatorid)*

Defined in types.ts:295

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *string | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[deletedFromConversationAt](baseconversationdto.md#markdown-header-deletedfromconversationat)*

Defined in types.ts:304

___

###  endBefore

• **endBefore**: *string | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[endBefore](baseconversationdto.md#markdown-header-endbefore)*

Defined in types.ts:303

___

###  group

• **group**: *[GroupDisplayDetail](groupdisplaydetail.md)*

*Overrides [GroupConversationDTO](groupconversationdto.md).[group](groupconversationdto.md#markdown-header-group)*

Defined in types.ts:2321

___

###  id

• **id**: *string*

*Inherited from [BaseConversation](baseconversation.md).[id](baseconversation.md#markdown-header-id)*

Defined in types.ts:285

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[isOwner](baseconversation.md#markdown-header-isowner)*

Defined in types.ts:288

___

###  isQuickGroup

• **isQuickGroup**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[isQuickGroup](baseconversation.md#markdown-header-isquickgroup)*

Defined in types.ts:293

___

###  isSpace

• **isSpace**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[isSpace](baseconversation.md#markdown-header-isspace)*

Defined in types.ts:292

___

###  lastMessage

• **lastMessage**: *[MessageDTO](messagedto.md) | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[lastMessage](baseconversationdto.md#markdown-header-lastmessage)*

Defined in types.ts:307

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

*Inherited from [BaseConversation](baseconversation.md).[lastReadMessageId](baseconversation.md#markdown-header-lastreadmessageid)*

Defined in types.ts:286

___

###  lastUnreadNudgedAt

• **lastUnreadNudgedAt**: *Date | null*

*Inherited from [BaseConversation](baseconversation.md).[lastUnreadNudgedAt](baseconversation.md#markdown-header-lastunreadnudgedat)*

Defined in types.ts:294

___

###  muteUntil

• **muteUntil**: *string | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[muteUntil](baseconversationdto.md#markdown-header-muteuntil)*

Defined in types.ts:299

___

###  pinnedAt

• **pinnedAt**: *string | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[pinnedAt](baseconversationdto.md#markdown-header-pinnedat)*

Defined in types.ts:300

___

###  serverCreated

• **serverCreated**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[serverCreated](baseconversation.md#markdown-header-servercreated)*

Defined in types.ts:290

___

###  serverManaged

• **serverManaged**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[serverManaged](baseconversation.md#markdown-header-servermanaged)*

Defined in types.ts:291

___

###  startAfter

• **startAfter**: *string | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[startAfter](baseconversationdto.md#markdown-header-startafter)*

Defined in types.ts:302

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group)*

*Inherited from [GroupConversationDTO](groupconversationdto.md).[type](groupconversationdto.md#markdown-header-type)*

*Overrides [BaseConversation](baseconversation.md).[type](baseconversation.md#markdown-header-type)*

Defined in types.ts:316

___

###  updatedAt

• **updatedAt**: *string*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[updatedAt](baseconversationdto.md#markdown-header-updatedat)*

Defined in types.ts:306

___

###  userAdminOwnerId

• **userAdminOwnerId**: *string*

Defined in types.ts:2322

___

###  visible

• **visible**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[visible](baseconversation.md#markdown-header-visible)*

Defined in types.ts:289
