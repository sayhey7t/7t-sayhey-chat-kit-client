[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [PeekMessageDomainEntity](peekmessagedomainentity.md)

# Interface: PeekMessageDomainEntity

## Hierarchy

  ↳ [MessageDomainEntity](messagedomainentity.md)

  ↳ **PeekMessageDomainEntity**

## Index

### Properties

* [conversationId](peekmessagedomainentity.md#markdown-header-conversationid)
* [createdAt](peekmessagedomainentity.md#markdown-header-createdat)
* [deletedAt](peekmessagedomainentity.md#markdown-header-deletedat)
* [file](peekmessagedomainentity.md#markdown-header-file)
* [flaggedInfo](peekmessagedomainentity.md#markdown-header-flaggedinfo)
* [id](peekmessagedomainentity.md#markdown-header-id)
* [parsedText](peekmessagedomainentity.md#markdown-header-parsedtext)
* [previewUrl](peekmessagedomainentity.md#markdown-header-optional-previewurl)
* [reactions](peekmessagedomainentity.md#markdown-header-reactions)
* [refUrl](peekmessagedomainentity.md#markdown-header-refurl)
* [sender](peekmessagedomainentity.md#markdown-header-sender)
* [sent](peekmessagedomainentity.md#markdown-header-sent)
* [sequence](peekmessagedomainentity.md#markdown-header-sequence)
* [systemMessage](peekmessagedomainentity.md#markdown-header-systemmessage)
* [text](peekmessagedomainentity.md#markdown-header-text)
* [type](peekmessagedomainentity.md#markdown-header-type)

## Properties

###  conversationId

• **conversationId**: *string*

*Inherited from [BaseMessage](basemessage.md).[conversationId](basemessage.md#markdown-header-conversationid)*

Defined in types.ts:167

___

###  createdAt

• **createdAt**: *Date*

*Inherited from [PeekMessageDomainEntity](peekmessagedomainentity.md).[createdAt](peekmessagedomainentity.md#markdown-header-createdat)*

Defined in types.ts:206

___

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [PeekMessageDomainEntity](peekmessagedomainentity.md).[deletedAt](peekmessagedomainentity.md#markdown-header-deletedat)*

Defined in types.ts:207

___

###  file

• **file**: *[MediaDomainEntity](mediadomainentity.md) | null*

*Inherited from [PeekMessageDomainEntity](peekmessagedomainentity.md).[file](peekmessagedomainentity.md#markdown-header-file)*

Defined in types.ts:205

___

###  flaggedInfo

• **flaggedInfo**: *[FlaggedInfoEntity](flaggedinfoentity.md) | null*

Defined in types.ts:196

___

###  id

• **id**: *string*

*Inherited from [BaseMessage](basemessage.md).[id](basemessage.md#markdown-header-id)*

Defined in types.ts:166

___

###  parsedText

• **parsedText**: *[ParsedLine](../globals.md#markdown-header-parsedline)[] | null*

*Inherited from [PeekMessageDomainEntity](peekmessagedomainentity.md).[parsedText](peekmessagedomainentity.md#markdown-header-parsedtext)*

Defined in types.ts:209

___

### `Optional` previewUrl

• **previewUrl**? : *string | null*

*Inherited from [PeekMessageDomainEntity](peekmessagedomainentity.md).[previewUrl](peekmessagedomainentity.md#markdown-header-optional-previewurl)*

Defined in types.ts:210

___

###  reactions

• **reactions**: *[ReactionsObject](../classes/reactionsobject.md)*

*Inherited from [BaseMessage](basemessage.md).[reactions](basemessage.md#markdown-header-reactions)*

Defined in types.ts:172

___

###  refUrl

• **refUrl**: *string | null*

*Inherited from [BaseMessage](basemessage.md).[refUrl](basemessage.md#markdown-header-refurl)*

Defined in types.ts:170

___

###  sender

• **sender**: *[UserDomainEntity](../globals.md#markdown-header-userdomainentity) | null*

*Inherited from [PeekMessageDomainEntity](peekmessagedomainentity.md).[sender](peekmessagedomainentity.md#markdown-header-sender)*

Defined in types.ts:204

___

###  sent

• **sent**: *boolean*

*Inherited from [PeekMessageDomainEntity](peekmessagedomainentity.md).[sent](peekmessagedomainentity.md#markdown-header-sent)*

Defined in types.ts:208

___

###  sequence

• **sequence**: *number*

*Inherited from [BaseMessage](basemessage.md).[sequence](basemessage.md#markdown-header-sequence)*

Defined in types.ts:171

___

###  systemMessage

• **systemMessage**: *[SystemMessageDomainEntity](../globals.md#markdown-header-systemmessagedomainentity) | null*

*Inherited from [PeekMessageDomainEntity](peekmessagedomainentity.md).[systemMessage](peekmessagedomainentity.md#markdown-header-systemmessage)*

Defined in types.ts:211

___

###  text

• **text**: *string | null*

*Inherited from [BaseMessage](basemessage.md).[text](basemessage.md#markdown-header-text)*

Defined in types.ts:169

___

###  type

• **type**: *[MessageType](../enums/messagetype.md)*

*Inherited from [BaseMessage](basemessage.md).[type](basemessage.md#markdown-header-type)*

Defined in types.ts:168
