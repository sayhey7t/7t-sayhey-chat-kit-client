[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserOnboardingJobDTO](useronboardingjobdto.md)

# Interface: UserOnboardingJobDTO

## Hierarchy

* [UserOnboardingJobMinDTO](useronboardingjobmindto.md)

  ↳ **UserOnboardingJobDTO**

## Index

### Properties

* [appId](useronboardingjobdto.md#markdown-header-appid)
* [createdAt](useronboardingjobdto.md#markdown-header-createdat)
* [department](useronboardingjobdto.md#markdown-header-department)
* [departmentId](useronboardingjobdto.md#markdown-header-departmentid)
* [division](useronboardingjobdto.md#markdown-header-division)
* [divisionId](useronboardingjobdto.md#markdown-header-divisionid)
* [fileName](useronboardingjobdto.md#markdown-header-filename)
* [id](useronboardingjobdto.md#markdown-header-id)
* [status](useronboardingjobdto.md#markdown-header-status)
* [uploadedById](useronboardingjobdto.md#markdown-header-uploadedbyid)

## Properties

###  appId

• **appId**: *string*

*Inherited from [UserOnboardingJobMinDTO](useronboardingjobmindto.md).[appId](useronboardingjobmindto.md#markdown-header-appid)*

Defined in types.ts:2366

___

###  createdAt

• **createdAt**: *Date*

*Inherited from [UserOnboardingJobMinDTO](useronboardingjobmindto.md).[createdAt](useronboardingjobmindto.md#markdown-header-createdat)*

Defined in types.ts:2372

___

###  department

• **department**: *[DepartmentDTO](departmentdto.md)*

Defined in types.ts:2376

___

###  departmentId

• **departmentId**: *string*

*Inherited from [UserOnboardingJobMinDTO](useronboardingjobmindto.md).[departmentId](useronboardingjobmindto.md#markdown-header-departmentid)*

Defined in types.ts:2370

___

###  division

• **division**: *[DivisionDTO](divisiondto.md)*

Defined in types.ts:2377

___

###  divisionId

• **divisionId**: *string*

*Inherited from [UserOnboardingJobMinDTO](useronboardingjobmindto.md).[divisionId](useronboardingjobmindto.md#markdown-header-divisionid)*

Defined in types.ts:2371

___

###  fileName

• **fileName**: *string*

*Inherited from [UserOnboardingJobMinDTO](useronboardingjobmindto.md).[fileName](useronboardingjobmindto.md#markdown-header-filename)*

Defined in types.ts:2367

___

###  id

• **id**: *string*

*Inherited from [UserOnboardingJobMinDTO](useronboardingjobmindto.md).[id](useronboardingjobmindto.md#markdown-header-id)*

Defined in types.ts:2365

___

###  status

• **status**: *[UserOnboardingJobStatus](../enums/useronboardingjobstatus.md)*

*Inherited from [UserOnboardingJobMinDTO](useronboardingjobmindto.md).[status](useronboardingjobmindto.md#markdown-header-status)*

Defined in types.ts:2368

___

###  uploadedById

• **uploadedById**: *string*

*Inherited from [UserOnboardingJobMinDTO](useronboardingjobmindto.md).[uploadedById](useronboardingjobmindto.md#markdown-header-uploadedbyid)*

Defined in types.ts:2369
