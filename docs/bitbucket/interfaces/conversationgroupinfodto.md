[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationGroupInfoDTO](conversationgroupinfodto.md)

# Interface: ConversationGroupInfoDTO

## Hierarchy

* **ConversationGroupInfoDTO**

## Index

### Properties

* [name](conversationgroupinfodto.md#markdown-header-name)
* [picture](conversationgroupinfodto.md#markdown-header-picture)
* [type](conversationgroupinfodto.md#markdown-header-type)

## Properties

###  name

• **name**: *string*

Defined in types.ts:234

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

Defined in types.ts:233

___

###  type

• **type**: *[ConversationGroupType](../enums/conversationgrouptype.md)*

Defined in types.ts:235
