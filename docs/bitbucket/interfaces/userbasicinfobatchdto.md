[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserBasicInfoBatchDTO](userbasicinfobatchdto.md)

# Interface: UserBasicInfoBatchDTO

## Hierarchy

* [SearchPaginateResponse](searchpaginateresponse.md)

  ↳ **UserBasicInfoBatchDTO**

## Index

### Properties

* [completed](userbasicinfobatchdto.md#markdown-header-completed)
* [limit](userbasicinfobatchdto.md#markdown-header-limit)
* [nextOffset](userbasicinfobatchdto.md#markdown-header-nextoffset)
* [offset](userbasicinfobatchdto.md#markdown-header-offset)
* [results](userbasicinfobatchdto.md#markdown-header-results)
* [search](userbasicinfobatchdto.md#markdown-header-search)
* [totalCount](userbasicinfobatchdto.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[UserBasicInfoDTO](../globals.md#markdown-header-userbasicinfodto)[]*

Defined in types.ts:1887

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
