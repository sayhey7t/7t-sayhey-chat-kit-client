[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [UserDeleted](sayhey.payload.userdeleted.md)

# Interface: UserDeleted

## Hierarchy

* **UserDeleted**

## Index

### Properties

* [deletedAt](sayhey.payload.userdeleted.md#markdown-header-deletedat)
* [userId](sayhey.payload.userdeleted.md#markdown-header-userid)

## Properties

###  deletedAt

• **deletedAt**: *Date*

Defined in types.ts:1171

___

###  userId

• **userId**: *string*

Defined in types.ts:1170
