[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationHideAndClear](sayhey.payload.conversationhideandclear.md)

# Interface: ConversationHideAndClear

## Hierarchy

* **ConversationHideAndClear**

## Index

### Properties

* [conversationId](sayhey.payload.conversationhideandclear.md#markdown-header-conversationid)
* [startAfter](sayhey.payload.conversationhideandclear.md#markdown-header-startafter)
* [visible](sayhey.payload.conversationhideandclear.md#markdown-header-visible)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1070

___

###  startAfter

• **startAfter**: *Date*

Defined in types.ts:1071

___

###  visible

• **visible**: *boolean*

Defined in types.ts:1072
