[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationUnmuted](publisher.payload.conversationunmuted.md)

# Interface: ConversationUnmuted

## Hierarchy

* **ConversationUnmuted**

## Index

### Properties

* [conversationId](publisher.payload.conversationunmuted.md#markdown-header-conversationid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:729
