[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationHideAndClearAll](sayhey.payload.conversationhideandclearall.md)

# Interface: ConversationHideAndClearAll

## Hierarchy

* **ConversationHideAndClearAll**

## Index

### Properties

* [startAfter](sayhey.payload.conversationhideandclearall.md#markdown-header-startafter)
* [visible](sayhey.payload.conversationhideandclearall.md#markdown-header-visible)

## Properties

###  startAfter

• **startAfter**: *Date*

Defined in types.ts:1038

___

###  visible

• **visible**: *boolean*

Defined in types.ts:1039
