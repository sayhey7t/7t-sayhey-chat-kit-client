[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserMessageDTO](usermessagedto.md)

# Interface: UserMessageDTO

## Hierarchy

* **UserMessageDTO**

## Index

### Properties

* [conversationId](usermessagedto.md#markdown-header-conversationid)
* [createdAt](usermessagedto.md#markdown-header-createdat)
* [deletedAt](usermessagedto.md#markdown-header-deletedat)
* [file](usermessagedto.md#markdown-header-file)
* [id](usermessagedto.md#markdown-header-id)
* [reactions](usermessagedto.md#markdown-header-reactions)
* [refUrl](usermessagedto.md#markdown-header-refurl)
* [sender](usermessagedto.md#markdown-header-sender)
* [sequence](usermessagedto.md#markdown-header-sequence)
* [text](usermessagedto.md#markdown-header-text)
* [type](usermessagedto.md#markdown-header-type)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:2084

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:2085

___

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:2093

___

###  file

• **file**: *[MediaDTO](mediadto.md) | null*

Defined in types.ts:2091

___

###  id

• **id**: *string*

Defined in types.ts:2083

___

###  reactions

• **reactions**: *[ReactionsObject](../classes/reactionsobject.md)*

Defined in types.ts:2092

___

###  refUrl

• **refUrl**: *string | null*

Defined in types.ts:2089

___

###  sender

• **sender**: *[UserDTO](../globals.md#markdown-header-userdto) | null*

Defined in types.ts:2086

___

###  sequence

• **sequence**: *number*

Defined in types.ts:2090

___

###  text

• **text**: *string | null*

Defined in types.ts:2088

___

###  type

• **type**: *[MessageType](../enums/messagetype.md)*

Defined in types.ts:2087
