[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [DivisionBatchDTO](divisionbatchdto.md)

# Interface: DivisionBatchDTO

## Hierarchy

* [SearchPaginateResponse](searchpaginateresponse.md)

  ↳ **DivisionBatchDTO**

## Index

### Properties

* [completed](divisionbatchdto.md#markdown-header-completed)
* [limit](divisionbatchdto.md#markdown-header-limit)
* [nextOffset](divisionbatchdto.md#markdown-header-nextoffset)
* [offset](divisionbatchdto.md#markdown-header-offset)
* [results](divisionbatchdto.md#markdown-header-results)
* [search](divisionbatchdto.md#markdown-header-search)
* [totalCount](divisionbatchdto.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[DivisionDTO](divisiondto.md)[]*

Defined in types.ts:1831

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
