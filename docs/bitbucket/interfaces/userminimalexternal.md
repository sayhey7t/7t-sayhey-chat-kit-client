[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserMinimalExternal](userminimalexternal.md)

# Interface: UserMinimalExternal

## Hierarchy

* [UserMinimalCommon](userminimalcommon.md)

  ↳ **UserMinimalExternal**

## Index

### Properties

* [deletedAt](userminimalexternal.md#markdown-header-deletedat)
* [disabled](userminimalexternal.md#markdown-header-disabled)
* [firstName](userminimalexternal.md#markdown-header-firstname)
* [id](userminimalexternal.md#markdown-header-id)
* [isPhoneVerified](userminimalexternal.md#markdown-header-isphoneverified)
* [isPreVerified](userminimalexternal.md#markdown-header-ispreverified)
* [lastName](userminimalexternal.md#markdown-header-lastname)
* [phone](userminimalexternal.md#markdown-header-phone)
* [profileImage](userminimalexternal.md#markdown-header-profileimage)
* [profileImageId](userminimalexternal.md#markdown-header-profileimageid)
* [profileImageLocalId](userminimalexternal.md#markdown-header-profileimagelocalid)
* [refId](userminimalexternal.md#markdown-header-refid)
* [userScope](userminimalexternal.md#markdown-header-userscope)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[deletedAt](userminimalcommon.md#markdown-header-deletedat)*

Defined in types.ts:2162

___

###  disabled

• **disabled**: *boolean*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[disabled](userminimalcommon.md#markdown-header-disabled)*

Defined in types.ts:2161

___

###  firstName

• **firstName**: *string*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[firstName](userminimalcommon.md#markdown-header-firstname)*

Defined in types.ts:2155

___

###  id

• **id**: *string*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[id](userminimalcommon.md#markdown-header-id)*

Defined in types.ts:2154

___

###  isPhoneVerified

• **isPhoneVerified**: *boolean*

Defined in types.ts:2197

___

###  isPreVerified

• **isPreVerified**: *boolean*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[isPreVerified](userminimalcommon.md#markdown-header-ispreverified)*

Defined in types.ts:2163

___

###  lastName

• **lastName**: *string*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[lastName](userminimalcommon.md#markdown-header-lastname)*

Defined in types.ts:2156

___

###  phone

• **phone**: *[PhoneWithCode](phonewithcode.md)*

Defined in types.ts:2196

___

###  profileImage

• **profileImage**: *[MediaDomain](mediadomain.md) | null*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[profileImage](userminimalcommon.md#markdown-header-profileimage)*

Defined in types.ts:2159

___

###  profileImageId

• **profileImageId**: *string | null*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[profileImageId](userminimalcommon.md#markdown-header-profileimageid)*

Defined in types.ts:2158

___

###  profileImageLocalId

• **profileImageLocalId**: *string | null*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[profileImageLocalId](userminimalcommon.md#markdown-header-profileimagelocalid)*

Defined in types.ts:2160

___

###  refId

• **refId**: *string | null*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[refId](userminimalcommon.md#markdown-header-refid)*

Defined in types.ts:2157

___

###  userScope

• **userScope**: *[External](../enums/userscope.md#markdown-header-external)*

*Overrides [UserMinimalCommon](userminimalcommon.md).[userScope](userminimalcommon.md#markdown-header-userscope)*

Defined in types.ts:2195
