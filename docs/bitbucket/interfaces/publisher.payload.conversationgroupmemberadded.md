[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationGroupMemberAdded](publisher.payload.conversationgroupmemberadded.md)

# Interface: ConversationGroupMemberAdded

## Hierarchy

* **ConversationGroupMemberAdded**

## Index

### Properties

* [conversationId](publisher.payload.conversationgroupmemberadded.md#markdown-header-conversationid)
* [deletedAt](publisher.payload.conversationgroupmemberadded.md#markdown-header-deletedat)
* [endBefore](publisher.payload.conversationgroupmemberadded.md#markdown-header-endbefore)
* [userIds](publisher.payload.conversationgroupmemberadded.md#markdown-header-userids)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:810

___

###  deletedAt

• **deletedAt**: *null*

Defined in types.ts:812

___

###  endBefore

• **endBefore**: *null*

Defined in types.ts:813

___

###  userIds

• **userIds**: *string[]*

Defined in types.ts:811
