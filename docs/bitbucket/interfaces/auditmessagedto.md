[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [AuditMessageDTO](auditmessagedto.md)

# Interface: AuditMessageDTO

## Hierarchy

* object

  ↳ **AuditMessageDTO**

## Index

### Properties

* [flagged](auditmessagedto.md#markdown-header-flagged)
* [resolved](auditmessagedto.md#markdown-header-optional-resolved)

## Properties

###  flagged

• **flagged**: *boolean*

Defined in types.ts:2242

___

### `Optional` resolved

• **resolved**? : *undefined | false | true*

Defined in types.ts:2243
