[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SystemMessageBase](systemmessagebase.md)

# Interface: SystemMessageBase

## Hierarchy

* **SystemMessageBase**

## Index

### Properties

* [actorCount](systemmessagebase.md#markdown-header-actorcount)
* [actors](systemmessagebase.md#markdown-header-actors)
* [id](systemmessagebase.md#markdown-header-id)
* [initiator](systemmessagebase.md#markdown-header-initiator)
* [refValue](systemmessagebase.md#markdown-header-refvalue)
* [systemMessageType](systemmessagebase.md#markdown-header-systemmessagetype)
* [text](systemmessagebase.md#markdown-header-optional-text)

## Properties

###  actorCount

• **actorCount**: *number*

Defined in types.ts:148

___

###  actors

• **actors**: *[UserSimpleInfo](usersimpleinfo.md)[]*

Defined in types.ts:150

___

###  id

• **id**: *string*

Defined in types.ts:146

___

###  initiator

• **initiator**: *[UserSimpleInfo](usersimpleinfo.md) | null*

Defined in types.ts:149

___

###  refValue

• **refValue**: *string | null*

Defined in types.ts:147

___

###  systemMessageType

• **systemMessageType**: *[SystemMessageType](../enums/systemmessagetype.md)*

Defined in types.ts:145

___

### `Optional` text

• **text**? : *string | null*

Defined in types.ts:151
