[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [PhoneWithCode](phonewithcode.md)

# Interface: PhoneWithCode

## Hierarchy

* **PhoneWithCode**

## Index

### Properties

* [countryCode](phonewithcode.md#markdown-header-countrycode)
* [phoneNumber](phonewithcode.md#markdown-header-phonenumber)

## Properties

###  countryCode

• **countryCode**: *string*

Defined in types.ts:2455

___

###  phoneNumber

• **phoneNumber**: *string*

Defined in types.ts:2454
