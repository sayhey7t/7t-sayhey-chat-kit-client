[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserMinimalInternal](userminimalinternal.md)

# Interface: UserMinimalInternal

## Hierarchy

* [UserMinimalCommon](userminimalcommon.md)

  ↳ **UserMinimalInternal**

## Index

### Properties

* [deletedAt](userminimalinternal.md#markdown-header-deletedat)
* [disabled](userminimalinternal.md#markdown-header-disabled)
* [email](userminimalinternal.md#markdown-header-email)
* [firstName](userminimalinternal.md#markdown-header-firstname)
* [id](userminimalinternal.md#markdown-header-id)
* [isAutoAssigned](userminimalinternal.md#markdown-header-isautoassigned)
* [isEmailVerified](userminimalinternal.md#markdown-header-isemailverified)
* [isPreVerified](userminimalinternal.md#markdown-header-ispreverified)
* [lastName](userminimalinternal.md#markdown-header-lastname)
* [passwordHash](userminimalinternal.md#markdown-header-passwordhash)
* [profileImage](userminimalinternal.md#markdown-header-profileimage)
* [profileImageId](userminimalinternal.md#markdown-header-profileimageid)
* [profileImageLocalId](userminimalinternal.md#markdown-header-profileimagelocalid)
* [refId](userminimalinternal.md#markdown-header-refid)
* [userScope](userminimalinternal.md#markdown-header-userscope)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[deletedAt](userminimalcommon.md#markdown-header-deletedat)*

Defined in types.ts:2162

___

###  disabled

• **disabled**: *boolean*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[disabled](userminimalcommon.md#markdown-header-disabled)*

Defined in types.ts:2161

___

###  email

• **email**: *string*

Defined in types.ts:2188

___

###  firstName

• **firstName**: *string*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[firstName](userminimalcommon.md#markdown-header-firstname)*

Defined in types.ts:2155

___

###  id

• **id**: *string*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[id](userminimalcommon.md#markdown-header-id)*

Defined in types.ts:2154

___

###  isAutoAssigned

• **isAutoAssigned**: *boolean*

Defined in types.ts:2191

___

###  isEmailVerified

• **isEmailVerified**: *boolean*

Defined in types.ts:2190

___

###  isPreVerified

• **isPreVerified**: *boolean*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[isPreVerified](userminimalcommon.md#markdown-header-ispreverified)*

Defined in types.ts:2163

___

###  lastName

• **lastName**: *string*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[lastName](userminimalcommon.md#markdown-header-lastname)*

Defined in types.ts:2156

___

###  passwordHash

• **passwordHash**: *string | null*

Defined in types.ts:2189

___

###  profileImage

• **profileImage**: *[MediaDomain](mediadomain.md) | null*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[profileImage](userminimalcommon.md#markdown-header-profileimage)*

Defined in types.ts:2159

___

###  profileImageId

• **profileImageId**: *string | null*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[profileImageId](userminimalcommon.md#markdown-header-profileimageid)*

Defined in types.ts:2158

___

###  profileImageLocalId

• **profileImageLocalId**: *string | null*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[profileImageLocalId](userminimalcommon.md#markdown-header-profileimagelocalid)*

Defined in types.ts:2160

___

###  refId

• **refId**: *string | null*

*Inherited from [UserMinimalCommon](userminimalcommon.md).[refId](userminimalcommon.md#markdown-header-refid)*

Defined in types.ts:2157

___

###  userScope

• **userScope**: *[Internal](../enums/userscope.md#markdown-header-internal)*

*Overrides [UserMinimalCommon](userminimalcommon.md).[userScope](userminimalcommon.md#markdown-header-userscope)*

Defined in types.ts:2187
