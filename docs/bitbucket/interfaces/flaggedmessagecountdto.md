[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageCountDTO](flaggedmessagecountdto.md)

# Interface: FlaggedMessageCountDTO

## Hierarchy

* **FlaggedMessageCountDTO**

## Index

### Properties

* [highUnresolved](flaggedmessagecountdto.md#markdown-header-highunresolved)
* [lowUnresolved](flaggedmessagecountdto.md#markdown-header-lowunresolved)
* [resolved](flaggedmessagecountdto.md#markdown-header-resolved)
* [userReportedUnresolved](flaggedmessagecountdto.md#markdown-header-userreportedunresolved)

## Properties

###  highUnresolved

• **highUnresolved**: *5*

Defined in types.ts:2121

___

###  lowUnresolved

• **lowUnresolved**: *5*

Defined in types.ts:2122

___

###  resolved

• **resolved**: *3*

Defined in types.ts:2124

___

###  userReportedUnresolved

• **userReportedUnresolved**: *5*

Defined in types.ts:2123
