[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SendTextMessageParams](sendtextmessageparams.md)

# Interface: SendTextMessageParams

## Hierarchy

* [SendMessageParamsBase](sendmessageparamsbase.md)

  ↳ **SendTextMessageParams**

## Index

### Properties

* [clientRefId](sendtextmessageparams.md#markdown-header-clientrefid)
* [conversationId](sendtextmessageparams.md#markdown-header-conversationid)
* [text](sendtextmessageparams.md#markdown-header-text)
* [type](sendtextmessageparams.md#markdown-header-type)

## Properties

###  clientRefId

• **clientRefId**: *string*

*Inherited from [SendMessageParamsBase](sendmessageparamsbase.md).[clientRefId](sendmessageparamsbase.md#markdown-header-clientrefid)*

Defined in types.ts:1689

___

###  conversationId

• **conversationId**: *string*

*Inherited from [SendMessageParamsBase](sendmessageparamsbase.md).[conversationId](sendmessageparamsbase.md#markdown-header-conversationid)*

Defined in types.ts:1688

___

###  text

• **text**: *string*

Defined in types.ts:1694

___

###  type

• **type**: *[Text](../enums/messagetype.md#markdown-header-text)*

Defined in types.ts:1693
