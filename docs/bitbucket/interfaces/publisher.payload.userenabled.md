[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [UserEnabled](publisher.payload.userenabled.md)

# Interface: UserEnabled

## Hierarchy

* **UserEnabled**

## Index

### Properties

* [disabled](publisher.payload.userenabled.md#markdown-header-disabled)
* [userId](publisher.payload.userenabled.md#markdown-header-userid)

## Properties

###  disabled

• **disabled**: *false*

Defined in types.ts:850

___

###  userId

• **userId**: *string*

Defined in types.ts:849
