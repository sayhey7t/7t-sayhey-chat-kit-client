[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationGroupMemberRemoved](publisher.event.conversationgroupmemberremoved.md)

# Interface: ConversationGroupMemberRemoved

## Hierarchy

* EventMessage

  ↳ **ConversationGroupMemberRemoved**

## Index

### Properties

* [event](publisher.event.conversationgroupmemberremoved.md#markdown-header-event)
* [payload](publisher.event.conversationgroupmemberremoved.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationGroupMemberRemoved](../enums/publisher.eventtype.md#markdown-header-conversationgroupmemberremoved)*

*Overrides void*

Defined in types.ts:950

___

###  payload

• **payload**: *[ConversationGroupMemberRemoved](publisher.payload.conversationgroupmemberremoved.md)*

*Overrides void*

Defined in types.ts:951
