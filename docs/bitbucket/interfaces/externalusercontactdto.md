[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ExternalUserContactDTO](externalusercontactdto.md)

# Interface: ExternalUserContactDTO

## Hierarchy

* **ExternalUserContactDTO**

## Index

### Properties

* [externalUser](externalusercontactdto.md#markdown-header-externaluser)
* [externalUserId](externalusercontactdto.md#markdown-header-externaluserid)
* [isOwnContact](externalusercontactdto.md#markdown-header-isowncontact)

## Properties

###  externalUser

• **externalUser**: *[UserBasicInfoDTOExternal](userbasicinfodtoexternal.md)*

Defined in types.ts:2437

___

###  externalUserId

• **externalUserId**: *string*

Defined in types.ts:2436

___

###  isOwnContact

• **isOwnContact**: *boolean*

Defined in types.ts:2435
