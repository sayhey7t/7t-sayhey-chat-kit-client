[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserOnboardingJobMinDTO](useronboardingjobmindto.md)

# Interface: UserOnboardingJobMinDTO

## Hierarchy

* **UserOnboardingJobMinDTO**

  ↳ [UserOnboardingJobDTO](useronboardingjobdto.md)

## Index

### Properties

* [appId](useronboardingjobmindto.md#markdown-header-appid)
* [createdAt](useronboardingjobmindto.md#markdown-header-createdat)
* [departmentId](useronboardingjobmindto.md#markdown-header-departmentid)
* [divisionId](useronboardingjobmindto.md#markdown-header-divisionid)
* [fileName](useronboardingjobmindto.md#markdown-header-filename)
* [id](useronboardingjobmindto.md#markdown-header-id)
* [status](useronboardingjobmindto.md#markdown-header-status)
* [uploadedById](useronboardingjobmindto.md#markdown-header-uploadedbyid)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:2366

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:2372

___

###  departmentId

• **departmentId**: *string*

Defined in types.ts:2370

___

###  divisionId

• **divisionId**: *string*

Defined in types.ts:2371

___

###  fileName

• **fileName**: *string*

Defined in types.ts:2367

___

###  id

• **id**: *string*

Defined in types.ts:2365

___

###  status

• **status**: *[UserOnboardingJobStatus](../enums/useronboardingjobstatus.md)*

Defined in types.ts:2368

___

###  uploadedById

• **uploadedById**: *string*

Defined in types.ts:2369
