[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [BaseMedia](basemedia.md)

# Interface: BaseMedia

## Hierarchy

* **BaseMedia**

  ↳ [MediaDTO](mediadto.md)

  ↳ [MediaDomainEntity](mediadomainentity.md)

## Index

### Properties

* [encoding](basemedia.md#markdown-header-encoding)
* [extension](basemedia.md#markdown-header-extension)
* [fileSize](basemedia.md#markdown-header-filesize)
* [fileType](basemedia.md#markdown-header-filetype)
* [filename](basemedia.md#markdown-header-filename)
* [imageInfo](basemedia.md#markdown-header-imageinfo)
* [mimeType](basemedia.md#markdown-header-mimetype)
* [urlPath](basemedia.md#markdown-header-urlpath)

## Properties

###  encoding

• **encoding**: *string*

Defined in types.ts:91

___

###  extension

• **extension**: *string*

Defined in types.ts:90

___

###  fileSize

• **fileSize**: *number*

Defined in types.ts:93

___

###  fileType

• **fileType**: *[FileType](../enums/filetype.md)*

Defined in types.ts:87

___

###  filename

• **filename**: *string*

Defined in types.ts:92

___

###  imageInfo

• **imageInfo**: *[ImageInfoMedia](imageinfomedia.md) | null*

Defined in types.ts:94

___

###  mimeType

• **mimeType**: *string*

Defined in types.ts:89

___

###  urlPath

• **urlPath**: *string*

Defined in types.ts:88
