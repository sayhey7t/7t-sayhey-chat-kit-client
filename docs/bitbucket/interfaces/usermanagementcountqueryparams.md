[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserManagementCountQueryParams](usermanagementcountqueryparams.md)

# Interface: UserManagementCountQueryParams

## Hierarchy

* **UserManagementCountQueryParams**

## Index

### Properties

* [active](usermanagementcountqueryparams.md#markdown-header-optional-active)
* [activeAndPerm](usermanagementcountqueryparams.md#markdown-header-optional-activeandperm)
* [activeAndTemp](usermanagementcountqueryparams.md#markdown-header-optional-activeandtemp)
* [inactive](usermanagementcountqueryparams.md#markdown-header-optional-inactive)
* [inactiveAndPerm](usermanagementcountqueryparams.md#markdown-header-optional-inactiveandperm)
* [inactiveAndTemp](usermanagementcountqueryparams.md#markdown-header-optional-inactiveandtemp)
* [perm](usermanagementcountqueryparams.md#markdown-header-optional-perm)
* [temp](usermanagementcountqueryparams.md#markdown-header-optional-temp)

## Properties

### `Optional` active

• **active**? : *undefined | false | true*

Defined in types.ts:1721

___

### `Optional` activeAndPerm

• **activeAndPerm**? : *undefined | false | true*

Defined in types.ts:1725

___

### `Optional` activeAndTemp

• **activeAndTemp**? : *undefined | false | true*

Defined in types.ts:1726

___

### `Optional` inactive

• **inactive**? : *undefined | false | true*

Defined in types.ts:1722

___

### `Optional` inactiveAndPerm

• **inactiveAndPerm**? : *undefined | false | true*

Defined in types.ts:1728

___

### `Optional` inactiveAndTemp

• **inactiveAndTemp**? : *undefined | false | true*

Defined in types.ts:1727

___

### `Optional` perm

• **perm**? : *undefined | false | true*

Defined in types.ts:1724

___

### `Optional` temp

• **temp**? : *undefined | false | true*

Defined in types.ts:1723
