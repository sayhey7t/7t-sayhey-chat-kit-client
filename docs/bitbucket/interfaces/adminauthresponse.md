[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [AdminAuthResponse](adminauthresponse.md)

# Interface: AdminAuthResponse

## Hierarchy

* **AdminAuthResponse**

## Index

### Properties

* [accessToken](adminauthresponse.md#markdown-header-accesstoken)
* [refreshToken](adminauthresponse.md#markdown-header-refreshtoken)
* [userId](adminauthresponse.md#markdown-header-userid)

## Properties

###  accessToken

• **accessToken**: *string*

Defined in types.ts:464

___

###  refreshToken

• **refreshToken**: *string*

Defined in types.ts:465

___

###  userId

• **userId**: *string*

Defined in types.ts:463
