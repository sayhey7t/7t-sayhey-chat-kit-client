[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [UserReactedToMessage](sayhey.payload.userreactedtomessage.md)

# Interface: UserReactedToMessage

## Hierarchy

* **UserReactedToMessage**

## Index

### Properties

* [conversationId](sayhey.payload.userreactedtomessage.md#markdown-header-conversationid)
* [messageId](sayhey.payload.userreactedtomessage.md#markdown-header-messageid)
* [type](sayhey.payload.userreactedtomessage.md#markdown-header-type)
* [userId](sayhey.payload.userreactedtomessage.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1134

___

###  messageId

• **messageId**: *string*

Defined in types.ts:1135

___

###  type

• **type**: *[ReactionType](../enums/reactiontype.md)*

Defined in types.ts:1136

___

###  userId

• **userId**: *string*

Defined in types.ts:1137
