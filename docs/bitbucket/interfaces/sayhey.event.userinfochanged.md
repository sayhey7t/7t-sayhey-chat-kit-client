[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [UserInfoChanged](sayhey.event.userinfochanged.md)

# Interface: UserInfoChanged

## Hierarchy

* **UserInfoChanged**

## Index

### Properties

* [event](sayhey.event.userinfochanged.md#markdown-header-event)
* [payload](sayhey.event.userinfochanged.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.UserInfoChanged*

Defined in types.ts:1245

___

###  payload

• **payload**: *[UserInfoChanged](sayhey.payload.userinfochanged.md)*

Defined in types.ts:1246
