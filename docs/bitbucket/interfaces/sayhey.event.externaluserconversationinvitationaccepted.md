[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ExternalUserConversationInvitationAccepted](sayhey.event.externaluserconversationinvitationaccepted.md)

# Interface: ExternalUserConversationInvitationAccepted

## Hierarchy

* **ExternalUserConversationInvitationAccepted**

## Index

### Properties

* [event](sayhey.event.externaluserconversationinvitationaccepted.md#markdown-header-event)
* [payload](sayhey.event.externaluserconversationinvitationaccepted.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ExternalUserConversationInvitationAccepted*

Defined in types.ts:1240

___

###  payload

• **payload**: *[ExternalUserConversationInvitationAccepted](sayhey.payload.externaluserconversationinvitationaccepted.md)*

Defined in types.ts:1241
