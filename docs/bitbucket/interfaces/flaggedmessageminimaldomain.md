[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageMinimalDomain](flaggedmessageminimaldomain.md)

# Interface: FlaggedMessageMinimalDomain

## Hierarchy

* **FlaggedMessageMinimalDomain**

  ↳ [FlaggedMessageDTO](flaggedmessagedto.md)

  ↳ [FlaggedMessageDetailsDTO](flaggedmessagedetailsdto.md)

## Index

### Properties

* [createdAt](flaggedmessageminimaldomain.md#markdown-header-createdat)
* [id](flaggedmessageminimaldomain.md#markdown-header-id)
* [messageDeleted](flaggedmessageminimaldomain.md#markdown-header-messagedeleted)
* [messageId](flaggedmessageminimaldomain.md#markdown-header-messageid)
* [resolved](flaggedmessageminimaldomain.md#markdown-header-resolved)
* [resolvedAt](flaggedmessageminimaldomain.md#markdown-header-resolvedat)
* [updatedAt](flaggedmessageminimaldomain.md#markdown-header-updatedat)

## Properties

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:2078

___

###  id

• **id**: *string*

Defined in types.ts:2073

___

###  messageDeleted

• **messageDeleted**: *boolean*

Defined in types.ts:2075

___

###  messageId

• **messageId**: *string*

Defined in types.ts:2077

___

###  resolved

• **resolved**: *boolean*

Defined in types.ts:2074

___

###  resolvedAt

• **resolvedAt**: *Date | null*

Defined in types.ts:2076

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:2079
