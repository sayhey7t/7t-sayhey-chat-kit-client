[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationCreated](sayhey.event.conversationcreated.md)

# Interface: ConversationCreated

## Hierarchy

* **ConversationCreated**

## Index

### Properties

* [event](sayhey.event.conversationcreated.md#markdown-header-event)
* [payload](sayhey.event.conversationcreated.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationCreated*

Defined in types.ts:1195

___

###  payload

• **payload**: *[ConversationCreated](sayhey.payload.conversationcreated.md)*

Defined in types.ts:1196
