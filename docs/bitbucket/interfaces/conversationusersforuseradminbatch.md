[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationUsersForUserAdminBatch](conversationusersforuseradminbatch.md)

# Interface: ConversationUsersForUserAdminBatch

## Hierarchy

  ↳ [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md)

  ↳ **ConversationUsersForUserAdminBatch**

## Index

### Properties

* [completed](conversationusersforuseradminbatch.md#markdown-header-completed)
* [conversationId](conversationusersforuseradminbatch.md#markdown-header-conversationid)
* [limit](conversationusersforuseradminbatch.md#markdown-header-limit)
* [nextOffset](conversationusersforuseradminbatch.md#markdown-header-nextoffset)
* [offset](conversationusersforuseradminbatch.md#markdown-header-offset)
* [results](conversationusersforuseradminbatch.md#markdown-header-results)
* [search](conversationusersforuseradminbatch.md#markdown-header-search)
* [totalCount](conversationusersforuseradminbatch.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  conversationId

• **conversationId**: *string*

*Inherited from [ConversationConversableUsersBatchDTO](conversationconversableusersbatchdto.md).[conversationId](conversationconversableusersbatchdto.md#markdown-header-conversationid)*

Defined in types.ts:627

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[ConversationUserDomainForUserAdminEntity](../globals.md#markdown-header-conversationuserdomainforuseradminentity)[]*

Defined in types.ts:678

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
