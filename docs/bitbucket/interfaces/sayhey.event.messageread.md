[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [MessageRead](sayhey.event.messageread.md)

# Interface: MessageRead

## Hierarchy

* EventMessage

  ↳ **MessageRead**

## Index

### Properties

* [event](sayhey.event.messageread.md#markdown-header-event)
* [payload](sayhey.event.messageread.md#markdown-header-payload)

## Properties

###  event

• **event**: *[MessageRead](../enums/publisher.eventtype.md#markdown-header-messageread)*

*Overrides void*

Defined in types.ts:1305

___

###  payload

• **payload**: *[MessageRead](sayhey.payload.messageread.md)*

*Overrides void*

Defined in types.ts:1306
