[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationGroupMemberRemoved](sayhey.payload.conversationgroupmemberremoved.md)

# Interface: ConversationGroupMemberRemoved

## Hierarchy

* **ConversationGroupMemberRemoved**

## Index

### Properties

* [conversationId](sayhey.payload.conversationgroupmemberremoved.md#markdown-header-conversationid)
* [deletedAt](sayhey.payload.conversationgroupmemberremoved.md#markdown-header-deletedat)
* [endBefore](sayhey.payload.conversationgroupmemberremoved.md#markdown-header-endbefore)
* [userIds](sayhey.payload.conversationgroupmemberremoved.md#markdown-header-userids)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1120

___

###  deletedAt

• **deletedAt**: *Date*

Defined in types.ts:1122

___

###  endBefore

• **endBefore**: *Date*

Defined in types.ts:1123

___

###  userIds

• **userIds**: *string[]*

Defined in types.ts:1121
