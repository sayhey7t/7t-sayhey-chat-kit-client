[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [BaseConversationDomainEntity](baseconversationdomainentity.md)

# Interface: BaseConversationDomainEntity

## Hierarchy

* object

  ↳ **BaseConversationDomainEntity**

  ↳ [GroupConversationDomainEntity](groupconversationdomainentity.md)

  ↳ [IndividualConversationDomainEntity](individualconversationdomainentity.md)

## Index

### Properties

* [badge](baseconversationdomainentity.md#markdown-header-badge)
* [createdAt](baseconversationdomainentity.md#markdown-header-createdat)
* [deletedFromConversationAt](baseconversationdomainentity.md#markdown-header-deletedfromconversationat)
* [endBefore](baseconversationdomainentity.md#markdown-header-endbefore)
* [lastMessage](baseconversationdomainentity.md#markdown-header-lastmessage)
* [muteUntil](baseconversationdomainentity.md#markdown-header-muteuntil)
* [pinnedAt](baseconversationdomainentity.md#markdown-header-pinnedat)
* [startAfter](baseconversationdomainentity.md#markdown-header-startafter)
* [updatedAt](baseconversationdomainentity.md#markdown-header-updatedat)

## Properties

###  badge

• **badge**: *number*

Defined in types.ts:338

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:342

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

Defined in types.ts:341

___

###  endBefore

• **endBefore**: *Date | null*

Defined in types.ts:340

___

###  lastMessage

• **lastMessage**: *[MessageDomainEntity](messagedomainentity.md) | null*

Defined in types.ts:344

___

###  muteUntil

• **muteUntil**: *Date | null*

Defined in types.ts:336

___

###  pinnedAt

• **pinnedAt**: *Date | null*

Defined in types.ts:337

___

###  startAfter

• **startAfter**: *Date | null*

Defined in types.ts:339

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:343
