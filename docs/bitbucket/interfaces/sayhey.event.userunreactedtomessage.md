[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [UserUnReactedToMessage](sayhey.event.userunreactedtomessage.md)

# Interface: UserUnReactedToMessage

## Hierarchy

* EventMessage

  ↳ **UserUnReactedToMessage**

## Index

### Properties

* [event](sayhey.event.userunreactedtomessage.md#markdown-header-event)
* [payload](sayhey.event.userunreactedtomessage.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.UserUnReactedToMessage*

*Overrides void*

Defined in types.ts:1275

___

###  payload

• **payload**: *[UserUnReactedToMessage](sayhey.payload.userunreactedtomessage.md)*

*Overrides void*

Defined in types.ts:1276
