[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SendFileMessageParams](sendfilemessageparams.md)

# Interface: SendFileMessageParams

## Hierarchy

* [SendMessageParamsBase](sendmessageparamsbase.md)

  ↳ **SendFileMessageParams**

## Index

### Properties

* [clientRefId](sendfilemessageparams.md#markdown-header-clientrefid)
* [conversationId](sendfilemessageparams.md#markdown-header-conversationid)
* [file](sendfilemessageparams.md#markdown-header-file)
* [progress](sendfilemessageparams.md#markdown-header-optional-progress)
* [type](sendfilemessageparams.md#markdown-header-type)

## Properties

###  clientRefId

• **clientRefId**: *string*

*Inherited from [SendMessageParamsBase](sendmessageparamsbase.md).[clientRefId](sendmessageparamsbase.md#markdown-header-clientrefid)*

Defined in types.ts:1689

___

###  conversationId

• **conversationId**: *string*

*Inherited from [SendMessageParamsBase](sendmessageparamsbase.md).[conversationId](sendmessageparamsbase.md#markdown-header-conversationid)*

Defined in types.ts:1688

___

###  file

• **file**: *[FileWithText](../globals.md#markdown-header-filewithtext)*

Defined in types.ts:1710

___

### `Optional` progress

• **progress**? : *undefined | function*

Defined in types.ts:1711

___

###  type

• **type**: *[Image](../enums/messagetype.md#markdown-header-image) | [Audio](../enums/messagetype.md#markdown-header-audio) | [Video](../enums/messagetype.md#markdown-header-video) | [Document](../enums/messagetype.md#markdown-header-document)*

Defined in types.ts:1709
