[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationQuickGroupChangedToGroup](publisher.event.conversationquickgroupchangedtogroup.md)

# Interface: ConversationQuickGroupChangedToGroup

## Hierarchy

* EventMessage

  ↳ **ConversationQuickGroupChangedToGroup**

## Index

### Properties

* [event](publisher.event.conversationquickgroupchangedtogroup.md#markdown-header-event)
* [payload](publisher.event.conversationquickgroupchangedtogroup.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationQuickGroupChangedToGroup](../enums/publisher.eventtype.md#markdown-header-conversationquickgroupchangedtogroup)*

*Overrides void*

Defined in types.ts:925

___

###  payload

• **payload**: *[ConversationQuickGroupChangedToGroup](publisher.payload.conversationquickgroupchangedtogroup.md)*

*Overrides void*

Defined in types.ts:926
