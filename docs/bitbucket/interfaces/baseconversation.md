[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [BaseConversation](baseconversation.md)

# Interface: BaseConversation

## Hierarchy

* **BaseConversation**

  ↳ [BaseConversationDTO](baseconversationdto.md)

## Index

### Properties

* [creatorId](baseconversation.md#markdown-header-creatorid)
* [id](baseconversation.md#markdown-header-id)
* [isOwner](baseconversation.md#markdown-header-isowner)
* [isQuickGroup](baseconversation.md#markdown-header-isquickgroup)
* [isSpace](baseconversation.md#markdown-header-isspace)
* [lastReadMessageId](baseconversation.md#markdown-header-lastreadmessageid)
* [lastUnreadNudgedAt](baseconversation.md#markdown-header-lastunreadnudgedat)
* [serverCreated](baseconversation.md#markdown-header-servercreated)
* [serverManaged](baseconversation.md#markdown-header-servermanaged)
* [type](baseconversation.md#markdown-header-type)
* [visible](baseconversation.md#markdown-header-visible)

## Properties

###  creatorId

• **creatorId**: *string | null*

Defined in types.ts:295

___

###  id

• **id**: *string*

Defined in types.ts:285

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:288

___

###  isQuickGroup

• **isQuickGroup**: *boolean*

Defined in types.ts:293

___

###  isSpace

• **isSpace**: *boolean*

Defined in types.ts:292

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

Defined in types.ts:286

___

###  lastUnreadNudgedAt

• **lastUnreadNudgedAt**: *Date | null*

Defined in types.ts:294

___

###  serverCreated

• **serverCreated**: *boolean*

Defined in types.ts:290

___

###  serverManaged

• **serverManaged**: *boolean*

Defined in types.ts:291

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group) | [Individual](../enums/conversationtype.md#markdown-header-individual)*

Defined in types.ts:287

___

###  visible

• **visible**: *boolean*

Defined in types.ts:289
