[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationGroupOwnerRemoved](sayhey.payload.conversationgroupownerremoved.md)

# Interface: ConversationGroupOwnerRemoved

## Hierarchy

* **ConversationGroupOwnerRemoved**

## Index

### Properties

* [conversationId](sayhey.payload.conversationgroupownerremoved.md#markdown-header-conversationid)
* [isOwner](sayhey.payload.conversationgroupownerremoved.md#markdown-header-isowner)
* [userId](sayhey.payload.conversationgroupownerremoved.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1108

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:1110

___

###  userId

• **userId**: *string*

Defined in types.ts:1109
