[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UnresolvedFlaggedMessagesCountDTO](unresolvedflaggedmessagescountdto.md)

# Interface: UnresolvedFlaggedMessagesCountDTO

## Hierarchy

* **UnresolvedFlaggedMessagesCountDTO**

## Index

### Properties

* [highUnresolvedCount](unresolvedflaggedmessagescountdto.md#markdown-header-highunresolvedcount)
* [lowUnresolvedCount](unresolvedflaggedmessagescountdto.md#markdown-header-lowunresolvedcount)
* [userReportedUnresolvedCount](unresolvedflaggedmessagescountdto.md#markdown-header-userreportedunresolvedcount)

## Properties

###  highUnresolvedCount

• **highUnresolvedCount**: *number*

Defined in types.ts:1979

___

###  lowUnresolvedCount

• **lowUnresolvedCount**: *number*

Defined in types.ts:1980

___

###  userReportedUnresolvedCount

• **userReportedUnresolvedCount**: *number*

Defined in types.ts:1981
