[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [UserInfoChanged](sayhey.payload.userinfochanged.md)

# Interface: UserInfoChanged

## Hierarchy

* **UserInfoChanged**

## Index

### Properties

* [userEmail](sayhey.payload.userinfochanged.md#markdown-header-useremail)
* [userFirstName](sayhey.payload.userinfochanged.md#markdown-header-userfirstname)
* [userId](sayhey.payload.userinfochanged.md#markdown-header-userid)
* [userLastName](sayhey.payload.userinfochanged.md#markdown-header-userlastname)
* [userMuteAllUntil](sayhey.payload.userinfochanged.md#markdown-header-usermutealluntil)
* [userPicture](sayhey.payload.userinfochanged.md#markdown-header-userpicture)
* [userRefId](sayhey.payload.userinfochanged.md#markdown-header-userrefid)

## Properties

###  userEmail

• **userEmail**: *string*

Defined in types.ts:1099

___

###  userFirstName

• **userFirstName**: *string*

Defined in types.ts:1101

___

###  userId

• **userId**: *string*

Defined in types.ts:1098

___

###  userLastName

• **userLastName**: *string*

Defined in types.ts:1102

___

###  userMuteAllUntil

• **userMuteAllUntil**: *Date | null*

Defined in types.ts:1104

___

###  userPicture

• **userPicture**: *[MediaDomainEntity](mediadomainentity.md) | null*

Defined in types.ts:1100

___

###  userRefId

• **userRefId**: *string | null*

Defined in types.ts:1103
