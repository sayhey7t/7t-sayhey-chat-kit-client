[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationGroupBatchDTOForUserAdminWithUserCount](conversationgroupbatchdtoforuseradminwithusercount.md)

# Interface: ConversationGroupBatchDTOForUserAdminWithUserCount

## Hierarchy

* [SearchPaginateResponse](searchpaginateresponse.md)

  ↳ **ConversationGroupBatchDTOForUserAdminWithUserCount**

## Index

### Properties

* [completed](conversationgroupbatchdtoforuseradminwithusercount.md#markdown-header-completed)
* [limit](conversationgroupbatchdtoforuseradminwithusercount.md#markdown-header-limit)
* [nextOffset](conversationgroupbatchdtoforuseradminwithusercount.md#markdown-header-nextoffset)
* [offset](conversationgroupbatchdtoforuseradminwithusercount.md#markdown-header-offset)
* [results](conversationgroupbatchdtoforuseradminwithusercount.md#markdown-header-results)
* [search](conversationgroupbatchdtoforuseradminwithusercount.md#markdown-header-search)
* [totalCount](conversationgroupbatchdtoforuseradminwithusercount.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[ConversationGroupDTOForUserAdminWithUserCount](conversationgroupdtoforuseradminwithusercount.md)[]*

Defined in types.ts:2331

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
