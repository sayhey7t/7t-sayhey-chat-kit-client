[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationGroupInfoChanged](sayhey.payload.conversationgroupinfochanged.md)

# Interface: ConversationGroupInfoChanged

## Hierarchy

* **ConversationGroupInfoChanged**

## Index

### Properties

* [conversationId](sayhey.payload.conversationgroupinfochanged.md#markdown-header-conversationid)
* [groupName](sayhey.payload.conversationgroupinfochanged.md#markdown-header-groupname)
* [groupPicture](sayhey.payload.conversationgroupinfochanged.md#markdown-header-grouppicture)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1076

___

###  groupName

• **groupName**: *string*

Defined in types.ts:1077

___

###  groupPicture

• **groupPicture**: *[MediaDomainEntity](mediadomainentity.md) | null*

Defined in types.ts:1078
