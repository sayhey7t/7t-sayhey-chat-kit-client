[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ExternalUserNudged](publisher.event.externalusernudged.md)

# Interface: ExternalUserNudged

## Hierarchy

* EventMessage

  ↳ **ExternalUserNudged**

## Index

### Properties

* [event](publisher.event.externalusernudged.md#markdown-header-event)
* [payload](publisher.event.externalusernudged.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ExternalUserNudged](../enums/publisher.eventtype.md#markdown-header-externalusernudged)*

*Overrides void*

Defined in types.ts:1000

___

###  payload

• **payload**: *[ExternalUserNudged](publisher.payload.externalusernudged.md)*

*Overrides void*

Defined in types.ts:1001
