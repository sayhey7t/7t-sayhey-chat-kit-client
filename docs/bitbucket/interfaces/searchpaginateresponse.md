[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SearchPaginateResponse](searchpaginateresponse.md)

# Interface: SearchPaginateResponse

## Hierarchy

* **SearchPaginateResponse**

  ↳ [ConversableUsersBatchMeta](conversableusersbatchmeta.md)

  ↳ [ConversableUsersForUserAdminBatchResult](conversableusersforuseradminbatchresult.md)

  ↳ [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md)

  ↳ [UserSearchPaginateResponse](usersearchpaginateresponse.md)

  ↳ [DepartmentBatchDTO](departmentbatchdto.md)

  ↳ [DivisionBatchDTO](divisionbatchdto.md)

  ↳ [UserBasicInfoBatchDTO](userbasicinfobatchdto.md)

  ↳ [UserManagementBatchDTO](usermanagementbatchdto.md)

  ↳ [UnmanagedUsersBatchDTO](unmanagedusersbatchdto.md)

  ↳ [UserAdminBatchDTO](useradminbatchdto.md)

  ↳ [KeywordBatchDTO](keywordbatchdto.md)

  ↳ [FlaggedMessageBatchDTO](flaggedmessagebatchdto.md)

  ↳ [ConversationGroupBatchDTOForUserAdminWithUserCount](conversationgroupbatchdtoforuseradminwithusercount.md)

  ↳ [UserOnboardingJobBatchDTO](useronboardingjobbatchdto.md)

  ↳ [InternalUserContactsBatchDTO](internalusercontactsbatchdto.md)

## Index

### Properties

* [completed](searchpaginateresponse.md#markdown-header-completed)
* [limit](searchpaginateresponse.md#markdown-header-limit)
* [nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)
* [offset](searchpaginateresponse.md#markdown-header-offset)
* [search](searchpaginateresponse.md#markdown-header-search)
* [totalCount](searchpaginateresponse.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

Defined in types.ts:21

___

###  limit

• **limit**: *number*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

Defined in types.ts:18

___

###  search

• **search**: *string | null*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

Defined in types.ts:17
