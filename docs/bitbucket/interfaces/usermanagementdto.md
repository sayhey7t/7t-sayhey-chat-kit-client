[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserManagementDTO](usermanagementdto.md)

# Interface: UserManagementDTO

## Hierarchy

* **UserManagementDTO**

## Index

### Properties

* [currentUserAdmin](usermanagementdto.md#markdown-header-currentuseradmin)
* [currentUserAdminId](usermanagementdto.md#markdown-header-currentuseradminid)
* [id](usermanagementdto.md#markdown-header-id)
* [permanentUserAdmin](usermanagementdto.md#markdown-header-permanentuseradmin)
* [permanentUserAdminId](usermanagementdto.md#markdown-header-permanentuseradminid)
* [user](usermanagementdto.md#markdown-header-user)
* [userId](usermanagementdto.md#markdown-header-userid)

## Properties

###  currentUserAdmin

• **currentUserAdmin**: *[UserAdminDTO](useradmindto.md)*

Defined in types.ts:1896

___

###  currentUserAdminId

• **currentUserAdminId**: *string*

Defined in types.ts:1893

___

###  id

• **id**: *string*

Defined in types.ts:1891

___

###  permanentUserAdmin

• **permanentUserAdmin**: *[UserAdminDTO](useradmindto.md)*

Defined in types.ts:1897

___

###  permanentUserAdminId

• **permanentUserAdminId**: *string*

Defined in types.ts:1894

___

###  user

• **user**: *[InternalUserFullInfoDTO](internaluserfullinfodto.md)*

Defined in types.ts:1895

___

###  userId

• **userId**: *string*

Defined in types.ts:1892
