[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [DivisionDTO](divisiondto.md)

# Interface: DivisionDTO

## Hierarchy

* **DivisionDTO**

## Index

### Properties

* [deletedAt](divisiondto.md#markdown-header-deletedat)
* [departmentId](divisiondto.md#markdown-header-departmentid)
* [id](divisiondto.md#markdown-header-id)
* [isDefault](divisiondto.md#markdown-header-isdefault)
* [name](divisiondto.md#markdown-header-name)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:1826

___

###  departmentId

• **departmentId**: *string*

Defined in types.ts:1825

___

###  id

• **id**: *string*

Defined in types.ts:1823

___

###  isDefault

• **isDefault**: *boolean*

Defined in types.ts:1827

___

###  name

• **name**: *string*

Defined in types.ts:1824
