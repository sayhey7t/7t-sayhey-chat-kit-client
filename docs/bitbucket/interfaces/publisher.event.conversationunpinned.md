[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationUnpinned](publisher.event.conversationunpinned.md)

# Interface: ConversationUnpinned

## Hierarchy

* EventMessage

  ↳ **ConversationUnpinned**

## Index

### Properties

* [event](publisher.event.conversationunpinned.md#markdown-header-event)
* [payload](publisher.event.conversationunpinned.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationUnpinned](../enums/publisher.eventtype.md#markdown-header-conversationunpinned)*

*Overrides void*

Defined in types.ts:895

___

###  payload

• **payload**: *[ConversationUnpinned](publisher.payload.conversationunpinned.md)*

*Overrides void*

Defined in types.ts:896
