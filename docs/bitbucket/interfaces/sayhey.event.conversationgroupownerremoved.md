[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationGroupOwnerRemoved](sayhey.event.conversationgroupownerremoved.md)

# Interface: ConversationGroupOwnerRemoved

## Hierarchy

* **ConversationGroupOwnerRemoved**

## Index

### Properties

* [event](sayhey.event.conversationgroupownerremoved.md#markdown-header-event)
* [payload](sayhey.event.conversationgroupownerremoved.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationGroupOwnerRemoved*

Defined in types.ts:1250

___

###  payload

• **payload**: *[ConversationGroupOwnerRemoved](sayhey.payload.conversationgroupownerremoved.md)*

Defined in types.ts:1251
