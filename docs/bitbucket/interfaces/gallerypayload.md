[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [GalleryPayload](gallerypayload.md)

# Interface: GalleryPayload

## Hierarchy

* **GalleryPayload**

## Index

### Properties

* [messageList](gallerypayload.md#markdown-header-messagelist)
* [nextCursor](gallerypayload.md#markdown-header-nextcursor)

## Properties

###  messageList

• **messageList**: *[MessageDomainEntity](messagedomainentity.md)[]*

Defined in types.ts:486

___

###  nextCursor

• **nextCursor**: *number*

Defined in types.ts:485
