[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [UserReactedToMessage](sayhey.event.userreactedtomessage.md)

# Interface: UserReactedToMessage

## Hierarchy

* EventMessage

  ↳ **UserReactedToMessage**

## Index

### Properties

* [event](sayhey.event.userreactedtomessage.md#markdown-header-event)
* [payload](sayhey.event.userreactedtomessage.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.UserReactedToMessage*

*Overrides void*

Defined in types.ts:1270

___

###  payload

• **payload**: *[UserReactedToMessage](sayhey.payload.userreactedtomessage.md)*

*Overrides void*

Defined in types.ts:1271
