[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [UserDeleted](sayhey.event.userdeleted.md)

# Interface: UserDeleted

## Hierarchy

* EventMessage

  ↳ **UserDeleted**

## Index

### Properties

* [event](sayhey.event.userdeleted.md#markdown-header-event)
* [payload](sayhey.event.userdeleted.md#markdown-header-payload)

## Properties

###  event

• **event**: *[UserDeleted](../enums/publisher.eventtype.md#markdown-header-userdeleted)*

*Overrides void*

Defined in types.ts:1300

___

###  payload

• **payload**: *[UserDeleted](sayhey.payload.userdeleted.md)*

*Overrides void*

Defined in types.ts:1301
