[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageResolutionScanBatchDTO](flaggedmessageresolutionscanbatchdto.md)

# Interface: FlaggedMessageResolutionScanBatchDTO

## Hierarchy

* [PaginateResponse](paginateresponse.md)

  ↳ **FlaggedMessageResolutionScanBatchDTO**

## Index

### Properties

* [completed](flaggedmessageresolutionscanbatchdto.md#markdown-header-completed)
* [limit](flaggedmessageresolutionscanbatchdto.md#markdown-header-limit)
* [nextOffset](flaggedmessageresolutionscanbatchdto.md#markdown-header-nextoffset)
* [offset](flaggedmessageresolutionscanbatchdto.md#markdown-header-offset)
* [results](flaggedmessageresolutionscanbatchdto.md#markdown-header-results)
* [totalCount](flaggedmessageresolutionscanbatchdto.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[completed](flaggedmessageuserreportbatchdto.md#markdown-header-completed)*

Defined in types.ts:2150

___

###  limit

• **limit**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[limit](flaggedmessageuserreportbatchdto.md#markdown-header-limit)*

Defined in types.ts:2149

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[nextOffset](flaggedmessageuserreportbatchdto.md#markdown-header-nextoffset)*

Defined in types.ts:2148

___

###  offset

• **offset**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[offset](flaggedmessageuserreportbatchdto.md#markdown-header-offset)*

Defined in types.ts:2147

___

###  results

• **results**: *[FlaggedMessageScanDTO](flaggedmessagescandto.md)[]*

Defined in types.ts:2308

___

###  totalCount

• **totalCount**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[totalCount](flaggedmessageuserreportbatchdto.md#markdown-header-totalcount)*

Defined in types.ts:2146
