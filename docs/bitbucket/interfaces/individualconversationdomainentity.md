[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [IndividualConversationDomainEntity](individualconversationdomainentity.md)

# Interface: IndividualConversationDomainEntity

## Hierarchy

  ↳ [BaseConversationDomainEntity](baseconversationdomainentity.md)

  ↳ **IndividualConversationDomainEntity**

## Index

### Properties

* [badge](individualconversationdomainentity.md#markdown-header-badge)
* [createdAt](individualconversationdomainentity.md#markdown-header-createdat)
* [deletedFromConversationAt](individualconversationdomainentity.md#markdown-header-deletedfromconversationat)
* [endBefore](individualconversationdomainentity.md#markdown-header-endbefore)
* [lastMessage](individualconversationdomainentity.md#markdown-header-lastmessage)
* [muteUntil](individualconversationdomainentity.md#markdown-header-muteuntil)
* [pinnedAt](individualconversationdomainentity.md#markdown-header-pinnedat)
* [startAfter](individualconversationdomainentity.md#markdown-header-startafter)
* [type](individualconversationdomainentity.md#markdown-header-type)
* [updatedAt](individualconversationdomainentity.md#markdown-header-updatedat)
* [user](individualconversationdomainentity.md#markdown-header-user)

## Properties

###  badge

• **badge**: *number*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[badge](baseconversationdomainentity.md#markdown-header-badge)*

Defined in types.ts:338

___

###  createdAt

• **createdAt**: *Date*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[createdAt](baseconversationdomainentity.md#markdown-header-createdat)*

Defined in types.ts:342

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[deletedFromConversationAt](baseconversationdomainentity.md#markdown-header-deletedfromconversationat)*

Defined in types.ts:341

___

###  endBefore

• **endBefore**: *Date | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[endBefore](baseconversationdomainentity.md#markdown-header-endbefore)*

Defined in types.ts:340

___

###  lastMessage

• **lastMessage**: *[MessageDomainEntity](messagedomainentity.md) | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[lastMessage](baseconversationdomainentity.md#markdown-header-lastmessage)*

Defined in types.ts:344

___

###  muteUntil

• **muteUntil**: *Date | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[muteUntil](baseconversationdomainentity.md#markdown-header-muteuntil)*

Defined in types.ts:336

___

###  pinnedAt

• **pinnedAt**: *Date | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[pinnedAt](baseconversationdomainentity.md#markdown-header-pinnedat)*

Defined in types.ts:337

___

###  startAfter

• **startAfter**: *Date | null*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[startAfter](baseconversationdomainentity.md#markdown-header-startafter)*

Defined in types.ts:339

___

###  type

• **type**: *[Individual](../enums/conversationtype.md#markdown-header-individual)*

Defined in types.ts:353

___

###  updatedAt

• **updatedAt**: *Date*

*Inherited from [BaseConversationDomainEntity](baseconversationdomainentity.md).[updatedAt](baseconversationdomainentity.md#markdown-header-updatedat)*

Defined in types.ts:343

___

###  user

• **user**: *[UserConversationInfoDomainEntity](../globals.md#markdown-header-userconversationinfodomainentity)*

Defined in types.ts:354
