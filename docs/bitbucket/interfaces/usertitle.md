[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserTitle](usertitle.md)

# Interface: UserTitle

## Hierarchy

* **UserTitle**

## Index

### Properties

* [title](usertitle.md#markdown-header-title)

## Properties

###  title

• **title**: *string | null*

Defined in types.ts:635
