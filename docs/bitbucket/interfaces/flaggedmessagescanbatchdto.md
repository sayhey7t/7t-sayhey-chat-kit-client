[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageScanBatchDTO](flaggedmessagescanbatchdto.md)

# Interface: FlaggedMessageScanBatchDTO

## Hierarchy

  ↳ [FlaggedMessageScanPaginateResponse](flaggedmessagescanpaginateresponse.md)

  ↳ **FlaggedMessageScanBatchDTO**

## Index

### Properties

* [completed](flaggedmessagescanbatchdto.md#markdown-header-completed)
* [limit](flaggedmessagescanbatchdto.md#markdown-header-limit)
* [nextOffset](flaggedmessagescanbatchdto.md#markdown-header-nextoffset)
* [offset](flaggedmessagescanbatchdto.md#markdown-header-offset)
* [resolved](flaggedmessagescanbatchdto.md#markdown-header-resolved)
* [results](flaggedmessagescanbatchdto.md#markdown-header-results)
* [totalCount](flaggedmessagescanbatchdto.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[completed](flaggedmessageuserreportbatchdto.md#markdown-header-completed)*

Defined in types.ts:2150

___

###  limit

• **limit**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[limit](flaggedmessageuserreportbatchdto.md#markdown-header-limit)*

Defined in types.ts:2149

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[nextOffset](flaggedmessageuserreportbatchdto.md#markdown-header-nextoffset)*

Defined in types.ts:2148

___

###  offset

• **offset**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[offset](flaggedmessageuserreportbatchdto.md#markdown-header-offset)*

Defined in types.ts:2147

___

###  resolved

• **resolved**: *boolean*

*Inherited from [FlaggedMessageScanBatchDTO](flaggedmessagescanbatchdto.md).[resolved](flaggedmessagescanbatchdto.md#markdown-header-resolved)*

Defined in types.ts:2275

___

###  results

• **results**: *[FlaggedMessageScanDTO](flaggedmessagescandto.md)[]*

Defined in types.ts:2271

___

###  totalCount

• **totalCount**: *number*

*Inherited from [FlaggedMessageUserReportBatchDTO](flaggedmessageuserreportbatchdto.md).[totalCount](flaggedmessageuserreportbatchdto.md#markdown-header-totalcount)*

Defined in types.ts:2146
