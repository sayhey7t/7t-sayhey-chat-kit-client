[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [AdditionalUserOfConversation](additionaluserofconversation.md)

# Interface: AdditionalUserOfConversation

## Hierarchy

* **AdditionalUserOfConversation**

## Index

### Properties

* [deletedFromConversationAt](additionaluserofconversation.md#markdown-header-deletedfromconversationat)
* [isOwner](additionaluserofconversation.md#markdown-header-isowner)

## Properties

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

Defined in types.ts:613

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:614
