[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [UserDisabled](publisher.payload.userdisabled.md)

# Interface: UserDisabled

## Hierarchy

* **UserDisabled**

## Index

### Properties

* [disabled](publisher.payload.userdisabled.md#markdown-header-disabled)
* [userId](publisher.payload.userdisabled.md#markdown-header-userid)

## Properties

###  disabled

• **disabled**: *true*

Defined in types.ts:845

___

###  userId

• **userId**: *string*

Defined in types.ts:844
