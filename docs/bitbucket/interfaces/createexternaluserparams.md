[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [CreateExternalUserParams](createexternaluserparams.md)

# Interface: CreateExternalUserParams

## Hierarchy

* **CreateExternalUserParams**

## Index

### Properties

* [userId](createexternaluserparams.md#markdown-header-userid)

## Properties

###  userId

• **userId**: *string*

Defined in types.ts:2407
