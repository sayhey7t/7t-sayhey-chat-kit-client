[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [TotalUsersCountDTO](totaluserscountdto.md)

# Interface: TotalUsersCountDTO

## Hierarchy

* **TotalUsersCountDTO**

## Index

### Properties

* [activeUsersCount](totaluserscountdto.md#markdown-header-activeuserscount)
* [inActiveUsersCount](totaluserscountdto.md#markdown-header-inactiveuserscount)
* [totalUsersCount](totaluserscountdto.md#markdown-header-totaluserscount)

## Properties

###  activeUsersCount

• **activeUsersCount**: *number*

Defined in types.ts:2348

___

###  inActiveUsersCount

• **inActiveUsersCount**: *number*

Defined in types.ts:2349

___

###  totalUsersCount

• **totalUsersCount**: *number*

Defined in types.ts:2350
