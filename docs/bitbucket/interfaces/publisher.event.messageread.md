[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [MessageRead](publisher.event.messageread.md)

# Interface: MessageRead

## Hierarchy

* EventMessage

  ↳ **MessageRead**

## Index

### Properties

* [event](publisher.event.messageread.md#markdown-header-event)
* [payload](publisher.event.messageread.md#markdown-header-payload)

## Properties

###  event

• **event**: *[MessageRead](../enums/publisher.eventtype.md#markdown-header-messageread)*

*Overrides void*

Defined in types.ts:995

___

###  payload

• **payload**: *[MessageRead](publisher.payload.messageread.md)*

*Overrides void*

Defined in types.ts:996
