[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ExternalUserNudged](publisher.payload.externalusernudged.md)

# Interface: ExternalUserNudged

## Hierarchy

* **ExternalUserNudged**

## Index

### Properties

* [conversationId](publisher.payload.externalusernudged.md#markdown-header-conversationid)
* [messageDomain](publisher.payload.externalusernudged.md#markdown-header-messagedomain)
* [messageDto](publisher.payload.externalusernudged.md#markdown-header-messagedto)
* [updateData](publisher.payload.externalusernudged.md#markdown-header-updatedata)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:871

___

###  messageDomain

• **messageDomain**: *[MessageDomainEntity](messagedomainentity.md)*

Defined in types.ts:872

___

###  messageDto

• **messageDto**: *[MessageDTO](messagedto.md)*

Defined in types.ts:873

___

###  updateData

• **updateData**: *object*

Defined in types.ts:874

#### Type declaration:

* **lastUnreadNudgedAt**: *Date*
