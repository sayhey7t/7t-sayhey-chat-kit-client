[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationUnmuted](publisher.event.conversationunmuted.md)

# Interface: ConversationUnmuted

## Hierarchy

* EventMessage

  ↳ **ConversationUnmuted**

## Index

### Properties

* [event](publisher.event.conversationunmuted.md#markdown-header-event)
* [payload](publisher.event.conversationunmuted.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationUnmuted](../enums/publisher.eventtype.md#markdown-header-conversationunmuted)*

*Overrides void*

Defined in types.ts:890

___

###  payload

• **payload**: *[ConversationUnmuted](publisher.payload.conversationunmuted.md)*

*Overrides void*

Defined in types.ts:891
