[@7t/sayhey-chat-kit-client](README.md) › [Globals](globals.md)

# @7t/sayhey-chat-kit-client

## Index

### Namespaces

* [Publisher](modules/publisher.md)
* [SayHey](modules/sayhey.md)

### Enumerations

* [APISignature](enums/apisignature.md)
* [ConversationGroupType](enums/conversationgrouptype.md)
* [ConversationType](enums/conversationtype.md)
* [ConversationUserQueryType](enums/conversationuserquerytype.md)
* [DepartmentSortFields](enums/departmentsortfields.md)
* [DivisionSortFields](enums/divisionsortfields.md)
* [FileType](enums/filetype.md)
* [FlaggedMessagePriorityType](enums/flaggedmessageprioritytype.md)
* [FlaggedMessageType](enums/flaggedmessagetype.md)
* [HighLightType](enums/highlighttype.md)
* [HttpMethod](enums/httpmethod.md)
* [ImageSizes](enums/imagesizes.md)
* [KeyWordPriorityTypes](enums/keywordprioritytypes.md)
* [MessageType](enums/messagetype.md)
* [QueryCondition](enums/querycondition.md)
* [ReactionType](enums/reactiontype.md)
* [ResolutionStatusType](enums/resolutionstatustype.md)
* [SortOrderType](enums/sortordertype.md)
* [SystemMessageType](enums/systemmessagetype.md)
* [TimeFrameType](enums/timeframetype.md)
* [TokenType](enums/tokentype.md)
* [UnmanagedUsersSortFields](enums/unmanageduserssortfields.md)
* [UnresolvedMsgScopeType](enums/unresolvedmsgscopetype.md)
* [UserAdminSortFields](enums/useradminsortfields.md)
* [UserAdminStatusType](enums/useradminstatustype.md)
* [UserManagementSortFields](enums/usermanagementsortfields.md)
* [UserManagementType](enums/usermanagementtype.md)
* [UserOnboardingJobRecordStatus](enums/useronboardingjobrecordstatus.md)
* [UserOnboardingJobStatus](enums/useronboardingjobstatus.md)
* [UserScope](enums/userscope.md)
* [UserStatusType](enums/userstatustype.md)
* [UserTypes](enums/usertypes.md)
* [tempDefaultColumn](enums/tempdefaultcolumn.md)

### Classes

* [AuditMessageMapper](classes/auditmessagemapper.md)
* [ChatAdminKit](classes/chatadminkit.md)
* [ChatClientKit](classes/chatclientkit.md)
* [ConversationMapper](classes/conversationmapper.md)
* [MediaMapper](classes/mediamapper.md)
* [MessageMapper](classes/messagemapper.md)
* [ReactionMapper](classes/reactionmapper.md)
* [ReactionsObject](classes/reactionsobject.md)
* [SentMessageMapper](classes/sentmessagemapper.md)
* [SystemMessageMapper](classes/systemmessagemapper.md)
* [UserMapper](classes/usermapper.md)
* [UserMovementSelected](classes/usermovementselected.md)

### Interfaces

* [AdditionalUserOfConversation](interfaces/additionaluserofconversation.md)
* [AdminAuthResponse](interfaces/adminauthresponse.md)
* [AllFlaggedMessageCountDailyDTO](interfaces/allflaggedmessagecountdailydto.md)
* [AppConfigDTO](interfaces/appconfigdto.md)
* [AuditMessageBatchDTO](interfaces/auditmessagebatchdto.md)
* [AuditMessageDTO](interfaces/auditmessagedto.md)
* [AuditMessageDomainEntityBatchDTO](interfaces/auditmessagedomainentitybatchdto.md)
* [AuditMessageDomainEntityDTO](interfaces/auditmessagedomainentitydto.md)
* [AuthDTO](interfaces/authdto.md)
* [AuthResponse](interfaces/authresponse.md)
* [BaseConversation](interfaces/baseconversation.md)
* [BaseConversationDTO](interfaces/baseconversationdto.md)
* [BaseConversationDomainEntity](interfaces/baseconversationdomainentity.md)
* [BaseMedia](interfaces/basemedia.md)
* [BaseMessage](interfaces/basemessage.md)
* [ConversableUsersBatchDTO](interfaces/conversableusersbatchdto.md)
* [ConversableUsersBatchMeta](interfaces/conversableusersbatchmeta.md)
* [ConversableUsersBatchResult](interfaces/conversableusersbatchresult.md)
* [ConversableUsersForUserAdminBatchDTO](interfaces/conversableusersforuseradminbatchdto.md)
* [ConversableUsersForUserAdminBatchResult](interfaces/conversableusersforuseradminbatchresult.md)
* [ConversationConversableUsersBatch](interfaces/conversationconversableusersbatch.md)
* [ConversationConversableUsersBatchDTO](interfaces/conversationconversableusersbatchdto.md)
* [ConversationConversableUsersBatchMeta](interfaces/conversationconversableusersbatchmeta.md)
* [ConversationDTOForUserAdmin](interfaces/conversationdtoforuseradmin.md)
* [ConversationGroupBatchDTOForUserAdminWithUserCount](interfaces/conversationgroupbatchdtoforuseradminwithusercount.md)
* [ConversationGroupDTOForUserAdmin](interfaces/conversationgroupdtoforuseradmin.md)
* [ConversationGroupDTOForUserAdminWithUserCount](interfaces/conversationgroupdtoforuseradminwithusercount.md)
* [ConversationGroupInfoDTO](interfaces/conversationgroupinfodto.md)
* [ConversationGroupInfoDomainEntity](interfaces/conversationgroupinfodomainentity.md)
* [ConversationInfoDomainEntityExternal](interfaces/conversationinfodomainentityexternal.md)
* [ConversationInfoDomainEntityInternal](interfaces/conversationinfodomainentityinternal.md)
* [ConversationReachableUsersQuery](interfaces/conversationreachableusersquery.md)
* [ConversationReactionsDTO](interfaces/conversationreactionsdto.md)
* [ConversationUserQuery](interfaces/conversationuserquery.md)
* [ConversationUsersBasicDTO](interfaces/conversationusersbasicdto.md)
* [ConversationUsersBatch](interfaces/conversationusersbatch.md)
* [ConversationUsersBatchDTO](interfaces/conversationusersbatchdto.md)
* [ConversationUsersBatchForUserAdminDTO](interfaces/conversationusersbatchforuseradmindto.md)
* [ConversationUsersBatchMeta](interfaces/conversationusersbatchmeta.md)
* [ConversationUsersForUserAdminBatch](interfaces/conversationusersforuseradminbatch.md)
* [CreateExternalUserParams](interfaces/createexternaluserparams.md)
* [CreateNewContact](interfaces/createnewcontact.md)
* [DefaultGroupImageUrlSet](interfaces/defaultgroupimageurlset.md)
* [DepartmentBatchDTO](interfaces/departmentbatchdto.md)
* [DepartmentDTO](interfaces/departmentdto.md)
* [DepartmentDivisionDTO](interfaces/departmentdivisiondto.md)
* [DepartmentWithDivisionCountDTO](interfaces/departmentwithdivisioncountdto.md)
* [DivisionBatchDTO](interfaces/divisionbatchdto.md)
* [DivisionDTO](interfaces/divisiondto.md)
* [ExternalUserContactDTO](interfaces/externalusercontactdto.md)
* [ExternalUserOptToReceiveSmsDTO](interfaces/externaluseropttoreceivesmsdto.md)
* [ExternalUserTermsAndPrivacyVersionDTO](interfaces/externalusertermsandprivacyversiondto.md)
* [FindByPhoneQueryParams](interfaces/findbyphonequeryparams.md)
* [FindSmsConversation](interfaces/findsmsconversation.md)
* [FlaggedInfoEntity](interfaces/flaggedinfoentity.md)
* [FlaggedMessageBatchDTO](interfaces/flaggedmessagebatchdto.md)
* [FlaggedMessageCount](interfaces/flaggedmessagecount.md)
* [FlaggedMessageCountDTO](interfaces/flaggedmessagecountdto.md)
* [FlaggedMessageCountDailyDTO](interfaces/flaggedmessagecountdailydto.md)
* [FlaggedMessageDTO](interfaces/flaggedmessagedto.md)
* [FlaggedMessageDetailsDTO](interfaces/flaggedmessagedetailsdto.md)
* [FlaggedMessageFlagTypeCount](interfaces/flaggedmessageflagtypecount.md)
* [FlaggedMessageMinimalDomain](interfaces/flaggedmessageminimaldomain.md)
* [FlaggedMessageResolutionBatchDTO](interfaces/flaggedmessageresolutionbatchdto.md)
* [FlaggedMessageResolutionDTO](interfaces/flaggedmessageresolutiondto.md)
* [FlaggedMessageResolutionScanBatchDTO](interfaces/flaggedmessageresolutionscanbatchdto.md)
* [FlaggedMessageResolutionUserReportBatchDTO](interfaces/flaggedmessageresolutionuserreportbatchdto.md)
* [FlaggedMessageScanBatchDTO](interfaces/flaggedmessagescanbatchdto.md)
* [FlaggedMessageScanDTO](interfaces/flaggedmessagescandto.md)
* [FlaggedMessageScanMinimalDomain](interfaces/flaggedmessagescanminimaldomain.md)
* [FlaggedMessageScanPaginateResponse](interfaces/flaggedmessagescanpaginateresponse.md)
* [FlaggedMessageUserReportBatchDTO](interfaces/flaggedmessageuserreportbatchdto.md)
* [FlaggedMessageUserReportDTO](interfaces/flaggedmessageuserreportdto.md)
* [FlaggedMessageUserReportPaginateResponse](interfaces/flaggedmessageuserreportpaginateresponse.md)
* [GalleryPayload](interfaces/gallerypayload.md)
* [GroupConversationDTO](interfaces/groupconversationdto.md)
* [GroupConversationDomainEntity](interfaces/groupconversationdomainentity.md)
* [GroupDisplayDetail](interfaces/groupdisplaydetail.md)
* [ImageInfoMedia](interfaces/imageinfomedia.md)
* [ImageMetadataDomain](interfaces/imagemetadatadomain.md)
* [IndividualConversationDTO](interfaces/individualconversationdto.md)
* [IndividualConversationDomainEntity](interfaces/individualconversationdomainentity.md)
* [InternalUserContactDTO](interfaces/internalusercontactdto.md)
* [InternalUserContactsBatchDTO](interfaces/internalusercontactsbatchdto.md)
* [InternalUserFullInfoDTO](interfaces/internaluserfullinfodto.md)
* [KeywordBatchDTO](interfaces/keywordbatchdto.md)
* [KeywordDTO](interfaces/keyworddto.md)
* [MediaDTO](interfaces/mediadto.md)
* [MediaDomain](interfaces/mediadomain.md)
* [MediaDomainEntity](interfaces/mediadomainentity.md)
* [MessageDTO](interfaces/messagedto.md)
* [MessageDomainEntity](interfaces/messagedomainentity.md)
* [PaginateResponse](interfaces/paginateresponse.md)
* [PeekMessageDTO](interfaces/peekmessagedto.md)
* [PeekMessageDomainEntity](interfaces/peekmessagedomainentity.md)
* [PhoneWithCode](interfaces/phonewithcode.md)
* [ReachableUsersQuery](interfaces/reachableusersquery.md)
* [ReactedUserDTO](interfaces/reacteduserdto.md)
* [Reaction](interfaces/reaction.md)
* [SearchPaginateParams](interfaces/searchpaginateparams.md)
* [SearchPaginateResponse](interfaces/searchpaginateresponse.md)
* [SendEmojiMessageParams](interfaces/sendemojimessageparams.md)
* [SendFileMessageParams](interfaces/sendfilemessageparams.md)
* [SendGifMessageParams](interfaces/sendgifmessageparams.md)
* [SendMessageParamsBase](interfaces/sendmessageparamsbase.md)
* [SendTextMessageParams](interfaces/sendtextmessageparams.md)
* [SentMessageCountDailyDTO](interfaces/sentmessagecountdailydto.md)
* [SmsConversationInvitationDTO](interfaces/smsconversationinvitationdto.md)
* [SmsConversationInvitationUserDTO](interfaces/smsconversationinvitationuserdto.md)
* [SubImageInfoMedia](interfaces/subimageinfomedia.md)
* [SubImageMinimal](interfaces/subimageminimal.md)
* [SystemMessageBase](interfaces/systemmessagebase.md)
* [TotalUsersCountDTO](interfaces/totaluserscountdto.md)
* [UnmanagedUsersBatchDTO](interfaces/unmanagedusersbatchdto.md)
* [UnmanagedUsersCountDTO](interfaces/unmanageduserscountdto.md)
* [UnresolvedFlaggedMessagesCountDTO](interfaces/unresolvedflaggedmessagescountdto.md)
* [UserAdminBatchDTO](interfaces/useradminbatchdto.md)
* [UserAdminDTO](interfaces/useradmindto.md)
* [UserAdminSearchDTO](interfaces/useradminsearchdto.md)
* [UserBasicInfoBatchDTO](interfaces/userbasicinfobatchdto.md)
* [UserBasicInfoCommon](interfaces/userbasicinfocommon.md)
* [UserBasicInfoDTOExternal](interfaces/userbasicinfodtoexternal.md)
* [UserBasicInfoDTOInternal](interfaces/userbasicinfodtointernal.md)
* [UserCommonDTO](interfaces/usercommondto.md)
* [UserDepartmentAndDivision](interfaces/userdepartmentanddivision.md)
* [UserDomainEntityBase](interfaces/userdomainentitybase.md)
* [UserDomainEntityExternal](interfaces/userdomainentityexternal.md)
* [UserDomainEntityInternal](interfaces/userdomainentityinternal.md)
* [UserExternalDTO](interfaces/userexternaldto.md)
* [UserInternalDTO](interfaces/userinternaldto.md)
* [UserInternalForUserAdminDTO](interfaces/userinternalforuseradmindto.md)
* [UserManagementBatchDTO](interfaces/usermanagementbatchdto.md)
* [UserManagementCountQueryParams](interfaces/usermanagementcountqueryparams.md)
* [UserManagementCountResultParams](interfaces/usermanagementcountresultparams.md)
* [UserManagementDTO](interfaces/usermanagementdto.md)
* [UserMessageDTO](interfaces/usermessagedto.md)
* [UserMinimalCommon](interfaces/userminimalcommon.md)
* [UserMinimalExternal](interfaces/userminimalexternal.md)
* [UserMinimalInternal](interfaces/userminimalinternal.md)
* [UserOnboardingJobBatchDTO](interfaces/useronboardingjobbatchdto.md)
* [UserOnboardingJobDTO](interfaces/useronboardingjobdto.md)
* [UserOnboardingJobMinDTO](interfaces/useronboardingjobmindto.md)
* [UserOnboardingJobRecordMinDTO](interfaces/useronboardingjobrecordmindto.md)
* [UserRole](interfaces/userrole.md)
* [UserSearchCriteria](interfaces/usersearchcriteria.md)
* [UserSearchPaginateResponse](interfaces/usersearchpaginateresponse.md)
* [UserSimpleInfo](interfaces/usersimpleinfo.md)
* [UserTitle](interfaces/usertitle.md)
* [ValidateResetPasswordOTPDTO](interfaces/validateresetpasswordotpdto.md)
* [ValidateSetPasswordOTPDTO](interfaces/validatesetpasswordotpdto.md)

### Type aliases

* [AuditMessagSearchPaginateResponse](globals.md#markdown-header-auditmessagsearchpaginateresponse)
* [ConversationDTO](globals.md#markdown-header-conversationdto)
* [ConversationDomainEntity](globals.md#markdown-header-conversationdomainentity)
* [ConversationInfoDTO](globals.md#markdown-header-conversationinfodto)
* [ConversationInfoDomainEntity](globals.md#markdown-header-conversationinfodomainentity)
* [ConversationInfoExternalDTO](globals.md#markdown-header-conversationinfoexternaldto)
* [ConversationInfoInternalDTO](globals.md#markdown-header-conversationinfointernaldto)
* [ConversationReactionsDomainEntity](globals.md#markdown-header-conversationreactionsdomainentity)
* [ConversationSearchPaginateResponse](globals.md#markdown-header-conversationsearchpaginateresponse)
* [ConversationUserDomainEntity](globals.md#markdown-header-conversationuserdomainentity)
* [ConversationUserDomainForUserAdminEntity](globals.md#markdown-header-conversationuserdomainforuseradminentity)
* [DepartmentSearchPaginateParams](globals.md#markdown-header-departmentsearchpaginateparams)
* [DepartmentSearchPaginateResponse](globals.md#markdown-header-departmentsearchpaginateresponse)
* [DivisionSearchPaginateParams](globals.md#markdown-header-divisionsearchpaginateparams)
* [DivisionSearchPaginateResponse](globals.md#markdown-header-divisionsearchpaginateresponse)
* [FileUpload](globals.md#markdown-header-fileupload)
* [FileWithText](globals.md#markdown-header-filewithtext)
* [FlaggedMessageResolutionPaginateResponse](globals.md#markdown-header-flaggedmessageresolutionpaginateresponse)
* [FlaggedMessageSearchPaginateResponse](globals.md#markdown-header-flaggedmessagesearchpaginateresponse)
* [InternalUserContactsSearchPaginateResponse](globals.md#markdown-header-internalusercontactssearchpaginateresponse)
* [KeywordSearchPaginateResponse](globals.md#markdown-header-keywordsearchpaginateresponse)
* [ParsedLine](globals.md#markdown-header-parsedline)
* [SendMessageParams](globals.md#markdown-header-sendmessageparams)
* [SentMessageDTO](globals.md#markdown-header-sentmessagedto)
* [SentMessageDomainEntity](globals.md#markdown-header-sentmessagedomainentity)
* [SystemMessageDTO](globals.md#markdown-header-systemmessagedto)
* [SystemMessageDomainEntity](globals.md#markdown-header-systemmessagedomainentity)
* [UnmanagedUsersSearchPaginateResponse](globals.md#markdown-header-unmanageduserssearchpaginateresponse)
* [UserAdminSearchPaginateResponse](globals.md#markdown-header-useradminsearchpaginateresponse)
* [UserBasicInfoDTO](globals.md#markdown-header-userbasicinfodto)
* [UserBasicInfoSearchPaginateResponse](globals.md#markdown-header-userbasicinfosearchpaginateresponse)
* [UserConversationInfoDTO](globals.md#markdown-header-userconversationinfodto)
* [UserConversationInfoDomainEntity](globals.md#markdown-header-userconversationinfodomainentity)
* [UserDTO](globals.md#markdown-header-userdto)
* [UserDomainEntity](globals.md#markdown-header-userdomainentity)
* [UserExternalForUserAdminDTO](globals.md#markdown-header-userexternalforuseradmindto)
* [UserForUserAdminDTO](globals.md#markdown-header-userforuseradmindto)
* [UserForUserAdminDomainEntity](globals.md#markdown-header-userforuseradmindomainentity)
* [UserForUserAdminDomainEntityExternal](globals.md#markdown-header-userforuseradmindomainentityexternal)
* [UserForUserAdminDomainEntityInternal](globals.md#markdown-header-userforuseradmindomainentityinternal)
* [UserFullInfoDTO](globals.md#markdown-header-userfullinfodto)
* [UserManagementSearchPaginateResponse](globals.md#markdown-header-usermanagementsearchpaginateresponse)
* [UserMinimal](globals.md#markdown-header-userminimal)
* [UserOfConversationDTO](globals.md#markdown-header-userofconversationdto)
* [UserOfConversationForUserAdminDTO](globals.md#markdown-header-userofconversationforuseradmindto)
* [UserOnboardingJobSearchPaginateResponse](globals.md#markdown-header-useronboardingjobsearchpaginateresponse)
* [UserSelfDTO](globals.md#markdown-header-userselfdto)
* [UserSelfDomainEntity](globals.md#markdown-header-userselfdomainentity)

### Variables

* [API_VERSION](globals.md#markdown-header-const-api_version)
* [ApiKeyName](globals.md#markdown-header-const-apikeyname)
* [MESSAGE_READ_UPDATE_INTERVAL](globals.md#markdown-header-const-message_read_update_interval)
* [emailRegex](globals.md#markdown-header-const-emailregex)
* [emojiRegex](globals.md#markdown-header-const-emojiregex)
* [phoneRegex](globals.md#markdown-header-const-phoneregex)
* [textHighlightRegex](globals.md#markdown-header-const-texthighlightregex)
* [urlRegex](globals.md#markdown-header-const-urlregex)
* [userIdRegex](globals.md#markdown-header-const-useridregex)

### Functions

* [checkApiKey](globals.md#markdown-header-checkapikey)
* [checkaccessToken](globals.md#markdown-header-checkaccesstoken)
* [isEmail](globals.md#markdown-header-const-isemail)
* [isPhone](globals.md#markdown-header-const-isphone)
* [isSingleEmoji](globals.md#markdown-header-const-issingleemoji)
* [isUrl](globals.md#markdown-header-const-isurl)
* [isUser](globals.md#markdown-header-const-isuser)
* [parseText](globals.md#markdown-header-const-parsetext)
* [requestFailed](globals.md#markdown-header-requestfailed)

### Object literals

* [ChatAdminRoutes](globals.md#markdown-header-const-chatadminroutes)
* [ChatClientRoutes](globals.md#markdown-header-const-chatclientroutes)
* [ChatCommonRoutes](globals.md#markdown-header-const-chatcommonroutes)
* [mimeTypes](globals.md#markdown-header-const-mimetypes)

## Type aliases

###  AuditMessagSearchPaginateResponse

Ƭ **AuditMessagSearchPaginateResponse**: *Omit‹[SearchPaginateResponse](interfaces/searchpaginateresponse.md), "search"›*

Defined in types.ts:2033

___

###  ConversationDTO

Ƭ **ConversationDTO**: *[IndividualConversationDTO](interfaces/individualconversationdto.md) | [GroupConversationDTO](interfaces/groupconversationdto.md)*

Defined in types.ts:320

___

###  ConversationDomainEntity

Ƭ **ConversationDomainEntity**: *[GroupConversationDomainEntity](interfaces/groupconversationdomainentity.md) | [IndividualConversationDomainEntity](interfaces/individualconversationdomainentity.md)*

Defined in types.ts:357

___

###  ConversationInfoDTO

Ƭ **ConversationInfoDTO**: *[ConversationInfoDomainEntityInternal](interfaces/conversationinfodomainentityinternal.md) | [ConversationInfoDomainEntityExternal](interfaces/conversationinfodomainentityexternal.md) & object*

Defined in types.ts:238

___

###  ConversationInfoDomainEntity

Ƭ **ConversationInfoDomainEntity**: *[ConversationInfoDomainEntityInternal](interfaces/conversationinfodomainentityinternal.md) | [ConversationInfoDomainEntityExternal](interfaces/conversationinfodomainentityexternal.md) & object*

Defined in types.ts:264

___

###  ConversationInfoExternalDTO

Ƭ **ConversationInfoExternalDTO**: *[ConversationInfoDomainEntityExternal](interfaces/conversationinfodomainentityexternal.md)*

Defined in types.ts:257

___

###  ConversationInfoInternalDTO

Ƭ **ConversationInfoInternalDTO**: *[ConversationInfoDomainEntityInternal](interfaces/conversationinfodomainentityinternal.md)*

Defined in types.ts:255

___

###  ConversationReactionsDomainEntity

Ƭ **ConversationReactionsDomainEntity**: *Partial‹object›*

Defined in types.ts:1747

___

###  ConversationSearchPaginateResponse

Ƭ **ConversationSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:2319

___

###  ConversationUserDomainEntity

Ƭ **ConversationUserDomainEntity**: *[UserDomainEntity](globals.md#markdown-header-userdomainentity) & [UserTitle](interfaces/usertitle.md) & object*

Defined in types.ts:660

___

###  ConversationUserDomainForUserAdminEntity

Ƭ **ConversationUserDomainForUserAdminEntity**: *[UserDomainEntity](globals.md#markdown-header-userdomainentity) & [UserTitle](interfaces/usertitle.md) & [UserDepartmentAndDivision](interfaces/userdepartmentanddivision.md) & object*

Defined in types.ts:666

___

###  DepartmentSearchPaginateParams

Ƭ **DepartmentSearchPaginateParams**: *[SearchPaginateParams](interfaces/searchpaginateparams.md)*

Defined in types.ts:1785

___

###  DepartmentSearchPaginateResponse

Ƭ **DepartmentSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:1786

___

###  DivisionSearchPaginateParams

Ƭ **DivisionSearchPaginateParams**: *[SearchPaginateParams](interfaces/searchpaginateparams.md)*

Defined in types.ts:1788

___

###  DivisionSearchPaginateResponse

Ƭ **DivisionSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:1789

___

###  FileUpload

Ƭ **FileUpload**: *object*

Defined in types.ts:468

#### Type declaration:

* **fileName**: *string*

* **path**: *string | Blob*

___

###  FileWithText

Ƭ **FileWithText**: *object*

Defined in types.ts:473

#### Type declaration:

* **fileName**: *string*

* **text**? : *undefined | string*

* **thumbnail**? : *undefined | object*

* **type**: *[MessageType](enums/messagetype.md)*

* **uri**: *string | Blob*

___

###  FlaggedMessageResolutionPaginateResponse

Ƭ **FlaggedMessageResolutionPaginateResponse**: *[PaginateResponse](interfaces/paginateresponse.md)*

Defined in types.ts:2278

___

###  FlaggedMessageSearchPaginateResponse

Ƭ **FlaggedMessageSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:2118

___

###  InternalUserContactsSearchPaginateResponse

Ƭ **InternalUserContactsSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:2422

___

###  KeywordSearchPaginateResponse

Ƭ **KeywordSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:2012

___

###  ParsedLine

Ƭ **ParsedLine**: *object[]*

Defined in types.ts:183

___

###  SendMessageParams

Ƭ **SendMessageParams**: *[SendTextMessageParams](interfaces/sendtextmessageparams.md) | [SendEmojiMessageParams](interfaces/sendemojimessageparams.md) | [SendGifMessageParams](interfaces/sendgifmessageparams.md) | [SendFileMessageParams](interfaces/sendfilemessageparams.md)*

Defined in types.ts:1714

___

###  SentMessageDTO

Ƭ **SentMessageDTO**: *Omit‹[MessageDTO](interfaces/messagedto.md), "systemMessage"›*

Defined in types.ts:2043

___

###  SentMessageDomainEntity

Ƭ **SentMessageDomainEntity**: *Omit‹[MessageDomainEntity](interfaces/messagedomainentity.md), "systemMessage"›*

Defined in types.ts:214

___

###  SystemMessageDTO

Ƭ **SystemMessageDTO**: *[SystemMessageBase](interfaces/systemmessagebase.md)*

Defined in types.ts:154

___

###  SystemMessageDomainEntity

Ƭ **SystemMessageDomainEntity**: *[SystemMessageBase](interfaces/systemmessagebase.md)*

Defined in types.ts:156

___

###  UnmanagedUsersSearchPaginateResponse

Ƭ **UnmanagedUsersSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:528

___

###  UserAdminSearchPaginateResponse

Ƭ **UserAdminSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:530

___

###  UserBasicInfoDTO

Ƭ **UserBasicInfoDTO**: *[UserBasicInfoDTOInternal](interfaces/userbasicinfodtointernal.md) | [UserBasicInfoDTOExternal](interfaces/userbasicinfodtoexternal.md)*

Defined in types.ts:1882

___

###  UserBasicInfoSearchPaginateResponse

Ƭ **UserBasicInfoSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:1884

___

###  UserConversationInfoDTO

Ƭ **UserConversationInfoDTO**: *[ConversationInfoDTO](globals.md#markdown-header-conversationinfodto) & object*

Defined in types.ts:274

___

###  UserConversationInfoDomainEntity

Ƭ **UserConversationInfoDomainEntity**: *[ConversationInfoDomainEntity](globals.md#markdown-header-conversationinfodomainentity) & object*

Defined in types.ts:279

___

###  UserDTO

Ƭ **UserDTO**: *[UserInternalDTO](interfaces/userinternaldto.md) | [UserExternalDTO](interfaces/userexternaldto.md)*

Defined in types.ts:412

___

###  UserDomainEntity

Ƭ **UserDomainEntity**: *[UserDomainEntityInternal](interfaces/userdomainentityinternal.md) | [UserDomainEntityExternal](interfaces/userdomainentityexternal.md)*

Defined in types.ts:438

___

###  UserExternalForUserAdminDTO

Ƭ **UserExternalForUserAdminDTO**: *[UserExternalDTO](interfaces/userexternaldto.md)*

Defined in types.ts:423

___

###  UserForUserAdminDTO

Ƭ **UserForUserAdminDTO**: *[UserInternalForUserAdminDTO](interfaces/userinternalforuseradmindto.md) | [UserExternalForUserAdminDTO](globals.md#markdown-header-userexternalforuseradmindto)*

Defined in types.ts:425

___

###  UserForUserAdminDomainEntity

Ƭ **UserForUserAdminDomainEntity**: *[UserForUserAdminDomainEntityInternal](globals.md#markdown-header-userforuseradmindomainentityinternal) | [UserForUserAdminDomainEntityExternal](globals.md#markdown-header-userforuseradmindomainentityexternal)*

Defined in types.ts:451

___

###  UserForUserAdminDomainEntityExternal

Ƭ **UserForUserAdminDomainEntityExternal**: *[UserDomainEntityExternal](interfaces/userdomainentityexternal.md)*

Defined in types.ts:449

___

###  UserForUserAdminDomainEntityInternal

Ƭ **UserForUserAdminDomainEntityInternal**: *[UserDomainEntityInternal](interfaces/userdomainentityinternal.md) & object*

Defined in types.ts:440

___

###  UserFullInfoDTO

Ƭ **UserFullInfoDTO**: *[UserDTO](globals.md#markdown-header-userdto) & object*

Defined in types.ts:1836

___

###  UserManagementSearchPaginateResponse

Ƭ **UserManagementSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:526

___

###  UserMinimal

Ƭ **UserMinimal**: *[UserMinimalInternal](interfaces/userminimalinternal.md) | [UserMinimalExternal](interfaces/userminimalexternal.md)*

Defined in types.ts:2200

___

###  UserOfConversationDTO

Ƭ **UserOfConversationDTO**: *[UserDTO](globals.md#markdown-header-userdto) & [AdditionalUserOfConversation](interfaces/additionaluserofconversation.md) & [UserTitle](interfaces/usertitle.md)*

Defined in types.ts:645

___

###  UserOfConversationForUserAdminDTO

Ƭ **UserOfConversationForUserAdminDTO**: *[UserDTO](globals.md#markdown-header-userdto) & [AdditionalUserOfConversation](interfaces/additionaluserofconversation.md) & [UserTitle](interfaces/usertitle.md) & [UserDepartmentAndDivision](interfaces/userdepartmentanddivision.md)*

Defined in types.ts:647

___

###  UserOnboardingJobSearchPaginateResponse

Ƭ **UserOnboardingJobSearchPaginateResponse**: *[SearchPaginateResponse](interfaces/searchpaginateresponse.md)*

Defined in types.ts:2380

___

###  UserSelfDTO

Ƭ **UserSelfDTO**: *[UserDTO](globals.md#markdown-header-userdto) & object*

Defined in types.ts:1751

___

###  UserSelfDomainEntity

Ƭ **UserSelfDomainEntity**: *[UserDomainEntity](globals.md#markdown-header-userdomainentity) & object*

Defined in types.ts:1755

## Variables

### `Const` API_VERSION

• **API_VERSION**: *"v1"* = "v1"

Defined in constants.ts:3

___

### `Const` ApiKeyName

• **ApiKeyName**: *"x-sayhey-client-api-key"* = "x-sayhey-client-api-key"

Defined in constants.ts:5

___

### `Const` MESSAGE_READ_UPDATE_INTERVAL

• **MESSAGE_READ_UPDATE_INTERVAL**: *5000* = 5000

Defined in constants.ts:7

___

### `Const` emailRegex

• **emailRegex**: *RegExp‹›* = /\S+@\S+\.\S+/i

Defined in helper.ts:17

___

### `Const` emojiRegex

• **emojiRegex**: *RegExp‹›* = /\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]/

Defined in helper.ts:5

___

### `Const` phoneRegex

• **phoneRegex**: *RegExp‹›* = /((^|\s{1}))((\+?1\s?)?((\([2-9]{1}\d{2}\))|([2-9]{1}\d{2}))[\s\-]?\d{3}[\s\-]?\d{4})(\s|$)/

Defined in helper.ts:16

___

### `Const` textHighlightRegex

• **textHighlightRegex**: *RegExp‹›* = /(\{\{([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})\}\})|((([hH][tT]{2}[pP][sS]?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b)([-a-zA-Z0-9@:%_\+.~#?&\/=]*[-a-zA-Z0-9@:%_\+~#?&\/=])*)|(((^|\s{1}))((\+?1\s?)?((\([2-9]{1}\d{2}\))|([2-9]{1}\d{2}))[\s\-]?\d{3}[\s\-]?\d{4})(\s|$))|(\S+@\S+\.\S+)/gi

Defined in helper.ts:9

___

### `Const` urlRegex

• **urlRegex**: *RegExp‹›* = /(([hH][tT]{2}[pP][sS]?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b)([-a-zA-Z0-9@:%_\+.~#?&\/=]*[-a-zA-Z0-9@:%_\+~#?&\/=])*/

Defined in helper.ts:12

___

### `Const` userIdRegex

• **userIdRegex**: *RegExp‹›* = /\{\{([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})\}\}/i

Defined in helper.ts:20

## Functions

###  checkApiKey

▸ **checkApiKey**(`config`: AxiosRequestConfig): *AxiosRequestConfig*

Defined in axios.ts:6

**Parameters:**

Name | Type |
------ | ------ |
`config` | AxiosRequestConfig |

**Returns:** *AxiosRequestConfig*

___

###  checkaccessToken

▸ **checkaccessToken**(`config`: AxiosRequestConfig): *AxiosRequestConfig*

Defined in axios.ts:18

**Parameters:**

Name | Type |
------ | ------ |
`config` | AxiosRequestConfig |

**Returns:** *AxiosRequestConfig*

___

### `Const` isEmail

▸ **isEmail**(`email`: string): *boolean*

Defined in helper.ts:23

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |

**Returns:** *boolean*

___

### `Const` isPhone

▸ **isPhone**(`phone`: string): *boolean*

Defined in helper.ts:22

**Parameters:**

Name | Type |
------ | ------ |
`phone` | string |

**Returns:** *boolean*

___

### `Const` isSingleEmoji

▸ **isSingleEmoji**(`text`: string): *null | false | RegExpMatchArray‹›*

Defined in helper.ts:6

**Parameters:**

Name | Type |
------ | ------ |
`text` | string |

**Returns:** *null | false | RegExpMatchArray‹›*

___

### `Const` isUrl

▸ **isUrl**(`url`: string): *boolean*

Defined in helper.ts:21

**Parameters:**

Name | Type |
------ | ------ |
`url` | string |

**Returns:** *boolean*

___

### `Const` isUser

▸ **isUser**(`userId`: string): *boolean*

Defined in helper.ts:24

**Parameters:**

Name | Type |
------ | ------ |
`userId` | string |

**Returns:** *boolean*

___

### `Const` parseText

▸ **parseText**(`text`: string | null): *object | null*

Defined in helper.ts:26

**Parameters:**

Name | Type |
------ | ------ |
`text` | string &#124; null |

**Returns:** *object | null*

___

###  requestFailed

▸ **requestFailed**(`error`: AxiosError): *void*

Defined in axios.ts:28

**Parameters:**

Name | Type |
------ | ------ |
`error` | AxiosError |

**Returns:** *void*

## Object literals

### `Const` ChatAdminRoutes

### ▪ **ChatAdminRoutes**: *object*

Defined in constants.ts:275

▪ **AssignManagedUsers**: *object*

Defined in constants.ts:335

* **Endpoint**: *string* = "/user-managements"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateConversationSpaceGroupDivision**: *object*

Defined in constants.ts:530

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **CreateDepartment**: *object*

Defined in constants.ts:387

* **Endpoint**: *string* = "/departments"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateDivisions**: *object*

Defined in constants.ts:401

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`departmentId`: string): *string*

▪ **CreateGroupConversation**: *object*

Defined in constants.ts:510

* **Endpoint**: *string* = "/conversations/user-admin-managed"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateKeyword**: *object*

Defined in constants.ts:418

* **Endpoint**: *string* = "/keywords"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateKeywordBulk**: *object*

Defined in constants.ts:422

* **Endpoint**: *string* = "/keywords/bulk"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateUpdateUserAdminSearch**: *object*

Defined in constants.ts:459

* **Endpoint**: *string* = "/user-admin-searches"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **DeleteConversationSpaceGroupDivision**: *object*

Defined in constants.ts:534

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string): *string*

▪ **DeleteKeyword**: *object*

Defined in constants.ts:426

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`id`: string): *string*

▪ **DisableAdmin**: *object*

Defined in constants.ts:293

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`userAdminId`: string): *string*

▪ **DisableUser**: *object*

Defined in constants.ts:365

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`userId`: string): *string*

▪ **DownloadAuditMessages**: *object*

Defined in constants.ts:455

* **Endpoint**: *string* = "/messages/audit-download"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **DownloadSampleFile**: *object*

Defined in constants.ts:583

* **Endpoint**: *string* = "user-onboarding-jobs/download"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **EnableAdmin**: *object*

Defined in constants.ts:297

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`userAdminId`: string): *string*

▪ **EnableUser**: *object*

Defined in constants.ts:369

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`userId`: string): *string*

▪ **ForgotPassword**: *object*

Defined in constants.ts:301

* **Endpoint**: *string* = "/user-admins/forgot-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **GetAllErroredUserOnboardingJobRecords**: *object*

Defined in constants.ts:587

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`id`: string): *string*

▪ **GetAuditMessagesBatch**: *object*

Defined in constants.ts:451

* **Endpoint**: *string* = "/messages/audit"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetDepartments**: *object*

Defined in constants.ts:391

* **Endpoint**: *string* = "/departments"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetDivisions**: *object*

Defined in constants.ts:406

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`departmentId`: string): *string*

▪ **GetEligibleRolesToInviteAdmin**: *object*

Defined in constants.ts:568

* **Endpoint**: *string* = `/user-admins/invitable-roles`

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetFlaggedMessageDetails**: *object*

Defined in constants.ts:484

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`id`: string): *string*

▪ **GetFlaggedMessageKeywordScans**: *object*

Defined in constants.ts:492

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`id`: string): *string*

▪ **GetFlaggedMessageResolutionKeywordScans**: *object*

Defined in constants.ts:504

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`id`: string): *string*

▪ **GetFlaggedMessageResolutionUserReports**: *object*

Defined in constants.ts:500

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`id`: string): *string*

▪ **GetFlaggedMessageResolutions**: *object*

Defined in constants.ts:496

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`id`: string): *string*

▪ **GetFlaggedMessageUserReports**: *object*

Defined in constants.ts:488

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`id`: string): *string*

▪ **GetFlaggedMessagesBatch**: *object*

Defined in constants.ts:472

* **Endpoint**: *string* = "/flagged-messages"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetFlaggedMessagesCount**: *object*

Defined in constants.ts:476

* **Endpoint**: *string* = "/flagged-messages/count"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetKeywordsBatch**: *object*

Defined in constants.ts:434

* **Endpoint**: *string* = "/keywords"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetManagedUsers**: *object*

Defined in constants.ts:347

* **Endpoint**: *string* = "/user-managements"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetManagedUsersCount**: *object*

Defined in constants.ts:351

* **Endpoint**: *string* = "/user-managements/count"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetMessageContext**: *object*

Defined in constants.ts:447

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`messageId`: string): *string*

▪ **GetMessagesAfterSequence**: *object*

Defined in constants.ts:443

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetMessagesBeforeSequence**: *object*

Defined in constants.ts:439

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetMyUsersCount**: *object*

Defined in constants.ts:544

* **Endpoint**: *string* = "user-managements/my-users-count"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetResolvedFlaggedMessagesCount**: *object*

Defined in constants.ts:548

* **Endpoint**: *string* = "flagged-messages/resolved/total-count"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetSentMessagesCountDaily**: *object*

Defined in constants.ts:552

* **Endpoint**: *string* = "sent-message-counts/daily"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetSentMessagesCountTotal**: *object*

Defined in constants.ts:556

* **Endpoint**: *string* = "sent-message-counts/total-count"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetSpaceGroupDivisions**: *object*

Defined in constants.ts:526

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetTotalUsersCount**: *object*

Defined in constants.ts:540

* **Endpoint**: *string* = "users/total-count"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUnmanagedUsers**: *object*

Defined in constants.ts:377

* **Endpoint**: *string* = "/users/unmanaged"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUnmanagedUsersCount**: *object*

Defined in constants.ts:381

* **Endpoint**: *string* = "/users/unmanaged-count"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUnresolvedFlaggedMessageCountsDaily**: *object*

Defined in constants.ts:564

* **Endpoint**: *string* = "flagged-messages/unresolved/daily"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUnresolvedFlaggedMessagesCount**: *object*

Defined in constants.ts:560

* **Endpoint**: *string* = "flagged-messages/unresolved/total-count"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUserAdminGroups**: *object*

Defined in constants.ts:519

* **Endpoint**: *string* = "conversations/user-admin-groups"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUserAdminSearch**: *object*

Defined in constants.ts:463

* **Endpoint**: *string* = "/user-admin-searches"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUserAdminSelfInfo**: *object*

Defined in constants.ts:325

* **Endpoint**: *string* = `/user-admins/${APISignature.ME}`

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUserAdmins**: *object*

Defined in constants.ts:309

* **Endpoint**: *string* = "/user-admins"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUserBasicInfoBatch**: *object*

Defined in constants.ts:467

* **Endpoint**: *string* = "/users"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUserOnboardingJobBatch**: *object*

Defined in constants.ts:579

* **Endpoint**: *string* = "user-onboarding-jobs"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **Invite**: *object*

Defined in constants.ts:277

* **Endpoint**: *string* = "/user-admins/invitation"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **MoveAllManagedUsers**: *object*

Defined in constants.ts:339

* **Endpoint**: *string* = "/user-managements"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **MoveBackAllManagedUsers**: *object*

Defined in constants.ts:343

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`userAdminId`: string): *string*

▪ **MoveSelectedUsers**: *object*

Defined in constants.ts:355

* **Endpoint**: *string* = "/user-managements/users/move-selected"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **ProcessUserOnboardingJobUpload**: *object*

Defined in constants.ts:575

* **Endpoint**: *string* = "user-onboarding-jobs/files"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **ReInvite**: *object*

Defined in constants.ts:289

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`userAdminId`: string): *string*

▪ **RegisterUser**: *object*

Defined in constants.ts:361

* **Endpoint**: *string* = "/users/registration"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **ResetPassword**: *object*

Defined in constants.ts:305

* **Endpoint**: *string* = "/user-admins/reset-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **ResolveFlaggedMessages**: *object*

Defined in constants.ts:480

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`id`: string): *string*

▪ **SignIn**: *object*

Defined in constants.ts:285

* **Endpoint**: *string* = "/user-admins/sign-in"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **SignUp**: *object*

Defined in constants.ts:281

* **Endpoint**: *string* = "/user-admins/sign-up"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **UpdateDepartments**: *object*

Defined in constants.ts:395

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`departmentId`: string): *string*

▪ **UpdateDivisions**: *object*

Defined in constants.ts:411

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`__namedParameters`: object): *string*

▪ **UpdateGroupConversationOwner**: *object*

Defined in constants.ts:514

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Patch

* **Endpoint**(`conversationId`: string, `userAdminId`: string): *string*

▪ **UpdateKeyword**: *object*

Defined in constants.ts:430

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Patch

* **Endpoint**(`id`: string): *string*

▪ **UpdateUser**: *object*

Defined in constants.ts:373

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`userId`: string): *string*

▪ **UpdateUserAdminInfo**: *object*

Defined in constants.ts:313

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Patch

* **Endpoint**(`userAdminId`: string): *string*

▪ **UpdateUserAdminOnLeave**: *object*

Defined in constants.ts:321

* **Endpoint**: *string* = "/user-admins/onLeave"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Patch

▪ **UpdateUserAdminRole**: *object*

Defined in constants.ts:317

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Patch

* **Endpoint**(`userAdminId`: string): *string*

▪ **UpdateUserAdminSelfInfo**: *object*

Defined in constants.ts:329

* **Endpoint**: *string* = `/user-admins/${APISignature.ME}`

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Patch

___

### `Const` ChatClientRoutes

### ▪ **ChatClientRoutes**: *object*

Defined in constants.ts:9

▪ **AcceptSmsConversationInvitation**: *object*

Defined in constants.ts:242

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Patch

* **Endpoint**(`conversationId`: string): *string*

▪ **AcceptTermsAndPrivacy**: *object*

Defined in constants.ts:225

* **Endpoint**: *string* = "external-users/accepted-terms-privacy"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **AddToContact**: *object*

Defined in constants.ts:197

* **Endpoint**: *string* = "contacts"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **ChangePassword**: *object*

Defined in constants.ts:18

* **Endpoint**: *string* = "/users/change-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CheckEmail**: *object*

Defined in constants.ts:146

* **Endpoint**: *string* = "/users/email-check"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CheckSmsConversationInvitation**: *object*

Defined in constants.ts:237

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **ClientSignInWithToken**: *object*

Defined in constants.ts:186

* **Endpoint**: *string* = "/users/client-signin-with-token"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateFlaggedMessage**: *object*

Defined in constants.ts:190

* **Endpoint**: *string* = "/flagged-messages"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateGroupConversation**: *object*

Defined in constants.ts:46

* **Endpoint**: *string* = "/conversations/group"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateIndividualConversation**: *object*

Defined in constants.ts:42

* **Endpoint**: *string* = "/conversations/individual"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateNewContact**: *object*

Defined in constants.ts:221

* **Endpoint**: *string* = "users"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateQuickConversation**: *object*

Defined in constants.ts:50

* **Endpoint**: *string* = "/conversations/quick-group"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateSmsConversation**: *object*

Defined in constants.ts:252

* **Endpoint**: *string* = "conversations/sms-group"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **DeleteAllConversations**: *object*

Defined in constants.ts:82

* **Endpoint**: *string* = `/conversations/hide-clear`

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **DeleteConversation**: *object*

Defined in constants.ts:78

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **FindExternalUserByPhone**: *object*

Defined in constants.ts:205

* **Endpoint**: *string* = "external-users"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **ForgotPassword**: *object*

Defined in constants.ts:22

* **Endpoint**: *string* = "/users/forgot-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **GetAppConfig**: *object*

Defined in constants.ts:182

* **Endpoint**: *string* = "/app-config"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetConversationBasicUsers**: *object*

Defined in constants.ts:130

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversationReachableUsers**: *object*

Defined in constants.ts:94

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversations**: *object*

Defined in constants.ts:38

* **Endpoint**: *string* = "/conversations"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetExistingConversationWithUser**: *object*

Defined in constants.ts:30

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`userId`: string): *string*

▪ **GetExistingIndvidualOrQuickConversation**: *object*

Defined in constants.ts:34

* **Endpoint**: *string* = "/conversations/individual-quick"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetExistingSmsConversation**: *object*

Defined in constants.ts:233

* **Endpoint**: *string* = "conversations/existing-sms-conversations"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetInternalUserContactsBatch**: *object*

Defined in constants.ts:201

* **Endpoint**: *string* = "contacts"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetMessages**: *object*

Defined in constants.ts:54

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetOptToReceiveSmsStatus**: *object*

Defined in constants.ts:264

* **Endpoint**: *string* = "external-users/opted-receive-sms"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetTermsAndPrivacy**: *object*

Defined in constants.ts:229

* **Endpoint**: *string* = "external-users/accepted-terms-privacy"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUserConversationMessagesReactions**: *object*

Defined in constants.ts:102

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **GetUserConversationReactions**: *object*

Defined in constants.ts:98

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetUserProfile**: *object*

Defined in constants.ts:122

* **Endpoint**: *string* = "/users/profile-info"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **MarkMessageAsRead**: *object*

Defined in constants.ts:126

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`messageId`: string): *string*

▪ **MuteAllNotifications**: *object*

Defined in constants.ts:114

* **Endpoint**: *string* = "/users/mute-all"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **MuteConversation**: *object*

Defined in constants.ts:62

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **NudgeExternalUserToConversation**: *object*

Defined in constants.ts:268

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **OptNotToReceiveSms**: *object*

Defined in constants.ts:260

* **Endpoint**: *string* = "external-users/opted-receive-sms"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

▪ **OptToReceiveSms**: *object*

Defined in constants.ts:256

* **Endpoint**: *string* = "external-users/opted-receive-sms"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **PinConversation**: *object*

Defined in constants.ts:70

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **RegisterPushNotification**: *object*

Defined in constants.ts:134

* **Endpoint**: *string* = "/device-tokens"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **RemoveMessageReaction**: *object*

Defined in constants.ts:110

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`messageId`: string): *string*

▪ **RequestPreverificationOTP**: *object*

Defined in constants.ts:162

* **Endpoint**: *string* = "/otp/pre-verification"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **RequestResetPasswordOTP**: *object*

Defined in constants.ts:158

* **Endpoint**: *string* = "/otp/reset-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **RequestSetPasswordOTP**: *object*

Defined in constants.ts:154

* **Endpoint**: *string* = "/otp/set-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **RequestSignInExternalOTP**: *object*

Defined in constants.ts:209

* **Endpoint**: *string* = "otp/sign-in-external"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **RequestSignUpOTP**: *object*

Defined in constants.ts:150

* **Endpoint**: *string* = "/otp/sign-up"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **ResendSmsConversationInvitation**: *object*

Defined in constants.ts:247

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **ResetPassword**: *object*

Defined in constants.ts:26

* **Endpoint**: *string* = "/users/reset-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **SendMessage**: *object*

Defined in constants.ts:58

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **SetMessageReaction**: *object*

Defined in constants.ts:106

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`messageId`: string): *string*

▪ **SetPassword**: *object*

Defined in constants.ts:142

* **Endpoint**: *string* = "/users/set-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **SignIn**: *object*

Defined in constants.ts:10

* **Endpoint**: *string* = "/users/sign-in"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **SignInExternal**: *object*

Defined in constants.ts:213

* **Endpoint**: *string* = "users/sign-in-external"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **SignInExternalByUserId**: *object*

Defined in constants.ts:217

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`id`: string): *string*

▪ **SignUp**: *object*

Defined in constants.ts:14

* **Endpoint**: *string* = "/users/sign-up"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **UnmuteAllNotifications**: *object*

Defined in constants.ts:118

* **Endpoint**: *string* = "/users/mute-all"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

▪ **UnmuteConversation**: *object*

Defined in constants.ts:66

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string): *string*

▪ **UnpinConversation**: *object*

Defined in constants.ts:74

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string): *string*

▪ **UnregisterPushNotification**: *object*

Defined in constants.ts:138

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`instanceId`: string): *string*

▪ **UpdateUserProfileImage**: *object*

Defined in constants.ts:86

* **Endpoint**: *string* = "/users/profile-image"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **UpdateUserProfileInfo**: *object*

Defined in constants.ts:90

* **Endpoint**: *string* = "/users/profile-info"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **ValidatePreverificationOTP**: *object*

Defined in constants.ts:178

* **Endpoint**: *string* = "/otp/validate-pre-verification"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **ValidateResetPasswordOTP**: *object*

Defined in constants.ts:174

* **Endpoint**: *string* = "/otp/validate-reset-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **ValidateSetPasswordOTP**: *object*

Defined in constants.ts:170

* **Endpoint**: *string* = "/otp/validate-set-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **ValidateSignUpOTP**: *object*

Defined in constants.ts:166

* **Endpoint**: *string* = "/otp/validate-sign-up"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

___

### `Const` ChatCommonRoutes

### ▪ **ChatCommonRoutes**: *object*

Defined in constants.ts:593

▪ **AddUsersToConversation**: *object*

Defined in constants.ts:610

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **DeleteGroupConversation**: *object*

Defined in constants.ts:648

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversationDetails**: *object*

Defined in constants.ts:644

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversationUsers**: *object*

Defined in constants.ts:606

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetGroupDefaultImageSet**: *object*

Defined in constants.ts:626

* **Endpoint**: *string* = "/default-group-images"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetReachableUsers**: *object*

Defined in constants.ts:602

* **Endpoint**: *string* = "/users/conversable"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetReactedUsers**: *object*

Defined in constants.ts:598

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`messageId`: string): *string*

▪ **MakeConversationOwner**: *object*

Defined in constants.ts:634

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string, `userId`: string): *string*

▪ **RemoveConversationOwner**: *object*

Defined in constants.ts:639

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string, `userId`: string): *string*

▪ **RemoveGroupImage**: *object*

Defined in constants.ts:622

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string): *string*

▪ **RemoveUsersFromConversation**: *object*

Defined in constants.ts:614

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **UpdateGroupImage**: *object*

Defined in constants.ts:618

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **UpdateGroupInfo**: *object*

Defined in constants.ts:630

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **UploadFile**: *object*

Defined in constants.ts:594

* **Endpoint**: *string* = "/files"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

___

### `Const` mimeTypes

### ▪ **mimeTypes**: *object*

Defined in helper.ts:101

###  3gp

• **3gp**: *string* = "video/3gpp"

Defined in helper.ts:102

###  a

• **a**: *string* = "application/octet-stream"

Defined in helper.ts:103

###  ai

• **ai**: *string* = "application/postscript"

Defined in helper.ts:104

###  aif

• **aif**: *string* = "audio/x-aiff"

Defined in helper.ts:105

###  aiff

• **aiff**: *string* = "audio/x-aiff"

Defined in helper.ts:106

###  asc

• **asc**: *string* = "application/pgp-signature"

Defined in helper.ts:107

###  asf

• **asf**: *string* = "video/x-ms-asf"

Defined in helper.ts:108

###  asm

• **asm**: *string* = "text/x-asm"

Defined in helper.ts:109

###  asx

• **asx**: *string* = "video/x-ms-asf"

Defined in helper.ts:110

###  atom

• **atom**: *string* = "application/atom+xml"

Defined in helper.ts:111

###  au

• **au**: *string* = "audio/basic"

Defined in helper.ts:112

###  avi

• **avi**: *string* = "video/x-msvideo"

Defined in helper.ts:113

###  bat

• **bat**: *string* = "application/x-msdownload"

Defined in helper.ts:114

###  bin

• **bin**: *string* = "application/octet-stream"

Defined in helper.ts:115

###  bmp

• **bmp**: *string* = "image/bmp"

Defined in helper.ts:116

###  bz2

• **bz2**: *string* = "application/x-bzip2"

Defined in helper.ts:117

###  c

• **c**: *string* = "text/x-csrc"

Defined in helper.ts:118

###  cab

• **cab**: *string* = "application/vnd.ms-cab-compressed"

Defined in helper.ts:119

###  can

• **can**: *string* = "application/candor"

Defined in helper.ts:120

###  cc

• **cc**: *string* = "text/x-c++src"

Defined in helper.ts:121

###  chm

• **chm**: *string* = "application/vnd.ms-htmlhelp"

Defined in helper.ts:122

###  class

• **class**: *string* = "application/octet-stream"

Defined in helper.ts:123

###  com

• **com**: *string* = "application/x-msdownload"

Defined in helper.ts:124

###  conf

• **conf**: *string* = "text/plain"

Defined in helper.ts:125

###  cpp

• **cpp**: *string* = "text/x-c"

Defined in helper.ts:126

###  crt

• **crt**: *string* = "application/x-x509-ca-cert"

Defined in helper.ts:127

###  css

• **css**: *string* = "text/css"

Defined in helper.ts:128

###  csv

• **csv**: *string* = "text/csv"

Defined in helper.ts:129

###  cxx

• **cxx**: *string* = "text/x-c"

Defined in helper.ts:130

###  deb

• **deb**: *string* = "application/x-debian-package"

Defined in helper.ts:131

###  der

• **der**: *string* = "application/x-x509-ca-cert"

Defined in helper.ts:132

###  diff

• **diff**: *string* = "text/x-diff"

Defined in helper.ts:133

###  djv

• **djv**: *string* = "image/vnd.djvu"

Defined in helper.ts:134

###  djvu

• **djvu**: *string* = "image/vnd.djvu"

Defined in helper.ts:135

###  dll

• **dll**: *string* = "application/x-msdownload"

Defined in helper.ts:136

###  dmg

• **dmg**: *string* = "application/octet-stream"

Defined in helper.ts:137

###  doc

• **doc**: *string* = "application/msword"

Defined in helper.ts:138

###  dot

• **dot**: *string* = "application/msword"

Defined in helper.ts:139

###  dtd

• **dtd**: *string* = "application/xml-dtd"

Defined in helper.ts:140

###  dvi

• **dvi**: *string* = "application/x-dvi"

Defined in helper.ts:141

###  ear

• **ear**: *string* = "application/java-archive"

Defined in helper.ts:142

###  eml

• **eml**: *string* = "message/rfc822"

Defined in helper.ts:143

###  eps

• **eps**: *string* = "application/postscript"

Defined in helper.ts:144

###  exe

• **exe**: *string* = "application/x-msdownload"

Defined in helper.ts:145

###  f

• **f**: *string* = "text/x-fortran"

Defined in helper.ts:146

###  f77

• **f77**: *string* = "text/x-fortran"

Defined in helper.ts:147

###  f90

• **f90**: *string* = "text/x-fortran"

Defined in helper.ts:148

###  flv

• **flv**: *string* = "video/x-flv"

Defined in helper.ts:149

###  for

• **for**: *string* = "text/x-fortran"

Defined in helper.ts:150

###  gem

• **gem**: *string* = "application/octet-stream"

Defined in helper.ts:151

###  gemspec

• **gemspec**: *string* = "text/x-script.ruby"

Defined in helper.ts:152

###  gif

• **gif**: *string* = "image/gif"

Defined in helper.ts:153

###  gyp

• **gyp**: *string* = "text/x-script.python"

Defined in helper.ts:154

###  gypi

• **gypi**: *string* = "text/x-script.python"

Defined in helper.ts:155

###  gz

• **gz**: *string* = "application/x-gzip"

Defined in helper.ts:156

###  h

• **h**: *string* = "text/x-chdr"

Defined in helper.ts:157

###  heic

• **heic**: *string* = "image/heic"

Defined in helper.ts:159

###  heif

• **heif**: *string* = "image/heif"

Defined in helper.ts:160

###  hevc

• **hevc**: *string* = "video/mp4"

Defined in helper.ts:161

###  hh

• **hh**: *string* = "text/x-c++hdr"

Defined in helper.ts:158

###  htm

• **htm**: *string* = "text/html"

Defined in helper.ts:162

###  html

• **html**: *string* = "text/html"

Defined in helper.ts:163

###  ico

• **ico**: *string* = "image/vnd.microsoft.icon"

Defined in helper.ts:164

###  ics

• **ics**: *string* = "text/calendar"

Defined in helper.ts:165

###  ifb

• **ifb**: *string* = "text/calendar"

Defined in helper.ts:166

###  iso

• **iso**: *string* = "application/octet-stream"

Defined in helper.ts:167

###  jpeg

• **jpeg**: *string* = "image/jpeg"

Defined in helper.ts:168

###  jpg

• **jpg**: *string* = "image/jpeg"

Defined in helper.ts:169

###  js

• **js**: *string* = "application/javascript"

Defined in helper.ts:170

###  json

• **json**: *string* = "application/json"

Defined in helper.ts:171

###  less

• **less**: *string* = "text/css"

Defined in helper.ts:172

###  log

• **log**: *string* = "text/plain"

Defined in helper.ts:173

###  lua

• **lua**: *string* = "text/x-script.lua"

Defined in helper.ts:174

###  luac

• **luac**: *string* = "application/x-bytecode.lua"

Defined in helper.ts:175

###  m3u

• **m3u**: *string* = "audio/x-mpegurl"

Defined in helper.ts:176

###  m4v

• **m4v**: *string* = "video/mp4"

Defined in helper.ts:177

###  man

• **man**: *string* = "text/troff"

Defined in helper.ts:178

###  manifest

• **manifest**: *string* = "text/cache-manifest"

Defined in helper.ts:179

###  markdown

• **markdown**: *string* = "text/x-markdown"

Defined in helper.ts:180

###  mathml

• **mathml**: *string* = "application/mathml+xml"

Defined in helper.ts:181

###  mbox

• **mbox**: *string* = "application/mbox"

Defined in helper.ts:182

###  md

• **md**: *string* = "text/x-markdown"

Defined in helper.ts:184

###  mdoc

• **mdoc**: *string* = "text/troff"

Defined in helper.ts:183

###  me

• **me**: *string* = "text/troff"

Defined in helper.ts:185

###  mid

• **mid**: *string* = "audio/midi"

Defined in helper.ts:186

###  midi

• **midi**: *string* = "audio/midi"

Defined in helper.ts:187

###  mime

• **mime**: *string* = "message/rfc822"

Defined in helper.ts:188

###  mml

• **mml**: *string* = "application/mathml+xml"

Defined in helper.ts:189

###  mng

• **mng**: *string* = "video/x-mng"

Defined in helper.ts:190

###  mov

• **mov**: *string* = "video/quicktime"

Defined in helper.ts:191

###  mp3

• **mp3**: *string* = "audio/mpeg"

Defined in helper.ts:192

###  mp4

• **mp4**: *string* = "video/mp4"

Defined in helper.ts:193

###  mp4v

• **mp4v**: *string* = "video/mp4"

Defined in helper.ts:194

###  mpeg

• **mpeg**: *string* = "video/mpeg"

Defined in helper.ts:195

###  mpg

• **mpg**: *string* = "video/mpeg"

Defined in helper.ts:196

###  ms

• **ms**: *string* = "text/troff"

Defined in helper.ts:197

###  msi

• **msi**: *string* = "application/x-msdownload"

Defined in helper.ts:198

###  odp

• **odp**: *string* = "application/vnd.oasis.opendocument.presentation"

Defined in helper.ts:199

###  ods

• **ods**: *string* = "application/vnd.oasis.opendocument.spreadsheet"

Defined in helper.ts:200

###  odt

• **odt**: *string* = "application/vnd.oasis.opendocument.text"

Defined in helper.ts:201

###  ogg

• **ogg**: *string* = "application/ogg"

Defined in helper.ts:202

###  p

• **p**: *string* = "text/x-pascal"

Defined in helper.ts:203

###  pas

• **pas**: *string* = "text/x-pascal"

Defined in helper.ts:204

###  pbm

• **pbm**: *string* = "image/x-portable-bitmap"

Defined in helper.ts:205

###  pdf

• **pdf**: *string* = "application/pdf"

Defined in helper.ts:206

###  pem

• **pem**: *string* = "application/x-x509-ca-cert"

Defined in helper.ts:207

###  pgm

• **pgm**: *string* = "image/x-portable-graymap"

Defined in helper.ts:208

###  pgp

• **pgp**: *string* = "application/pgp-encrypted"

Defined in helper.ts:209

###  pkg

• **pkg**: *string* = "application/octet-stream"

Defined in helper.ts:210

###  png

• **png**: *string* = "image/png"

Defined in helper.ts:211

###  pnm

• **pnm**: *string* = "image/x-portable-anymap"

Defined in helper.ts:212

###  ppm

• **ppm**: *string* = "image/x-portable-pixmap"

Defined in helper.ts:213

###  pps

• **pps**: *string* = "application/vnd.ms-powerpoint"

Defined in helper.ts:214

###  ppt

• **ppt**: *string* = "application/vnd.ms-powerpoint"

Defined in helper.ts:215

###  ps

• **ps**: *string* = "application/postscript"

Defined in helper.ts:216

###  psd

• **psd**: *string* = "image/vnd.adobe.photoshop"

Defined in helper.ts:217

###  qt

• **qt**: *string* = "video/quicktime"

Defined in helper.ts:218

###  ra

• **ra**: *string* = "audio/x-pn-realaudio"

Defined in helper.ts:219

###  ram

• **ram**: *string* = "audio/x-pn-realaudio"

Defined in helper.ts:220

###  rar

• **rar**: *string* = "application/x-rar-compressed"

Defined in helper.ts:221

###  rdf

• **rdf**: *string* = "application/rdf+xml"

Defined in helper.ts:222

###  roff

• **roff**: *string* = "text/troff"

Defined in helper.ts:223

###  rss

• **rss**: *string* = "application/rss+xml"

Defined in helper.ts:224

###  rtf

• **rtf**: *string* = "application/rtf"

Defined in helper.ts:225

###  s

• **s**: *string* = "text/x-asm"

Defined in helper.ts:226

###  sgm

• **sgm**: *string* = "text/sgml"

Defined in helper.ts:227

###  sgml

• **sgml**: *string* = "text/sgml"

Defined in helper.ts:228

###  sh

• **sh**: *string* = "application/x-sh"

Defined in helper.ts:229

###  sig

• **sig**: *string* = "application/pgp-signature"

Defined in helper.ts:230

###  snd

• **snd**: *string* = "audio/basic"

Defined in helper.ts:231

###  so

• **so**: *string* = "application/octet-stream"

Defined in helper.ts:232

###  svg

• **svg**: *string* = "image/svg+xml"

Defined in helper.ts:233

###  svgz

• **svgz**: *string* = "image/svg+xml"

Defined in helper.ts:234

###  swf

• **swf**: *string* = "application/x-shockwave-flash"

Defined in helper.ts:235

###  t

• **t**: *string* = "text/troff"

Defined in helper.ts:236

###  tar

• **tar**: *string* = "application/x-tar"

Defined in helper.ts:237

###  tbz

• **tbz**: *string* = "application/x-bzip-compressed-tar"

Defined in helper.ts:238

###  tci

• **tci**: *string* = "application/x-topcloud"

Defined in helper.ts:239

###  tcl

• **tcl**: *string* = "application/x-tcl"

Defined in helper.ts:240

###  tex

• **tex**: *string* = "application/x-tex"

Defined in helper.ts:241

###  texi

• **texi**: *string* = "application/x-texinfo"

Defined in helper.ts:242

###  texinfo

• **texinfo**: *string* = "application/x-texinfo"

Defined in helper.ts:243

###  text

• **text**: *string* = "text/plain"

Defined in helper.ts:244

###  tif

• **tif**: *string* = "image/tiff"

Defined in helper.ts:245

###  tiff

• **tiff**: *string* = "image/tiff"

Defined in helper.ts:246

###  torrent

• **torrent**: *string* = "application/x-bittorrent"

Defined in helper.ts:247

###  tr

• **tr**: *string* = "text/troff"

Defined in helper.ts:248

###  ttf

• **ttf**: *string* = "application/x-font-ttf"

Defined in helper.ts:249

###  txt

• **txt**: *string* = "text/plain"

Defined in helper.ts:250

###  vcf

• **vcf**: *string* = "text/x-vcard"

Defined in helper.ts:251

###  vcs

• **vcs**: *string* = "text/x-vcalendar"

Defined in helper.ts:252

###  war

• **war**: *string* = "application/java-archive"

Defined in helper.ts:253

###  wav

• **wav**: *string* = "audio/x-wav"

Defined in helper.ts:254

###  webm

• **webm**: *string* = "video/webm"

Defined in helper.ts:255

###  wma

• **wma**: *string* = "audio/x-ms-wma"

Defined in helper.ts:256

###  wmv

• **wmv**: *string* = "video/x-ms-wmv"

Defined in helper.ts:257

###  wmx

• **wmx**: *string* = "video/x-ms-wmx"

Defined in helper.ts:258

###  wrl

• **wrl**: *string* = "model/vrml"

Defined in helper.ts:259

###  wsdl

• **wsdl**: *string* = "application/wsdl+xml"

Defined in helper.ts:260

###  xbm

• **xbm**: *string* = "image/x-xbitmap"

Defined in helper.ts:261

###  xhtml

• **xhtml**: *string* = "application/xhtml+xml"

Defined in helper.ts:262

###  xls

• **xls**: *string* = "application/vnd.ms-excel"

Defined in helper.ts:263

###  xml

• **xml**: *string* = "application/xml"

Defined in helper.ts:264

###  zip

• **zip**: *string* = "application/zip"

Defined in helper.ts:265
