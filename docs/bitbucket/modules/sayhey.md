[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](sayhey.md)

# Namespace: SayHey

## Index

### Namespaces

* [ChatKitError](sayhey.chatkiterror.md)
* [Event](sayhey.event.md)
* [Payload](sayhey.payload.md)

### Variables

* [Error](sayhey.md#markdown-header-error)
* [ErrorType](sayhey.md#markdown-header-errortype)
* [EventType](sayhey.md#markdown-header-eventtype)

## Variables

###  Error

• **Error**: *[AppError](../classes/sayhey.chatkiterror.apperror.md)*

Defined in types.ts:1684

___

###  ErrorType

• **ErrorType**: *[ErrorType](sayhey.chatkiterror.errortype.md)*

Defined in types.ts:1680

___

###  EventType

• **EventType**: *[EventType](../enums/publisher.eventtype.md)*

Defined in types.ts:1676
