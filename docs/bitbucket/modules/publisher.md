[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](publisher.md)

# Namespace: Publisher

## Index

### Namespaces

* [Event](publisher.event.md)
* [Payload](publisher.payload.md)

### Enumerations

* [EventType](../enums/publisher.eventtype.md)
