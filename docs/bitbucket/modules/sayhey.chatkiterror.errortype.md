[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](sayhey.md) › [ChatKitError](sayhey.chatkiterror.md) › [ErrorType](sayhey.chatkiterror.errortype.md)

# Namespace: ErrorType

## Index

### Enumerations

* [AppConfigErrorType](../enums/sayhey.chatkiterror.errortype.appconfigerrortype.md)
* [AppFeatureErrorTpe](../enums/sayhey.chatkiterror.errortype.appfeatureerrortpe.md)
* [AppRegistrationErrorType](../enums/sayhey.chatkiterror.errortype.appregistrationerrortype.md)
* [AuthErrorType](../enums/sayhey.chatkiterror.errortype.autherrortype.md)
* [ContactErrorType](../enums/sayhey.chatkiterror.errortype.contacterrortype.md)
* [ConversationErrorType](../enums/sayhey.chatkiterror.errortype.conversationerrortype.md)
* [DateErrorType](../enums/sayhey.chatkiterror.errortype.dateerrortype.md)
* [DepartmentErrorTpe](../enums/sayhey.chatkiterror.errortype.departmenterrortpe.md)
* [DivisionErrorType](../enums/sayhey.chatkiterror.errortype.divisionerrortype.md)
* [EmailErrorType](../enums/sayhey.chatkiterror.errortype.emailerrortype.md)
* [ExternalUserErrorType](../enums/sayhey.chatkiterror.errortype.externalusererrortype.md)
* [FileErrorType](../enums/sayhey.chatkiterror.errortype.fileerrortype.md)
* [FlaggedMessageErrorType](../enums/sayhey.chatkiterror.errortype.flaggedmessageerrortype.md)
* [GeneralErrorType](../enums/sayhey.chatkiterror.errortype.generalerrortype.md)
* [InternalErrorType](../enums/sayhey.chatkiterror.errortype.internalerrortype.md)
* [KeywordErrorType](../enums/sayhey.chatkiterror.errortype.keyworderrortype.md)
* [MessageErrorType](../enums/sayhey.chatkiterror.errortype.messageerrortype.md)
* [OTPErrorType](../enums/sayhey.chatkiterror.errortype.otperrortype.md)
* [ServerErrorType](../enums/sayhey.chatkiterror.errortype.servererrortype.md)
* [SmsConversationErrorType](../enums/sayhey.chatkiterror.errortype.smsconversationerrortype.md)
* [SmsConversationInvitationErrorType](../enums/sayhey.chatkiterror.errortype.smsconversationinvitationerrortype.md)
* [SmsErrorType](../enums/sayhey.chatkiterror.errortype.smserrortype.md)
* [UserAdminErrorType](../enums/sayhey.chatkiterror.errortype.useradminerrortype.md)
* [UserAdminUpdateRoleType](../enums/sayhey.chatkiterror.errortype.useradminupdateroletype.md)
* [UserErrorType](../enums/sayhey.chatkiterror.errortype.usererrortype.md)
* [UserManagementErrorType](../enums/sayhey.chatkiterror.errortype.usermanagementerrortype.md)
* [UserOnboardingJobErrorType](../enums/sayhey.chatkiterror.errortype.useronboardingjoberrortype.md)
* [UserRegistrationErrorType](../enums/sayhey.chatkiterror.errortype.userregistrationerrortype.md)

### Type aliases

* [Type](sayhey.chatkiterror.errortype.md#markdown-header-type)

## Type aliases

###  Type

Ƭ **Type**: *[InternalErrorType](../enums/sayhey.chatkiterror.errortype.internalerrortype.md) | [AuthErrorType](../enums/sayhey.chatkiterror.errortype.autherrortype.md) | [FileErrorType](../enums/sayhey.chatkiterror.errortype.fileerrortype.md) | [ServerErrorType](../enums/sayhey.chatkiterror.errortype.servererrortype.md) | [GeneralErrorType](../enums/sayhey.chatkiterror.errortype.generalerrortype.md) | [MessageErrorType](../enums/sayhey.chatkiterror.errortype.messageerrortype.md) | [UserErrorType](../enums/sayhey.chatkiterror.errortype.usererrortype.md) | [UserRegistrationErrorType](../enums/sayhey.chatkiterror.errortype.userregistrationerrortype.md) | [ConversationErrorType](../enums/sayhey.chatkiterror.errortype.conversationerrortype.md) | [UserManagementErrorType](../enums/sayhey.chatkiterror.errortype.usermanagementerrortype.md) | [DateErrorType](../enums/sayhey.chatkiterror.errortype.dateerrortype.md) | [AppRegistrationErrorType](../enums/sayhey.chatkiterror.errortype.appregistrationerrortype.md) | [OTPErrorType](../enums/sayhey.chatkiterror.errortype.otperrortype.md) | [EmailErrorType](../enums/sayhey.chatkiterror.errortype.emailerrortype.md) | [DepartmentErrorTpe](../enums/sayhey.chatkiterror.errortype.departmenterrortpe.md) | [DivisionErrorType](../enums/sayhey.chatkiterror.errortype.divisionerrortype.md) | [AppFeatureErrorTpe](../enums/sayhey.chatkiterror.errortype.appfeatureerrortpe.md) | [UserAdminErrorType](../enums/sayhey.chatkiterror.errortype.useradminerrortype.md) | [AppConfigErrorType](../enums/sayhey.chatkiterror.errortype.appconfigerrortype.md) | [KeywordErrorType](../enums/sayhey.chatkiterror.errortype.keyworderrortype.md) | [UserAdminUpdateRoleType](../enums/sayhey.chatkiterror.errortype.useradminupdateroletype.md) | [FlaggedMessageErrorType](../enums/sayhey.chatkiterror.errortype.flaggedmessageerrortype.md) | [UserOnboardingJobErrorType](../enums/sayhey.chatkiterror.errortype.useronboardingjoberrortype.md) | [ExternalUserErrorType](../enums/sayhey.chatkiterror.errortype.externalusererrortype.md) | [ContactErrorType](../enums/sayhey.chatkiterror.errortype.contacterrortype.md) | [SmsConversationErrorType](../enums/sayhey.chatkiterror.errortype.smsconversationerrortype.md) | [PushNotificationError](../enums/sayhey.chatkiterror.pushnotificationerror.md) | [SmsErrorType](../enums/sayhey.chatkiterror.errortype.smserrortype.md) | [SmsConversationInvitationErrorType](../enums/sayhey.chatkiterror.errortype.smsconversationinvitationerrortype.md)*

Defined in types.ts:1344
