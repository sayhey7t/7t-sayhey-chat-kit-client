[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](publisher.md) › [Event](publisher.event.md)

# Namespace: Event

## Index

### Interfaces

* [ConversationCreated](../interfaces/publisher.event.conversationcreated.md)
* [ConversationGroupInfoChanged](../interfaces/publisher.event.conversationgroupinfochanged.md)
* [ConversationGroupMemberAdded](../interfaces/publisher.event.conversationgroupmemberadded.md)
* [ConversationGroupMemberRemoved](../interfaces/publisher.event.conversationgroupmemberremoved.md)
* [ConversationGroupOwnerAdded](../interfaces/publisher.event.conversationgroupowneradded.md)
* [ConversationGroupOwnerRemoved](../interfaces/publisher.event.conversationgroupownerremoved.md)
* [ConversationHideAndClear](../interfaces/publisher.event.conversationhideandclear.md)
* [ConversationHideAndClearAll](../interfaces/publisher.event.conversationhideandclearall.md)
* [ConversationMuted](../interfaces/publisher.event.conversationmuted.md)
* [ConversationPinned](../interfaces/publisher.event.conversationpinned.md)
* [ConversationQuickGroupChangedToGroup](../interfaces/publisher.event.conversationquickgroupchangedtogroup.md)
* [ConversationUnmuted](../interfaces/publisher.event.conversationunmuted.md)
* [ConversationUnpinned](../interfaces/publisher.event.conversationunpinned.md)
* [ExternalUserConversationInvitationAccepted](../interfaces/publisher.event.externaluserconversationinvitationaccepted.md)
* [ExternalUserNudged](../interfaces/publisher.event.externalusernudged.md)
* [MessageDeleted](../interfaces/publisher.event.messagedeleted.md)
* [MessageReactionsUpdate](../interfaces/publisher.event.messagereactionsupdate.md)
* [MessageRead](../interfaces/publisher.event.messageread.md)
* [NewMessage](../interfaces/publisher.event.newmessage.md)
* [UserDeleted](../interfaces/publisher.event.userdeleted.md)
* [UserDisabled](../interfaces/publisher.event.userdisabled.md)
* [UserEnabled](../interfaces/publisher.event.userenabled.md)
* [UserInfoChanged](../interfaces/publisher.event.userinfochanged.md)
* [UserReactedToMessage](../interfaces/publisher.event.userreactedtomessage.md)
* [UserUnReactedToMessage](../interfaces/publisher.event.userunreactedtomessage.md)

### Type aliases

* [AllEvents](publisher.event.md#markdown-header-allevents)

## Type aliases

###  AllEvents

Ƭ **AllEvents**: *[NewMessage](../interfaces/publisher.event.newmessage.md) | [ConversationCreated](../interfaces/publisher.event.conversationcreated.md) | [ConversationMuted](../interfaces/publisher.event.conversationmuted.md) | [ConversationPinned](../interfaces/publisher.event.conversationpinned.md) | [ConversationUnmuted](../interfaces/publisher.event.conversationunmuted.md) | [ConversationUnpinned](../interfaces/publisher.event.conversationunpinned.md) | [ConversationHideAndClear](../interfaces/publisher.event.conversationhideandclear.md) | [ConversationHideAndClearAll](../interfaces/publisher.event.conversationhideandclearall.md) | [ConversationGroupInfoChanged](../interfaces/publisher.event.conversationgroupinfochanged.md) | [ConversationQuickGroupChangedToGroup](../interfaces/publisher.event.conversationquickgroupchangedtogroup.md) | [ExternalUserConversationInvitationAccepted](../interfaces/publisher.event.externaluserconversationinvitationaccepted.md) | [UserInfoChanged](../interfaces/publisher.event.userinfochanged.md) | [ConversationGroupOwnerAdded](../interfaces/publisher.event.conversationgroupowneradded.md) | [ConversationGroupOwnerRemoved](../interfaces/publisher.event.conversationgroupownerremoved.md) | [ConversationGroupMemberAdded](../interfaces/publisher.event.conversationgroupmemberadded.md) | [ConversationGroupMemberRemoved](../interfaces/publisher.event.conversationgroupmemberremoved.md) | [UserReactedToMessage](../interfaces/publisher.event.userreactedtomessage.md) | [UserUnReactedToMessage](../interfaces/publisher.event.userunreactedtomessage.md) | [MessageReactionsUpdate](../interfaces/publisher.event.messagereactionsupdate.md) | [UserDisabled](../interfaces/publisher.event.userdisabled.md) | [UserEnabled](../interfaces/publisher.event.userenabled.md) | [MessageDeleted](../interfaces/publisher.event.messagedeleted.md) | [UserDeleted](../interfaces/publisher.event.userdeleted.md) | [MessageRead](../interfaces/publisher.event.messageread.md) | [ExternalUserNudged](../interfaces/publisher.event.externalusernudged.md)*

Defined in types.ts:1004
