[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](sayhey.md) › [ChatKitError](sayhey.chatkiterror.md)

# Namespace: ChatKitError

## Index

### Namespaces

* [ErrorType](sayhey.chatkiterror.errortype.md)

### Enumerations

* [PushNotificationError](../enums/sayhey.chatkiterror.pushnotificationerror.md)

### Classes

* [AppError](../classes/sayhey.chatkiterror.apperror.md)

### Type aliases

* [Type](sayhey.chatkiterror.md#markdown-header-type)

## Type aliases

###  Type

Ƭ **Type**: *[Type](sayhey.chatkiterror.errortype.md#markdown-header-type) | [PushNotificationError](../enums/sayhey.chatkiterror.pushnotificationerror.md)*

Defined in types.ts:1629
