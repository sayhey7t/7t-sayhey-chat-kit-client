[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](publisher.md) › [Payload](publisher.payload.md)

# Namespace: Payload

## Index

### Interfaces

* [ConversationCreated](../interfaces/publisher.payload.conversationcreated.md)
* [ConversationGroupInfoChanged](../interfaces/publisher.payload.conversationgroupinfochanged.md)
* [ConversationGroupMemberAdded](../interfaces/publisher.payload.conversationgroupmemberadded.md)
* [ConversationGroupMemberRemoved](../interfaces/publisher.payload.conversationgroupmemberremoved.md)
* [ConversationGroupOwnerAdded](../interfaces/publisher.payload.conversationgroupowneradded.md)
* [ConversationGroupOwnerRemoved](../interfaces/publisher.payload.conversationgroupownerremoved.md)
* [ConversationHideAndClear](../interfaces/publisher.payload.conversationhideandclear.md)
* [ConversationHideAndClearAll](../interfaces/publisher.payload.conversationhideandclearall.md)
* [ConversationMuted](../interfaces/publisher.payload.conversationmuted.md)
* [ConversationPinned](../interfaces/publisher.payload.conversationpinned.md)
* [ConversationQuickGroupChangedToGroup](../interfaces/publisher.payload.conversationquickgroupchangedtogroup.md)
* [ConversationUnmuted](../interfaces/publisher.payload.conversationunmuted.md)
* [ConversationUnpinned](../interfaces/publisher.payload.conversationunpinned.md)
* [ExternalUserConversationInvitationAccepted](../interfaces/publisher.payload.externaluserconversationinvitationaccepted.md)
* [ExternalUserNudged](../interfaces/publisher.payload.externalusernudged.md)
* [MessageDeleted](../interfaces/publisher.payload.messagedeleted.md)
* [MessageReactionsUpdate](../interfaces/publisher.payload.messagereactionsupdate.md)
* [MessageRead](../interfaces/publisher.payload.messageread.md)
* [NewMessage](../interfaces/publisher.payload.newmessage.md)
* [UserDeleted](../interfaces/publisher.payload.userdeleted.md)
* [UserDisabled](../interfaces/publisher.payload.userdisabled.md)
* [UserEnabled](../interfaces/publisher.payload.userenabled.md)
* [UserInfoChanged](../interfaces/publisher.payload.userinfochanged.md)
* [UserReactedToMessage](../interfaces/publisher.payload.userreactedtomessage.md)
* [UserUnReactedToMessage](../interfaces/publisher.payload.userunreactedtomessage.md)
