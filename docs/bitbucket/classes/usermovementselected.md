[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserMovementSelected](usermovementselected.md)

# Class: UserMovementSelected

## Hierarchy

* **UserMovementSelected**

## Index

### Properties

* [assignmentType](usermovementselected.md#markdown-header-assignmenttype)
* [toUserAdminId](usermovementselected.md#markdown-header-touseradminid)
* [userIds](usermovementselected.md#markdown-header-userids)

## Properties

###  assignmentType

• **assignmentType**: *[UserManagementType](../enums/usermanagementtype.md)*

Defined in types.ts:552

___

###  toUserAdminId

• **toUserAdminId**: *string*

Defined in types.ts:550

___

###  userIds

• **userIds**: *string[]*

Defined in types.ts:548
