[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ReactionMapper](reactionmapper.md)

# Class: ReactionMapper

## Hierarchy

* **ReactionMapper**

## Index

### Methods

* [toDomianEntity](reactionmapper.md#markdown-header-static-todomianentity)

## Methods

### `Static` toDomianEntity

▸ **toDomianEntity**(`reactions`: [ConversationReactionsDTO](../interfaces/conversationreactionsdto.md)[]): *[ConversationReactionsDomainEntity](../globals.md#markdown-header-conversationreactionsdomainentity)*

Defined in mapper.ts:368

**Parameters:**

Name | Type |
------ | ------ |
`reactions` | [ConversationReactionsDTO](../interfaces/conversationreactionsdto.md)[] |

**Returns:** *[ConversationReactionsDomainEntity](../globals.md#markdown-header-conversationreactionsdomainentity)*
