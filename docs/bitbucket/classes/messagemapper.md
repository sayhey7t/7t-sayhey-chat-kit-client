[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [MessageMapper](messagemapper.md)

# Class: MessageMapper

## Hierarchy

* **MessageMapper**

## Index

### Methods

* [toDomainEntities](messagemapper.md#markdown-header-static-todomainentities)
* [toDomainEntity](messagemapper.md#markdown-header-static-todomainentity)
* [toPeekDomainEntities](messagemapper.md#markdown-header-static-topeekdomainentities)

## Methods

### `Static` toDomainEntities

▸ **toDomainEntities**(`messages`: [MessageDTO](../interfaces/messagedto.md)[]): *[MessageDomainEntity](../interfaces/messagedomainentity.md)[]*

Defined in mapper.ts:162

**Parameters:**

Name | Type |
------ | ------ |
`messages` | [MessageDTO](../interfaces/messagedto.md)[] |

**Returns:** *[MessageDomainEntity](../interfaces/messagedomainentity.md)[]*

___

### `Static` toDomainEntity

▸ **toDomainEntity**(`message`: [MessageDTO](../interfaces/messagedto.md)): *[MessageDomainEntity](../interfaces/messagedomainentity.md)*

Defined in mapper.ts:153

**Parameters:**

Name | Type |
------ | ------ |
`message` | [MessageDTO](../interfaces/messagedto.md) |

**Returns:** *[MessageDomainEntity](../interfaces/messagedomainentity.md)*

___

### `Static` toPeekDomainEntities

▸ **toPeekDomainEntities**(`messages`: [PeekMessageDTO](../interfaces/peekmessagedto.md)[]): *[PeekMessageDomainEntity](../interfaces/peekmessagedomainentity.md)[]*

Defined in mapper.ts:166

**Parameters:**

Name | Type |
------ | ------ |
`messages` | [PeekMessageDTO](../interfaces/peekmessagedto.md)[] |

**Returns:** *[PeekMessageDomainEntity](../interfaces/peekmessagedomainentity.md)[]*
