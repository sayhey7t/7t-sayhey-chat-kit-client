[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SentMessageMapper](sentmessagemapper.md)

# Class: SentMessageMapper

## Hierarchy

* **SentMessageMapper**

## Index

### Methods

* [toDomainEntities](sentmessagemapper.md#markdown-header-static-todomainentities)
* [toDomainEntity](sentmessagemapper.md#markdown-header-static-todomainentity)

## Methods

### `Static` toDomainEntities

▸ **toDomainEntities**(`messages`: [SentMessageDTO](../globals.md#markdown-header-sentmessagedto)[]): *[SentMessageDomainEntity](../globals.md#markdown-header-sentmessagedomainentity)[]*

Defined in mapper.ts:147

**Parameters:**

Name | Type |
------ | ------ |
`messages` | [SentMessageDTO](../globals.md#markdown-header-sentmessagedto)[] |

**Returns:** *[SentMessageDomainEntity](../globals.md#markdown-header-sentmessagedomainentity)[]*

___

### `Static` toDomainEntity

▸ **toDomainEntity**(`message`: [SentMessageDTO](../globals.md#markdown-header-sentmessagedto)): *object*

Defined in mapper.ts:112

**Parameters:**

Name | Type |
------ | ------ |
`message` | [SentMessageDTO](../globals.md#markdown-header-sentmessagedto) |

**Returns:** *object*
