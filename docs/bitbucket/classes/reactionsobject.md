[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ReactionsObject](reactionsobject.md)

# Class: ReactionsObject

## Hierarchy

* **ReactionsObject**

## Index

### Properties

* [[ReactionType.Clap]](reactionsobject.md#markdown-header-[reactiontype.clap])
* [[ReactionType.Exclamation]](reactionsobject.md#markdown-header-[reactiontype.exclamation])
* [[ReactionType.HaHa]](reactionsobject.md#markdown-header-[reactiontype.haha])
* [[ReactionType.Heart]](reactionsobject.md#markdown-header-[reactiontype.heart])
* [[ReactionType.Thumb]](reactionsobject.md#markdown-header-[reactiontype.thumb])

## Properties

###  [ReactionType.Clap]

• **[ReactionType.Clap]**: *number* = 0

Defined in types.ts:47

___

###  [ReactionType.Exclamation]

• **[ReactionType.Exclamation]**: *number* = 0

Defined in types.ts:51

___

###  [ReactionType.HaHa]

• **[ReactionType.HaHa]**: *number* = 0

Defined in types.ts:49

___

###  [ReactionType.Heart]

• **[ReactionType.Heart]**: *number* = 0

Defined in types.ts:45

___

###  [ReactionType.Thumb]

• **[ReactionType.Thumb]**: *number* = 0

Defined in types.ts:43
