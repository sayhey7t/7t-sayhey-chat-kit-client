[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SystemMessageMapper](systemmessagemapper.md)

# Class: SystemMessageMapper

## Hierarchy

* **SystemMessageMapper**

## Index

### Methods

* [toDomainEntity](systemmessagemapper.md#markdown-header-static-todomainentity)

## Methods

### `Static` toDomainEntity

▸ **toDomainEntity**(`systemMessage`: [SystemMessageDTO](../globals.md#markdown-header-systemmessagedto)): *[SystemMessageDomainEntity](../globals.md#markdown-header-systemmessagedomainentity)*

Defined in mapper.ts:353

**Parameters:**

Name | Type |
------ | ------ |
`systemMessage` | [SystemMessageDTO](../globals.md#markdown-header-systemmessagedto) |

**Returns:** *[SystemMessageDomainEntity](../globals.md#markdown-header-systemmessagedomainentity)*
