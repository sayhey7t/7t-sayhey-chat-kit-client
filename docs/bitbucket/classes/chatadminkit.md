[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ChatAdminKit](chatadminkit.md)

# Class: ChatAdminKit

## Hierarchy

* **ChatAdminKit**

## Index

### Properties

* [_apiVersion](chatadminkit.md#markdown-header-private-_apiversion)
* [_initialized](chatadminkit.md#markdown-header-private-_initialized)
* [_isUserAuthenticated](chatadminkit.md#markdown-header-private-_isuserauthenticated)
* [_subscribed](chatadminkit.md#markdown-header-private-_subscribed)
* [accessToken](chatadminkit.md#markdown-header-accesstoken)
* [apiKey](chatadminkit.md#markdown-header-apikey)
* [appId](chatadminkit.md#markdown-header-appid)
* [authDetailChangeListener](chatadminkit.md#markdown-header-optional-authdetailchangelistener)
* [disableAutoRefreshToken](chatadminkit.md#markdown-header-private-optional-disableautorefreshtoken)
* [exchangeTokenPromise](chatadminkit.md#markdown-header-private-exchangetokenpromise)
* [httpClient](chatadminkit.md#markdown-header-httpclient)
* [loginId](chatadminkit.md#markdown-header-private-loginid)
* [refreshToken](chatadminkit.md#markdown-header-refreshtoken)
* [serverUrl](chatadminkit.md#markdown-header-serverurl)
* [sessionExpiredListener](chatadminkit.md#markdown-header-optional-sessionexpiredlistener)

### Methods

* [addListeners](chatadminkit.md#markdown-header-addlisteners)
* [addUsersToConversation](chatadminkit.md#markdown-header-adduserstoconversation)
* [assignManagedUsers](chatadminkit.md#markdown-header-assignmanagedusers)
* [createConversationSpaceGroupDivision](chatadminkit.md#markdown-header-createconversationspacegroupdivision)
* [createDepartment](chatadminkit.md#markdown-header-createdepartment)
* [createDivision](chatadminkit.md#markdown-header-createdivision)
* [createGroupConversation](chatadminkit.md#markdown-header-creategroupconversation)
* [createKeyword](chatadminkit.md#markdown-header-createkeyword)
* [createKeywordBulk](chatadminkit.md#markdown-header-createkeywordbulk)
* [createUpdateUserAdminSearch](chatadminkit.md#markdown-header-createupdateuseradminsearch)
* [deleteConversationSpaceGroupDivision](chatadminkit.md#markdown-header-deleteconversationspacegroupdivision)
* [deleteGroupConversation](chatadminkit.md#markdown-header-deletegroupconversation)
* [deleteKeyword](chatadminkit.md#markdown-header-deletekeyword)
* [disableAdmin](chatadminkit.md#markdown-header-disableadmin)
* [disableUser](chatadminkit.md#markdown-header-disableuser)
* [downloadAuditMessages](chatadminkit.md#markdown-header-downloadauditmessages)
* [downloadSampleFile](chatadminkit.md#markdown-header-downloadsamplefile)
* [enableAdmin](chatadminkit.md#markdown-header-enableadmin)
* [enableUser](chatadminkit.md#markdown-header-enableuser)
* [exchangeToken](chatadminkit.md#markdown-header-exchangetoken)
* [exchangeTokenOnce](chatadminkit.md#markdown-header-exchangetokenonce)
* [forgotPassword](chatadminkit.md#markdown-header-forgotpassword)
* [getAllErroredUserOnboardingJobRecords](chatadminkit.md#markdown-header-getallerroreduseronboardingjobrecords)
* [getAllManagedUsers](chatadminkit.md#markdown-header-getallmanagedusers)
* [getAllManagedUsersCount](chatadminkit.md#markdown-header-getallmanageduserscount)
* [getAuditMessagesBatch](chatadminkit.md#markdown-header-getauditmessagesbatch)
* [getConversationDetails](chatadminkit.md#markdown-header-getconversationdetails)
* [getConversationUsers](chatadminkit.md#markdown-header-getconversationusers)
* [getDepartments](chatadminkit.md#markdown-header-getdepartments)
* [getDivisions](chatadminkit.md#markdown-header-getdivisions)
* [getEligibleRolesToInviteUserAdmin](chatadminkit.md#markdown-header-geteligiblerolestoinviteuseradmin)
* [getFlaggedMessageDetails](chatadminkit.md#markdown-header-getflaggedmessagedetails)
* [getFlaggedMessagesBatch](chatadminkit.md#markdown-header-getflaggedmessagesbatch)
* [getFlaggedMessagesCount](chatadminkit.md#markdown-header-getflaggedmessagescount)
* [getFlaggedMessagesKeywordScansBatch](chatadminkit.md#markdown-header-getflaggedmessageskeywordscansbatch)
* [getFlaggedMessagesResolutionKeywordScansBatch](chatadminkit.md#markdown-header-getflaggedmessagesresolutionkeywordscansbatch)
* [getFlaggedMessagesResolutionUserReportsBatch](chatadminkit.md#markdown-header-getflaggedmessagesresolutionuserreportsbatch)
* [getFlaggedMessagesResolutionsBatch](chatadminkit.md#markdown-header-getflaggedmessagesresolutionsbatch)
* [getFlaggedMessagesUserReportsBatch](chatadminkit.md#markdown-header-getflaggedmessagesuserreportsbatch)
* [getGroupConversationBatch](chatadminkit.md#markdown-header-getgroupconversationbatch)
* [getGroupDefaultImageSet](chatadminkit.md#markdown-header-getgroupdefaultimageset)
* [getKeywordsBatch](chatadminkit.md#markdown-header-getkeywordsbatch)
* [getMediaDirectUrl](chatadminkit.md#markdown-header-getmediadirecturl)
* [getMediaSourceDetails](chatadminkit.md#markdown-header-getmediasourcedetails)
* [getMessageContext](chatadminkit.md#markdown-header-getmessagecontext)
* [getMessagesAfterSequence](chatadminkit.md#markdown-header-getmessagesaftersequence)
* [getMessagesBeforeSequence](chatadminkit.md#markdown-header-getmessagesbeforesequence)
* [getMyUsersCount](chatadminkit.md#markdown-header-getmyuserscount)
* [getReachableUsers](chatadminkit.md#markdown-header-getreachableusers)
* [getReactedUsers](chatadminkit.md#markdown-header-getreactedusers)
* [getResolvedFlaggedMessagesCount](chatadminkit.md#markdown-header-getresolvedflaggedmessagescount)
* [getSentMessagesCountDaily](chatadminkit.md#markdown-header-getsentmessagescountdaily)
* [getSentMessagesCountTotal](chatadminkit.md#markdown-header-getsentmessagescounttotal)
* [getSpaceGroupDivisions](chatadminkit.md#markdown-header-getspacegroupdivisions)
* [getTotalUsersCount](chatadminkit.md#markdown-header-gettotaluserscount)
* [getUnmanagedUsers](chatadminkit.md#markdown-header-getunmanagedusers)
* [getUnmanagedUsersCount](chatadminkit.md#markdown-header-getunmanageduserscount)
* [getUnresolvedFlaggedMessagesCount](chatadminkit.md#markdown-header-getunresolvedflaggedmessagescount)
* [getUnresolvedFlaggedMessagesCountDaily](chatadminkit.md#markdown-header-getunresolvedflaggedmessagescountdaily)
* [getUserAdminSearch](chatadminkit.md#markdown-header-getuseradminsearch)
* [getUserAdmins](chatadminkit.md#markdown-header-getuseradmins)
* [getUserAdminsSelfInfo](chatadminkit.md#markdown-header-getuseradminsselfinfo)
* [getUserBasicInfoBatch](chatadminkit.md#markdown-header-getuserbasicinfobatch)
* [getUserOnboardingJobBatch](chatadminkit.md#markdown-header-getuseronboardingjobbatch)
* [guard](chatadminkit.md#markdown-header-private-guard)
* [guardWithAuth](chatadminkit.md#markdown-header-private-guardwithauth)
* [initialize](chatadminkit.md#markdown-header-initialize)
* [invite](chatadminkit.md#markdown-header-invite)
* [isUserAuthenticated](chatadminkit.md#markdown-header-isuserauthenticated)
* [makeConversationOwner](chatadminkit.md#markdown-header-makeconversationowner)
* [moveAllManagedUsers](chatadminkit.md#markdown-header-moveallmanagedusers)
* [moveBackAllManagedUsers](chatadminkit.md#markdown-header-movebackallmanagedusers)
* [moveSelectedUsers](chatadminkit.md#markdown-header-moveselectedusers)
* [processUserOnboardingJobUpload](chatadminkit.md#markdown-header-processuseronboardingjobupload)
* [reInvite](chatadminkit.md#markdown-header-reinvite)
* [registerUser](chatadminkit.md#markdown-header-registeruser)
* [removeConversationOwner](chatadminkit.md#markdown-header-removeconversationowner)
* [removeGroupImage](chatadminkit.md#markdown-header-removegroupimage)
* [removeUsersFromConversation](chatadminkit.md#markdown-header-removeusersfromconversation)
* [resetPassword](chatadminkit.md#markdown-header-resetpassword)
* [resolveFlaggedMessage](chatadminkit.md#markdown-header-resolveflaggedmessage)
* [signIn](chatadminkit.md#markdown-header-signin)
* [signInWithToken](chatadminkit.md#markdown-header-signinwithtoken)
* [signOut](chatadminkit.md#markdown-header-signout)
* [signUp](chatadminkit.md#markdown-header-signup)
* [updateAuthDetails](chatadminkit.md#markdown-header-private-updateauthdetails)
* [updateDepartment](chatadminkit.md#markdown-header-updatedepartment)
* [updateDivision](chatadminkit.md#markdown-header-updatedivision)
* [updateGroupImage](chatadminkit.md#markdown-header-updategroupimage)
* [updateGroupInfo](chatadminkit.md#markdown-header-updategroupinfo)
* [updateGroupOwner](chatadminkit.md#markdown-header-updategroupowner)
* [updateKeyword](chatadminkit.md#markdown-header-updatekeyword)
* [updateUser](chatadminkit.md#markdown-header-updateuser)
* [updateUserAdminInfo](chatadminkit.md#markdown-header-updateuseradmininfo)
* [updateUserAdminOnLeave](chatadminkit.md#markdown-header-updateuseradminonleave)
* [updateUserAdminRole](chatadminkit.md#markdown-header-updateuseradminrole)
* [updateUserAdminsSelfInfo](chatadminkit.md#markdown-header-updateuseradminsselfinfo)
* [uploadFile](chatadminkit.md#markdown-header-private-uploadfile)
* [uploadImage](chatadminkit.md#markdown-header-uploadimage)

## Properties

### `Private` _apiVersion

• **_apiVersion**: *string* = API_VERSION

Defined in chat-admin-kit.ts:87

___

### `Private` _initialized

• **_initialized**: *boolean* = false

Defined in chat-admin-kit.ts:102

___

### `Private` _isUserAuthenticated

• **_isUserAuthenticated**: *boolean* = false

Defined in chat-admin-kit.ts:106

___

### `Private` _subscribed

• **_subscribed**: *boolean* = false

Defined in chat-admin-kit.ts:104

___

###  accessToken

• **accessToken**: *string* = ""

Defined in chat-admin-kit.ts:83

___

###  apiKey

• **apiKey**: *string* = ""

Defined in chat-admin-kit.ts:93

___

###  appId

• **appId**: *string* = ""

Defined in chat-admin-kit.ts:91

___

### `Optional` authDetailChangeListener

• **authDetailChangeListener**? : *undefined | function*

Defined in chat-admin-kit.ts:97

___

### `Private` `Optional` disableAutoRefreshToken

• **disableAutoRefreshToken**? : *undefined | false | true*

Defined in chat-admin-kit.ts:110

___

### `Private` exchangeTokenPromise

• **exchangeTokenPromise**: *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)›› | null* = null

Defined in chat-admin-kit.ts:108

___

###  httpClient

• **httpClient**: *AxiosInstance*

Defined in chat-admin-kit.ts:89

___

### `Private` loginId

• **loginId**: *number* = 0

Defined in chat-admin-kit.ts:112

___

###  refreshToken

• **refreshToken**: *string* = ""

Defined in chat-admin-kit.ts:85

___

###  serverUrl

• **serverUrl**: *string* = ""

Defined in chat-admin-kit.ts:95

___

### `Optional` sessionExpiredListener

• **sessionExpiredListener**? : *undefined | function*

Defined in chat-admin-kit.ts:100

NOTE: Will be called when refresh token exchange session API call has failed

## Methods

###  addListeners

▸ **addListeners**(`listeners`: object): *Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)›*

Defined in chat-admin-kit.ts:143

Listeners

**Parameters:**

▪ **listeners**: *object*

Name | Type |
------ | ------ |
`onAuthDetailChange` | function |
`onSessionExpired?` | undefined &#124; function |

**Returns:** *Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)›*

___

###  addUsersToConversation

▸ **addUsersToConversation**(`conversationId`: string, `userIds`: string[], `force?`: undefined | false | true): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2114

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userIds` | string[] |
`force?` | undefined &#124; false &#124; true |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  assignManagedUsers

▸ **assignManagedUsers**(`list`: object[]): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:761

**Parameters:**

Name | Type |
------ | ------ |
`list` | object[] |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createConversationSpaceGroupDivision

▸ **createConversationSpaceGroupDivision**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1883

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`divisionIds` | undefined &#124; string[] |
`memberIds` | undefined &#124; string[] |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createDepartment

▸ **createDepartment**(`name`: string): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1082

**Parameters:**

Name | Type |
------ | ------ |
`name` | string |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createDivision

▸ **createDivision**(`params`: object): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1153

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`departmentId` | string |
`name` | string |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createGroupConversation

▸ **createGroupConversation**(`params`: object): *Promise‹Result‹[ConversationGroupDTOForUserAdminWithUserCount](../interfaces/conversationgroupdtoforuseradminwithusercount.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1933

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`divisionIds?` | string[] |
`file?` | [FileUpload](../globals.md#markdown-header-fileupload) |
`groupName` | string |
`userIds?` | string[] |

**Returns:** *Promise‹Result‹[ConversationGroupDTOForUserAdminWithUserCount](../interfaces/conversationgroupdtoforuseradminwithusercount.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createKeyword

▸ **createKeyword**(`__namedParameters`: object): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1198

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`priority` | [KeyWordPriorityTypes](../enums/keywordprioritytypes.md) |
`term` | string |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createKeywordBulk

▸ **createKeywordBulk**(`list`: object[]): *Promise‹Result‹object[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1223

**Parameters:**

Name | Type |
------ | ------ |
`list` | object[] |

**Returns:** *Promise‹Result‹object[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createUpdateUserAdminSearch

▸ **createUpdateUserAdminSearch**(`userAdminSearchCriteria`: [UserSearchCriteria](../interfaces/usersearchcriteria.md)): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1542

**Parameters:**

Name | Type |
------ | ------ |
`userAdminSearchCriteria` | [UserSearchCriteria](../interfaces/usersearchcriteria.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  deleteConversationSpaceGroupDivision

▸ **deleteConversationSpaceGroupDivision**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1911

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`divisionIds` | string[] |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  deleteGroupConversation

▸ **deleteGroupConversation**(`params`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2025

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  deleteKeyword

▸ **deleteKeyword**(`id`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1243

**Parameters:**

Name | Type |
------ | ------ |
`id` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  disableAdmin

▸ **disableAdmin**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:531

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`userAdminId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  disableUser

▸ **disableUser**(`params`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1309

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  downloadAuditMessages

▸ **downloadAuditMessages**(`__namedParameters`: object): *Promise‹Result‹[AuditMessageDomainEntityDTO](../interfaces/auditmessagedomainentitydto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1470

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`endDate` | undefined &#124; string |
`hasAttachment` | undefined &#124; false &#124; true |
`hasFlagged` | undefined &#124; false &#124; true |
`startDate` | undefined &#124; string |
`text` | undefined &#124; string[] |
`textSearchCondition` | undefined &#124; [AND](../enums/querycondition.md#markdown-header-and) &#124; [OR](../enums/querycondition.md#markdown-header-or) |
`userId` | undefined &#124; string |

**Returns:** *Promise‹Result‹[AuditMessageDomainEntityDTO](../interfaces/auditmessagedomainentitydto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  downloadSampleFile

▸ **downloadSampleFile**(): *Promise‹Result‹Blob, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2522

**Returns:** *Promise‹Result‹Blob, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  enableAdmin

▸ **enableAdmin**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:549

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`userAdminId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  enableUser

▸ **enableUser**(`params`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1327

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  exchangeToken

▸ **exchangeToken**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:380

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  exchangeTokenOnce

▸ **exchangeTokenOnce**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:200

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  forgotPassword

▸ **forgotPassword**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:632

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`email` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getAllErroredUserOnboardingJobRecords

▸ **getAllErroredUserOnboardingJobRecords**(`__namedParameters`: object): *Promise‹Result‹Blob, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2538

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`userOnboardingJobId` | string |

**Returns:** *Promise‹Result‹Blob, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getAllManagedUsers

▸ **getAllManagedUsers**(`__namedParameters`: object): *Promise‹Result‹[UserManagementBatchDTO](../interfaces/usermanagementbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:826

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`search` | undefined &#124; null &#124; string |
`sort` | undefined &#124; [UserName](../enums/usermanagementsortfields.md#markdown-header-username) &#124; [UserEmail](../enums/usermanagementsortfields.md#markdown-header-useremail) &#124; [UserEmpCode](../enums/usermanagementsortfields.md#markdown-header-userempcode) &#124; [UserDepartment](../enums/usermanagementsortfields.md#markdown-header-userdepartment) &#124; [UserTitle](../enums/usermanagementsortfields.md#markdown-header-usertitle) &#124; [UserHrAdmin](../enums/usermanagementsortfields.md#markdown-header-userhradmin) |
`sortOrder` | undefined &#124; [ASC](../enums/sortordertype.md#markdown-header-asc) &#124; [DESC](../enums/sortordertype.md#markdown-header-desc) |
`userManagementType` | undefined &#124; [Perm](../enums/usermanagementtype.md#markdown-header-perm) &#124; [Temp](../enums/usermanagementtype.md#markdown-header-temp) |
`userStatus` | undefined &#124; [Enabled](../enums/userstatustype.md#markdown-header-enabled) &#124; [Disabled](../enums/userstatustype.md#markdown-header-disabled) &#124; [All](../enums/userstatustype.md#markdown-header-all) |

**Returns:** *Promise‹Result‹[UserManagementBatchDTO](../interfaces/usermanagementbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getAllManagedUsersCount

▸ **getAllManagedUsersCount**(`queryParams`: [UserManagementCountQueryParams](../interfaces/usermanagementcountqueryparams.md)): *Promise‹Result‹[UserManagementCountResultParams](../interfaces/usermanagementcountresultparams.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:862

**Parameters:**

Name | Type |
------ | ------ |
`queryParams` | [UserManagementCountQueryParams](../interfaces/usermanagementcountqueryparams.md) |

**Returns:** *Promise‹Result‹[UserManagementCountResultParams](../interfaces/usermanagementcountresultparams.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getAuditMessagesBatch

▸ **getAuditMessagesBatch**(`__namedParameters`: object): *Promise‹Result‹[AuditMessageDomainEntityBatchDTO](../interfaces/auditmessagedomainentitybatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1423

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`endDate` | undefined &#124; string |
`hasAttachment` | undefined &#124; false &#124; true |
`hasFlagged` | undefined &#124; false &#124; true |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`startDate` | undefined &#124; string |
`text` | undefined &#124; string[] |
`textSearchCondition` | undefined &#124; [AND](../enums/querycondition.md#markdown-header-and) &#124; [OR](../enums/querycondition.md#markdown-header-or) |
`userId` | undefined &#124; string |

**Returns:** *Promise‹Result‹[AuditMessageDomainEntityBatchDTO](../interfaces/auditmessagedomainentitybatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getConversationDetails

▸ **getConversationDetails**(`conversationId`: string): *Promise‹Result‹[ConversationDTOForUserAdmin](../interfaces/conversationdtoforuseradmin.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2269

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹[ConversationDTOForUserAdmin](../interfaces/conversationdtoforuseradmin.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getConversationUsers

▸ **getConversationUsers**(`__namedParameters`: object): *Promise‹Result‹[ConversationUsersForUserAdminBatch](../interfaces/conversationusersforuseradminbatch.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2081

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`includeRefIdInSearch` | undefined &#124; false &#124; true |
`limit` | undefined &#124; number |
`nextCursor` | undefined &#124; number |
`searchString` | undefined &#124; string |
`type` | undefined &#124; [All](../enums/conversationuserquerytype.md#markdown-header-all) &#124; [Current](../enums/conversationuserquerytype.md#markdown-header-current) &#124; [Removed](../enums/conversationuserquerytype.md#markdown-header-removed) |

**Returns:** *Promise‹Result‹[ConversationUsersForUserAdminBatch](../interfaces/conversationusersforuseradminbatch.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getDepartments

▸ **getDepartments**(`__namedParameters`: object): *Promise‹Result‹[DepartmentBatchDTO](../interfaces/departmentbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1052

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type | Default |
------ | ------ | ------ |
`includeDivisions` | boolean | false |
`limit` | undefined &#124; number | - |
`offset` | undefined &#124; number | - |
`search` | undefined &#124; null &#124; string | - |
`sort` | undefined &#124; [DepartmentName](../enums/departmentsortfields.md#markdown-header-departmentname) | - |
`sortOrder` | undefined &#124; [ASC](../enums/sortordertype.md#markdown-header-asc) &#124; [DESC](../enums/sortordertype.md#markdown-header-desc) | - |

**Returns:** *Promise‹Result‹[DepartmentBatchDTO](../interfaces/departmentbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getDivisions

▸ **getDivisions**(`__namedParameters`: object): *Promise‹Result‹[DivisionBatchDTO](../interfaces/divisionbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1123

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`departmentId` | string |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`search` | undefined &#124; null &#124; string |
`sort` | undefined &#124; [DivisionName](../enums/divisionsortfields.md#markdown-header-divisionname) |
`sortOrder` | undefined &#124; [ASC](../enums/sortordertype.md#markdown-header-asc) &#124; [DESC](../enums/sortordertype.md#markdown-header-desc) |

**Returns:** *Promise‹Result‹[DivisionBatchDTO](../interfaces/divisionbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getEligibleRolesToInviteUserAdmin

▸ **getEligibleRolesToInviteUserAdmin**(): *Promise‹Result‹[UserRole](../interfaces/userrole.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2430

**Returns:** *Promise‹Result‹[UserRole](../interfaces/userrole.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getFlaggedMessageDetails

▸ **getFlaggedMessageDetails**(`__namedParameters`: object): *Promise‹Result‹[FlaggedMessageDetailsDTO](../interfaces/flaggedmessagedetailsdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1659

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`flaggedMessageId` | string |

**Returns:** *Promise‹Result‹[FlaggedMessageDetailsDTO](../interfaces/flaggedmessagedetailsdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getFlaggedMessagesBatch

▸ **getFlaggedMessagesBatch**(`__namedParameters`: object): *Promise‹Result‹[FlaggedMessageBatchDTO](../interfaces/flaggedmessagebatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1576

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type | Default |
------ | ------ | ------ |
`flagType` | "high" &#124; "low" &#124; "user_reported" &#124; "all" | "all" |
`limit` | undefined &#124; number | - |
`offset` | undefined &#124; number | - |
`search` | undefined &#124; null &#124; string | - |
`status` | "all" &#124; "pending" &#124; "resolved" | "all" |

**Returns:** *Promise‹Result‹[FlaggedMessageBatchDTO](../interfaces/flaggedmessagebatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getFlaggedMessagesCount

▸ **getFlaggedMessagesCount**(): *Promise‹Result‹[FlaggedMessageCountDTO](../interfaces/flaggedmessagecountdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1609

**Returns:** *Promise‹Result‹[FlaggedMessageCountDTO](../interfaces/flaggedmessagecountdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getFlaggedMessagesKeywordScansBatch

▸ **getFlaggedMessagesKeywordScansBatch**(`__namedParameters`: object): *Promise‹Result‹[FlaggedMessageScanBatchDTO](../interfaces/flaggedmessagescanbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1711

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`flaggedMessageId` | string |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`status` | undefined &#124; "pending" &#124; "resolved" |

**Returns:** *Promise‹Result‹[FlaggedMessageScanBatchDTO](../interfaces/flaggedmessagescanbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getFlaggedMessagesResolutionKeywordScansBatch

▸ **getFlaggedMessagesResolutionKeywordScansBatch**(`__namedParameters`: object): *Promise‹Result‹[FlaggedMessageResolutionScanBatchDTO](../interfaces/flaggedmessageresolutionscanbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1799

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`resolutionId` | string |

**Returns:** *Promise‹Result‹[FlaggedMessageResolutionScanBatchDTO](../interfaces/flaggedmessageresolutionscanbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getFlaggedMessagesResolutionUserReportsBatch

▸ **getFlaggedMessagesResolutionUserReportsBatch**(`__namedParameters`: object): *Promise‹Result‹[FlaggedMessageResolutionUserReportBatchDTO](../interfaces/flaggedmessageresolutionuserreportbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1769

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`resolutionId` | string |

**Returns:** *Promise‹Result‹[FlaggedMessageResolutionUserReportBatchDTO](../interfaces/flaggedmessageresolutionuserreportbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getFlaggedMessagesResolutionsBatch

▸ **getFlaggedMessagesResolutionsBatch**(`__namedParameters`: object): *Promise‹Result‹[FlaggedMessageResolutionBatchDTO](../interfaces/flaggedmessageresolutionbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1741

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`flaggedMessageId` | string |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |

**Returns:** *Promise‹Result‹[FlaggedMessageResolutionBatchDTO](../interfaces/flaggedmessageresolutionbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getFlaggedMessagesUserReportsBatch

▸ **getFlaggedMessagesUserReportsBatch**(`__namedParameters`: object): *Promise‹Result‹[FlaggedMessageUserReportBatchDTO](../interfaces/flaggedmessageuserreportbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1681

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`flaggedMessageId` | string |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`status` | undefined &#124; "pending" &#124; "resolved" |

**Returns:** *Promise‹Result‹[FlaggedMessageUserReportBatchDTO](../interfaces/flaggedmessageuserreportbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getGroupConversationBatch

▸ **getGroupConversationBatch**(`__namedParameters`: object): *Promise‹Result‹[ConversationGroupBatchDTOForUserAdminWithUserCount](../interfaces/conversationgroupbatchdtoforuseradminwithusercount.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1995

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`search` | undefined &#124; string |

**Returns:** *Promise‹Result‹[ConversationGroupBatchDTOForUserAdminWithUserCount](../interfaces/conversationgroupbatchdtoforuseradminwithusercount.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getGroupDefaultImageSet

▸ **getGroupDefaultImageSet**(): *Promise‹Result‹[DefaultGroupImageUrlSet](../interfaces/defaultgroupimageurlset.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2202

**Returns:** *Promise‹Result‹[DefaultGroupImageUrlSet](../interfaces/defaultgroupimageurlset.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getKeywordsBatch

▸ **getKeywordsBatch**(`__namedParameters`: object): *Promise‹Result‹[KeywordBatchDTO](../interfaces/keywordbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1279

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`priority` | undefined &#124; [High](../enums/keywordprioritytypes.md#markdown-header-high) &#124; [Low](../enums/keywordprioritytypes.md#markdown-header-low) |
`search` | undefined &#124; null &#124; string |

**Returns:** *Promise‹Result‹[KeywordBatchDTO](../interfaces/keywordbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getMediaDirectUrl

▸ **getMediaDirectUrl**(`params`: object): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:283

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`size?` | [ImageSizes](../enums/imagesizes.md) |
`urlPath` | string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getMediaSourceDetails

▸ **getMediaSourceDetails**(`urlPath`: string): *object*

Defined in chat-admin-kit.ts:273

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`urlPath` | string | url path returned from SayHey  |

**Returns:** *object*

* **method**: *string* = "GET"

* **uri**: *string* = `${this.serverUrl}${urlPath}`

* ### **headers**: *object*

  * **Accept**: *string* = "image/*, video/*, audio/*"

  * **Authorization**: *string* = `Bearer ${this.accessToken}`

  * **[ApiKeyName]**: *string* = this.apiKey

___

###  getMessageContext

▸ **getMessageContext**(`__namedParameters`: object): *Promise‹Result‹[PeekMessageDomainEntity](../interfaces/peekmessagedomainentity.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1395

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`messageId` | string |
`peekAfterLimit` | undefined &#124; number |
`peekBeforeLimit` | undefined &#124; number |

**Returns:** *Promise‹Result‹[PeekMessageDomainEntity](../interfaces/peekmessagedomainentity.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getMessagesAfterSequence

▸ **getMessagesAfterSequence**(`__namedParameters`: object): *Promise‹Result‹[PeekMessageDomainEntity](../interfaces/peekmessagedomainentity.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1369

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type | Default |
------ | ------ | ------ |
`afterSequence` | undefined &#124; number | - |
`conversationId` | string | - |
`limit` | number | 20 |

**Returns:** *Promise‹Result‹[PeekMessageDomainEntity](../interfaces/peekmessagedomainentity.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getMessagesBeforeSequence

▸ **getMessagesBeforeSequence**(`__namedParameters`: object): *Promise‹Result‹[PeekMessageDomainEntity](../interfaces/peekmessagedomainentity.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1343

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type | Default |
------ | ------ | ------ |
`beforeSequence` | undefined &#124; number | - |
`conversationId` | string | - |
`limit` | number | 20 |

**Returns:** *Promise‹Result‹[PeekMessageDomainEntity](../interfaces/peekmessagedomainentity.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getMyUsersCount

▸ **getMyUsersCount**(): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2305

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getReachableUsers

▸ **getReachableUsers**(`__namedParameters`: object): *Promise‹Result‹[ConversableUsersForUserAdminBatchResult](../interfaces/conversableusersforuseradminbatchresult.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2043

**Parameters:**

▪`Default value`  **__namedParameters**: *object*= {}

Name | Type |
------ | ------ |
`includeRefIdInSearch` | undefined &#124; false &#124; true |
`limit` | undefined &#124; number |
`nextCursor` | undefined &#124; number |
`searchString` | undefined &#124; string |

**Returns:** *Promise‹Result‹[ConversableUsersForUserAdminBatchResult](../interfaces/conversableusersforuseradminbatchresult.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getReactedUsers

▸ **getReactedUsers**(`__namedParameters`: object): *Promise‹Result‹[ReactedUserDTO](../interfaces/reacteduserdto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1829

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`afterSequence` | null &#124; number |
`limit` | number |
`messageId` | string |
`type` | undefined &#124; [Thumb](../enums/reactiontype.md#markdown-header-thumb) &#124; [Heart](../enums/reactiontype.md#markdown-header-heart) &#124; [Clap](../enums/reactiontype.md#markdown-header-clap) &#124; [HaHa](../enums/reactiontype.md#markdown-header-haha) &#124; [Exclamation](../enums/reactiontype.md#markdown-header-exclamation) |

**Returns:** *Promise‹Result‹[ReactedUserDTO](../interfaces/reacteduserdto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getResolvedFlaggedMessagesCount

▸ **getResolvedFlaggedMessagesCount**(`timeFrame?`: [TimeFrameType](../enums/timeframetype.md)): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2324

**Parameters:**

Name | Type |
------ | ------ |
`timeFrame?` | [TimeFrameType](../enums/timeframetype.md) |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getSentMessagesCountDaily

▸ **getSentMessagesCountDaily**(): *Promise‹Result‹[SentMessageCountDailyDTO](../interfaces/sentmessagecountdailydto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2346

**Returns:** *Promise‹Result‹[SentMessageCountDailyDTO](../interfaces/sentmessagecountdailydto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getSentMessagesCountTotal

▸ **getSentMessagesCountTotal**(`timeFrame?`: [TimeFrameType](../enums/timeframetype.md)): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2365

**Parameters:**

Name | Type |
------ | ------ |
`timeFrame?` | [TimeFrameType](../enums/timeframetype.md) |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getSpaceGroupDivisions

▸ **getSpaceGroupDivisions**(`__namedParameters`: object): *Promise‹Result‹[DepartmentDivisionDTO](../interfaces/departmentdivisiondto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1860

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹[DepartmentDivisionDTO](../interfaces/departmentdivisiondto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getTotalUsersCount

▸ **getTotalUsersCount**(): *Promise‹Result‹[TotalUsersCountDTO](../interfaces/totaluserscountdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2286

**Returns:** *Promise‹Result‹[TotalUsersCountDTO](../interfaces/totaluserscountdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUnmanagedUsers

▸ **getUnmanagedUsers**(`__namedParameters`: object): *Promise‹Result‹[UnmanagedUsersBatchDTO](../interfaces/unmanagedusersbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:904

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`search` | undefined &#124; null &#124; string |
`sort` | undefined &#124; [UserName](../enums/unmanageduserssortfields.md#markdown-header-username) &#124; [UserEmail](../enums/unmanageduserssortfields.md#markdown-header-useremail) &#124; [UserEmpCode](../enums/unmanageduserssortfields.md#markdown-header-userempcode) &#124; [UserDepartment](../enums/unmanageduserssortfields.md#markdown-header-userdepartment) &#124; [UserTitle](../enums/unmanageduserssortfields.md#markdown-header-usertitle) |
`sortOrder` | undefined &#124; 1 &#124; -1 |
`userStatus` | undefined &#124; [Enabled](../enums/userstatustype.md#markdown-header-enabled) &#124; [Disabled](../enums/userstatustype.md#markdown-header-disabled) &#124; [All](../enums/userstatustype.md#markdown-header-all) |

**Returns:** *Promise‹Result‹[UnmanagedUsersBatchDTO](../interfaces/unmanagedusersbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUnmanagedUsersCount

▸ **getUnmanagedUsersCount**(): *Promise‹Result‹[UnmanagedUsersCountDTO](../interfaces/unmanageduserscountdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:938

**Returns:** *Promise‹Result‹[UnmanagedUsersCountDTO](../interfaces/unmanageduserscountdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUnresolvedFlaggedMessagesCount

▸ **getUnresolvedFlaggedMessagesCount**(`unresolvedMsgScope?`: [UnresolvedMsgScopeType](../enums/unresolvedmsgscopetype.md)): *Promise‹Result‹[UnresolvedFlaggedMessagesCountDTO](../interfaces/unresolvedflaggedmessagescountdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2387

**Parameters:**

Name | Type |
------ | ------ |
`unresolvedMsgScope?` | [UnresolvedMsgScopeType](../enums/unresolvedmsgscopetype.md) |

**Returns:** *Promise‹Result‹[UnresolvedFlaggedMessagesCountDTO](../interfaces/unresolvedflaggedmessagescountdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUnresolvedFlaggedMessagesCountDaily

▸ **getUnresolvedFlaggedMessagesCountDaily**(): *Promise‹Result‹[AllFlaggedMessageCountDailyDTO](../interfaces/allflaggedmessagecountdailydto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2409

**Returns:** *Promise‹Result‹[AllFlaggedMessageCountDailyDTO](../interfaces/allflaggedmessagecountdailydto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUserAdminSearch

▸ **getUserAdminSearch**(): *Promise‹Result‹[UserAdminSearchDTO](../interfaces/useradminsearchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1559

**Returns:** *Promise‹Result‹[UserAdminSearchDTO](../interfaces/useradminsearchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUserAdmins

▸ **getUserAdmins**(`__namedParameters`: object): *Promise‹Result‹[UserAdminBatchDTO](../interfaces/useradminbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:677

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`role` | undefined &#124; [HrAdmin](../enums/usertypes.md#markdown-header-hradmin) &#124; [SuperHrAdmin](../enums/usertypes.md#markdown-header-superhradmin) |
`search` | undefined &#124; null &#124; string |
`sort` | undefined &#124; [UserAdminName](../enums/useradminsortfields.md#markdown-header-useradminname) &#124; [UserAdminEmail](../enums/useradminsortfields.md#markdown-header-useradminemail) &#124; [UserAdminRole](../enums/useradminsortfields.md#markdown-header-useradminrole) |
`sortOrder` | undefined &#124; [ASC](../enums/sortordertype.md#markdown-header-asc) &#124; [DESC](../enums/sortordertype.md#markdown-header-desc) |
`userAdminStatus` | undefined &#124; [Enabled](../enums/useradminstatustype.md#markdown-header-enabled) &#124; [Disabled](../enums/useradminstatustype.md#markdown-header-disabled) &#124; [All](../enums/useradminstatustype.md#markdown-header-all) |

**Returns:** *Promise‹Result‹[UserAdminBatchDTO](../interfaces/useradminbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUserAdminsSelfInfo

▸ **getUserAdminsSelfInfo**(): *Promise‹Result‹[UserAdminDTO](../interfaces/useradmindto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:713

**Returns:** *Promise‹Result‹[UserAdminDTO](../interfaces/useradmindto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUserBasicInfoBatch

▸ **getUserBasicInfoBatch**(`__namedParameters`: object): *Promise‹Result‹[UserBasicInfoBatchDTO](../interfaces/userbasicinfobatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1511

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`search` | undefined &#124; null &#124; string |

**Returns:** *Promise‹Result‹[UserBasicInfoBatchDTO](../interfaces/userbasicinfobatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUserOnboardingJobBatch

▸ **getUserOnboardingJobBatch**(`__namedParameters`: object): *Promise‹Result‹[UserOnboardingJobBatchDTO](../interfaces/useronboardingjobbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2494

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`limit` | undefined &#124; number |
`offset` | undefined &#124; number |
`search` | undefined &#124; null &#124; string |
`status` | undefined &#124; [UserOnboardingJobStatus](../enums/useronboardingjobstatus.md)[] |

**Returns:** *Promise‹Result‹[UserOnboardingJobBatchDTO](../interfaces/useronboardingjobbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Private` guard

▸ **guard**‹**T**›(`cb`: function): *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:167

**Type parameters:**

▪ **T**

**Parameters:**

▪ **cb**: *function*

Callback to be executed only if ChatKit is initialized and subscribed to

▸ (...`args`: any[]): *Promise‹T | [Error](../modules/sayhey.md#markdown-header-error)›*

**Parameters:**

Name | Type |
------ | ------ |
`...args` | any[] |

**Returns:** *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Private` guardWithAuth

▸ **guardWithAuth**‹**T**›(`cb`: function): *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:214

**Type parameters:**

▪ **T**

**Parameters:**

▪ **cb**: *function*

Callback to be called only when user is authenticated and will try to refresh token if access token expired

▸ (...`args`: any[]): *Promise‹T | [Error](../modules/sayhey.md#markdown-header-error)›*

**Parameters:**

Name | Type |
------ | ------ |
`...args` | any[] |

**Returns:** *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  initialize

▸ **initialize**(`params`: object): *void*

Defined in chat-admin-kit.ts:119

Setup methods

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`apiVersion?` | "v1" &#124; "v2" &#124; "v3" &#124; "v4" |
`disableAutoRefreshToken?` | undefined &#124; false &#124; true |
`sayheyApiKey` | string |
`sayheyAppId` | string |
`sayheyUrl` | string |

**Returns:** *void*

___

###  invite

▸ **invite**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:429

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`email` | string |
`firstName` | string |
`lastName` | string |
`role` | [HrAdmin](../enums/usertypes.md#markdown-header-hradmin) &#124; [SuperHrAdmin](../enums/usertypes.md#markdown-header-superhradmin) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  isUserAuthenticated

▸ **isUserAuthenticated**(): *boolean*

Defined in chat-admin-kit.ts:114

**Returns:** *boolean*

___

###  makeConversationOwner

▸ **makeConversationOwner**(`conversationId`: string, `userId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2237

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  moveAllManagedUsers

▸ **moveAllManagedUsers**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:781

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`assignmentType` | [UserManagementType](../enums/usermanagementtype.md) |
`fromUserAdminId` | string |
`toUserAdminId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  moveBackAllManagedUsers

▸ **moveBackAllManagedUsers**(`userAdminId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:809

**Parameters:**

Name | Type |
------ | ------ |
`userAdminId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  moveSelectedUsers

▸ **moveSelectedUsers**(`userMovementSelected`: [UserMovementSelected](usermovementselected.md)): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:884

**Parameters:**

Name | Type |
------ | ------ |
`userMovementSelected` | [UserMovementSelected](usermovementselected.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  processUserOnboardingJobUpload

▸ **processUserOnboardingJobUpload**(`params`: object): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2451

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`departmentId` | string |
`divisionId` | string |
`fileName` | string |
`fileUri` | string &#124; Blob |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  reInvite

▸ **reInvite**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:513

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`userAdminId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  registerUser

▸ **registerUser**(`__namedParameters`: object): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:961

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`departmentId` | string |
`divisionId` | string |
`email` | string |
`employeeId` | undefined &#124; string |
`firstName` | string |
`hrAdminId` | string |
`lastName` | string |
`title` | undefined &#124; string |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  removeConversationOwner

▸ **removeConversationOwner**(`conversationId`: string, `userId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2253

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  removeGroupImage

▸ **removeGroupImage**(`conversationId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2185

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  removeUsersFromConversation

▸ **removeUsersFromConversation**(`conversationId`: string, `userIds`: string[], `force?`: undefined | false | true): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2138

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userIds` | string[] |
`force?` | undefined &#124; false &#124; true |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  resetPassword

▸ **resetPassword**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:653

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`password` | string |
`resetToken` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  resolveFlaggedMessage

▸ **resolveFlaggedMessage**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1630

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`flaggedMessageId` | string |
`messageDeletion` | undefined &#124; false &#124; true |
`notifySuperAdmin` | undefined &#124; false &#124; true |
`resolutionNotes` | undefined &#124; string |
`resolutionStatus` | [ResolutionStatusType](../enums/resolutionstatustype.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  signIn

▸ **signIn**(`email`: string, `password`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:489

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |
`password` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  signInWithToken

▸ **signInWithToken**(`accessToken`: string, `refreshToken`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:417

**Parameters:**

Name | Type |
------ | ------ |
`accessToken` | string |
`refreshToken` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  signOut

▸ **signOut**(): *Promise‹void›*

Defined in chat-admin-kit.ts:411

**Returns:** *Promise‹void›*

___

###  signUp

▸ **signUp**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:460

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`invitationCode` | string |
`password` | string |
`userAdminId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Private` updateAuthDetails

▸ **updateAuthDetails**(`accessToken`: string, `refreshToken`: string): *Promise‹void›*

Defined in chat-admin-kit.ts:247

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`accessToken` | string | access token returned from SayHey that will be used to verify user with SayHey |
`refreshToken` | string | token returned from SayHey that will be used to refresh access token when it expired  |

**Returns:** *Promise‹void›*

___

###  updateDepartment

▸ **updateDepartment**(`params`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1100

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`departmentId` | string |
`name` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateDivision

▸ **updateDivision**(`params`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1174

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`departmentId` | string |
`divisionId` | string |
`name` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateGroupImage

▸ **updateGroupImage**(`conversationId`: string, `file`: [FileUpload](../globals.md#markdown-header-fileupload)): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2160

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`file` | [FileUpload](../globals.md#markdown-header-fileupload) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateGroupInfo

▸ **updateGroupInfo**(`conversationId`: string, `groupName`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:2216

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`groupName` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateGroupOwner

▸ **updateGroupOwner**(`params`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1976

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`userAdminId` | [KeyWordPriorityTypes](../enums/keywordprioritytypes.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateKeyword

▸ **updateKeyword**(`params`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1258

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`id` | string |
`priority` | [KeyWordPriorityTypes](../enums/keywordprioritytypes.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateUser

▸ **updateUser**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:1006

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`departmentId` | undefined &#124; string |
`divisionId` | undefined &#124; string |
`email` | undefined &#124; string |
`employeeId` | undefined &#124; string |
`firstName` | undefined &#124; string |
`lastName` | undefined &#124; string |
`title` | undefined &#124; string |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateUserAdminInfo

▸ **updateUserAdminInfo**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:567

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`email` | undefined &#124; string |
`firstName` | undefined &#124; string |
`lastName` | undefined &#124; string |
`userAdminId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateUserAdminOnLeave

▸ **updateUserAdminOnLeave**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:613

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`onLeave` | boolean |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateUserAdminRole

▸ **updateUserAdminRole**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:592

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`role` | [HrAdmin](../enums/usertypes.md#markdown-header-hradmin) &#124; [SuperHrAdmin](../enums/usertypes.md#markdown-header-superhradmin) |
`userAdminId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateUserAdminsSelfInfo

▸ **updateUserAdminsSelfInfo**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:732

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`firstName` | undefined &#124; string |
`lastName` | undefined &#124; string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Private` uploadFile

▸ **uploadFile**(`fileUri`: string | Blob, `fileName`: string, `progress?`: undefined | function, `thumbnailId?`: undefined | string): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:304

**Parameters:**

Name | Type |
------ | ------ |
`fileUri` | string &#124; Blob |
`fileName` | string |
`progress?` | undefined &#124; function |
`thumbnailId?` | undefined &#124; string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  uploadImage

▸ **uploadImage**(`fileUri`: string | Blob, `fileName`: string): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-admin-kit.ts:352

**Parameters:**

Name | Type |
------ | ------ |
`fileUri` | string &#124; Blob |
`fileName` | string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*
