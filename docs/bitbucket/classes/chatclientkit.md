[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ChatClientKit](chatclientkit.md)

# Class: ChatClientKit

## Hierarchy

* **ChatClientKit**

## Index

### Constructors

* [constructor](chatclientkit.md#markdown-header-constructor)

### Properties

* [_apiVersion](chatclientkit.md#markdown-header-private-_apiversion)
* [_initialized](chatclientkit.md#markdown-header-private-_initialized)
* [_isUserAuthenticated](chatclientkit.md#markdown-header-private-_isuserauthenticated)
* [_subscribed](chatclientkit.md#markdown-header-private-_subscribed)
* [accessToken](chatclientkit.md#markdown-header-accesstoken)
* [apiKey](chatclientkit.md#markdown-header-apikey)
* [appId](chatclientkit.md#markdown-header-appid)
* [authDetailChangeListener](chatclientkit.md#markdown-header-optional-authdetailchangelistener)
* [disableAutoRefreshToken](chatclientkit.md#markdown-header-private-optional-disableautorefreshtoken)
* [disableUserDeletedHandler](chatclientkit.md#markdown-header-private-optional-disableuserdeletedhandler)
* [disableUserDisabledHandler](chatclientkit.md#markdown-header-private-optional-disableuserdisabledhandler)
* [eventListener](chatclientkit.md#markdown-header-optional-eventlistener)
* [exchangeTokenPromise](chatclientkit.md#markdown-header-private-exchangetokenpromise)
* [httpClient](chatclientkit.md#markdown-header-httpclient)
* [instanceIdPushNotification](chatclientkit.md#markdown-header-instanceidpushnotification)
* [isSendingReadReceipt](chatclientkit.md#markdown-header-private-issendingreadreceipt)
* [loginId](chatclientkit.md#markdown-header-private-loginid)
* [publisherKitClient](chatclientkit.md#markdown-header-publisherkitclient)
* [publisherToken](chatclientkit.md#markdown-header-publishertoken)
* [readMessageMap](chatclientkit.md#markdown-header-private-readmessagemap)
* [readMessageSequenceMap](chatclientkit.md#markdown-header-readmessagesequencemap)
* [refreshToken](chatclientkit.md#markdown-header-refreshtoken)
* [serverUrl](chatclientkit.md#markdown-header-serverurl)
* [sessionExpiredListener](chatclientkit.md#markdown-header-optional-sessionexpiredlistener)
* [timerHandle](chatclientkit.md#markdown-header-private-timerhandle)

### Methods

* [acceptSmsConversationInvitation](chatclientkit.md#markdown-header-acceptsmsconversationinvitation)
* [acceptTermsAndPrivacy](chatclientkit.md#markdown-header-accepttermsandprivacy)
* [addListeners](chatclientkit.md#markdown-header-addlisteners)
* [addToContact](chatclientkit.md#markdown-header-addtocontact)
* [addUsersToConversation](chatclientkit.md#markdown-header-adduserstoconversation)
* [changePassword](chatclientkit.md#markdown-header-changepassword)
* [checkEmail](chatclientkit.md#markdown-header-checkemail)
* [checkForExistingConversation](chatclientkit.md#markdown-header-checkforexistingconversation)
* [checkSmsConversationInvitation](chatclientkit.md#markdown-header-checksmsconversationinvitation)
* [clientSignInWithToken](chatclientkit.md#markdown-header-clientsigninwithtoken)
* [createGroupConversation](chatclientkit.md#markdown-header-creategroupconversation)
* [createIndividualConversation](chatclientkit.md#markdown-header-createindividualconversation)
* [createNewContact](chatclientkit.md#markdown-header-createnewcontact)
* [createQuickConversation](chatclientkit.md#markdown-header-createquickconversation)
* [createSmsConversation](chatclientkit.md#markdown-header-createsmsconversation)
* [deleteAllConversations](chatclientkit.md#markdown-header-deleteallconversations)
* [deleteConversation](chatclientkit.md#markdown-header-deleteconversation)
* [deleteGroupConversation](chatclientkit.md#markdown-header-deletegroupconversation)
* [disconnectPublisher](chatclientkit.md#markdown-header-disconnectpublisher)
* [exchangeToken](chatclientkit.md#markdown-header-exchangetoken)
* [exchangeTokenOnce](chatclientkit.md#markdown-header-exchangetokenonce)
* [findExternalUserByPhone](chatclientkit.md#markdown-header-findexternaluserbyphone)
* [forgotPassword](chatclientkit.md#markdown-header-forgotpassword)
* [getAppConfig](chatclientkit.md#markdown-header-getappconfig)
* [getConversationBasicUsers](chatclientkit.md#markdown-header-getconversationbasicusers)
* [getConversationDetails](chatclientkit.md#markdown-header-getconversationdetails)
* [getConversationReachableUsers](chatclientkit.md#markdown-header-getconversationreachableusers)
* [getConversationUsers](chatclientkit.md#markdown-header-getconversationusers)
* [getConversations](chatclientkit.md#markdown-header-getconversations)
* [getExistingIndvidualOrQuickConversation](chatclientkit.md#markdown-header-getexistingindvidualorquickconversation)
* [getExistingSmsConversation](chatclientkit.md#markdown-header-getexistingsmsconversation)
* [getGalleryMessages](chatclientkit.md#markdown-header-getgallerymessages)
* [getGroupDefaultImageSet](chatclientkit.md#markdown-header-getgroupdefaultimageset)
* [getInternalUserContactsBatch](chatclientkit.md#markdown-header-getinternalusercontactsbatch)
* [getMediaDirectUrl](chatclientkit.md#markdown-header-getmediadirecturl)
* [getMediaSourceDetails](chatclientkit.md#markdown-header-getmediasourcedetails)
* [getMessages](chatclientkit.md#markdown-header-getmessages)
* [getOptToReceiveSmsStatus](chatclientkit.md#markdown-header-getopttoreceivesmsstatus)
* [getReachableUsers](chatclientkit.md#markdown-header-getreachableusers)
* [getReactedUsers](chatclientkit.md#markdown-header-getreactedusers)
* [getTermsAndPrivacy](chatclientkit.md#markdown-header-gettermsandprivacy)
* [getUserConversationMessagesReactions](chatclientkit.md#markdown-header-getuserconversationmessagesreactions)
* [getUserConversationReactions](chatclientkit.md#markdown-header-getuserconversationreactions)
* [getUserDetails](chatclientkit.md#markdown-header-getuserdetails)
* [guard](chatclientkit.md#markdown-header-private-guard)
* [guardWithAuth](chatclientkit.md#markdown-header-private-guardwithauth)
* [initialize](chatclientkit.md#markdown-header-initialize)
* [isSocketConnected](chatclientkit.md#markdown-header-issocketconnected)
* [isUserAuthenticated](chatclientkit.md#markdown-header-isuserauthenticated)
* [makeConversationOwner](chatclientkit.md#markdown-header-makeconversationowner)
* [markMessageRead](chatclientkit.md#markdown-header-private-markmessageread)
* [muteAllNotifications](chatclientkit.md#markdown-header-muteallnotifications)
* [muteConversation](chatclientkit.md#markdown-header-muteconversation)
* [nudgeExternalUserToConversation](chatclientkit.md#markdown-header-nudgeexternalusertoconversation)
* [onMessage](chatclientkit.md#markdown-header-private-onmessage)
* [optNotToReceiveSms](chatclientkit.md#markdown-header-optnottoreceivesms)
* [optToReceiveSms](chatclientkit.md#markdown-header-opttoreceivesms)
* [pinConversation](chatclientkit.md#markdown-header-pinconversation)
* [readMessage](chatclientkit.md#markdown-header-readmessage)
* [reconnectPublisher](chatclientkit.md#markdown-header-reconnectpublisher)
* [registerForPushNotification](chatclientkit.md#markdown-header-registerforpushnotification)
* [removeConversationOwner](chatclientkit.md#markdown-header-removeconversationowner)
* [removeGroupImage](chatclientkit.md#markdown-header-removegroupimage)
* [removeMessageReaction](chatclientkit.md#markdown-header-removemessagereaction)
* [removeUsersFromConversation](chatclientkit.md#markdown-header-removeusersfromconversation)
* [reportFlaggedMessage](chatclientkit.md#markdown-header-reportflaggedmessage)
* [requestPreverificationOTP](chatclientkit.md#markdown-header-requestpreverificationotp)
* [requestResetPasswordOTP](chatclientkit.md#markdown-header-requestresetpasswordotp)
* [requestSetPasswordOTP](chatclientkit.md#markdown-header-requestsetpasswordotp)
* [requestSignInExternalOTP](chatclientkit.md#markdown-header-requestsigninexternalotp)
* [requestSignUpOTP](chatclientkit.md#markdown-header-requestsignupotp)
* [resendSmsConversationInvitation](chatclientkit.md#markdown-header-resendsmsconversationinvitation)
* [resetPassword](chatclientkit.md#markdown-header-resetpassword)
* [sendMessage](chatclientkit.md#markdown-header-sendmessage)
* [setMessageReaction](chatclientkit.md#markdown-header-setmessagereaction)
* [setPassword](chatclientkit.md#markdown-header-setpassword)
* [signIn](chatclientkit.md#markdown-header-signin)
* [signInExternal](chatclientkit.md#markdown-header-signinexternal)
* [signInExternalByUserId](chatclientkit.md#markdown-header-signinexternalbyuserid)
* [signInWithToken](chatclientkit.md#markdown-header-signinwithtoken)
* [signOut](chatclientkit.md#markdown-header-signout)
* [signUp](chatclientkit.md#markdown-header-signup)
* [unmuteAllNotifications](chatclientkit.md#markdown-header-unmuteallnotifications)
* [unmuteConversation](chatclientkit.md#markdown-header-unmuteconversation)
* [unpinConversation](chatclientkit.md#markdown-header-unpinconversation)
* [unregisterForPushNotification](chatclientkit.md#markdown-header-unregisterforpushnotification)
* [updateAuthDetails](chatclientkit.md#markdown-header-private-updateauthdetails)
* [updateGroupImage](chatclientkit.md#markdown-header-updategroupimage)
* [updateGroupInfo](chatclientkit.md#markdown-header-updategroupinfo)
* [updateUserProfileImage](chatclientkit.md#markdown-header-updateuserprofileimage)
* [updateUserProfileInfo](chatclientkit.md#markdown-header-updateuserprofileinfo)
* [uploadFile](chatclientkit.md#markdown-header-private-uploadfile)
* [uploadImage](chatclientkit.md#markdown-header-uploadimage)
* [validatePreverificationOTP](chatclientkit.md#markdown-header-validatepreverificationotp)
* [validateResetPasswordOTP](chatclientkit.md#markdown-header-validateresetpasswordotp)
* [validateSetPasswordOTP](chatclientkit.md#markdown-header-validatesetpasswordotp)
* [validateSignUpOTP](chatclientkit.md#markdown-header-validatesignupotp)

## Constructors

###  constructor

\+ **new ChatClientKit**(): *[ChatClientKit](chatclientkit.md)*

Defined in chat-client-kit.ts:121

**Returns:** *[ChatClientKit](chatclientkit.md)*

## Properties

### `Private` _apiVersion

• **_apiVersion**: *string* = API_VERSION

Defined in chat-client-kit.ts:81

___

### `Private` _initialized

• **_initialized**: *boolean* = false

Defined in chat-client-kit.ts:98

___

### `Private` _isUserAuthenticated

• **_isUserAuthenticated**: *boolean* = false

Defined in chat-client-kit.ts:102

___

### `Private` _subscribed

• **_subscribed**: *boolean* = false

Defined in chat-client-kit.ts:100

___

###  accessToken

• **accessToken**: *string* = ""

Defined in chat-client-kit.ts:73

___

###  apiKey

• **apiKey**: *string* = ""

Defined in chat-client-kit.ts:87

___

###  appId

• **appId**: *string* = ""

Defined in chat-client-kit.ts:85

___

### `Optional` authDetailChangeListener

• **authDetailChangeListener**? : *undefined | function*

Defined in chat-client-kit.ts:91

___

### `Private` `Optional` disableAutoRefreshToken

• **disableAutoRefreshToken**? : *undefined | false | true*

Defined in chat-client-kit.ts:115

___

### `Private` `Optional` disableUserDeletedHandler

• **disableUserDeletedHandler**? : *undefined | false | true*

Defined in chat-client-kit.ts:119

___

### `Private` `Optional` disableUserDisabledHandler

• **disableUserDisabledHandler**? : *undefined | false | true*

Defined in chat-client-kit.ts:117

___

### `Optional` eventListener

• **eventListener**? : *undefined | function*

Defined in chat-client-kit.ts:93

___

### `Private` exchangeTokenPromise

• **exchangeTokenPromise**: *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)›› | null* = null

Defined in chat-client-kit.ts:113

___

###  httpClient

• **httpClient**: *AxiosInstance*

Defined in chat-client-kit.ts:83

___

###  instanceIdPushNotification

• **instanceIdPushNotification**: *string* = ""

Defined in chat-client-kit.ts:79

___

### `Private` isSendingReadReceipt

• **isSendingReadReceipt**: *boolean* = false

Defined in chat-client-kit.ts:109

___

### `Private` loginId

• **loginId**: *number* = 0

Defined in chat-client-kit.ts:121

___

###  publisherKitClient

• **publisherKitClient**: *PublisherKitClient*

Defined in chat-client-kit.ts:71

___

###  publisherToken

• **publisherToken**: *string* = ""

Defined in chat-client-kit.ts:75

___

### `Private` readMessageMap

• **readMessageMap**: *object*

Defined in chat-client-kit.ts:104

#### Type declaration:

* \[ **conversationId**: *string*\]: string

___

###  readMessageSequenceMap

• **readMessageSequenceMap**: *Map‹string, number | string›* = new Map()

Defined in chat-client-kit.ts:107

___

###  refreshToken

• **refreshToken**: *string* = ""

Defined in chat-client-kit.ts:77

___

###  serverUrl

• **serverUrl**: *string* = ""

Defined in chat-client-kit.ts:89

___

### `Optional` sessionExpiredListener

• **sessionExpiredListener**? : *undefined | function*

Defined in chat-client-kit.ts:96

NOTE: Will be called when refresh token exchange session API call has failed

___

### `Private` timerHandle

• **timerHandle**: *any* = null

Defined in chat-client-kit.ts:111

## Methods

###  acceptSmsConversationInvitation

▸ **acceptSmsConversationInvitation**(`params`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2351

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`receiverId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  acceptTermsAndPrivacy

▸ **acceptTermsAndPrivacy**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2276

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  addListeners

▸ **addListeners**(`listeners`: object): *Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)›*

Defined in chat-client-kit.ts:196

**Parameters:**

▪ **listeners**: *object*

Name | Type |
------ | ------ |
`onAuthDetailChange` | function |
`onEvent` | function |
`onSessionExpired?` | undefined &#124; function |
`onSocketConnect?` | undefined &#124; function |
`onSocketDisconnect?` | undefined &#124; function |
`onSocketError?` | undefined &#124; function |
`onSocketReconnect?` | undefined &#124; function |

**Returns:** *Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)›*

___

###  addToContact

▸ **addToContact**(`__namedParameters`: object): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2122

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`externalUserId` | string |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  addUsersToConversation

▸ **addUsersToConversation**(`conversationId`: string, `userIds`: string[], `force?`: undefined | false | true): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1176

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userIds` | string[] |
`force?` | undefined &#124; false &#124; true |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  changePassword

▸ **changePassword**(`currentPassword`: string, `newPassword`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:602

**Parameters:**

Name | Type |
------ | ------ |
`currentPassword` | string |
`newPassword` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  checkEmail

▸ **checkEmail**(`email`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1855

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  checkForExistingConversation

▸ **checkForExistingConversation**(`withUserId`: string): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:853

**Parameters:**

Name | Type |
------ | ------ |
`withUserId` | string |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  checkSmsConversationInvitation

▸ **checkSmsConversationInvitation**(`params`: object): *Promise‹Result‹[SmsConversationInvitationUserDTO](../interfaces/smsconversationinvitationuserdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2332

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹[SmsConversationInvitationUserDTO](../interfaces/smsconversationinvitationuserdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  clientSignInWithToken

▸ **clientSignInWithToken**(`sayheyLoginToken`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2072

**Parameters:**

Name | Type |
------ | ------ |
`sayheyLoginToken` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createGroupConversation

▸ **createGroupConversation**(`params`: object): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:888

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`file?` | [FileUpload](../globals.md#markdown-header-fileupload) |
`groupName` | string |
`userIds` | string[] |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createIndividualConversation

▸ **createIndividualConversation**(`withUserId`: string): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:869

**Parameters:**

Name | Type |
------ | ------ |
`withUserId` | string |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createNewContact

▸ **createNewContact**(`params`: [CreateNewContact](../interfaces/createnewcontact.md)): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2258

**Parameters:**

Name | Type |
------ | ------ |
`params` | [CreateNewContact](../interfaces/createnewcontact.md) |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createQuickConversation

▸ **createQuickConversation**(`params`: object): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:928

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`groupName` | string |
`memberIds` | string[] |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  createSmsConversation

▸ **createSmsConversation**(`params`: object): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2390

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`groupImageId?` | string &#124; null |
`memberId` | string |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  deleteAllConversations

▸ **deleteAllConversations**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1031

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  deleteConversation

▸ **deleteConversation**(`conversationId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1015

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  deleteGroupConversation

▸ **deleteGroupConversation**(`params`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1271

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  disconnectPublisher

▸ **disconnectPublisher**(): *Promise‹void›*

Defined in chat-client-kit.ts:472

**Returns:** *Promise‹void›*

___

###  exchangeToken

▸ **exchangeToken**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:573

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  exchangeTokenOnce

▸ **exchangeTokenOnce**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:275

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  findExternalUserByPhone

▸ **findExternalUserByPhone**(`params`: [FindByPhoneQueryParams](../interfaces/findbyphonequeryparams.md)): *Promise‹Result‹[ExternalUserContactDTO](../interfaces/externalusercontactdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2162

**Parameters:**

Name | Type |
------ | ------ |
`params` | [FindByPhoneQueryParams](../interfaces/findbyphonequeryparams.md) |

**Returns:** *Promise‹Result‹[ExternalUserContactDTO](../interfaces/externalusercontactdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  forgotPassword

▸ **forgotPassword**(`email`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:623

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getAppConfig

▸ **getAppConfig**(): *Promise‹Result‹[AppConfigDTO](../interfaces/appconfigdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1839

**Returns:** *Promise‹Result‹[AppConfigDTO](../interfaces/appconfigdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getConversationBasicUsers

▸ **getConversationBasicUsers**(`params`: object): *Promise‹Result‹[ConversationUsersBasicDTO](../interfaces/conversationusersbasicdto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1156

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`type?` | "all" &#124; "current" &#124; "default" |

**Returns:** *Promise‹Result‹[ConversationUsersBasicDTO](../interfaces/conversationusersbasicdto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getConversationDetails

▸ **getConversationDetails**(`conversationId`: string): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:836

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getConversationReachableUsers

▸ **getConversationReachableUsers**(`__namedParameters`: object): *Promise‹Result‹[ConversableUsersBatchResult](../interfaces/conversableusersbatchresult.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1124

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`includeRefIdInSearch` | undefined &#124; false &#124; true |
`limit` | undefined &#124; number |
`nextCursor` | undefined &#124; number |
`searchString` | undefined &#124; string |

**Returns:** *Promise‹Result‹[ConversableUsersBatchResult](../interfaces/conversableusersbatchresult.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getConversationUsers

▸ **getConversationUsers**(`__namedParameters`: object): *Promise‹Result‹[ConversationUsersBatch](../interfaces/conversationusersbatch.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1091

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`includeRefIdInSearch` | undefined &#124; false &#124; true |
`limit` | undefined &#124; number |
`nextCursor` | undefined &#124; number |
`searchString` | undefined &#124; string |
`type` | undefined &#124; [All](../enums/conversationuserquerytype.md#markdown-header-all) &#124; [Current](../enums/conversationuserquerytype.md#markdown-header-current) &#124; [Removed](../enums/conversationuserquerytype.md#markdown-header-removed) |

**Returns:** *Promise‹Result‹[ConversationUsersBatch](../interfaces/conversationusersbatch.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getConversations

▸ **getConversations**(`params`: object): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:814

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationGroupType?` | [ConversationGroupType](../enums/conversationgrouptype.md) |
`conversationType?` | [ConversationType](../enums/conversationtype.md) |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getExistingIndvidualOrQuickConversation

▸ **getExistingIndvidualOrQuickConversation**(`memberIds`: string[]): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity) | null, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:791

Conversation APIs

**Parameters:**

Name | Type |
------ | ------ |
`memberIds` | string[] |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity) | null, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getExistingSmsConversation

▸ **getExistingSmsConversation**(`params`: [FindSmsConversation](../interfaces/findsmsconversation.md)): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity) | null, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2308

**Parameters:**

Name | Type |
------ | ------ |
`params` | [FindSmsConversation](../interfaces/findsmsconversation.md) |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity) | null, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getGalleryMessages

▸ **getGalleryMessages**(`conversationId`: string, `beforeSequence?`: undefined | number, `limit?`: undefined | number): *Promise‹Result‹[GalleryPayload](../interfaces/gallerypayload.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1658

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`beforeSequence?` | undefined &#124; number |
`limit?` | undefined &#124; number |

**Returns:** *Promise‹Result‹[GalleryPayload](../interfaces/gallerypayload.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getGroupDefaultImageSet

▸ **getGroupDefaultImageSet**(): *Promise‹Result‹[DefaultGroupImageUrlSet](../interfaces/defaultgroupimageurlset.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1324

**Returns:** *Promise‹Result‹[DefaultGroupImageUrlSet](../interfaces/defaultgroupimageurlset.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getInternalUserContactsBatch

▸ **getInternalUserContactsBatch**(`params`: [SearchPaginateParams](../interfaces/searchpaginateparams.md)): *Promise‹Result‹[InternalUserContactsBatchDTO](../interfaces/internalusercontactsbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2144

**Parameters:**

Name | Type |
------ | ------ |
`params` | [SearchPaginateParams](../interfaces/searchpaginateparams.md) |

**Returns:** *Promise‹Result‹[InternalUserContactsBatchDTO](../interfaces/internalusercontactsbatchdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getMediaDirectUrl

▸ **getMediaDirectUrl**(`params`: object): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:386

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`size?` | [ImageSizes](../enums/imagesizes.md) |
`urlPath` | string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getMediaSourceDetails

▸ **getMediaSourceDetails**(`urlPath`: string): *object*

Defined in chat-client-kit.ts:376

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`urlPath` | string | url path returned from SayHey  |

**Returns:** *object*

* **method**: *string* = "GET"

* **uri**: *string* = `${this.serverUrl}${urlPath}`

* ### **headers**: *object*

  * **Accept**: *string* = "image/*, video/*, audio/*"

  * **Authorization**: *string* = `Bearer ${this.accessToken}`

  * **[ApiKeyName]**: *string* = this.apiKey

___

###  getMessages

▸ **getMessages**(`conversationId`: string, `pivotSequence?`: undefined | number, `after?`: undefined | false | true, `limit`: number): *Promise‹Result‹[MessageDomainEntity](../interfaces/messagedomainentity.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1342

Message APIs

**Parameters:**

Name | Type | Default |
------ | ------ | ------ |
`conversationId` | string | - |
`pivotSequence?` | undefined &#124; number | - |
`after?` | undefined &#124; false &#124; true | - |
`limit` | number | 20 |

**Returns:** *Promise‹Result‹[MessageDomainEntity](../interfaces/messagedomainentity.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getOptToReceiveSmsStatus

▸ **getOptToReceiveSmsStatus**(): *Promise‹Result‹[ExternalUserOptToReceiveSmsDTO](../interfaces/externaluseropttoreceivesmsdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2443

**Returns:** *Promise‹Result‹[ExternalUserOptToReceiveSmsDTO](../interfaces/externaluseropttoreceivesmsdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getReachableUsers

▸ **getReachableUsers**(`__namedParameters`: object): *Promise‹Result‹[ConversableUsersBatchResult](../interfaces/conversableusersbatchresult.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:668

User APIs

**Parameters:**

▪`Default value`  **__namedParameters**: *object*= {}

Name | Type |
------ | ------ |
`includeRefIdInSearch` | undefined &#124; false &#124; true |
`limit` | undefined &#124; number |
`nextCursor` | undefined &#124; number |
`searchString` | undefined &#124; string |

**Returns:** *Promise‹Result‹[ConversableUsersBatchResult](../interfaces/conversableusersbatchresult.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getReactedUsers

▸ **getReactedUsers**(`__namedParameters`: object): *Promise‹Result‹[ReactedUserDTO](../interfaces/reacteduserdto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1810

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`afterSequence` | null &#124; number |
`limit` | number |
`messageId` | string |
`type` | undefined &#124; [Thumb](../enums/reactiontype.md#markdown-header-thumb) &#124; [Heart](../enums/reactiontype.md#markdown-header-heart) &#124; [Clap](../enums/reactiontype.md#markdown-header-clap) &#124; [HaHa](../enums/reactiontype.md#markdown-header-haha) &#124; [Exclamation](../enums/reactiontype.md#markdown-header-exclamation) |

**Returns:** *Promise‹Result‹[ReactedUserDTO](../interfaces/reacteduserdto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getTermsAndPrivacy

▸ **getTermsAndPrivacy**(): *Promise‹Result‹[ExternalUserTermsAndPrivacyVersionDTO](../interfaces/externalusertermsandprivacyversiondto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2291

**Returns:** *Promise‹Result‹[ExternalUserTermsAndPrivacyVersionDTO](../interfaces/externalusertermsandprivacyversiondto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUserConversationMessagesReactions

▸ **getUserConversationMessagesReactions**(`conversationId`: string, `messageIds`: string[]): *Promise‹Result‹[ConversationReactionsDomainEntity](../globals.md#markdown-header-conversationreactionsdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1306

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`messageIds` | string[] |

**Returns:** *Promise‹Result‹[ConversationReactionsDomainEntity](../globals.md#markdown-header-conversationreactionsdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUserConversationReactions

▸ **getUserConversationReactions**(`conversationId`: string): *Promise‹Result‹[ConversationReactionsDomainEntity](../globals.md#markdown-header-conversationreactionsdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1292

**`deprecated`** The method should not be used

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹[ConversationReactionsDomainEntity](../globals.md#markdown-header-conversationreactionsdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  getUserDetails

▸ **getUserDetails**(): *Promise‹Result‹[UserSelfDomainEntity](../globals.md#markdown-header-userselfdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:745

**Returns:** *Promise‹Result‹[UserSelfDomainEntity](../globals.md#markdown-header-userselfdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Private` guard

▸ **guard**‹**T**›(`cb`: function): *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:242

**Type parameters:**

▪ **T**

**Parameters:**

▪ **cb**: *function*

Callback to be executed only if ChatKit is initialized and subscribed to

▸ (...`args`: any[]): *Promise‹T | [Error](../modules/sayhey.md#markdown-header-error)›*

**Parameters:**

Name | Type |
------ | ------ |
`...args` | any[] |

**Returns:** *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Private` guardWithAuth

▸ **guardWithAuth**‹**T**›(`cb`: function): *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:291

**Type parameters:**

▪ **T**

**Parameters:**

▪ **cb**: *function*

Callback to be called only when user is authenticated and will try to refresh token if access token expired

▸ (...`args`: any[]): *Promise‹T | [Error](../modules/sayhey.md#markdown-header-error)›*

**Parameters:**

Name | Type |
------ | ------ |
`...args` | any[] |

**Returns:** *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  initialize

▸ **initialize**(`params`: object): *void*

Defined in chat-client-kit.ts:132

Setup methods

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`apiVersion?` | "v1" &#124; "v2" &#124; "v3" &#124; "v4" |
`disableAutoRefreshToken?` | undefined &#124; false &#124; true |
`disableUserDeletedHandler?` | undefined &#124; false &#124; true |
`disableUserDisabledHandler?` | undefined &#124; false &#124; true |
`messageUpdateInterval?` | undefined &#124; number |
`publisherApiKey` | string |
`publisherAppId` | string |
`publisherUrl` | string |
`sayheyApiKey` | string |
`sayheyAppId` | string |
`sayheyUrl` | string |

**Returns:** *void*

___

###  isSocketConnected

▸ **isSocketConnected**(): *boolean*

Defined in chat-client-kit.ts:285

**Returns:** *boolean*

___

###  isUserAuthenticated

▸ **isUserAuthenticated**(): *boolean*

Defined in chat-client-kit.ts:127

**Returns:** *boolean*

___

###  makeConversationOwner

▸ **makeConversationOwner**(`conversationId`: string, `userId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1222

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Private` markMessageRead

▸ **markMessageRead**(`messageId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1754

**Parameters:**

Name | Type |
------ | ------ |
`messageId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  muteAllNotifications

▸ **muteAllNotifications**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:759

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  muteConversation

▸ **muteConversation**(`conversationId`: string): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:951

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  nudgeExternalUserToConversation

▸ **nudgeExternalUserToConversation**(`params`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2460

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Private` onMessage

▸ **onMessage**(`data`: [Event](../modules/publisher.event.md)): *void*

Defined in chat-client-kit.ts:1400

**Parameters:**

Name | Type |
------ | ------ |
`data` | [Event](../modules/publisher.event.md) |

**Returns:** *void*

___

###  optNotToReceiveSms

▸ **optNotToReceiveSms**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2428

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  optToReceiveSms

▸ **optToReceiveSms**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2413

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  pinConversation

▸ **pinConversation**(`conversationId`: string): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:983

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  readMessage

▸ **readMessage**(`params`: object): *void*

Defined in chat-client-kit.ts:1720

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`immediateUpdate?` | undefined &#124; false &#124; true |
`messageId` | string |
`sequence` | number |

**Returns:** *void*

___

###  reconnectPublisher

▸ **reconnectPublisher**(): *Promise‹void›*

Defined in chat-client-kit.ts:476

**Returns:** *Promise‹void›*

___

###  registerForPushNotification

▸ **registerForPushNotification**(`instanceId`: string, `token`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1767

Push Notification APIs

**Parameters:**

Name | Type |
------ | ------ |
`instanceId` | string |
`token` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  removeConversationOwner

▸ **removeConversationOwner**(`conversationId`: string, `userId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1238

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  removeGroupImage

▸ **removeGroupImage**(`conversationId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1254

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  removeMessageReaction

▸ **removeMessageReaction**(`messageId`: string, `type`: [ReactionType](../enums/reactiontype.md)): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1702

**Parameters:**

Name | Type |
------ | ------ |
`messageId` | string |
`type` | [ReactionType](../enums/reactiontype.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  removeUsersFromConversation

▸ **removeUsersFromConversation**(`conversationId`: string, `userIds`: string[], `force?`: undefined | false | true): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1200

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userIds` | string[] |
`force?` | undefined &#124; false &#124; true |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  reportFlaggedMessage

▸ **reportFlaggedMessage**(`__namedParameters`: object): *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2095

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`messageId` | string |
`reason` | string |

**Returns:** *Promise‹Result‹object, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  requestPreverificationOTP

▸ **requestPreverificationOTP**(`email`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1955

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  requestResetPasswordOTP

▸ **requestResetPasswordOTP**(`email`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1936

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  requestSetPasswordOTP

▸ **requestSetPasswordOTP**(`email`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1919

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  requestSignInExternalOTP

▸ **requestSignInExternalOTP**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2180

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`phone` | [PhoneWithCode](../interfaces/phonewithcode.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  requestSignUpOTP

▸ **requestSignUpOTP**(`email`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1902

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  resendSmsConversationInvitation

▸ **resendSmsConversationInvitation**(`params`: object): *Promise‹Result‹[SmsConversationInvitationDTO](../interfaces/smsconversationinvitationdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2371

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹[SmsConversationInvitationDTO](../interfaces/smsconversationinvitationdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  resetPassword

▸ **resetPassword**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:640

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`password` | string |
`resetPasswordToken` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  sendMessage

▸ **sendMessage**(`params`: [SendMessageParams](../globals.md#markdown-header-sendmessageparams)): *Promise‹Result‹[MessageDomainEntity](../interfaces/messagedomainentity.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1551

**Parameters:**

Name | Type |
------ | ------ |
`params` | [SendMessageParams](../globals.md#markdown-header-sendmessageparams) |

**Returns:** *Promise‹Result‹[MessageDomainEntity](../interfaces/messagedomainentity.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  setMessageReaction

▸ **setMessageReaction**(`messageId`: string, `type`: [ReactionType](../enums/reactiontype.md)): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1684

**Parameters:**

Name | Type |
------ | ------ |
`messageId` | string |
`type` | [ReactionType](../enums/reactiontype.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  setPassword

▸ **setPassword**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1873

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`password` | string |
`setPasswordToken` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  signIn

▸ **signIn**(`email`: string, `password`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:490

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`email` | string | user email for SayHey |
`password` | string | user password for SayHey   |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  signInExternal

▸ **signInExternal**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2202

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`otp` | string |
`phone` | [PhoneWithCode](../interfaces/phonewithcode.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  signInExternalByUserId

▸ **signInExternalByUserId**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2231

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`id` | string |
`otp` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  signInWithToken

▸ **signInWithToken**(`accessToken`: string, `publisherToken`: string, `refreshToken`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:514

**Parameters:**

Name | Type |
------ | ------ |
`accessToken` | string |
`publisherToken` | string |
`refreshToken` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  signOut

▸ **signOut**(): *Promise‹void›*

Defined in chat-client-kit.ts:555

**Returns:** *Promise‹void›*

___

###  signUp

▸ **signUp**(`email`: string, `password`: string, `firstName`: string, `lastName`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:527

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |
`password` | string |
`firstName` | string |
`lastName` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  unmuteAllNotifications

▸ **unmuteAllNotifications**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:773

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  unmuteConversation

▸ **unmuteConversation**(`conversationId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:967

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  unpinConversation

▸ **unpinConversation**(`conversationId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:999

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  unregisterForPushNotification

▸ **unregisterForPushNotification**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1792

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Private` updateAuthDetails

▸ **updateAuthDetails**(`accessToken`: string, `publisherToken`: string, `refreshToken`: string): *Promise‹void›*

Defined in chat-client-kit.ts:325

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`accessToken` | string | access token returned from SayHey that will be used to verify user with SayHey |
`publisherToken` | string | publisher token returned from SayHey that will be used to verify Publisher access |
`refreshToken` | string | token returned from SayHey that will be used to refresh access token when it expired  |

**Returns:** *Promise‹void›*

___

###  updateGroupImage

▸ **updateGroupImage**(`conversationId`: string, `file`: [FileUpload](../globals.md#markdown-header-fileupload)): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1045

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`file` | [FileUpload](../globals.md#markdown-header-fileupload) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateGroupInfo

▸ **updateGroupInfo**(`conversationId`: string, `groupName`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1070

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`groupName` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateUserProfileImage

▸ **updateUserProfileImage**(`fileUri`: string, `fileName`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:702

**Parameters:**

Name | Type |
------ | ------ |
`fileUri` | string |
`fileName` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  updateUserProfileInfo

▸ **updateUserProfileInfo**(`options`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:726

**Parameters:**

▪ **options**: *object*

Name | Type |
------ | ------ |
`email?` | undefined &#124; string |
`firstName?` | undefined &#124; string |
`lastName?` | undefined &#124; string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Private` uploadFile

▸ **uploadFile**(`fileUri`: string | Blob, `fileName`: string, `progress?`: undefined | function, `thumbnailId?`: undefined | string): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:407

**Parameters:**

Name | Type |
------ | ------ |
`fileUri` | string &#124; Blob |
`fileName` | string |
`progress?` | undefined &#124; function |
`thumbnailId?` | undefined &#124; string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  uploadImage

▸ **uploadImage**(`fileUri`: string | Blob, `fileName`: string): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:455

**Parameters:**

Name | Type |
------ | ------ |
`fileUri` | string &#124; Blob |
`fileName` | string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  validatePreverificationOTP

▸ **validatePreverificationOTP**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2048

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`email` | string |
`otp` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  validateResetPasswordOTP

▸ **validateResetPasswordOTP**(`__namedParameters`: object): *Promise‹Result‹[ValidateResetPasswordOTPDTO](../interfaces/validateresetpasswordotpdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2024

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`email` | string |
`otp` | string |

**Returns:** *Promise‹Result‹[ValidateResetPasswordOTPDTO](../interfaces/validateresetpasswordotpdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  validateSetPasswordOTP

▸ **validateSetPasswordOTP**(`__namedParameters`: object): *Promise‹Result‹[ValidateSetPasswordOTPDTO](../interfaces/validatesetpasswordotpdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:2000

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`email` | string |
`otp` | string |

**Returns:** *Promise‹Result‹[ValidateSetPasswordOTPDTO](../interfaces/validatesetpasswordotpdto.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

###  validateSignUpOTP

▸ **validateSignUpOTP**(`__namedParameters`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in chat-client-kit.ts:1976

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`email` | string |
`otp` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*
