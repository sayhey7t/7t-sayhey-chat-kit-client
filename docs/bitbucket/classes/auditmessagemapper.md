[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [AuditMessageMapper](auditmessagemapper.md)

# Class: AuditMessageMapper

## Hierarchy

* **AuditMessageMapper**

## Index

### Methods

* [toAuditMessageDomainEntityBatchDTO](auditmessagemapper.md#markdown-header-static-toauditmessagedomainentitybatchdto)
* [toAuditMessageDomainEntityDTO](auditmessagemapper.md#markdown-header-static-toauditmessagedomainentitydto)

## Methods

### `Static` toAuditMessageDomainEntityBatchDTO

▸ **toAuditMessageDomainEntityBatchDTO**(`auditMessageBatchDTO`: [AuditMessageBatchDTO](../interfaces/auditmessagebatchdto.md)): *[AuditMessageDomainEntityBatchDTO](../interfaces/auditmessagedomainentitybatchdto.md)*

Defined in mapper.ts:397

**Parameters:**

Name | Type |
------ | ------ |
`auditMessageBatchDTO` | [AuditMessageBatchDTO](../interfaces/auditmessagebatchdto.md) |

**Returns:** *[AuditMessageDomainEntityBatchDTO](../interfaces/auditmessagedomainentitybatchdto.md)*

___

### `Static` toAuditMessageDomainEntityDTO

▸ **toAuditMessageDomainEntityDTO**(`auditMessageDTO`: [AuditMessageDTO](../interfaces/auditmessagedto.md)): *[AuditMessageDomainEntityDTO](../interfaces/auditmessagedomainentitydto.md)*

Defined in mapper.ts:414

**Parameters:**

Name | Type |
------ | ------ |
`auditMessageDTO` | [AuditMessageDTO](../interfaces/auditmessagedto.md) |

**Returns:** *[AuditMessageDomainEntityDTO](../interfaces/auditmessagedomainentitydto.md)*
