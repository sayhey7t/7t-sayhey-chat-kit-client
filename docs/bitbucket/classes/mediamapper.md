[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [MediaMapper](mediamapper.md)

# Class: MediaMapper

## Hierarchy

* **MediaMapper**

## Index

### Methods

* [toDomainEntity](mediamapper.md#markdown-header-static-todomainentity)

## Methods

### `Static` toDomainEntity

▸ **toDomainEntity**(`media`: [MediaDTO](../interfaces/mediadto.md)): *[MediaDomainEntity](../interfaces/mediadomainentity.md)*

Defined in mapper.ts:321

**Parameters:**

Name | Type |
------ | ------ |
`media` | [MediaDTO](../interfaces/mediadto.md) |

**Returns:** *[MediaDomainEntity](../interfaces/mediadomainentity.md)*
