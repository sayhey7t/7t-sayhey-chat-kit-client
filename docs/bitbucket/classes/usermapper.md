[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserMapper](usermapper.md)

# Class: UserMapper

## Hierarchy

* **UserMapper**

## Index

### Methods

* [toConversableUserDomainEntities](usermapper.md#markdown-header-static-toconversableuserdomainentities)
* [toConversableUserDomainEntity](usermapper.md#markdown-header-static-toconversableuserdomainentity)
* [toConversableUserDomainForUserAdminEntities](usermapper.md#markdown-header-static-toconversableuserdomainforuseradminentities)
* [toConversableUserDomainForUserAdminEntity](usermapper.md#markdown-header-static-toconversableuserdomainforuseradminentity)
* [toDomainEntities](usermapper.md#markdown-header-static-todomainentities)
* [toDomainEntity](usermapper.md#markdown-header-static-todomainentity)
* [toSelfDomainEntity](usermapper.md#markdown-header-static-toselfdomainentity)
* [toUserForUserAdminDomainEntity](usermapper.md#markdown-header-static-touserforuseradmindomainentity)
* [toUserForUserDomainEntities](usermapper.md#markdown-header-static-touserforuserdomainentities)

## Methods

### `Static` toConversableUserDomainEntities

▸ **toConversableUserDomainEntities**(`users`: [UserOfConversationDTO](../globals.md#markdown-header-userofconversationdto)[]): *[ConversationUserDomainEntity](../globals.md#markdown-header-conversationuserdomainentity)[]*

Defined in mapper.ts:284

**Parameters:**

Name | Type |
------ | ------ |
`users` | [UserOfConversationDTO](../globals.md#markdown-header-userofconversationdto)[] |

**Returns:** *[ConversationUserDomainEntity](../globals.md#markdown-header-conversationuserdomainentity)[]*

___

### `Static` toConversableUserDomainEntity

▸ **toConversableUserDomainEntity**(`user`: [UserOfConversationDTO](../globals.md#markdown-header-userofconversationdto)): *[ConversationUserDomainEntity](../globals.md#markdown-header-conversationuserdomainentity)*

Defined in mapper.ts:231

**Parameters:**

Name | Type |
------ | ------ |
`user` | [UserOfConversationDTO](../globals.md#markdown-header-userofconversationdto) |

**Returns:** *[ConversationUserDomainEntity](../globals.md#markdown-header-conversationuserdomainentity)*

___

### `Static` toConversableUserDomainForUserAdminEntities

▸ **toConversableUserDomainForUserAdminEntities**(`users`: [UserOfConversationForUserAdminDTO](../globals.md#markdown-header-userofconversationforuseradmindto)[]): *[ConversationUserDomainForUserAdminEntity](../globals.md#markdown-header-conversationuserdomainforuseradminentity)[]*

Defined in mapper.ts:290

**Parameters:**

Name | Type |
------ | ------ |
`users` | [UserOfConversationForUserAdminDTO](../globals.md#markdown-header-userofconversationforuseradmindto)[] |

**Returns:** *[ConversationUserDomainForUserAdminEntity](../globals.md#markdown-header-conversationuserdomainforuseradminentity)[]*

___

### `Static` toConversableUserDomainForUserAdminEntity

▸ **toConversableUserDomainForUserAdminEntity**(`user`: [UserOfConversationForUserAdminDTO](../globals.md#markdown-header-userofconversationforuseradmindto)): *[ConversationUserDomainForUserAdminEntity](../globals.md#markdown-header-conversationuserdomainforuseradminentity)*

Defined in mapper.ts:253

**Parameters:**

Name | Type |
------ | ------ |
`user` | [UserOfConversationForUserAdminDTO](../globals.md#markdown-header-userofconversationforuseradmindto) |

**Returns:** *[ConversationUserDomainForUserAdminEntity](../globals.md#markdown-header-conversationuserdomainforuseradminentity)*

___

### `Static` toDomainEntities

▸ **toDomainEntities**(`users`: [UserDTO](../globals.md#markdown-header-userdto)[]): *[UserDomainEntity](../globals.md#markdown-header-userdomainentity)[]*

Defined in mapper.ts:276

**Parameters:**

Name | Type |
------ | ------ |
`users` | [UserDTO](../globals.md#markdown-header-userdto)[] |

**Returns:** *[UserDomainEntity](../globals.md#markdown-header-userdomainentity)[]*

___

### `Static` toDomainEntity

▸ **toDomainEntity**(`user`: [UserDTO](../globals.md#markdown-header-userdto)): *[UserDomainEntity](../globals.md#markdown-header-userdomainentity)*

Defined in mapper.ts:178

**Parameters:**

Name | Type |
------ | ------ |
`user` | [UserDTO](../globals.md#markdown-header-userdto) |

**Returns:** *[UserDomainEntity](../globals.md#markdown-header-userdomainentity)*

___

### `Static` toSelfDomainEntity

▸ **toSelfDomainEntity**(`user`: [UserSelfDTO](../globals.md#markdown-header-userselfdto)): *[UserSelfDomainEntity](../globals.md#markdown-header-userselfdomainentity)*

Defined in mapper.ts:296

**Parameters:**

Name | Type |
------ | ------ |
`user` | [UserSelfDTO](../globals.md#markdown-header-userselfdto) |

**Returns:** *[UserSelfDomainEntity](../globals.md#markdown-header-userselfdomainentity)*

___

### `Static` toUserForUserAdminDomainEntity

▸ **toUserForUserAdminDomainEntity**(`user`: [UserForUserAdminDTO](../globals.md#markdown-header-userforuseradmindto)): *[UserForUserAdminDomainEntity](../globals.md#markdown-header-userforuseradmindomainentity)*

Defined in mapper.ts:201

**Parameters:**

Name | Type |
------ | ------ |
`user` | [UserForUserAdminDTO](../globals.md#markdown-header-userforuseradmindto) |

**Returns:** *[UserForUserAdminDomainEntity](../globals.md#markdown-header-userforuseradmindomainentity)*

___

### `Static` toUserForUserDomainEntities

▸ **toUserForUserDomainEntities**(`users`: [UserForUserAdminDTO](../globals.md#markdown-header-userforuseradmindto)[]): *[UserForUserAdminDomainEntity](../globals.md#markdown-header-userforuseradmindomainentity)[]*

Defined in mapper.ts:280

**Parameters:**

Name | Type |
------ | ------ |
`users` | [UserForUserAdminDTO](../globals.md#markdown-header-userforuseradmindto)[] |

**Returns:** *[UserForUserAdminDomainEntity](../globals.md#markdown-header-userforuseradmindomainentity)[]*
