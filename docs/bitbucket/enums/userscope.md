[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserScope](userscope.md)

# Enumeration: UserScope

User Types

## Index

### Enumeration members

* [External](userscope.md#markdown-header-external)
* [Internal](userscope.md#markdown-header-internal)

## Enumeration members

###  External

• **External**: = "external"

Defined in types.ts:377

___

###  Internal

• **Internal**: = "internal"

Defined in types.ts:376
