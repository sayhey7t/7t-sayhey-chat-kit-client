[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [PushNotificationError](sayhey.chatkiterror.pushnotificationerror.md)

# Enumeration: PushNotificationError

## Index

### Enumeration members

* [AlreadyRegisteredForUser](sayhey.chatkiterror.pushnotificationerror.md#markdown-header-alreadyregisteredforuser)
* [InvalidPushParams](sayhey.chatkiterror.pushnotificationerror.md#markdown-header-invalidpushparams)
* [NoTokensToPush](sayhey.chatkiterror.pushnotificationerror.md#markdown-header-notokenstopush)
* [NotFound](sayhey.chatkiterror.pushnotificationerror.md#markdown-header-notfound)
* [OnlyStringData](sayhey.chatkiterror.pushnotificationerror.md#markdown-header-onlystringdata)

## Enumeration members

###  AlreadyRegisteredForUser

• **AlreadyRegisteredForUser**: = "DeviceTokenErrorTypeAlreadyRegisteredForUser"

Defined in types.ts:1623

___

###  InvalidPushParams

• **InvalidPushParams**: = "DeviceTokenErrorTypeInvalidPushParams"

Defined in types.ts:1622

___

###  NoTokensToPush

• **NoTokensToPush**: = "DeviceTokenErrorTypeNoTokensToPush"

Defined in types.ts:1625

___

###  NotFound

• **NotFound**: = "DeviceTokenErrorTypeNotFound"

Defined in types.ts:1624

___

###  OnlyStringData

• **OnlyStringData**: = "DeviceTokenErrorTypeOnlyStringData"

Defined in types.ts:1626
