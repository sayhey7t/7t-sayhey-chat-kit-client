[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [GeneralErrorType](sayhey.chatkiterror.errortype.generalerrortype.md)

# Enumeration: GeneralErrorType

## Index

### Enumeration members

* [LimitNotValid](sayhey.chatkiterror.errortype.generalerrortype.md#markdown-header-limitnotvalid)
* [MissingInfoToUpdate](sayhey.chatkiterror.errortype.generalerrortype.md#markdown-header-missinginfotoupdate)
* [MissingQueryParams](sayhey.chatkiterror.errortype.generalerrortype.md#markdown-header-missingqueryparams)
* [OffsetNotValid](sayhey.chatkiterror.errortype.generalerrortype.md#markdown-header-offsetnotvalid)
* [RouteNotFound](sayhey.chatkiterror.errortype.generalerrortype.md#markdown-header-routenotfound)

## Enumeration members

###  LimitNotValid

• **LimitNotValid**: = "GeneralErrorTypeLimitNotValid"

Defined in types.ts:1461

___

###  MissingInfoToUpdate

• **MissingInfoToUpdate**: = "GeneralErrorTypeMissingInfoToUpdate"

Defined in types.ts:1463

___

###  MissingQueryParams

• **MissingQueryParams**: = "GeneralErrorTypeMissingQueryParams"

Defined in types.ts:1459

___

###  OffsetNotValid

• **OffsetNotValid**: = "GeneralErrorTypeOffsetNotValid"

Defined in types.ts:1462

___

###  RouteNotFound

• **RouteNotFound**: = "GeneralErrorTypeRouteNotFound"

Defined in types.ts:1460
