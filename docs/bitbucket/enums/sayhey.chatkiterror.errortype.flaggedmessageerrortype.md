[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [FlaggedMessageErrorType](sayhey.chatkiterror.errortype.flaggedmessageerrortype.md)

# Enumeration: FlaggedMessageErrorType

## Index

### Enumeration members

* [AlreadyReported](sayhey.chatkiterror.errortype.flaggedmessageerrortype.md#markdown-header-alreadyreported)
* [AlreadyResolved](sayhey.chatkiterror.errortype.flaggedmessageerrortype.md#markdown-header-alreadyresolved)
* [NotFound](sayhey.chatkiterror.errortype.flaggedmessageerrortype.md#markdown-header-notfound)

## Enumeration members

###  AlreadyReported

• **AlreadyReported**: = "FlaggedMessageErrorTypeAlreadyReported"

Defined in types.ts:1590

___

###  AlreadyResolved

• **AlreadyResolved**: = "FlaggedMessageErrorTypeAlreadyResolved"

Defined in types.ts:1592

___

###  NotFound

• **NotFound**: = "FlaggedMessageErrorTypeNotFound"

Defined in types.ts:1591
