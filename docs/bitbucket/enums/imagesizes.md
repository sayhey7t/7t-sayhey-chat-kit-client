[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ImageSizes](imagesizes.md)

# Enumeration: ImageSizes

Message Types

## Index

### Enumeration members

* [Large](imagesizes.md#markdown-header-large)
* [Medium](imagesizes.md#markdown-header-medium)
* [Small](imagesizes.md#markdown-header-small)
* [Tiny](imagesizes.md#markdown-header-tiny)
* [XLarge](imagesizes.md#markdown-header-xlarge)

## Enumeration members

###  Large

• **Large**: = "large"

Defined in types.ts:62

___

###  Medium

• **Medium**: = "medium"

Defined in types.ts:61

___

###  Small

• **Small**: = "small"

Defined in types.ts:60

___

###  Tiny

• **Tiny**: = "tiny"

Defined in types.ts:59

___

###  XLarge

• **XLarge**: = "xlarge"

Defined in types.ts:63
