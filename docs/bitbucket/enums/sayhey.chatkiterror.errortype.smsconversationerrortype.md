[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [SmsConversationErrorType](sayhey.chatkiterror.errortype.smsconversationerrortype.md)

# Enumeration: SmsConversationErrorType

## Index

### Enumeration members

* [ExternalUserNotFound](sayhey.chatkiterror.errortype.smsconversationerrortype.md#markdown-header-externalusernotfound)
* [NotAnExternalUser](sayhey.chatkiterror.errortype.smsconversationerrortype.md#markdown-header-notanexternaluser)

## Enumeration members

###  ExternalUserNotFound

• **ExternalUserNotFound**: = "SmsConversationErrorTypeNotFoundExternalUserNotFound"

Defined in types.ts:1610

___

###  NotAnExternalUser

• **NotAnExternalUser**: = "SmsConversationErrorTypeNotAnExternalUser"

Defined in types.ts:1609
