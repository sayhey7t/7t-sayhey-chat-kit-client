[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [HighLightType](highlighttype.md)

# Enumeration: HighLightType

## Index

### Enumeration members

* [Email](highlighttype.md#markdown-header-email)
* [Phone](highlighttype.md#markdown-header-phone)
* [Text](highlighttype.md#markdown-header-text)
* [Url](highlighttype.md#markdown-header-url)
* [UserId](highlighttype.md#markdown-header-userid)

## Enumeration members

###  Email

• **Email**: = "email"

Defined in types.ts:124

___

###  Phone

• **Phone**: = "phone"

Defined in types.ts:125

___

###  Text

• **Text**: = "text"

Defined in types.ts:123

___

###  Url

• **Url**: = "url"

Defined in types.ts:126

___

###  UserId

• **UserId**: = "userId"

Defined in types.ts:127
