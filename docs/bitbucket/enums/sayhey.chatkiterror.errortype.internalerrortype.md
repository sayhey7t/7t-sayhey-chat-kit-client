[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [InternalErrorType](sayhey.chatkiterror.errortype.internalerrortype.md)

# Enumeration: InternalErrorType

## Index

### Enumeration members

* [AuthMissing](sayhey.chatkiterror.errortype.internalerrortype.md#markdown-header-authmissing)
* [BadData](sayhey.chatkiterror.errortype.internalerrortype.md#markdown-header-baddata)
* [MissingParameter](sayhey.chatkiterror.errortype.internalerrortype.md#markdown-header-missingparameter)
* [Uninitialized](sayhey.chatkiterror.errortype.internalerrortype.md#markdown-header-uninitialized)
* [Unknown](sayhey.chatkiterror.errortype.internalerrortype.md#markdown-header-unknown)

## Enumeration members

###  AuthMissing

• **AuthMissing**: = "InternalAuthMissing"

Defined in types.ts:1378

___

###  BadData

• **BadData**: = "InternalBadData"

Defined in types.ts:1379

___

###  MissingParameter

• **MissingParameter**: = "InternalMissingParameter"

Defined in types.ts:1380

___

###  Uninitialized

• **Uninitialized**: = "InternalUninitialized"

Defined in types.ts:1377

___

###  Unknown

• **Unknown**: = "InternalUnknown"

Defined in types.ts:1376
