[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserAdminSortFields](useradminsortfields.md)

# Enumeration: UserAdminSortFields

## Index

### Enumeration members

* [UserAdminEmail](useradminsortfields.md#markdown-header-useradminemail)
* [UserAdminName](useradminsortfields.md#markdown-header-useradminname)
* [UserAdminRole](useradminsortfields.md#markdown-header-useradminrole)

## Enumeration members

###  UserAdminEmail

• **UserAdminEmail**: = "userAdminEmail"

Defined in types.ts:500

___

###  UserAdminName

• **UserAdminName**: = "userAdminName"

Defined in types.ts:499

___

###  UserAdminRole

• **UserAdminRole**: = "userAdminRole"

Defined in types.ts:501
