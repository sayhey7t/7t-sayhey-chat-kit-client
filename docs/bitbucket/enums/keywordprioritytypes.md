[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [KeyWordPriorityTypes](keywordprioritytypes.md)

# Enumeration: KeyWordPriorityTypes

## Index

### Enumeration members

* [High](keywordprioritytypes.md#markdown-header-high)
* [Low](keywordprioritytypes.md#markdown-header-low)

## Enumeration members

###  High

• **High**: = "high"

Defined in types.ts:2000

___

###  Low

• **Low**: = "low"

Defined in types.ts:2001
