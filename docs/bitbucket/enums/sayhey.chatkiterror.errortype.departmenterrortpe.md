[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [DepartmentErrorTpe](sayhey.chatkiterror.errortype.departmenterrortpe.md)

# Enumeration: DepartmentErrorTpe

## Index

### Enumeration members

* [AlreadyExists](sayhey.chatkiterror.errortype.departmenterrortpe.md#markdown-header-alreadyexists)
* [MissingDefaultDeptOrDiv](sayhey.chatkiterror.errortype.departmenterrortpe.md#markdown-header-missingdefaultdeptordiv)
* [MissingInfoToUpdate](sayhey.chatkiterror.errortype.departmenterrortpe.md#markdown-header-missinginfotoupdate)
* [NotFound](sayhey.chatkiterror.errortype.departmenterrortpe.md#markdown-header-notfound)
* [UpdateDefaultNotAllowed](sayhey.chatkiterror.errortype.departmenterrortpe.md#markdown-header-updatedefaultnotallowed)

## Enumeration members

###  AlreadyExists

• **AlreadyExists**: = "DepartmentErrorTypeAlreadyExists"

Defined in types.ts:1546

___

###  MissingDefaultDeptOrDiv

• **MissingDefaultDeptOrDiv**: = "DepartmentErrorTypeMissingDefaultDeptOrDiv"

Defined in types.ts:1548

___

###  MissingInfoToUpdate

• **MissingInfoToUpdate**: = "DepartmentErrorTypeMissingInfoToUpdate"

Defined in types.ts:1547

___

###  NotFound

• **NotFound**: = "DepartmentErrorTypeNotFound"

Defined in types.ts:1545

___

###  UpdateDefaultNotAllowed

• **UpdateDefaultNotAllowed**: = "DepartmentErrorTypeUpdateDefaultNotAllowed"

Defined in types.ts:1549
