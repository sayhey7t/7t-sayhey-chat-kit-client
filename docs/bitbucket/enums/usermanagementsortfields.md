[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserManagementSortFields](usermanagementsortfields.md)

# Enumeration: UserManagementSortFields

## Index

### Enumeration members

* [UserDepartment](usermanagementsortfields.md#markdown-header-userdepartment)
* [UserEmail](usermanagementsortfields.md#markdown-header-useremail)
* [UserEmpCode](usermanagementsortfields.md#markdown-header-userempcode)
* [UserHrAdmin](usermanagementsortfields.md#markdown-header-userhradmin)
* [UserName](usermanagementsortfields.md#markdown-header-username)
* [UserTitle](usermanagementsortfields.md#markdown-header-usertitle)

## Enumeration members

###  UserDepartment

• **UserDepartment**: = "userDepartment"

Defined in types.ts:536

___

###  UserEmail

• **UserEmail**: = "userEmail"

Defined in types.ts:534

___

###  UserEmpCode

• **UserEmpCode**: = "userEmployeeCode"

Defined in types.ts:535

___

###  UserHrAdmin

• **UserHrAdmin**: = "userHrAdmin"

Defined in types.ts:538

___

###  UserName

• **UserName**: = "userName"

Defined in types.ts:533

___

###  UserTitle

• **UserTitle**: = "userTitle"

Defined in types.ts:537
