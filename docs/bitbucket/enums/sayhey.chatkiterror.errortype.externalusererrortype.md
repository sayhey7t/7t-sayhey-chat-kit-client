[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [ExternalUserErrorType](sayhey.chatkiterror.errortype.externalusererrortype.md)

# Enumeration: ExternalUserErrorType

## Index

### Enumeration members

* [ContactAlreadyExists](sayhey.chatkiterror.errortype.externalusererrortype.md#markdown-header-contactalreadyexists)
* [InvalidPhoneNumber](sayhey.chatkiterror.errortype.externalusererrortype.md#markdown-header-invalidphonenumber)
* [NotFound](sayhey.chatkiterror.errortype.externalusererrortype.md#markdown-header-notfound)
* [UserDisabled](sayhey.chatkiterror.errortype.externalusererrortype.md#markdown-header-userdisabled)

## Enumeration members

###  ContactAlreadyExists

• **ContactAlreadyExists**: = "ExternalUserErrorTypeContactAlreadyExists"

Defined in types.ts:1599

___

###  InvalidPhoneNumber

• **InvalidPhoneNumber**: = "ExternalUserErrorTypeInvalidPhoneNumber"

Defined in types.ts:1602

___

###  NotFound

• **NotFound**: = "ExternalUserErrorTypeNotFound"

Defined in types.ts:1600

___

###  UserDisabled

• **UserDisabled**: = "ExternalUserErrorTypeUserDisabled"

Defined in types.ts:1601
