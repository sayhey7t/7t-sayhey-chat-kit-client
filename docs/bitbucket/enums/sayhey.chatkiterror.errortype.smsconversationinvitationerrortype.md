[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [SmsConversationInvitationErrorType](sayhey.chatkiterror.errortype.smsconversationinvitationerrortype.md)

# Enumeration: SmsConversationInvitationErrorType

## Index

### Enumeration members

* [InvitationAlreadyAccepted](sayhey.chatkiterror.errortype.smsconversationinvitationerrortype.md#markdown-header-invitationalreadyaccepted)
* [InvitationExpired](sayhey.chatkiterror.errortype.smsconversationinvitationerrortype.md#markdown-header-invitationexpired)
* [InvitationNotAccepted](sayhey.chatkiterror.errortype.smsconversationinvitationerrortype.md#markdown-header-invitationnotaccepted)
* [NotFound](sayhey.chatkiterror.errortype.smsconversationinvitationerrortype.md#markdown-header-notfound)

## Enumeration members

###  InvitationAlreadyAccepted

• **InvitationAlreadyAccepted**: = "SmsConversationInvitationErrorTypeInvitationAlreadyAccepted"

Defined in types.ts:1616

___

###  InvitationExpired

• **InvitationExpired**: = "SmsConversationInvitationErrorTypeInvitationExpired"

Defined in types.ts:1617

___

###  InvitationNotAccepted

• **InvitationNotAccepted**: = "SmsConversationInvitationErrorTypeInvitationNotAccepted"

Defined in types.ts:1615

___

###  NotFound

• **NotFound**: = "SmsConversationInvitationErrorTypeNotFound"

Defined in types.ts:1614
