[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserOnboardingJobStatus](useronboardingjobstatus.md)

# Enumeration: UserOnboardingJobStatus

## Index

### Enumeration members

* [Completed](useronboardingjobstatus.md#markdown-header-completed)
* [InProgress](useronboardingjobstatus.md#markdown-header-inprogress)
* [Pending](useronboardingjobstatus.md#markdown-header-pending)

## Enumeration members

###  Completed

• **Completed**: = "completed"

Defined in types.ts:2359

___

###  InProgress

• **InProgress**: = "in_progress"

Defined in types.ts:2361

___

###  Pending

• **Pending**: = "pending"

Defined in types.ts:2360
