[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [OTPErrorType](sayhey.chatkiterror.errortype.otperrortype.md)

# Enumeration: OTPErrorType

## Index

### Enumeration members

* [MissingEmailService](sayhey.chatkiterror.errortype.otperrortype.md#markdown-header-missingemailservice)

## Enumeration members

###  MissingEmailService

• **MissingEmailService**: = "OTPErrorTypeMissingEmailService"

Defined in types.ts:1532
