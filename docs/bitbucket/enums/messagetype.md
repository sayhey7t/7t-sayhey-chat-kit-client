[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [MessageType](messagetype.md)

# Enumeration: MessageType

## Index

### Enumeration members

* [Audio](messagetype.md#markdown-header-audio)
* [Document](messagetype.md#markdown-header-document)
* [Emoji](messagetype.md#markdown-header-emoji)
* [Gif](messagetype.md#markdown-header-gif)
* [Image](messagetype.md#markdown-header-image)
* [System](messagetype.md#markdown-header-system)
* [Text](messagetype.md#markdown-header-text)
* [Video](messagetype.md#markdown-header-video)

## Enumeration members

###  Audio

• **Audio**: = "audio"

Defined in types.ts:119

___

###  Document

• **Document**: = "document"

Defined in types.ts:116

___

###  Emoji

• **Emoji**: = "emoji"

Defined in types.ts:118

___

###  Gif

• **Gif**: = "gif"

Defined in types.ts:117

___

###  Image

• **Image**: = "image"

Defined in types.ts:114

___

###  System

• **System**: = "system"

Defined in types.ts:112

___

###  Text

• **Text**: = "text"

Defined in types.ts:113

___

###  Video

• **Video**: = "video"

Defined in types.ts:115
