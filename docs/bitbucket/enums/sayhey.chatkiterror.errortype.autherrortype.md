[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [AuthErrorType](sayhey.chatkiterror.errortype.autherrortype.md)

# Enumeration: AuthErrorType

## Index

### Enumeration members

* [AccountAlreadyExists](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-accountalreadyexists)
* [AccountAlreadyPreVerified](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-accountalreadypreverified)
* [AccountNotPreVerified](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-accountnotpreverified)
* [AdminAlreadyExists](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-adminalreadyexists)
* [AdminNotAssociatedWithApp](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-adminnotassociatedwithapp)
* [AppHasNoNotificationSetup](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-apphasnonotificationsetup)
* [EmailDoesNotExist](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-emaildoesnotexist)
* [HashComparison](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-hashcomparison)
* [HashGeneration](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-hashgeneration)
* [InCorrectPhoneNumberOrOtp](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-incorrectphonenumberorotp)
* [InCorrectUserNameOrPassword](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-incorrectusernameorpassword)
* [InCorrectUserOrOtp](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-incorrectuserorotp)
* [IncorrectPassword](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-incorrectpassword)
* [InvalidCredentials](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidcredentials)
* [InvalidIntegratedLoginUrl](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidintegratedloginurl)
* [InvalidKeyErrorType](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidkeyerrortype)
* [InvalidOTP](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidotp)
* [InvalidPassword](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidpassword)
* [InvalidToken](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidtoken)
* [InvalidUserType](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidusertype)
* [InvalidWebhookKey](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidwebhookkey)
* [InvitationCodeExpired](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invitationcodeexpired)
* [InvitationCodeNotFound](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invitationcodenotfound)
* [InvitationCodeRedeemed](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invitationcoderedeemed)
* [InvitationNotForUser](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invitationnotforuser)
* [MissingAuthHeaderField](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-missingauthheaderfield)
* [MissingIntegratedLoginUrl](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-missingintegratedloginurl)
* [MissingParamsErrorType](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-missingparamserrortype)
* [NoAccessToUser](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-noaccesstouser)
* [NoAppAccessToUser](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-noappaccesstouser)
* [NoPasswordSet](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-nopasswordset)
* [NoSamePassword](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-nosamepassword)
* [NotFoundErrorType](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-notfounderrortype)
* [NotPartOfApp](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-notpartofapp)
* [OTPExpired](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-otpexpired)
* [PasswordAlreadySet](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-passwordalreadyset)
* [RefreshTokenAlreadyExchanged](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-refreshtokenalreadyexchanged)
* [RefreshTokenNotFound](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-refreshtokennotfound)
* [SelfSignUpNotAllowed](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-selfsignupnotallowed)
* [ServerNoAccessToUser](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-servernoaccesstouser)
* [SignInIntegrated](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-signinintegrated)
* [SignUpIncomplete](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-signupincomplete)
* [TokenExpiredType](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-tokenexpiredtype)
* [TokenNotStarted](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-tokennotstarted)
* [TooManyFailedAttempts](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-toomanyfailedattempts)
* [UserAlreadySignedUp](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-useralreadysignedup)
* [UserDisabled](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-userdisabled)
* [UserInactive](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-userinactive)
* [UserNotFound](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-usernotfound)
* [YouMustAcceptTermsAndPrivacy](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-youmustaccepttermsandprivacy)

## Enumeration members

###  AccountAlreadyExists

• **AccountAlreadyExists**: = "AuthErrorTypeAccountAlreadyExists"

Defined in types.ts:1403

___

###  AccountAlreadyPreVerified

• **AccountAlreadyPreVerified**: = "AuthErrorTypeAccountAlreadyPreVerified"

Defined in types.ts:1400

___

###  AccountNotPreVerified

• **AccountNotPreVerified**: = "AuthErrorTypeAccountNotPreVerified"

Defined in types.ts:1399

___

###  AdminAlreadyExists

• **AdminAlreadyExists**: = "AuthErrorTypeAdminAlreadyExists"

Defined in types.ts:1404

___

###  AdminNotAssociatedWithApp

• **AdminNotAssociatedWithApp**: = "AuthErrorTypeAdminNotAssociatedWithApp"

Defined in types.ts:1416

___

###  AppHasNoNotificationSetup

• **AppHasNoNotificationSetup**: = "AuthErrorTypeAppHasNoNotificationSetup"

Defined in types.ts:1390

___

###  EmailDoesNotExist

• **EmailDoesNotExist**: = "AuthErrorTypeEmailDoesNotExist"

Defined in types.ts:1426

___

###  HashComparison

• **HashComparison**: = "AuthErrorTypeHashComparison"

Defined in types.ts:1410

___

###  HashGeneration

• **HashGeneration**: = "AuthErrorTypeHashGeneration"

Defined in types.ts:1409

___

###  InCorrectPhoneNumberOrOtp

• **InCorrectPhoneNumberOrOtp**: = "AuthErrorTypeInCorrectPhoneNumberOrOtp"

Defined in types.ts:1432

___

###  InCorrectUserNameOrPassword

• **InCorrectUserNameOrPassword**: = "AuthErrorTypeInCorrectUserNameOrPassword"

Defined in types.ts:1431

___

###  InCorrectUserOrOtp

• **InCorrectUserOrOtp**: = "AuthErrorTypeInCorrectUserOrOtp"

Defined in types.ts:1433

___

###  IncorrectPassword

• **IncorrectPassword**: = "AuthErrorTypeIncorrectPassword"

Defined in types.ts:1395

___

###  InvalidCredentials

• **InvalidCredentials**: = "AuthErrorTypeInvalidCredentials"

Defined in types.ts:1408

___

###  InvalidIntegratedLoginUrl

• **InvalidIntegratedLoginUrl**: = "AuthErrorTypeInvalidIntegratedLoginUrl"

Defined in types.ts:1428

___

###  InvalidKeyErrorType

• **InvalidKeyErrorType**: = "AuthErrorTypeInvalidKey"

Defined in types.ts:1384

___

###  InvalidOTP

• **InvalidOTP**: = "AuthErrorTypeInvalidOTP"

Defined in types.ts:1414

___

###  InvalidPassword

• **InvalidPassword**: = "AuthErrorTypeInvalidPassword"

Defined in types.ts:1388

___

###  InvalidToken

• **InvalidToken**: = "AuthErrorTypeInvalidToken"

Defined in types.ts:1406

___

###  InvalidUserType

• **InvalidUserType**: = "AuthErrorTypeInvalidUserType"

Defined in types.ts:1385

___

###  InvalidWebhookKey

• **InvalidWebhookKey**: = "AuthErrorTypeInvalidWebhookKey"

Defined in types.ts:1392

___

###  InvitationCodeExpired

• **InvitationCodeExpired**: = "AuthErrorTypeInvitationCodeExpired"

Defined in types.ts:1418

___

###  InvitationCodeNotFound

• **InvitationCodeNotFound**: = "AuthErrorTypeInvitationCodeNotFound"

Defined in types.ts:1417

___

###  InvitationCodeRedeemed

• **InvitationCodeRedeemed**: = "AuthErrorTypeInvitationCodeRedeemed"

Defined in types.ts:1419

___

###  InvitationNotForUser

• **InvitationNotForUser**: = "AuthErrorTypeInvitationNotForUser"

Defined in types.ts:1420

___

###  MissingAuthHeaderField

• **MissingAuthHeaderField**: = "AuthErrorTypeMissingAuthHeaderField"

Defined in types.ts:1407

___

###  MissingIntegratedLoginUrl

• **MissingIntegratedLoginUrl**: = "AuthErrorTypeMissingIntegratedLoginUrl"

Defined in types.ts:1427

___

###  MissingParamsErrorType

• **MissingParamsErrorType**: = "AuthErrorTypeMissingParams"

Defined in types.ts:1387

___

###  NoAccessToUser

• **NoAccessToUser**: = "AuthErrorTypeNoAccessToUser"

Defined in types.ts:1393

___

###  NoAppAccessToUser

• **NoAppAccessToUser**: = "AuthErrorTypeNoAppAccessToUser"

Defined in types.ts:1401

___

###  NoPasswordSet

• **NoPasswordSet**: = "AuthErrorTypeNoPasswordSet"

Defined in types.ts:1397

___

###  NoSamePassword

• **NoSamePassword**: = "AuthErrorTypeNoSamePassword"

Defined in types.ts:1396

___

###  NotFoundErrorType

• **NotFoundErrorType**: = "AuthErrorTypeNotFound"

Defined in types.ts:1386

___

###  NotPartOfApp

• **NotPartOfApp**: = "AuthErrorTypeNotPartOfApp"

Defined in types.ts:1391

___

###  OTPExpired

• **OTPExpired**: = "AuthErrorTypeOTPExpired"

Defined in types.ts:1415

___

###  PasswordAlreadySet

• **PasswordAlreadySet**: = "AuthErrorTypePasswordAlreadySet"

Defined in types.ts:1398

___

###  RefreshTokenAlreadyExchanged

• **RefreshTokenAlreadyExchanged**: = "AuthErrorTypeRefreshTokenAlreadyExchanged"

Defined in types.ts:1411

___

###  RefreshTokenNotFound

• **RefreshTokenNotFound**: = "AuthErrorTypeRefreshTokenNotFound"

Defined in types.ts:1412

___

###  SelfSignUpNotAllowed

• **SelfSignUpNotAllowed**: = "AuthErrorTypeSelfSignUpNotAllowed"

Defined in types.ts:1425

___

###  ServerNoAccessToUser

• **ServerNoAccessToUser**: = "AuthErrorTypeServerNoAccessToUser"

Defined in types.ts:1394

___

###  SignInIntegrated

• **SignInIntegrated**: = "AuthErrorTypeSignInIntegrated"

Defined in types.ts:1424

___

###  SignUpIncomplete

• **SignUpIncomplete**: = "AuthErrorTypeSignUpIncomplete"

Defined in types.ts:1422

___

###  TokenExpiredType

• **TokenExpiredType**: = "AuthErrorTypeTokenExpired"

Defined in types.ts:1389

___

###  TokenNotStarted

• **TokenNotStarted**: = "AuthErrorTypeTokenNotStarted"

Defined in types.ts:1402

___

###  TooManyFailedAttempts

• **TooManyFailedAttempts**: = "AuthErrorTypeTooManyFailedAttempts"

Defined in types.ts:1430

___

###  UserAlreadySignedUp

• **UserAlreadySignedUp**: = "AuthErrorTypeUserAlreadySignedUp"

Defined in types.ts:1421

___

###  UserDisabled

• **UserDisabled**: = "AuthErrorTypeUserDisabled"

Defined in types.ts:1413

___

###  UserInactive

• **UserInactive**: = "AuthErrorTypeUserInactive"

Defined in types.ts:1423

___

###  UserNotFound

• **UserNotFound**: = "AuthErrorTypeUserNotFound"

Defined in types.ts:1405

___

###  YouMustAcceptTermsAndPrivacy

• **YouMustAcceptTermsAndPrivacy**: = "AuthErrorTypeYouMustAcceptTermsAndPrivacy"

Defined in types.ts:1429
