[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [DateErrorType](sayhey.chatkiterror.errortype.dateerrortype.md)

# Enumeration: DateErrorType

## Index

### Enumeration members

* [InvalidFromDateError](sayhey.chatkiterror.errortype.dateerrortype.md#markdown-header-invalidfromdateerror)
* [InvalidToDateError](sayhey.chatkiterror.errortype.dateerrortype.md#markdown-header-invalidtodateerror)

## Enumeration members

###  InvalidFromDateError

• **InvalidFromDateError**: = "DateErrorTypeInvalidFromDateError"

Defined in types.ts:1525

___

###  InvalidToDateError

• **InvalidToDateError**: = "DateErrorTypeInvalidToDateError"

Defined in types.ts:1526
