[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [QueryCondition](querycondition.md)

# Enumeration: QueryCondition

## Index

### Enumeration members

* [AND](querycondition.md#markdown-header-and)
* [OR](querycondition.md#markdown-header-or)

## Enumeration members

###  AND

• **AND**: = "and"

Defined in types.ts:2029

___

###  OR

• **OR**: = "or"

Defined in types.ts:2030
