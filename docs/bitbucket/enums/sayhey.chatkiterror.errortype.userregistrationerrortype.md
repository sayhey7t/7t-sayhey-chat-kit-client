[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [UserRegistrationErrorType](sayhey.chatkiterror.errortype.userregistrationerrortype.md)

# Enumeration: UserRegistrationErrorType

## Index

### Enumeration members

* [ContactCompanyAdmin](sayhey.chatkiterror.errortype.userregistrationerrortype.md#markdown-header-contactcompanyadmin)

## Enumeration members

###  ContactCompanyAdmin

• **ContactCompanyAdmin**: = "UserRegistrationErrorTypeContactCompanyAdmin"

Defined in types.ts:1482
