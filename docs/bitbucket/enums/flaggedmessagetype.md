[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessageType](flaggedmessagetype.md)

# Enumeration: FlaggedMessageType

## Index

### Enumeration members

* [HIGH](flaggedmessagetype.md#markdown-header-high)
* [LOW](flaggedmessagetype.md#markdown-header-low)
* [USER_REPORTED](flaggedmessagetype.md#markdown-header-user_reported)

## Enumeration members

###  HIGH

• **HIGH**: = "high"

Defined in types.ts:2021

___

###  LOW

• **LOW**: = "low"

Defined in types.ts:2022

___

###  USER_REPORTED

• **USER_REPORTED**: = "user_reported"

Defined in types.ts:2023
