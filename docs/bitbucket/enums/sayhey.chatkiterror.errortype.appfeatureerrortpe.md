[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [AppFeatureErrorTpe](sayhey.chatkiterror.errortype.appfeatureerrortpe.md)

# Enumeration: AppFeatureErrorTpe

## Index

### Enumeration members

* [AdminPortalLink](sayhey.chatkiterror.errortype.appfeatureerrortpe.md#markdown-header-adminportallink)
* [AlreadyExists](sayhey.chatkiterror.errortype.appfeatureerrortpe.md#markdown-header-alreadyexists)
* [InvalidParams](sayhey.chatkiterror.errortype.appfeatureerrortpe.md#markdown-header-invalidparams)
* [NotFound](sayhey.chatkiterror.errortype.appfeatureerrortpe.md#markdown-header-notfound)

## Enumeration members

###  AdminPortalLink

• **AdminPortalLink**: = "AppFeatureErrorTypeAdminPortalLink"

Defined in types.ts:1561

___

###  AlreadyExists

• **AlreadyExists**: = "AppFeatureErrorTypeAlreadyExists"

Defined in types.ts:1559

___

###  InvalidParams

• **InvalidParams**: = "AppFeatureErrorTypeInvalidParams"

Defined in types.ts:1560

___

###  NotFound

• **NotFound**: = "AppFeatureErrorTpeNotFound"

Defined in types.ts:1558
