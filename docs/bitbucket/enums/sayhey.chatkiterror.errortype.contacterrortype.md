[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [ContactErrorType](sayhey.chatkiterror.errortype.contacterrortype.md)

# Enumeration: ContactErrorType

## Index

### Enumeration members

* [ContactAlreadyExists](sayhey.chatkiterror.errortype.contacterrortype.md#markdown-header-contactalreadyexists)

## Enumeration members

###  ContactAlreadyExists

• **ContactAlreadyExists**: = "ContactErrorTypeContactAlreadyExists"

Defined in types.ts:1606
