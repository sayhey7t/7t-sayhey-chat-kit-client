[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [EmailErrorType](sayhey.chatkiterror.errortype.emailerrortype.md)

# Enumeration: EmailErrorType

## Index

### Enumeration members

* [EmailDomainNotAllowed](sayhey.chatkiterror.errortype.emailerrortype.md#markdown-header-emaildomainnotallowed)
* [MissingEmailService](sayhey.chatkiterror.errortype.emailerrortype.md#markdown-header-missingemailservice)

## Enumeration members

###  EmailDomainNotAllowed

• **EmailDomainNotAllowed**: = "EmailErrorTypeEmailDomainNotAllowed"

Defined in types.ts:1536

___

###  MissingEmailService

• **MissingEmailService**: = "EmailErrorTypeMissingEmailService"

Defined in types.ts:1535
