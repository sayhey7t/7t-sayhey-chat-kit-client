[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [DivisionErrorType](sayhey.chatkiterror.errortype.divisionerrortype.md)

# Enumeration: DivisionErrorType

## Index

### Enumeration members

* [AlreadyExists](sayhey.chatkiterror.errortype.divisionerrortype.md#markdown-header-alreadyexists)
* [DepartmentRequired](sayhey.chatkiterror.errortype.divisionerrortype.md#markdown-header-departmentrequired)
* [NotPartOfDepartment](sayhey.chatkiterror.errortype.divisionerrortype.md#markdown-header-notpartofdepartment)
* [UpdateDefaultNotAllowed](sayhey.chatkiterror.errortype.divisionerrortype.md#markdown-header-updatedefaultnotallowed)

## Enumeration members

###  AlreadyExists

• **AlreadyExists**: = "DivisionErrorTypeAlreadyExists"

Defined in types.ts:1553

___

###  DepartmentRequired

• **DepartmentRequired**: = "DivisionErrorTypeDepartmentRequired"

Defined in types.ts:1554

___

###  NotPartOfDepartment

• **NotPartOfDepartment**: = "DivisionErrorTypeNotPartOfDepartment"

Defined in types.ts:1552

___

###  UpdateDefaultNotAllowed

• **UpdateDefaultNotAllowed**: = "DivisionErrorTypeUpdateDefaultNotAllowed"

Defined in types.ts:1555
