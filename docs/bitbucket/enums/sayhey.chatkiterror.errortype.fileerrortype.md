[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [FileErrorType](sayhey.chatkiterror.errortype.fileerrortype.md)

# Enumeration: FileErrorType

## Index

### Enumeration members

* [FileNotForMessage](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-filenotformessage)
* [FileSizeLimitExceeded](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-filesizelimitexceeded)
* [ImageNotForConversation](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-imagenotforconversation)
* [ImageNotForUser](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-imagenotforuser)
* [IncorrectFileType](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-incorrectfiletype)
* [InvalidFieldNameForFile](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-invalidfieldnameforfile)
* [InvalidFileName](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-invalidfilename)
* [InvalidHeaders](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-invalidheaders)
* [InvalidMetadata](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-invalidmetadata)
* [NoFileSentType](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-nofilesenttype)
* [NoHeadersFound](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-noheadersfound)
* [NoThumbnailFound](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-nothumbnailfound)
* [NotForApp](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-notforapp)
* [NotFound](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-notfound)
* [NotSent](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-notsent)
* [SizeLimitExceeded](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-sizelimitexceeded)
* [SizeNotFound](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-sizenotfound)

## Enumeration members

###  FileNotForMessage

• **FileNotForMessage**: = "FileErrorTypeFileNotForMessage"

Defined in types.ts:1447

___

###  FileSizeLimitExceeded

• **FileSizeLimitExceeded**: = "FileErrorTypeFileSizeLimitExceeded"

Defined in types.ts:1440

___

###  ImageNotForConversation

• **ImageNotForConversation**: = "FileErrorTypeImageNotForConversation"

Defined in types.ts:1445

___

###  ImageNotForUser

• **ImageNotForUser**: = "FileErrorTypeImageNotForUser"

Defined in types.ts:1446

___

###  IncorrectFileType

• **IncorrectFileType**: = "FileErrorTypeIncorrectFileType"

Defined in types.ts:1450

___

###  InvalidFieldNameForFile

• **InvalidFieldNameForFile**: = "FileErrorTypeInvalidFieldNameForFile"

Defined in types.ts:1438

___

###  InvalidFileName

• **InvalidFileName**: = "FileErrorTypeInvalidFileName"

Defined in types.ts:1441

___

###  InvalidHeaders

• **InvalidHeaders**: = "FileErrorTypeInvalidHeaders"

Defined in types.ts:1452

___

###  InvalidMetadata

• **InvalidMetadata**: = "FileErrorTypeInvalidMetadata"

Defined in types.ts:1439

___

###  NoFileSentType

• **NoFileSentType**: = "FileErrorTypeNoFileSentType"

Defined in types.ts:1437

___

###  NoHeadersFound

• **NoHeadersFound**: = "FileErrorTypeNoHeadersFound"

Defined in types.ts:1451

___

###  NoThumbnailFound

• **NoThumbnailFound**: = "FileErrorTypeNoThumbnailFound"

Defined in types.ts:1444

___

###  NotForApp

• **NotForApp**: = "FileErrorTypeNotForApp"

Defined in types.ts:1449

___

###  NotFound

• **NotFound**: = "FileErrorTypeNotFound"

Defined in types.ts:1443

___

###  NotSent

• **NotSent**: = "FileErrorTypeNotSent"

Defined in types.ts:1442

___

###  SizeLimitExceeded

• **SizeLimitExceeded**: = "FileErrorTypeSizeLimitExceeded"

Defined in types.ts:1436

___

###  SizeNotFound

• **SizeNotFound**: = "FileErrorTypeSizeNotFound"

Defined in types.ts:1448
