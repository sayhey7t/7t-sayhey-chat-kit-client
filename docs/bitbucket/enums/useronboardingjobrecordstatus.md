[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserOnboardingJobRecordStatus](useronboardingjobrecordstatus.md)

# Enumeration: UserOnboardingJobRecordStatus

## Index

### Enumeration members

* [Completed](useronboardingjobrecordstatus.md#markdown-header-completed)
* [Errored](useronboardingjobrecordstatus.md#markdown-header-errored)
* [Pending](useronboardingjobrecordstatus.md#markdown-header-pending)

## Enumeration members

###  Completed

• **Completed**: = "completed"

Defined in types.ts:2387

___

###  Errored

• **Errored**: = "errored"

Defined in types.ts:2389

___

###  Pending

• **Pending**: = "pending"

Defined in types.ts:2388
