[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [KeywordErrorType](sayhey.chatkiterror.errortype.keyworderrortype.md)

# Enumeration: KeywordErrorType

## Index

### Enumeration members

* [AlreadyExists](sayhey.chatkiterror.errortype.keyworderrortype.md#markdown-header-alreadyexists)
* [NoChangeInPriority](sayhey.chatkiterror.errortype.keyworderrortype.md#markdown-header-nochangeinpriority)
* [NoDuplicates](sayhey.chatkiterror.errortype.keyworderrortype.md#markdown-header-noduplicates)
* [NotFound](sayhey.chatkiterror.errortype.keyworderrortype.md#markdown-header-notfound)

## Enumeration members

###  AlreadyExists

• **AlreadyExists**: = "KeywordErrorTypeAlreadyExists"

Defined in types.ts:1580

___

###  NoChangeInPriority

• **NoChangeInPriority**: = "KeywordErrorTypeNoChangeInPriority"

Defined in types.ts:1582

___

###  NoDuplicates

• **NoDuplicates**: = "KeywordErrorTypeNoDuplicates"

Defined in types.ts:1579

___

###  NotFound

• **NotFound**: = "KeywordErrorTypeNotFound"

Defined in types.ts:1581
