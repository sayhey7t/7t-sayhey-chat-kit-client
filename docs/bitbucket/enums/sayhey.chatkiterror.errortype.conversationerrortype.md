[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [ConversationErrorType](sayhey.chatkiterror.errortype.conversationerrortype.md)

# Enumeration: ConversationErrorType

## Index

### Enumeration members

* [AlreadyAnOwner](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-alreadyanowner)
* [AlreadyExists](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-alreadyexists)
* [AlreadyMuted](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-alreadymuted)
* [AlreadyPinned](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-alreadypinned)
* [AlreadyRemovedUsers](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-alreadyremovedusers)
* [CannotAddExistingDivisions](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotaddexistingdivisions)
* [CannotAddExistingUsers](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotaddexistingusers)
* [CannotChangeAccessToSelf](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotchangeaccesstoself)
* [CannotChatWithSelf](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotchatwithself)
* [CannotConverseWithUser](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotconversewithuser)
* [CannotRemoveNonExistingDivisions](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotremovenonexistingdivisions)
* [CannotRemoveOwners](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotremoveowners)
* [DivisionIdOrMemberIdRequiredForSpaceGroup](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-divisionidormemberidrequiredforspacegroup)
* [DivisionsNotInApp](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-divisionsnotinapp)
* [InvalidConversationGroupType](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-invalidconversationgrouptype)
* [InvalidConversationUserType](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-invalidconversationusertype)
* [InvalidType](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-invalidtype)
* [MustBeCreator](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-mustbecreator)
* [MustBeOwner](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-mustbeowner)
* [NoValidUsersToAdd](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-novaliduserstoadd)
* [NoValidUsersToRemove](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-novaliduserstoremove)
* [NotAnOwner](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notanowner)
* [NotFound](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notfound)
* [NotFoundForApp](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notfoundforapp)
* [NotFoundForUser](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notfoundforuser)
* [NotOnMute](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notonmute)
* [NotPartOfApp](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notpartofapp)
* [NotPinned](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notpinned)
* [NudgeLessThanThreshold](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-nudgelessthanthreshold)
* [UsersNotInApp](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-usersnotinapp)
* [UsersNotInConversation](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-usersnotinconversation)

## Enumeration members

###  AlreadyAnOwner

• **AlreadyAnOwner**: = "ConversationErrorTypeAlreadyAnOwner"

Defined in types.ts:1488

___

###  AlreadyExists

• **AlreadyExists**: = "ConversationErrorTypeAlreadyExists"

Defined in types.ts:1497

___

###  AlreadyMuted

• **AlreadyMuted**: = "ConversationErrorTypeAlreadyMuted"

Defined in types.ts:1499

___

###  AlreadyPinned

• **AlreadyPinned**: = "ConversationErrorTypeAlreadyPinned"

Defined in types.ts:1501

___

###  AlreadyRemovedUsers

• **AlreadyRemovedUsers**: = "ConversationErrorTypeAlreadyRemovedUsers"

Defined in types.ts:1508

___

###  CannotAddExistingDivisions

• **CannotAddExistingDivisions**: = "ConversationErrorTypeCannotAddExistingDivisions"

Defined in types.ts:1512

___

###  CannotAddExistingUsers

• **CannotAddExistingUsers**: = "ConversationErrorTypeCannotAddExistingUsers"

Defined in types.ts:1507

___

###  CannotChangeAccessToSelf

• **CannotChangeAccessToSelf**: = "ConversationErrorTypeCannotChangeAccessToSelf"

Defined in types.ts:1506

___

###  CannotChatWithSelf

• **CannotChatWithSelf**: = "ConversationErrorTypeCannotChatWithSelf"

Defined in types.ts:1498

___

###  CannotConverseWithUser

• **CannotConverseWithUser**: = "ConversationErrorTypeCannotConverseWithUser"

Defined in types.ts:1486

___

###  CannotRemoveNonExistingDivisions

• **CannotRemoveNonExistingDivisions**: = "ConversationErrorTypeCannotRemoveNonExistingDivisions"

Defined in types.ts:1513

___

###  CannotRemoveOwners

• **CannotRemoveOwners**: = "ConversationErrorTypeCannotRemoveOwners"

Defined in types.ts:1510

___

###  DivisionIdOrMemberIdRequiredForSpaceGroup

• **DivisionIdOrMemberIdRequiredForSpaceGroup**: = "ConversationErrorTypeDivisionIdOrMemberIdRequiredForSpaceGroup"

Defined in types.ts:1514

___

###  DivisionsNotInApp

• **DivisionsNotInApp**: = "ConversationErrorTypeDivisionsNotInApp"

Defined in types.ts:1511

___

###  InvalidConversationGroupType

• **InvalidConversationGroupType**: = "ConversationErrorTypeInvalidConversationGroupType"

Defined in types.ts:1491

___

###  InvalidConversationUserType

• **InvalidConversationUserType**: = "ConversationErrorTypeInvalidConversationUserType"

Defined in types.ts:1492

___

###  InvalidType

• **InvalidType**: = "ConversationErrorTypeInvalidType"

Defined in types.ts:1490

___

###  MustBeCreator

• **MustBeCreator**: = "ConversationErrorTypeMustBeCreator"

Defined in types.ts:1494

___

###  MustBeOwner

• **MustBeOwner**: = "ConversationErrorTypeMustBeOwner"

Defined in types.ts:1493

___

###  NoValidUsersToAdd

• **NoValidUsersToAdd**: = "ConversationErrorTypeNoValidUsersToAdd"

Defined in types.ts:1504

___

###  NoValidUsersToRemove

• **NoValidUsersToRemove**: = "ConversationErrorTypeNoValidUsersToRemove"

Defined in types.ts:1505

___

###  NotAnOwner

• **NotAnOwner**: = "ConversationErrorTypeNotAnOwner"

Defined in types.ts:1487

___

###  NotFound

• **NotFound**: = "ConversationErrorTypeNotFound"

Defined in types.ts:1485

___

###  NotFoundForApp

• **NotFoundForApp**: = "ConversationErrorTypeNotFoundForApp"

Defined in types.ts:1489

___

###  NotFoundForUser

• **NotFoundForUser**: = "ConversationErrorTypeNotFoundForUser"

Defined in types.ts:1496

___

###  NotOnMute

• **NotOnMute**: = "ConversationErrorTypeNotOnMute"

Defined in types.ts:1500

___

###  NotPartOfApp

• **NotPartOfApp**: = "ConversationErrorTypeNotPartOfApp"

Defined in types.ts:1495

___

###  NotPinned

• **NotPinned**: = "ConversationErrorTypeNotPinned"

Defined in types.ts:1502

___

###  NudgeLessThanThreshold

• **NudgeLessThanThreshold**: = "ConversationErrorTypeNudgeLessThanThreshold"

Defined in types.ts:1515

___

###  UsersNotInApp

• **UsersNotInApp**: = "ConversationErrorTypeUsersNotInApp"

Defined in types.ts:1503

___

###  UsersNotInConversation

• **UsersNotInConversation**: = "ConversationErrorTypeUsersNotInConversation"

Defined in types.ts:1509
