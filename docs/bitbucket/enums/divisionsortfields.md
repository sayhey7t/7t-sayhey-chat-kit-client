[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [DivisionSortFields](divisionsortfields.md)

# Enumeration: DivisionSortFields

## Index

### Enumeration members

* [DivisionName](divisionsortfields.md#markdown-header-divisionname)

## Enumeration members

###  DivisionName

• **DivisionName**: = "divisionName"

Defined in types.ts:1801
