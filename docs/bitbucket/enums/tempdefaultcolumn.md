[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [tempDefaultColumn](tempdefaultcolumn.md)

# Enumeration: tempDefaultColumn

## Index

### Enumeration members

* [isDefault](tempdefaultcolumn.md#markdown-header-isdefault)

## Enumeration members

###  isDefault

• **isDefault**: = 1

Defined in types.ts:1782
