[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [TimeFrameType](timeframetype.md)

# Enumeration: TimeFrameType

## Index

### Enumeration members

* [AllTime](timeframetype.md#markdown-header-alltime)
* [Month](timeframetype.md#markdown-header-month)
* [Year](timeframetype.md#markdown-header-year)

## Enumeration members

###  AllTime

• **AllTime**: = "allTime"

Defined in types.ts:1960

___

###  Month

• **Month**: = "month"

Defined in types.ts:1961

___

###  Year

• **Year**: = "year"

Defined in types.ts:1962
