[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [ServerErrorType](sayhey.chatkiterror.errortype.servererrortype.md)

# Enumeration: ServerErrorType

## Index

### Enumeration members

* [InternalServerError](sayhey.chatkiterror.errortype.servererrortype.md#markdown-header-internalservererror)
* [UnknownError](sayhey.chatkiterror.errortype.servererrortype.md#markdown-header-unknownerror)

## Enumeration members

###  InternalServerError

• **InternalServerError**: = "InternalServerError"

Defined in types.ts:1456

___

###  UnknownError

• **UnknownError**: = "UnknownError"

Defined in types.ts:1455
