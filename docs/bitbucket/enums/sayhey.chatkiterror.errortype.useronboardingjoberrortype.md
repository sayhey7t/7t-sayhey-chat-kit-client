[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [UserOnboardingJobErrorType](sayhey.chatkiterror.errortype.useronboardingjoberrortype.md)

# Enumeration: UserOnboardingJobErrorType

## Index

### Enumeration members

* [NotCompleted](sayhey.chatkiterror.errortype.useronboardingjoberrortype.md#markdown-header-notcompleted)
* [NotFound](sayhey.chatkiterror.errortype.useronboardingjoberrortype.md#markdown-header-notfound)

## Enumeration members

###  NotCompleted

• **NotCompleted**: = "UserOnboardingJobErrorTypeNotCompleted"

Defined in types.ts:1596

___

###  NotFound

• **NotFound**: = "UserOnboardingJobErrorTypeNotFound"

Defined in types.ts:1595
