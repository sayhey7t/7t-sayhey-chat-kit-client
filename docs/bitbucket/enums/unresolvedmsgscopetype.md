[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UnresolvedMsgScopeType](unresolvedmsgscopetype.md)

# Enumeration: UnresolvedMsgScopeType

## Index

### Enumeration members

* [Global](unresolvedmsgscopetype.md#markdown-header-global)
* [Mine](unresolvedmsgscopetype.md#markdown-header-mine)

## Enumeration members

###  Global

• **Global**: = "global"

Defined in types.ts:1974

___

###  Mine

• **Mine**: = "mine"

Defined in types.ts:1975
