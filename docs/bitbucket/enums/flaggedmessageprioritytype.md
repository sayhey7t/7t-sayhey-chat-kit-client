[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [FlaggedMessagePriorityType](flaggedmessageprioritytype.md)

# Enumeration: FlaggedMessagePriorityType

## Index

### Enumeration members

* [HIGH](flaggedmessageprioritytype.md#markdown-header-high)
* [LOW](flaggedmessageprioritytype.md#markdown-header-low)

## Enumeration members

###  HIGH

• **HIGH**: = "high"

Defined in types.ts:2247

___

###  LOW

• **LOW**: = "low"

Defined in types.ts:2248
