[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [UserErrorType](sayhey.chatkiterror.errortype.usererrortype.md)

# Enumeration: UserErrorType

## Index

### Enumeration members

* [AlreadyDisabled](sayhey.chatkiterror.errortype.usererrortype.md#markdown-header-alreadydisabled)
* [AlreadyEnabled](sayhey.chatkiterror.errortype.usererrortype.md#markdown-header-alreadyenabled)
* [AlreadyMuted](sayhey.chatkiterror.errortype.usererrortype.md#markdown-header-alreadymuted)
* [NotFound](sayhey.chatkiterror.errortype.usererrortype.md#markdown-header-notfound)
* [NotOnMute](sayhey.chatkiterror.errortype.usererrortype.md#markdown-header-notonmute)

## Enumeration members

###  AlreadyDisabled

• **AlreadyDisabled**: = "UserErrorTypeAlreadyDisabled"

Defined in types.ts:1478

___

###  AlreadyEnabled

• **AlreadyEnabled**: = "UserErrorTypeAlreadyEnabled"

Defined in types.ts:1477

___

###  AlreadyMuted

• **AlreadyMuted**: = "UserErrorTypeAlreadyMuted"

Defined in types.ts:1475

___

###  NotFound

• **NotFound**: = "UserErrorTypeNotFound"

Defined in types.ts:1479

___

###  NotOnMute

• **NotOnMute**: = "UserErrorTypeNotOnMute"

Defined in types.ts:1476
