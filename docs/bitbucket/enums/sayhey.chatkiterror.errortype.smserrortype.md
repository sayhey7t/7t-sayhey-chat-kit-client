[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [SmsErrorType](sayhey.chatkiterror.errortype.smserrortype.md)

# Enumeration: SmsErrorType

## Index

### Enumeration members

* [CountryCodeNotSupported](sayhey.chatkiterror.errortype.smserrortype.md#markdown-header-countrycodenotsupported)
* [NotConfigured](sayhey.chatkiterror.errortype.smserrortype.md#markdown-header-notconfigured)
* [NotConfiguredForClient](sayhey.chatkiterror.errortype.smserrortype.md#markdown-header-notconfiguredforclient)
* [RequiredCountryCode](sayhey.chatkiterror.errortype.smserrortype.md#markdown-header-requiredcountrycode)

## Enumeration members

###  CountryCodeNotSupported

• **CountryCodeNotSupported**: = "SmsErrorTypeCountryCodeNotSupported"

Defined in types.ts:1542

___

###  NotConfigured

• **NotConfigured**: = "SmsErrorTypeNotConfigured"

Defined in types.ts:1539

___

###  NotConfiguredForClient

• **NotConfiguredForClient**: = "SmsErrorTypeNotConfiguredForClient"

Defined in types.ts:1540

___

###  RequiredCountryCode

• **RequiredCountryCode**: = "SmsErrorTypeRequiredCountryCode"

Defined in types.ts:1541
