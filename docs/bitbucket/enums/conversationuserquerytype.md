[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationUserQueryType](conversationuserquerytype.md)

# Enumeration: ConversationUserQueryType

## Index

### Enumeration members

* [All](conversationuserquerytype.md#markdown-header-all)
* [Current](conversationuserquerytype.md#markdown-header-current)
* [Removed](conversationuserquerytype.md#markdown-header-removed)

## Enumeration members

###  All

• **All**: = "all"

Defined in types.ts:595

___

###  Current

• **Current**: = "current"

Defined in types.ts:596

___

###  Removed

• **Removed**: = "removed"

Defined in types.ts:597
