[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SortOrderType](sortordertype.md)

# Enumeration: SortOrderType

## Index

### Enumeration members

* [ASC](sortordertype.md#markdown-header-asc)
* [DESC](sortordertype.md#markdown-header-desc)

## Enumeration members

###  ASC

• **ASC**: = 1

Defined in types.ts:1792

___

###  DESC

• **DESC**: = -1

Defined in types.ts:1793
