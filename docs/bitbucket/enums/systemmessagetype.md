[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SystemMessageType](systemmessagetype.md)

# Enumeration: SystemMessageType

## Index

### Enumeration members

* [ExternalUserConversationInvitationAccepted](systemmessagetype.md#markdown-header-externaluserconversationinvitationaccepted)
* [ExternalUserNudged](systemmessagetype.md#markdown-header-externalusernudged)
* [GroupCreated](systemmessagetype.md#markdown-header-groupcreated)
* [GroupDisabled](systemmessagetype.md#markdown-header-groupdisabled)
* [GroupNameChanged](systemmessagetype.md#markdown-header-groupnamechanged)
* [GroupPictureChanged](systemmessagetype.md#markdown-header-grouppicturechanged)
* [IndividualChatCreated](systemmessagetype.md#markdown-header-individualchatcreated)
* [MemberAdded](systemmessagetype.md#markdown-header-memberadded)
* [MemberRemoved](systemmessagetype.md#markdown-header-memberremoved)
* [OwnerAdded](systemmessagetype.md#markdown-header-owneradded)
* [OwnerRemoved](systemmessagetype.md#markdown-header-ownerremoved)

## Enumeration members

###  ExternalUserConversationInvitationAccepted

• **ExternalUserConversationInvitationAccepted**: = "ExternalUserConversationInvitationAccepted"

Defined in types.ts:140

___

###  ExternalUserNudged

• **ExternalUserNudged**: = "ExternalUserNudged"

Defined in types.ts:141

___

###  GroupCreated

• **GroupCreated**: = "GroupCreated"

Defined in types.ts:138

___

###  GroupDisabled

• **GroupDisabled**: = "GroupDisabled"

Defined in types.ts:137

___

###  GroupNameChanged

• **GroupNameChanged**: = "GroupNameChanged"

Defined in types.ts:136

___

###  GroupPictureChanged

• **GroupPictureChanged**: = "GroupPictureChanged"

Defined in types.ts:135

___

###  IndividualChatCreated

• **IndividualChatCreated**: = "IndividualChatCreated"

Defined in types.ts:139

___

###  MemberAdded

• **MemberAdded**: = "MemberAdded"

Defined in types.ts:132

___

###  MemberRemoved

• **MemberRemoved**: = "MemberRemoved"

Defined in types.ts:131

___

###  OwnerAdded

• **OwnerAdded**: = "OwnerAdded"

Defined in types.ts:134

___

###  OwnerRemoved

• **OwnerRemoved**: = "OwnerRemoved"

Defined in types.ts:133
