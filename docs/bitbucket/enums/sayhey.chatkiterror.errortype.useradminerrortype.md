[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [UserAdminErrorType](sayhey.chatkiterror.errortype.useradminerrortype.md)

# Enumeration: UserAdminErrorType

## Index

### Enumeration members

* [AlreadyExists](sayhey.chatkiterror.errortype.useradminerrortype.md#markdown-header-alreadyexists)
* [CannotUpdateSelf](sayhey.chatkiterror.errortype.useradminerrortype.md#markdown-header-cannotupdateself)
* [Disabled](sayhey.chatkiterror.errortype.useradminerrortype.md#markdown-header-disabled)
* [NoAccessToUserAdmin](sayhey.chatkiterror.errortype.useradminerrortype.md#markdown-header-noaccesstouseradmin)
* [NoChangeInLeaveStatus](sayhey.chatkiterror.errortype.useradminerrortype.md#markdown-header-nochangeinleavestatus)
* [NoEmailUpdateAccessOfSignedUpUsers](sayhey.chatkiterror.errortype.useradminerrortype.md#markdown-header-noemailupdateaccessofsignedupusers)
* [NotFound](sayhey.chatkiterror.errortype.useradminerrortype.md#markdown-header-notfound)
* [SignUpIncomplete](sayhey.chatkiterror.errortype.useradminerrortype.md#markdown-header-signupincomplete)
* [UserAdminNoAccessToUser](sayhey.chatkiterror.errortype.useradminerrortype.md#markdown-header-useradminnoaccesstouser)

## Enumeration members

###  AlreadyExists

• **AlreadyExists**: = "UserAdminErrorTypeAlreadyExists"

Defined in types.ts:1565

___

###  CannotUpdateSelf

• **CannotUpdateSelf**: = "UserAdminErrorTypeCannotUpdateSelf"

Defined in types.ts:1568

___

###  Disabled

• **Disabled**: = "UserAdminErrorTypeDisabled"

Defined in types.ts:1572

___

###  NoAccessToUserAdmin

• **NoAccessToUserAdmin**: = "UserAdminErrorTypeNoAccessToUserAdmin"

Defined in types.ts:1566

___

###  NoChangeInLeaveStatus

• **NoChangeInLeaveStatus**: = "UserAdminErrorTypeNoChangeInLeaveStatus"

Defined in types.ts:1570

___

###  NoEmailUpdateAccessOfSignedUpUsers

• **NoEmailUpdateAccessOfSignedUpUsers**: = "UserAdminErrorTypeNoEmailUpdateAccessOfSignedUpUsers"

Defined in types.ts:1567

___

###  NotFound

• **NotFound**: = "UserAdminErrorTypeNotFound"

Defined in types.ts:1564

___

###  SignUpIncomplete

• **SignUpIncomplete**: = "UserAdminErrorTypeSignUpIncomplete"

Defined in types.ts:1571

___

###  UserAdminNoAccessToUser

• **UserAdminNoAccessToUser**: = "UserAdminNoAccessToUser"

Defined in types.ts:1569
