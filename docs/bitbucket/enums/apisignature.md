[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [APISignature](apisignature.md)

# Enumeration: APISignature

## Index

### Enumeration members

* [ME](apisignature.md#markdown-header-me)

## Enumeration members

###  ME

• **ME**: = "me"

Defined in types.ts:511
