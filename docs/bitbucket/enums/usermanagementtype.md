[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserManagementType](usermanagementtype.md)

# Enumeration: UserManagementType

## Index

### Enumeration members

* [Perm](usermanagementtype.md#markdown-header-perm)
* [Temp](usermanagementtype.md#markdown-header-temp)

## Enumeration members

###  Perm

• **Perm**: = "permanent"

Defined in types.ts:516

___

###  Temp

• **Temp**: = "temporary"

Defined in types.ts:517
