[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [TokenType](tokentype.md)

# Enumeration: TokenType

## Index

### Enumeration members

* [AccessToken](tokentype.md#markdown-header-accesstoken)
* [NewUserSetPasswordToken](tokentype.md#markdown-header-newusersetpasswordtoken)
* [PublisherToken](tokentype.md#markdown-header-publishertoken)
* [RefreshToken](tokentype.md#markdown-header-refreshtoken)
* [ResetPasswordToken](tokentype.md#markdown-header-resetpasswordtoken)
* [VerifyAccountToken](tokentype.md#markdown-header-verifyaccounttoken)

## Enumeration members

###  AccessToken

• **AccessToken**: = "TokenTypeAccessToken"

Defined in types.ts:1933

___

###  NewUserSetPasswordToken

• **NewUserSetPasswordToken**: = "TokenTypeNewUserSetPasswordToken"

Defined in types.ts:1937

___

###  PublisherToken

• **PublisherToken**: = "TokenTypePublisherToken"

Defined in types.ts:1935

___

###  RefreshToken

• **RefreshToken**: = "TokenTypeRefreshToken"

Defined in types.ts:1934

___

###  ResetPasswordToken

• **ResetPasswordToken**: = "TokenTypeResetPasswordToken"

Defined in types.ts:1936

___

###  VerifyAccountToken

• **VerifyAccountToken**: = "TokenTypeVerifyAccountToken"

Defined in types.ts:1938
