[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [UserManagementErrorType](sayhey.chatkiterror.errortype.usermanagementerrortype.md)

# Enumeration: UserManagementErrorType

## Index

### Enumeration members

* [MustBeHrAdmin](sayhey.chatkiterror.errortype.usermanagementerrortype.md#markdown-header-mustbehradmin)
* [NoDuplicateUsers](sayhey.chatkiterror.errortype.usermanagementerrortype.md#markdown-header-noduplicateusers)
* [NoSameAdminAssignment](sayhey.chatkiterror.errortype.usermanagementerrortype.md#markdown-header-nosameadminassignment)
* [NotHrAdmin](sayhey.chatkiterror.errortype.usermanagementerrortype.md#markdown-header-nothradmin)
* [UserAlreadyAssigned](sayhey.chatkiterror.errortype.usermanagementerrortype.md#markdown-header-useralreadyassigned)

## Enumeration members

###  MustBeHrAdmin

• **MustBeHrAdmin**: = "UserManagementErrorTypeMustBeHrAdmin"

Defined in types.ts:1519

___

###  NoDuplicateUsers

• **NoDuplicateUsers**: = "UserManagementErrorTypeNoDuplicateUsers"

Defined in types.ts:1520

___

###  NoSameAdminAssignment

• **NoSameAdminAssignment**: = "UserManagementErrorTypeNoSameAdminAssignment"

Defined in types.ts:1518

___

###  NotHrAdmin

• **NotHrAdmin**: = "UserManagementErrorTypeNotHrAdmin"

Defined in types.ts:1521

___

###  UserAlreadyAssigned

• **UserAlreadyAssigned**: = "UserManagementErrorTypeUserAlreadyAssigned"

Defined in types.ts:1522
