[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationGroupType](conversationgrouptype.md)

# Enumeration: ConversationGroupType

## Index

### Enumeration members

* [Quick](conversationgrouptype.md#markdown-header-quick)
* [Regular](conversationgrouptype.md#markdown-header-regular)
* [Sms](conversationgrouptype.md#markdown-header-sms)
* [Space](conversationgrouptype.md#markdown-header-space)

## Enumeration members

###  Quick

• **Quick**: = "quick"

Defined in types.ts:227

___

###  Regular

• **Regular**: = "regular"

Defined in types.ts:226

___

###  Sms

• **Sms**: = "sms"

Defined in types.ts:229

___

###  Space

• **Space**: = "space"

Defined in types.ts:228
