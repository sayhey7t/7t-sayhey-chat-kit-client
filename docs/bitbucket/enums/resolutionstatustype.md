[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ResolutionStatusType](resolutionstatustype.md)

# Enumeration: ResolutionStatusType

## Index

### Enumeration members

* [ACTION_TAKEN](resolutionstatustype.md#markdown-header-action_taken)
* [NO_ACTION](resolutionstatustype.md#markdown-header-no_action)

## Enumeration members

###  ACTION_TAKEN

• **ACTION_TAKEN**: = "action_taken"

Defined in types.ts:2299

___

###  NO_ACTION

• **NO_ACTION**: = "no_action"

Defined in types.ts:2298
