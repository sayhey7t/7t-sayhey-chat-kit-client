[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserTypes](usertypes.md)

# Enumeration: UserTypes

## Index

### Enumeration members

* [Admin](usertypes.md#markdown-header-admin)
* [HrAdmin](usertypes.md#markdown-header-hradmin)
* [SuperHrAdmin](usertypes.md#markdown-header-superhradmin)
* [User](usertypes.md#markdown-header-user)

## Enumeration members

###  Admin

• **Admin**: = "admin"

Defined in types.ts:491

___

###  HrAdmin

• **HrAdmin**: = "hr_admin"

Defined in types.ts:492

___

###  SuperHrAdmin

• **SuperHrAdmin**: = "super_hr_admin"

Defined in types.ts:493

___

###  User

• **User**: = "user"

Defined in types.ts:490
