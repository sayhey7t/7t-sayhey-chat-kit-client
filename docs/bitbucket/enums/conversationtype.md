[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ConversationType](conversationtype.md)

# Enumeration: ConversationType

Conversation Types

## Index

### Enumeration members

* [Group](conversationtype.md#markdown-header-group)
* [Individual](conversationtype.md#markdown-header-individual)

## Enumeration members

###  Group

• **Group**: = "group"

Defined in types.ts:221

___

###  Individual

• **Individual**: = "individual"

Defined in types.ts:222
