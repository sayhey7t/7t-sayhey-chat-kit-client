[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [MessageErrorType](sayhey.chatkiterror.errortype.messageerrortype.md)

# Enumeration: MessageErrorType

## Index

### Enumeration members

* [CannotDeleteSystemMessage](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-cannotdeletesystemmessage)
* [InvalidDate](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-invaliddate)
* [IsoDateStringRequired](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-isodatestringrequired)
* [MessageAlreadyRead](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-messagealreadyread)
* [MissingMessageId](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-missingmessageid)
* [NotFound](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-notfound)
* [NotFoundForUser](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-notfoundforuser)

## Enumeration members

###  CannotDeleteSystemMessage

• **CannotDeleteSystemMessage**: = "MessageErrorTypeCannotDeleteSystemMessage"

Defined in types.ts:1470

___

###  InvalidDate

• **InvalidDate**: = "MessageErrorTypeInvalidDate"

Defined in types.ts:1466

___

###  IsoDateStringRequired

• **IsoDateStringRequired**: = "MessageErrorTypeIsoDateStringRequired"

Defined in types.ts:1472

___

###  MessageAlreadyRead

• **MessageAlreadyRead**: = "MessageErrorTypeMessageAlreadyRead"

Defined in types.ts:1469

___

###  MissingMessageId

• **MissingMessageId**: = "MessageErrorTypeMissingMessageId"

Defined in types.ts:1471

___

###  NotFound

• **NotFound**: = "MessageErrorTypeNotFound"

Defined in types.ts:1467

___

###  NotFoundForUser

• **NotFoundForUser**: = "MessageErrorTypeNotFoundForUser"

Defined in types.ts:1468
