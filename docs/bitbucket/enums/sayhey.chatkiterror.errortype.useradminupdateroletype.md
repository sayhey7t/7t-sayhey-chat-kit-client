[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [UserAdminUpdateRoleType](sayhey.chatkiterror.errortype.useradminupdateroletype.md)

# Enumeration: UserAdminUpdateRoleType

## Index

### Enumeration members

* [AlreadyHasRequestedRole](sayhey.chatkiterror.errortype.useradminupdateroletype.md#markdown-header-alreadyhasrequestedrole)
* [CannotUpdateRoleOfSelf](sayhey.chatkiterror.errortype.useradminupdateroletype.md#markdown-header-cannotupdateroleofself)
* [ManagedUserExist](sayhey.chatkiterror.errortype.useradminupdateroletype.md#markdown-header-manageduserexist)

## Enumeration members

###  AlreadyHasRequestedRole

• **AlreadyHasRequestedRole**: = "UserAdminErrorTypeAlreadyHasRequestedRole"

Defined in types.ts:1587

___

###  CannotUpdateRoleOfSelf

• **CannotUpdateRoleOfSelf**: = "UserAdminUpdateRoleTypeCannotUpdateRoleOfSelf"

Defined in types.ts:1585

___

###  ManagedUserExist

• **ManagedUserExist**: = "UserAdminUpdateRoleTypeManagedUserExist"

Defined in types.ts:1586
