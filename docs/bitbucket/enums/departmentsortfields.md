[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [DepartmentSortFields](departmentsortfields.md)

# Enumeration: DepartmentSortFields

## Index

### Enumeration members

* [DepartmentName](departmentsortfields.md#markdown-header-departmentname)

## Enumeration members

###  DepartmentName

• **DepartmentName**: = "departmentName"

Defined in types.ts:1797
