[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UnmanagedUsersSortFields](unmanageduserssortfields.md)

# Enumeration: UnmanagedUsersSortFields

## Index

### Enumeration members

* [UserDepartment](unmanageduserssortfields.md#markdown-header-userdepartment)
* [UserEmail](unmanageduserssortfields.md#markdown-header-useremail)
* [UserEmpCode](unmanageduserssortfields.md#markdown-header-userempcode)
* [UserName](unmanageduserssortfields.md#markdown-header-username)
* [UserTitle](unmanageduserssortfields.md#markdown-header-usertitle)

## Enumeration members

###  UserDepartment

• **UserDepartment**: = "userDepartment"

Defined in types.ts:559

___

###  UserEmail

• **UserEmail**: = "userEmail"

Defined in types.ts:557

___

###  UserEmpCode

• **UserEmpCode**: = "userEmployeeCode"

Defined in types.ts:558

___

###  UserName

• **UserName**: = "userName"

Defined in types.ts:556

___

###  UserTitle

• **UserTitle**: = "userTitle"

Defined in types.ts:560
