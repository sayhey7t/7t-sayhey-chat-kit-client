[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [ReactionType](reactiontype.md)

# Enumeration: ReactionType

Reaction Types

## Index

### Enumeration members

* [Clap](reactiontype.md#markdown-header-clap)
* [Exclamation](reactiontype.md#markdown-header-exclamation)
* [HaHa](reactiontype.md#markdown-header-haha)
* [Heart](reactiontype.md#markdown-header-heart)
* [Thumb](reactiontype.md#markdown-header-thumb)

## Enumeration members

###  Clap

• **Clap**: = "Clap"

Defined in types.ts:31

___

###  Exclamation

• **Exclamation**: = "Exclamation"

Defined in types.ts:33

___

###  HaHa

• **HaHa**: = "HaHa"

Defined in types.ts:32

___

###  Heart

• **Heart**: = "Heart"

Defined in types.ts:30

___

###  Thumb

• **Thumb**: = "Thumb"

Defined in types.ts:29
