[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserStatusType](userstatustype.md)

# Enumeration: UserStatusType

## Index

### Enumeration members

* [All](userstatustype.md#markdown-header-all)
* [Disabled](userstatustype.md#markdown-header-disabled)
* [Enabled](userstatustype.md#markdown-header-enabled)

## Enumeration members

###  All

• **All**: = "all"

Defined in types.ts:544

___

###  Disabled

• **Disabled**: = "disabled"

Defined in types.ts:543

___

###  Enabled

• **Enabled**: = "enabled"

Defined in types.ts:542
