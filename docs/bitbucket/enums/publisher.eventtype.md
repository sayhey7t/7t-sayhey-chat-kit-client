[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [Publisher](../modules/publisher.md) › [EventType](publisher.eventtype.md)

# Enumeration: EventType

## Index

### Enumeration members

* [ConversationCreated](publisher.eventtype.md#markdown-header-conversationcreated)
* [ConversationGroupInfoChanged](publisher.eventtype.md#markdown-header-conversationgroupinfochanged)
* [ConversationGroupMemberAdded](publisher.eventtype.md#markdown-header-conversationgroupmemberadded)
* [ConversationGroupMemberRemoved](publisher.eventtype.md#markdown-header-conversationgroupmemberremoved)
* [ConversationGroupOwnerAdded](publisher.eventtype.md#markdown-header-conversationgroupowneradded)
* [ConversationGroupOwnerRemoved](publisher.eventtype.md#markdown-header-conversationgroupownerremoved)
* [ConversationHideAndClear](publisher.eventtype.md#markdown-header-conversationhideandclear)
* [ConversationHideAndClearAll](publisher.eventtype.md#markdown-header-conversationhideandclearall)
* [ConversationMuted](publisher.eventtype.md#markdown-header-conversationmuted)
* [ConversationPinned](publisher.eventtype.md#markdown-header-conversationpinned)
* [ConversationQuickGroupChangedToGroup](publisher.eventtype.md#markdown-header-conversationquickgroupchangedtogroup)
* [ConversationUnmuted](publisher.eventtype.md#markdown-header-conversationunmuted)
* [ConversationUnpinned](publisher.eventtype.md#markdown-header-conversationunpinned)
* [ExternalUserConversationInvitationAccepted](publisher.eventtype.md#markdown-header-externaluserconversationinvitationaccepted)
* [ExternalUserNudged](publisher.eventtype.md#markdown-header-externalusernudged)
* [MessageDeleted](publisher.eventtype.md#markdown-header-messagedeleted)
* [MessageReactionsUpdate](publisher.eventtype.md#markdown-header-messagereactionsupdate)
* [MessageRead](publisher.eventtype.md#markdown-header-messageread)
* [NewMessage](publisher.eventtype.md#markdown-header-newmessage)
* [UserDeleted](publisher.eventtype.md#markdown-header-userdeleted)
* [UserDisabled](publisher.eventtype.md#markdown-header-userdisabled)
* [UserEnabled](publisher.eventtype.md#markdown-header-userenabled)
* [UserInfoChanged](publisher.eventtype.md#markdown-header-userinfochanged)
* [UserReactedToMessage](publisher.eventtype.md#markdown-header-userreactedtomessage)
* [UserUnReactedToMessage](publisher.eventtype.md#markdown-header-userunreactedtomessage)

## Enumeration members

###  ConversationCreated

• **ConversationCreated**: = "CONVERSATION_CREATED"

Defined in types.ts:692

___

###  ConversationGroupInfoChanged

• **ConversationGroupInfoChanged**: = "CONVERSATION_GROUP_INFO_CHANGED"

Defined in types.ts:699

___

###  ConversationGroupMemberAdded

• **ConversationGroupMemberAdded**: = "CONVERSATION_GROUP_MEMBER_ADDED"

Defined in types.ts:705

___

###  ConversationGroupMemberRemoved

• **ConversationGroupMemberRemoved**: = "CONVERSATION_GROUP_MEMBER_REMOVED"

Defined in types.ts:706

___

###  ConversationGroupOwnerAdded

• **ConversationGroupOwnerAdded**: = "CONVERSATION_GROUP_OWNER_ADDED"

Defined in types.ts:703

___

###  ConversationGroupOwnerRemoved

• **ConversationGroupOwnerRemoved**: = "CONVERSATION_GROUP_OWNER_REMOVED"

Defined in types.ts:704

___

###  ConversationHideAndClear

• **ConversationHideAndClear**: = "CONVERSATION_HIDE_AND_CLEAR"

Defined in types.ts:697

___

###  ConversationHideAndClearAll

• **ConversationHideAndClearAll**: = "CONVERSATION_HIDE_AND_CLEAR_ALL"

Defined in types.ts:698

___

###  ConversationMuted

• **ConversationMuted**: = "CONVERSATION_MUTED"

Defined in types.ts:693

___

###  ConversationPinned

• **ConversationPinned**: = "CONVERSATION_PINNED"

Defined in types.ts:695

___

###  ConversationQuickGroupChangedToGroup

• **ConversationQuickGroupChangedToGroup**: = "CONVERSATION_QUICK_GROUP_CHANGED_TO_GROUP"

Defined in types.ts:700

___

###  ConversationUnmuted

• **ConversationUnmuted**: = "CONVERSATION_UNMUTED"

Defined in types.ts:694

___

###  ConversationUnpinned

• **ConversationUnpinned**: = "CONVERSATION_UNPINNED"

Defined in types.ts:696

___

###  ExternalUserConversationInvitationAccepted

• **ExternalUserConversationInvitationAccepted**: = "EXTERNAL_USER_CONVERSATION_INVITATION_ACCEPTED"

Defined in types.ts:701

___

###  ExternalUserNudged

• **ExternalUserNudged**: = "EXTERNAL_USER_NUDGED"

Defined in types.ts:715

___

###  MessageDeleted

• **MessageDeleted**: = "MESSAGE_DELETED"

Defined in types.ts:712

___

###  MessageReactionsUpdate

• **MessageReactionsUpdate**: = "MESSAGE_REACTIONS_UPDATE"

Defined in types.ts:709

___

###  MessageRead

• **MessageRead**: = "MESSAGE_READ"

Defined in types.ts:714

___

###  NewMessage

• **NewMessage**: = "NEW_MESSAGE"

Defined in types.ts:691

___

###  UserDeleted

• **UserDeleted**: = "USER_DELETED"

Defined in types.ts:713

___

###  UserDisabled

• **UserDisabled**: = "USER_DISABLED"

Defined in types.ts:710

___

###  UserEnabled

• **UserEnabled**: = "USER_ENABLED"

Defined in types.ts:711

___

###  UserInfoChanged

• **UserInfoChanged**: = "USER_INFO_CHANGED"

Defined in types.ts:702

___

###  UserReactedToMessage

• **UserReactedToMessage**: = "USER_REACTED_TO_MESSAGE"

Defined in types.ts:707

___

###  UserUnReactedToMessage

• **UserUnReactedToMessage**: = "USER_UNREACTED_TO_MESSAGE"

Defined in types.ts:708
