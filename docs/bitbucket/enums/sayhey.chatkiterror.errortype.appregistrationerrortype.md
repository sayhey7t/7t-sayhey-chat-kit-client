[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [AppRegistrationErrorType](sayhey.chatkiterror.errortype.appregistrationerrortype.md)

# Enumeration: AppRegistrationErrorType

## Index

### Enumeration members

* [MissingEmailConfigParams](sayhey.chatkiterror.errortype.appregistrationerrortype.md#markdown-header-missingemailconfigparams)

## Enumeration members

###  MissingEmailConfigParams

• **MissingEmailConfigParams**: = "AppRegistrationErrorTypeMissingEmailConfigParams"

Defined in types.ts:1529
