[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [UserAdminStatusType](useradminstatustype.md)

# Enumeration: UserAdminStatusType

## Index

### Enumeration members

* [All](useradminstatustype.md#markdown-header-all)
* [Disabled](useradminstatustype.md#markdown-header-disabled)
* [Enabled](useradminstatustype.md#markdown-header-enabled)

## Enumeration members

###  All

• **All**: = "all"

Defined in types.ts:507

___

###  Disabled

• **Disabled**: = "disabled"

Defined in types.ts:506

___

###  Enabled

• **Enabled**: = "enabled"

Defined in types.ts:505
