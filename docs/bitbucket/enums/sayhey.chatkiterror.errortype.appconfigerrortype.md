[@7t/sayhey-chat-kit-client](../README.md) › [Globals](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [AppConfigErrorType](sayhey.chatkiterror.errortype.appconfigerrortype.md)

# Enumeration: AppConfigErrorType

## Index

### Enumeration members

* [AlreadyExists](sayhey.chatkiterror.errortype.appconfigerrortype.md#markdown-header-alreadyexists)
* [NotFound](sayhey.chatkiterror.errortype.appconfigerrortype.md#markdown-header-notfound)

## Enumeration members

###  AlreadyExists

• **AlreadyExists**: = "AppConfigErrorTypeAlreadyExists"

Defined in types.ts:1576

___

###  NotFound

• **NotFound**: = "AppConfigErrorTypeNotFound"

Defined in types.ts:1575
