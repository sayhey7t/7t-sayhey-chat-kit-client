import StaticAxios, { AxiosInstance } from 'axios'
import decode from 'jwt-decode'
import { err, ok, Result } from 'neverthrow'

import httpClient from './axios'
import { API_VERSION, ApiKeyName, ChatAdminRoutes, ChatCommonRoutes } from './constants'
import { mimeTypes } from './helper'
import { AuditMessageMapper, MessageMapper, UserMapper } from './mapper'
import {
  AdminAuthResponse,
  AllFlaggedMessageCountDailyDTO,
  AuditMessageBatchDTO,
  AuditMessageDomainEntityBatchDTO,
  AuditMessageDomainEntityDTO,
  ConversableUsersForUserAdminBatchDTO,
  ConversableUsersForUserAdminBatchResult,
  ConversationDTOForUserAdmin,
  ConversationGroupBatchDTOForUserAdminWithUserCount,
  ConversationGroupDTOForUserAdminWithUserCount,
  ConversationUserQuery,
  ConversationUserQueryType,
  ConversationUsersBatchForUserAdminDTO,
  ConversationUsersForUserAdminBatch,
  DefaultGroupImageUrlSet,
  DepartmentBatchDTO,
  DepartmentDivisionDTO,
  DepartmentSortFields,
  DivisionBatchDTO,
  DivisionSortFields,
  FileUpload,
  FlaggedMessageBatchDTO,
  FlaggedMessageCountDTO,
  FlaggedMessageDetailsDTO,
  FlaggedMessageResolutionBatchDTO,
  FlaggedMessageResolutionScanBatchDTO,
  FlaggedMessageResolutionUserReportBatchDTO,
  FlaggedMessageScanBatchDTO,
  FlaggedMessageUserReportBatchDTO,
  HttpMethod,
  ImageSizes,
  KeywordBatchDTO,
  KeyWordPriorityTypes,
  PeekMessageDomainEntity,
  PeekMessageDTO,
  QueryCondition,
  ReachableUsersQuery,
  ReactedUserDTO,
  ReactionType,
  ResolutionStatusType,
  SayHey,
  SentMessageCountDailyDTO,
  SortOrderType,
  TimeFrameType,
  TotalUsersCountDTO,
  UnmanagedUsersBatchDTO,
  UnmanagedUsersCountDTO,
  UnmanagedUsersSortFields,
  UnresolvedFlaggedMessagesCountDTO,
  UnresolvedMsgScopeType,
  UserAdminBatchDTO,
  UserAdminDTO,
  UserAdminSearchDTO,
  UserAdminSortFields,
  UserAdminStatusType,
  UserBasicInfoBatchDTO,
  UserManagementBatchDTO,
  UserManagementCountQueryParams,
  UserManagementCountResultParams,
  UserManagementSortFields,
  UserManagementType,
  UserMovementSelected,
  UserOnboardingJobBatchDTO,
  UserOnboardingJobStatus,
  UserRole,
  UserSearchCriteria,
  UserStatusType,
  UserTypes
} from './types'

export * from './types'

export class ChatAdminKit {
  accessToken = ''

  refreshToken = ''

  private _apiVersion: string = API_VERSION

  httpClient!: AxiosInstance

  appId = ''

  apiKey = ''

  serverUrl = ''

  authDetailChangeListener?: (res: AdminAuthResponse) => Promise<void> | void

  /** NOTE: Will be called when refresh token exchange session API call has failed */
  sessionExpiredListener?: () => void

  private _initialized = false

  private _subscribed = false

  private _isUserAuthenticated = false

  private exchangeTokenPromise: Promise<Result<boolean, SayHey.Error>> | null = null

  private disableAutoRefreshToken?: boolean

  private loginId = 0

  public isUserAuthenticated = () => this._isUserAuthenticated

  /**
   * Setup methods
   */
  public initialize(params: {
    sayheyAppId: string
    sayheyApiKey: string
    sayheyUrl: string
    apiVersion?: 'v1' | 'v2' | 'v3' | 'v4'
    disableAutoRefreshToken?: boolean
  }) {
    const { sayheyAppId, sayheyApiKey, sayheyUrl, disableAutoRefreshToken, apiVersion } = params
    this.serverUrl = sayheyUrl
    this._apiVersion = apiVersion || API_VERSION
    const baseURL = `${this.serverUrl}/${this._apiVersion}/apps/${sayheyAppId}`
    this.apiKey = sayheyApiKey
    this.appId = sayheyAppId
    this.disableAutoRefreshToken = disableAutoRefreshToken
    this.httpClient = httpClient.createWithoutAuth({
      apiKey: this.apiKey,
      baseURL
    })
    this._initialized = true
  }

  /**
   * Listeners
   */
  public addListeners = (listeners: {
    onAuthDetailChange: (res: AdminAuthResponse) => Promise<void> | void
    onSessionExpired?: () => void
  }): Result<boolean, SayHey.Error> => {
    if (this._initialized) {
      const { onAuthDetailChange, onSessionExpired } = listeners
      this._subscribed = true
      this.authDetailChangeListener = onAuthDetailChange
      this.sessionExpiredListener = onSessionExpired

      return ok(true)
    }
    return err(
      new SayHey.Error(
        'ChatKit has not been initialized!',
        SayHey.ErrorType.InternalErrorType.Uninitialized
      )
    )
  }

  /**
   *
   * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
   */
  private guard = async <T>(
    cb: (...args: any[]) => Promise<T | SayHey.Error>
  ): Promise<Result<T, SayHey.Error>> => {
    if (!this._initialized) {
      return err(
        new SayHey.Error(
          'ChatKit has not been initialized!',
          SayHey.ErrorType.InternalErrorType.Uninitialized
        )
      )
    }
    if (!this._subscribed) {
      return err(
        new SayHey.Error(
          'ChatKit events have not been subscribed to!',
          SayHey.ErrorType.InternalErrorType.Uninitialized
        )
      )
    }
    try {
      const res = await cb()
      if (res instanceof SayHey.Error) {
        return err(res)
      }
      return ok(res)
    } catch (e) {
      if (e instanceof SayHey.Error) {
        return err(e)
      }
      return err(new SayHey.Error('Unknown error', SayHey.ErrorType.InternalErrorType.Unknown))
    }
  }

  public exchangeTokenOnce = async (): Promise<Result<boolean, SayHey.Error>> => {
    if (this.exchangeTokenPromise) {
      return this.exchangeTokenPromise
    }
    this.exchangeTokenPromise = this.exchangeToken()
    const response = await this.exchangeTokenPromise
    this.exchangeTokenPromise = null
    return response
  }

  /**
   *
   * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
   */
  private guardWithAuth = async <T>(
    cb: (...args: any[]) => Promise<T | SayHey.Error>
  ): Promise<Result<T, SayHey.Error>> => {
    // Capturing current login id
    const currentLoginId = this.loginId
    const res = await this.guard(cb)
    if (res.isErr()) {
      if (
        !this.disableAutoRefreshToken &&
        res.error.type === SayHey.ErrorType.AuthErrorType.TokenExpiredType
      ) {
        // If token expires check if token has been exchanged since this call started
        if (currentLoginId === this.loginId) {
          const response = await this.exchangeTokenOnce()
          if (response.isOk() && response.value === true) {
            // --> NOTE: Modified to check response is also true => isUserAuthenticated, else logout
            return this.guard(cb)
          }
          await this.signOut()
        } else {
          // If token has been exchanged, use the new credentials to make the request again
          return this.guard(cb)
        }
      }
    }
    return res
  }

  /**
   *
   * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
   * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
   */
  private updateAuthDetails = async (accessToken: string, refreshToken: string) => {
    this.accessToken = accessToken
    this.refreshToken = refreshToken
    const id = accessToken ? decode<any>(accessToken).id : ''
    if (accessToken && refreshToken) {
      this.loginId += 1
      this.httpClient = httpClient.createWithAuth({
        apiKey: this.apiKey,
        baseURL: `${this.serverUrl}/${this._apiVersion}/apps/${this.appId}`,
        accessToken: this.accessToken
      })
    }
    await this.authDetailChangeListener?.({
      userId: id,
      accessToken,
      refreshToken
    })

    // NOTE: Should be done at the end?
    this._isUserAuthenticated = !!(accessToken && refreshToken)
  }

  /**
   *
   * @param urlPath - url path returned from SayHey
   */
  public getMediaSourceDetails = (urlPath: string) => ({
    uri: `${this.serverUrl}${urlPath}`,
    method: 'GET',
    headers: {
      [ApiKeyName]: this.apiKey,
      Authorization: `Bearer ${this.accessToken}`,
      Accept: 'image/*, video/*, audio/*'
    }
  })

  public getMediaDirectUrl = async (params: {
    urlPath: string
    size?: ImageSizes
  }): Promise<Result<string, SayHey.Error>> => {
    const getMediaDirectUrl = async () => {
      const queryString: any = {
        noRedirect: true
      }
      if (params.size) {
        queryString.size = params.size
      }
      const { data } = await this.httpClient({
        url: `${this.serverUrl}${params.urlPath}`,
        method: 'GET',
        params: queryString
      })
      return data.url as string
    }
    return this.guardWithAuth(getMediaDirectUrl)
  }

  private uploadFile = async (
    fileUri: string | Blob,
    fileName: string,
    progress?: (percentage: number) => void,
    thumbnailId?: string
  ): Promise<Result<string, SayHey.Error>> => {
    const uploadFile = async () => {
      const {
        UploadFile: { Endpoint, Method }
      } = ChatCommonRoutes
      const formData = new FormData()
      if (fileUri instanceof Blob) {
        formData.append('file', fileUri, fileName)
        if (thumbnailId) {
          formData.append('thumbnailId', thumbnailId)
        }
      } else {
        const extension = fileName.split('.').pop()
        if (extension) {
          formData.append('file', {
            uri: fileUri,
            name: fileName,
            type: mimeTypes[extension.toLowerCase()]
          } as any)
          if (thumbnailId) {
            formData.append('thumbnailId', thumbnailId)
          }
        } else {
          return new SayHey.Error('Invalid file')
        }
      }
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: formData,
        onUploadProgress: progress
          ? (event: ProgressEvent) => {
              const { loaded, total } = event
              progress((loaded / total) * 0.95)
            }
          : undefined
      })
      progress?.(1)
      return data.id
    }
    return this.guardWithAuth(uploadFile)
  }

  public uploadImage = async (
    fileUri: string | Blob,
    fileName: string
  ): Promise<Result<string, SayHey.Error>> => {
    const extension = fileName
      .split('.')
      .pop()
      ?.toLowerCase()
    const isSupportedImage = mimeTypes[extension || ''].split('/')[0] === 'image'
    if (isSupportedImage) {
      return this.uploadFile(fileUri, fileName)
    }
    return err(
      new SayHey.Error('Please select a valid image!', SayHey.ErrorType.InternalErrorType.BadData)
    )
  }

  /**
   * Token APIs
   */

  /**
   *
   * @param email - user email for SayHey
   * @param password - user password for SayHey
   *
   */

  public exchangeToken = async (): Promise<Result<boolean, SayHey.Error>> => {
    const exchangeToken = async () => {
      try {
        const { data } = await StaticAxios({
          method: HttpMethod.Post,
          url: `${this.serverUrl}/${this._apiVersion}/tokens/exchange`,
          data: {
            refreshToken: this.refreshToken
          }
        })
        const { refreshToken, accessToken } = data
        if (refreshToken && accessToken) {
          await this.updateAuthDetails(accessToken, refreshToken)
          // this._isUserAuthenticated = true
        } else {
          await this.updateAuthDetails('', '')
          this.sessionExpiredListener?.()
          // this._isUserAuthenticated = false
        }
      } catch (e) {
        await this.updateAuthDetails('', '')
        this.sessionExpiredListener?.()
        // this._isUserAuthenticated = false
      }
      return this._isUserAuthenticated
    }
    return this.guard(exchangeToken) // --> NOTE: Looks like it will always be ok(true| false), never err()??
  }

  /*  User Admin APIs */

  public signOut = async () => {
    this._isUserAuthenticated = false // NOTE: Is manually set in the beginning as well?
    this.loginId = 0
    await this.updateAuthDetails('', '')
  }

  public signInWithToken = async (
    accessToken: string,
    refreshToken: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const signInWithTokenCallback = async () => {
      await this.updateAuthDetails(accessToken, refreshToken)
      // this._isUserAuthenticated = true
      return true
    }
    return this.guard(signInWithTokenCallback)
  }

  public invite = async ({
    firstName,
    lastName,
    email,
    role
  }: {
    firstName: string
    lastName: string
    email: string
    role: UserTypes.HrAdmin | UserTypes.SuperHrAdmin
  }): Promise<Result<boolean, SayHey.Error>> => {
    const invite = async () => {
      const {
        Invite: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          firstName,
          lastName,
          email,
          role
        }
      })
      return true
    }

    return this.guard(invite)
  }

  public signUp = async ({
    password,
    userAdminId,
    invitationCode
  }: {
    password: string
    userAdminId: string
    invitationCode: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const signUp = async () => {
      const {
        SignUp: { Endpoint, Method }
      } = ChatAdminRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          password,
          userAdminId,
          invitationCode
        }
      })
      const { accessToken, refreshToken } = data
      await this.updateAuthDetails(accessToken, refreshToken)
      return true
    }
    return this.guard(signUp)
  }

  public signIn = async (
    email: string,
    password: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const signIn = async () => {
      const {
        SignIn: { Endpoint, Method }
      } = ChatAdminRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email,
          password
        }
      })
      const { accessToken, refreshToken } = data
      await this.updateAuthDetails(accessToken, refreshToken)

      return true
    }
    return this.guard(signIn)
  }

  public reInvite = async ({
    userAdminId
  }: {
    userAdminId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const reInvite = async () => {
      const {
        ReInvite: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint(userAdminId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(reInvite)
  }

  public disableAdmin = async ({
    userAdminId
  }: {
    userAdminId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const disableAdmin = async () => {
      const {
        DisableAdmin: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint(userAdminId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(disableAdmin)
  }

  public enableAdmin = async ({
    userAdminId
  }: {
    userAdminId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const enableAdmin = async () => {
      const {
        EnableAdmin: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint(userAdminId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(enableAdmin)
  }

  public updateUserAdminInfo = async ({
    firstName,
    lastName,
    email,
    userAdminId
  }: {
    userAdminId: string
    firstName?: string
    lastName?: string
    email?: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const updateUserAdminInfo = async () => {
      const {
        UpdateUserAdminInfo: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint(userAdminId),
        method: Method,
        data: { firstName, lastName, email }
      })
      return true
    }
    return this.guardWithAuth(updateUserAdminInfo)
  }

  public updateUserAdminRole = async ({
    role,
    userAdminId
  }: {
    userAdminId: string
    role: UserTypes.HrAdmin | UserTypes.SuperHrAdmin
  }): Promise<Result<boolean, SayHey.Error>> => {
    const updateUserAdminRole = async () => {
      const {
        UpdateUserAdminRole: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint(userAdminId),
        method: Method,
        data: { role }
      })
      return true
    }
    return this.guardWithAuth(updateUserAdminRole)
  }

  public updateUserAdminOnLeave = async ({
    onLeave
  }: {
    onLeave: boolean
  }): Promise<Result<boolean, SayHey.Error>> => {
    const updateUserAdminOnLeave = async () => {
      const {
        UpdateUserAdminOnLeave: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: { onLeave }
      })
      return true
    }
    return this.guardWithAuth(updateUserAdminOnLeave)
  }

  public forgotPassword = async ({
    email
  }: {
    email: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const forgotPassword = async () => {
      const {
        ForgotPassword: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email
        }
      })
      return true
    }
    return this.guardWithAuth(forgotPassword)
  }

  public resetPassword = async ({
    password,
    resetToken
  }: {
    password: string
    resetToken: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const resetPassword = async () => {
      const {
        ResetPassword: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          password,
          resetToken
        }
      })
      return true
    }
    return this.guardWithAuth(resetPassword)
  }

  public getUserAdmins = async ({
    offset,
    limit,
    search,
    userAdminStatus,
    sort,
    sortOrder,
    role
  }: {
    offset?: number
    limit?: number
    search?: string | null
    userAdminStatus?: UserAdminStatusType
    sort?: UserAdminSortFields
    sortOrder?: SortOrderType
    role?: UserTypes.HrAdmin | UserTypes.SuperHrAdmin
  }): Promise<Result<UserAdminBatchDTO, SayHey.Error>> => {
    const getUserAdminsBatch = async () => {
      const {
        GetUserAdmins: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method,
        params: { offset, limit, search, userAdminStatus, sort, sortOrder, role }
      })
      return {
        ...(data as UserAdminBatchDTO)
      }
    }

    return this.guardWithAuth(getUserAdminsBatch)
  }

  public getUserAdminsSelfInfo = async (): Promise<Result<UserAdminDTO, SayHey.Error>> => {
    const getUserAdminsSelfInfo = async () => {
      const {
        GetUserAdminSelfInfo: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method
      })
      return {
        ...(data as UserAdminDTO)
      }
    }

    return this.guardWithAuth(getUserAdminsSelfInfo)
  }

  public updateUserAdminsSelfInfo = async ({
    firstName,
    lastName
  }: {
    firstName?: string
    lastName?: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const updateUserAdminSelfInfo = async () => {
      const {
        UpdateUserAdminSelfInfo: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      await this.httpClient({
        url,
        method: Method,
        data: {
          firstName,
          lastName
        }
      })
      return true
    }

    return this.guardWithAuth(updateUserAdminSelfInfo)
  }

  /* User Management APIs */

  public assignManagedUsers = async (
    list: { userId: string; userAdminId: string }[]
  ): Promise<Result<{ ids: string[] }, SayHey.Error>> => {
    const assignManagedUsers = async () => {
      const {
        AssignManagedUsers: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          list
        }
      })
      return data
    }
    return this.guardWithAuth(assignManagedUsers)
  }

  public moveAllManagedUsers = async ({
    fromUserAdminId,
    toUserAdminId,
    assignmentType
  }: {
    fromUserAdminId: string
    toUserAdminId: string
    assignmentType: UserManagementType
  }): Promise<Result<boolean, SayHey.Error>> => {
    const moveAllManagedUsers = async () => {
      const {
        MoveAllManagedUsers: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          fromUserAdminId,
          toUserAdminId,
          assignmentType
        }
      })
      return true
    }
    return this.guardWithAuth(moveAllManagedUsers)
  }

  public moveBackAllManagedUsers = async (
    userAdminId: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const moveBackAllManagedUsers = async () => {
      const {
        MoveBackAllManagedUsers: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint(userAdminId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(moveBackAllManagedUsers)
  }

  public getAllManagedUsers = async ({
    offset,
    limit,
    search,
    userStatus,
    userManagementType,
    sort,
    sortOrder
  }: {
    offset?: number
    limit?: number
    search?: string | null
    userStatus?: UserStatusType
    userManagementType?: UserManagementType
    sort?: UserManagementSortFields
    sortOrder?: SortOrderType
  }): Promise<Result<UserManagementBatchDTO, SayHey.Error>> => {
    const getManagedUsers = async () => {
      const {
        GetManagedUsers: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method,
        params: { offset, limit, search, userStatus, userManagementType, sort, sortOrder }
      })
      return {
        ...(data as UserManagementBatchDTO)
      }
    }

    return this.guardWithAuth(getManagedUsers)
  }

  public getAllManagedUsersCount = async (
    queryParams: UserManagementCountQueryParams
  ): Promise<Result<UserManagementCountResultParams, SayHey.Error>> => {
    const getManagedUsers = async () => {
      const {
        GetManagedUsersCount: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method,
        params: queryParams
      })
      return {
        ...(data as UserManagementCountResultParams)
      }
    }

    return this.guardWithAuth(getManagedUsers)
  }

  public moveSelectedUsers = async (
    userMovementSelected: UserMovementSelected
  ): Promise<Result<boolean, SayHey.Error>> => {
    const moveSelectedUsers = async () => {
      const {
        MoveSelectedUsers: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          ...userMovementSelected
        }
      })
      return true
    }
    return this.guardWithAuth(moveSelectedUsers)
  }

  public getUnmanagedUsers = async ({
    offset,
    limit,
    search,
    userStatus,
    sort,
    sortOrder
  }: {
    offset?: number
    limit?: number
    search?: string | null
    userStatus?: UserStatusType
    sort?: UnmanagedUsersSortFields
    sortOrder?: 1 | -1
  }): Promise<Result<UnmanagedUsersBatchDTO, SayHey.Error>> => {
    const getUnmanagedUsers = async () => {
      const {
        GetUnmanagedUsers: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method,
        params: { offset, limit, search, userStatus, sort, sortOrder }
      })
      return {
        ...(data as UnmanagedUsersBatchDTO)
      }
    }

    return this.guardWithAuth(getUnmanagedUsers)
  }

  public getUnmanagedUsersCount = async (): Promise<
    Result<UnmanagedUsersCountDTO, SayHey.Error>
  > => {
    const getUnmanagedUsersCount = async () => {
      const {
        GetUnmanagedUsersCount: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method
      })
      return {
        ...(data as UnmanagedUsersCountDTO)
      }
    }

    return this.guardWithAuth(getUnmanagedUsersCount)
  }

  /* User Onboarding APIs */

  public registerUser = async ({
    firstName,
    lastName,
    email,
    hrAdminId,
    departmentId,
    divisionId,
    employeeId,
    title
  }: {
    firstName: string
    lastName: string
    email: string
    hrAdminId: string
    departmentId: string
    divisionId: string
    employeeId?: string
    title?: string
  }): Promise<Result<{ id: string }, SayHey.Error>> => {
    const userRegistration = async () => {
      const {
        RegisterUser: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          firstName,
          lastName,
          email,
          hrAdminId,
          departmentId,
          divisionId,
          employeeId,
          title
        }
      })

      return data
    }

    return this.guardWithAuth(userRegistration)
  }

  public updateUser = async ({
    firstName,
    lastName,
    email,
    title,
    departmentId,
    divisionId,
    employeeId,
    userId
  }: {
    firstName?: string
    lastName?: string
    email?: string
    title?: string
    departmentId?: string
    divisionId?: string
    employeeId?: string
    userId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const updateUser = async () => {
      const {
        UpdateUser: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint(userId),
        method: Method,
        data: {
          firstName,
          lastName,
          email,
          title,
          departmentId,
          divisionId,
          employeeId
        }
      })

      return true
    }

    return this.guardWithAuth(updateUser)
  }

  /* Department APIs */

  public getDepartments = async ({
    offset,
    limit,
    search,
    sort,
    sortOrder,
    includeDivisions = false
  }: {
    offset?: number
    limit?: number
    search?: string | null
    sort?: DepartmentSortFields
    sortOrder?: SortOrderType
    includeDivisions?: boolean
  }): Promise<Result<DepartmentBatchDTO, SayHey.Error>> => {
    const getDepartments = async () => {
      const {
        GetDepartments: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        params: { offset, limit, search, sort, sortOrder, includeDivisions }
      })
      return data
    }
    return this.guardWithAuth(getDepartments)
  }

  public createDepartment = async (name: string): Promise<Result<{ id: string }, SayHey.Error>> => {
    const createDepartment = async () => {
      const {
        CreateDepartment: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          name
        }
      })
      return data
    }
    return this.guardWithAuth(createDepartment)
  }

  public updateDepartment = async (params: {
    name: string
    departmentId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const updateDepartment = async () => {
      const {
        UpdateDepartments: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint(params.departmentId),
        method: Method,
        data: {
          name: params.name
        }
      })
      return true
    }
    return this.guardWithAuth(updateDepartment)
  }

  /* Division APIs */

  public getDivisions = async ({
    offset,
    limit,
    search,
    sort,
    sortOrder,
    departmentId
  }: {
    departmentId: string
    offset?: number
    limit?: number
    search?: string | null
    sort?: DivisionSortFields
    sortOrder?: SortOrderType
  }): Promise<Result<DivisionBatchDTO, SayHey.Error>> => {
    const getDivisions = async () => {
      const {
        GetDivisions: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint(departmentId),
        method: Method,
        params: { offset, limit, search, sort, sortOrder }
      })
      return data
    }
    return this.guardWithAuth(getDivisions)
  }

  public createDivision = async (params: {
    name: string
    departmentId: string
  }): Promise<Result<{ id: string }, SayHey.Error>> => {
    const createDivision = async () => {
      const {
        CreateDivisions: { Endpoint, Method }
      } = ChatAdminRoutes
      const { name, departmentId } = params
      const { data } = await this.httpClient({
        url: Endpoint(departmentId),
        method: Method,
        data: {
          name
        }
      })
      return data
    }
    return this.guardWithAuth(createDivision)
  }

  public updateDivision = async (params: {
    name: string
    departmentId: string
    divisionId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const updateDivision = async () => {
      const {
        UpdateDivisions: { Endpoint, Method }
      } = ChatAdminRoutes
      const { name, departmentId, divisionId } = params
      await this.httpClient({
        url: Endpoint({ departmentId, divisionId }),
        method: Method,
        data: {
          name
        }
      })
      return true
    }
    return this.guardWithAuth(updateDivision)
  }

  /* Keyword APIs */

  public createKeyword = async ({
    term,
    priority
  }: {
    term: string
    priority: KeyWordPriorityTypes
  }): Promise<Result<{ id: string }, SayHey.Error>> => {
    const createKeyword = async () => {
      const {
        CreateKeyword: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          term,
          priority
        }
      })
      return data
    }
    return this.guardWithAuth(createKeyword)
  }

  public createKeywordBulk = async (
    list: { term: string; priority: KeyWordPriorityTypes }[]
  ): Promise<Result<{ id: string }[], SayHey.Error>> => {
    const createKeywordBulk = async () => {
      const {
        CreateKeywordBulk: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          list
        }
      })
      return data
    }
    return this.guardWithAuth(createKeywordBulk)
  }

  public deleteKeyword = async (id: string): Promise<Result<boolean, SayHey.Error>> => {
    const deleteKeyword = async () => {
      const {
        DeleteKeyword: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint(id),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(deleteKeyword)
  }

  public updateKeyword = async (params: {
    id: string
    priority: KeyWordPriorityTypes
  }): Promise<Result<boolean, SayHey.Error>> => {
    const updateKeyword = async () => {
      const {
        UpdateKeyword: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint(params.id),
        method: Method,
        data: {
          priority: params.priority
        }
      })
      return true
    }
    return this.guardWithAuth(updateKeyword)
  }

  public getKeywordsBatch = async ({
    offset,
    limit,
    search,
    priority
  }: {
    offset?: number
    limit?: number
    search?: string | null
    priority?: KeyWordPriorityTypes
  }): Promise<Result<KeywordBatchDTO, SayHey.Error>> => {
    const getKeywordsBatch = async () => {
      const {
        GetKeywordsBatch: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method,
        params: { offset, limit, search, priority }
      })
      return {
        ...(data as KeywordBatchDTO)
      }
    }

    return this.guardWithAuth(getKeywordsBatch)
  }

  public disableUser = async (params: {
    userId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const disableUser = async () => {
      const {
        DisableUser: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint(params.userId),
        method: Method
      })

      return true
    }
    return this.guardWithAuth(disableUser)
  }

  public enableUser = (params: { userId: string }): Promise<Result<boolean, SayHey.Error>> => {
    const enableUser = async () => {
      const {
        EnableUser: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint(params.userId),
        method: Method
      })

      return true
    }
    return this.guardWithAuth(enableUser)
  }

  public getMessagesBeforeSequence = async ({
    beforeSequence,
    limit = 20,
    conversationId
  }: {
    beforeSequence?: number
    limit?: number
    conversationId: string
  }): Promise<Result<PeekMessageDomainEntity[], SayHey.Error>> => {
    const getMessagesBeforeSequence = async () => {
      const {
        GetMessagesBeforeSequence: { Endpoint, Method }
      } = ChatAdminRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          beforeSequence,
          limit
        }
      })
      return MessageMapper.toPeekDomainEntities(data as PeekMessageDTO[])
    }
    return this.guardWithAuth(getMessagesBeforeSequence)
  }

  public getMessagesAfterSequence = async ({
    afterSequence,
    limit = 20,
    conversationId
  }: {
    afterSequence?: number
    limit?: number
    conversationId: string
  }): Promise<Result<PeekMessageDomainEntity[], SayHey.Error>> => {
    const getMessagesAfterSequence = async () => {
      const {
        GetMessagesAfterSequence: { Endpoint, Method }
      } = ChatAdminRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          afterSequence,
          limit
        }
      })
      return MessageMapper.toPeekDomainEntities(data as PeekMessageDTO[])
    }
    return this.guardWithAuth(getMessagesAfterSequence)
  }

  public getMessageContext = async ({
    peekBeforeLimit,
    peekAfterLimit,
    messageId
  }: {
    peekBeforeLimit?: number
    peekAfterLimit?: number
    messageId: string
  }): Promise<Result<PeekMessageDomainEntity[], SayHey.Error>> => {
    const getMessageContext = async () => {
      const {
        GetMessageContext: { Endpoint, Method }
      } = ChatAdminRoutes
      const { data } = await this.httpClient({
        url: Endpoint(messageId),
        method: Method,
        params: {
          peekBeforeLimit,
          peekAfterLimit
        }
      })
      return MessageMapper.toPeekDomainEntities(data as PeekMessageDTO[])
    }
    return this.guardWithAuth(getMessageContext)
  }

  /* Messages Audit */

  public getAuditMessagesBatch = async ({
    offset,
    limit,
    startDate,
    endDate,
    text,
    textSearchCondition,
    userId,
    hasAttachment,
    hasFlagged
  }: {
    offset?: number
    limit?: number
    startDate?: string
    endDate?: string
    text?: string[]
    textSearchCondition?: QueryCondition
    userId?: string
    hasAttachment?: boolean
    hasFlagged?: boolean
  }): Promise<Result<AuditMessageDomainEntityBatchDTO, SayHey.Error>> => {
    const getAuditMessagesBatch = async () => {
      const {
        GetAuditMessagesBatch: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        params: {
          offset,
          limit,
          startDate,
          endDate,
          text,
          textSearchCondition,
          userId,
          hasAttachment,
          hasFlagged
        }
      })
      return AuditMessageMapper.toAuditMessageDomainEntityBatchDTO(data as AuditMessageBatchDTO)
    }

    return this.guardWithAuth(getAuditMessagesBatch)
  }

  public downloadAuditMessages = async ({
    startDate,
    endDate,
    text,
    textSearchCondition,
    userId,
    hasAttachment,
    hasFlagged
  }: {
    startDate?: string
    endDate?: string
    text?: string[]
    textSearchCondition?: QueryCondition
    userId?: string
    hasAttachment?: boolean
    hasFlagged?: boolean
  }): Promise<Result<AuditMessageDomainEntityDTO[], SayHey.Error>> => {
    const downloadAuditMessages = async () => {
      const {
        DownloadAuditMessages: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        params: {
          startDate,
          endDate,
          text,
          textSearchCondition,
          userId,
          hasAttachment,
          hasFlagged
        }
      })
      return data.map(AuditMessageMapper.toAuditMessageDomainEntityDTO)
    }

    return this.guardWithAuth(downloadAuditMessages)
  }

  public getUserBasicInfoBatch = async ({
    offset,
    limit,
    search
  }: {
    offset?: number
    limit?: number
    search?: string | null
  }): Promise<Result<UserBasicInfoBatchDTO, SayHey.Error>> => {
    const getAuditMessagesBatch = async () => {
      const {
        GetUserBasicInfoBatch: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        params: {
          offset,
          limit,
          search
        }
      })
      return data as UserBasicInfoBatchDTO
    }

    return this.guardWithAuth(getAuditMessagesBatch)
  }

  /*User Admin Search */

  public createUpdateUserAdminSearch = async (
    userAdminSearchCriteria: UserSearchCriteria
  ): Promise<Result<boolean, SayHey.Error>> => {
    const createUpdateUserAdminSearch = async () => {
      const {
        CreateUpdateUserAdminSearch: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: userAdminSearchCriteria
      })
      return true
    }
    return this.guardWithAuth(createUpdateUserAdminSearch)
  }

  public getUserAdminSearch = async (): Promise<Result<UserAdminSearchDTO, SayHey.Error>> => {
    const getUserAdminSearch = async () => {
      const {
        GetUserAdminSearch: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return data as UserAdminSearchDTO
    }
    return this.guardWithAuth(getUserAdminSearch)
  }

  /* Message Flagging */

  public getFlaggedMessagesBatch = async ({
    offset,
    limit,
    search,
    flagType = 'all',
    status = 'all'
  }: {
    offset?: number
    limit?: number
    search?: string | null
    flagType?: 'high' | 'low' | 'user_reported' | 'all'
    status?: 'pending' | 'resolved' | 'all'
  }): Promise<Result<FlaggedMessageBatchDTO, SayHey.Error>> => {
    const getFlaggedMessagesBatch = async () => {
      const {
        GetFlaggedMessagesBatch: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method,
        params: { offset, limit, search, flagType, status }
      })

      const flaggedMsgsDTO = { ...(data as FlaggedMessageBatchDTO) }

      return flaggedMsgsDTO
    }

    return this.guardWithAuth(getFlaggedMessagesBatch)
  }

  public getFlaggedMessagesCount = async (): Promise<
    Result<FlaggedMessageCountDTO, SayHey.Error>
  > => {
    const getFlaggedMessagesCount = async () => {
      const {
        GetFlaggedMessagesCount: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method
      })
      return {
        ...(data as FlaggedMessageCountDTO)
      }
    }

    return this.guardWithAuth(getFlaggedMessagesCount)
  }

  public resolveFlaggedMessage = async ({
    resolutionStatus,
    resolutionNotes,
    messageDeletion,
    flaggedMessageId,
    notifySuperAdmin
  }: {
    resolutionStatus: ResolutionStatusType.NO_ACTION | ResolutionStatusType.ACTION_TAKEN
    resolutionNotes?: string
    messageDeletion?: boolean
    notifySuperAdmin?: boolean
    flaggedMessageId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const resolveFlaggedMessage = async () => {
      const {
        ResolveFlaggedMessages: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint(flaggedMessageId),
        method: Method,
        data: { resolutionStatus, resolutionNotes, messageDeletion, notifySuperAdmin }
      })

      return true
    }
    return this.guardWithAuth(resolveFlaggedMessage)
  }

  public getFlaggedMessageDetails = async ({
    flaggedMessageId
  }: {
    flaggedMessageId: string
  }): Promise<Result<FlaggedMessageDetailsDTO, SayHey.Error>> => {
    const getFlaggedMessageDetails = async () => {
      const {
        GetFlaggedMessageDetails: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint(flaggedMessageId),
        method: Method
      })

      return {
        ...(data as FlaggedMessageDetailsDTO)
      }
    }
    return this.guardWithAuth(getFlaggedMessageDetails)
  }

  public getFlaggedMessagesUserReportsBatch = async ({
    offset,
    limit,
    flaggedMessageId,
    status
  }: {
    offset?: number
    limit?: number
    flaggedMessageId: string
    status?: 'pending' | 'resolved'
  }): Promise<Result<FlaggedMessageUserReportBatchDTO, SayHey.Error>> => {
    const getFlaggedMessagesUserReportsBatch = async () => {
      const {
        GetFlaggedMessageUserReports: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint(flaggedMessageId),
        method: Method,
        params: { offset, limit, status }
      })

      const flaggedMsgsUserReportsDTO = { ...(data as FlaggedMessageUserReportBatchDTO) }

      return flaggedMsgsUserReportsDTO
    }

    return this.guardWithAuth(getFlaggedMessagesUserReportsBatch)
  }

  public getFlaggedMessagesKeywordScansBatch = async ({
    offset,
    limit,
    flaggedMessageId,
    status
  }: {
    offset?: number
    limit?: number
    flaggedMessageId: string
    status?: 'pending' | 'resolved'
  }): Promise<Result<FlaggedMessageScanBatchDTO, SayHey.Error>> => {
    const getFlaggedMessagesKeywordScansBatch = async () => {
      const {
        GetFlaggedMessageKeywordScans: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint(flaggedMessageId),
        method: Method,
        params: { offset, limit, status }
      })

      const flaggedMsgsKeywordScansDTO = { ...(data as FlaggedMessageScanBatchDTO) }

      return flaggedMsgsKeywordScansDTO
    }

    return this.guardWithAuth(getFlaggedMessagesKeywordScansBatch)
  }

  public getFlaggedMessagesResolutionsBatch = async ({
    offset,
    limit,
    flaggedMessageId
  }: {
    offset?: number
    limit?: number
    flaggedMessageId: string
  }): Promise<Result<FlaggedMessageResolutionBatchDTO, SayHey.Error>> => {
    const getFlaggedMessagesResolutionsBatch = async () => {
      const {
        GetFlaggedMessageResolutions: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint(flaggedMessageId),
        method: Method,
        params: { offset, limit }
      })

      const flaggedMsgsResolutionsDTO = { ...(data as FlaggedMessageResolutionBatchDTO) }

      return flaggedMsgsResolutionsDTO
    }

    return this.guardWithAuth(getFlaggedMessagesResolutionsBatch)
  }

  public getFlaggedMessagesResolutionUserReportsBatch = async ({
    offset,
    limit,
    resolutionId
  }: {
    offset?: number
    limit?: number
    resolutionId: string
  }): Promise<Result<FlaggedMessageResolutionUserReportBatchDTO, SayHey.Error>> => {
    const getFlaggedMessagesResolutionUserReportsBatch = async () => {
      const {
        GetFlaggedMessageResolutionUserReports: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint(resolutionId),
        method: Method,
        params: { offset, limit }
      })

      const flaggedMsgsResolutionUserReportsDTO = {
        ...(data as FlaggedMessageResolutionUserReportBatchDTO)
      }

      return flaggedMsgsResolutionUserReportsDTO
    }

    return this.guardWithAuth(getFlaggedMessagesResolutionUserReportsBatch)
  }

  public getFlaggedMessagesResolutionKeywordScansBatch = async ({
    offset,
    limit,
    resolutionId
  }: {
    offset?: number
    limit?: number
    resolutionId: string
  }): Promise<Result<FlaggedMessageResolutionScanBatchDTO, SayHey.Error>> => {
    const getFlaggedMessagesResolutionKeywordScansBatch = async () => {
      const {
        GetFlaggedMessageResolutionKeywordScans: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint(resolutionId),
        method: Method,
        params: { offset, limit }
      })

      const flaggedMsgsResolutionKeywordScansDTO = {
        ...(data as FlaggedMessageResolutionScanBatchDTO)
      }

      return flaggedMsgsResolutionKeywordScansDTO
    }

    return this.guardWithAuth(getFlaggedMessagesResolutionKeywordScansBatch)
  }

  public getReactedUsers = async ({
    messageId,
    type,
    limit,
    afterSequence
  }: {
    messageId: string
    type?: ReactionType
    limit: number
    afterSequence: number | null
  }): Promise<Result<ReactedUserDTO[], SayHey.Error>> => {
    const getReactedUsers = async () => {
      const {
        GetReactedUsers: { Endpoint, Method }
      } = ChatCommonRoutes
      const { data } = await this.httpClient({
        url: Endpoint(messageId),
        method: Method,
        params: {
          limit,
          afterSequence,
          type: type || ''
        }
      })
      return data
    }
    return this.guardWithAuth(getReactedUsers)
  }

  /* Conversation Space Group Divsions */

  public getSpaceGroupDivisions = async ({
    conversationId
  }: {
    conversationId: string
    offset?: number
    limit?: number
  }): Promise<Result<DepartmentDivisionDTO[], SayHey.Error>> => {
    const getSpaceGroupDivisions = async () => {
      const {
        GetSpaceGroupDivisions: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })

      return data
    }

    return this.guardWithAuth(getSpaceGroupDivisions)
  }

  public createConversationSpaceGroupDivision = async ({
    conversationId,
    divisionIds,
    memberIds
  }: {
    divisionIds?: string[]
    conversationId: string
    memberIds?: string[]
  }): Promise<Result<boolean, SayHey.Error>> => {
    const createConversationSpaceGroupDivision = async () => {
      const {
        CreateConversationSpaceGroupDivision: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          divisionIds,
          memberIds
        }
      })

      return true
    }
    return this.guardWithAuth(createConversationSpaceGroupDivision)
  }

  public deleteConversationSpaceGroupDivision = async ({
    conversationId,
    divisionIds
  }: {
    divisionIds: string[]
    conversationId: string
  }): Promise<Result<boolean, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        DeleteConversationSpaceGroupDivision: { Endpoint, Method }
      } = ChatAdminRoutes
      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          divisionIds
        }
      })
      return true
    })

  /* Groups */
  public createGroupConversation = async (params: {
    groupName: string
    file?: FileUpload
    userIds?: string[]
    divisionIds?: string[]
  }): Promise<Result<ConversationGroupDTOForUserAdminWithUserCount, SayHey.Error>> => {
    const createGroupConversation = async () => {
      const {
        CreateGroupConversation: { Endpoint, Method }
      } = ChatAdminRoutes
      const { file, groupName, userIds, divisionIds } = params
      if (file) {
        const fileIdResponse = await this.uploadFile(file.path, file.fileName)
        if (fileIdResponse.isOk()) {
          const { data } = await this.httpClient({
            url: Endpoint,
            method: Method,
            data: {
              groupName,
              groupImageId: fileIdResponse.value,
              memberIds: userIds,
              divisionIds
            }
          })
          return data as ConversationGroupDTOForUserAdminWithUserCount
        }
        return fileIdResponse.error
      }
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          groupName,
          groupImageId: null,
          memberIds: userIds,
          divisionIds
        }
      })
      return data as ConversationGroupDTOForUserAdminWithUserCount
    }
    return this.guardWithAuth(createGroupConversation)
  }

  public updateGroupOwner = async (params: {
    conversationId: string
    userAdminId: KeyWordPriorityTypes
  }): Promise<Result<boolean, SayHey.Error>> => {
    const updateGroupOwner = async () => {
      const { conversationId, userAdminId } = params
      const {
        UpdateGroupConversationOwner: { Endpoint, Method }
      } = ChatAdminRoutes

      await this.httpClient({
        url: Endpoint(conversationId, userAdminId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(updateGroupOwner)
  }

  public getGroupConversationBatch = async ({
    search,
    offset,
    limit
  }: {
    search?: string
    offset?: number
    limit?: number
  }): Promise<Result<ConversationGroupBatchDTOForUserAdminWithUserCount, SayHey.Error>> => {
    const getConversationGroupForUserAdminBatch = async () => {
      const {
        GetUserAdminGroups: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        params: { search, offset, limit }
      })

      const conversationGroupBatchDTOForUserAdminWithUserCount = {
        ...(data as ConversationGroupBatchDTOForUserAdminWithUserCount)
      }

      return conversationGroupBatchDTOForUserAdminWithUserCount
    }

    return this.guardWithAuth(getConversationGroupForUserAdminBatch)
  }

  public deleteGroupConversation = async (params: {
    conversationId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const deleteGroupConversation = async () => {
      const { conversationId } = params
      const {
        DeleteGroupConversation: { Endpoint, Method }
      } = ChatCommonRoutes

      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(deleteGroupConversation)
  }

  public getReachableUsers = async ({
    searchString,
    limit,
    nextCursor,
    includeRefIdInSearch
  }: ReachableUsersQuery = {}): Promise<
    Result<ConversableUsersForUserAdminBatchResult, SayHey.Error>
  > => {
    const getReachableUsers = async () => {
      const {
        GetReachableUsers: { Endpoint, Method }
      } = ChatCommonRoutes
      const url = Endpoint
      const params: any = {
        search: searchString || '',
        includeRefIdInSearch: includeRefIdInSearch || false
      }
      if (limit) {
        params.limit = limit
      }
      if (nextCursor) {
        params.offset = nextCursor
      }
      const { data } = await this.httpClient({
        url,
        method: Method,
        params
      })
      return {
        ...(data as ConversableUsersForUserAdminBatchDTO),
        results: UserMapper.toUserForUserDomainEntities(
          (data as ConversableUsersForUserAdminBatchDTO).results
        )
      }
    }
    return this.guardWithAuth(getReachableUsers)
  }

  public getConversationUsers = ({
    conversationId,
    searchString,
    limit,
    nextCursor,
    type,
    includeRefIdInSearch
  }: ConversationUserQuery): Promise<Result<ConversationUsersForUserAdminBatch, SayHey.Error>> => {
    const getConversationUsers = async () => {
      const {
        GetConversationUsers: { Endpoint, Method }
      } = ChatCommonRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          type: type || ConversationUserQueryType.Current,
          offset: nextCursor,
          limit,
          search: searchString,
          includeRefIdInSearch: includeRefIdInSearch || false
        }
      })
      return {
        ...(data as ConversationUsersBatchForUserAdminDTO),
        results: UserMapper.toConversableUserDomainForUserAdminEntities(
          (data as ConversationUsersBatchForUserAdminDTO).results
        )
      }
    }
    return this.guardWithAuth(getConversationUsers)
  }

  public addUsersToConversation = async (
    conversationId: string,
    userIds: string[],
    force?: boolean
  ): Promise<Result<boolean, SayHey.Error>> => {
    const addUsersToConversation = async () => {
      const {
        AddUsersToConversation: { Endpoint, Method }
      } = ChatCommonRoutes
      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          memberIds: userIds
        },
        params: {
          force
        }
      })
      return true
    }
    return this.guardWithAuth(addUsersToConversation)
  }

  public removeUsersFromConversation = async (
    conversationId: string,
    userIds: string[],
    force?: boolean
  ): Promise<Result<boolean, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        RemoveUsersFromConversation: { Endpoint, Method }
      } = ChatCommonRoutes
      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          memberIds: userIds
        },
        params: {
          force
        }
      })
      return true
    })

  public updateGroupImage = async (
    conversationId: string,
    file: FileUpload
  ): Promise<Result<boolean, SayHey.Error>> => {
    const updateGroupImage = async () => {
      const fileIdResponse = await this.uploadFile(file.path, file.fileName)
      if (fileIdResponse.isOk()) {
        const {
          UpdateGroupImage: { Endpoint, Method }
        } = ChatCommonRoutes

        await this.httpClient({
          url: Endpoint(conversationId),
          method: Method,
          data: {
            groupImageId: fileIdResponse.value
          }
        })
        return true
      }
      return false
    }
    return this.guardWithAuth(updateGroupImage)
  }

  public removeGroupImage = async (
    conversationId: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const removeGroupImage = async () => {
      const {
        RemoveGroupImage: { Endpoint, Method }
      } = ChatCommonRoutes

      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(removeGroupImage)
  }

  public getGroupDefaultImageSet = async (): Promise<
    Result<DefaultGroupImageUrlSet[], SayHey.Error>
  > =>
    this.guardWithAuth(async () => {
      const {
        GetGroupDefaultImageSet: { Endpoint, Method }
      } = ChatCommonRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return data as DefaultGroupImageUrlSet[]
    })

  public updateGroupInfo = async (
    conversationId: string,
    groupName: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const updateGroupInfo = async () => {
      const {
        UpdateGroupInfo: { Endpoint, Method }
      } = ChatCommonRoutes

      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          groupName
        }
      })
      return true
    }
    return this.guardWithAuth(updateGroupInfo)
  }

  public makeConversationOwner = async (
    conversationId: string,
    userId: string
  ): Promise<Result<boolean, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        MakeConversationOwner: { Endpoint, Method }
      } = ChatCommonRoutes

      await this.httpClient({
        url: Endpoint(conversationId, userId),
        method: Method
      })
      return true
    })

  public removeConversationOwner = async (
    conversationId: string,
    userId: string
  ): Promise<Result<boolean, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        RemoveConversationOwner: { Endpoint, Method }
      } = ChatCommonRoutes

      await this.httpClient({
        url: Endpoint(conversationId, userId),
        method: Method
      })
      return true
    })

  public getConversationDetails = async (
    conversationId: string
  ): Promise<Result<ConversationDTOForUserAdmin, SayHey.Error>> => {
    const getConversationDetails = async () => {
      const {
        GetConversationDetails: { Endpoint, Method }
      } = ChatCommonRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })

      return data as ConversationDTOForUserAdmin
    }
    return this.guardWithAuth(getConversationDetails)
  }

  public getTotalUsersCount = async (): Promise<Result<TotalUsersCountDTO, SayHey.Error>> => {
    const getTotalUsersCount = async () => {
      const {
        GetTotalUsersCount: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method
      })
      return {
        ...(data as TotalUsersCountDTO)
      }
    }

    return this.guardWithAuth(getTotalUsersCount)
  }

  public getMyUsersCount = async (): Promise<Result<{ myUsersCount: number }, SayHey.Error>> => {
    const getMyUsersCount = async () => {
      const {
        GetMyUsersCount: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method
      })
      return {
        ...(data as { myUsersCount: number })
      }
    }

    return this.guardWithAuth(getMyUsersCount)
  }

  public getResolvedFlaggedMessagesCount = async (
    timeFrame?: TimeFrameType
  ): Promise<Result<{ resolvedMessagesCount: number }, SayHey.Error>> => {
    const getResolvedFlaggedMessagesCount = async () => {
      const {
        GetResolvedFlaggedMessagesCount: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method,
        params: { timeFrame }
      })
      return {
        ...(data as { resolvedMessagesCount: number })
      }
    }

    return this.guardWithAuth(getResolvedFlaggedMessagesCount)
  }

  public getSentMessagesCountDaily = async (): Promise<
    Result<SentMessageCountDailyDTO[], SayHey.Error>
  > => {
    const getSentMessagesCountDaily = async () => {
      const {
        GetSentMessagesCountDaily: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method
      })
      return data as SentMessageCountDailyDTO[]
    }

    return this.guardWithAuth(getSentMessagesCountDaily)
  }

  public getSentMessagesCountTotal = async (
    timeFrame?: TimeFrameType
  ): Promise<Result<{ sentMessagesCount: number }, SayHey.Error>> => {
    const getSentMessagesCountTotal = async () => {
      const {
        GetSentMessagesCountTotal: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method,
        params: { timeFrame }
      })
      return {
        ...(data as { sentMessagesCount: number })
      }
    }

    return this.guardWithAuth(getSentMessagesCountTotal)
  }

  public getUnresolvedFlaggedMessagesCount = async (
    unresolvedMsgScope?: UnresolvedMsgScopeType
  ): Promise<Result<UnresolvedFlaggedMessagesCountDTO, SayHey.Error>> => {
    const getUnresolvedFlaggedMessagesCount = async () => {
      const {
        GetUnresolvedFlaggedMessagesCount: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method,
        params: { unresolvedMsgScope }
      })
      return {
        ...(data as UnresolvedFlaggedMessagesCountDTO)
      }
    }

    return this.guardWithAuth(getUnresolvedFlaggedMessagesCount)
  }

  public getUnresolvedFlaggedMessagesCountDaily = async (): Promise<
    Result<AllFlaggedMessageCountDailyDTO, SayHey.Error>
  > => {
    const getUnresolvedFlaggedMessagesCountDaily = async () => {
      const {
        GetUnresolvedFlaggedMessageCountsDaily: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method
      })
      return {
        ...(data as AllFlaggedMessageCountDailyDTO)
      }
    }

    return this.guardWithAuth(getUnresolvedFlaggedMessagesCountDaily)
  }

  public getEligibleRolesToInviteUserAdmin = async (): Promise<
    Result<UserRole[], SayHey.Error>
  > => {
    const getEligibleRolesToInviteUserAdmin = async () => {
      const {
        GetEligibleRolesToInviteAdmin: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method
      })
      return data
    }

    return this.guardWithAuth(getEligibleRolesToInviteUserAdmin)
  }

  /* User Onboarding Job  */

  public processUserOnboardingJobUpload = async (params: {
    fileUri: string | Blob
    fileName: string
    departmentId: string
    divisionId: string
  }): Promise<Result<{ message: string }, SayHey.Error>> => {
    const processUserOnboardingJobUpload = async () => {
      const {
        ProcessUserOnboardingJobUpload: { Endpoint, Method }
      } = ChatAdminRoutes

      const formData = new FormData()
      const { fileUri, fileName, departmentId, divisionId } = params
      formData.append('file', fileUri)

      if (fileUri instanceof Blob) {
        formData.append('file', fileUri, fileName)
      } else {
        const extension = fileName.split('.').pop()
        if (extension) {
          formData.append('file', {
            uri: fileUri,
            name: fileName,
            type: mimeTypes[extension.toLowerCase()]
          } as any)
        } else {
          return new SayHey.Error('Invalid file')
        }
      }

      formData.append('departmentId', departmentId)
      formData.append('divisionId', divisionId)

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: formData
      })
      return data as { message: string }
    }
    return this.guardWithAuth(processUserOnboardingJobUpload)
  }

  public getUserOnboardingJobBatch = async ({
    offset,
    limit,
    search,
    status
  }: {
    offset?: number
    limit?: number
    search?: string | null
    status?: UserOnboardingJobStatus[]
  }): Promise<Result<UserOnboardingJobBatchDTO, SayHey.Error>> => {
    const getUserOnboardingJobBatch = async () => {
      const {
        GetUserOnboardingJobBatch: { Endpoint, Method }
      } = ChatAdminRoutes
      const url = Endpoint

      const { data } = await this.httpClient({
        url,
        method: Method,
        params: { offset, limit, search, status }
      })
      return data
    }

    return this.guardWithAuth(getUserOnboardingJobBatch)
  }

  public downloadSampleFile = async (): Promise<Result<Blob, SayHey.Error>> => {
    const downloadSampleFile = async () => {
      const {
        DownloadSampleFile: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return data
    }

    return this.guardWithAuth(downloadSampleFile)
  }

  public getAllErroredUserOnboardingJobRecords = async ({
    userOnboardingJobId
  }: {
    userOnboardingJobId: string
  }): Promise<Result<Blob, SayHey.Error>> => {
    const getAllErroredUserOnboardingJobRecords = async () => {
      const {
        GetAllErroredUserOnboardingJobRecords: { Endpoint, Method }
      } = ChatAdminRoutes

      const { data } = await this.httpClient({
        url: Endpoint(userOnboardingJobId),
        method: Method
      })
      return data
    }

    return this.guardWithAuth(getAllErroredUserOnboardingJobRecords)
  }
}
