import PublisherKitClient from '@7t/publisher-kit-client'
import StaticAxios, { AxiosInstance } from 'axios'
import decode from 'jwt-decode'
import { err, ok, Result } from 'neverthrow'

import httpClient from './axios'
import {
  API_VERSION,
  ApiKeyName,
  ChatClientRoutes,
  ChatCommonRoutes,
  MESSAGE_READ_UPDATE_INTERVAL
} from './constants'
import { isUrl, mimeTypes } from './helper'
import {
  ConversationMapper,
  MediaMapper,
  MessageMapper,
  ReactionMapper,
  UserMapper
} from './mapper'
import {
  AppConfigDTO,
  AuthResponse,
  ConversableUsersBatchDTO,
  ConversableUsersBatchResult,
  ConversationDomainEntity,
  ConversationDTO,
  ConversationGroupType,
  ConversationReachableUsersQuery,
  ConversationReactionsDomainEntity,
  ConversationType,
  ConversationUserQuery,
  ConversationUserQueryType,
  ConversationUsersBasicDTO,
  ConversationUsersBatch,
  ConversationUsersBatchDTO,
  CreateNewContact,
  DefaultGroupImageUrlSet,
  ExternalUserContactDTO,
  ExternalUserOptToReceiveSmsDTO,
  ExternalUserTermsAndPrivacyVersionDTO,
  FileUpload,
  FindByPhoneQueryParams,
  FindSmsConversation,
  GalleryPayload,
  HttpMethod,
  ImageSizes,
  InternalUserContactsBatchDTO,
  MediaDomainEntity,
  MessageDomainEntity,
  MessageDTO,
  MessageType,
  PhoneWithCode,
  Publisher,
  ReachableUsersQuery,
  ReactedUserDTO,
  ReactionType,
  SayHey,
  SearchPaginateParams,
  SendMessageParams,
  SmsConversationInvitationDTO,
  SmsConversationInvitationUserDTO,
  UserSelfDomainEntity,
  UserSelfDTO,
  ValidateResetPasswordOTPDTO,
  ValidateSetPasswordOTPDTO
} from './types'

export class ChatClientKit {
  publisherKitClient: PublisherKitClient

  accessToken = ''

  publisherToken = ''

  refreshToken = ''

  instanceIdPushNotification = ''

  private _apiVersion: string = API_VERSION

  httpClient!: AxiosInstance

  appId = ''

  apiKey = ''

  serverUrl = ''

  authDetailChangeListener?: (res: AuthResponse) => Promise<void> | void

  eventListener?: (event: SayHey.Event) => void

  /** NOTE: Will be called when refresh token exchange session API call has failed */
  sessionExpiredListener?: () => void

  private _initialized = false

  private _subscribed = false

  private _isUserAuthenticated = false

  private readMessageMap: { [conversationId: string]: string } = {}

  // readMessageSequenceMap: { [conversationId: string]: number } = {}
  readMessageSequenceMap: Map<string, number | string> = new Map()

  private isSendingReadReceipt = false

  private timerHandle: any = null

  private exchangeTokenPromise: Promise<Result<boolean, SayHey.Error>> | null = null

  private disableAutoRefreshToken?: boolean

  private disableUserDisabledHandler?: boolean

  private disableUserDeletedHandler?: boolean

  private loginId = 0

  public constructor() {
    this.publisherKitClient = new PublisherKitClient()
  }

  public isUserAuthenticated = () => this._isUserAuthenticated

  /**
   * Setup methods
   */
  public initialize(params: {
    sayheyAppId: string
    sayheyApiKey: string
    publisherAppId: string
    publisherApiKey: string
    sayheyUrl: string
    publisherUrl: string
    apiVersion?: 'v1' | 'v2' | 'v3' | 'v4'
    messageUpdateInterval?: number
    disableAutoRefreshToken?: boolean
    disableUserDisabledHandler?: boolean
    disableUserDeletedHandler?: boolean
  }) {
    const {
      sayheyAppId,
      sayheyApiKey,
      publisherApiKey,
      publisherAppId,
      sayheyUrl,
      publisherUrl,
      messageUpdateInterval,
      disableAutoRefreshToken,
      disableUserDeletedHandler,
      disableUserDisabledHandler,
      apiVersion
    } = params
    this.serverUrl = sayheyUrl
    this._apiVersion = apiVersion || API_VERSION
    const baseURL = `${this.serverUrl}/${this._apiVersion}/apps/${sayheyAppId}`
    this.apiKey = sayheyApiKey
    this.appId = sayheyAppId
    this.disableAutoRefreshToken = disableAutoRefreshToken
    this.disableUserDisabledHandler = disableUserDisabledHandler
    this.disableUserDeletedHandler = disableUserDeletedHandler
    this.httpClient = httpClient.createWithoutAuth({
      apiKey: this.apiKey,
      baseURL
    })
    this.publisherKitClient.initialize({
      appId: publisherAppId,
      clientApiKey: publisherApiKey,
      publisherUrl
    })
    if (this.timerHandle !== null) {
      clearInterval(this.timerHandle)
    }
    this.timerHandle = setInterval(() => {
      this.isSendingReadReceipt = true
      const copy = new Map(Object.entries(this.readMessageMap))

      this.readMessageMap = {}
      this.isSendingReadReceipt = false

      const conversationIds = Object.keys(copy)

      for (let i = 0; i < conversationIds.length; i++) {
        const conversationId = conversationIds[+i]
        const messageId = copy.get(conversationId)
        messageId && this.markMessageRead(messageId)
      }
    }, messageUpdateInterval || MESSAGE_READ_UPDATE_INTERVAL)
    this._initialized = true
  }

  public addListeners = (listeners: {
    onAuthDetailChange: (res: AuthResponse) => Promise<void> | void
    onEvent: (event: SayHey.Event) => void
    onSocketConnect?: () => void
    onSocketDisconnect?: (reason: string) => void
    onSocketError?: (error: Error) => void
    onSocketReconnect?: (attemptNumber: number) => void
    onSessionExpired?: () => void
  }): Result<boolean, SayHey.Error> => {
    if (this._initialized) {
      const {
        onSocketConnect,
        onSocketDisconnect,
        onSocketError,
        onSocketReconnect,
        onAuthDetailChange,
        onEvent,
        onSessionExpired
      } = listeners
      this._subscribed = true
      this.authDetailChangeListener = onAuthDetailChange
      this.eventListener = onEvent
      this.sessionExpiredListener = onSessionExpired
      this.publisherKitClient.onMessage(this.onMessage as any)
      onSocketConnect && this.publisherKitClient.onConnect(onSocketConnect)
      onSocketDisconnect && this.publisherKitClient.onDisconnect(onSocketDisconnect)
      onSocketError && this.publisherKitClient.onError(onSocketError)
      onSocketReconnect && this.publisherKitClient.onReconnect(onSocketReconnect)
      return ok(true)
    }
    return err(
      new SayHey.Error(
        'ChatKit has not been initialized!',
        SayHey.ErrorType.InternalErrorType.Uninitialized
      )
    )
  }

  /**
   * Helper methods
   */

  /**
   *
   * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
   */
  private guard = async <T>(
    cb: (...args: any[]) => Promise<T | SayHey.Error>
  ): Promise<Result<T, SayHey.Error>> => {
    if (!this._initialized) {
      return err(
        new SayHey.Error(
          'ChatKit has not been initialized!',
          SayHey.ErrorType.InternalErrorType.Uninitialized
        )
      )
    }
    if (!this._subscribed) {
      return err(
        new SayHey.Error(
          'ChatKit events have not been subscribed to!',
          SayHey.ErrorType.InternalErrorType.Uninitialized
        )
      )
    }
    try {
      const res = await cb()
      if (res instanceof SayHey.Error) {
        return err(res)
      }
      return ok(res)
    } catch (e) {
      if (e instanceof SayHey.Error) {
        return err(e)
      }
      return err(new SayHey.Error('Unknown error', SayHey.ErrorType.InternalErrorType.Unknown))
    }
  }

  public exchangeTokenOnce = async (): Promise<Result<boolean, SayHey.Error>> => {
    if (this.exchangeTokenPromise) {
      return this.exchangeTokenPromise
    }
    this.exchangeTokenPromise = this.exchangeToken()
    const response = await this.exchangeTokenPromise
    this.exchangeTokenPromise = null
    return response
  }

  public isSocketConnected = (): boolean => this.publisherKitClient.isConnected()

  /**
   *
   * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
   */
  private guardWithAuth = async <T>(
    cb: (...args: any[]) => Promise<T | SayHey.Error>
  ): Promise<Result<T, SayHey.Error>> => {
    // Capturing current login id
    const currentLoginId = this.loginId
    const res = await this.guard(cb)
    if (res.isErr()) {
      if (
        !this.disableAutoRefreshToken &&
        res.error.type === SayHey.ErrorType.AuthErrorType.TokenExpiredType
      ) {
        // If token expires check if token has been exchanged since this call started
        if (currentLoginId === this.loginId) {
          const response = await this.exchangeTokenOnce()
          if (response.isOk() && response.value === true) {
            // --> NOTE: Modified to check response is also true => isUserAuthenticated, else logout
            return this.guard(cb)
          }
          await this.signOut()
        } else {
          // If token has been exchanged, use the new credentials to make the request again
          return this.guard(cb)
        }
      }
    }
    return res
  }

  /**
   *
   * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
   * @param publisherToken - publisher token returned from SayHey that will be used to verify Publisher access
   * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
   */
  private updateAuthDetails = async (
    accessToken: string,
    publisherToken: string,
    refreshToken: string
  ) => {
    this.accessToken = accessToken
    this.publisherToken = publisherToken
    this.refreshToken = refreshToken
    const id = accessToken ? decode<any>(accessToken).id : ''
    if (publisherToken && accessToken && refreshToken) {
      this.loginId += 1
      this.httpClient = httpClient.createWithAuth({
        apiKey: this.apiKey,
        baseURL: `${this.serverUrl}/${this._apiVersion}/apps/${this.appId}`,
        accessToken: this.accessToken
      })
      this.publisherKitClient.updateAccessParams({
        token: publisherToken,
        userId: id
      })
      if (!this.publisherKitClient.isConnected()) {
        this.publisherKitClient.connect()
      }
    }
    await this.authDetailChangeListener?.({
      userId: id,
      accessToken,
      publisherToken,
      refreshToken
    })

    // NOTE: Should be done at the end?
    this._isUserAuthenticated = !!(publisherToken && accessToken && refreshToken)
  }

  // NOTE: Expose these methods only when there is a strong need to do so
  // public addGuard = async <T>(
  //   cb: (...args: any[]) => Promise<T | SayHey.Error>
  // ): Promise<Result<T, SayHey.Error>> => {
  //   return this.guard(cb)
  // }
  // public addGuardWithAuth = async <T>(
  //   cb: (...args: any[]) => Promise<T | SayHey.Error>
  // ): Promise<Result<T, SayHey.Error>> => {
  //   return this.guardWithAuth(cb)
  // }

  /**
   *
   * @param urlPath - url path returned from SayHey
   */
  public getMediaSourceDetails = (urlPath: string) => ({
    uri: `${this.serverUrl}${urlPath}`,
    method: 'GET',
    headers: {
      [ApiKeyName]: this.apiKey,
      Authorization: `Bearer ${this.accessToken}`,
      Accept: 'image/*, video/*, audio/*'
    }
  })

  public getMediaDirectUrl = async (params: {
    urlPath: string
    size?: ImageSizes
  }): Promise<Result<string, SayHey.Error>> => {
    const getMediaDirectUrl = async () => {
      const queryString: any = {
        noRedirect: true
      }
      if (params.size) {
        queryString.size = params.size
      }
      const { data } = await this.httpClient({
        url: `${this.serverUrl}${params.urlPath}`,
        method: 'GET',
        params: queryString
      })
      return data.url as string
    }
    return this.guardWithAuth(getMediaDirectUrl)
  }

  private uploadFile = async (
    fileUri: string | Blob,
    fileName: string,
    progress?: (percentage: number) => void,
    thumbnailId?: string
  ): Promise<Result<string, SayHey.Error>> => {
    const uploadFile = async () => {
      const {
        UploadFile: { Endpoint, Method }
      } = ChatCommonRoutes
      const formData = new FormData()
      if (fileUri instanceof Blob) {
        formData.append('file', fileUri, fileName)
        if (thumbnailId) {
          formData.append('thumbnailId', thumbnailId)
        }
      } else {
        const extension = fileName.split('.').pop()
        if (extension) {
          formData.append('file', {
            uri: fileUri,
            name: fileName,
            type: mimeTypes[extension.toLowerCase()]
          } as any)
          if (thumbnailId) {
            formData.append('thumbnailId', thumbnailId)
          }
        } else {
          return new SayHey.Error('Invalid file')
        }
      }
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: formData,
        onUploadProgress: progress
          ? (event: ProgressEvent) => {
              const { loaded, total } = event
              progress((loaded / total) * 0.95)
            }
          : undefined
      })
      progress?.(1)
      return data.id
    }
    return this.guardWithAuth(uploadFile)
  }

  public uploadImage = async (
    fileUri: string | Blob,
    fileName: string
  ): Promise<Result<string, SayHey.Error>> => {
    const extension = fileName
      .split('.')
      .pop()
      ?.toLowerCase()
    const isSupportedImage = mimeTypes[extension || ''].split('/')[0] === 'image'
    if (isSupportedImage) {
      return this.uploadFile(fileUri, fileName)
    }
    return err(
      new SayHey.Error('Please select a valid image!', SayHey.ErrorType.InternalErrorType.BadData)
    )
  }

  public disconnectPublisher = async () => {
    this.publisherKitClient.disconnect()
  }

  public reconnectPublisher = async () => {
    this.publisherKitClient.connect()
  }

  /**
   * Token APIs
   */

  /**
   *
   * @param email - user email for SayHey
   * @param password - user password for SayHey
   *
   */
  public signIn = async (
    email: string,
    password: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const signInCallback = async () => {
      const {
        SignIn: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email,
          password
        }
      })
      const { accessToken, refreshToken, publisherToken } = data
      await this.updateAuthDetails(accessToken, publisherToken, refreshToken)
      // this._isUserAuthenticated = true
      return true
    }
    return this.guard(signInCallback)
  }

  public signInWithToken = async (
    accessToken: string,
    publisherToken: string,
    refreshToken: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const signInWithTokenCallback = async () => {
      await this.updateAuthDetails(accessToken, publisherToken, refreshToken)
      // this._isUserAuthenticated = true
      return true
    }
    return this.guard(signInWithTokenCallback)
  }

  public signUp = async (
    email: string,
    password: string,
    firstName: string,
    lastName: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const signUp = async () => {
      const {
        SignUp: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email,
          password,
          firstName,
          lastName
        }
      })
      const { accessToken, refreshToken, publisherToken } = data
      await this.updateAuthDetails(accessToken, publisherToken, refreshToken)
      // this._isUserAuthenticated = true
      return true
    }
    return this.guard(signUp)
  }

  public signOut = async () => {
    this.publisherKitClient.disconnect()
    this._isUserAuthenticated = false // NOTE: Is manually set in the beginning as well?
    this.loginId = 0
    const unregisteringPushNotification = async () => {
      const result = await this.unregisterForPushNotification()
      if (
        result.isOk() ||
        (result.isErr() && result.error.statusCode && result.error.statusCode < 500)
      ) {
        return
      }
      setTimeout(unregisteringPushNotification, 3000)
    }
    await unregisteringPushNotification()
    await this.updateAuthDetails('', '', '')
  }

  public exchangeToken = async (): Promise<Result<boolean, SayHey.Error>> => {
    const exchangeToken = async () => {
      try {
        const { data } = await StaticAxios({
          method: HttpMethod.Post,
          url: `${this.serverUrl}/${this._apiVersion}/tokens/exchange`,
          data: {
            refreshToken: this.refreshToken
          }
        })
        const { refreshToken, accessToken, publisherToken } = data
        if (refreshToken && accessToken) {
          await this.updateAuthDetails(accessToken, publisherToken, refreshToken)
          // this._isUserAuthenticated = true
        } else {
          await this.updateAuthDetails('', '', '')
          this.sessionExpiredListener?.()
          // this._isUserAuthenticated = false
        }
      } catch (e) {
        await this.updateAuthDetails('', '', '')
        this.sessionExpiredListener?.()
        // this._isUserAuthenticated = false
      }
      return this._isUserAuthenticated
    }
    return this.guard(exchangeToken) // --> NOTE: Looks like it will always be ok(true| false), never err()??
  }

  public changePassword = async (
    currentPassword: string,
    newPassword: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const changePassword = async () => {
      const {
        ChangePassword: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          currentPassword,
          newPassword
        }
      })
      return true
    }
    return this.guardWithAuth(changePassword)
  }

  public forgotPassword = async (email: string): Promise<Result<boolean, SayHey.Error>> => {
    const forgotPassword = async () => {
      const {
        ForgotPassword: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email
        }
      })
      return true
    }
    return this.guardWithAuth(forgotPassword)
  }

  public resetPassword = async ({
    password,
    resetPasswordToken
  }: {
    password: string
    resetPasswordToken: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const resetPassword = async () => {
      const {
        ResetPassword: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          password,
          resetPasswordToken
        }
      })
      return true
    }
    return this.guard(resetPassword)
  }

  /**
   * User APIs
   */

  public getReachableUsers = async ({
    searchString,
    limit,
    nextCursor,
    includeRefIdInSearch
  }: ReachableUsersQuery = {}): Promise<Result<ConversableUsersBatchResult, SayHey.Error>> => {
    const getReachableUsers = async () => {
      const {
        GetReachableUsers: { Endpoint, Method }
      } = ChatCommonRoutes
      const url = Endpoint
      const params: any = {
        search: searchString || '',
        includeRefIdInSearch: includeRefIdInSearch || false
      }
      if (limit) {
        params.limit = limit
      }
      if (nextCursor) {
        params.offset = nextCursor
      }
      const { data } = await this.httpClient({
        url,
        method: Method,
        params
      })
      return {
        ...(data as ConversableUsersBatchDTO),
        results: UserMapper.toDomainEntities((data as ConversableUsersBatchDTO).results)
      }
    }
    return this.guardWithAuth(getReachableUsers)
  }

  public updateUserProfileImage = async (
    fileUri: string,
    fileName: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const updateUserProfileImage = async () => {
      const fileIdResponse = await this.uploadFile(fileUri, fileName)
      if (fileIdResponse.isOk()) {
        const {
          UpdateUserProfileImage: { Endpoint, Method }
        } = ChatClientRoutes
        await this.httpClient({
          url: Endpoint,
          method: Method,
          data: {
            profileImageId: fileIdResponse.value
          }
        })
        return true
      }
      return false
    }
    return this.guardWithAuth(updateUserProfileImage)
  }

  public updateUserProfileInfo = async (options: {
    firstName?: string
    lastName?: string
    email?: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const updateUserProfileInfo = async () => {
      const {
        UpdateUserProfileInfo: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: options
      })
      return true
    }
    return this.guardWithAuth(updateUserProfileInfo)
  }

  public getUserDetails = async (): Promise<Result<UserSelfDomainEntity, SayHey.Error>> => {
    const getUserDetails = async () => {
      const {
        GetUserProfile: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return UserMapper.toSelfDomainEntity(data as UserSelfDTO)
    }
    return this.guardWithAuth(getUserDetails)
  }

  public muteAllNotifications = async (): Promise<Result<boolean, SayHey.Error>> => {
    const muteAllNotifications = async () => {
      const {
        MuteAllNotifications: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return true
    }
    return this.guardWithAuth(muteAllNotifications)
  }

  public unmuteAllNotifications = async (): Promise<Result<boolean, SayHey.Error>> => {
    const unmuteAllNotifications = async () => {
      const {
        UnmuteAllNotifications: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return true
    }
    return this.guardWithAuth(unmuteAllNotifications)
  }

  /**
   * Conversation APIs
   */

  public getExistingIndvidualOrQuickConversation = async (
    memberIds: string[]
  ): Promise<Result<ConversationDomainEntity | null, SayHey.Error>> => {
    const getExistingIndvidualOrQuickConversation = async () => {
      const {
        GetExistingIndvidualOrQuickConversation: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        params: {
          memberIds
        }
      })

      if (!data) {
        return null
      }
      return ConversationMapper.toDomainEntity(data)
    }
    return this.guardWithAuth(getExistingIndvidualOrQuickConversation)
  }

  public getConversations = async (params: {
    conversationType?: ConversationType
    conversationGroupType?: ConversationGroupType
  }): Promise<Result<ConversationDomainEntity[], SayHey.Error>> => {
    const { conversationType, conversationGroupType } = params
    const getConversations = async () => {
      const {
        GetConversations: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        params: {
          conversationType,
          conversationGroupType
        }
      })
      return ConversationMapper.toDomainEntities(data)
    }
    return this.guardWithAuth(getConversations)
  }

  public getConversationDetails = async (
    conversationId: string
  ): Promise<Result<ConversationDomainEntity, SayHey.Error>> => {
    const getConversationDetails = async () => {
      const {
        GetConversationDetails: { Endpoint, Method }
      } = ChatCommonRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })

      return ConversationMapper.toDomainEntity(data as ConversationDTO)
    }
    return this.guardWithAuth(getConversationDetails)
  }

  public checkForExistingConversation = async (
    withUserId: string
  ): Promise<Result<ConversationDomainEntity, SayHey.Error>> => {
    const checkForExistingConversation = async () => {
      const {
        GetExistingConversationWithUser: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint(withUserId),
        method: Method
      })
      return ConversationMapper.toDomainEntity(data as ConversationDTO)
    }
    return this.guardWithAuth(checkForExistingConversation)
  }

  public createIndividualConversation = async (
    withUserId: string
  ): Promise<Result<ConversationDomainEntity, SayHey.Error>> => {
    const createIndividualConversation = async () => {
      const {
        CreateIndividualConversation: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          memberId: withUserId
        }
      })
      return ConversationMapper.toDomainEntity(data as ConversationDTO)
    }
    return this.guardWithAuth(createIndividualConversation)
  }

  public createGroupConversation = async (params: {
    groupName: string
    file?: FileUpload
    userIds: string[]
  }): Promise<Result<ConversationDomainEntity, SayHey.Error>> => {
    const createGroupConversation = async () => {
      const {
        CreateGroupConversation: { Endpoint, Method }
      } = ChatClientRoutes
      const { file, groupName, userIds } = params
      if (file) {
        const fileIdResponse = await this.uploadFile(file.path, file.fileName)
        if (fileIdResponse.isOk()) {
          const { data } = await this.httpClient({
            url: Endpoint,
            method: Method,
            data: {
              groupName,
              groupImageId: fileIdResponse.value,
              memberIds: userIds
            }
          })
          return ConversationMapper.toDomainEntity(data as ConversationDTO)
        }
        return fileIdResponse.error
      }
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          groupName,
          groupImageId: null,
          memberIds: userIds
        }
      })
      return ConversationMapper.toDomainEntity(data as ConversationDTO)
    }
    return this.guardWithAuth(createGroupConversation)
  }

  public createQuickConversation = async (params: {
    groupName: string
    memberIds: string[]
  }): Promise<Result<ConversationDomainEntity, SayHey.Error>> => {
    const createQuickConversation = async () => {
      const {
        CreateQuickConversation: { Endpoint, Method }
      } = ChatClientRoutes
      const { groupName, memberIds } = params
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          groupName,
          memberIds
        }
      })

      return ConversationMapper.toDomainEntity(data as ConversationDTO)
    }
    return this.guardWithAuth(createQuickConversation)
  }

  public muteConversation = async (
    conversationId: string
  ): Promise<Result<string, SayHey.Error>> => {
    const muteConversation = async () => {
      const {
        MuteConversation: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return data.muteUntil as string
    }
    return this.guardWithAuth(muteConversation)
  }

  public unmuteConversation = async (
    conversationId: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const unmuteConversation = async () => {
      const {
        UnmuteConversation: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(unmuteConversation)
  }

  public pinConversation = async (
    conversationId: string
  ): Promise<Result<string, SayHey.Error>> => {
    const pinConversation = async () => {
      const {
        PinConversation: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return data.pinnedAt as string
    }
    return this.guardWithAuth(pinConversation)
  }

  public unpinConversation = async (
    conversationId: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const unpinConversation = async () => {
      const {
        UnpinConversation: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(unpinConversation)
  }

  public deleteConversation = async (
    conversationId: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const deleteConversation = async () => {
      const {
        DeleteConversation: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(deleteConversation)
  }

  public deleteAllConversations = async (): Promise<Result<boolean, SayHey.Error>> => {
    const deleteAllConversations = async () => {
      const {
        DeleteAllConversations: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return true
    }
    return this.guardWithAuth(deleteAllConversations)
  }

  public updateGroupImage = async (
    conversationId: string,
    file: FileUpload
  ): Promise<Result<boolean, SayHey.Error>> => {
    const updateGroupImage = async () => {
      const fileIdResponse = await this.uploadFile(file.path, file.fileName)
      if (fileIdResponse.isOk()) {
        const {
          UpdateGroupImage: { Endpoint, Method }
        } = ChatCommonRoutes

        await this.httpClient({
          url: Endpoint(conversationId),
          method: Method,
          data: {
            groupImageId: fileIdResponse.value
          }
        })
        return true
      }
      return false
    }
    return this.guardWithAuth(updateGroupImage)
  }

  public updateGroupInfo = async (
    conversationId: string,
    groupName: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const updateGroupInfo = async () => {
      const {
        UpdateGroupInfo: { Endpoint, Method }
      } = ChatCommonRoutes

      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          groupName
        }
      })
      return true
    }
    return this.guardWithAuth(updateGroupInfo)
  }

  public getConversationUsers = ({
    conversationId,
    searchString,
    limit,
    nextCursor,
    type,
    includeRefIdInSearch
  }: ConversationUserQuery): Promise<Result<ConversationUsersBatch, SayHey.Error>> => {
    const getConversationUsers = async () => {
      const {
        GetConversationUsers: { Endpoint, Method }
      } = ChatCommonRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          type: type || ConversationUserQueryType.Current,
          offset: nextCursor,
          limit,
          search: searchString,
          includeRefIdInSearch: includeRefIdInSearch || false
        }
      })
      return {
        ...(data as ConversationUsersBatchDTO),
        results: UserMapper.toConversableUserDomainEntities(
          (data as ConversationUsersBatchDTO).results
        )
      }
    }
    return this.guardWithAuth(getConversationUsers)
  }

  public getConversationReachableUsers = async ({
    conversationId,
    searchString,
    limit,
    nextCursor,
    includeRefIdInSearch
  }: ConversationReachableUsersQuery): Promise<
    Result<ConversableUsersBatchResult, SayHey.Error>
  > => {
    const getConversationReachableUsers = async () => {
      const {
        GetConversationReachableUsers: { Endpoint, Method }
      } = ChatClientRoutes
      const url = Endpoint(conversationId)
      const { data } = await this.httpClient({
        url,
        method: Method,
        params: {
          limit,
          search: searchString,
          offset: nextCursor,
          includeRefIdInSearch: includeRefIdInSearch || false
        }
      })
      return {
        ...(data as ConversableUsersBatchDTO),
        results: UserMapper.toDomainEntities((data as ConversableUsersBatchDTO).results)
      }
    }
    return this.guardWithAuth(getConversationReachableUsers)
  }

  public getConversationBasicUsers = async (params: {
    conversationId: string
    type?: 'all' | 'current' | 'default'
  }): Promise<Result<ConversationUsersBasicDTO[], SayHey.Error>> => {
    const { conversationId, type = 'current' } = params
    return this.guardWithAuth(async () => {
      const {
        GetConversationBasicUsers: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          type
        }
      })
      return data as ConversationUsersBasicDTO[]
    })
  }

  public addUsersToConversation = async (
    conversationId: string,
    userIds: string[],
    force?: boolean
  ): Promise<Result<boolean, SayHey.Error>> => {
    const addUsersToConversation = async () => {
      const {
        AddUsersToConversation: { Endpoint, Method }
      } = ChatCommonRoutes
      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          memberIds: userIds
        },
        params: {
          force
        }
      })
      return true
    }
    return this.guardWithAuth(addUsersToConversation)
  }

  public removeUsersFromConversation = async (
    conversationId: string,
    userIds: string[],
    force?: boolean
  ): Promise<Result<boolean, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        RemoveUsersFromConversation: { Endpoint, Method }
      } = ChatCommonRoutes
      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          memberIds: userIds
        },
        params: {
          force
        }
      })
      return true
    })

  public makeConversationOwner = async (
    conversationId: string,
    userId: string
  ): Promise<Result<boolean, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        MakeConversationOwner: { Endpoint, Method }
      } = ChatCommonRoutes

      await this.httpClient({
        url: Endpoint(conversationId, userId),
        method: Method
      })
      return true
    })

  public removeConversationOwner = async (
    conversationId: string,
    userId: string
  ): Promise<Result<boolean, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        RemoveConversationOwner: { Endpoint, Method }
      } = ChatCommonRoutes

      await this.httpClient({
        url: Endpoint(conversationId, userId),
        method: Method
      })
      return true
    })

  public removeGroupImage = async (
    conversationId: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const removeGroupImage = async () => {
      const {
        RemoveGroupImage: { Endpoint, Method }
      } = ChatCommonRoutes

      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(removeGroupImage)
  }

  public deleteGroupConversation = async (params: {
    conversationId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const deleteGroupConversation = async () => {
      const { conversationId } = params
      const {
        DeleteGroupConversation: { Endpoint, Method }
      } = ChatCommonRoutes

      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(deleteGroupConversation)
  }

  /**
   * @deprecated The method should not be used
   */
  public getUserConversationReactions = async (
    conversationId: string
  ): Promise<Result<ConversationReactionsDomainEntity, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        GetUserConversationReactions: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return ReactionMapper.toDomianEntity(data)
    })

  public getUserConversationMessagesReactions = async (
    conversationId: string,
    messageIds: string[]
  ): Promise<Result<ConversationReactionsDomainEntity, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        GetUserConversationMessagesReactions: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          messageIds
        }
      })
      return ReactionMapper.toDomianEntity(data)
    })

  public getGroupDefaultImageSet = async (): Promise<
    Result<DefaultGroupImageUrlSet[], SayHey.Error>
  > =>
    this.guardWithAuth(async () => {
      const {
        GetGroupDefaultImageSet: { Endpoint, Method }
      } = ChatCommonRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return data as DefaultGroupImageUrlSet[]
    })

  /**
   * Message APIs
   */

  public getMessages = async (
    conversationId: string,
    pivotSequence?: number,
    after?: boolean,
    limit = 20
  ): Promise<Result<MessageDomainEntity[], SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        GetMessages: { Endpoint, Method }
      } = ChatClientRoutes
      if (!after) {
        const { data } = await this.httpClient({
          url: Endpoint(conversationId),
          method: Method,
          params: {
            beforeSequence: pivotSequence,
            limit
          }
        })
        return MessageMapper.toDomainEntities(data as MessageDTO[])
      }
      if (!pivotSequence) {
        return []
      }

      let lastMessageSequenceInBatch = Number.MAX_SAFE_INTEGER
      const messageListIds: Set<string> = new Set()
      const messageList: MessageDTO[] = []
      while (lastMessageSequenceInBatch > pivotSequence + 1) {
        const { data } = await this.httpClient({
          url: Endpoint(conversationId),
          method: Method,
          params: {
            limit,
            beforeSequence: lastMessageSequenceInBatch
          }
        })
        const incomingMessageList: MessageDTO[] = data
        if (incomingMessageList.length === 0) {
          break
        }

        for (const m of incomingMessageList) {
          lastMessageSequenceInBatch = m.sequence
          if (lastMessageSequenceInBatch <= pivotSequence) {
            break
          }

          if (!messageListIds.has(m.id)) {
            messageListIds.add(m.id)
            messageList.push(m)
          }
        }
      }

      return MessageMapper.toDomainEntities(messageList)
    })

  private onMessage = (data: Publisher.Event) => {
    switch (data.event) {
      case Publisher.EventType.NewMessage: {
        this.eventListener?.({
          event: data.event,
          payload: {
            message: MessageMapper.toDomainEntity(data.payload.message),
            clientRefId: data.payload.clientRefId
          }
        })
        break
      }

      case Publisher.EventType.ConversationMuted: {
        const {
          conversationId,
          conversationMuteDto: { muteUntil }
        } = data.payload
        this.eventListener?.({
          event: data.event,
          payload: {
            conversationId,
            muteUntil: new Date(muteUntil)
          }
        })
        break
      }

      case Publisher.EventType.ConversationPinned: {
        const {
          conversationId,
          conversationPinDto: { pinnedAt }
        } = data.payload
        this.eventListener?.({
          event: data.event,
          payload: {
            conversationId,
            pinnedAt: new Date(pinnedAt)
          }
        })
        break
      }

      case Publisher.EventType.ConversationHideAndClear: {
        const {
          conversationId,
          conversationUserHideClearDto: { startAfter, visible }
        } = data.payload
        this.eventListener?.({
          event: data.event,
          payload: {
            conversationId,
            visible,
            startAfter: new Date(startAfter)
          }
        })
        break
      }

      case Publisher.EventType.ConversationHideAndClearAll: {
        const {
          conversationUserHideClearDto: { startAfter, visible }
        } = data.payload
        this.eventListener?.({
          event: data.event,
          payload: {
            visible,
            startAfter: new Date(startAfter)
          }
        })
        break
      }

      case Publisher.EventType.ConversationGroupInfoChanged: {
        this.eventListener?.({
          event: data.event,
          payload: {
            ...data.payload,
            groupPicture: data.payload.groupPicture
              ? MediaMapper.toDomainEntity(data.payload.groupPicture)
              : null
          }
        })
        break
      }

      case Publisher.EventType.ConversationQuickGroupChangedToGroup: {
        this.eventListener?.({
          event: data.event,
          payload: {
            ...data.payload
          }
        })
        break
      }

      case Publisher.EventType.ExternalUserConversationInvitationAccepted: {
        this.eventListener?.({
          event: data.event,
          payload: {
            ...data.payload
          }
        })
        break
      }

      case Publisher.EventType.ExternalUserNudged: {
        this.eventListener?.({
          event: data.event,
          payload: {
            ...data.payload
          }
        })
        break
      }

      case Publisher.EventType.ConversationCreated:
      case Publisher.EventType.ConversationUnpinned:
      case Publisher.EventType.ConversationUnmuted:
      case Publisher.EventType.UserInfoChanged:
      case Publisher.EventType.ConversationGroupOwnerAdded:
      case Publisher.EventType.ConversationGroupOwnerRemoved:
      case Publisher.EventType.ConversationGroupMemberAdded:
      case Publisher.EventType.ConversationGroupMemberRemoved:
      case Publisher.EventType.UserReactedToMessage:
      case Publisher.EventType.UserUnReactedToMessage:
      case Publisher.EventType.MessageReactionsUpdate:
      case Publisher.EventType.MessageDeleted:
      case Publisher.EventType.UserEnabled: {
        this.eventListener?.(data)
        break
      }
      case Publisher.EventType.UserDisabled: {
        this.eventListener?.(data)
        if (!this.disableUserDisabledHandler) {
          this.signOut()
        }
        break
      }
      case Publisher.EventType.UserDeleted: {
        this.eventListener?.(data)
        if (!this.disableUserDeletedHandler) {
          this.signOut()
        }
        break
      }
      default:
        break
    }
  }

  public sendMessage = async (
    params: SendMessageParams
  ): Promise<Result<MessageDomainEntity, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const { conversationId, clientRefId } = params
      const {
        SendMessage: { Endpoint, Method }
      } = ChatClientRoutes
      let payload: any
      switch (params.type) {
        case MessageType.Text:
        case MessageType.Emoji: {
          payload = {
            type: params.type,
            text: params.text.trim()
          }
          break
        }
        case MessageType.Gif: {
          if (!isUrl(params.url)) {
            return new SayHey.Error(
              'Invalid URL!',
              SayHey.ErrorType.InternalErrorType.MissingParameter
            )
          }
          payload = {
            type: MessageType.Gif,
            refUrl: params.url,
            text: params.text?.trim()
          }
          break
        }
        case MessageType.Audio:
        case MessageType.Image:
        case MessageType.Document: {
          const { file, progress } = params
          const subProgress = progress
            ? (percentage: number) => {
                progress(percentage * 0.9)
              }
            : undefined
          const fileIdResponse = await this.uploadFile(file.uri, file.fileName, subProgress)
          if (fileIdResponse.isErr()) {
            return fileIdResponse.error
          }
          payload = {
            type: file.type,
            text: file.text?.trim(),
            fileId: fileIdResponse.value
          }
          break
        }
        case MessageType.Video: {
          const { file, progress } = params
          const subProgress = progress
            ? (percentage: number) => {
                progress(percentage * 0.4)
              }
            : undefined
          const { uri, fileName, thumbnail } = file
          let thumbnailId = ''
          if (thumbnail) {
            const thumbnailIdResponse = await this.uploadFile(
              thumbnail.uri,
              thumbnail.name,
              subProgress
            )
            if (thumbnailIdResponse.isErr()) {
              return thumbnailIdResponse.error
            }
            thumbnailId = thumbnailIdResponse.value
          }
          const videoUploadProgress = progress
            ? (percentage: number) => {
                progress(percentage * 0.6 + 0.4)
              }
            : undefined
          const fileIdResponse = await this.uploadFile(
            uri,
            fileName,
            videoUploadProgress,
            thumbnailId
          )
          if (fileIdResponse.isErr()) {
            return fileIdResponse.error
          }
          payload = {
            type: file.type,
            text: file.text?.trim(),
            fileId: fileIdResponse.value
          }
          break
        }
        default:
          break
      }
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: payload,
        params: {
          clientRefId
        }
      })
      return MessageMapper.toDomainEntity(data as MessageDTO)
    })

  public getGalleryMessages = async (
    conversationId: string,
    beforeSequence?: number,
    limit?: number
  ): Promise<Result<GalleryPayload, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        GetMessages: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          galleryOnly: true,
          limit,
          beforeSequence
        }
      })
      const messages = MessageMapper.toDomainEntities(data as MessageDTO[])
      const mediaList = messages.map(message => message.file) as MediaDomainEntity[]
      return {
        nextCursor: mediaList.length ? messages[messages.length - 1].sequence : -1,
        messageList: messages
      }
    })

  public setMessageReaction = async (
    messageId: string,
    type: ReactionType
  ): Promise<Result<boolean, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        SetMessageReaction: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint(messageId),
        method: Method,
        data: {
          type
        }
      })
      return true
    })

  public removeMessageReaction = async (
    messageId: string,
    type: ReactionType
  ): Promise<Result<boolean, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        RemoveMessageReaction: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint(messageId),
        method: Method,
        data: {
          type
        }
      })
      return true
    })

  public readMessage = (params: {
    conversationId: string
    messageId: string
    sequence: number
    immediateUpdate?: boolean
  }) => {
    const { conversationId, messageId, sequence, immediateUpdate } = params
    const mark = () => {
      const readMessageSequenceMapForConvId = this.readMessageSequenceMap.get(conversationId)
      if (
        readMessageSequenceMapForConvId &&
        Number.isInteger(readMessageSequenceMapForConvId) &&
        readMessageSequenceMapForConvId >= sequence
      ) {
        return
      }
      if (immediateUpdate) {
        this.markMessageRead(messageId).then(res => {
          if (res.isErr()) {
            this.readMessageSequenceMap.set(conversationId, messageId)
          }
        })
      } else {
        this.readMessageSequenceMap.set(conversationId, messageId)
      }
      this.readMessageSequenceMap.set(conversationId, sequence)
    }
    if (!this.isSendingReadReceipt) {
      mark()
    } else {
      setTimeout(mark, 500)
    }
  }

  private markMessageRead = async (messageId: string): Promise<Result<boolean, SayHey.Error>> =>
    this.guardWithAuth(async () => {
      const {
        MarkMessageAsRead: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint(messageId),
        method: Method
      })
      return true
    })

  /** Push Notification APIs */
  public registerForPushNotification = async (
    instanceId: string,
    token: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    if (!instanceId || !token) {
      return err(new SayHey.Error('Invalid instance id or token string!'))
    }
    const registerForPushNotification = async () => {
      const {
        RegisterPushNotification: { Endpoint, Method }
      } = ChatClientRoutes
      this.instanceIdPushNotification = instanceId
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          instanceId,
          token
        }
      })
      return true
    }
    return this.guardWithAuth(registerForPushNotification)
  }

  public unregisterForPushNotification = async (): Promise<Result<boolean, SayHey.Error>> => {
    if (!this.instanceIdPushNotification) {
      return ok(true)
    }
    const unregisterForPushNotification = async () => {
      const {
        UnregisterPushNotification: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint(this.instanceIdPushNotification),
        method: Method
      })
      this.instanceIdPushNotification = ''
      return true
    }
    return this.guard(unregisterForPushNotification)
  }

  public getReactedUsers = async ({
    messageId,
    type,
    limit,
    afterSequence
  }: {
    messageId: string
    type?: ReactionType
    limit: number
    afterSequence: number | null
  }): Promise<Result<ReactedUserDTO[], SayHey.Error>> => {
    const getReactedUsers = async () => {
      const {
        GetReactedUsers: { Endpoint, Method }
      } = ChatCommonRoutes
      const { data } = await this.httpClient({
        url: Endpoint(messageId),
        method: Method,
        params: {
          limit,
          afterSequence,
          type: type || ''
        }
      })
      return data
    }
    return this.guardWithAuth(getReactedUsers)
  }

  public getAppConfig = async (): Promise<Result<AppConfigDTO, SayHey.Error>> => {
    const getAppConfig = async () => {
      const {
        GetAppConfig: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return data
    }
    return this.guardWithAuth(getAppConfig)
  }

  /* User Onboarding APIs */

  public checkEmail = async (email: string): Promise<Result<boolean, SayHey.Error>> => {
    const checkEmail = async () => {
      const {
        CheckEmail: { Endpoint, Method }
      } = ChatClientRoutes

      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email
        }
      })
      return true
    }
    return this.guard(checkEmail)
  }

  public setPassword = async ({
    setPasswordToken,
    password
  }: {
    setPasswordToken: string
    password: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const setPassword = async () => {
      const {
        SetPassword: { Endpoint, Method }
      } = ChatClientRoutes

      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          setPasswordToken,
          password
        }
      })

      return true
    }

    return this.guard(setPassword)
  }

  /* Request OTP */

  public requestSignUpOTP = async (email: string): Promise<Result<boolean, SayHey.Error>> => {
    const requestSignUpOTP = async () => {
      const {
        RequestSignUpOTP: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email
        }
      })
      return true
    }
    return this.guard(requestSignUpOTP)
  }

  public requestSetPasswordOTP = async (email: string): Promise<Result<boolean, SayHey.Error>> => {
    const requestSetPasswordOTP = async () => {
      const {
        RequestSetPasswordOTP: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email
        }
      })
      return true
    }
    return this.guard(requestSetPasswordOTP)
  }

  public requestResetPasswordOTP = async (
    email: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const requestResetPasswordOTP = async () => {
      const {
        RequestResetPasswordOTP: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email
        }
      })
      return true
    }
    return this.guard(requestResetPasswordOTP)
  }

  public requestPreverificationOTP = async (
    email: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const requestPreverificationOTP = async () => {
      const {
        RequestPreverificationOTP: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email
        }
      })
      return true
    }
    return this.guard(requestPreverificationOTP)
  }

  /* Validate OTP */

  public validateSignUpOTP = async ({
    otp,
    email
  }: {
    otp: string
    email: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const validateSignUpOTP = async () => {
      const {
        ValidateSignUpOTP: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          otp,
          email
        }
      })
      return true
    }
    return this.guard(validateSignUpOTP)
  }

  public validateSetPasswordOTP = async ({
    otp,
    email
  }: {
    otp: string
    email: string
  }): Promise<Result<ValidateSetPasswordOTPDTO, SayHey.Error>> => {
    const validateSetPasswordOTP = async () => {
      const {
        ValidateSetPasswordOTP: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          otp,
          email
        }
      })
      return data
    }
    return this.guard(validateSetPasswordOTP)
  }

  public validateResetPasswordOTP = async ({
    otp,
    email
  }: {
    otp: string
    email: string
  }): Promise<Result<ValidateResetPasswordOTPDTO, SayHey.Error>> => {
    const validateResetPasswordOTP = async () => {
      const {
        ValidateResetPasswordOTP: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          otp,
          email
        }
      })
      return data
    }
    return this.guard(validateResetPasswordOTP)
  }

  public validatePreverificationOTP = async ({
    otp,
    email
  }: {
    otp: string
    email: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const validatePreverificationOTP = async () => {
      const {
        ValidatePreverificationOTP: { Endpoint, Method }
      } = ChatClientRoutes
      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          otp,
          email
        }
      })
      return true
    }
    return this.guard(validatePreverificationOTP)
  }

  public clientSignInWithToken = async (
    sayheyLoginToken: string
  ): Promise<Result<boolean, SayHey.Error>> => {
    const clientSignInWithToken = async () => {
      const {
        ClientSignInWithToken: { Endpoint, Method }
      } = ChatClientRoutes
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          sayheyLoginToken
        }
      })
      const { accessToken, refreshToken, publisherToken } = data
      await this.updateAuthDetails(accessToken, publisherToken, refreshToken)
      return true
    }
    return this.guard(clientSignInWithToken)
  }

  /* Message Flagging */

  public reportFlaggedMessage = async ({
    messageId,
    reason
  }: {
    messageId: string
    reason: string
  }): Promise<Result<{ id: string }, SayHey.Error>> => {
    const reportFlaggedMessage = async () => {
      const {
        CreateFlaggedMessage: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          messageId,
          reason
        }
      })
      return data
    }
    return this.guardWithAuth(reportFlaggedMessage)
  }

  /* SMS */

  public addToContact = async ({
    externalUserId
  }: {
    externalUserId: string
  }): Promise<Result<{ internalUserId: string; externalUserId: string }, SayHey.Error>> => {
    const addToContact = async () => {
      const {
        AddToContact: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          externalUserId
        }
      })
      return data
    }
    return this.guardWithAuth(addToContact)
  }

  public getInternalUserContactsBatch = async (
    params: SearchPaginateParams
  ): Promise<Result<InternalUserContactsBatchDTO, SayHey.Error>> => {
    const getInternalUserContactsBatch = async () => {
      const {
        GetInternalUserContactsBatch: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        params
      })
      return data
    }
    return this.guardWithAuth(getInternalUserContactsBatch)
  }

  public findExternalUserByPhone = async (
    params: FindByPhoneQueryParams
  ): Promise<Result<ExternalUserContactDTO, SayHey.Error>> => {
    const findExternalUserByPhone = async () => {
      const {
        FindExternalUserByPhone: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        params
      })
      return data
    }
    return this.guardWithAuth(findExternalUserByPhone)
  }

  public requestSignInExternalOTP = async ({
    phone
  }: {
    phone: PhoneWithCode
  }): Promise<Result<boolean, SayHey.Error>> => {
    const requestSignInExternalOTP = async () => {
      const {
        RequestSignInExternalOTP: { Endpoint, Method }
      } = ChatClientRoutes

      await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          phone
        }
      })
      return true
    }
    return this.guardWithAuth(requestSignInExternalOTP)
  }

  public signInExternal = async ({
    otp,
    phone
  }: {
    otp: string
    phone: PhoneWithCode
  }): Promise<Result<boolean, SayHey.Error>> => {
    const signInExternal = async () => {
      const {
        SignInExternal: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          otp,
          phone
        }
      })

      const { accessToken, refreshToken, publisherToken } = data
      await this.updateAuthDetails(accessToken, publisherToken, refreshToken)

      return true
    }
    return this.guardWithAuth(signInExternal)
  }

  public signInExternalByUserId = async ({
    id,
    otp
  }: {
    id: string
    otp: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const signInExternalByUserId = async () => {
      const {
        SignInExternalByUserId: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint(id),
        method: Method,
        data: {
          otp
        }
      })
      const { accessToken, refreshToken, publisherToken } = data
      await this.updateAuthDetails(accessToken, publisherToken, refreshToken)

      return true
    }
    return this.guardWithAuth(signInExternalByUserId)
  }

  public createNewContact = async (
    params: CreateNewContact
  ): Promise<Result<{ id: string }, SayHey.Error>> => {
    const createNewContact = async () => {
      const {
        CreateNewContact: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: params
      })
      return data
    }
    return this.guardWithAuth(createNewContact)
  }

  public acceptTermsAndPrivacy = async (): Promise<Result<boolean, SayHey.Error>> => {
    const acceptTermsAndPrivacy = async () => {
      const {
        AcceptTermsAndPrivacy: { Endpoint, Method }
      } = ChatClientRoutes

      await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return true
    }
    return this.guardWithAuth(acceptTermsAndPrivacy)
  }

  public getTermsAndPrivacy = async (): Promise<
    Result<ExternalUserTermsAndPrivacyVersionDTO, SayHey.Error>
  > => {
    const getTermsAndPrivacy = async () => {
      const {
        GetTermsAndPrivacy: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return data
    }
    return this.guardWithAuth(getTermsAndPrivacy)
  }

  public getExistingSmsConversation = async (
    params: FindSmsConversation
  ): Promise<Result<ConversationDomainEntity | null, SayHey.Error>> => {
    const { externalUserId } = params
    const getExistingSmsConversation = async () => {
      const {
        GetExistingSmsConversation: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        params: {
          externalUserId
        }
      })
      if (!data) {
        return null
      }
      return ConversationMapper.toDomainEntity(data)
    }
    return this.guardWithAuth(getExistingSmsConversation)
  }

  public checkSmsConversationInvitation = async (params: {
    conversationId: string
  }): Promise<Result<SmsConversationInvitationUserDTO, SayHey.Error>> => {
    const { conversationId } = params
    const checkSmsConversationInvitation = async () => {
      const {
        CheckSmsConversationInvitation: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })

      return data
    }
    return this.guardWithAuth(checkSmsConversationInvitation)
  }

  public acceptSmsConversationInvitation = async (params: {
    conversationId: string
    receiverId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const { conversationId } = params
    const acceptSmsConversationInvitation = async () => {
      const {
        AcceptSmsConversationInvitation: { Endpoint, Method }
      } = ChatClientRoutes

      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })

      return true
    }
    return this.guardWithAuth(acceptSmsConversationInvitation)
  }

  public resendSmsConversationInvitation = async (params: {
    conversationId: string
  }): Promise<Result<SmsConversationInvitationDTO, SayHey.Error>> => {
    const { conversationId } = params
    const resendSmsConversationInvitation = async () => {
      const {
        ResendSmsConversationInvitation: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })

      return data
    }
    return this.guardWithAuth(resendSmsConversationInvitation)
  }

  public createSmsConversation = async (params: {
    memberId: string
    groupImageId?: string | null
  }): Promise<Result<ConversationDomainEntity, SayHey.Error>> => {
    const createSmsConversation = async () => {
      const {
        CreateSmsConversation: { Endpoint, Method }
      } = ChatClientRoutes
      const { memberId, groupImageId } = params
      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          memberId,
          groupImageId
        }
      })

      return ConversationMapper.toDomainEntity(data as ConversationDTO)
    }
    return this.guardWithAuth(createSmsConversation)
  }

  public optToReceiveSms = async (): Promise<Result<boolean, SayHey.Error>> => {
    const optToReceiveSms = async () => {
      const {
        OptToReceiveSms: { Endpoint, Method }
      } = ChatClientRoutes

      await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return true
    }
    return this.guardWithAuth(optToReceiveSms)
  }

  public optNotToReceiveSms = async (): Promise<Result<boolean, SayHey.Error>> => {
    const optNotToReceiveSms = async () => {
      const {
        OptNotToReceiveSms: { Endpoint, Method }
      } = ChatClientRoutes

      await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return true
    }
    return this.guardWithAuth(optNotToReceiveSms)
  }

  public getOptToReceiveSmsStatus = async (): Promise<
    Result<ExternalUserOptToReceiveSmsDTO, SayHey.Error>
  > => {
    const getOptToReceiveSmsStatus = async () => {
      const {
        GetOptToReceiveSmsStatus: { Endpoint, Method }
      } = ChatClientRoutes

      const { data } = await this.httpClient({
        url: Endpoint,
        method: Method
      })
      return data
    }
    return this.guardWithAuth(getOptToReceiveSmsStatus)
  }

  public nudgeExternalUserToConversation = async (params: {
    conversationId: string
  }): Promise<Result<boolean, SayHey.Error>> => {
    const { conversationId } = params
    const nudgeExternalUserToConversation = async () => {
      const {
        NudgeExternalUserToConversation: { Endpoint, Method }
      } = ChatClientRoutes

      await this.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return this.guardWithAuth(nudgeExternalUserToConversation)
  }
}
