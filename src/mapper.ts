import { parseText } from './helper'
import {
  AuditMessageBatchDTO,
  AuditMessageDomainEntityBatchDTO,
  AuditMessageDomainEntityDTO,
  AuditMessageDTO,
  ConversationDomainEntity,
  ConversationDTO,
  ConversationReactionsDomainEntity,
  ConversationReactionsDTO,
  ConversationType,
  ConversationUserDomainEntity,
  ConversationUserDomainForUserAdminEntity,
  MediaDomainEntity,
  MediaDTO,
  MessageDomainEntity,
  MessageDTO,
  PeekMessageDomainEntity,
  PeekMessageDTO,
  ReactionType,
  SentMessageDomainEntity,
  SentMessageDTO,
  SystemMessageDomainEntity,
  SystemMessageDTO,
  UserDomainEntity,
  UserDTO,
  UserForUserAdminDomainEntity,
  UserForUserAdminDTO,
  UserOfConversationDTO,
  UserOfConversationForUserAdminDTO,
  UserScope,
  UserSelfDomainEntity,
  UserSelfDTO
} from './types'

export class ConversationMapper {
  static toDomainEntity(conversation: ConversationDTO): ConversationDomainEntity {
    const {
      id,
      lastReadMessageId,
      lastMessage,
      muteUntil,
      pinnedAt,
      badge,
      type,
      createdAt,
      updatedAt,
      isOwner,
      startAfter,
      visible,
      endBefore,
      deletedFromConversationAt,
      serverCreated,
      serverManaged,
      isQuickGroup,
      isSpace,
      lastUnreadNudgedAt,
      creatorId
    } = conversation
    const common = {
      id,
      lastReadMessageId,
      lastMessage: lastMessage ? MessageMapper.toDomainEntity(lastMessage) : null,
      muteUntil: muteUntil ? new Date(muteUntil) : null,
      pinnedAt: pinnedAt ? new Date(pinnedAt) : null,
      createdAt: new Date(createdAt),
      updatedAt: new Date(updatedAt),
      startAfter: startAfter ? new Date(startAfter) : null,
      endBefore: endBefore ? new Date(endBefore) : null,
      deletedFromConversationAt: deletedFromConversationAt
        ? new Date(deletedFromConversationAt)
        : null,
      badge: badge || 0,
      type,
      isOwner,
      visible,
      serverCreated,
      serverManaged,
      isQuickGroup,
      isSpace,
      lastUnreadNudgedAt,
      creatorId
    }
    if (conversation.type === ConversationType.Group) {
      const { group } = conversation
      return {
        ...common,
        type: ConversationType.Group,
        group: {
          ...group,
          picture: group.picture ? MediaMapper.toDomainEntity(group.picture) : null
        }
      }
    }
    const { user } = conversation
    return {
      ...common,
      type: ConversationType.Individual,
      user: {
        ...user,
        picture: user.picture ? MediaMapper.toDomainEntity(user.picture) : null
      }
    }
  }

  static toDomainEntities(conversations: ConversationDTO[]): ConversationDomainEntity[] {
    return conversations.map(conversation => ConversationMapper.toDomainEntity(conversation))
  }
}

export class SentMessageMapper {
  static toDomainEntity(message: SentMessageDTO) {
    const {
      id,
      conversationId,
      sender,
      type,
      text,
      sequence,
      file,
      createdAt,
      reactions,
      refUrl,
      deletedAt
    } = message
    const parsedResult = parseText(text)

    return {
      id,
      conversationId,
      sender,
      type,
      text,
      deletedAt: deletedAt ? new Date(deletedAt) : null,
      sequence,
      createdAt: new Date(createdAt),
      reactions,
      sent: true,
      parsedText: parsedResult ? parsedResult.parsedText : null,
      file: file ? MediaMapper.toDomainEntity(file) : null,
      refUrl,
      systemMessage: null,
      previewUrl: parsedResult?.previewUrl
    } as SentMessageDomainEntity
  }

  static toDomainEntities(messages: SentMessageDTO[]): SentMessageDomainEntity[] {
    return messages.map(value => SentMessageMapper.toDomainEntity(value))
  }
}

export class MessageMapper {
  static toDomainEntity(message: MessageDTO) {
    return {
      ...SentMessageMapper.toDomainEntity(message),
      systemMessage: message.systemMessage
        ? SystemMessageMapper.toDomainEntity(message.systemMessage)
        : null
    } as MessageDomainEntity
  }

  static toDomainEntities(messages: MessageDTO[]): MessageDomainEntity[] {
    return messages.map(value => MessageMapper.toDomainEntity(value))
  }

  static toPeekDomainEntities(messages: PeekMessageDTO[]): PeekMessageDomainEntity[] {
    return messages.map(value => {
      const { flaggedInfo } = value
      return {
        ...MessageMapper.toDomainEntity(value),
        flaggedInfo
      }
    })
  }
}

export class UserMapper {
  static toDomainEntity(user: UserDTO): UserDomainEntity {
    const { firstName, lastName, picture, deletedAt } = user

    if (user.userScope === UserScope.Internal) {
      return {
        ...user,
        deletedAt: deletedAt ? new Date(deletedAt) : null,
        picture: picture ? MediaMapper.toDomainEntity(picture) : null,
        fullName: `${firstName} ${lastName}`,
        email: user.email,
        username: user.email
      }
    }
    return {
      ...user,
      deletedAt: deletedAt ? new Date(deletedAt) : null,
      picture: picture ? MediaMapper.toDomainEntity(picture) : null,
      fullName: `${firstName} ${lastName}`,
      phone: user.phone,
      username: user.phone.phoneNumber
    }
  }

  static toUserForUserAdminDomainEntity(user: UserForUserAdminDTO): UserForUserAdminDomainEntity {
    const { firstName, lastName, picture, deletedAt } = user
    if (user.userScope === UserScope.Internal) {
      return {
        ...user,
        title: user.title,
        employeeId: user.employeeId,
        departmentId: user.departmentId,
        divisionId: user.divisionId,
        division: user.division,
        department: user.department,
        email: user.email,
        deletedAt: deletedAt ? new Date(deletedAt) : null,
        picture: picture ? MediaMapper.toDomainEntity(picture) : null,
        fullName: `${firstName} ${lastName}`,
        username: user.email
      }
    }

    return {
      ...user,
      userScope: user.userScope,
      fullName: `${firstName} ${lastName}`,
      username: user.phone.phoneNumber,
      phone: user.phone,
      deletedAt: deletedAt ? new Date(deletedAt) : null,
      picture: picture ? MediaMapper.toDomainEntity(picture) : null
    }
  }

  static toConversableUserDomainEntity(user: UserOfConversationDTO): ConversationUserDomainEntity {
    const { firstName, lastName, picture, deletedAt } = user

    if (user.userScope === UserScope.Internal) {
      return {
        ...user,
        username: user.email,
        fullName: `${firstName} ${lastName}`,
        picture: picture ? MediaMapper.toDomainEntity(picture) : null,
        deletedAt: deletedAt ? new Date(deletedAt) : null
      }
    }
    return {
      ...user,
      username: user.phone.phoneNumber,
      fullName: `${firstName} ${lastName}`,
      phone: user.phone,
      picture: picture ? MediaMapper.toDomainEntity(picture) : null,
      deletedAt: deletedAt ? new Date(deletedAt) : null
    }
  }

  static toConversableUserDomainForUserAdminEntity(
    user: UserOfConversationForUserAdminDTO
  ): ConversationUserDomainForUserAdminEntity {
    const { firstName, lastName, picture, deletedAt } = user

    if (user.userScope === UserScope.Internal) {
      return {
        ...user,
        username: user.email,
        fullName: `${firstName} ${lastName}`,
        picture: picture ? MediaMapper.toDomainEntity(picture) : null,
        deletedAt: deletedAt ? new Date(deletedAt) : null
      }
    }
    return {
      ...user,
      username: user.phone.phoneNumber,
      fullName: `${firstName} ${lastName}`,
      picture: picture ? MediaMapper.toDomainEntity(picture) : null,
      deletedAt: deletedAt ? new Date(deletedAt) : null
    }
  }

  static toDomainEntities(users: UserDTO[]): UserDomainEntity[] {
    return users.map(UserMapper.toDomainEntity)
  }

  static toUserForUserDomainEntities(users: UserForUserAdminDTO[]): UserForUserAdminDomainEntity[] {
    return users.map(UserMapper.toUserForUserAdminDomainEntity)
  }

  static toConversableUserDomainEntities(
    users: UserOfConversationDTO[]
  ): ConversationUserDomainEntity[] {
    return users.map(UserMapper.toConversableUserDomainEntity)
  }

  static toConversableUserDomainForUserAdminEntities(
    users: UserOfConversationForUserAdminDTO[]
  ): ConversationUserDomainForUserAdminEntity[] {
    return users.map(UserMapper.toConversableUserDomainForUserAdminEntity)
  }

  static toSelfDomainEntity(user: UserSelfDTO): UserSelfDomainEntity {
    const { firstName, lastName, picture, muteAllUntil, deletedAt } = user

    if (user.userScope === UserScope.Internal) {
      return {
        ...user,
        username: user.email,
        fullName: `${firstName} ${lastName}`,
        picture: picture ? MediaMapper.toDomainEntity(picture) : null,
        muteAllUntil: muteAllUntil ? new Date(muteAllUntil) : null,
        deletedAt: deletedAt ? new Date(deletedAt) : null
      }
    }
    return {
      ...user,
      username: user.phone.phoneNumber,
      fullName: `${firstName} ${lastName}`,
      picture: picture ? MediaMapper.toDomainEntity(picture) : null,
      muteAllUntil: muteAllUntil ? new Date(muteAllUntil) : null,
      deletedAt: deletedAt ? new Date(deletedAt) : null
    }
  }
}

export class MediaMapper {
  static toDomainEntity(media: MediaDTO): MediaDomainEntity {
    const {
      urlPath,
      fileSize,
      fileType,
      extension,
      filename,
      mimeType,
      encoding,
      imageInfo,
      videoInfo
    } = media
    return {
      urlPath,
      fileSize,
      fileType,
      extension,
      filename,
      mimeType,
      encoding,
      imageInfo,
      videoInfo: !videoInfo
        ? null
        : {
            duration: videoInfo.duration,
            thumbnail: videoInfo.thumbnail ? MediaMapper.toDomainEntity(videoInfo.thumbnail) : null
          }
    }
  }
}

export class SystemMessageMapper {
  static toDomainEntity(systemMessage: SystemMessageDTO): SystemMessageDomainEntity {
    const { systemMessageType, id, refValue, actorCount, initiator, actors, text } = systemMessage
    return {
      systemMessageType,
      id,
      refValue,
      actorCount,
      initiator,
      actors,
      text
    }
  }
}

export class ReactionMapper {
  static toDomianEntity(reactions: ConversationReactionsDTO[]): ConversationReactionsDomainEntity {
    const conversationReactions: Map<string, ReactionType[]> = new Map()
    reactions.forEach(item => {
      const { messageId } = item
      const reactionTypesArray = Array.isArray(item.type) ? item.type : [item.type]
      const conversationReactionsForMsg = conversationReactions.get(messageId)

      if (!conversationReactionsForMsg) {
        conversationReactions.set(messageId, [])
      }
      const currentReactionTypes = conversationReactionsForMsg || []
      const reactionSet = new Set([...currentReactionTypes, ...reactionTypesArray])

      conversationReactions.set(messageId, Array.from(reactionSet))
    })

    const conversationReactionObj: ConversationReactionsDomainEntity = {}
    const keys = Array.from(conversationReactions.keys())
    for (const key of keys) {
      // SAFE To remove - not user input
      // eslint-disable-next-line security/detect-object-injection
      conversationReactionObj[key] = conversationReactions.get(key) || undefined
    }

    return conversationReactionObj
  }
}

export class AuditMessageMapper {
  static toAuditMessageDomainEntityBatchDTO(
    auditMessageBatchDTO: AuditMessageBatchDTO
  ): AuditMessageDomainEntityBatchDTO {
    const auditMessageDomainEntityDTO = auditMessageBatchDTO.results.map(auditMsg => {
      const messageDomainEntity = SentMessageMapper.toDomainEntity(auditMsg)
      return {
        ...messageDomainEntity,
        flagged: auditMsg.flagged
      }
    })

    return {
      ...auditMessageBatchDTO,
      results: auditMessageDomainEntityDTO
    }
  }

  static toAuditMessageDomainEntityDTO(
    auditMessageDTO: AuditMessageDTO
  ): AuditMessageDomainEntityDTO {
    const messageDomainEntity = SentMessageMapper.toDomainEntity(auditMessageDTO)
    return {
      ...messageDomainEntity,
      flagged: auditMessageDTO.flagged,
      resolved: auditMessageDTO.resolved ? auditMessageDTO.resolved : undefined
    }
  }
}
