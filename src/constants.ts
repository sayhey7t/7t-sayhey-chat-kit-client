import { APISignature, HttpMethod } from './types'

export const API_VERSION = 'v1'

export const ApiKeyName = 'x-sayhey-client-api-key'

export const MESSAGE_READ_UPDATE_INTERVAL = 5000

export const ChatClientRoutes = {
  SignIn: {
    Endpoint: '/users/sign-in',
    Method: HttpMethod.Post
  },
  SignUp: {
    Endpoint: '/users/sign-up',
    Method: HttpMethod.Post
  },
  ChangePassword: {
    Endpoint: '/users/change-password',
    Method: HttpMethod.Post
  },
  ForgotPassword: {
    Endpoint: '/users/forgot-password',
    Method: HttpMethod.Post
  },
  ResetPassword: {
    Endpoint: '/users/reset-password',
    Method: HttpMethod.Post
  },
  GetExistingConversationWithUser: {
    Endpoint: (userId: string) => `/users/${userId}/conversations/individual`,
    Method: HttpMethod.Get
  },
  GetExistingIndvidualOrQuickConversation: {
    Endpoint: '/conversations/individual-quick',
    Method: HttpMethod.Get
  },
  GetConversations: {
    Endpoint: '/conversations',
    Method: HttpMethod.Get
  },
  CreateIndividualConversation: {
    Endpoint: '/conversations/individual',
    Method: HttpMethod.Post
  },
  CreateGroupConversation: {
    Endpoint: '/conversations/group',
    Method: HttpMethod.Post
  },
  CreateQuickConversation: {
    Endpoint: '/conversations/quick-group',
    Method: HttpMethod.Post
  },
  GetMessages: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/messages`,
    Method: HttpMethod.Get
  },
  SendMessage: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/messages`,
    Method: HttpMethod.Post
  },
  MuteConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/mute`,
    Method: HttpMethod.Put
  },
  UnmuteConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/mute`,
    Method: HttpMethod.Delete
  },
  PinConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/pin`,
    Method: HttpMethod.Put
  },
  UnpinConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/pin`,
    Method: HttpMethod.Delete
  },
  DeleteConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/hide-clear`,
    Method: HttpMethod.Put
  },
  DeleteAllConversations: {
    Endpoint: `/conversations/hide-clear`,
    Method: HttpMethod.Put
  },
  UpdateUserProfileImage: {
    Endpoint: '/users/profile-image',
    Method: HttpMethod.Put
  },
  UpdateUserProfileInfo: {
    Endpoint: '/users/profile-info',
    Method: HttpMethod.Put
  },
  GetConversationReachableUsers: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/conversable-users`,
    Method: HttpMethod.Get
  },
  GetUserConversationReactions: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/reactions`,
    Method: HttpMethod.Get
  },
  GetUserConversationMessagesReactions: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/get-messages-reactions`,
    Method: HttpMethod.Post
  },
  SetMessageReaction: {
    Endpoint: (messageId: string) => `/messages/${messageId}/reactions/add`,
    Method: HttpMethod.Put
  },
  RemoveMessageReaction: {
    Endpoint: (messageId: string) => `/messages/${messageId}/reactions/remove`,
    Method: HttpMethod.Put
  },
  MuteAllNotifications: {
    Endpoint: '/users/mute-all',
    Method: HttpMethod.Put
  },
  UnmuteAllNotifications: {
    Endpoint: '/users/mute-all',
    Method: HttpMethod.Delete
  },
  GetUserProfile: {
    Endpoint: '/users/profile-info',
    Method: HttpMethod.Get
  },
  MarkMessageAsRead: {
    Endpoint: (messageId: string) => `/read-messages/${messageId}`,
    Method: HttpMethod.Put
  },
  GetConversationBasicUsers: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/basic-users`,
    Method: HttpMethod.Get
  },
  RegisterPushNotification: {
    Endpoint: '/device-tokens',
    Method: HttpMethod.Post
  },
  UnregisterPushNotification: {
    Endpoint: (instanceId: string) => `/device-tokens/${instanceId}`,
    Method: HttpMethod.Delete
  },
  SetPassword: {
    Endpoint: '/users/set-password',
    Method: HttpMethod.Post
  },
  CheckEmail: {
    Endpoint: '/users/email-check',
    Method: HttpMethod.Post
  },
  RequestSignUpOTP: {
    Endpoint: '/otp/sign-up',
    Method: HttpMethod.Post
  },
  RequestSetPasswordOTP: {
    Endpoint: '/otp/set-password',
    Method: HttpMethod.Post
  },
  RequestResetPasswordOTP: {
    Endpoint: '/otp/reset-password',
    Method: HttpMethod.Post
  },
  RequestPreverificationOTP: {
    Endpoint: '/otp/pre-verification',
    Method: HttpMethod.Post
  },
  ValidateSignUpOTP: {
    Endpoint: '/otp/validate-sign-up',
    Method: HttpMethod.Post
  },
  ValidateSetPasswordOTP: {
    Endpoint: '/otp/validate-set-password',
    Method: HttpMethod.Post
  },
  ValidateResetPasswordOTP: {
    Endpoint: '/otp/validate-reset-password',
    Method: HttpMethod.Post
  },
  ValidatePreverificationOTP: {
    Endpoint: '/otp/validate-pre-verification',
    Method: HttpMethod.Post
  },
  GetAppConfig: {
    Endpoint: '/app-config',
    Method: HttpMethod.Get
  },
  ClientSignInWithToken: {
    Endpoint: '/users/client-signin-with-token',
    Method: HttpMethod.Post
  },
  CreateFlaggedMessage: {
    Endpoint: '/flagged-messages',
    Method: HttpMethod.Post
  },

  /* SMS */

  AddToContact: {
    Endpoint: 'contacts',
    Method: HttpMethod.Post
  },
  GetInternalUserContactsBatch: {
    Endpoint: 'contacts',
    Method: HttpMethod.Get
  },
  FindExternalUserByPhone: {
    Endpoint: 'external-users',
    Method: HttpMethod.Get
  },
  RequestSignInExternalOTP: {
    Endpoint: 'otp/sign-in-external',
    Method: HttpMethod.Post
  },
  SignInExternal: {
    Endpoint: 'users/sign-in-external',
    Method: HttpMethod.Post
  },
  SignInExternalByUserId: {
    Endpoint: (id: string) => `users/${id}/sign-in-external'`,
    Method: HttpMethod.Post
  },
  CreateNewContact: {
    Endpoint: 'users',
    Method: HttpMethod.Post
  },
  AcceptTermsAndPrivacy: {
    Endpoint: 'external-users/accepted-terms-privacy',
    Method: HttpMethod.Post
  },
  GetTermsAndPrivacy: {
    Endpoint: 'external-users/accepted-terms-privacy',
    Method: HttpMethod.Get
  },
  GetExistingSmsConversation: {
    Endpoint: 'conversations/existing-sms-conversations',
    Method: HttpMethod.Get
  },
  CheckSmsConversationInvitation: {
    Endpoint: (conversationId: string) =>
      `conversations/${conversationId}/smsConversationInvitations/status`,
    Method: HttpMethod.Get
  },
  AcceptSmsConversationInvitation: {
    Endpoint: (conversationId: string) =>
      `conversations/${conversationId}/smsConversationInvitations/status`,
    Method: HttpMethod.Patch
  },
  ResendSmsConversationInvitation: {
    Endpoint: (conversationId: string) =>
      `conversations/${conversationId}/smsConversationInvitations`,
    Method: HttpMethod.Put
  },
  CreateSmsConversation: {
    Endpoint: 'conversations/sms-group',
    Method: HttpMethod.Post
  },
  OptToReceiveSms: {
    Endpoint: 'external-users/opted-receive-sms',
    Method: HttpMethod.Put
  },
  OptNotToReceiveSms: {
    Endpoint: 'external-users/opted-receive-sms',
    Method: HttpMethod.Delete
  },
  GetOptToReceiveSmsStatus: {
    Endpoint: 'external-users/opted-receive-sms',
    Method: HttpMethod.Get
  },
  NudgeExternalUserToConversation: {
    Endpoint: (conversationId: string) =>
      `conversations/${conversationId}/nudge-unread-sms-conversation`,
    Method: HttpMethod.Put
  }
}

export const ChatAdminRoutes = {
  /* User admins */
  Invite: {
    Endpoint: '/user-admins/invitation',
    Method: HttpMethod.Post
  },
  SignUp: {
    Endpoint: '/user-admins/sign-up',
    Method: HttpMethod.Post
  },
  SignIn: {
    Endpoint: '/user-admins/sign-in',
    Method: HttpMethod.Post
  },
  ReInvite: {
    Endpoint: (userAdminId: string) => `/user-admins/${userAdminId}/reinvitation`,
    Method: HttpMethod.Post
  },
  DisableAdmin: {
    Endpoint: (userAdminId: string) => `/user-admins/${userAdminId}/disable`,
    Method: HttpMethod.Put
  },
  EnableAdmin: {
    Endpoint: (userAdminId: string) => `/user-admins/${userAdminId}/enable`,
    Method: HttpMethod.Put
  },
  ForgotPassword: {
    Endpoint: '/user-admins/forgot-password',
    Method: HttpMethod.Post
  },
  ResetPassword: {
    Endpoint: '/user-admins/reset-password',
    Method: HttpMethod.Post
  },
  GetUserAdmins: {
    Endpoint: '/user-admins',
    Method: HttpMethod.Get
  },
  UpdateUserAdminInfo: {
    Endpoint: (userAdminId: string) => `/user-admins/${userAdminId}`,
    Method: HttpMethod.Patch
  },
  UpdateUserAdminRole: {
    Endpoint: (userAdminId: string) => `/user-admins/${userAdminId}/role`,
    Method: HttpMethod.Patch
  },
  UpdateUserAdminOnLeave: {
    Endpoint: '/user-admins/onLeave',
    Method: HttpMethod.Patch
  },
  GetUserAdminSelfInfo: {
    Endpoint: `/user-admins/${APISignature.ME}`,
    Method: HttpMethod.Get
  },
  UpdateUserAdminSelfInfo: {
    Endpoint: `/user-admins/${APISignature.ME}`,
    Method: HttpMethod.Patch
  },

  /* User Management */
  AssignManagedUsers: {
    Endpoint: '/user-managements',
    Method: HttpMethod.Post
  },
  MoveAllManagedUsers: {
    Endpoint: '/user-managements',
    Method: HttpMethod.Put
  },
  MoveBackAllManagedUsers: {
    Endpoint: (userAdminId: string) => `/user-managements/${userAdminId}/movement`,
    Method: HttpMethod.Put
  },
  GetManagedUsers: {
    Endpoint: '/user-managements',
    Method: HttpMethod.Get
  },
  GetManagedUsersCount: {
    Endpoint: '/user-managements/count',
    Method: HttpMethod.Get
  },
  MoveSelectedUsers: {
    Endpoint: '/user-managements/users/move-selected',
    Method: HttpMethod.Put
  },

  /* Users */
  RegisterUser: {
    Endpoint: '/users/registration',
    Method: HttpMethod.Post
  },
  DisableUser: {
    Endpoint: (userId: string) => `/users/${userId}/disabled`,
    Method: HttpMethod.Put
  },
  EnableUser: {
    Endpoint: (userId: string) => `/users/${userId}/disabled`,
    Method: HttpMethod.Delete
  },
  UpdateUser: {
    Endpoint: (userId: string) => `/users/${userId}`,
    Method: HttpMethod.Put
  },
  GetUnmanagedUsers: {
    Endpoint: '/users/unmanaged',
    Method: HttpMethod.Get
  },
  GetUnmanagedUsersCount: {
    Endpoint: '/users/unmanaged-count',
    Method: HttpMethod.Get
  },

  /* Departments */
  CreateDepartment: {
    Endpoint: '/departments',
    Method: HttpMethod.Post
  },
  GetDepartments: {
    Endpoint: '/departments',
    Method: HttpMethod.Get
  },
  UpdateDepartments: {
    Endpoint: (departmentId: string) => `/departments/${departmentId}`,
    Method: HttpMethod.Put
  },

  /* Divisions */
  CreateDivisions: {
    Endpoint: (departmentId: string) => `/departments/${departmentId}/divisions`,
    Method: HttpMethod.Post
  },

  GetDivisions: {
    Endpoint: (departmentId: string) => `/departments/${departmentId}/divisions`,
    Method: HttpMethod.Get
  },

  UpdateDivisions: {
    Endpoint: ({ departmentId, divisionId }: { departmentId: string; divisionId: string }) =>
      `/departments/${departmentId}/divisions/${divisionId}`,
    Method: HttpMethod.Put
  },

  /* Keywords */
  CreateKeyword: {
    Endpoint: '/keywords',
    Method: HttpMethod.Post
  },
  CreateKeywordBulk: {
    Endpoint: '/keywords/bulk',
    Method: HttpMethod.Post
  },
  DeleteKeyword: {
    Endpoint: (id: string) => `/keywords/${id}`,
    Method: HttpMethod.Delete
  },
  UpdateKeyword: {
    Endpoint: (id: string) => `/keywords/${id}`,
    Method: HttpMethod.Patch
  },
  GetKeywordsBatch: {
    Endpoint: '/keywords',
    Method: HttpMethod.Get
  },

  GetMessagesBeforeSequence: {
    Endpoint: (conversationId: string) => `/conversation/${conversationId}/before-sequence`,
    Method: HttpMethod.Get
  },
  GetMessagesAfterSequence: {
    Endpoint: (conversationId: string) => `/conversation/${conversationId}/after-sequence`,
    Method: HttpMethod.Get
  },
  GetMessageContext: {
    Endpoint: (messageId: string) => `/messages/${messageId}/context`,
    Method: HttpMethod.Get
  },
  GetAuditMessagesBatch: {
    Endpoint: '/messages/audit',
    Method: HttpMethod.Get
  },
  DownloadAuditMessages: {
    Endpoint: '/messages/audit-download',
    Method: HttpMethod.Get
  },
  CreateUpdateUserAdminSearch: {
    Endpoint: '/user-admin-searches',
    Method: HttpMethod.Put
  },
  GetUserAdminSearch: {
    Endpoint: '/user-admin-searches',
    Method: HttpMethod.Get
  },
  GetUserBasicInfoBatch: {
    Endpoint: '/users',
    Method: HttpMethod.Get
  },
  // Monitoring or Flagged Messages
  GetFlaggedMessagesBatch: {
    Endpoint: '/flagged-messages',
    Method: HttpMethod.Get
  },
  GetFlaggedMessagesCount: {
    Endpoint: '/flagged-messages/count',
    Method: HttpMethod.Get
  },
  ResolveFlaggedMessages: {
    Endpoint: (id: string) => `/flagged-messages/${id}/resolve`,
    Method: HttpMethod.Post
  },
  GetFlaggedMessageDetails: {
    Endpoint: (id: string) => `/flagged-messages/${id}`,
    Method: HttpMethod.Get
  },
  GetFlaggedMessageUserReports: {
    Endpoint: (id: string) => `/flagged-messages/${id}/user-reports`,
    Method: HttpMethod.Get
  },
  GetFlaggedMessageKeywordScans: {
    Endpoint: (id: string) => `/flagged-messages/${id}/keyword-scans`,
    Method: HttpMethod.Get
  },
  GetFlaggedMessageResolutions: {
    Endpoint: (id: string) => `/flagged-messages/${id}/resolutions`,
    Method: HttpMethod.Get
  },
  GetFlaggedMessageResolutionUserReports: {
    Endpoint: (id: string) => `/flagged-resolutions/${id}/user-reports`,
    Method: HttpMethod.Get
  },
  GetFlaggedMessageResolutionKeywordScans: {
    Endpoint: (id: string) => `/flagged-resolutions/${id}/keyword-scans`,
    Method: HttpMethod.Get
  },

  /* Groups */
  CreateGroupConversation: {
    Endpoint: '/conversations/user-admin-managed',
    Method: HttpMethod.Post
  },
  UpdateGroupConversationOwner: {
    Endpoint: (conversationId: string, userAdminId: string) =>
      `conversations/${conversationId}/owner/${userAdminId}`,
    Method: HttpMethod.Patch
  },
  GetUserAdminGroups: {
    Endpoint: 'conversations/user-admin-groups',
    Method: HttpMethod.Get
  },

  /* Conversation Space Group Divisions */

  GetSpaceGroupDivisions: {
    Endpoint: (conversationId: string) => `conversations/${conversationId}/space-group-divisions`,
    Method: HttpMethod.Get
  },
  CreateConversationSpaceGroupDivision: {
    Endpoint: (conversationId: string) => `conversations/${conversationId}/space-group-divisions`,
    Method: HttpMethod.Post
  },
  DeleteConversationSpaceGroupDivision: {
    Endpoint: (conversationId: string) => `conversations/${conversationId}/space-group-divisions`,
    Method: HttpMethod.Delete
  },

  /* Dashboard */
  GetTotalUsersCount: {
    Endpoint: 'users/total-count',
    Method: HttpMethod.Get
  },
  GetMyUsersCount: {
    Endpoint: 'user-managements/my-users-count',
    Method: HttpMethod.Get
  },
  GetResolvedFlaggedMessagesCount: {
    Endpoint: 'flagged-messages/resolved/total-count',
    Method: HttpMethod.Get
  },
  GetSentMessagesCountDaily: {
    Endpoint: 'sent-message-counts/daily',
    Method: HttpMethod.Get
  },
  GetSentMessagesCountTotal: {
    Endpoint: 'sent-message-counts/total-count',
    Method: HttpMethod.Get
  },
  GetUnresolvedFlaggedMessagesCount: {
    Endpoint: 'flagged-messages/unresolved/total-count',
    Method: HttpMethod.Get
  },
  GetUnresolvedFlaggedMessageCountsDaily: {
    Endpoint: 'flagged-messages/unresolved/daily',
    Method: HttpMethod.Get
  },
  GetEligibleRolesToInviteAdmin: {
    Endpoint: `/user-admins/invitable-roles`,
    Method: HttpMethod.Get
  },

  /* User Onboarding Job */

  ProcessUserOnboardingJobUpload: {
    Endpoint: 'user-onboarding-jobs/files',
    Method: HttpMethod.Post
  },
  GetUserOnboardingJobBatch: {
    Endpoint: 'user-onboarding-jobs',
    Method: HttpMethod.Get
  },
  DownloadSampleFile: {
    Endpoint: 'user-onboarding-jobs/download',
    Method: HttpMethod.Get
  },
  GetAllErroredUserOnboardingJobRecords: {
    Endpoint: (id: string) => `user-onboarding-jobs/${id}/user-onboarding-job-records/errored`,
    Method: HttpMethod.Get
  }
}

export const ChatCommonRoutes = {
  UploadFile: {
    Endpoint: '/files',
    Method: HttpMethod.Post
  },
  GetReactedUsers: {
    Endpoint: (messageId: string) => `/messages/${messageId}/reacted-users`,
    Method: HttpMethod.Get
  },
  GetReachableUsers: {
    Endpoint: '/users/conversable',
    Method: HttpMethod.Get
  },
  GetConversationUsers: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/users`,
    Method: HttpMethod.Get
  },
  AddUsersToConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/users`,
    Method: HttpMethod.Post
  },
  RemoveUsersFromConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/delete-users`,
    Method: HttpMethod.Post
  },
  UpdateGroupImage: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/group-image`,
    Method: HttpMethod.Put
  },
  RemoveGroupImage: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/group-image`,
    Method: HttpMethod.Delete
  },
  GetGroupDefaultImageSet: {
    Endpoint: '/default-group-images',
    Method: HttpMethod.Get
  },
  UpdateGroupInfo: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/group-info`,
    Method: HttpMethod.Put
  },
  MakeConversationOwner: {
    Endpoint: (conversationId: string, userId: string) =>
      `/conversations/${conversationId}/owners/${userId}`,
    Method: HttpMethod.Put
  },
  RemoveConversationOwner: {
    Endpoint: (conversationId: string, userId: string) =>
      `/conversations/${conversationId}/owners/${userId}`,
    Method: HttpMethod.Delete
  },
  GetConversationDetails: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}`,
    Method: HttpMethod.Get
  },
  DeleteGroupConversation: {
    Endpoint: (conversationId: string) => `conversations/${conversationId}`,
    Method: HttpMethod.Delete
  }
}
